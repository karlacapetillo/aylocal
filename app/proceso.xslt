﻿<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:x="http://panax.io/xdom"
  xmlns:state="http://panax.io/state"
  xmlns:px="http://panax.io"
  xmlns="http://www.w3.org/1999/xhtml"
  exclude-result-prefixes="x state px"
>
  <xsl:import href="resources/keys.xslt"/>
  <xsl:import href="resources/controls.xslt"/>
  <xsl:import href="templates/FO-PG-CPR-016_Alta_proveedores.xsl"/>

  <xsl:variable name="resources-path">http://localhost/petro/Version/Beta_12.0/xdom/templates/</xsl:variable>

  <xsl:key name="data" match="Proveedor/px:data/px:dataRow/@identity" use="'Id'"/>
  <xsl:key name="data" match="Proveedor/px:data/px:dataRow/Nombre" use="'Proveedor/Nombre'"/>
  <xsl:key name="data" match="Proveedor/px:data/px:dataRow/RFC" use="'Proveedor/RFC'"/>
  <xsl:key name="data" match="Proveedor/px:data/px:dataRow/Domicilio" use="'Calle'"/>
  <xsl:key name="data" match="Proveedor/px:data/px:dataRow/NumeroExterior" use="'NumeroExterior'"/>
  <xsl:key name="data" match="Proveedor/px:data/px:dataRow/NumeroInterior" use="'NumeroInterior'"/>
  <xsl:key name="data" match="Proveedor/px:data/px:dataRow/Colonia" use="'Colonia'"/>
  <xsl:key name="data" match="Proveedor/px:data/px:dataRow/CodigoPostal" use="'CP'"/>
  <xsl:key name="data" match="Proveedor/px:data/px:dataRow/Telefono" use="'Telefono'"/>
  <xsl:key name="data" match="Proveedor/px:data/px:dataRow/Localidad" use="'Localidad'"/>
  <xsl:key name="data" match="Proveedor/px:data/px:dataRow/Municipio" use="'Municipio'"/>
  <xsl:key name="data" match="Proveedor/px:data/px:dataRow/Estado" use="'EntidadFederativa'"/>
  <xsl:key name="data" match="Proveedor/px:data/px:dataRow/Email" use="'Contacto/Email'"/>
  <xsl:key name="data" match="ProductoServicio" use="'ProductoServicio'"/>
  <xsl:key name="data" match="Contacto/Nombre" use="'Contacto/Nombre'"/>
  <xsl:key name="data" match="DiasCredito" use="'DiasCredito'"/>
  <xsl:key name="data" match="Banco" use="'Banco'"/>
  <xsl:key name="data" match="CuentaBancaria/Ciudad" use="'CuentaBancaria/Ciudad'"/>
  <xsl:key name="data" match="CuentaBancaria/Estado" use="'CuentaBancaria/Estado'"/>
  <xsl:key name="data" match="NumeroCuenta" use="'NumeroCuenta'"/>
  <xsl:key name="data" match="Moneda" use="'Moneda'"/>
  <xsl:key name="data" match="Sucursal" use="'Sucursal'"/>
  <xsl:key name="data" match="Sucursal/Entidad" use="'Sucursal/Entidad'"/>
  <xsl:key name="data" match="Plaza" use="'Plaza'"/>
  <xsl:key name="data" match="Plaza/Entidad" use="'Plaza/Entidad'"/>
  <xsl:key name="data" match="CLABE" use="'CLABE'"/>
  <xsl:key name="data" match="Proveedor/px:data/px:dataRow/InformacionAdicional" use="'InformacionAdicional'"/>
  <xsl:key name="data" match="Solicita/Nombre" use="'Solicita/Nombre'"/>
  <xsl:key name="data" match="Departamento" use="'Departamento'"/>
  <xsl:key name="data" match="Fecha" use="'Fecha'"/>

  <xsl:template match="@*">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="*" mode="resources-path">
    <xsl:value-of select="$resources-path"/>
  </xsl:template>

  <xsl:template mode="control.attributes" match="*">
    <xsl:attribute name="style">background-color: aliceblue;</xsl:attribute>
  </xsl:template>

  <xsl:template mode="control.attributes" match="Calle">
    <xsl:attribute name="style">background-color: aliceblue; max-height: 38;vertical-align: top;</xsl:attribute>
  </xsl:template>

  <xsl:template mode="html.head" match="*">
    <link rel="stylesheet" href="{$resources-path}../css/fontawesome.all.css" />
    <link rel="stylesheet" href="{$resources-path}../css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
  </xsl:template>

  <xsl:template match="px:dataRow/*">
    <xsl:variable name="field" select="key('field',@fieldId)"/>
    <xsl:apply-templates mode="control" select="$field">
      <xsl:with-param name="data_field" select="."/>
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template match="CLABE" mode="container">
    <xsl:attribute name="style">
      <xsl:text/>display:inline; letter-spacing: 4.3px; font-family: monospace;<xsl:text/>
    </xsl:attribute>
    <style>
      .CLABE_5,.CLABE_10,.CLABE_15,.CLABE_20 {color:black;}
    </style>
  </xsl:template>

  <xsl:template match="CLABE">
    <xsl:call-template name="separate-letters">
      <xsl:with-param name="string" select="text()"></xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template name="separate-letters">
    <xsl:param name="string"/>
    <span class="{concat(local-name(),'_',string-length(text())-string-length($string)+1)}">
      <xsl:text/>
      <xsl:value-of select="substring($string,1,1)"/>
      <xsl:text/>
    </span>
    <xsl:if test="string-length($string)&gt;1">
      <xsl:call-template name="separate-letters">
        <xsl:with-param name="string" select="normalize-space(substring($string,2))"></xsl:with-param>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>
</xsl:stylesheet>