﻿<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:x="http://panax.io/xdom"
xmlns:source="http://panax.io/xdom/binding/source"
xmlns:query="http://panax.io/xdom/binding/query"
>
  <xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>
  <xsl:key name="bound" match="source:*" use="concat(name(),'_',generate-id(..),'::',@for)"/>
  <xsl:key name="bound" match="source:*" use="concat(name(),'::',@for)"/>


  <xsl:template match="@*|text()|node()" mode="sources"/>

  <xsl:template match="@*" mode="sources">
    <xsl:if test="not(key('bound',concat(name(),'_',generate-id(..),'::',.)))">
      <xsl:copy-of select="key('bound',concat(name(),'::',.))[1]"/>
    </xsl:if>
  </xsl:template>

  <xsl:template match="@* | node() | text()" priority="-1">
    <xsl:copy>
      <xsl:copy-of select="@*"/>
      <xsl:apply-templates/>
      <xsl:apply-templates mode="sources" select="@source:*"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>