﻿<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml" xmlns:session="http://panax.io/session"
xmlns:sitemap="http://panax.io/sitemap"
xmlns:shell="http://panax.io/shell"
xmlns:state="http://panax.io/state"
exclude-result-prefixes="#default session sitemap shell"
>
  <xsl:output method="xml"
     omit-xml-declaration="yes"
     indent="yes"/>

  <xsl:template match="/" priority="-1">
    <section>
      <xsl:apply-templates mode="shell"/>
    </section>
  </xsl:template>

  <xsl:template match="shell:shell" mode="shell">
    <div id="shell" class="wrapper">
      <style>
        body {
        overflow-y: unset;
        }
      </style>
      <!--estilos-->
      <!--Hacer que el código se ejecute cuando se carge. Provisionalmente se agrega a la página principal-->
      <!--<script>
        <![CDATA[
        function toggle_sidebar() {
            $(".sidebar").toggleClass("toggled").one("transitionend", function () { setTimeout(function () { window.dispatchEvent(new Event("resize")) }, 100) });
        }

        function toggle_settings() {
            $(".settings").toggleClass("open").one("transitionend", function () { setTimeout(function () { window.dispatchEvent(new Event("resize")) }, 100) });
        }]]>
      </script>-->
      <div class="wrapper">
        <nav class="sidebar" style="background: #00B0AD">
          <xsl:if test="not(//sitemap:*)">
            <xsl:attribute name="class">sidebar toggled</xsl:attribute>
          </xsl:if>
          <div class="sidebar-content " style="background: #00B0AD">
            <strong>
              <a class="sidebar-brand" href="#shell">
                <img src="custom/images/viva.png" height="39px" />
              </a>
            </strong>

            <ul class="sidebar-nav">
              <xsl:apply-templates mode="sitemap" select="/"/>
            </ul>

            <div class="sidebar-bottom d-none d-lg-block" style="background: #006C6C">
              <div class="media">
                <xsl:choose>
                  <xsl:when test="@session:user_id=-1">
                    <img class="rounded-circle mr-3" src="./custom/images/jacki.jpg" alt="{@session:user_login}" width="50" height="50"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <span class="fas fa-user-circle rounded-circle mr-3" style="font-size: 4em;"></span>
                  </xsl:otherwise>
                </xsl:choose>

                <div class="media-body">
                  <h5 class="mb-1">
                    <xsl:choose>
                      <xsl:when test="@session:user_id=-1">
                        <xsl:value-of select="@session:user_login"/>
                      </xsl:when>
                      <xsl:when test="@session:user_login">
                        <xsl:value-of select="@session:user_login"/>
                      </xsl:when>
                      <xsl:otherwise>Invitado</xsl:otherwise>
                    </xsl:choose>
                  </h5>
                  <div>
                    <i class="fas fa-circle text-success"></i> Online
                  </div>
                </div>
              </div>


            </div>
          </div>
        </nav>

        <div class="main">
          <nav class="navbar navbar-expand navbar-light bg-white" style="padding:.6rem 1.25rem;">
            <xsl:if test="//sitemap:*">
              <a class="sidebar-toggle d-flex mr-2" onclick="toggle_sidebar()">
                <i class="hamburger align-self-center"></i>
              </a>
            </xsl:if>
            <h2 id="main_title" style="margin:0;"></h2>

            <!--<form class="form-inline d-none d-sm-inline-block">
                    <input class="form-control form-control-no-border mr-sm-2" type="text" placeholder="Search projects..." aria-label="Search">
                </form>-->

            <div class="navbar-collapse collapse">

              <!--Barra de búsqueda-->
              <img src="custom/images/EmblemaAyLocal_VHorizontal.png" height="40px" />
              <!-- <div class="col-md-3 offset-md-1 mt-2">
                <div class="input-group mb-2">
                  <input type="text" class="form-control" placeholder="Prueba... productos disponibles"/>
                  <div class="input-group-append">
                    <span class="input-group-text"><i class="fa fa-search"></i></span>
                  </div>
                </div>
              </div>-->
              <form style=" width: 400px;" class="d-none d-sm-inline-block form-inline mr-auto ml-md-5 my-2 my-md-0 mw-100 navbar-search" >
                <div class="input-group">
                  <input type="text" class="form-control bg-light border-0 small" placeholder="Busca un producto o servicio..." aria-label="Search" aria-describedby="basic-addon2"/>
                  <div class="input-group-append">
                    <button class="btn btn-primary" type="button" onclick="xdom.data.load('productos.xml', function(){{ xdom.dom.navigateTo('#busqueda'); }})">
                      <i class="fas fa-search fa-sm"></i>
                    </button>
                  </div>
                </div>
              </form>


              <ul class="navbar-nav ml-auto">
                <!--<li class="nav-item dropdown">
                  <a class="nav-icon dropdown-toggle" href="#" id="messagesDropdown" data-toggle="dropdown">
                    <div class="position-relative">
                      <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-message-circle align-middle">
                        <path d="M21 11.5a8.38 8.38 0 0 1-.9 3.8 8.5 8.5 0 0 1-7.6 4.7 8.38 8.38 0 0 1-3.8-.9L3 21l1.9-5.7a8.38 8.38 0 0 1-.9-3.8 8.5 8.5 0 0 1 4.7-7.6 8.38 8.38 0 0 1 3.8-.9h.5a8.48 8.48 0 0 1 8 8v.5z"></path>
                      </svg>
                      <span class="indicator">4</span>
                    </div>
                  </a>
                  <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right py-0" aria-labelledby="messagesDropdown">
                    <div class="dropdown-menu-header">
                      <div class="position-relative">
                        4 New Messages
                      </div>
                    </div>
                    <div class="list-group">
                      <a href="#" class="list-group-item">
                        <div class="row no-gutters align-items-center">
                          <div class="col-2">
                            <img src="./resources/avatar-5.jpg" class="avatar img-fluid rounded-circle" alt="Ashley Briggs"/>
                          </div>
                          <div class="col-10 pl-2">
                            <div class="text-dark">Ashley Briggs</div>
                            <div class="text-muted small mt-1">Nam pretium turpis et arcu. Duis arcu tortor.</div>
                            <div class="text-muted small mt-1">15m ago</div>
                          </div>
                        </div>
                      </a>
                      <a href="#" class="list-group-item">
                        <div class="row no-gutters align-items-center">
                          <div class="col-2">
                            <img src="./resources/avatar-2.jpg" class="avatar img-fluid rounded-circle" alt="Carl Jenkins"/>
                          </div>
                          <div class="col-10 pl-2">
                            <div class="text-dark">Carl Jenkins</div>
                            <div class="text-muted small mt-1">Curabitur ligula sapien euismod vitae.</div>
                            <div class="text-muted small mt-1">2h ago</div>
                          </div>
                        </div>
                      </a>
                      <a href="#" class="list-group-item">
                        <div class="row no-gutters align-items-center">
                          <div class="col-2">
                            <img src="./resources/avatar-4.jpg" class="avatar img-fluid rounded-circle" alt="Stacie Hall"/>
                          </div>
                          <div class="col-10 pl-2">
                            <div class="text-dark">Stacie Hall</div>
                            <div class="text-muted small mt-1">Pellentesque auctor neque nec urna.</div>
                            <div class="text-muted small mt-1">4h ago</div>
                          </div>
                        </div>
                      </a>
                      <a href="#" class="list-group-item">
                        <div class="row no-gutters align-items-center">
                          <div class="col-2">
                            <img src="./resources/avatar-3.jpg" class="avatar img-fluid rounded-circle" alt="Bertha Martin"/>
                          </div>
                          <div class="col-10 pl-2">
                            <div class="text-dark">Bertha Martin</div>
                            <div class="text-muted small mt-1">Aenean tellus metus, bibendum sed, posuere ac, mattis non.</div>
                            <div class="text-muted small mt-1">5h ago</div>
                          </div>
                        </div>
                      </a>
                    </div>
                    <div class="dropdown-menu-footer">
                      <a href="#" class="text-muted">Show all messages</a>
                    </div>
                  </div>
                </li>-->
                <!--<li class="nav-item dropdown">
                  <a class="nav-icon dropdown-toggle" href="#" id="alertsDropdown" data-toggle="dropdown">
                    <div class="position-relative">
                      <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-bell-off align-middle">
                        <path d="M13.73 21a2 2 0 0 1-3.46 0"></path>
                        <path d="M18.63 13A17.89 17.89 0 0 1 18 8"></path>
                        <path d="M6.26 6.26A5.86 5.86 0 0 0 6 8c0 7-3 9-3 9h14"></path>
                        <path d="M18 8a6 6 0 0 0-9.33-5"></path>
                        <line x1="1" y1="1" x2="23" y2="23"></line>
                      </svg>
                    </div>
                  </a>
                  <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right py-0" aria-labelledby="alertsDropdown">
                    <div class="dropdown-menu-header">
                      4 New Notifications
                    </div>
                    <div class="list-group">
                      <a href="#" class="list-group-item">
                        <div class="row no-gutters align-items-center">
                          <div class="col-2">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-alert-circle text-danger">
                              <circle cx="12" cy="12" r="10"></circle>
                              <line x1="12" y1="8" x2="12" y2="12"></line>
                              <line x1="12" y1="16" x2="12" y2="16"></line>
                            </svg>
                          </div>
                          <div class="col-10">
                            <div class="text-dark">Update completed</div>
                            <div class="text-muted small mt-1">Restart server 12 to complete the update.</div>
                            <div class="text-muted small mt-1">2h ago</div>
                          </div>
                        </div>
                      </a>
                      <a href="#" class="list-group-item">
                        <div class="row no-gutters align-items-center">
                          <div class="col-2">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-bell text-warning">
                              <path d="M18 8A6 6 0 0 0 6 8c0 7-3 9-3 9h18s-3-2-3-9"></path>
                              <path d="M13.73 21a2 2 0 0 1-3.46 0"></path>
                            </svg>
                          </div>
                          <div class="col-10">
                            <div class="text-dark">Lorem ipsum</div>
                            <div class="text-muted small mt-1">Aliquam ex eros, imperdiet vulputate hendrerit et.</div>
                            <div class="text-muted small mt-1">6h ago</div>
                          </div>
                        </div>
                      </a>
                      <a href="#" class="list-group-item">
                        <div class="row no-gutters align-items-center">
                          <div class="col-2">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home text-primary">
                              <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                              <polyline points="9 22 9 12 15 12 15 22"></polyline>
                            </svg>
                          </div>
                          <div class="col-10">
                            <div class="text-dark">Login from 192.186.1.1</div>
                            <div class="text-muted small mt-1">8h ago</div>
                          </div>
                        </div>
                      </a>
                      <a href="#" class="list-group-item">
                        <div class="row no-gutters align-items-center">
                          <div class="col-2">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user-plus text-success">
                              <path d="M16 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path>
                              <circle cx="8.5" cy="7" r="4"></circle>
                              <line x1="20" y1="8" x2="20" y2="14"></line>
                              <line x1="23" y1="11" x2="17" y2="11"></line>
                            </svg>
                          </div>
                          <div class="col-10">
                            <div class="text-dark">New connection</div>
                            <div class="text-muted small mt-1">Anna accepted your request.</div>
                            <div class="text-muted small mt-1">12h ago</div>
                          </div>
                        </div>
                      </a>
                    </div>
                    <div class="dropdown-menu-footer">
                      <a href="#" class="text-muted">Show all notifications</a>
                    </div>
                  </div>
                </li>-->
                <!--<li class="nav-item dropdown">
                  <a class="nav-flag dropdown-toggle" href="#" id="languageDropdown" data-toggle="dropdown">
                    <img src="./resources/us.png" alt="English"/>
                  </a>
                  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="languageDropdown">
                    <a class="dropdown-item" href="#">
                      <img src="./resources/us.png" alt="English" width="20" class="align-middle mr-1"/>
                      <span class="align-middle">English</span>
                    </a>
                    <a class="dropdown-item" href="#">
                      <img src="./resources/es.png" alt="Spanish" width="20" class="align-middle mr-1"/>
                      <span class="align-middle">Spanish</span>
                    </a>
                    <a class="dropdown-item" href="#">
                      <img src="./resources/de.png" alt="German" width="20" class="align-middle mr-1"/>
                      <span class="align-middle">German</span>
                    </a>
                    <a class="dropdown-item" href="#">
                      <img src="./resources/nl.png" alt="Dutch" width="20" class="align-middle mr-1"/>
                      <span class="align-middle">Dutch</span>
                    </a>
                  </div>
                </li>-->
                <li class="nav-item dropdown">
                  <a class="nav-icon dropdown-toggle d-inline-block d-sm-none" href="#" data-toggle="dropdown">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-settings align-middle">
                      <circle cx="12" cy="12" r="3"></circle>
                      <path d="M19.4 15a1.65 1.65 0 0 0 .33 1.82l.06.06a2 2 0 0 1 0 2.83 2 2 0 0 1-2.83 0l-.06-.06a1.65 1.65 0 0 0-1.82-.33 1.65 1.65 0 0 0-1 1.51V21a2 2 0 0 1-2 2 2 2 0 0 1-2-2v-.09A1.65 1.65 0 0 0 9 19.4a1.65 1.65 0 0 0-1.82.33l-.06.06a2 2 0 0 1-2.83 0 2 2 0 0 1 0-2.83l.06-.06a1.65 1.65 0 0 0 .33-1.82 1.65 1.65 0 0 0-1.51-1H3a2 2 0 0 1-2-2 2 2 0 0 1 2-2h.09A1.65 1.65 0 0 0 4.6 9a1.65 1.65 0 0 0-.33-1.82l-.06-.06a2 2 0 0 1 0-2.83 2 2 0 0 1 2.83 0l.06.06a1.65 1.65 0 0 0 1.82.33H9a1.65 1.65 0 0 0 1-1.51V3a2 2 0 0 1 2-2 2 2 0 0 1 2 2v.09a1.65 1.65 0 0 0 1 1.51 1.65 1.65 0 0 0 1.82-.33l.06-.06a2 2 0 0 1 2.83 0 2 2 0 0 1 0 2.83l-.06.06a1.65 1.65 0 0 0-.33 1.82V9a1.65 1.65 0 0 0 1.51 1H21a2 2 0 0 1 2 2 2 2 0 0 1-2 2h-.09a1.65 1.65 0 0 0-1.51 1z"></path>
                    </svg>
                  </a>




                  <ul class="navbar-nav ml-auto">

                    <!--Idiomas Menu-->
                    <li class="nav-item dropdown no-arrow d-sm-none">
                      <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-globe"></i>
                      </a>

                      <!-- Dropdown - Idiomas -->
                      <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchdropdown">
                        <form class="form-inline mr-auto w-100 navbar-search">
                          <div class="input-group">
                            <input type="text" class="form-control bg-light border-0 small" placeholder="search for..." aria-label="search" aria-describedby="basic-addon2"/>
                            <div class="input-group-append">
                              <button class="btn btn-primary" type="button">
                                <i class="fas fa-globe"></i>
                              </button>
                            </div>
                          </div>
                        </form>
                      </div>
                    </li>
                    <!-- Nav Item - Idiomas -->
                    <li class="nav-item dropdown no-arrow mx-1">
                      <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-globe"></i>

                      </a>
                      <!-- Dropdown - Idiomas -->
                      <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
                        <h6 class="dropdown-header">
                          Idiomas
                        </h6>
                        <a class="dropdown-item d-flex align-items-center" href="#">
                          <div class="mr-3">
                            <div>
                              <img src="./custom/images/me.png" width="20" height="20"/>
                            </div>
                          </div>
                          <div>
                            <span class="font-weight-bold">Español (México)</span>
                          </div>
                        </a>

                        <a class="dropdown-item d-flex align-items-center" href="#">
                          <div class="mr-3">
                            <div>
                              <img src="./custom/images/Eua.png" width="20" height="20"/>
                            </div>
                          </div>
                          <div>
                            <span class="font-weight-bold">English (US)</span>
                          </div>
                        </a>
                      </div>
                    </li>



                    <!--Compras Menu-->
                    <li class="nav-item dropdown no-arrow d-sm-none">
                      <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-shopping-cart"></i>
                      </a>

                      <!-- Dropdown - Compras -->
                      <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchdropdown">
                        <form class="form-inline mr-auto w-100 navbar-search">
                          <div class="input-group">
                            <input type="text" class="form-control bg-light border-0 small" placeholder="search for..." aria-label="search" aria-describedby="basic-addon2"/>
                            <div class="input-group-append">
                              <button class="btn btn-primary" type="button">
                                <i class="fas fa-shopping-cart"></i>
                              </button>
                            </div>
                          </div>
                        </form>
                      </div>
                    </li>
                    <!-- Nav Item - Compras -->
                    <xsl:variable name="cart" select="//shell:cart/*"/>
                    <li class="nav-item dropdown no-arrow mx-1">
                      <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-shopping-cart"></i>
                        <xsl:if test="$cart">
                          <!-- Counter - Cart -->
                          <span class="badge badge-danger badge-counter">
                            <xsl:value-of select="count($cart)"/>
                          </span>
                        </xsl:if>
                      </a>
                      <!-- Dropdown - Compras -->
                      <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
                        <xsl:choose>
                          <xsl:when test="$cart">
                            <h6 class="dropdown-header">
                              Mis productos
                            </h6>
                            <xsl:apply-templates mode="shell_messages" select="$cart"/>
                            <a class="dropdown-item text-center small text-gray-500" href="#" onclick="cart.empty()">Vaciar tu carrito de compras</a>
                          </xsl:when>
                          <xsl:otherwise>
                            <a class="dropdown-item text-center small text-gray" href="#">Carrito Vacío</a>
                          </xsl:otherwise>
                        </xsl:choose>
                      </div>
                    </li>

                    <!--Mensajes Menu-->
                    <li class="nav-item dropdown no-arrow d-sm-none">
                      <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-search fa-fw"></i>
                      </a>

                      <!-- Dropdown - Messages -->
                      <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchdropdown">
                        <form class="form-inline mr-auto w-100 navbar-search">
                          <div class="input-group">
                            <input type="text" class="form-control bg-light border-0 small" placeholder="search for..." aria-label="search" aria-describedby="basic-addon2"/>
                            <div class="input-group-append">
                              <button class="btn btn-primary" type="button">
                                <i class="fas fa-search fa-sm"></i>
                              </button>
                            </div>
                          </div>
                        </form>
                      </div>
                    </li>

                    <!-- Nav Item - Alerts -->
                    <li class="nav-item dropdown no-arrow mx-1">
                      <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-bell fa-fw"></i>
                        <xsl:variable name="notifications" select="//shell:messages/notifications/*"/>
                        <xsl:if test="$notifications">
                          <!-- Counter - Alerts -->
                          <span class="badge badge-danger badge-counter">
                            <xsl:value-of select="count($notifications)"/>
                          </span>
                        </xsl:if>
                      </a>
                      <!-- Dropdown - Alerts -->
                      <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
                        <h6 class="dropdown-header">
                          Notificaciones
                        </h6>
                        <a class="dropdown-item d-flex align-items-center" href="#">
                          <div class="mr-3">
                            <div class="icon-circle bg-primary">
                              <i class="fas fa-file-alt text-white"></i>
                            </div>
                          </div>
                          <div>
                            <div class="small text-gray-500">13 de Abril de 2020</div>
                            <span class="font-weight-bold">Un nuevo proveedor se ha unido</span>
                          </div>
                        </a>
                        <a class="dropdown-item text-center small text-gray-500" href="#">Mostrar todas las notificaciones</a>
                      </div>
                    </li>

                    <!-- Nav Item - Messages -->
                    <li class="nav-item dropdown no-arrow mx-1">
                      <xsl:variable name="messages" select="//shell:messages/messages/*"/>
                      <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-envelope fa-fw"></i>
                        <xsl:if test="$messages">
                          <!-- Counter - Messages -->
                          <span class="badge badge-danger badge-counter">
                            <xsl:value-of select="count($messages)"/>
                          </span>
                        </xsl:if>
                      </a>
                      <!-- Dropdown - Messages -->
                      <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="messagesDropdown">
                        <h6 class="dropdown-header">
                          Mensajes
                        </h6>
                        <xsl:apply-templates mode="shell_messages" select="$messages"/>
                        <a class="dropdown-item text-center small text-gray-500" href="#">Leer más mensajes</a>
                      </div>
                    </li>


                    <div class="topbar-divider d-none d-sm-block"></div>

                    <li class="nav-item dropdown no-arrow">
                      <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="mr-2 d-none d-lg-inline text-gray-600 small">
                          <xsl:choose>
                            <xsl:when test="@session:user_id=-1">
                              <xsl:value-of select="@session:user_login"/>
                            </xsl:when>
                            <xsl:when test="@session:user_login">
                              <xsl:value-of select="@session:user_login"/>
                            </xsl:when>
                            <xsl:otherwise>Invitado</xsl:otherwise>
                          </xsl:choose>
                        </span>
                        <xsl:choose>
                          <xsl:when test="@session:user_id=-1">
                            <img src="./custom/images/jacki.jpg" class="avatar img-fluid rounded-circle mr-1" alt="{@session:user_login}"/>
                          </xsl:when>
                          <xsl:otherwise>
                            <span class="fas fa-user-circle rounded-circle mr-3" style="font-size: 1.7em;"></span>
                          </xsl:otherwise>
                        </xsl:choose>
                      </a>
                      <!-- Dropdown - User Information -->
                      <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                        <a class="dropdown-item" href="#" onclick="px.request('Seguridad.Usuario','edit','[username]=SUSER_NAME()')">
                          <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                          Perfil
                        </a>
                        <a class="dropdown-item" href="#">
                          <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                          Ajustes
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#" onclick="xdom.session.logout();">
                          <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                          Salir
                        </a>
                      </div>
                    </li>

                  </ul>



                  <!--<a class="nav-link dropdown-toggle d-none d-sm-inline-block" href="#" data-toggle="dropdown">
                    
                      
                   <xsl:choose>
                      <xsl:when test="@session:user_id=-1">
                        <img src="./custom/images/jacki.jpg" class="avatar img-fluid rounded-circle mr-1" alt="{@session:user_login}"/>
                      </xsl:when>
                      <xsl:otherwise>
                        <span class="fas fa-user-circle rounded-circle mr-3" style="font-size: 1.7em;"></span>
                      </xsl:otherwise>
                    </xsl:choose>
                    
                    <span class="text-dark">
                      <xsl:choose>
                        <xsl:when test="@session:user_id=-1">
                          <xsl:value-of select="@session:user_login"/>
                        </xsl:when>
                        <xsl:when test="@session:user_login">
                          <xsl:value-of select="@session:user_login"/>
                        </xsl:when>
                        <xsl:otherwise>Invitado</xsl:otherwise>
                      </xsl:choose>
                    </span>                
                  </a>-->


                  <!--<div class="dropdown-menu dropdown-menu-right">
                    
                <a class="dropdown-item" href="pages-profile.html">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Perfil
                </a>
                    
                <a class="dropdown-item" href="#">
                  <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                  Ajustes
                </a>
                 
                    -->
                  <!--<a class="dropdown-item disabled" href="pages-profile.html">
                      <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user align-middle mr-1">
                        <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path>
                        <circle cx="12" cy="7" r="4"></circle>
                      </svg> Perfil
                    </a>-->
                  <!--
                    <div class="dropdown-divider"></div>
                    -->
                  <!--<a class="dropdown-item" href="pages-settings.html">Settings &amp; Privacy</a>
                                <a class="dropdown-item" href="#">Help</a>-->
                  <!--
                    <a class="dropdown-item" href="#" onclick="xdom.session.logout();"><i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                    Salir</a>
                  </div>-->


                </li>
              </ul>
            </div>
          </nav>

     
          <main>
               <!--header: ¡que viva tu origen!-->

          <header>
            <section class="textos-header">
              <h1>¡Que viva tu origen!</h1>
              <h2>Compra productos directamente por fabricantes locales</h2>
            </section>
            <div class="wave">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
                <path fill="#ffffff" fill-opacity="1" d="M0,224L60,208C120,192,240,160,360,149.3C480,139,600,149,720,160C840,171,960,181,1080,176C1200,171,1320,149,1380,138.7L1440,128L1440,320L1380,320C1320,320,1200,320,1080,320C960,320,840,320,720,320C600,320,480,320,360,320C240,320,120,320,60,320L0,320Z"></path>
              </svg>
            </div>
          </header>



            <!--***********descubre ay local**********-->
            <section class="deal-of-week set-bg spad" style="background: #ffffff;">
              <div class="container">
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-6 d-none d-md-block text-center" >
                  <div class="section-title">
                    <h2>Descubre aylocal</h2>
                    <p>
                      consulta todos nuestros <br/>
                      productos y servicios en línea
                    </p>
                  </div>
                  <a href="#" class="primary-btn">Comprar</a>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-8">
                  <div class="hero__img">
                    <img src="./custom/images/aylocal_head.jpg"/>
                  </div>
                </div>
              </div>
            </section>

            <!--***********lo mas vendido**********-->
            <div class="site-section" style="padding-top: 80px;   padding-bottom: 80px;">
              <div class="container_mas">
                <div class="row">
                  <div class="title-section text-center mb-5 col-12">
                    <div class="section-title_title">
                      <h2>Lo más vendido</h2>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <xsl:apply-templates mode="dashboard" select="//shell:dashboard/vendidos"/>

                </div>
              </div>
            </div>
            <!--***********novedades**********-->
            <div class="site-section" style="padding-top: 80px;   padding-bottom: 80px;">
              <div class="container_mas">
                <div class="row">
                  <div class="title-section text-center mb-5 col-12">
                    <div class="section-title_title">
                      <h2>Novedades</h2>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <xsl:apply-templates mode="dashboard" select="//shell:dashboard/novedades"/>
                </div>
              </div>
            </div>
            <!--***********favoritos**********-->
            <div class="site-section" style="padding-top: 80px;   padding-bottom: 80px;">
              <div class="container_mas">
                <div class="row">
                  <div class="title-section text-center mb-5 col-12">
                    <div class="section-title_title">
                      <h2>Inspirado para sugerirte</h2>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <xsl:apply-templates mode="dashboard" select="//shell:dashboard/sugerencias"/>
                </div>
              </div>
            </div>


            <!--***********metodos de pago**********-->

            <div class="shop-method-area" style="padding-top: 80px; padding-bottom: 80px;;">
              <div class="container">

                <div class="row d-flex justify-content-between">
                  <div class="col-xl-3 col-lg-3 col-md-6">
                    <div class="single-method mb-40">
                      <img src="./custom/images/bx-credit.svg"  alt=""/>
                      <h6>Paga con tarjeta, efectivo u otro</h6>
                      <p>Con aylocal, tienes meses sin intereses con cualquier tarjeta que elijas.</p>
                    </div>
                  </div>
                  <div class="col-xl-3 col-lg-3 col-md-6">
                    <div class="single-method mb-40">
                      <img src="./custom/images/bx.svg"  alt=""/>
                      <h6>Envio a cualquier parte de México</h6>
                      <p>Con solo estar registrado, tienes acceso a envíos gratis en productos seleccionados.</p>
                    </div>
                  </div>
                  <div class="col-xl-3 col-lg-3 col-md-6">
                    <div class="single-method mb-40">
                      <img src="./custom/images/bx-check.svg"  alt=""/>
                      <h6>Tu seguridad nos interesa</h6>
                      <p>¿No te gusta? ¡Devuélvelo! Nuestros vendedores quieren que siempre estés protegido.</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </main>


          <!--*****************************Footer*****************************-->
          <div class="container-footer">
            <footer class="containter-ft ">
              <div class="cont-footer">
                <div class="cont-ft">
                  <section class="cont-sct">
                    <h4 class="cont-sct-h4">Acerca de nosotros</h4>
                    <ul class="cont-sct-ul">
                      <li class="cont-sct-li">
                        <a href="#" class="cont-sct-a">Vendedores</a>
                      </li>
                      <li class="cont-sct-li">
                        <a href="#" class="cont-sct-a">Categorías</a>
                      </li>
                      <li class="cont-sct-li">
                        <a href="#" class="cont-sct-a">Ayuda</a>
                      </li>
                    </ul>
                  </section>
                  <section class="cont-sct">
                    <h4 class="cont-sct-h4">Descubre</h4>
                    <ul class="cont-sct-ul">
                      <li class="cont-sct-li">
                        <a href="#" class="cont-sct-a">¿Por qué comprar en aylocal?</a>
                      </li>
                      <li class="cont-sct-li">
                        <a href="#" class="cont-sct-a">¿Por qué vender en aylocal?</a>
                      </li>
                    </ul>
                  </section>
                  <section class="cont-sct">
                    <h4 class="cont-sct-h4">Servicio al cliente</h4>
                    <ul class="cont-sct-ul">
                      <li class="cont-sct-li">
                        <a href="#" class="cont-sct-a">Contáctanos</a>
                      </li>
                      <li class="cont-sct-li">
                        <a href="#" class="cont-sct-a">Términos y condiciones</a>
                      </li>
                    </ul>
                  </section>
                  <section class="cont-sct">
                    <h4 class="cont-sct-h4">Siguenos en</h4>
                    <div class="cont-social-s">
                      <ul class="cont-sct-ul-social">
                        <li class="cont-sct-li-social">
                          <a href="#" class="cont-sct-a-social">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" style="display:block;fill:currentColor">
                              <path d="M9 8h-3v4h3v12h5v-12h3.642l.358-4h-4v-1.667c0-.955.192-1.333 1.115-1.333h2.885v-5h-3.808c-3.596 0-5.192 1.583-5.192 4.615v3.385z"/>
                            </svg>
                          </a>
                        </li>
                        <li class="cont-sct-li-social">
                          <a href="#" class="cont-sct-a-social">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" style="display:block;fill:currentColor">
                              <path d="M12 2.163c3.204 0 3.584.012 4.85.07 3.252.148 4.771 1.691 4.919 4.919.058 1.265.069 1.645.069 4.849 0 3.205-.012 3.584-.069 4.849-.149 3.225-1.664 4.771-4.919 4.919-1.266.058-1.644.07-4.85.07-3.204 0-3.584-.012-4.849-.07-3.26-.149-4.771-1.699-4.919-4.92-.058-1.265-.07-1.644-.07-4.849 0-3.204.013-3.583.07-4.849.149-3.227 1.664-4.771 4.919-4.919 1.266-.057 1.645-.069 4.849-.069zm0-2.163c-3.259 0-3.667.014-4.947.072-4.358.2-6.78 2.618-6.98 6.98-.059 1.281-.073 1.689-.073 4.948 0 3.259.014 3.668.072 4.948.2 4.358 2.618 6.78 6.98 6.98 1.281.058 1.689.072 4.948.072 3.259 0 3.668-.014 4.948-.072 4.354-.2 6.782-2.618 6.979-6.98.059-1.28.073-1.689.073-4.948 0-3.259-.014-3.667-.072-4.947-.196-4.354-2.617-6.78-6.979-6.98-1.281-.059-1.69-.073-4.949-.073zm0 5.838c-3.403 0-6.162 2.759-6.162 6.162s2.759 6.163 6.162 6.163 6.162-2.759 6.162-6.163c0-3.403-2.759-6.162-6.162-6.162zm0 10.162c-2.209 0-4-1.79-4-4 0-2.209 1.791-4 4-4s4 1.791 4 4c0 2.21-1.791 4-4 4zm6.406-11.845c-.796 0-1.441.645-1.441 1.44s.645 1.44 1.441 1.44c.795 0 1.439-.645 1.439-1.44s-.644-1.44-1.439-1.44z"/>
                            </svg>
                          </a>
                        </li>
                        <li class="cont-sct-li-social">
                          <a href="#" class="cont-sct-a-social">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" style="display:block;fill:currentColor">
                              <path d="M.057 24l1.687-6.163c-1.041-1.804-1.588-3.849-1.587-5.946.003-6.556 5.338-11.891 11.893-11.891 3.181.001 6.167 1.24 8.413 3.488 2.245 2.248 3.481 5.236 3.48 8.414-.003 6.557-5.338 11.892-11.893 11.892-1.99-.001-3.951-.5-5.688-1.448l-6.305 1.654zm6.597-3.807c1.676.995 3.276 1.591 5.392 1.592 5.448 0 9.886-4.434 9.889-9.885.002-5.462-4.415-9.89-9.881-9.892-5.452 0-9.887 4.434-9.889 9.884-.001 2.225.651 3.891 1.746 5.634l-.999 3.648 3.742-.981zm11.387-5.464c-.074-.124-.272-.198-.57-.347-.297-.149-1.758-.868-2.031-.967-.272-.099-.47-.149-.669.149-.198.297-.768.967-.941 1.165-.173.198-.347.223-.644.074-.297-.149-1.255-.462-2.39-1.475-.883-.788-1.48-1.761-1.653-2.059-.173-.297-.018-.458.13-.606.134-.133.297-.347.446-.521.151-.172.2-.296.3-.495.099-.198.05-.372-.025-.521-.075-.148-.669-1.611-.916-2.206-.242-.579-.487-.501-.669-.51l-.57-.01c-.198 0-.52.074-.792.372s-1.04 1.016-1.04 2.479 1.065 2.876 1.213 3.074c.149.198 2.095 3.2 5.076 4.487.709.306 1.263.489 1.694.626.712.226 1.36.194 1.872.118.571-.085 1.758-.719 2.006-1.413.248-.695.248-1.29.173-1.414z"/>
                            </svg>
                          </a>
                        </li>
                      </ul>
                    </div>
                  </section>

                </div>

                <div class="container-footer-text">
                  <div class="ct-text">
                    <div class="ct-text-a">
                      <div class="ct-text-b">
                        <div class="ct-panel">
                          <span class="ct-span">
                            <a data-no-client-routing="true" href="/account-settings/language" class="ct-a">
                              <span class="ct-icon">
                                <svg aria-hidden="true" role="presentation" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg" style="display: block; height: 16px; width: 16px; fill: currentcolor;">
                                  <path d="m8.002.25a7.77 7.77 0 0 1 7.748 7.776 7.75 7.75 0 0 1 -7.521 7.72l-.246.004a7.75 7.75 0 0 1 -7.73-7.513l-.003-.245a7.75 7.75 0 0 1 7.752-7.742zm1.949 8.5h-3.903c.155 2.897 1.176 5.343 1.886 5.493l.068.007c.68-.002 1.72-2.365 1.932-5.23zm4.255 0h-2.752c-.091 1.96-.53 3.783-1.188 5.076a6.257 6.257 0 0 0 3.905-4.829zm-9.661 0h-2.75a6.257 6.257 0 0 0 3.934 5.075c-.615-1.208-1.036-2.875-1.162-4.686l-.022-.39zm1.188-6.576-.115.046a6.257 6.257 0 0 0 -3.823 5.03h2.75c.085-1.83.471-3.54 1.059-4.81zm2.262-.424c-.702.002-1.784 2.512-1.947 5.5h3.904c-.156-2.903-1.178-5.343-1.892-5.494l-.065-.007zm2.28.432.023.05c.643 1.288 1.069 3.084 1.157 5.018h2.748a6.275 6.275 0 0 0 -3.929-5.068z"></path>
                                </svg>
                              </span>
                              <span class="ct-text">
                                Español (MX)
                              </span>
                            </a>
                          </span>
                          <span class="ct-span">
                            <a data-no-client-routing="true" href="/account-settings/currency" class="ct-a">
                              <span class="ct-icon">$</span>
                              <span class="ct-text">
                                MXN
                              </span>
                            </a>
                          </span>
                        </div>
                      </div>
                      <!--<div class="container-social">
                        <ul class="ct-social-ul">
                          <li class="ct-social-li">
                            <a rel="noopener noreferrer" target="_blank" href="https://www.facebook.com/AirbnbMexico" class="ct-social-icon">
                              <svg viewBox="0 0 32 32" role="img" aria-label="Acceder a Facebook" focusable="false" style="height: 18px; width: 18px; display: block; fill: currentcolor;">
                                <path d="m8 14.41v-4.17c0-.42.35-.81.77-.81h2.52v-2.08c0-4.84 2.48-7.31 7.42-7.35 1.65 0 3.22.21 4.69.64.46.14.63.42.6.88l-.56 4.06c-.04.18-.14.35-.32.53-.21.11-.42.18-.63.14-.88-.25-1.78-.35-2.8-.35-1.4 0-1.61.28-1.61 1.73v1.8h4.52c.42 0 .81.42.81.88l-.35 4.17c0 .42-.35.71-.77.71h-4.21v16c0 .42-.35.81-.77.81h-5.21c-.42 0-.8-.39-.8-.81v-16h-2.52a.78.78 0 0 1 -.78-.78" fill-rule="evenodd"></path>
                              </svg>
                            </a>
                          </li>
                          <li class="ct-social-li">
                            <a rel="noopener noreferrer" target="_blank" href="https://twitter.com/airbnb_mx" class="ct-social-icon">
                              <svg viewBox="0 0 32 32" role="img" aria-label="Acceder a Twitter" focusable="false" style="height: 18px; width: 18px; display: block; fill: currentcolor;">
                                <path d="m31 6.36c-1.16.49-2.32.82-3.55.95 1.29-.76 2.22-1.87 2.72-3.38a13.05 13.05 0 0 1 -3.91 1.51c-1.23-1.28-2.75-1.94-4.51-1.94-3.41 0-6.17 2.73-6.17 6.12 0 .49.07.95.17 1.38-4.94-.23-9.51-2.6-12.66-6.38-.56.95-.86 1.97-.86 3.09 0 2.07 1.03 3.91 2.75 5.06-1-.03-1.92-.3-2.82-.76v.07c0 2.89 2.12 5.42 4.94 5.98-.63.17-1.16.23-1.62.23-.3 0-.7-.03-1.13-.13a6.07 6.07 0 0 0 5.74 4.24c-2.22 1.74-4.78 2.63-7.66 2.63-.56 0-1.06-.03-1.43-.1 2.85 1.84 6 2.76 9.41 2.76 7.29 0 12.83-4.01 15.51-9.3 1.36-2.66 2.02-5.36 2.02-8.09v-.46c-.03-.17-.03-.3-.03-.33a12.66 12.66 0 0 0 3.09-3.16" fill-rule="evenodd"></path>
                              </svg>
                            </a>
                          </li>
                          <li class="ct-social-li">
                            <a rel="noopener noreferrer" target="_blank" href="https://instagram.com/airbnb" class="ct-social-icon">
                              <svg viewBox="0 0 24 24" role="img" aria-label="Acceder a Instagram" focusable="false" style="height: 18px; width: 18px; display: block; fill: currentcolor;">
                                <path d="m23.09.91c-.61-.61-1.33-.91-2.17-.91h-17.84c-.85 0-1.57.3-2.17.91s-.91 1.33-.91 2.17v17.84c0 .85.3 1.57.91 2.17s1.33.91 2.17.91h17.84c.85 0 1.57-.3 2.17-.91s.91-1.33.91-2.17v-17.84c0-.85-.3-1.57-.91-2.17zm-14.48 7.74c.94-.91 2.08-1.37 3.4-1.37 1.33 0 2.47.46 3.41 1.37s1.41 2.01 1.41 3.3-.47 2.39-1.41 3.3-2.08 1.37-3.41 1.37c-1.32 0-2.46-.46-3.4-1.37s-1.41-2.01-1.41-3.3.47-2.39 1.41-3.3zm12.66 11.63c0 .27-.09.5-.28.68a.92.92 0 0 1 -.67.28h-16.7a.93.93 0 0 1 -.68-.28.92.92 0 0 1 -.27-.68v-10.13h2.2a6.74 6.74 0 0 0 -.31 2.05c0 2 .73 3.71 2.19 5.12s3.21 2.12 5.27 2.12a7.5 7.5 0 0 0 3.75-.97 7.29 7.29 0 0 0 2.72-2.63 6.93 6.93 0 0 0 1-3.63c0-.71-.11-1.39-.31-2.05h2.11v10.12zm0-13.95c0 .3-.11.56-.31.77a1.05 1.05 0 0 1 -.77.31h-2.72c-.3 0-.56-.11-.77-.31a1.05 1.05 0 0 1 -.31-.77v-2.58c0-.29.11-.54.31-.76s.47-.32.77-.32h2.72c.3 0 .56.11.77.32s.31.47.31.76z" fill-rule="evenodd"></path>
                              </svg>
                            </a>
                          </li>
                        </ul>
                      </div>-->
                    </div>
                    <div class="container-register">
                      <div class="ct-rg">
                        <div class="ct-rg-content">
                          <div class="ct-rg-text" dir="ltr">© 2020 aylocal. Todos los derechos reservados</div>
                          <div class="ct-rg-txt">
                            <span class="ct-rg-panel">
                              <span class="ct-rg-iconn" aria-hidden="true">·</span>
                            </span>
                            <a href="#" class="ct-rg-sn">Privacidad</a>
                            <span class="ct-rg-icon" aria-hidden="true">·</span>
                            <a href="#" class="ct-rg-sn">Términos</a>
                            <span class="ct-rg-icon" aria-hidden="true">·</span>
                            <a href="#" class="ct-rg-sn">Mapa del sitio</a>
                            <span class="ct-rg-icon" aria-hidden="true">·</span>
                            <a href="#" class="ct-rg-sn">Información de la compañía</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </footer>
          </div>

          <!--****************************************************************-->



        </div>
      </div>




      <div class="settings">
        <div class="settings-toggle toggle-settings" onclick="toggle_settings()">
          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-settings align-middle">
            <circle cx="12" cy="12" r="3"></circle>
            <path d="M19.4 15a1.65 1.65 0 0 0 .33 1.82l.06.06a2 2 0 0 1 0 2.83 2 2 0 0 1-2.83 0l-.06-.06a1.65 1.65 0 0 0-1.82-.33 1.65 1.65 0 0 0-1 1.51V21a2 2 0 0 1-2 2 2 2 0 0 1-2-2v-.09A1.65 1.65 0 0 0 9 19.4a1.65 1.65 0 0 0-1.82.33l-.06.06a2 2 0 0 1-2.83 0 2 2 0 0 1 0-2.83l.06-.06a1.65 1.65 0 0 0 .33-1.82 1.65 1.65 0 0 0-1.51-1H3a2 2 0 0 1-2-2 2 2 0 0 1 2-2h.09A1.65 1.65 0 0 0 4.6 9a1.65 1.65 0 0 0-.33-1.82l-.06-.06a2 2 0 0 1 0-2.83 2 2 0 0 1 2.83 0l.06.06a1.65 1.65 0 0 0 1.82.33H9a1.65 1.65 0 0 0 1-1.51V3a2 2 0 0 1 2-2 2 2 0 0 1 2 2v.09a1.65 1.65 0 0 0 1 1.51 1.65 1.65 0 0 0 1.82-.33l.06-.06a2 2 0 0 1 2.83 0 2 2 0 0 1 0 2.83l-.06.06a1.65 1.65 0 0 0-.33 1.82V9a1.65 1.65 0 0 0 1.51 1H21a2 2 0 0 1 2 2 2 2 0 0 1-2 2h-.09a1.65 1.65 0 0 0-1.51 1z"></path>
          </svg>
        </div>

        <div class="settings-panel">
          <div class="settings-content js-simplebar" data-simplebar="init">
            <div class="simplebar-wrapper" style="margin: 0px;">
              <div class="simplebar-height-auto-observer-wrapper">
                <div class="simplebar-height-auto-observer"></div>
              </div>
              <div class="simplebar-mask">
                <div class="simplebar-offset" style="right: -17px; bottom: 0px;">
                  <div class="simplebar-content-wrapper" style="height: 100%; overflow: hidden scroll;">
                    <div class="simplebar-content" style="padding: 0px;">
                      <div class="settings-title">
                        <button type="button" class="close float-right" aria-label="Close" onclick="toggle_settings()">
                          <span aria-hidden="true">×</span>
                        </button>
                        <h4>Herramientas</h4>
                      </div>

                      <div class="settings-section">
                        <small class="d-block text-uppercase font-weight-bold text-muted mb-2">Sesión</small>
                        <div class="list-group">
                          <button type="button" class="list-group-item list-group-item-action" onclick="xdom.dom.refresh({{forced:true}});">Actualizar</button>
                          <button type="button" class="list-group-item list-group-item-action" onclick="xdom.session.saveSession();">Guardar sesión</button>
                          <button type="button" class="list-group-item list-group-item-action" onclick="xdom.session.loadSession()">Restaurar sesión</button>
                        </div>
                      </div>

                      <div class="settings-section">
                        <small class="d-block text-uppercase font-weight-bold text-muted mb-2">Edición</small>
                        <div class="list-group">
                          <button type="button" class="list-group-item list-group-item-action" onclick="xdom.data.undo();">Deshacer</button>
                          <button type="button" class="list-group-item list-group-item-action" onclick="xdom.data.redo()">Rehacer</button>
                          <button type="button" class="list-group-item list-group-item-action" onclick="xdom.dom.print()">Imprimir</button>
                        </div>
                      </div>

                      <div class="settings-section">
                        <small class="d-block text-uppercase font-weight-bold text-muted mb-2">Desarrollador</small>
                        <div class="list-group">
                          <button type="button" class="list-group-item list-group-item-action" onclick="xdom.library.reload(); /*window.location.reload(true);*/">Actualizar librerías</button>
                          <button type="button" class="list-group-item list-group-item-action" onclick="xdom.data.toClipboard();">Copiar fuente</button>
                          <button type="button" class="list-group-item list-group-item-action" onclick="xdom.devTools.debug();">Debug</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="simplebar-placeholder" style="width: 239px; height: 854px;"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </xsl:template>

  <xsl:template mode="sitemap" match="text()"/>

  <xsl:template mode="sitemap" match="sitemap:menu">
    <li class="sidebar-item">
      <a href="#{generate-id()}" data-toggle="collapse" class="text-white sidebar-link collapsed" style="background: #00B0AD; font-size:16px">

        <!--<img class="rounded-circle mr-3" src="./custom/images/EmblemaAyLocal_A.png" alt="{@session:user_login}" width="40" height="20"/>-->
        <!--<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-layout align-middle">
          <rect x="3" y="3" width="18" height="18" rx="2" ry="2"></rect>
          <line x1="3" y1="9" x2="21" y2="9"></line>
          <line x1="9" y1="21" x2="9" y2="9"></line>
        </svg>-->
        <span class="align-middle">
          <xsl:value-of select="@title"/>
        </span>
      </a>
      <ul id="{generate-id()}" class="sidebar-dropdown list-unstyled collapse">
        <xsl:apply-templates mode="sitemap"/>
      </ul>
    </li>
  </xsl:template>

  <xsl:template mode="sitemap" match="sitemap:item|sitemap:catalog">
    <li class="sidebar-item text-white ">
      <strong>
        <a href="#{generate-id((ancestor::sitemap:menu)[1])}" class="text-dark sidebar-link" style=" font-size:16px; background: white;">
          <xsl:attribute name="onclick">
            <xsl:apply-templates mode="sitemap.script" select="."/><![CDATA[;toggle_sidebar();]]>
          </xsl:attribute>
          <xsl:value-of select="@title"/>

        </a>
      </strong>
    </li>
  </xsl:template>

  <xsl:template mode="sitemap" match="sitemap:sitemap">
    <li class="sidebar-header">
      <xsl:value-of select="@title"/>
    </li>
    <xsl:apply-templates mode="sitemap"/>
  </xsl:template>

  <xsl:template mode="sitemap.script" match="sitemap:catalog">
    <xsl:text/>px.request("<xsl:value-of select="@catalogName"/>")<xsl:text/>
  </xsl:template>

  <xsl:template mode="dashboard" match="text()"/>

  <xsl:template mode="dashboard" match="shell:dashboard//producto">
    <div class="col-lg-4">
      <div class="testimonial-item mx-auto mb-5 mb-lg-0">
        <img class="rounded-circle mr-3" src="./custom/images/{img/text()}" width="150" height="150"/>
        <h5>
          <xsl:value-of select="Nombre"/>
        </h5>
        <p class="font-weight-light mb-0">
          <xsl:value-of select="Descripcion"/>
        </p>
      </div>
    </div>
  </xsl:template>

  <xsl:template mode="shell_messages_status" match="*"/>

  <xsl:template mode="shell_messages_status" match="*[@type='new']">
    <xsl:text>bg-success</xsl:text>
  </xsl:template>

  <xsl:template mode="shell_messages_status" match="*[@type='old']">
    <xsl:text>bg-warning</xsl:text>
  </xsl:template>

  <xsl:template mode="shell_messages" match="shell:cart/*">
    <a class="dropdown-item d-flex align-items-center" href="#">
      <div class="mr-3">
        <div>
          <img src="{img/text()}" width="40" height="40"/>
        </div>
      </div>
      <div>
        <span class="font-weight-bold">
          <xsl:value-of select="Nombre/text()"/>
          <xsl:text> $</xsl:text>
          <xsl:value-of select="Precio/text()"/>
        </span>
      </div>
    </a>
  </xsl:template>

  <xsl:template mode="shell_messages" match="shell:messages//*">
    <a class="dropdown-item d-flex align-items-center" href="#">
      <div class="dropdown-list-image mr-3">
        <img class="rounded-circle" width="50" height="50" src="./custom/images/{@pic}.jpg"  alt=""/>
        <div>
          <xsl:attribute name="class">
            <xsl:text>status-indicator </xsl:text>
            <xsl:apply-templates mode="shell_messages_status" select="."/>
          </xsl:attribute>
        </div>
      </div>
      <div>
        <xsl:if test="@type='new'">
          <xsl:attribute name="class">font-weight-bold</xsl:attribute>
        </xsl:if>
        <div class="text-truncate">
          <xsl:value-of select="text()"/>
        </div>
        <div class="small text-gray-500">
          <xsl:value-of select="@user"/> · 58m
        </div>
      </div>
    </a>
  </xsl:template>

</xsl:stylesheet>
