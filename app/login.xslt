﻿<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns="http://www.w3.org/1999/xhtml"
  exclude-result-prefixes="xsl"
>
  <xsl:import href="messages.xslt"/>
  <xsl:include href="custom/login.xslt"/>

  <xsl:template match="/*" priority="-10">
    <h1>Upload your transformation file to custom/login.xslt</h1>
  </xsl:template>
</xsl:stylesheet>