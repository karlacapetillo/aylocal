﻿<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  exclude-result-prefixes="#default xsl"
  xmlns="http://www.w3.org/1999/xhtml"
>
  <xsl:output method="xml"
     omit-xml-declaration="yes"
     indent="yes"/>

  <xsl:template match="*">
    <style>
      <![CDATA[
      .group-tab-nav {
        background-color: red;
      }]]>
    </style>
  </xsl:template>

</xsl:stylesheet>
