﻿<?xml-stylesheet type="text/css" href="custom/css/gridview.css"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:debug="http://panax.io/debug"
  xmlns:msxsl="urn:schemas-microsoft-com:xslt"
  xmlns:x="http://panax.io/xdom"
  xmlns:session="http://panax.io/session"
  xmlns:filters="http://panax.io/filters"
  xmlns:custom="http://panax.io/custom"
  xmlns:datagrid="http://panax.io/widgets/datagrid"
  xmlns:xhr="http://panax.io/xdom/xhr"
  xmlns="http://www.w3.org/1999/xhtml"
  exclude-result-prefixes="debug msxsl x session filters custom datagrid xhr"
  xmlns:px="http://panax.io"
>
  <xsl:include href="xdom/resources/datagrid.xslt"/>

  <xsl:key name="hidden" match="px:dataRow[not(EmpresasVinculadas/*/px:data/px:dataRow[@x:checked='true' and contains(IdEmpresa/@x:text,'FLIMSA')])]/Documentacion/*/px:data/px:dataRow/*[self::PoliticasSeguridadPatrimonial|self::ReferenciasComerciales|self::DatosRepresentanteLegal|self::NumeroCertifcacionSeguridad|self::ConvenioCumplimientoSeguridadFisica|self::CartaCumplimientoSeguridadFisica]" use="generate-id()" />

  <xsl:key name="controls.picture" match="Foto" use="@fieldId"/>
  
  <xsl:decimal-format
      name="money"
      grouping-separator=","
      decimal-separator="."/>

  <xsl:key name="filterBy" match="*" use="generate-id()"/>
  <xsl:key name="groupBy" match="/" use="."/>

  <xsl:key name="proveedores_x_status" match="px:dataRow" use="Estatus/@x:value"/>
  <xsl:key name="proveedores_x_status" match="px:dataRow/Proveedores/@x:value" use="../../Status/@x:value"/>

  <xsl:template mode="identity" match="px:dataRow">
    <xsl:text/>NULL<xsl:text/>
  </xsl:template>

  <xsl:template mode="identity" match="px:dataRow/*">
    <xsl:text/>\'\'<xsl:value-of select="@x:value"/>\'\'<xsl:text/>
  </xsl:template>

  <xsl:template mode="identity" match="px:dataRow">
    <xsl:variable name="primary_field" select="*[key('primary_field',@fieldId)]"/>
    <xsl:apply-templates mode="identity" select="$primary_field"/>
  </xsl:template>

  <xsl:template mode="identity" match="px:dataRow[@identity!='']">
    <xsl:value-of select="@identity"/>
  </xsl:template>

  <xsl:template match="*[key('field',generate-id())]" mode="datagrid.header.headertext">
    <xsl:value-of select="@headerText"/>
  </xsl:template>

  <xsl:template match="px:layout//px:field" mode="datagrid.header">
    <xsl:apply-templates mode="datagrid.header" select="key('field',@fieldId)"/>
  </xsl:template>

  <xsl:template match="px:layout//px:field" mode="datagrid.body">
    <xsl:param name="data_row"/>
    <xsl:variable name="field" select="."/>
    <xsl:apply-templates mode="datagrid.body" select="$data_row/*[@fieldId=$field/@fieldId]">
      <xsl:with-param name="field" select="key('field',@fieldId)"/>
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template match="px:layout//px:field" mode="datagrid.footer">
    <xsl:apply-templates mode="datagrid.footer" select="key('field',@fieldId)"/>
  </xsl:template>

  <xsl:template mode="datagrid.data.buttons.edit.action" match="*[not(key('junctionTable',../@fieldId))]/px:data/px:dataRow">
    <xsl:variable name="table" select="ancestor::*[key('data_table',generate-id())][1]"/>
    <xsl:attribute name="onclick">
      <xsl:text/>px.modifyInlineRecord({ref:'<xsl:value-of select="@x:id"/>', name:'[<xsl:value-of select="$table/@Schema"/>].[<xsl:value-of select="$table/@Name"/>]', mode:'add'})<xsl:text/>
    </xsl:attribute>
  </xsl:template>

  <xsl:template mode="datagrid.data.buttons.edit.action" match="*[not(key('junctionTable',../@fieldId))]/px:data/px:dataRow[not(@identity!='')][../../px:fields/*/@isPrimaryKey]">
    <xsl:variable name="table" select="ancestor::*[key('data_table',generate-id())][1]"/>
    <xsl:attribute name="onclick">
      <xsl:text/>/*<xsl:value-of select="count(ancestor::*[key('data_table',generate-id())])"/>*/px.request('[<xsl:value-of select="$table/@Schema"/>].[<xsl:value-of select="$table/@Name"/>]', 'edit', '[<xsl:value-of select="$table/@primaryKey"/>]=<xsl:apply-templates mode="identity" select="."/>')<xsl:text/>
    </xsl:attribute>
  </xsl:template>

  <xsl:template mode="datagrid.data.buttons.edit.action" match="*[not(key('junctionTable',../@fieldId))]/px:data/px:dataRow[@identity!='']">
    <xsl:variable name="table" select="ancestor::*[key('data_table',generate-id())][1]"/>
    <xsl:attribute name="onclick">
      <xsl:text/>/*<xsl:value-of select="count(ancestor::*[key('data_table',generate-id())])"/>*/px.request('[<xsl:value-of select="$table/@Schema"/>].[<xsl:value-of select="$table/@Name"/>]', 'edit', '[<xsl:value-of select="$table/@identityKey"/>]=<xsl:apply-templates mode="identity" select="."/>')<xsl:text/>
    </xsl:attribute>
  </xsl:template>
  
  <xsl:template mode="datagrid.data.buttons.delete.action" match="*[not(key('junctionTable',../@fieldId))]/px:data/px:dataRow">
    <xsl:variable name="table" select="ancestor::*[key('data_table',generate-id())][1]"/>
    <xsl:attribute name="onclick">
      <xsl:text/>xdom.data.remove('<xsl:value-of select="@x:id"/>'); xdom.dom.refresh({forced:true});<xsl:text/>
    </xsl:attribute>
  </xsl:template>

  <!--<xsl:template mode="control" match="*[key('controls.picture',@fieldId)]" priority="10">
    Foto
  </xsl:template>-->

  <xsl:template mode="datagrid.body" match="Fotos">
    <td>
      <ul class="checklist">
        <xsl:for-each select=".//px:dataRow/Foto[not(key('hidden', generate-id()))]">
          <xsl:apply-templates mode="control" select="."/>
        </xsl:for-each>
      </ul>
    </td>
  </xsl:template>
  <!--<xsl:template match="*[key('data_table',generate-id())]" mode="datagrid.header"/>-->

  <xsl:template mode="datagrid.body" match="*[@Schema='Dashboard' or @Name='Dashboard']//*"/>
  <xsl:template mode="datagrid.body" match="*[@Schema='Dashboard' or @Name='Dashboard']//*[1]">
    <xsl:param name="columns"/>
    <tr>
      <td colspan="15">
        <style>
          <![CDATA[
        .anchor, .anchor40, .anchor60, .anchor80-line, .anchor100-line, .anchor-text {
            position: absolute;  
            background-color: red;
            box-sizing: border-box;
            padding: 5px 0;
            font-size: 24px;
            line-height: 1;
            text-align: center;
        }

        .anchor40 {
            width: 80px;
            height: 80px;
            padding-top: 25px;
            text-align: center;
        }

        .anchor60 {
            width: 60px;
            height: 60px;
        }

        .anchor80-line {
            width: 80px;
        }

        .anchor100-line {
            width: 100px;
        }

        .anchor-text {
            font-size: 18px;
        }

        .stage {
            position: relative;
            margin-bottom: 1em;
        }

        .text-fill {
            height: 100%;
            overflow: hidden;
            line-height: 1.1;
            word-break: break-all;
        }

        .stage, .col, .leader-line {
            font-family: Arial, sans-serif;
        }

        #ex-020-frame {
            position: absolute;
            left: 620px;
            top: 80px;
            width: 160px;
            height: 310px;
            border: 1px solid #ccc;
            overflow-y: scroll;
        }

        #ex-020-frame > div {
            width: 100%;
            height: 555px;
            background-color: #f0f0f0;
        }

        #ex-020-frame p {
            margin: 0;
        }

        body {
            background-color: #fafafa;
        }

        .container {
            margin: 150px auto;
            max-width: 968px;
        }]]>
        </style>
        <div>
          <center>
            <h1>Proceso Proveedores</h1>
          </center>
          <div id="ex-020" class="stage" style="width: 1200px; height: 400px;">
            <div id="ex-020-d" class="anchor40" style="left: 0px; top: 10px; cursor:hand;" onclick="px.request('[Proveedores].[Proveedor]', 'view', '[Estatus]=\'\'Compras\'\'')">
              <xsl:value-of select="sum(key('proveedores_x_status', 'Compras'))"/>
            </div>
            <!--<div id="ex-020-a" class="anchor40" style="left: 0px; top: 210px; background-color: orange;">10</div>
            <div id="ex-020-b" class="anchor40" style="left: 0px; top: 310px; background-color: red;">2</div>-->
            <div id="ex-020-c" class="anchor40" style="left: 250px; top: 10px; cursor:hand;" onclick="px.request({{name:'[Proveedores].[Proveedor]', mode:'view', filters:'[Estatus]=\'\'Financiero\'\'', pageSize:30}})">
              <xsl:value-of select="sum(key('proveedores_x_status', 'Financiero'))"/>
            </div>
            <div id="ex-020-h" class="anchor40" style="left: 500px; top: 10px; cursor:hand;" onclick="px.request({{name:'[Proveedores].[Proveedor]', mode:'view', filters:'[Estatus]=\'\'CXP\'\'', pageSize:30}})">
              <xsl:value-of select="sum(key('proveedores_x_status', 'CXP'))"/>
            </div>
            <div id="ex-020-g" class="anchor40" style="left: 750px; top: 10px; cursor:hand;" onclick="px.request({{name:'[Proveedores].[Proveedor]', mode:'view', filters:'[Estatus]=\'\'Tesoreria\'\'', pageSize:30}})">
              <xsl:value-of select="sum(key('proveedores_x_status', 'Tesoreria'))"/>
            </div>
            <!--<div id="ex-020-i" class="anchor40" style="left: 1000px; top: 10px;">250</div>-->
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" id="ex-020-i" style="left: 1000px; top: 4px; position: absolute; cursor:hand;" onclick="px.request({{name:'[Proveedores].[Proveedor]', mode:'view', filters:'[Estatus]=\'\'Autorizado\'\'', pageSize:30}})" height="94" width="94">
              <circle cx="50%" cy="50%" r="45" stroke="black" stroke-width="3" fill="green" />
              <text x="50%" y="50%" text-anchor="middle" fill="#fff" dy=".3em" style="font-size:14pt;">
                <xsl:value-of select="sum(key('proveedores_x_status', 'Autorizado'))"/>
              </text>
            </svg>
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 210 165" class="indefinidos" style="left: 500px; top: 210px; position: absolute; width:150px; cursor:hand;" onclick="px.request({{name:'[Proveedores].[Proveedor]', mode:'view', filters:'ISNULL([Estatus],\'\',\'\')=\'\'\'\'', pageSize:30}})">
              <title>Proveedores indefinidos</title>
              <defs>
                <path d="M100,37c34.8,0,63,28.2,63,63s-28.2,63-63,63s-63-28.2-63-63S65.2,37,100,37z" id="indefinidos" transform="rotate(-85 100 100)" />
              </defs>
              <circle cx="100" cy="100" r="50" fill="silver" id="background" />
              <text>
                <textPath xlink:href="#indefinidos">Indefinidos</textPath>
              </text>
              <text x="47%" y="65%" text-anchor="middle" fill="#fff">
                <xsl:value-of select="sum(key('proveedores_x_status', ''))"/>
              </text>
            </svg>
            <style>
              .indefinidos text {
              font-family: Santana-Black, sans-serif;
              text-transform: uppercase;
              font-size: 28px;
              }
            </style>
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 210 165" class="rechazados" style="left: 750px; top: 210px; position: absolute; width:150px; cursor:hand;" onclick="px.request({{name:'[Proveedores].[Proveedor]', mode:'view', filters:'[Estatus]=\'\'Rechazado\'\'', pageSize:30}})">
              <title>Proveedores rechazados</title>
              <defs>
                <path d="M100,37c34.8,0,63,28.2,63,63s-28.2,63-63,63s-63-28.2-63-63S65.2,37,100,37z" id="rechazados" transform="rotate(-85 100 100)" />
              </defs>
              <circle cx="100" cy="100" r="50" fill="silver" id="background" />
              <text>
                <textPath xlink:href="#rechazados">Rechazados</textPath>
              </text>
              <text x="47%" y="65%" text-anchor="middle" fill="#fff">
                <xsl:value-of select="sum(key('proveedores_x_status', 'Rechazado'))"/>
              </text>
            </svg>
            <style>
              .rechazados text {
              font-family: Santana-Black, sans-serif;
              text-transform: uppercase;
              font-size: 28px;
              }
            </style>
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 210 165" class="baja" style="left: 1000px; top: 210px; position: absolute; width:150px; cursor:hand;" onclick="px.request({{name:'[Proveedores].[Proveedor]', mode:'view', filters:'[Estatus]=\'\'Baja\'\'', pageSize:30}})">
              <title>Proveedores dados de baja</title>
              <defs>
                <path d="M100,37c34.8,0,63,28.2,63,63s-28.2,63-63,63s-63-28.2-63-63S65.2,37,100,37z" id="baja" transform="rotate(-45 100 100)" />
              </defs>
              <circle cx="100" cy="100" r="50" fill="red" id="background" />
              <text id="baja_label">
                <textPath xlink:href="#baja">Baja</textPath>
              </text>
              <text x="47%" y="65%" text-anchor="middle" fill="#fff">
                <xsl:value-of select="sum(key('proveedores_x_status', 'Baja'))"/>
              </text>
            </svg>
            <style>
              .baja text {
              font-family: Santana-Black, sans-serif;
              text-transform: uppercase;
              font-size: 28px;
              }
              #baja_label { letter-spacing: 6px; }
            </style>
            <!--<svg id="ex-020-i" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd; left: 1000px; top: 10px; position: absolute;" viewBox="0 0 0 0">
                <g id="UrTavla">
                  <circle style="fill:url(#toning);stroke:#010101;stroke-width:1.6871;stroke-miterlimit:10;" cx="250" cy="250" r="50">
                  </circle>
                  <text x="50%" y="50%" text-anchor="middle" stroke="#51c5cf" stroke-width="2px" dy=".3em">Look, I’m centered!Look, I’m centered!</text>
                </g>
              </svg>-->
            <!--<div class="anchor40" style="left: 1000px; top: 10px;">250</div>-->
            <!--<div id="ex-020-a" class="anchor40" style="left: 10px;  top: 150px;">4</div>
            <div id="ex-020-b" class="anchor40" style="left: 300px; top: 150px;">3</div>
            <div id="ex-020-e" class="anchor40" style="left: 510px; top: 150px;">8</div>
            <div id="ex-020-f" class="anchor40" style="left: 720px; top: 150px;">7</div>-->
          </div>

          <script>
            <![CDATA[
            'use strict';
            var lines = document.querySelectorAll(".leader-line")
            for (var l=0; l<lines.length; ++l) {
                lines[l].remove()
            }
            line = new LeaderLine(
                document.getElementById('ex-020-d'),
                document.getElementById('ex-020-c'),
                {
                    color: '#ef1e28',
                    startPlug: 'square',
                    endPlug: 'arrow2',
                    startPlugColor: '#43bc5b',
                    endPlugColor: '#1085e0',
                    startPlugSize: 1.5,
                    endPlugSize: 1.5,
                    dash: true,
                    middleLabel: LeaderLine.pathLabel('Compras')
                }
            );

            line = new LeaderLine(
                document.getElementById('ex-020-c'),
                document.getElementById('ex-020-h'),
                {
                    color: '#ef1e28',
                    startPlug: 'square',
                    endPlug: 'arrow2',
                    startPlugColor: '#43bc5b',
                    endPlugColor: '#1085e0',
                    startPlugSize: 1.5,
                    endPlugSize: 1.5,
                    dash: true,
                    middleLabel: LeaderLine.pathLabel('Gte. Financiero')
                }
            );

            line = new LeaderLine(
                document.getElementById('ex-020-h'),
                document.getElementById('ex-020-g'),
                {
                    color: '#ef1e28',
                    startPlug: 'square',
                    endPlug: 'arrow2',
                    startPlugColor: '#43bc5b',
                    endPlugColor: '#1085e0',
                    startPlugSize: 1.5,
                    endPlugSize: 1.5,
                    dash: true,
                    middleLabel: LeaderLine.pathLabel('CXP')
                }
            );

            var line = document.getElementById('line_4');
            if (line) {
              line.remove()
            }
            line = new LeaderLine(
                document.getElementById('ex-020-g'),
                document.getElementById('ex-020-i'),
                {
                    color: '#ef1e28',
                    startPlug: 'square',
                    endPlug: 'arrow2',
                    startPlugColor: '#43bc5b',
                    endPlugColor: '#1085e0',
                    startPlugSize: 1.5,
                    endPlugSize: 1.5,
                    dash: true,
                    middleLabel: LeaderLine.pathLabel('Jefe Tesorería')
                }
            );
            
            // new LeaderLine(
            //    document.getElementById('ex-020-g'),
            //    document.getElementById('ex-020-f'),
            //    {
            //        color: '#ef1e28',
            //        startPlug: 'square',
            //        endPlug: 'arrow2',
            //        startPlugColor: '#43bc5b',
            //        endPlugColor: '#1085e0',
            //        startPlugSize: 1.5,
            //        endPlugSize: 1.5,
            //        dash: true,
            //        middleLabel: LeaderLine.pathLabel('Compras')
            //    }
            //);

            //new LeaderLine(
            //    document.getElementById('ex-020-f'),
            //    document.getElementById('ex-020-e'),
            //    {
            //        color: '#ef1e28',
            //        startPlug: 'square',
            //        endPlug: 'arrow2',
            //        startPlugColor: '#43bc5b',
            //        endPlugColor: '#1085e0',
            //        startPlugSize: 1.5,
            //        endPlugSize: 1.5,
            //        dash: true,
            //        middleLabel: LeaderLine.pathLabel('Gte. Financiero')
            //    }
            //);

            //new LeaderLine(
            //    document.getElementById('ex-020-e'),
            //    document.getElementById('ex-020-b'),
            //    {
            //        color: '#ef1e28',
            //        startPlug: 'square',
            //        endPlug: 'arrow2',
            //        startPlugColor: '#43bc5b',
            //        endPlugColor: '#1085e0',
            //        startPlugSize: 1.5,
            //        endPlugSize: 1.5,
            //        dash: true,
            //        middleLabel: LeaderLine.pathLabel('Jefe Tesorería')
            //    }
            //);

            //new LeaderLine(
            //    document.getElementById('ex-020-b'),
            //    document.getElementById('ex-020-a'),
            //    {
            //        color: '#ef1e28',
            //        startPlug: 'square',
            //        endPlug: 'arrow2',
            //        startPlugColor: '#43bc5b',
            //        endPlugColor: '#1085e0',
            //        startPlugSize: 1.5,
            //        endPlugSize: 1.5,
            //        dash: true,
            //        middleLabel: LeaderLine.pathLabel('Jefe Tesorería')
            //    }
            //);
]]>
          </script>
        </div>
      </td>
    </tr>
  </xsl:template>
</xsl:stylesheet>
