﻿<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:msxsl="urn:schemas-microsoft-com:xslt"
	xmlns:math="http://exslt.org/math"
	xmlns:exsl="http://exslt.org/functions"
	xmlns:set="http://exslt.org/sets"
	xmlns:session="http://panax.io/session"
  xmlns:x="http://panax.io/xdom"
  xmlns:xdom="http://panax.io/xdom"
  xmlns:custom="http://panax.io/custom"
  xmlns:prev="http://panax.io/xdom/values/previous"
  xmlns:initial="http://panax.io/xdom/values/initial"
  xmlns:source="http://panax.io/xdom/binding/source"
  xmlns:bind="http://panax.io/xdom/binding"
  xmlns:query="http://panax.io/xdom/binding/query"
  xmlns:xhr="http://panax.io/xdom/xhr"
  xmlns:js="http://panax.io/languages/javascript"
  xmlns:router="http://panax.io/xdom/router"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xmlns="http://www.w3.org/1999/xhtml"
  extension-element-prefixes="math exsl msxsl"
  exclude-result-prefixes="xsl msxsl math exsl set session x xdom custom prev initial source query xhr js router xsi"
>
  <xsl:output method="xml"
     omit-xml-declaration="yes"
     indent="yes"/>

  <xsl:key name="grid_layout" match="*" use="concat(generate-id(..),'::',floor(count(preceding-sibling::*) div 3),',',count(preceding-sibling::*) mod 3)"/>
  <xsl:key name="grid_layout" match="*" use="concat(generate-id(..),'::',floor(count(preceding-sibling::*) div 3))"/>

  <xsl:template match="/">
    <xsl:apply-templates mode="shell"/>
  </xsl:template>

  <xsl:template match="*" mode="grid"></xsl:template>

  <xsl:template match="*[@x:selected='true']/*" mode="grid">
    <li class="flex-item" style="width:300px; text-align:center; cursor:pointer;" onclick="{@js:script}">
      <xsl:value-of select="@title"/>
    </li>
  </xsl:template>

  <xsl:template match="*[@x:selected='true'][*]" mode="grid">
    <xsl:param name="rows"/>
    <xsl:param name="cols">3</xsl:param>
    <xsl:variable name="total" select="count(*)"/>
    <tr>
      <th colspan="3" style="text-align:center; cursor:pointer; background-color:silver;">
        &#160;
      </th>
      <th>
        <ul class="flex-container wrap">
          <xsl:apply-templates mode="grid"/>
        </ul>
        <!--<table border="1" class="grid">
          <xsl:for-each select="*[position()&lt;=($total div $cols)]">
            <xsl:variable name="position" select="position()-1"/>
            <tr>
              <xsl:for-each select="key('grid_layout',concat(generate-id(..),'::',$position))">
                <th style="width:300px; text-align:center; cursor:pointer;" onclick="{@js:script}">
                  <xsl:value-of select="@title"/>
                </th>
              </xsl:for-each>
            </tr>
          </xsl:for-each>
        </table>-->
      </th>
    </tr>
  </xsl:template>

  <xsl:template match="*" mode="menu_item">
    <tr>
      <th colspan="3" style="text-align:center; cursor:pointer; background-color:silver;" onclick="{@js:script}">
        <xsl:value-of select="floor(count(preceding-sibling::*) div 3)"/>-
        <xsl:value-of select="count(preceding-sibling::*) mod 3"/>-
        <xsl:value-of select="@title"/>
      </th>
    </tr>
  </xsl:template>

  <xsl:template match="*[*]" mode="menu_item">
    <tr>
      <th colspan="2" style="text-align:center; cursor:pointer;" onclick="xdom.data.clearSelection(); xdom.data.selectRecord('{@x:id}');">
        <xsl:value-of select="@title"/>
      </th>
      <th style="cursor:pointer; width:15px;">
        <i class="fas fa-caret-down"></i>
      </th>
    </tr>
  </xsl:template>

  <xsl:template match="/*" mode="shell">
    <form autocomplete="off">
      <table border="0" width="100%" class="menu">
        <tr>
          <td rowspan="2" style="vertical-align:top; width:200px">
            <table width="100%">
              <xsl:apply-templates mode="menu_item"/>
              <tr>
                <th colspan="3" style="text-align:center">&#160;</th>
              </tr>
              <tr>
                <th colspan="4" style="text-align:center; cursor:pointer;" onclick="xdom.session.loadSession();">Cargar sesión</th>
              </tr>
              <tr>
                <th colspan="3" style="text-align:center; cursor:pointer;" onclick="xdom.session.logout();">Logout</th>
              </tr>
              <tr>
                <th colspan="3" style="text-align:center">&#160;</th>
              </tr>
            </table>
          </td>
          <td align="center">
            <xsl:apply-templates mode="grid" select="*"/>
            <xsl:apply-templates select="descendant-or-self::Operacion"/>
          </td>
        </tr>
      </table>
    </form>
  </xsl:template>

  <xsl:template match="/*[string(@session:id)='' or @session:status='unauthorized']" priority="9" mode="shell">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
      <style>
        <![CDATA[
        .carousel-item {
          height: 100vh;
          min-height: 350px;
          background: no-repeat center center scroll;
          -webkit-background-size: cover;
          -moz-background-size: cover;
          -o-background-size: cover;
          background-size: cover;
        }
        
        div.carousel-item::after {
          content: "";
          background: url(assets/altania.jpg) no-repeat center center scroll;
          opacity: 0.5;
          top: 0;
          left: 0;
          bottom: 0;
          right: 0;
          position: absolute;
          z-index: -1;
          -webkit-background-size: cover;
          -moz-background-size: cover;
          -o-background-size: cover;
          background-size: cover;
        }
      ]]></style>
      <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <!--<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>-->
      </ol>
      <div class="carousel-inner" role="listbox">
        <!-- Slide One - Set the background image for this slide in the line below -->
        <div class="carousel-item active" style="">
          <!--<div class="carousel-caption d-none d-md-block">
            <h2 class="display-4">Altania</h2>
            <p class="lead">This is a description for the first slide.</p>
          </div>-->
        </div>
        <!-- Slide Two - Set the background image for this slide in the line below --><!--
        <div class="carousel-item" style="background-image: url('https://source.unsplash.com/bF2vsubyHcQ/1920x1080')">
          <div class="carousel-caption d-none d-md-block">
            <h2 class="display-4">Second Slide</h2>
            <p class="lead">This is a description for the second slide.</p>
          </div>
        </div>
        --><!-- Slide Three - Set the background image for this slide in the line below --><!--
        <div class="carousel-item" style="background-image: url('https://source.unsplash.com/szFUQoyvrxM/1920x1080')">
          <div class="carousel-caption d-none d-md-block">
            <h2 class="display-4">Third Slide</h2>
            <p class="lead">This is a description for the third slide.</p>
          </div>
        </div>-->
      </div>
      <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
  </xsl:template>
</xsl:stylesheet>