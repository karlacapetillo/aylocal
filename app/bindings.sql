DECLARE @xml XML
--EXEC #xml.FromFile 'D:\Dropbox (Habi)\websites\habicheques\operaciones\operacion.xml', @xml OUT


declare @t table (result XML)
insert @t (result)
EXEC Promotora.[OperacionesFinancieras].[LeerOperacionXML] @IdOperacion=0
SELECT @xml=result FROM @t
SELECT @xml

DECLARE @xsl_initilize_operacion XML
EXEC #xml.FromFile 'D:\Dropbox (Habi)\websites\habicheques\operaciones\initialize_operacion.xslt', @xsl_initilize_operacion OUT
SELECT @xml=#xml.transform(@xml, @xsl_initilize_operacion)
SELECT @xml

DECLARE @xsl_prepare_operacion XML
EXEC #xml.FromFile 'D:\Dropbox (Habi)\websites\habicheques\operaciones\prepare_operacion.xslt', @xsl_prepare_operacion OUT
SELECT @xml=#xml.transform(@xml, @xsl_prepare_operacion)
SELECT @xml

DECLARE @xsl_prepare_data XML
EXEC #xml.FromFile 'D:\Dropbox (Habi)\websites\habicheques\operaciones\prepare_data.xslt', @xsl_prepare_data OUT
SELECT @xml=#xml.transform(@xml, @xsl_prepare_data)

DECLARE @xsl_bindings XML
EXEC #xml.FromFile 'D:\Dropbox (Habi)\websites\habicheques\operaciones\bindings_operacion.xslt', @xsl_bindings OUT
SELECT @xml=#xml.transform(@xml, @xsl_bindings)
SELECT @xml

DECLARE @xsl_html XML
EXEC #xml.FromFile 'D:\Dropbox (Habi)\websites\habicheques\operaciones\operacion.xslt', @xsl_html OUT
SELECT @xml=#xml.transform(@xml, @xsl_html)
SELECT @xml

GO 

DECLARE @xml XML
EXEC #xml.FromFile 'D:\Dropbox (Habi)\websites\habicheques\operaciones\cobertura.xml', @xml OUT
SELECT @xml=Promotora.OperacionesFinancieras.getOperacion(1, 0, 'C', 0)
SELECT @xml

DECLARE @xsl_initilize_cobertura XML
EXEC #xml.FromFile 'D:\Dropbox (Habi)\websites\habicheques\operaciones\initialize_cobertura.xslt', @xsl_initilize_cobertura OUT
SELECT @xml=#xml.transform(@xml, @xsl_initilize_cobertura)
SELECT @xml

DECLARE @xsl_prepare_cobertura XML
EXEC #xml.FromFile 'D:\Dropbox (Habi)\websites\habicheques\operaciones\prepare_cobertura.xslt', @xsl_prepare_cobertura OUT
SELECT @xml=#xml.transform(@xml, @xsl_prepare_cobertura)

DECLARE @xsl_prepare_data XML
EXEC #xml.FromFile 'D:\Dropbox (Habi)\websites\habicheques\operaciones\prepare_data.xslt', @xsl_prepare_data OUT
SELECT @xml=#xml.transform(@xml, @xsl_prepare_data)

DECLARE @xsl_bindings XML
EXEC #xml.FromFile 'D:\Dropbox (Habi)\websites\habicheques\operaciones\bindings_cobertura.xslt', @xsl_bindings OUT
SELECT @xml=#xml.transform(@xml, @xsl_bindings)
SELECT @xml

DECLARE @xsl_html XML
EXEC #xml.FromFile 'D:\Dropbox (Habi)\websites\habicheques\operaciones\operacion.xslt', @xsl_html OUT
SELECT @xml=#xml.transform(@xml, @xsl_html)
SELECT @xml

GO 

DECLARE @xml XML
EXEC #xml.FromFile 'D:\Dropbox (Habi)\websites\habicheques\operaciones\traspaso.xml', @xml OUT
SELECT @xml=Promotora.OperacionesFinancieras.getOperacion(1, 0, 'T', 0)
SELECT @xml

DECLARE @xsl_initilize_traspaso XML
EXEC #xml.FromFile 'D:\Dropbox (Habi)\websites\habicheques\operaciones\initialize_traspaso.xslt', @xsl_initilize_traspaso OUT
SELECT @xml=#xml.transform(@xml, @xsl_initilize_traspaso)
SELECT @xml

DECLARE @xsl_prepare_traspaso XML
EXEC #xml.FromFile 'D:\Dropbox (Habi)\websites\habicheques\operaciones\prepare_traspaso.xslt', @xsl_prepare_traspaso OUT
SELECT @xml=#xml.transform(@xml, @xsl_prepare_traspaso)
SELECT @xml

DECLARE @xsl_prepare_data XML
EXEC #xml.FromFile 'D:\Dropbox (Habi)\websites\habicheques\operaciones\prepare_data.xslt', @xsl_prepare_data OUT
SELECT @xml=#xml.transform(@xml, @xsl_prepare_data)

DECLARE @xsl_bindings XML
EXEC #xml.FromFile 'D:\Dropbox (Habi)\websites\habicheques\operaciones\bindings_traspaso.xslt', @xsl_bindings OUT
SELECT @xml=#xml.transform(@xml, @xsl_bindings)
SELECT @xml

DECLARE @xsl_html XML
EXEC #xml.FromFile 'D:\Dropbox (Habi)\websites\habicheques\operaciones\operacion.xslt', @xsl_html OUT
SELECT @xml=#xml.transform(@xml, @xsl_html)
SELECT @xml

GO 

--DECLARE @xml XML
--EXEC #xml.FromFile 'D:\Dropbox (Habi)\websites\habicheques\operaciones\sessions\traspaso.xml', @xml OUT
--SELECT @xml

--DECLARE @xsl_prepare_traspaso XML
--EXEC #xml.FromFile 'D:\Dropbox (Habi)\websites\habicheques\operaciones\prepare_traspaso.xslt', @xsl_prepare_traspaso OUT
--SELECT @xml=#xml.transform(@xml, @xsl_prepare_traspaso)
--SELECT @xml
GO 


DECLARE @xml XML
EXEC #xml.FromFile 'D:\Dropbox (Habi)\websites\habicheques\operaciones\NewSource.xml', @xml OUT
SELECT @xml

DECLARE @xsl_sources_operacion XML
EXEC #xml.FromFile 'D:\Dropbox (Habi)\websites\habicheques\operaciones\sources_operacion.xslt', @xsl_sources_operacion OUT
SELECT @xml=#xml.transform(@xml, @xsl_sources_operacion)
SELECT @xml

SELECT @xml=#xml.transform(@xml, @xsl_sources_operacion)
SELECT @xml
GO
-- SUBMIT
DECLARE @xml XML
EXEC #xml.FromFile 'D:\Dropbox (Habi)\websites\habicheques\operaciones\sessions\user_1.xml', @xml OUT
SELECT @xml=@xml.query('//Operacion[@xsi:type="cobertura"][1]')
SELECT @xml

DECLARE @xsl_sources_operacion XML
EXEC #xml.FromFile 'D:\Dropbox (Habi)\websites\habicheques\operaciones\post.traspaso.xslt', @xsl_sources_operacion OUT
SELECT @xml=#xml.transform(@xml, @xsl_sources_operacion)
SELECT @xml
GO

DECLARE @xml XML
EXEC #xml.FromFile 'D:\Dropbox (Habi)\websites\habicheques\operaciones\sessions\user_1.xml', @xml OUT
SELECT @xml=@xml.query('//Operacion[@xsi:type="traspaso"][1]')
SELECT @xml

DECLARE @xsl_sources_operacion XML
EXEC #xml.FromFile 'D:\Dropbox (Habi)\websites\habicheques\operaciones\post.traspaso.xslt', @xsl_sources_operacion OUT
SELECT @xml=#xml.transform(@xml, @xsl_sources_operacion)
SELECT @xml

GO
SET DATEFORMAT DMY
DECLARE @xml XML
EXEC #xml.FromFile 'D:\Dropbox (Habi)\websites\habicheques\operaciones\sessions\multiples centros de costos.xml', @xml OUT
--SELECT @xml=T.source
--FROM Promotora.[$Database].[Transactions]  T
--WHERE date>='19/03/2019'
----AND userId=1
--AND userId=3074
--AND trid=39083
----AND trid>39560
--ORDER BY date DESC

SELECT @xml=@xml.query('//Operacion[@xsi:type="operacion"][1]')
SELECT @xml

DECLARE @xsl_sources_operacion XML
EXEC #xml.FromFile 'D:\Dropbox (Habi)\websites\habicheques\operaciones\post.operacion.xslt', @xsl_sources_operacion OUT
SELECT @xml=#xml.transform(@xml, @xsl_sources_operacion)
SELECT @xml