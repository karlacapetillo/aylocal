﻿<?xml-stylesheet type="text/css" href="custom/css/wizard.css"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:x="http://panax.io/xdom"
  xmlns:state="http://panax.io/state"
  xmlns:source="http://panax.io/xdom/binding/source"
  xmlns:px="http://panax.io"
  exclude-result-prefixes="px state x"
  xmlns="http://www.w3.org/1999/xhtml"
>
  <xsl:import href="xdom/resources/wizard.xslt"/>
  <xsl:import href="messages.xslt"/>

  <xsl:template match="*" mode="styles">
    <style>
      <![CDATA[
    .btn-primary {
    color: #fff;
    background-color: #00B0AD;
    border-color: #2e6da4;
    }

    .aiia-wizard-step-title-number {
    background-color: #00B0AD;
    border-radius: 300px;
    color: rgb(255, 255, 255);
    float: left;
    font-size: 36px;
    height: 70px;
    margin-right: 10px;
    text-align: center;
    width: 70px;
    font-weight: bold;
    padding-top: 11px;
    }

    .aiia-wizard-step-title-text {
    color: #000000;
    font-size: 36px;
    float: left;
    margin-top: 8px;
    }

    .nav > li > a:focus, .nav > li > a:hover {
    text-decoration: none;
    background-color: #99ff99;
    }

    ]]>
    </style>
  </xsl:template>

  <xsl:template match="*[not(@validatedEmail=@email)]/*[1]" mode="wizard.buttons.next.attributes.onclick">
    <xsl:text/>register.validateEmail('<xsl:value-of select="../@email"/>','<xsl:value-of select="../@x:id"/>')<xsl:text/>
  </xsl:template>

  <xsl:template match="*[not(@validatedCode=@verification_code)]/*[2]" mode="wizard.buttons.next.attributes.onclick">
    <xsl:text/>register.validateCode('<xsl:value-of select="../@email"/>','<xsl:value-of select="../@verification_code"/>','<xsl:value-of select="../@x:id"/>')<xsl:text/>
  </xsl:template>

  <xsl:template match="*[not(@usuarioRegistrado=1)]/*[3]" mode="wizard.buttons.next.attributes.onclick">
    <xsl:text/>register.newUser('<xsl:value-of select="../@email"/>','<xsl:value-of select="../@verification_code"/>','<xsl:value-of select="../@confirmed_password"/>','<xsl:value-of select="../@nombre"/>','<xsl:value-of select="../@x:id"/>')<xsl:text/>
  </xsl:template>

  <xsl:template match="*[1]" mode="wizard.step.panel.content">
    <p class="text_p">
      Escribe la dirección de correo electrónico que será asociada a tu cuenta de aylocal.
    </p>
    <div class="form-group row">
      <label class="col-sm-3 col-form-label">Correo Eléctronico:</label>
      <div class="col-sm-9">
        <input id="{@x:id}" name="fname" onchange="xdom.data.update('{../@x:id}', '@email', this.value)" value="{../@email}" size="25" maxlength="255" class="form-control"/>
      </div>
    </div>
  </xsl:template>

  <xsl:template match="*[2]" mode="wizard.step.panel.content">
    <p>Por favor escribe el código que llegó a tu correo:</p>
    <div class="table form">
      <div class="body">
        <div class="form-group row">
          <label class="col-sm-3 col-form-label">Código de verificación:</label>
          <div class="col-sm-9">
            <input id="VerificationCode" type="input" value="{../@verification_code}" size="5" maxlength="5" class="form-control 
            required  is-invalid" onchange="xdom.data.update('{../@x:id}', '@verification_code', this.value)"/>
            <span class="invalid-feedback" style="margin-left:5pt;">Campo requerido</span>
          </div>
        </div>
      </div>
    </div>
  </xsl:template>

  <xsl:template match="*[3]" mode="wizard.step.panel.content">
    <p>Por favor escribe tu contraseña y confírmala:</p>
    <div class="table form">
      <div class="body">
        <div class="form-group row">
          <label class="col-sm-3 col-form-label">Nombre:</label>
          <div class="col-sm-9">
            <input id="Nombre" type="text" value="{../@nombre}" size="25" maxlength="255" class="form-control 
            required  is-invalid" onchange="xdom.data.update('{../@x:id}', '@nombre', this.value)"/>
            <span class="invalid-feedback" style="margin-left:5pt;">Campo requerido</span>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-sm-3 col-form-label">Contraseña:</label>
          <div class="col-sm-9">
            <input id="Password" type="password" value="{../@password}" size="25" maxlength="255" class="form-control 
            required  is-invalid" onchange="xdom.data.update('{../@x:id}', '@password', calcMD5(this.value)); xdom.data.update('{../@x:id}', '@confirmed_password', '')"/>
            <span class="invalid-feedback" style="margin-left:5pt;">Campo requerido</span>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-sm-3 col-form-label">Confirma tu contraseña:</label>
          <div class="col-sm-9">
            <input id="ConfirmedPassword" type="password" value="{../@confirmed_password}" size="25" maxlength="255" class="form-control 
            required  is-invalid" onchange="this.value=calcMD5(this.value); if (this.value!='{../@password}') {{ alert('Las contraseñas no coinciden'); this.value=''; }} xdom.data.update('{../@x:id}', '@confirmed_password', this.value)"/>
            <span class="invalid-feedback" style="margin-left:5pt;">Campo requerido</span>
          </div>
        </div>
      </div>
    </div>
  </xsl:template>

  <xsl:template match="*[4]" mode="wizard.step.panel.content">
    <p>¿Deseas convertirte en vendedor?</p>
    <div class="container-btn">
      <div class="container-button-1">
        <button class="btn btn-primary-wizard pull-left aiia-wizard-button-previous" style="display: block; margin: 0 30px;" onclick="register.convertirVendedor('{../@email}','{../@x:id}')">
          <span>Sí</span>
        </button>
      </div>
      <div class="container-button-2">
        <button class="btn btn-primary-wizard pull-right aiia-wizard-button-next" style="display: block; margin: 0 30px;">
          <xsl:attribute name="onclick">
            <xsl:apply-templates select="." mode="wizard.buttons.next.attributes.onclick"/>
          </xsl:attribute>
          <span>No</span>
        </button>
      </div>
    </div>
  </xsl:template>

  <xsl:template match="*[last()]" mode="wizard.step.panel.content">
    <p>Bienvenido al sistema. Ya puedes firmarte con tus credenciales.</p>
  </xsl:template>

  <xsl:template match="*[last()]" mode="wizard.buttons.next.attributes.onclick">
    <xsl:text/>xdom.session.login('<xsl:value-of select="../@email"/>', '<xsl:value-of select="../@confirmed_password"/>', 'main'); xdom.data.load('sitemap.asp')<xsl:text/>
  </xsl:template>

  <xsl:template match="*[last()]" mode="wizard.buttons.next">
    <xsl:call-template name="wizard.buttons.next">
      <xsl:with-param name="text">Iniciar sesión</xsl:with-param>
    </xsl:call-template>
  </xsl:template>

</xsl:stylesheet>