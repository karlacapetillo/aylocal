﻿<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:x="http://panax.io/xdom"
xmlns:bind="http://panax.io/xdom/binding"
xmlns:request="http://panax.io/xdom/binding/request"
xmlns:source="http://panax.io/xdom/binding/source"
xmlns:query="http://panax.io/xdom/binding/query"
xmlns:prev="http://panax.io/xdom/values/previous"
>
  <xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>
  <xsl:key name="undependant" match="@source:*[.!='' and not(contains(.,'{{'))]" use="concat(name(),'_',generate-id(..))"/>
  <xsl:key name="datasource" match="@source:*" use="concat(name(),'_',generate-id(..))"/>
  <xsl:key name="datasource" match="source:*[@for]" use="concat(name(),'::',@for)"/>
  <xsl:key name="datasource" match="source:*[@for]" use="concat(name(),'_',generate-id(..),'::',@for)"/>
  <xsl:key name="datasource" match="source:*[@for]/x:r" use="concat(name(..),'::',../@for,'::',@x:value)"/>
  <!--<xsl:key name="datasource" match="source:*[@for]/x:r[@x:value=1]" use="concat(name(..),'::',../@for,'::','')"/>
  <xsl:key name="datasource" match="source:*[@for]/x:r[@x:value=2]" use="concat(name(..),'::',../@for,'::','')"/>-->


  <xsl:template match="@* | text()" priority="-1">
    <xsl:copy-of select="."/>
  </xsl:template>

  <xsl:template match="node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*"/>
      <xsl:apply-templates mode="sources" select="@source:*"/>
      <xsl:apply-templates/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="source:*[key('datasource',concat(name(),'_',generate-id(..)))]"/>

  <xsl:template match="*" mode="sources"/>

  <xsl:template match="@request:*|@bind:pending"/>

    <xsl:template match="@source:*" mode="sources">
    <xsl:variable name="attribute_name" select="local-name()"/>
    <xsl:variable name="data_source" select="key('datasource',concat(name(),'::',.,'::',../@x:*[local-name()=$attribute_name]))[1]"/>
    <xsl:variable name="value" select="$data_source/@x:value"/>
    <xsl:variable name="text" select="$data_source/@x:text"/>
    <xsl:if test="$value!=@x:value and @x:value!='' and @x:value!='0'">
      <xsl:attribute name="prev:value">
        <xsl:value-of select="@x:value"/>
      </xsl:attribute>
      <xsl:attribute name="prev:text">
        <xsl:value-of select="@x:text"/>
      </xsl:attribute>
    </xsl:if>
    <xsl:attribute name="x:value">
      <xsl:value-of select="$value"/>
    </xsl:attribute>
    <xsl:attribute name="x:text">
      <xsl:value-of select="$text"/>
    </xsl:attribute>
    <xsl:if test=".!='' and not(contains(.,'{{')) and not(key('datasource',concat(name(),'::',.)))">
      <xsl:attribute name="request:{local-name()}">true</xsl:attribute>
    </xsl:if>
    <xsl:copy-of select="key('datasource',concat(name(),'::',.))[1]"/>
  </xsl:template>
</xsl:stylesheet>