﻿<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:debug="http://panax.io/debug"
  xmlns:msxsl="urn:schemas-microsoft-com:xslt"
  xmlns:x="http://panax.io/xdom"
  xmlns:session="http://panax.io/session"
  xmlns:filters="http://panax.io/filters"
  xmlns:custom="http://panax.io/custom"
  xmlns:datagrid="http://panax.io/widgets/datagrid"
  xmlns="http://www.w3.org/1999/xhtml"
>
  <xsl:include href="resources/datagrid.xslt"/>

  <xsl:decimal-format
      name="money"
      grouping-separator=","
      decimal-separator="."/>

  <xsl:key match="/*/*/*" name="data" use="name()"/>

  <xsl:key name="filterBy" match="/*/*" use="generate-id(.)"/>

  <xsl:key name="groupBy" match="/" use="."/>
  <xsl:key name="groupBy" match="Número_de_Cuenta/@x:value" use="concat(name(..),'::',.)"/>

  <!--<xsl:key name="groupCollapse" match="Número_de_Cuenta/@x:value[.='65503130892']" use="concat(name(..),'::',.)"/>-->


  <xsl:key name="hidden" match="/" use="."/>
  <xsl:key name="hidden" match="Id" use="generate-id()"/>
  <xsl:key name="hidden" match="Linea" use="generate-id()"/>
  <xsl:key name="hidden" match="Sucursal" use="generate-id()"/>
  <xsl:key name="hidden" match="Clacon" use="generate-id()"/>
  <xsl:key name="hidden" match="/*[string(@filterByValue)!='']//Saldo" use="generate-id()"/>
  <xsl:key name="hidden" match="Acumulado" use="generate-id()"/>
  <xsl:key name="hidden" match="Linea_Original" use="generate-id()"/>
  <xsl:key name="hidden" match="IdMovimiento" use="generate-id()"/>

  <xsl:key name="sortBy" match="/*" use="@sortBy"/>

  <xsl:key name="filters" match="/*/*[not(@forDeletion='true')]/*" use="concat(name(),'::',@x:value)"/>

  <xsl:key name="sortOrder" match="/*" use="concat(@sortOrder,'::',@sortBy)"/>

  <xsl:key name="visible" match="/*/*/*[count(preceding-sibling::*|.)&lt;=40]" use="generate-id()"/>

  <xsl:key name="money" match="Importe" use="generate-id(.)"/>
  <xsl:key name="money" match="Saldo" use="generate-id(.)"/>
  <xsl:key name="money" match="Acumulado" use="generate-id(.)"/>

  <xsl:key name="date" match="Fecha_del_movimiento" use="generate-id(.)"/>

  <xsl:key name="totalizer" match="Importe" use="name()"/>

  <xsl:template match="IdMovimiento" mode="datagrid.header.headertext">
    <xsl:text>Mov.</xsl:text>
  </xsl:template>

  <xsl:template match="/*" mode="datagrid.header">
    <tr id="container_{@x:id}">
      <th colspan="{2+count(*[1]/*)}">
        <div class="w3-bar">
          <div class="w3-bar-item">
            <input id="fecha_inicio" type="text" maxSize="10" size="12" onchange="xdom.data.update('{@x:id}', '@custom:fecha_inicio', autoCompleteDate(this.value));" style="color:black;" value="{@custom:fecha_inicio}" class="w3-input w3-border"/>
          </div>
          <div class="w3-bar-item">
            <input id="fecha_fin" type="text" maxSize="10" size="12" onchange="xdom.data.update('{@x:id}', '@custom:fecha_final', autoCompleteDate(this.value));" style="color:black;" value="{@custom:fecha_final}" class="w3-input w3-border"/>
          </div>
          <div class="w3-bar-item">
            <button id="buscar" type="button" onclick="xdom.app['Operaciones'].cargarMovimientosImportados('{@custom:fecha_inicio}','{@custom:fecha_final}');" class="w3-btn w3-green w3-round w3-border w3-border-white">Buscar</button>
            &#160;Registros: <xsl:value-of select="count(/*/*[key('filterBy',generate-id())])"/><xsl:if test="count(/*/*[key('filterBy',generate-id())])!=count(/*/*)">
              <span class="fas fa-filter" style="padding-left: 5pt;" onclick="xdom.data.clearFilter()"></span> / <xsl:value-of select="count(/*/*)"/>
            </xsl:if>
          </div>
          <div class="w3-bar-item">
            <button id="reload" type="button" onclick="xdom.library.reload();" class="w3-btn w3-green w3-round w3-border w3-border-white">Actualizar archivos</button>
          </div>
        </div>
        <!--<button type="button" style="color:black" onclick="xdom.data.newRecord()">New</button>
        <button type="button" style="color:black" onclick="xdom.data.reload()">Refresh</button>-->
      </th>
    </tr>
    <xsl:apply-templates select="." mode="datagrid.header.columns"/>
  </xsl:template>

  <xsl:template match="Status/@x:value[.='Aplicado']">
    <xsl:attribute name="style">color:green</xsl:attribute>
    <strong>
      <xsl:value-of select="."/>
    </strong>
  </xsl:template>

  <xsl:template match="Status/@x:value[.='Sin aplicar']">
    <xsl:attribute name="style">color:silver</xsl:attribute>
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="Status/@x:value[.='Aplicar']">
    <button id="buscar" type="button" onclick="xdom.data.update('{../../@x:id}','../../Status/@x:value','Aplicando'); xdom.app['Operaciones'].aplicarOperacion('{../../Id/@x:value}','{../../IdMovimiento/@x:value}');" class="w3-button w3-green w3-round w3-border w3-border-white">Aplicar</button>
  </xsl:template>

  <xsl:template match="Status/@x:value[.='Aplicando']">
    <button id="buscar" type="button" onclick="xdom.app['EstadosDeCuenta'].actualizarAlternativas('{../../IdMovimiento/@x:value}');" class="w3-button w3-green w3-round w3-border w3-border-white w3-disabled">Aplicando</button>
  </xsl:template>

</xsl:stylesheet>
