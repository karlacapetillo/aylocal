﻿<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" indent="no"/>
    <xsl:template match="@* | node()">
        <xsl:copy>
            <xsl:apply-templates select="@* | node()"/>
        </xsl:copy>
    </xsl:template>
  <xsl:template match="xsl:apply-templates[@mode='body' and @select='d/r']">
    <xsl:copy>
      <xsl:copy-of select="@*"/>
      <xsl:element name="sort" namespace="http://www.w3.org/1999/XSL/Transform">
        <!--<xsl:element name="xsl:sort">-->
          <xsl:attribute name="select">*[@ref='location']</xsl:attribute>
      </xsl:element>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>
