﻿<?xml-stylesheet type="text/css" href="custom/css/formview.css"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:x="http://panax.io/xdom"
  xmlns:source="http://panax.io/xdom/binding/source"
  xmlns:state="http://panax.io/state"
  xmlns:px="http://panax.io"
  exclude-result-prefixes="px state x source"
  xmlns="http://www.w3.org/1999/xhtml"
>
  <xsl:import href="datagridview.xslt"/>
  <xsl:import href="xdom/resources/formview.xslt"/>
  <xsl:import href="xdom/resources/messages.xslt"/>

  <!--<xsl:key name="status" match="px:data/px:dataRow/EstatusProveedor/*/px:data/px:dataRow/StatusActual" use="@x:value"/>-->
  <xsl:key name="controls.email" match="px:fields/Email[@controlType='default']" use="@fieldId"/>
  <xsl:key name="controls.email" match="px:fields/NombreUsuario[@controlType='default']" use="@fieldId"/>

  <xsl:key name="hidden" match="*[not(px:data/px:dataRow/TipoPersona/@x:value='Física')]/px:layout/px:field[@fieldName='PersonaFisica']" use="generate-id()"/>
  <xsl:key name="hidden" match="*[not(px:data/px:dataRow/TipoPersona/@x:value='Moral')]/px:layout/px:field[@fieldName='PersonaMoral']" use="generate-id()"/>

  <xsl:key name="hidden" match="*[not(px:data/px:dataRow/TipoArticulo/@x:value='Producto')]/px:layout/px:field[@fieldName='FK_Producto_TipoCategoria1']" use="generate-id()"/>
  <xsl:key name="hidden" match="*[not(px:data/px:dataRow/TipoArticulo/@x:value='Servicio')]/px:layout/px:field[@fieldName='FK_Servicio_TipoCategoria1']" use="generate-id()"/>
  <xsl:key name="hidden" match="*[not(px:data/px:dataRow/TipoArticulo/@x:value='Evento')]/px:layout/px:field[@fieldName='FK_Evento_TipoCategoria1']" use="generate-id()"/>

  <xsl:key name="hidden" match="*/px:layout/px:field[@fieldName='Vendedor']" use="generate-id()" />

  <xsl:key name="hidden" match="px:dataRow[not(EstatusProveedor/*/px:data/px:dataRow/EsProveedorCritico/@x:value=1)]/Documentacion/*/px:data/px:dataRow/NumeroCertifcacionSeguridad" use="generate-id()"/>

  <xsl:template match="*[not(key('junctionTable',../@fieldId))]/px:data/px:dataRow" mode="datagrid.data.buttons.delete.action">
    <xsl:attribute name="onclick">
      xdom.data.remove('<xsl:value-of select="@x:id"/>')
    </xsl:attribute>
  </xsl:template>

  <xsl:template match="/">
    <div>
      <xsl:apply-templates select="*/x:message"/>
      <xsl:apply-templates select="*/px:data/px:dataRow"/>
    </div>
  </xsl:template>

  <xsl:template mode="form.header.buttons" match="Proveedor">
    <xsl:param name="data_row"/>
    <xsl:apply-imports/>
    <style>
      <![CDATA[
    .whenForm {display:none;}
    .form .whenForm {display:inline !important;}
    .form .whenDoc {display:none;}
    ]]>
    </style>
    <xsl:if test="not(@state:submitting='true' or @state:submitted='true')">
      <button type="button" onclick="proveedores.verFormatoAlta();" style="margin-left:10px;">
        <xsl:attribute name="class">
          <xsl:text/>btn btn-outline-info<xsl:text/>
        </xsl:attribute>
        <!--<xsl:choose>
        <xsl:when test="key('submitted',true())">
          <xsl:attribute name="class">btn btn-success my-2 my-sm-0</xsl:attribute>
          <xsl:attribute name="onclick">window.history.back();</xsl:attribute>
          <xsl:text>Regresar</xsl:text>
        </xsl:when>
        <xsl:otherwise>Ver documento de alta</xsl:otherwise>
      </xsl:choose>-->
        <span class="whenForm">
          <xsl:text>Ver documento de alta</xsl:text>
        </span>
        <span class="whenDoc">
          <xsl:text>Ver formulario</xsl:text>
        </span>
      </button>
      <div style="margin-left:10px;">
        <xsl:apply-templates mode="control" select="$data_row/Estatus">
          <xsl:with-param name="data_row" select="$data_row"/>
        </xsl:apply-templates>
      </div>
    </xsl:if>
  </xsl:template>
  
  <xsl:template match="IVA//source:value//@x:value">
    <xsl:apply-imports/>
    <xsl:text> %</xsl:text>
  </xsl:template>

  <xsl:template mode="control.attributes" match="px:data/px:dataRow/SitioWeb|px:data/px:dataRow/Email">
  </xsl:template>
  
  <xsl:template match="px:dataRow/DiasCredito" mode="control" priority="10">
    <div class="d-flex flex-nowrap">
      <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" id="{@x:id}_8" name="{@x:id}" class="custom-control-input" onclick="xdom.data.update('{@x:id}','@x:value',this.value)" value="8">
          <xsl:if test="@x:value=8">
            <xsl:attribute name="checked">checked</xsl:attribute>
          </xsl:if>
        </input>
        <label class="custom-control-label" for="{@x:id}_8">8 días</label>
      </div>
      <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" id="{@x:id}_15" name="{@x:id}" class="custom-control-input" onclick="xdom.data.update('{@x:id}','@x:value',this.value)" value="15">
          <xsl:if test="@x:value=15">
            <xsl:attribute name="checked">checked</xsl:attribute>
          </xsl:if>
        </input>
        <label class="custom-control-label" for="{@x:id}_15">15 días</label>
      </div>
      <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" id="{@x:id}_21" name="{@x:id}" class="custom-control-input" onclick="xdom.data.update('{@x:id}','@x:value',this.value)" value="21">
          <xsl:if test="@x:value=21">
            <xsl:attribute name="checked">checked</xsl:attribute>
          </xsl:if>
        </input>
        <label class="custom-control-label" for="{@x:id}_21">21 días</label>
      </div>
      <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" id="{@x:id}_30" name="{@x:id}" class="custom-control-input" onclick="xdom.data.update('{@x:id}','@x:value',this.value)" value="30">
          <xsl:if test="@x:value=30">
            <xsl:attribute name="checked">checked</xsl:attribute>
          </xsl:if>
        </input>
        <label class="custom-control-label" for="{@x:id}_30">30 días</label>
      </div>
      <div class="d-flex flex-nowrap">
        <input type="text" id="{@x:id}_otro" name="{@x:id}" onchange="xdom.data.update('{@x:id}','@x:value',this.value)" size="4" maxlength="3">
          <xsl:if test="@x:value!=30 and @x:value!=21 and @x:value!=15 and @x:value!=8">
            <xsl:attribute name="value">
              <xsl:value-of select="@x:value"/>
            </xsl:attribute>
          </xsl:if>
        </input>
        <label class="control-label" for="{@x:id}_otro" style="margin-left:5px;"> Días</label>
      </div>
    </div>
  </xsl:template>


</xsl:stylesheet>