﻿<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:x="http://panax.io/xdom" xmlns:state="http://panax.io/state" xmlns:px="http://panax.io" xmlns="http://www.w3.org/1999/xhtml" version="1.0" exclude-result-prefixes="x state px">
  <xsl:import href="xdom/resources/keys.xslt"/>
  <xsl:import href="xdom/resources/format_values.xslt"/>
  <xsl:import href="xdom/resources/controls.xslt"/>
  <xsl:import href="templates/FO-PG-CPR-016_Alta_proveedores.xsl"/>

  <xsl:variable name="resources-path">http://localhost/petro/Version/Beta_12.0/xdom/templates/</xsl:variable>
  <!--<xsl:variable name="resources-path">http://urielisc.ddns.net//petro/xdom/templates/</xsl:variable>-->

  <xsl:key name="controls.email" match="px:fields/Email[@controlType='default']" use="@fieldId"/>
  <xsl:key name="controls.email" match="px:fields/NombreUsuario[@controlType='default']" use="@fieldId"/>

  <xsl:key name="blocked" match="*" use="true()"/>

  <xsl:key name="data" match="Proveedor/px:data[1]/px:dataRow[1]/ContactadoPor/@x:value" use="'Colaborador'"/>
  <xsl:key name="data" match="Proveedor/px:data[1]/px:dataRow[1]/PuestoDeQuienContacta/@x:value" use="'Colaborador/Puesto'"/>
  <xsl:key name="data" match="Proveedor/px:data[1]/px:dataRow[1]/@identity" use="'Id'"/>
  <xsl:key name="data" match="Proveedor/px:data[1]/px:dataRow[1]/Nombre/@x:value" use="'Proveedor/Nombre'"/>
  <xsl:key name="data" match="Proveedor/px:data[1]/px:dataRow[1]/RFC/@x:value" use="'Proveedor/RFC'"/>
  <xsl:key name="data" match="Proveedor/px:data[1]/px:dataRow[1]/Calle/@x:value" use="'Calle'"/>
  <xsl:key name="data" match="Proveedor/px:data[1]/px:dataRow[1]/NumeroExterior/@x:value" use="'NumeroExterior'"/>
  <xsl:key name="data" match="Proveedor/px:data[1]/px:dataRow[1]/NumeroInterior/@x:value" use="'NumeroInterior'"/>
  <xsl:key name="data" match="Proveedor/px:data[1]/px:dataRow[1]/Colonia/@x:value" use="'Colonia'"/>
  <xsl:key name="data" match="Proveedor/px:data[1]/px:dataRow[1]/CodigoPostal/@x:value" use="'CP'"/>
  <xsl:key name="data" match="Proveedor/px:data[1]/px:dataRow[1]/Telefono/@x:value" use="'Telefono'"/>
  <xsl:key name="data" match="Proveedor/px:data[1]/px:dataRow[1]/Localidad/@x:value" use="'Localidad'"/>
  <xsl:key name="data" match="Proveedor/px:data[1]/px:dataRow[1]/Municipio/@x:value" use="'Municipio'"/>
  <xsl:key name="data" match="Proveedor/px:data[1]/px:dataRow[1]/Estado/@x:value" use="'EntidadFederativa'"/>

  <xsl:key name="data" match="Contactos/px:data[px:dataRow]/px:dataRow[1]/Email/@x:value" use="'Email'"/>
  <xsl:key name="data" match="Contactos/px:data[px:dataRow]/px:dataRow[1]/Nombre/@x:value" use="'Contacto'"/>

  <xsl:key name="data" match="ProductosOfrecidos/px:data[1]/px:dataRow[1][@x:checked='true']/IdProducto/@x:text" use="'ProductoServicio'"/>

  <xsl:key name="data" match="px:dataRow[1]/DiasCredito/@x:value" use="'Dias'"/>
  <xsl:key name="data" match="px:dataRow[1]/AproximadoCredito/@x:value" use="'MontoCredito'"/>

  <xsl:key name="data" match="DatosBancarios/px:data[1]/px:dataRow[1]/Banco/@x:text" use="'Banco'"/>
  <xsl:key name="data" match="DatosBancarios/px:data[1]/px:dataRow[1]/Ciudad/Municipio/@x:text" use="'CuentaBancaria/Ciudad'"/>
  <xsl:key name="data" match="DatosBancarios/px:data[1]/px:dataRow[1]/Ciudad/Municipio/Estado/@x:text" use="'CuentaBancaria/Estado'"/>
  <xsl:key name="data" match="DatosBancarios/px:data[1]/px:dataRow[1]/NumeroCuenta/@x:value" use="'NumeroCuenta'"/>
  <xsl:key name="data" match="DatosBancarios/px:data[1]/px:dataRow[1]/Moneda/@x:text" use="'Moneda'"/>
  <xsl:key name="data" match="DatosBancarios/px:data[1]/px:dataRow[1]/NumeroSucursal/@x:value" use="'Sucursal'"/>
  <xsl:key name="data" match="DatosBancarios/px:data[1]/px:dataRow[1]/EntidadSucursal/@x:value" use="'Sucursal/Entidad'"/>
  <xsl:key name="data" match="DatosBancarios/px:data[1]/px:dataRow[1]/PlazaNumero/@x:value" use="'Plaza'"/>
  <xsl:key name="data" match="DatosBancarios/px:data[1]/px:dataRow[1]/PlazaEntidad/@x:value" use="'Plaza/Entidad'"/>

  <xsl:key name="data" match="DatosBancarios/px:data[1]/px:dataRow[1]/CLABE" use="'CLABE'"/>
  <xsl:key name="data" match="Proveedor/px:data[1]/px:dataRow[1]/InformacionAdicional/@x:value" use="'InformacionAdicional'"/>
  <xsl:key name="data" match="Contactos/px:data[px:dataRow]/px:dataRow[1]/Nombre/@x:value" use="'Solicita/Nombre'"/>
  <xsl:key name="data" match="Contactos/px:data[px:dataRow]/px:dataRow[1]/Puesto/@x:text" use="'Departamento'"/>
  <xsl:key name="data" match="px:dataRow[1]/Fecha/@x:text" use="'Fecha'"/>

  <xsl:key name="data" match="Contactos/px:data[px:dataRow]/px:dataRow[1]/Nombre/@x:value" use="'Contacto[1]/Nombre'"/>
  <xsl:key name="data" match="Contactos/px:data[px:dataRow]/px:dataRow[1]/Puesto/@x:text" use="'Contacto[1]/Puesto'"/>
  <xsl:key name="data" match="Contactos/px:data[px:dataRow]/px:dataRow[1]/Email/@x:value" use="'Contacto[1]/Email'"/>
  <xsl:key name="data" match="Contactos/px:data[px:dataRow]/px:dataRow[1]/Telefono/@x:value" use="'Contacto[1]/Telefono'"/>
  <xsl:key name="data" match="Contactos/px:data[px:dataRow]/px:dataRow[1]/Extensión/@x:value" use="'Contacto[1]/Extensión'"/>

  <xsl:key name="data" match="Contactos/px:data[px:dataRow]/px:dataRow[2]/Nombre/@x:value" use="'Contacto[2]/Nombre'"/>
  <xsl:key name="data" match="Contactos/px:data[px:dataRow]/px:dataRow[2]/Puesto/@x:text" use="'Contacto[2]/Puesto'"/>
  <xsl:key name="data" match="Contactos/px:data[px:dataRow]/px:dataRow[2]/Email/@x:value" use="'Contacto[2]/Email'"/>
  <xsl:key name="data" match="Contactos/px:data[px:dataRow]/px:dataRow[2]/Telefono/@x:value" use="'Contacto[2]/Telefono'"/>
  <xsl:key name="data" match="Contactos/px:data[px:dataRow]/px:dataRow[2]/Extensión/@x:value" use="'Contacto[2]/Extensión'"/>

  <xsl:key name="data" match="Contactos/px:data[px:dataRow]/px:dataRow[3]/Nombre/@x:value" use="'Contacto[3]/Nombre'"/>
  <xsl:key name="data" match="Contactos/px:data[px:dataRow]/px:dataRow[3]/Puesto/@x:text" use="'Contacto[3]/Puesto'"/>
  <xsl:key name="data" match="Contactos/px:data[px:dataRow]/px:dataRow[3]/Email/@x:value" use="'Contacto[3]/Email'"/>
  <xsl:key name="data" match="Contactos/px:data[px:dataRow]/px:dataRow[3]/Telefono/@x:value" use="'Contacto[3]/Telefono'"/>
  <xsl:key name="data" match="Contactos/px:data[px:dataRow]/px:dataRow[3]/Extensión/@x:value" use="'Contacto[3]/Extensión'"/>

  <xsl:key name="data" match="Contactos/px:data[px:dataRow]/px:dataRow[4]/Nombre/@x:value" use="'Contacto[4]/Nombre'"/>
  <xsl:key name="data" match="Contactos/px:data[px:dataRow]/px:dataRow[4]/Puesto/@x:text" use="'Contacto[4]/Puesto'"/>
  <xsl:key name="data" match="Contactos/px:data[px:dataRow]/px:dataRow[4]/Email/@x:value" use="'Contacto[4]/Email'"/>
  <xsl:key name="data" match="Contactos/px:data[px:dataRow]/px:dataRow[4]/Telefono/@x:value" use="'Contacto[4]/Telefono'"/>
  <xsl:key name="data" match="Contactos/px:data[px:dataRow]/px:dataRow[4]/Extensión/@x:value" use="'Contacto[4]/Extensión'"/>



  <xsl:template match="@*">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="*" mode="resources-path">
    <xsl:value-of select="$resources-path"/>
  </xsl:template>

  <xsl:template mode="control.attributes" match="*">
    <xsl:attribute name="style">background-color: aliceblue;</xsl:attribute>
  </xsl:template>

  <xsl:template mode="control.attributes" match="Calle">
    <xsl:attribute name="style">background-color: aliceblue; max-height: 38;vertical-align: top;</xsl:attribute>
  </xsl:template>

  <xsl:template mode="html.head" match="*">
    <link rel="stylesheet" href="{$resources-path}../css/fontawesome.all.css" />
    <link rel="stylesheet" href="{$resources-path}../css/bootstrap.min.css" />
  </xsl:template>

  <xsl:template match="px:dataRow/*">
    <xsl:variable name="field" select="key('field',@fieldId)"/>
    <xsl:apply-templates mode="control" select="$field">
      <xsl:with-param name="data_field" select="."/>
    </xsl:apply-templates>
  </xsl:template>

  <!--<xsl:template match="*|@*" mode="container">
    <xsl:attribute name="style">
      <xsl:text/>white-space: nowrap; overflow: hidden;<xsl:text/>
    </xsl:attribute>
  </xsl:template>-->

  <xsl:template match="CLABE" mode="container">
    <xsl:attribute name="style">
      <xsl:text/>display:inline; letter-spacing: 4.3px; font-family: monospace;<xsl:text/>
    </xsl:attribute>
    <!--<style>
      .CLABE_5,.CLABE_10,.CLABE_15,.CLABE_20 {color:black;}
    </style>-->
  </xsl:template>

  <xsl:template match="CLABE" priority="2">
    <style>
      .separated-text {margin: 3.2px;}
    </style>
    <xsl:call-template name="separate-letters">
      <xsl:with-param name="string" select="@x:value"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template name="separate-letters">
    <xsl:param name="string"/>
    <xsl:variable name="current_position" select="string-length(@x:value) - string-length($string)+1"/>
    <span class="{concat(local-name(),'_',$current_position)} separated-text">
      <xsl:text/>
      <xsl:value-of select="substring($string,1,1)"/>
      <xsl:text/>
    </span>
    <xsl:if test="$current_position=4 or $current_position=8 or $current_position=12 or $current_position=16">
      <span class="{concat(local-name(),'_',$current_position)} separated-text" style="margin: 4px; color:black;">-</span>
    </xsl:if>
    <xsl:if test="string-length($string)&gt;1">
      <xsl:call-template name="separate-letters">
        <xsl:with-param name="string" select="normalize-space(substring($string,2))"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <xsl:template match="px:dataRow/*"/>

  <xsl:template match="@x:value|@x:text">
    <span style="text-transform: uppercase">
      <xsl:apply-imports/>
    </span>
  </xsl:template>

  <xsl:template match="*[key('controls.email',@fieldId)]/@x:value">
    <span style="text-transform: lowercase">
      <xsl:apply-imports/>
    </span>
  </xsl:template>

</xsl:stylesheet>