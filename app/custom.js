﻿xdom.messages.unauthorized = "Credenciales no válidas";
xdom.messages.transform_exception = undefined;
xdom.messages["filter.error"] = "Filtering is not available."
var sat = {};
sat.validarFactura = function () {
    var xhr = xdom.server.request({
        "request": "#SAT.validarFactura"
        , "xhr": { "headers": { "Accept": 'text/xml' } }
    });
}
//var xhr = new xdom.xhr.Request('server/upload_xml.asp', {
//    headers: {
//        "Accept": 'text/xml'
//        , "SOAPAction": 'http://tempuri.org/IConsultaCFDIService/Consulta'
//        , "Content-Type": 'text/xml; charset=utf-8'
//        , "cache-control": 'no-cache'
//    }
//    , onSuccess: function () {
//        console.log("Uploaded file");
//    }
//});
//    xhr.post('<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/"> \
//        < soapenv:Header />    \
//    <soapenv:Body> \
//        <tem:Consulta> \
//         <tem:expresionImpresa><![CDATA[?re=CSO160705BN5&rr=DIU171207NF8&tt=20880.00&id=458D3FD4-8FCC-458D-BF07-F218B3B9569A]]></tem:expresionImpresa>   \
//      </tem:Consulta> \
//   </soapenv:Body>    \
//</soapenv:Envelope>');
//}

var cart = {};
cart.empty = function () {
    xdom.data.remove(xdom.dom.shell.selectNodes('//shell:cart/*'));
    xdom.dom.refresh({ forced: true });
}

cart.add = function (id) {
    if (!xdom.data.documents["#busqueda"]) {
        return;
    }


    var item = xdom.xml.createDocument(xdom.data.documents["#busqueda"].selectSingleNode('//producto[Id/text()="' + id + '"]'));
    xdom.dom.cart.appendChild(item.documentElement);
    xdom.dom.refresh({ forced: true });
}

var register = {}
register.validateEmail = function (email, target) {
    xdom.server.request({
        request: '[Seguridad].[validarCorreo]&parameters=@email=\'' + email + '\''
        , onSuccess: function (Response) {
            switch (Response.type) {
                case "xml":
                    var oTarget = xdom.data.find(target);
                    if (oTarget) {
                        oTarget.setAttribute('validatedEmail', (Response.document || Response).selectSingleNode('//email').textContent);
                        oTarget.setAttribute('state:active', 2);
                        xdom.dom.refresh();
                    }
                    break;
                default:
                    console.log(Response.document)
            }
        }
    })
}


register.validateCode = function (email, code, target) {
    xdom.server.request({
        request: '[Seguridad].[validarCodigo]&parameters=@codigo=\'' + code + '\', @email=\'' + email + '\''
        , onSuccess: function (Response) {
            switch (Response.type) {
                case "xml":
                    var oTarget = xdom.data.find(target);
                    if (oTarget) {
                        oTarget.setAttribute('validatedCode', (Response.document || Response).selectSingleNode('//codigo').textContent);
                        oTarget.setAttribute('state:active', 3);
                        xdom.dom.refresh();
                    }
                    break;
                default:
                    console.log(Response.document)
            }
        }
    })
}

register.newUser = function (email, code, passwd, nombre, target) {
    xdom.server.request({
        request: '[Seguridad].[registrarUsuario]&parameters=@codigo=\'' + code + '\', @email=\'' + email + '\', @pass=\'' + passwd + '\', @nombre=\'' + nombre +'\''
        , onSuccess: function (Response) {
            switch (Response.type) {
                case "xml":
                    var oTarget = xdom.data.find(target);
                    if (oTarget) {
                        oTarget.setAttribute('usuarioRegistrado', (Response.document || Response).selectSingleNode('//success').textContent);
                        oTarget.setAttribute('state:active', 4);
                        xdom.dom.refresh();
                    }
                    break;
                default:
                    console.log(Response.document)
            }
        }
    })
}

register.convertirVendedor = function (email, target) {
    xdom.server.request({
        request: '[SocioDeNegocios].[convertirEnVendedor]&parameters=@email=\'' + email + '\''
        , onSuccess: function (Response) {
            switch (Response.type) {
                case "xml":
                    var oTarget = xdom.data.find(target);
                    if (oTarget) {
                        oTarget.setAttribute('vendedorRegistrado', (Response.document || Response).selectSingleNode('//success').textContent);
                        oTarget.setAttribute('state:active', 5);
                        xdom.dom.refresh();
                    }
                    break;
                default:
                    console.log(Response.document)
            }
        }
    })
}

var facturas = {};
facturas.cargarDatos = function (src) {
    var upload_check = new XMLHttpRequest();
    upload_check.open('GET', '../../../FilesRepository/tmp_284516.xml')
    upload_check.onreadystatechange = function (oEvent) {
        if (upload_check.readyState === 4) {
            var xml_doc = xdom.xml.createDocument(upload_check.responseText)
            var data_row = xdom.data.findById(src).selectSingleNode('..')

            var comprobante = xml_doc.selectSingleNode('//cfdi:Comprobante')
            var timbre = xml_doc.selectSingleNode('//tfd:TimbreFiscalDigital')

            console.log('Emisor: ' + comprobante.selectSingleNode('cfdi:Emisor').getAttribute('Rfc'))
            console.log('target: ' + data_row.selectSingleNode('UUID').getAttribute('x:id'))
            xdom.data.update(data_row.selectSingleNode('UUID').getAttribute('x:id'), '@x:value', timbre.getAttribute('UUID'));
            xdom.data.update(data_row.selectSingleNode('Emisor').getAttribute('x:id'), '@x:value', comprobante.selectSingleNode('cfdi:Emisor').getAttribute('Rfc'));
        }
    }
    upload_check.send();
}

var proveedores = {};
proveedores.verFormatoAlta = function () {
    var doc = xdom.data.document;
    //var pi = doc.createProcessingInstruction('xml-stylesheet', 'href="templates/FO-PG-CPR-016 Alta proveedores.xsl" type="text/xsl"');
    xdom.library.load('alta_proveedores.xslt', function () {
        var xsl_alta = xdom.library['alta_proveedores.xslt'];
        var attribute_ref = xsl_alta.selectSingleNode('//xsl:variable[@name="resources-path"]');
        xdom.xml.update(attribute_ref, window.location.origin + '/' + window.location.pathname + 'templates/')
        var stylesheet = xdom.data.document.selectNodes("processing-instruction('xml-stylesheet')[contains(.,'alta_proveedores.xslt')]")[0];
        var target = document.querySelector("div.body");
        if (stylesheet) {
            stylesheet.remove();
            if (target) {
                target.classList.remove("embed-container");
                target.parentElement.classList.add("form");
            }
            xdom.dom.refresh(null);
        } else {
            var pi = doc.createProcessingInstruction('xml-stylesheet', 'href="alta_proveedores.xslt" type="text/xsl"');
            doc.insertBefore(pi, doc.firstChild);
            var on_success = function () {
                document.querySelector("div.body").parentElement.classList.add("form");
                target.classList.add("embed-container");
                target.parentElement.classList.remove("form");
                xdom.dom.refresh(target);
            }
            if (!xdom.data.document.selectSingleNode('//DatosBancarios/px:data/px:dataRow')) {
                px.request({
                    "schema": 'Proveedores'
                    , "name": 'DatosBancarios'
                    , "mode": 'add'
                    , "on_success": function (datarow) {
                        var target = xdom.data.document.selectSingleNode('Proveedor/px:data/px:dataRow/DatosBancarios/DatosBancarios')
                        target.appendChild(datarow.document.selectSingleNode('//px:data'));
                        on_success.apply(this);
                    }
                });
            } else {
                on_success.apply(this);
            }
        }
    });
}

proveedores.validarArchivo = function (target_id, rfc, archivo) {
    xdom.data.update(xdom.data.find(target_id), "@state:validating", "true");
    xdom.xhr.Request('server/validate_file.asp?rfc=' + rfc + '&full_path=' + archivo, {
        "src": (event || {}).srcElement
        , "target": xdom.data.findById(target_id)
        , "onSuccess": function (Response, Request) {
            switch (Response.type) {
                case "xml":
                    //new xdom.xml.Document('<?xml-stylesheet type="text/xsl" href="' + Response.document.documentElement.getAttribute('controlType').toLowerCase() + '.xslt"?>' + Response.responseText, function () {
                    var caller = Request.requester;
                    xml_document = this;
                    //if (xml_document) {
                    //    xdom.xhr.cache[request];
                    //    if (cache_results) {
                    //        xdom.cache[request] = xml_document;
                    //    }
                    //}
                    xml_document.document = xdom.data.reseed(xml_document.document);
                    if (Request.target && Response.document) {
                        (Request.target.documentElement || Request.target).appendChild(Response.document.documentElement);
                    }

                    //}, Response, Request);
                    break;
                default:
                    console.log(Response.document)
            }
        }, "onComplete": function (Response, Request) {
            var target = xdom.data.find(this.target);
            target.removeAttribute("state:validating");
            xdom.dom.refresh();
        }
    }).load();
}

function fncLetra(cant) {
    if (cant == 0) {
        strng = 'CERO '
    }
    else {
        strng = ''
        temp = cant / 1000000
        temp1 = Math.floor(temp)
        cant = cant - temp1 * 1000000
        if (temp1 > 0) {
            if (temp1 > 1) {
                strng = strng + fncLetra(temp1) + 'MILLONES '
            }
            else {
                strng = strng + fncLetra(temp1) + 'MILLÓN '
            }
        }

        //con esto checamos los miles
        temp = cant / 1000
        temp1 = Math.floor(temp)
        cant = cant - temp1 * 1000
        if (temp1 > 0) {
            strng = strng + fncLetra(temp1) + 'MIL '
        }
        //con esto checamos las centenas

        temp = cant / 100
        temp1 = Math.floor(temp)
        temp2 = (cant - temp1 * 100) / 10
        cant = cant - temp1 * 100
        if (temp1 == 1) {
            if (temp2 != 0) {
                strng = strng + 'CIENTO '
            }
            else {
                strng = strng + 'CIEN '
            }
        }
        else if (temp1 == 2) {
            strng = strng + 'DOSCIENTOS '
        }
        else if (temp1 == 3) {
            strng = strng + 'TRESCIENTOS '
        }
        else if (temp1 == 4) {
            strng = strng + 'CUATROCIENTOS '
        }
        else if (temp1 == 5) {
            strng = strng + 'QUINIENTOS '
        }
        else if (temp1 == 6) {
            strng = strng + 'SEISCIENTOS '
        }
        else if (temp1 == 7) {
            strng = strng + 'SETECIENTOS '
        }
        else if (temp1 == 8) {
            strng = strng + 'OCHOCIENTOS '
        }
        else if (temp1 == 9) {
            strng = strng + 'NOVECIENTOS '
        }

        //con esto checamos las decenas
        temp = cant / 10
        temp1 = Math.floor(temp)
        temp2 = cant - temp1 * 10
        if (temp1 < 3) {
            if (temp1 == 1) {
                if (temp2 < 6) {
                    if (temp2 == 0) {
                        strng = strng + 'DIEZ '
                    }
                    else if (temp2 == 1) {
                        strng = strng + 'ONCE '
                    }
                    else if (temp2 == 2) {
                        strng = strng + 'DOCE '
                    }
                    else if (temp2 == 3) {
                        strng = strng + 'TRECE '
                    }
                    else if (temp2 == 4) {
                        strng = strng + 'CATORCE '
                    }
                    else if (temp2 == 5) {
                        strng = strng + 'QUINCE '
                    }
                    temp2 = 0
                }
                else {
                    strng = strng + 'DIECI'
                }
            }
            else if (temp1 == 2) {
                if (temp2 == 0) {
                    strng = strng + 'VEINTE '
                }
                else {
                    strng = strng + 'VEINTI'
                }
            }
        }
        else {
            if (temp1 == 3) {
                strng = strng + 'TREINTA '
            }
            else if (temp1 == 4) {
                strng = strng + 'CUARENTA '
            }
            else if (temp1 == 5) {
                strng = strng + 'CINCUENTA '
            }
            else if (temp1 == 6) {
                strng = strng + 'SESENTA '
            }
            else if (temp1 == 7) {
                strng = strng + 'SETENTA '
            }
            else if (temp1 == 8) {
                strng = strng + 'OCHENTA '
            }
            else if (temp1 == 9) {
                strng = strng + 'NOVENTA '
            }

            if (temp2 != 0) {
                strng = strng + 'Y '
            }
        }

        //con esto checamos los demás
        if (temp2 == 1) {
            strng = strng + 'UN '
        }
        else if (temp2 == 2) {
            strng = strng + 'DOS '
        }
        else if (temp2 == 3) {
            strng = strng + 'TRES '
        }
        else if (temp2 == 4) {
            strng = strng + 'CUATRO '
        }
        else if (temp2 == 5) {
            strng = strng + 'CINCO '
        }
        else if (temp2 == 6) {
            strng = strng + 'SEIS '
        }
        else if (temp2 == 7) {
            strng = strng + 'SIETE '
        }
        else if (temp2 == 8) {
            strng = strng + 'OCHO '
        }
        else if (temp2 == 9) {
            strng = strng + 'NUEVE '
        }
    }
    return strng;
}