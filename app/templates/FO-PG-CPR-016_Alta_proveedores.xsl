<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"><xsl:output method="html" indent="yes" omit-xml-declaration="yes"/><xsl:key name="data" match="Colaborador" use="'Colaborador'"/><xsl:key name="data" match="Id" use="'Id'"/><xsl:key name="data" match="Colaborador/Puesto" use="'Colaborador/Puesto'"/><xsl:key name="data" match="Fecha" use="'Fecha'"/><xsl:key name="data" match="Proveedor/Nombre" use="'Proveedor/Nombre'"/><xsl:key name="data" match="Proveedor/RFC" use="'Proveedor/RFC'"/><xsl:key name="data" match="Calle" use="'Calle'"/><xsl:key name="data" match="NumeroExterior" use="'NumeroExterior'"/><xsl:key name="data" match="NumeroInterior" use="'NumeroInterior'"/><xsl:key name="data" match="Colonia" use="'Colonia'"/><xsl:key name="data" match="CP" use="'CP'"/><xsl:key name="data" match="Telefono" use="'Telefono'"/><xsl:key name="data" match="Localidad" use="'Localidad'"/><xsl:key name="data" match="Municipio" use="'Municipio'"/><xsl:key name="data" match="EntidadFederativa" use="'EntidadFederativa'"/><xsl:key name="data" match="Email" use="'Email'"/><xsl:key name="data" match="ProductoServicio" use="'ProductoServicio'"/><xsl:key name="data" match="Contacto" use="'Contacto'"/><xsl:key name="data" match="Dias" use="'Dias'"/><xsl:key name="data" match="MontoCredito" use="'MontoCredito'"/><xsl:key name="data" match="Contacto[1]/Nombre" use="'Contacto[1]/Nombre'"/><xsl:key name="data" match="Contacto[1]/Telefono" use="'Contacto[1]/Telefono'"/><xsl:key name="data" match="Contacto[1]/Extensión" use="'Contacto[1]/Extensión'"/><xsl:key name="data" match="Contacto[1]/Puesto" use="'Contacto[1]/Puesto'"/><xsl:key name="data" match="Contacto[1]/Email" use="'Contacto[1]/Email'"/><xsl:key name="data" match="Contacto[2]/Nombre" use="'Contacto[2]/Nombre'"/><xsl:key name="data" match="Contacto[2]/Telefono" use="'Contacto[2]/Telefono'"/><xsl:key name="data" match="Contacto[2]/Extensión" use="'Contacto[2]/Extensión'"/><xsl:key name="data" match="Contacto[2]/Puesto" use="'Contacto[2]/Puesto'"/><xsl:key name="data" match="Contacto[2]/Email" use="'Contacto[2]/Email'"/><xsl:key name="data" match="Contacto[3]/Nombre" use="'Contacto[3]/Nombre'"/><xsl:key name="data" match="Contacto[3]/Telefono" use="'Contacto[3]/Telefono'"/><xsl:key name="data" match="Contacto[3]/Extensión" use="'Contacto[3]/Extensión'"/><xsl:key name="data" match="Contacto[3]/Puesto" use="'Contacto[3]/Puesto'"/><xsl:key name="data" match="Contacto[3]/Email" use="'Contacto[3]/Email'"/><xsl:key name="data" match="Contacto[4]/Nombre" use="'Contacto[4]/Nombre'"/><xsl:key name="data" match="Contacto[4]/Telefono" use="'Contacto[4]/Telefono'"/><xsl:key name="data" match="Contacto[4]/Extensión" use="'Contacto[4]/Extensión'"/><xsl:key name="data" match="Contacto[4]/Puesto" use="'Contacto[4]/Puesto'"/><xsl:key name="data" match="Contacto[4]/Email" use="'Contacto[4]/Email'"/><xsl:key name="data" match="Banco" use="'Banco'"/><xsl:key name="data" match="CuentaBancaria/Ciudad" use="'CuentaBancaria/Ciudad'"/><xsl:key name="data" match="CuentaBancaria/Estado" use="'CuentaBancaria/Estado'"/><xsl:key name="data" match="NumeroCuenta" use="'NumeroCuenta'"/><xsl:key name="data" match="Moneda" use="'Moneda'"/><xsl:key name="data" match="Sucursal/Numero" use="'Sucursal/Numero'"/><xsl:key name="data" match="Sucursal/Entidad" use="'Sucursal/Entidad'"/><xsl:key name="data" match="Sucursal/Plaza" use="'Sucursal/Plaza'"/><xsl:key name="data" match="Plaza/Entidad" use="'Plaza/Entidad'"/><xsl:key name="data" match="CLABE" use="'CLABE'"/><xsl:key name="data" match="InformacionAdicional" use="'InformacionAdicional'"/><xsl:template match="*|@*|text()" mode="container" priority="-1"/><xsl:template match="*|@*" mode="resources-path" priority="-1"/><xsl:template match="/"><html xmlns="http://www.w3.org/TR/REC-html40" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel"><head><meta http-equiv="Content-Type" content="text/html; charset=windows-1252"/><meta name="ProgId" content="Excel.Sheet"/><meta name="Generator" content="Microsoft Excel 15"/><link rel="File-List" href="FO-PG-CPR-016%20Alta%20proveedores_files/filelist.xml"><xsl:attribute name="href"><xsl:apply-templates select="." mode="resources-path"/><xsl:text>FO-PG-CPR-016%20Alta%20proveedores_files/filelist.xml</xsl:text></xsl:attribute></link><xsl:comment><xsl:text/>[if !mso]&gt;
&lt;style&gt;
v\:* {behavior:url(#default#VML);}
o\:* {behavior:url(#default#VML);}
x\:* {behavior:url(#default#VML);}
.shape {behavior:url(#default#VML);}
&lt;/style&gt;
&lt;![endif]<xsl:text/></xsl:comment><style id="FO-PG-CPR-016 Alta proveedores_4813_Styles"><xsl:comment><xsl:text/> table
	{mso-displayed-decimal-separator:"\.";
	mso-displayed-thousand-separator:"\,";}
.font54813
	{color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;}
.font64813
	{color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;}
.font74813
	{color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:italic;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;}
.xl154813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial;
	mso-generic-font-family:auto;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl644813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial;
	mso-generic-font-family:auto;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:.5pt solid windowtext;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:.5pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl654813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial;
	mso-generic-font-family:auto;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:.5pt solid windowtext;
	border-right:.5pt solid windowtext;
	border-bottom:.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl664813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial;
	mso-generic-font-family:auto;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:.5pt solid windowtext;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl674813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial;
	mso-generic-font-family:auto;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:1.0pt solid windowtext;
	border-right:none;
	border-bottom:none;
	border-left:1.0pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl684813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial;
	mso-generic-font-family:auto;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:1.0pt solid windowtext;
	border-right:none;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl694813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial;
	mso-generic-font-family:auto;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center-across;
	vertical-align:bottom;
	border-top:1.0pt solid windowtext;
	border-right:none;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl704813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial;
	mso-generic-font-family:auto;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:1.0pt solid windowtext;
	border-right:1.0pt solid windowtext;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl714813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial;
	mso-generic-font-family:auto;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:none;
	border-left:1.0pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl724813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:9.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl734813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:9.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center-across;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl744813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial;
	mso-generic-font-family:auto;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:none;
	border-right:1.0pt solid windowtext;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl754813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial;
	mso-generic-font-family:auto;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:1.0pt solid windowtext;
	border-left:1.0pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl764813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial;
	mso-generic-font-family:auto;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:1.0pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl774813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial;
	mso-generic-font-family:auto;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:none;
	border-right:1.0pt solid windowtext;
	border-bottom:1.0pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl784813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial;
	mso-generic-font-family:auto;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center-across;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl794813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial;
	mso-generic-font-family:auto;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center-across;
	vertical-align:bottom;
	border-top:none;
	border-right:1.0pt solid windowtext;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl804813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:none;
	border-left:1.0pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl814813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl824813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:none;
	border-right:1.0pt solid windowtext;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl834813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl844813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:none;
	border-right:1.0pt solid windowtext;
	border-bottom:.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl854813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:none;
	border-left:1.0pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl864813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:1.0pt solid windowtext;
	border-left:1.0pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl874813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:1.0pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl884813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:1.0pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl894813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:none;
	border-right:1.0pt solid windowtext;
	border-bottom:1.0pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl904813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:9.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center-across;
	vertical-align:bottom;
	border-top:1.0pt solid windowtext;
	border-right:none;
	border-bottom:none;
	border-left:1.0pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl914813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial;
	mso-generic-font-family:auto;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center-across;
	vertical-align:bottom;
	border-top:1.0pt solid windowtext;
	border-right:1.0pt solid windowtext;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl924813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl934813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial;
	mso-generic-font-family:auto;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center-across;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:none;
	border-left:1.0pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl944813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial;
	mso-generic-font-family:auto;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl954813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center-across;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:none;
	border-left:1.0pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl964813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center-across;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:1.0pt solid windowtext;
	border-left:1.0pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl974813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial;
	mso-generic-font-family:auto;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center-across;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:1.0pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl984813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center-across;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:1.0pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl994813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:.5pt solid windowtext;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1004813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial;
	mso-generic-font-family:auto;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:.5pt solid windowtext;
	border-right:1.0pt solid windowtext;
	border-bottom:.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1014813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:.5pt solid windowtext;
	border-right:none;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1024813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial;
	mso-generic-font-family:auto;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:.5pt solid windowtext;
	border-right:none;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1034813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:bottom;
	border-top:.5pt solid windowtext;
	border-right:none;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1044813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial;
	mso-generic-font-family:auto;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:none;
	border-left:.5pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1054813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial;
	mso-generic-font-family:auto;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:none;
	border-right:.5pt solid windowtext;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1064813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:none;
	border-left:1.0pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1074813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial;
	mso-generic-font-family:auto;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:.5pt solid windowtext;
	border-right:none;
	border-bottom:none;
	border-left:.5pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1084813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial;
	mso-generic-font-family:auto;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:.5pt solid windowtext;
	border-right:.5pt solid windowtext;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1094813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:none;
	border-left:1.0pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1104813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:italic;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:none;
	border-left:1.0pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1114813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:italic;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1124813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:italic;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:.5pt solid windowtext;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1134813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:6.0pt;
	font-weight:400;
	font-style:italic;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:.5pt solid windowtext;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1144813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:italic;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:.5pt solid windowtext;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1154813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:italic;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1164813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:"\@";
	text-align:general;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1174813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:italic;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:.5pt solid windowtext;
	border-right:none;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1184813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial;
	mso-generic-font-family:auto;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:.5pt solid windowtext;
	border-right:1.0pt solid windowtext;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1194813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial;
	mso-generic-font-family:auto;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1204813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:italic;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1214813
	{padding:0px;
	mso-ignore:padding;
	color:blue;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:underline;
	text-underline-style:single;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1224813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:12.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:"\@";
	text-align:general;
	vertical-align:bottom;
	border-top:1.0pt solid windowtext;
	border-right:none;
	border-bottom:1.0pt solid windowtext;
	border-left:none;
	background:black;
	mso-pattern:black none;
	white-space:nowrap;}
.xl1234813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:12.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:"\@";
	text-align:general;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1244813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:12.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:"\@";
	text-align:general;
	vertical-align:bottom;
	border-top:1.0pt solid windowtext;
	border-right:.5pt solid windowtext;
	border-bottom:1.0pt solid windowtext;
	border-left:.5pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1254813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:12.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:"\@";
	text-align:general;
	vertical-align:bottom;
	border-top:1.0pt solid windowtext;
	border-right:1.0pt solid windowtext;
	border-bottom:1.0pt solid windowtext;
	border-left:none;
	background:black;
	mso-pattern:black none;
	white-space:nowrap;}
.xl1264813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:11.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center-across;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1274813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:7.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1284813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border-top:1.0pt solid windowtext;
	border-right:none;
	border-bottom:none;
	border-left:1.0pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1294813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border-top:1.0pt solid windowtext;
	border-right:none;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1304813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border-top:1.0pt solid windowtext;
	border-right:1.0pt solid windowtext;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1314813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:5.0pt;
	font-weight:400;
	font-style:italic;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:middle;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:normal;}
.xl1324813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:5.0pt;
	font-weight:400;
	font-style:italic;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:middle;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1334813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:italic;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1344813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:italic;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:none;
	border-left:1.0pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1354813
	{padding:0px;
	mso-ignore:padding;
	color:blue;
	font-size:10.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:underline;
	text-underline-style:single;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1364813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial;
	mso-generic-font-family:auto;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:middle;
	border-top:1.0pt solid windowtext;
	border-right:none;
	border-bottom:1.0pt solid windowtext;
	border-left:1.0pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1374813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:22.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:middle;
	border-top:1.0pt solid windowtext;
	border-right:1.0pt solid windowtext;
	border-bottom:.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:normal;}
.xl1384813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial;
	mso-generic-font-family:auto;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border-top:1.0pt solid windowtext;
	border-right:1.0pt solid windowtext;
	border-bottom:.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1394813
	{padding:0px;
	mso-ignore:padding;
	color:#0070C0;
	font-size:10.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1404813
	{padding:0px;
	mso-ignore:padding;
	color:#0070C0;
	font-size:10.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:1.0pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1414813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:italic;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:.5pt solid windowtext;
	border-right:1.0pt solid windowtext;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1424813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:1.0pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1434813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:.5pt solid windowtext;
	border-right:1.0pt solid windowtext;
	border-bottom:.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1444813
	{padding:0px;
	mso-ignore:padding;
	color:#0070C0;
	font-size:10.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:"\@";
	text-align:general;
	vertical-align:bottom;
	border-top:.5pt solid windowtext;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1454813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:"\@";
	text-align:general;
	vertical-align:bottom;
	border-top:.5pt solid windowtext;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1464813
	{padding:0px;
	mso-ignore:padding;
	color:#0070C0;
	font-size:10.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:"\@";
	text-align:general;
	vertical-align:bottom;
	border-top:1.0pt solid windowtext;
	border-right:.5pt solid windowtext;
	border-bottom:1.0pt solid windowtext;
	border-left:1.0pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;
	mso-text-control:shrinktofit;}
.xl1474813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:none;
	border-right:1.0pt solid windowtext;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1484813
	{padding:0px;
	mso-ignore:padding;
	color:#0070C0;
	font-size:10.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:top;
	border-top:none;
	border-right:none;
	border-bottom:none;
	border-left:.5pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1494813
	{padding:0px;
	mso-ignore:padding;
	color:#0070C0;
	font-size:10.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:top;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1504813
	{padding:0px;
	mso-ignore:padding;
	color:#0070C0;
	font-size:10.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:top;
	border-top:none;
	border-right:.5pt solid windowtext;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1514813
	{padding:0px;
	mso-ignore:padding;
	color:#0070C0;
	font-size:10.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:top;
	border-top:none;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:.5pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1524813
	{padding:0px;
	mso-ignore:padding;
	color:#0070C0;
	font-size:10.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:top;
	border-top:none;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1534813
	{padding:0px;
	mso-ignore:padding;
	color:#0070C0;
	font-size:10.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:top;
	border-top:none;
	border-right:.5pt solid windowtext;
	border-bottom:.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1544813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:italic;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:.5pt solid windowtext;
	border-right:none;
	border-bottom:none;
	border-left:1.0pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1554813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:7.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:none;
	border-right:1.0pt solid windowtext;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1564813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:9.0pt;
	font-weight:400;
	font-style:italic;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:none;
	border-left:1.0pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1574813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:9.0pt;
	font-weight:400;
	font-style:italic;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1584813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:9.0pt;
	font-weight:400;
	font-style:italic;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:bottom;
	border-top:none;
	border-right:.5pt solid windowtext;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1594813
	{padding:0px;
	mso-ignore:padding;
	color:#0070C0;
	font-size:10.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1604813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:italic;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:bottom;
	border-top:.5pt solid windowtext;
	border-right:.5pt solid windowtext;
	border-bottom:.5pt solid windowtext;
	border-left:1.0pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1614813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:italic;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:bottom;
	border:.5pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1624813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:italic;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:bottom;
	border-top:.5pt solid windowtext;
	border-right:none;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1634813
	{padding:0px;
	mso-ignore:padding;
	color:#0070C0;
	font-size:10.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border-top:.5pt solid windowtext;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:.5pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;
	mso-text-control:shrinktofit;}
.xl1644813
	{padding:0px;
	mso-ignore:padding;
	color:#0070C0;
	font-size:10.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border-top:.5pt solid windowtext;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;
	mso-text-control:shrinktofit;}
.xl1654813
	{padding:0px;
	mso-ignore:padding;
	color:#0070C0;
	font-size:10.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border-top:.5pt solid windowtext;
	border-right:.5pt solid windowtext;
	border-bottom:.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;
	mso-text-control:shrinktofit;}
.xl1664813
	{padding:0px;
	mso-ignore:padding;
	color:#0070C0;
	font-size:10.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border-top:.5pt solid windowtext;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:1.0pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;
	mso-text-control:shrinktofit;}
.xl1674813
	{padding:0px;
	mso-ignore:padding;
	color:#0070C0;
	font-size:10.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:bottom;
	border-top:.5pt solid windowtext;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1684813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:middle;
	border-top:1.0pt solid windowtext;
	border-right:none;
	border-bottom:1.0pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1694813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial;
	mso-generic-font-family:auto;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:middle;
	border-top:1.0pt solid windowtext;
	border-right:none;
	border-bottom:1.0pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1704813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial;
	mso-generic-font-family:auto;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:middle;
	border-top:1.0pt solid windowtext;
	border-right:1.0pt solid windowtext;
	border-bottom:1.0pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1714813
	{padding:0px;
	mso-ignore:padding;
	color:red;
	font-size:10.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:middle;
	border-top:1.0pt solid windowtext;
	border-right:none;
	border-bottom:1.0pt solid windowtext;
	border-left:1.0pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1724813
	{padding:0px;
	mso-ignore:padding;
	color:red;
	font-size:10.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:middle;
	border-top:1.0pt solid windowtext;
	border-right:none;
	border-bottom:1.0pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1734813
	{padding:0px;
	mso-ignore:padding;
	color:red;
	font-size:10.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:middle;
	border-top:1.0pt solid windowtext;
	border-right:1.0pt solid windowtext;
	border-bottom:1.0pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1744813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:italic;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:bottom;
	border-top:1.0pt solid windowtext;
	border-right:none;
	border-bottom:none;
	border-left:1.0pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:normal;}
.xl1754813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:italic;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:bottom;
	border-top:1.0pt solid windowtext;
	border-right:none;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:normal;}
.xl1764813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial;
	mso-generic-font-family:auto;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1774813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border-top:1.0pt solid windowtext;
	border-right:none;
	border-bottom:1.0pt solid windowtext;
	border-left:1.0pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1784813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border-top:1.0pt solid windowtext;
	border-right:none;
	border-bottom:1.0pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1794813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border-top:1.0pt solid windowtext;
	border-right:1.0pt solid windowtext;
	border-bottom:1.0pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1804813
	{padding:0px;
	mso-ignore:padding;
	color:#0070C0;
	font-size:10.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:bottom;
	border-top:1.0pt solid windowtext;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:normal;}
.xl1814813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:italic;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:none;
	border-left:1.0pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1824813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:italic;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1834813
	{padding:0px;
	mso-ignore:padding;
	color:#0070C0;
	font-size:10.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:middle;
	border-top:.5pt solid windowtext;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:normal;}
.xl1844813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:italic;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border-top:1.0pt solid windowtext;
	border-right:none;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1854813
	{padding:0px;
	mso-ignore:padding;
	color:#0070C0;
	font-size:9.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:bottom;
	border-top:1.0pt solid windowtext;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1864813
	{padding:0px;
	mso-ignore:padding;
	color:#0070C0;
	font-size:10.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:1.0pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1874813
	{padding:0px;
	mso-ignore:padding;
	color:#0070C0;
	font-size:10.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1884813
	{padding:0px;
	mso-ignore:padding;
	color:#0070C0;
	font-size:10.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border-top:none;
	border-right:1.0pt solid windowtext;
	border-bottom:.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1894813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:italic;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border-top:.5pt solid windowtext;
	border-right:none;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1904813
	{padding:0px;
	mso-ignore:padding;
	color:#0070C0;
	font-size:10.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border-top:.5pt solid windowtext;
	border-right:1.0pt solid windowtext;
	border-bottom:.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;
	mso-text-control:shrinktofit;}
.xl1914813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:22.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:middle;
	border-top:1.0pt solid windowtext;
	border-right:none;
	border-bottom:1.0pt solid windowtext;
	border-left:1.0pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:normal;}
.xl1924813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:22.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:middle;
	border-top:1.0pt solid windowtext;
	border-right:none;
	border-bottom:1.0pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:normal;}
.xl1934813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:22.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:middle;
	border-top:1.0pt solid windowtext;
	border-right:1.0pt solid windowtext;
	border-bottom:1.0pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:normal;}
.xl1944813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:9.0pt;
	font-weight:700;
	font-style:italic;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:none;
	border-left:1.0pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1954813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:9.0pt;
	font-weight:700;
	font-style:italic;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1964813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:italic;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border-top:.5pt solid windowtext;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:.5pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1974813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:italic;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border-top:.5pt solid windowtext;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1984813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:6.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border-top:1.0pt solid windowtext;
	border-right:none;
	border-bottom:1.0pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl1994813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:italic;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl2004813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:7.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:underline;
	text-underline-style:single;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:none;
	border-left:1.0pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl2014813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:7.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:underline;
	text-underline-style:single;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl2024813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:7.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:underline;
	text-underline-style:single;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:bottom;
	border-top:none;
	border-right:.5pt solid windowtext;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl2034813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:italic;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:bottom;
	border-top:.5pt solid windowtext;
	border-right:1.0pt solid windowtext;
	border-bottom:.5pt solid windowtext;
	border-left:.5pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl2044813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial;
	mso-generic-font-family:auto;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border-top:.5pt solid windowtext;
	border-right:none;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl2054813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial;
	mso-generic-font-family:auto;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl2064813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial;
	mso-generic-font-family:auto;
	mso-font-charset:0;
	mso-number-format:"Short Date";
	text-align:center;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:.5pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl2074813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:top;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:normal;}
.xl2084813
	{padding:0px;
	mso-ignore:padding;
	color:#0070C0;
	font-size:10.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:"\@";
	text-align:left;
	vertical-align:bottom;
	border-top:.5pt solid windowtext;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:.5pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl2094813
	{padding:0px;
	mso-ignore:padding;
	color:#0070C0;
	font-size:10.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:"\@";
	text-align:left;
	vertical-align:bottom;
	border-top:.5pt solid windowtext;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl2104813
	{padding:0px;
	mso-ignore:padding;
	color:#0070C0;
	font-size:10.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:"\@";
	text-align:left;
	vertical-align:bottom;
	border-top:.5pt solid windowtext;
	border-right:none;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl2114813
	{padding:0px;
	mso-ignore:padding;
	color:#0070C0;
	font-size:10.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl2124813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border-top:1.0pt solid windowtext;
	border-right:none;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl2134813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:top;
	border-top:1.0pt solid windowtext;
	border-right:none;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:normal;}
.xl2144813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:top;
	border-top:1.0pt solid windowtext;
	border-right:none;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl2154813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:middle;
	border-top:none;
	border-right:none;
	border-bottom:1.0pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:normal;}
.xl2164813
	{padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:11.0pt;
	font-weight:700;
	font-style:italic;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl2174813
	{padding:0px;
	mso-ignore:padding;
	color:#0070C0;
	font-size:10.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:top;
	border-top:.5pt solid windowtext;
	border-right:none;
	border-bottom:none;
	border-left:.5pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:normal;}
.xl2184813
	{padding:0px;
	mso-ignore:padding;
	color:#0070C0;
	font-size:10.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:top;
	border-top:.5pt solid windowtext;
	border-right:none;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:normal;}
.xl2194813
	{padding:0px;
	mso-ignore:padding;
	color:#0070C0;
	font-size:10.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:top;
	border-top:.5pt solid windowtext;
	border-right:.5pt solid windowtext;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:normal;}
.xl2204813
	{padding:0px;
	mso-ignore:padding;
	color:#0070C0;
	font-size:10.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:top;
	border-top:none;
	border-right:none;
	border-bottom:none;
	border-left:.5pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:normal;}
.xl2214813
	{padding:0px;
	mso-ignore:padding;
	color:#0070C0;
	font-size:10.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:top;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:normal;}
.xl2224813
	{padding:0px;
	mso-ignore:padding;
	color:#0070C0;
	font-size:10.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:top;
	border-top:none;
	border-right:.5pt solid windowtext;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:normal;}
 <xsl:text/></xsl:comment></style></head><body><xsl:comment><xsl:text/>[if !excel]&gt;&amp;#160;&amp;#160;&lt;![endif]<xsl:text/></xsl:comment><xsl:comment><xsl:text/> The following information was generated by Microsoft Excel's Publish as Web
Page wizard. <xsl:text/></xsl:comment><xsl:comment><xsl:text/> If the same item is republished from Excel, all information between the DIV
tags will be replaced. <xsl:text/></xsl:comment><xsl:comment><xsl:text/><xsl:text/></xsl:comment><xsl:comment><xsl:text/> START OF OUTPUT FROM EXCEL PUBLISH AS WEB PAGE WIZARD  <xsl:text/></xsl:comment><xsl:comment><xsl:text/><xsl:text/></xsl:comment><div id="FO-PG-CPR-016 Alta proveedores_4813" align="center" x:publishsource="Excel"><table border="0" cellpadding="0" cellspacing="0" width="771" style="border-collapse:  collapse;table-layout:fixed;width:580pt"><col width="7" style="mso-width-source:userset;mso-width-alt:256;width:5pt"/><col width="0" style="display:none;mso-width-source:userset;mso-width-alt:512"/><col width="14" span="3" style="mso-width-source:userset;mso-width-alt:512;  width:11pt"/><col width="18" style="mso-width-source:userset;mso-width-alt:658;width:14pt"/><col width="14" span="3" style="mso-width-source:userset;mso-width-alt:512;  width:11pt"/><col width="26" style="mso-width-source:userset;mso-width-alt:950;width:20pt"/><col width="20" style="mso-width-source:userset;mso-width-alt:731;width:15pt"/><col width="13" style="mso-width-source:userset;mso-width-alt:475;width:10pt"/><col width="19" span="25" style="mso-width-source:userset;mso-width-alt:694;  width:14pt"/><col width="14" span="2" style="mso-width-source:userset;mso-width-alt:512;  width:11pt"/><col width="18" style="mso-width-source:userset;mso-width-alt:658;width:14pt"/><col width="14" span="3" style="mso-width-source:userset;mso-width-alt:512;  width:11pt"/><col width="7" style="mso-width-source:userset;mso-width-alt:256;width:5pt"/><col width="14" span="2" style="width:11pt"/><col width="5" style="mso-width-source:userset;mso-width-alt:182;width:4pt"/><tr height="10" style="mso-height-source:userset;height:7.5pt"><td height="10" class="xl674813" width="7" style="height:7.5pt;width:5pt"> </td><td class="xl684813" width="0"> </td><td class="xl684813" width="14" style="width:11pt"> </td><td class="xl684813" width="14" style="width:11pt"> </td><td class="xl684813" width="14" style="width:11pt"> </td><td class="xl684813" width="18" style="width:14pt"> </td><td class="xl684813" width="14" style="width:11pt"> </td><td class="xl684813" width="14" style="width:11pt"> </td><td class="xl684813" width="14" style="width:11pt"> </td><td class="xl684813" width="26" style="width:20pt"> </td><td class="xl684813" width="20" style="width:15pt"> </td><td class="xl684813" width="13" style="width:10pt"> </td><td class="xl684813" width="19" style="width:14pt"> </td><td class="xl684813" width="19" style="width:14pt"> </td><td class="xl684813" width="19" style="width:14pt"> </td><td class="xl684813" width="19" style="width:14pt"> </td><td class="xl684813" width="19" style="width:14pt"> </td><td class="xl684813" width="19" style="width:14pt"> </td><td class="xl684813" width="19" style="width:14pt"> </td><td class="xl684813" width="19" style="width:14pt"> </td><td class="xl684813" width="19" style="width:14pt"> </td><td class="xl684813" width="19" style="width:14pt"> </td><td class="xl684813" width="19" style="width:14pt"> </td><td class="xl684813" width="19" style="width:14pt"> </td><td class="xl684813" width="19" style="width:14pt"> </td><td class="xl684813" width="19" style="width:14pt"> </td><td class="xl684813" width="19" style="width:14pt"> </td><td class="xl684813" width="19" style="width:14pt"> </td><td class="xl684813" width="19" style="width:14pt"> </td><td class="xl684813" width="19" style="width:14pt"> </td><td class="xl684813" width="19" style="width:14pt"> </td><td class="xl684813" width="19" style="width:14pt"> </td><td class="xl684813" width="19" style="width:14pt"> </td><td class="xl684813" width="19" style="width:14pt"> </td><td class="xl684813" width="19" style="width:14pt"> </td><td class="xl684813" width="19" style="width:14pt"> </td><td class="xl684813" width="19" style="width:14pt"> </td><td class="xl684813" width="14" style="width:11pt"> </td><td class="xl684813" width="14" style="width:11pt"> </td><td class="xl684813" width="18" style="width:14pt"> </td><td class="xl684813" width="14" style="width:11pt"> </td><td class="xl684813" width="14" style="width:11pt"> </td><td class="xl684813" width="14" style="width:11pt"> </td><td class="xl684813" width="7" style="width:5pt"> </td><td class="xl684813" width="14" style="width:11pt"> </td><td class="xl684813" width="14" style="width:11pt"> </td><td class="xl704813" width="5" style="width:4pt"> </td></tr><tr height="61" style="mso-height-source:userset;height:45.75pt"><td height="61" class="xl934813" style="height:45.75pt"> </td><td class="xl1264813"/><td colspan="44" height="61" width="759" style="border-right:1.0pt solid black;   height:45.75pt;width:571pt" align="left" valign="top"><xsl:comment><xsl:text/>[if gte vml 1]&gt;&lt;v:shapetype
   id="_x0000_t75" coordsize="21600,21600" o:spt="75" o:preferrelative="t"
   path="m@4@5l@4@11@9@11@9@5xe" filled="f" stroked="f"&gt;
   &lt;v:stroke joinstyle="miter"&gt;&lt;/v:stroke&gt;
   &lt;v:formulas&gt;
    &lt;v:f eqn="if lineDrawn pixelLineWidth 0"&gt;&lt;/v:f&gt;
    &lt;v:f eqn="sum @0 1 0"&gt;&lt;/v:f&gt;
    &lt;v:f eqn="sum 0 0 @1"&gt;&lt;/v:f&gt;
    &lt;v:f eqn="prod @2 1 2"&gt;&lt;/v:f&gt;
    &lt;v:f eqn="prod @3 21600 pixelWidth"&gt;&lt;/v:f&gt;
    &lt;v:f eqn="prod @3 21600 pixelHeight"&gt;&lt;/v:f&gt;
    &lt;v:f eqn="sum @0 0 1"&gt;&lt;/v:f&gt;
    &lt;v:f eqn="prod @6 1 2"&gt;&lt;/v:f&gt;
    &lt;v:f eqn="prod @7 21600 pixelWidth"&gt;&lt;/v:f&gt;
    &lt;v:f eqn="sum @8 21600 0"&gt;&lt;/v:f&gt;
    &lt;v:f eqn="prod @7 21600 pixelHeight"&gt;&lt;/v:f&gt;
    &lt;v:f eqn="sum @10 21600 0"&gt;&lt;/v:f&gt;
   &lt;/v:formulas&gt;
   &lt;v:path o:extrusionok="f" gradientshapeok="t" o:connecttype="rect"&gt;&lt;/v:path&gt;
   &lt;o:lock v:ext="edit" aspectratio="t"&gt;&lt;/o:lock&gt;
  &lt;/v:shapetype&gt;&lt;v:shape id="Picture_x0020_3" o:spid="_x0000_s1026" type="#_x0000_t75"
   alt="Logo Petro" style='position:absolute;margin-left:21pt;margin-top:2.25pt;
   width:75pt;height:41.25pt;z-index:2;visibility:visible' o:gfxdata="UEsDBBQABgAIAAAAIQBamK3CDAEAABgCAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbJSRwU7DMAyG
70i8Q5QralM4IITW7kDhCBMaDxAlbhvROFGcle3tSdZNgokh7Rjb3+8vyWK5tSObIJBxWPPbsuIM
UDltsK/5x/qleOCMokQtR4dQ8x0QXzbXV4v1zgOxRCPVfIjRPwpBagArqXQeMHU6F6yM6Rh64aX6
lD2Iu6q6F8phBIxFzBm8WbTQyc0Y2fM2lWcTjz1nT/NcXlVzYzOf6+JPIsBIJ4j0fjRKxnQ3MaE+
8SoOTmUi9zM0GE83SfzMhtz57fRzwYF7S48ZjAa2kiG+SpvMhQ4kvFFxEyBNlf/nZFFLhes6o6Bs
A61m8ih2boF2XxhgujS9Tdg7TMd0sf/X5hsAAP//AwBQSwMEFAAGAAgAAAAhAAjDGKTUAAAAkwEA
AAsAAABfcmVscy8ucmVsc6SQwWrDMAyG74O+g9F9cdrDGKNOb4NeSwu7GltJzGLLSG7avv1M2WAZ
ve2oX+j7xL/dXeOkZmQJlAysmxYUJkc+pMHA6fj+/ApKik3eTpTQwA0Fdt3qaXvAyZZ6JGPIoiol
iYGxlPymtbgRo5WGMqa66YmjLXXkQWfrPu2AetO2L5p/M6BbMNXeG+C934A63nI1/2HH4JiE+tI4
ipr6PrhHVO3pkg44V4rlAYsBz3IPGeemPgf6sXf9T28OrpwZP6phof7Oq/nHrhdVdl8AAAD//wMA
UEsDBBQABgAIAAAAIQC7z+xnVgIAAEMGAAASAAAAZHJzL3BpY3R1cmV4bWwueG1srFTbjtMwEH1H
4h8sv7NJmia0UZPVassipAUqBB/gOk5j4Utku5f9e8Z22gBPQOjTdCY+c+acsTf3FynQiRnLtapx
dpdixBTVLVeHGn/7+vRmhZF1RLVEaMVq/MIsvm9ev9pcWlMRRXttEEAoW0Gixr1zQ5UklvZMEnun
B6ag2mkjiYO/5pC0hpwBXIpkkaZlYgfDSGt7xtw2VnATsN1ZPzIhHkKLmOqMljGiWjTLTeI5+DAc
gOBz1zXlukhvFZ8IRaPPTRbTPrzmfD1fLsviVgonAvDUzelbhyabwG9JfybL16vyr/oWRV4WE6ep
8bXdwGlsoU47Tndm7PfptDOIt2BWtgC3FJFgC3zgjoahHKOWWQpOPOuDRjvmjMbJdDLikAqwnzX9
bkfvyD84JwlX0F4/9kQd2IMdGHVA6qeUAal7765PA4noF/CPLMLfX2bbCz48cQGGksrHs9nFzfyj
vdRdxynbanqUTLm4nIYJ4uBi2J4PFiNTMblnoLz50MKcFO6FA+0Hw5Xz85HKGvoFZJjLO2I5wxzt
52J5Wh1o6nl5D4BkBB79mDT3t8kOsFz780fdwmDk6DTcRlJdOiP/Bw/QGF1qnC/TfFFg9AJ7sVi/
zYooHrs4RKG8LvNVWYK+UC+ydbYqw/JEGp7OYKx7z/RsSsgDgZugTBiTnJ7tqNG1hW+ntN/JufMH
T4WaC4POIFAB6k3MArLkjhkkuKzxKvW/qKm/fu9UGz5xhIsYwxIINfrvHR9DeBTGl0JwuANb4ojf
GL8Xv73GIRdf/+YHAAAA//8DAFBLAwQKAAAAAAAAACEAxGcX3mMMAABjDAAAFAAAAGRycy9tZWRp
YS9pbWFnZTEucG5niVBORw0KGgoAAAANSUhEUgAAAXEAAADpCAMAAADlG/cMAAAAAXNSR0IArs4c
6QAAAARnQU1BAACxjwv8YQUAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAHUwAADqYAAAOpgAABdw
nLpRPAAAAGBQTFRFd3d3mZmZgICAhoaGlpaWoKCksrKy19fXwMDA3d3dzMzM/zMz/1BQ/2Yz/2Zm
/3yA/5kz/5lm/5mZ/8wz/8xm/8yZ/8zM79bG5+fW///M6urq8fHx+Pj4//vw4+Pj////Mo37cwAA
C3VJREFUeNrt3YuSojoQBmBAFLyBh1F0gsL7v+Uh4ZYrIDoY3L+rtmoVUOejbUIS0CkQ84YDAohD
HAFxiCMgDnEExCEOcQTEIY6AOMQREIc4xBEQhzgC4hBHQBziEEdAHOIIiEMcAXGIIyAOcYgjIA5x
BMQhjvhXxAnEZ4003kN8Xu89xGf2hvjc3hCf2xvic3tDfA7vaL+H+Me8IT63N8Tn9ob43N4Qn9sb
4n/lHe/3ELfCG+Jze0P8D7yPR4jPmt/H0wnic0WWlKKnn58jxOfzpuA/J4jPEHfmvT/+0DhC/M+9
00MFfmLiJ4j/cdTeVU0ZSHKIv8G7PaFvwPuSHOKvBulOeI4/bRwh/kdRHzD5Ij6Q5BB/U0ERwXuS
HOJvSnCuiPcnOcSnB4nN4OYkh/j0inLgIY8SuDHJIT41hIoiFfG+JIf4W8D3KrgpySE+Ke7xILgp
ySH+DvCjDtyQ5BB/B7g2xQ1JDvEJIQ9kGsD1SQ7x5yMZCa5Pcog/Hel+TBE3JjnEXwY3p/jP+Qjx
lyM7jK4pl9vtBPGXY+xR8+d8LYqbppJD/MWa0uNNxU8Qfy3uh1FF/Hx9FJW4muQQfyrGNQwvj3r1
m6a5AvFnIhsD3nozcSXJIf5Cih+1DRRu/ZumTQ7x6SmuKeLnX2GDm+bEE+JPRDpQU6oGiix+gvjk
iPpryvVRaMWPEJ8YpLemXB7qFjdN7wrEpxYVEfx8020B8dciNtYUpYAL4uKxE+Lj42BKcV1B4cVP
EJ8Ud0OK6wsKLy4kOcRHR6pPcWOCc+IniL8q3qX4tW+Thzr8FkF8kniT4udH/zaqeAzxKeLHkeCF
OuAJ8Unip5HgGvEU4tPFh8ELdR4FxKeIH8eCF+o8Cog/EVKK30ZsoorfIT4+xIsIryO2eCji1jYO
rRRPePHLmC1uyoS4BOJPRMqX8ceYLVRxa8u4leJ3TrxO8cel7yRfI27tDd/tHANKuqJSOT/O/eXl
Jk/6tLeMWz3OeWqYKXhvfVHE7S3jls6eiBrxWwfe12hRxO0t45aKp7X4mT261N215vWvkvjBXnBb
Z8FFlThD/m26s8aLW1xUbBVPK3FWSM4Npnn1iyRub0vF3tnM1d3eaBnvavRtrHhsMbi14nQ6cyV+
GSF+FtvjFh83Lb5GIqVnQDdB09hYeYiXvVncGLdZvCRn4pymUfyXE7c9xW2+ujDZn25CU9tYVcQy
bneK230F7ek6TvwsiNud4nZfJZ5chKpiEhfLeGw3uOX3nrgICWzqWBHOfw4ZxF+I24Mr0ucRReVo
e02x/o42v1whN/WrCN1YNp/fL0KclZaBxiHXUjnG9v85tos/2s5a0yQKPsVja0fwF5XjNbkpxc/L
Al/GPT0f18vl17Csa6ic/lsC+OJ/84qbqWL/QfMbxB9tTTlZ3yz8CvEO/L/HUj7zosVb8NN1OR96
yeK/5/5rDiH+5gSvznzOl8XUk2WLU+/L5fq7MO5F5/hSA+IQhzgC4hBHzCZO0jY+NtMvy/l47mN8
ZEh0uvhd/KX6Q/KRriTXkcJbB+O2DDYrd1Hi6cGKO8Uq4mWs8qGt8tCnKy5KPLXkHus6cccJB8Dr
1ZYkntryyw168QHyBYoTa34PxiDubL9MPLZe3PsucTHF4zKsE++tK8sTT7g2YdMCTtOPjOy24iGL
zagkX554W1Tij0+rbMXrx0FL3jURSVDui4AMiiunUPmu3HCXv/cTTxJvmuKHz08QkcWLUC4rodc0
00NZnN8zO796uG5WyjbNht7mnYk1SXzEfXqyNI5o0Xnqs9a9BrpFTW9CNiSeieJbT3NqpIp3q/ny
nmPmO9vFux9GpquQuIqOr3qccP9PuV+vPqTSdydLuhPcOO0XL5onWLLupONpoBffco+rV9lIG27s
Fud+PYmuksqtx5S7QV79/1T49epIKKfS6Zaw0CzuClVdaKcr4itZfKNsuLFanGuujxVPxCY+f4SQ
fzdlfyBjxEsiYminy+J8iju6r8bgOdWHxfnm+lhxuV8sNoML5Ip4ztXxdqHnC7kqi4eyuKfrH7NY
PJ0grkSqLyny/lDE3a5ktwfR8riX+dyKRnG/bA3yKe6F27ap4wRWiWdNW4JHisvH96nizflUxJ3b
RuodO2XxrgBnLWTVall1cHJ7PJRqdfOiHv0uEc+R2o1WiKd7DaK8cEA8LncPaWvIQUrxat/F0v7o
xF0WXTnwu2Uia2gWD6VDQSAs9b5NPJGIiVDF67dqf+/tLos7ajtQL+4OimfiyVGuHJy/Q7wtzbHw
4pF0w8hUemuTuM+l6rPi+b8hnmpN73IVycaJswoM8V7xQmuq7I8m6eNecQZeiCV+BXGxPa68esJv
2jXA4xHiKyIcAMWAuHJL/KjvGyDvIY24Jzc5ID4gHo8R38vibh1hd6IC8T8V1/1tEIf4PyK+k6cm
QvyPxZWByn9PPO4V17UOu8G2xNA6hHifeH/7r/8MKIY4n3njxDPdykRem704Ud5JOu3vE/dNozcD
4sQ+8aazqe1fagc2kz5x0rd79P0qhdzPZezJ0nxMV4IdK16IA22BBeKJ1MN6j/e6UiAryf2xgngz
snmPhP0TS8KxtPf6xNuhHbc9DJDtGHFfWO6bdtyM4twoAv3L025UmPSJM7Ys2WvFq5fq9qY0IlHf
HiiRv1194hnXeRuG2zBcexVbWyc2QZ5rxLtdRYrc58buPibO/0psFHODwlFv3S4Z40gerez23iFJ
0+5Kl0Tali2N98YxIN3n3GjPf8zjnKG6q7jumuKD4rrxde67b2qbaMeHTeOcWd9bdbeR7BXXjMm7
wqHRIC4O7ssLPyJ+j8yE5jOeZ8TbQ+X9YNyzw+LE04oLM4IyjXjhaweVPiheEJ1Dcys2tYkt7aBk
UDy6970VN2+6X1wlr8T5slLoxJXt3gY++corjUN77ztVXFw7JZpzTqF68LOu1LfiJ6q7Q023tShX
T4HgJoBqxbn5Le9sp7wiXtzlAttNz1TFC9I1Z6K0aWUL4vzlijHpeatImJc0KF7k7axkZ7VpTz+7
ucrlg6DuWxfmAO260rMKCgvES8WEm73DTzPmZwt1T7K1o+o6W+46565f5c4mQDerCJGl9XvF8kLS
dAn2fdK8bBmGW3mVaj5+z3TrbViNcWyLwg5x9gdrJ3U/E2pP1pfHx++EAHGIQxziEIc4xCEOcYj/
vbhbX/GgGeYsTzK9bOAJiE8QZx3agVacuENP0Nj5f/EHf8m94Nrr6fOsFWfDlL5XiWvvnUboc1mX
3Vmzc0hevUT97Fvz/yvEA4/NYA6dsOsA9Ne0rGTOhj7BesliUnj0EsGdE4TlbnB933F8Oqi2ob2H
edUrRq9J9F02A92t+rlcYaAU4iyvS7Pcc2mvNxWsJ4+Ha4dKZiViSsf7aOXa0IW+x4DLsrPblHtq
S7egT6y9bblCQInDneMXxHdyutqu2L2xe/wrxNe0YodOKb6m1x+HtXhQ6q3W5X/oBICMDt+lpHxA
d0ElXurT62Lz6omMbVi+hFvle/WPsAPwxnnfLT++QbzuI89z6pK34qUeKdHL/7ADc0oH61arMmGz
SpxdnOVWq4Z063rswa0GKSrxnB0Ntm8b5fwScb/qIefFaTavndBjqGySARPfOcT3q7LturJ4WE29
1Yq/73YfX1FV6j+CF6dSATsolgWeFhQ6i4mUJWVNr/TUiWfNJcut+LqsO4SN062d990l9juOnH6Q
B1tJPKeTJ2hFdtv7WzC7quir4uWiTZ6HWSceOP62fBTmoRXjnDbFdsXadYp4SPOTytKpRQc2Xrdl
mawVZxOKVlxVyVx6YKXPbt74YXE37LkD4hCHOALiEEdAHOIIiEMc4giIQxwBcYgjIA5xiCMgDnEE
xCGOgDjEIY6AOMQREIc4AuIQR0Ac4hBH/EX8D3XOuFZQr6X8AAAAAElFTkSuQmCCUEsDBBQABgAI
AAAAIQDuamuqEAEAAIIBAAAPAAAAZHJzL2Rvd25yZXYueG1sVJBLT8MwEITvSPwHa5G4USdRTasQ
pypISFx4tEUIbiaxk4jYjmyTBH49Wx4KvXlmPbOfna1G3ZJeOt9YwyGeRUCkKWzZmIrD4+76bAnE
B2FK0VojOXxID6v8+CgTaWkHs5H9NlQES4xPBYc6hC6l1Be11MLPbCcNzpR1WgSUrqKlEwOW65Ym
UXROtWgMbqhFJ69qWbxt3zWH5sGMvboMYfdU9+Pzy+0iZvevnJ+ejOsLIEGOYbr8m74pET9OkB9f
g0fIEXFs16aorSNqI33zifw/vnJWE2eHvSaFbTnMYa/vlPIycEiWbMG+J39OBHRfGOxhLMZ9/3KM
JXOGFlYeBOmEkmcopq/LvwAAAP//AwBQSwMEFAAGAAgAAAAhAKomDr68AAAAIQEAAB0AAABkcnMv
X3JlbHMvcGljdHVyZXhtbC54bWwucmVsc4SPQWrDMBBF94XcQcw+lp1FKMWyN6HgbUgOMEhjWcQa
CUkt9e0jyCaBQJfzP/89ph///Cp+KWUXWEHXtCCIdTCOrYLr5Xv/CSIXZINrYFKwUYZx2H30Z1qx
1FFeXMyiUjgrWEqJX1JmvZDH3IRIXJs5JI+lnsnKiPqGluShbY8yPTNgeGGKyShIk+lAXLZYzf+z
wzw7TaegfzxxeaOQzld3BWKyVBR4Mg4fYddEtiCHXr48NtwBAAD//wMAUEsBAi0AFAAGAAgAAAAh
AFqYrcIMAQAAGAIAABMAAAAAAAAAAAAAAAAAAAAAAFtDb250ZW50X1R5cGVzXS54bWxQSwECLQAU
AAYACAAAACEACMMYpNQAAACTAQAACwAAAAAAAAAAAAAAAAA9AQAAX3JlbHMvLnJlbHNQSwECLQAU
AAYACAAAACEAu8/sZ1YCAABDBgAAEgAAAAAAAAAAAAAAAAA6AgAAZHJzL3BpY3R1cmV4bWwueG1s
UEsBAi0ACgAAAAAAAAAhAMRnF95jDAAAYwwAABQAAAAAAAAAAAAAAAAAwAQAAGRycy9tZWRpYS9p
bWFnZTEucG5nUEsBAi0AFAAGAAgAAAAhAO5qa6oQAQAAggEAAA8AAAAAAAAAAAAAAAAAVREAAGRy
cy9kb3ducmV2LnhtbFBLAQItABQABgAIAAAAIQCqJg6+vAAAACEBAAAdAAAAAAAAAAAAAAAAAJIS
AABkcnMvX3JlbHMvcGljdHVyZXhtbC54bWwucmVsc1BLBQYAAAAABgAGAIQBAACJEwAAAAA=
"&gt;
   &lt;v:imagedata src="FO-PG-CPR-016%20Alta%20proveedores_files/FO-PG-CPR-016%20Alta%20proveedores_4813_image001.png"
    o:title=""""&gt;&lt;/v:imagedata&gt;
   &lt;x:ClientData ObjectType="Pict"&gt;
    &lt;x:SizeWithCells&gt;&lt;/x:SizeWithCells&gt;
    &lt;x:CF&gt;Bitmap&lt;/x:CF&gt;
    &lt;x:AutoPict&gt;&lt;/x:AutoPict&gt;
   &lt;/x:ClientData&gt;
  &lt;/v:shape&gt;&lt;![endif]<xsl:text/></xsl:comment><xsl:comment><xsl:text/>[if !vml]&gt;<xsl:text/></xsl:comment><span style="mso-ignore:vglayout;   position:absolute;z-index:2;margin-left:28px;margin-top:3px;width:100px;   height:55px"><img width="100" height="55" src="FO-PG-CPR-016%20Alta%20proveedores_files/FO-PG-CPR-016%20Alta%20proveedores_4813_image002.png" alt="Logo Petro" v:shapes="Picture_x0020_3"><xsl:attribute name="src"><xsl:apply-templates select="." mode="resources-path"/><xsl:text>FO-PG-CPR-016%20Alta%20proveedores_files/FO-PG-CPR-016%20Alta%20proveedores_4813_image002.png</xsl:text></xsl:attribute></img></span><xsl:comment><xsl:text/>[endif]&gt;<xsl:text/></xsl:comment><span style="mso-ignore:vglayout2"><table cellpadding="0" cellspacing="0"><tr><td colspan="44" height="61" class="xl1914813" width="759" style="border-right:     1.0pt solid black;height:45.75pt;width:571pt">PROVEEDORES</td></tr></table></span></td><td class="xl794813"> </td></tr><tr height="48" style="mso-height-source:userset;height:36.0pt"><td height="48" class="xl934813" style="height:36.0pt"> </td><td class="xl1264813"/><td colspan="9" class="xl1744813" width="148" style="width:115pt">NOMBRE DE LA
  PERSONA DE GRUPO PETRO QUE LO ATENDIO:</td><td colspan="17" class="xl1804813" width="317" style="width:234pt"><xsl:apply-templates select="key('data','Colaborador')" mode="container"/><div xmlns="" class="Colaborador"><xsl:apply-templates select="key('data','Colaborador')"/></div></td><td class="xl1374813" width="19" style="border-top:none;width:14pt"> </td><td class="xl1364813" style="border-top:none;border-left:none"> </td><td colspan="7" class="xl1684813" style="border-right:1.0pt solid black"><span style="mso-spacerun:yes"> </span>ID PROVEEDOR</td><td colspan="9" class="xl1714813" style="border-right:1.0pt solid black;   border-left:none"><xsl:apply-templates select="key('data','Id')" mode="container"/><div xmlns="" class="Id"><xsl:apply-templates select="key('data','Id')"/></div></td><td class="xl794813"> </td></tr><tr height="21" style="mso-height-source:userset;height:15.75pt"><td height="21" class="xl934813" style="height:15.75pt"> </td><td class="xl1264813"/><td colspan="5" class="xl1814813">PUESTO:</td><td colspan="22" class="xl1834813" width="410" style="width:305pt"><xsl:apply-templates select="key('data','Colaborador/Puesto')" mode="container"/><div xmlns="" class="Colaborador/Puesto"><xsl:apply-templates select="key('data','Colaborador/Puesto')"/></div></td><td colspan="4" class="xl1844813">FECHA:</td><td colspan="12" class="xl1854813"><xsl:apply-templates select="key('data','Fecha')" mode="container"/><div xmlns="" class="Fecha"><xsl:apply-templates select="key('data','Fecha')"/></div></td><td class="xl1384813" style="border-top:none"> </td><td class="xl794813"> </td></tr><tr height="9" style="mso-height-source:userset;height:6.75pt"><td height="9" class="xl714813" style="height:6.75pt"> </td><td class="xl154813"/><td class="xl754813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl774813"> </td><td class="xl744813"> </td></tr><tr height="4" style="mso-height-source:userset;height:3.0pt"><td height="4" class="xl714813" style="height:3.0pt"> </td><td class="xl154813"/><td class="xl674813" style="border-top:none"> </td><td class="xl684813" style="border-top:none"> </td><td class="xl684813" style="border-top:none"> </td><td class="xl684813" style="border-top:none"> </td><td class="xl684813" style="border-top:none"> </td><td class="xl684813" style="border-top:none"> </td><td class="xl684813" style="border-top:none"> </td><td class="xl684813" style="border-top:none"> </td><td class="xl694813" style="border-top:none"> </td><td class="xl694813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813" style="border-top:none"> </td><td class="xl684813" style="border-top:none"> </td><td class="xl684813" style="border-top:none"> </td><td class="xl684813" style="border-top:none"> </td><td class="xl684813" style="border-top:none"> </td><td class="xl684813" style="border-top:none"> </td><td class="xl684813" style="border-top:none"> </td><td class="xl684813" style="border-top:none"> </td><td class="xl704813" style="border-top:none"> </td><td class="xl744813"> </td></tr><tr height="16" style="mso-height-source:userset;height:12.0pt"><td height="16" class="xl714813" style="height:12.0pt"> </td><td class="xl154813"/><td class="xl714813"> </td><td class="xl154813"/><td class="xl154813"/><td colspan="36" class="xl1764813">ACTUALIZACION DEL SISTEMA DE PROVEEDORES Y
  DISPERSION DE FONDOS PARA</td><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl744813"> </td><td class="xl744813"> </td></tr><tr height="8" style="mso-height-source:userset;height:6.0pt"><td height="8" class="xl714813" style="height:6.0pt"> </td><td class="xl154813"/><td class="xl714813"> </td><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td align="left" valign="top"><xsl:comment><xsl:text/>[if gte vml 1]&gt;&lt;v:shape id="TipoFormato_Alta"
   o:spid="_x0000_s1025" type="#_x0000_t75" style='position:absolute;
   margin-left:5.25pt;margin-top:.75pt;width:23.25pt;height:18.75pt;z-index:1;
   visibility:visible;mso-wrap-style:square;v-text-anchor:top' o:gfxdata="UEsDBBQABgAIAAAAIQBSPcolCgEAABQCAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbJSRz07DMAyH
70i8Q5QralM4IITW7kDhCAjGA1ip20Y0ThSHsr09yf5IMDEkjrH9/fwlWSzXdhIzBjaOanlZVlIg
adcZGmr5tnoobqTgCNTB5AhruUGWy+b8bLHaeGSRaOJajjH6W6VYj2iBS+eRUqd3wUJMxzAoD/od
BlRXVXWttKOIFIuYM2SzaLGHjymK+3Uq70w8DVLc7ebyqloam/lcV78SASc+QsD7yWiI6W5qpu7I
q9g7lYnczvBoPF8k8RMbcuen0/cFe+4pPWYwHYpnCPERbDJXXWDFI3hMM+XfKVnTcuH63mgs28Cv
mTtInQrv3CcFnP+b3SbsBedDutr+afMFAAD//wMAUEsDBBQABgAIAAAAIQAx3V9h0gAAAI8BAAAL
AAAAX3JlbHMvLnJlbHOkkMFqwzAMhu+DvYPRvXHaQxmjTm+FXksHuwpbSUxjy1gmbd++pjBYRm87
6hf6PvHv9rcwqZmyeI4G1k0LiqJl5+Ng4Ot8WH2AkoLR4cSRDNxJYN+9v+1ONGGpRzL6JKpSohgY
S0mfWosdKaA0nCjWTc85YKljHnRCe8GB9KZttzr/ZkC3YKqjM5CPbgPqfE/V/IcdvM0s3JfGctDc
996+omrH13iiuVIwD1QMuCzPMNPc1OdAv/au/+mVERN9V/5C/Eyr9cesFzV2DwAAAP//AwBQSwME
FAAGAAgAAAAhAKUbI1xCAgAAagUAABAAAABkcnMvc2hhcGV4bWwueG1srFTJbtswEL0X6D8QvDda
vEqwFBh2XRRIk6BNzwVNURYRihRIesnfd0jKchPkUNTV6WlmOO/NQi5uT61AB6YNV7LAyU2MEZNU
VVzuCvzzafNpjpGxRFZEKMkK/MIMvi0/flicKp0TSRulEaSQJgdDgRtruzyKDG1YS8yN6pgEb610
Syz86l1UaXKE5K2I0jieRqbTjFSmYcyugweXPrc9qhUTYukpgqnWqg2IKlFmi8hpcNAfAPBQ12WW
pKOLy1m8V6tjOQsnHDzbnB90TMaDy5/wmS90Vg0UZRK/z5sko3GcDr5XxL2et8RJOhm9R3ymMx1q
CdWqwBhZdrKCy2fAQYs8/Ogeda/r/vCoEa9ggEkyw0iSFkb1xDu18Z1Xv5bCEhwN0eEoyU13p+iz
6SdI/mF+LeESCNWqIXLHlhpqbNxAQYrjCyO677X6vz+FGygBbY/fVAV6yd4qKI7kp1q310pyeVRd
o5PrSZqNp2OMXgDP0kmcZk4ayaGliEJAOs3myQgjCgFpmsTz2EsPQlxgp439wtTVopBLVGBYa94Z
5mslhztjXaMuLI5xCxEbLsT/aIPL1ef5qxsKbeOUrRXdt0zacE01E8TCE2EaUI6Rzlm7ZbBv+mvl
5wyrZDWztHHiaxD+ndG+rMEBJZ7LclFCXlscOhY4m6QT30ijBK9cy1xuo3fbldDoQESBY//1I30V
ptVeVn4V3M5+7rElXAQMioXsl9it6gD7iyQ4NGhNLDlv+ptHy+97eCTL3wAAAP//AwBQSwMECgAA
AAAAAAAhAIY+7GFBDQAAQQ0AABQAAABkcnMvbWVkaWEvaW1hZ2UxLnBuZ4lQTkcNChoKAAAADUlI
RFIAAAGAAAABgAgGAAAApMe1vwAAAAFzUkdCAK7OHOkAAAAEZ0FNQQAAsY8L/GEFAAAACXBIWXMA
ADsOAAA7DgHMtqGDAAAM1klEQVR4Xu3da5FrxxUF4AshEALBEAwhEAIhEMzAEAzBEAIhEALBEBKt
tBXdHvXM1eM8+vF9Vasq5R8pW3NmbY10Tu9vAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAABj+Mslv5T/CcAqUv7/uuQ/l/wz/wCA+f31kn9fkvK/5vdLAJjYT5d8LP9rfr0EgAl9
Vf7XGAIAk0n5/3FJq/Q/xhfDAJP4+ZJHy/+af1wCwMBeKf9r/n4JAAPKu/hXy/8aQwBgMFuU/zWG
AMAgUv6tIn8nhgBA5/Yo/2vyfQIAHcrtm63i3jKGAEBnfrukVdh7xBAA6EAOdTuy/K/Jg2UAnCTl
n0PcWgW9d3KHkSEAcIIzy/8aQwDgYD2U/zWGAMBBUv5Z4NIq47NiCADsrLXIpZdkCOTfD4CN5R12
r+V/Tf798hcKABsZofyvMQQANpLyz8crrbLtNVk2bwgAvOGds/zPToYAAC8YufyvMQQAnpSjl0cv
/2tyyyoAD9hykUsvMQQAfmDPs/zPjiEA8ImZy/8aQwDggyMWufQSQwDgT2ec5X928t8MsKw8KLVi
+V/z6yUAy0n593Kc85kxBIClKP86+fIbYHrKv06ed/jbJQBTU/51cmqoJTLA9LIoJefitIpwxeS1
sDwGmN5IZ/kfEeUPLEH510n556MwgKmNuMhlzyh/YAnKv06Of1D+wPRmWOSyZXLnk/IHpjfTIpct
kid9lT8wvRkXubwT5Q8sQfnXccYPsIQVFrk8E+UPLGGlRS6PJK8HwPTyTrdVgqvGqZ7A9PLF5sqL
XD4m333k7ieAqSn/OsofWELK33HOt6T8neUPTE/511H+wBKUfx3lDywh5W+Ryy0pf1u8gOllaYmz
/G/Ja2GRCzA9i1zq2OIFLMFZ/nWUP7AE5V/HFi9gCRa51FH+wBJyW6Pyv8UKR2AJzvKvk6MulD8w
PeVfxxYvYAkWudRR/sASLHKpY4sXsASLXOoof2B6+XjDWf51rHAEpqf872ORCzC9lL/jnG/JXU/K
H5ie8q+T8neWPzA95V9H+QNLSPlb5HKL8geWYJFLHeUPLMEilzopfyscgekp/zpWOAJLsMilji1e
wBIscqmj/IElKP86tngBS8jTrMr/Flu8gCVY5FJH+QNLsMiljhWOwBKUfx1bvIAl2OJVR/kDS3CW
fx1bvIDp5R2u8q9jixcwvZS/45zr5DsQgKkp/zq55dUWL2B6yr+O8geWkPLPQ02tIlwxKX9n+QPT
s8iljvIHluAs/zrKH1iC8q+j/IElWORSJ4MwrwnA1JzlX8cil0Hlz7XctgY8RvnXUf6D+n4pRW5f
A75mkUsdW7wG1VpKYQjA5yxyqaP8B/XVueQ5vAqoOcu/ji1eg3rkQnZcK9wo/zr5zlD5D+iZpRSG
AFjk8jEWuQwqP7jWD/SrOLublTnLv47yH9Q7F7IhwGpScsq/jk8EBrTVhZzPQGEFyv8+yn9AuZC3
PJfcEGB2W//OzBCfAAxorwvZEGBWyv8+ft8HtPeFbLsPs1H+dfKwm9/zAR11Ibs4mEXOsMkTra3r
fMUo/0Gl/I+8kF0kjM5Z/nVS/s7yH9BZ6+gMAUal/Oso/0GdfSEbAozGIpc6yn9QvbyLyfnoMALl
Xyevhd/fAfV2IbuI6J1FLnXy5tEilwH1+i7GEKBX+ahS+d9ii9egen8XYwjQmzzQpPxvUf6DGuVP
WEOAXij/OrZ4DWq0P2HzMRWcKeXfujZXjfIf1IjvYvLvawhwFotc6ljhOKiR/4Q1BDjDK8uPZk6O
t1b+A5rhT1hDgKOk5JzlX8cWr0HN9PmlIcDelP99lP+gZvz80hBgLyk5xznXscVrUDO/izEE2Jry
v4/yH9Aqf8IaAmxF+d/HCscBrVL+12QI5L8ZXpXr58j9FyPEybwDWvVdTA6iMgR4xVn7L3pN3lAp
/wGtWv7XGAI8Kx8fKv9bUv7O8h+UpxUNAR6X8k/hta6jFaP8J5BHtFs/3JXijBJ+RPnXUf4TMQTK
EICWUU7BPSrKf0KGgCHAvRSd8r8lr4XbqCfltjZDgJuRD0LcI/m+zCKXieVzcEOg/DXE2pR/nfSC
8l+AIVBiCKxrpoMQt4jyX4whUGIIrMet0XXSA+6QW5AhUGIIrMMilzq59pX/wgyBEkNgbrnOneVf
R/nzP4ZAiSEwJ+V/n7weyp//y8Xg/BNDYDa5rh3nXMcWL5pyF4Db4gyBWSj/+yh/vmQIlORPZMal
/O9jixcPcShWiV+YMaX8fadVxxYvnmIIlBgCY8lfsL7LqpOH3uBpGQKtC2q1ePc0hlyvyv+WvIGz
xYu3GAIlhkDflH8d5c9mDIESf0r3KdenjytvyWvhLH82lYUZrYtttRgCfbHIpY7yZzeGQIkh0Afl
X0f5sztDoMTnq+fK66/8b1H+HMYQKDEEzpG/wJT/LfnyO9+DwGEMgRJD4Fgp/9bPYdXkgTeLXDiF
IVBiCBxD+ddR/pwunzu2Ls7VYgjsyxavOil/h7rRBe/MSvIXEdtzln8d5U93DIESQ2A7KTnlX8cW
L7rlz/QSQ+B9KTnHOdfJ66H86ZohUGIIvE7538cKR4aRI5RbF/FqMQSep/zvY4sXwzEESjyg87iU
XD7jbr2Oq8Y+CoblC7zyxKoh8GMWudxH+TM8Q8AQ+JG8Nsq/jv0TTMMQMAQ+o/zvk1uqYSqGgCHw
UV4Lh7rdktfCE+VMyxAwBK5yh5Tyv0X5swR3eRgCyr9OXgtn+bMMQ2DdIZB3ucr/FuXPkgyB8su/
0gM++XJT+d+i/FmaIVDugFlhCDgssE7K35PiLM8QmH8IKP86+Xlb5AIXKb6cb976RVkps57x7nDA
OrZ4wQeGQMlsQ8Btv3WUP3zCECjJazC6/CyVf53ZhjtszhAoGXkIKP/7KH94kCFQMuIQyM/OWf51
cpOD8ocnGAIlKY9RKP/75C8h5Q8vMARKRhgCyv8+tnjBm3LHhCdH+x4C+RkZ1HWUP2zEECjpcQjk
LCNn+dexxQs2ZgiU9DQElP99lD/sJIVjCPQxBPws7mOFI+wsxdP65VstZw4B5X8fi1zgIIZAyRlD
wCKXOnktlD8czBAoOfIzZ4tc6uS1cJY/nCTvRlu/mKvliCFgkUsd5Q8dMARK9hwCyr+O8oeOGAIl
e9yFYpFLHeUPHTIESrYcAha51En557snoEOGQEnetb8rHym1/r9XjRWOMABDoOTVIZDza5zlXyfn
HCl/GIQhUPLs/enK/z7KHwbky8uSR4eA45zvk/J3oicMyhAo+dEQUP73yVPWyh8GZwiUfDYElP99
lD9MxO2MJR+HQErOIpc6VjjChAyBkusQyBebzvKvk1tflT9Myr3tJflYTPnXUf6wAENAPibXBLAI
97rLNXucnwR0zhCQV5+WBiZgCKyZHOr26ANywMQMgbWi/IGKB6HWiLP8gaY8/dkqDZkjyh/4kiEw
Z5Q/8BBDYK4of+AphsAcydPOVjgCT3NI2tixyAV4mZMyx43yB95mCIyX/Lwc6gZswhAYJ8of2Jwh
0H9s8QJ2Ywj0mzzJrfyBXRkC/cUKR+AwKZs8XNQqIzk2tngBh8sthobAubHFCziNIXBelD9wuhwz
YAgcGyscgW4YAsfFCkegOxkCrcKSbZIBa4sX0C1DYJ8of2AIP1/SKjF5LSl/Z/kDwzAEtonyB4Zk
CLwX5Q8MzRB4LSn/vHYAQzMEnktWOFrkAkzDEHgsOWRP+QPTMQS+jvIHppYvNVvlt3pS/k70BKaX
owxaJbhqlD+wFEOgxApHYEk50bJViqvEFi9gaasOAVu8AC5Shq2SnDUWuQB8Z5UhYJELQEM+E2+V
5ixxnDPAF2YcAs7yB3jQTEPAiZ4AT5phCCh/gBeNPAQc5wzwpjwp2yrYnuM4Z4CNjDQEnOgJsLER
hoBD3QB20vMQcKgbwM56HAK/X6L8AXaWos1HLa0iPiMOdQM4UC9DwKFuACc4ewgof4ATnTUEss0M
gJMdOQQc6gbQmSOGgHN9ADqVIZAjGFrl/W6UP0DncgRDyrpV4q9G+QMMYssh4FA3gMH8dMm7QyDf
KSh/gAG9MwRS/vlOAYBBZQi0Cv6rONQNYBLPDAHlDzCZR4aAQ90AJpX9vK3iT5Q/wORaQ8ChbgCL
+H4I/JJ/AMA6MgQc6gYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAz/v27b/4/ujV
gu6f4gAAAABJRU5ErkJgglBLAwQUAAYACAAAACEA6UA+pSMBAACbAQAADwAAAGRycy9kb3ducmV2
LnhtbEyQX0/CMBTF3038Ds018U3aohuCFAJGAk+aIR+gbt2fsLZLW9nw03sZEnw89/R37rmdzjtd
k4NyvrJGAB8wIMqkNqtMIWD3uXp4BuKDNJmsrVECjsrDfHZ7M5WTzLYmUYdtKAiGGD+RAsoQmgml
Pi2Vln5gG2XQy63TMqB0Bc2cbDFc13TIWEy1rAxuKGWjXkuV7rffWkC3Wvr1wrRve/exa31gy2a9
SoS4v+sWL0CC6sL18R+9ybA+5yMg+fr45aoskT4oJwAvwvvQhBmW7uqFSUvrSJ4oX/3gRed57qwm
zrYCMCG1tYAxnPR7nnsVUEXDqDcugziORxHQU2awZxKJnuS48h/KxyzCCVoXlvOnR8ZOML026sX1
T2e/AAAA//8DAFBLAwQUAAYACAAAACEAqiYOvrwAAAAhAQAAGwAAAGRycy9fcmVscy9zaGFwZXht
bC54bWwucmVsc4SPQWrDMBBF94XcQcw+lp1FKMWyN6HgbUgOMEhjWcQaCUkt9e0jyCaBQJfzP/89
ph///Cp+KWUXWEHXtCCIdTCOrYLr5Xv/CSIXZINrYFKwUYZx2H30Z1qx1FFeXMyiUjgrWEqJX1Jm
vZDH3IRIXJs5JI+lnsnKiPqGluShbY8yPTNgeGGKyShIk+lAXLZYzf+zwzw7TaegfzxxeaOQzld3
BWKyVBR4Mg4fYddEtiCHXr48NtwBAAD//wMAUEsBAi0AFAAGAAgAAAAhAFI9yiUKAQAAFAIAABMA
AAAAAAAAAAAAAAAAAAAAAFtDb250ZW50X1R5cGVzXS54bWxQSwECLQAUAAYACAAAACEAMd1fYdIA
AACPAQAACwAAAAAAAAAAAAAAAAA7AQAAX3JlbHMvLnJlbHNQSwECLQAUAAYACAAAACEApRsjXEIC
AABqBQAAEAAAAAAAAAAAAAAAAAA2AgAAZHJzL3NoYXBleG1sLnhtbFBLAQItAAoAAAAAAAAAIQCG
PuxhQQ0AAEENAAAUAAAAAAAAAAAAAAAAAKYEAABkcnMvbWVkaWEvaW1hZ2UxLnBuZ1BLAQItABQA
BgAIAAAAIQDpQD6lIwEAAJsBAAAPAAAAAAAAAAAAAAAAABkSAABkcnMvZG93bnJldi54bWxQSwEC
LQAUAAYACAAAACEAqiYOvrwAAAAhAQAAGwAAAAAAAAAAAAAAAABpEwAAZHJzL19yZWxzL3NoYXBl
eG1sLnhtbC5yZWxzUEsFBgAAAAAGAAYAgAEAAF4UAAAAAA==" o:insetmode="auto"&gt;
   &lt;v:imagedata src="FO-PG-CPR-016%20Alta%20proveedores_files/FO-PG-CPR-016%20Alta%20proveedores_4813_image003.png"
    o:title=""""&gt;&lt;/v:imagedata&gt;
   &lt;o:lock v:ext="edit" aspectratio="f"&gt;&lt;/o:lock&gt;
   &lt;x:ClientData ObjectType="Pict"&gt;
    &lt;x:SizeWithCells&gt;&lt;/x:SizeWithCells&gt;
    &lt;x:CF&gt;Bitmap&lt;/x:CF&gt;
   &lt;/x:ClientData&gt;
  &lt;/v:shape&gt;&lt;![endif]<xsl:text/></xsl:comment><xsl:comment><xsl:text/>[if !vml]&gt;<xsl:text/></xsl:comment><span style="mso-ignore:vglayout;   position:absolute;z-index:1;margin-left:7px;margin-top:1px;width:31px;   height:25px"><img width="31" height="25" src="FO-PG-CPR-016%20Alta%20proveedores_files/FO-PG-CPR-016%20Alta%20proveedores_4813_image004.png" v:shapes="TipoFormato_Alta"><xsl:attribute name="src"><xsl:apply-templates select="." mode="resources-path"/><xsl:text>FO-PG-CPR-016%20Alta%20proveedores_files/FO-PG-CPR-016%20Alta%20proveedores_4813_image004.png</xsl:text></xsl:attribute></img></span><xsl:comment><xsl:text/>[endif]&gt;<xsl:text/></xsl:comment><span style="mso-ignore:vglayout2"><table cellpadding="0" cellspacing="0"><tr><td height="8" class="xl734813" width="26" style="height:6.0pt;width:20pt"/></tr></table></span></td><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td align="left" valign="top"><xsl:comment><xsl:text/>[if gte vml 1]&gt;&lt;v:shape id="TipoFormato_Modificacion"
   o:spid="_x0000_s1027" type="#_x0000_t75" style='position:absolute;
   margin-left:2.25pt;margin-top:.75pt;width:22.5pt;height:18pt;z-index:3;
   visibility:visible;mso-wrap-style:square;v-text-anchor:top' o:gfxdata="UEsDBBQABgAIAAAAIQDw94q7/QAAAOIBAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbJSRzUrEMBDH
74LvEOYqbaoHEWm6B6tHFV0fYEimbdg2CZlYd9/edD8u4goeZ+b/8SOpV9tpFDNFtt4puC4rEOS0
N9b1Cj7WT8UdCE7oDI7ekYIdMayay4t6vQvEIrsdKxhSCvdSsh5oQi59IJcvnY8TpjzGXgbUG+xJ
3lTVrdTeJXKpSEsGNHVLHX6OSTxu8/pAEmlkEA8H4dKlAEMYrcaUSeXszI+W4thQZudew4MNfJUx
QP7asFzOFxx9L/lpojUkXjGmZ5wyhjSRJQ8YKGvKv1MWzIkL33VWU9lGfl98J6hz4cZ/uUjzf7Pb
bHuj+ZQu9z/UfAMAAP//AwBQSwMEFAAGAAgAAAAhADHdX2HSAAAAjwEAAAsAAABfcmVscy8ucmVs
c6SQwWrDMAyG74O9g9G9cdpDGaNOb4VeSwe7CltJTGPLWCZt376mMFhGbzvqF/o+8e/2tzCpmbJ4
jgbWTQuKomXn42Dg63xYfYCSgtHhxJEM3Elg372/7U40YalHMvokqlKiGBhLSZ9aix0poDScKNZN
zzlgqWMedEJ7wYH0pm23Ov9mQLdgqqMzkI9uA+p8T9X8hx28zSzcl8Zy0Nz33r6iasfXeKK5UjAP
VAy4LM8w09zU50C/9q7/6ZURE31X/kL8TKv1x6wXNXYPAAAA//8DAFBLAwQUAAYACAAAACEA83PW
zggCAADWBAAAEAAAAGRycy9zaGFwZXhtbC54bWyklE1zmzAQhu+d6X/Q6N6AsTGGMWQyTt1LmmSm
6bmzAWE0EVpGUmzn33cliNOm7aXmtOxK+z77AevLY6/YXhgrUZd8dhFzJnSNjdS7kn9/2H5acWYd
6AYUalHyF2H5ZfXxw/rYmAJ03aFhlELbghwl75wbiiiydSd6sBc4CE3RFk0Pjl7NLmoMHCh5r6Ik
jpeRHYyAxnZCuOsxwquQ2x1wI5S6ChKjqzXYj1aNqkrydeQhvB1ukHHXtlWap/NTxDtC0OChyka3
N199Pj7LVsv4FAo3QuI3OYcnhWo+HX0vm+fpv3Qn0D904/nib7qvanZgPdQGS86ZE0enpH4ie0TR
+2/DvZmwbvf3hsmm5ClnGnoa04MccBu6jj++0jRbWUNNI+bR6cp4Hwo73GD9ZKcpwn/MsAepSRg3
HeiduDJUaOeHSvvk9cYp3U7A4e1Xekt1sMcDURI3PDukCqE4tqY/F8nnwbZlx5IvVvFiliWcvRBU
liwXcebRoKC+spoOJFkyyxec1XQgSRbLNAnoI4g/OBjrvgg8G4r5RCWn1ZaDFaFW2N9Y5xv1puIV
NW6lUuc2IVSp9Llp2KHkeZqkAdiiko2H85jW7B43yrA9qJLH4Zla99sxg8+6CTB+Nz5PtgOpRpuK
V3paFr8SJ3NaWCWFdtfgwDfKf/fvfhDBN/6Qqp8AAAD//wMAUEsDBBQABgAIAAAAIQDN5zJDIQEA
AJgBAAAPAAAAZHJzL2Rvd25yZXYueG1sfJBbT8MwDIXfkfgPkZF4Y+lalW1l2TSBJi5CTBsgxFto
3Qs0SUnC1vHrcTfQ3nj0sb9jH4+nrarZGq2rjBbQ7wXAUKcmq3Qh4OlxfjYE5rzUmayNRgFbdDCd
HB+NZZKZjV7ieuULRibaJVJA6X2TcO7SEpV0PdOgpl5urJKeSlvwzMoNmauah0FwzpWsNG0oZYOX
JaYfqy8lYHF1V728vxb4HN5/zo1sbhe1bYQ4PWlnF8A8tv4w/EvfZAJiYPn19s1W2VI6j1YAxaFw
FAwmdHFbz3RaGsvyJbrqm+Ls9dwaxazZCBgAS00tIBxBJzzkuUMvYBSH5E2dPyEcxoMYeGfqzR4l
YodGtPM/tB9FURx0LD9ctCsOD538AAAA//8DAFBLAQItABQABgAIAAAAIQDw94q7/QAAAOIBAAAT
AAAAAAAAAAAAAAAAAAAAAABbQ29udGVudF9UeXBlc10ueG1sUEsBAi0AFAAGAAgAAAAhADHdX2HS
AAAAjwEAAAsAAAAAAAAAAAAAAAAALgEAAF9yZWxzLy5yZWxzUEsBAi0AFAAGAAgAAAAhAPNz1s4I
AgAA1gQAABAAAAAAAAAAAAAAAAAAKQIAAGRycy9zaGFwZXhtbC54bWxQSwECLQAUAAYACAAAACEA
zecyQyEBAACYAQAADwAAAAAAAAAAAAAAAABfBAAAZHJzL2Rvd25yZXYueG1sUEsFBgAAAAAEAAQA
9QAAAK0FAAAAAA==" o:insetmode="auto"&gt;
   &lt;v:imagedata src="FO-PG-CPR-016%20Alta%20proveedores_files/FO-PG-CPR-016%20Alta%20proveedores_4813_image005.png"
    o:title=""""&gt;&lt;/v:imagedata&gt;
   &lt;o:lock v:ext="edit" aspectratio="f"&gt;&lt;/o:lock&gt;
   &lt;x:ClientData ObjectType="Pict"&gt;
    &lt;x:SizeWithCells&gt;&lt;/x:SizeWithCells&gt;
    &lt;x:CF&gt;Bitmap&lt;/x:CF&gt;
    &lt;x:AutoPict&gt;&lt;/x:AutoPict&gt;
   &lt;/x:ClientData&gt;
  &lt;/v:shape&gt;&lt;![endif]<xsl:text/></xsl:comment><xsl:comment><xsl:text/>[if !vml]&gt;<xsl:text/></xsl:comment><span style="mso-ignore:vglayout;   position:absolute;z-index:3;margin-left:3px;margin-top:1px;width:30px;   height:24px"><img width="30" height="24" src="FO-PG-CPR-016%20Alta%20proveedores_files/FO-PG-CPR-016%20Alta%20proveedores_4813_image006.png" v:shapes="TipoFormato_Modificacion"><xsl:attribute name="src"><xsl:apply-templates select="." mode="resources-path"/><xsl:text>FO-PG-CPR-016%20Alta%20proveedores_files/FO-PG-CPR-016%20Alta%20proveedores_4813_image006.png</xsl:text></xsl:attribute></img></span><xsl:comment><xsl:text/>[endif]&gt;<xsl:text/></xsl:comment><span style="mso-ignore:   vglayout2"><table cellpadding="0" cellspacing="0"><tr><td height="8" class="xl154813" width="19" style="height:6.0pt;width:14pt"/></tr></table></span></td><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl744813"> </td><td class="xl744813"> </td></tr><tr height="16" style="mso-height-source:userset;height:12.0pt"><td height="16" class="xl714813" style="height:12.0pt"> </td><td class="xl154813"/><td class="xl714813"> </td><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl1154813" colspan="3">ALTA</td><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl724813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td colspan="8" class="xl1994813">MODIFICACION</td><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl744813"> </td><td class="xl744813"> </td></tr><tr height="5" style="mso-height-source:userset;height:3.75pt"><td height="5" class="xl714813" style="height:3.75pt"> </td><td class="xl154813"/><td class="xl754813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl774813"> </td><td class="xl744813"> </td></tr><tr height="18" style="height:13.5pt"><td height="18" class="xl714813" style="height:13.5pt"> </td><td class="xl154813"/><td class="xl814813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl814813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl1274813"/><td class="xl1274813"/><td colspan="2" class="xl1984813"> </td><td class="xl1274813"/><td class="xl1274813"/><td class="xl1274813"/><td class="xl1274813"/><td class="xl1274813"/><td class="xl1274813"/><td class="xl1274813"/><td class="xl1554813"> </td></tr><tr height="5" style="mso-height-source:userset;height:3.75pt"><td height="5" class="xl714813" style="height:3.75pt"> </td><td class="xl154813"/><td class="xl674813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813" style="border-top:none"> </td><td class="xl684813" style="border-top:none"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl704813"> </td><td class="xl744813"> </td></tr><tr height="18" style="height:13.5pt"><td height="18" class="xl714813" style="height:13.5pt"> </td><td class="xl154813"/><td colspan="44" class="xl1774813" style="border-right:1.0pt solid black">DATOS
  DE GENERALES</td><td class="xl744813"> </td></tr><tr height="17" style="height:12.75pt"><td height="17" class="xl714813" style="height:12.75pt"> </td><td class="xl154813"/><td class="xl1284813" style="border-top:none"> </td><td class="xl1294813" style="border-top:none"> </td><td class="xl1294813" style="border-top:none"> </td><td class="xl1294813" style="border-top:none"> </td><td class="xl1294813" style="border-top:none"> </td><td class="xl1294813" style="border-top:none"> </td><td class="xl1294813" style="border-top:none"> </td><td class="xl1294813" style="border-top:none"> </td><td class="xl1294813" style="border-top:none"> </td><td class="xl1294813" style="border-top:none"> </td><td class="xl1294813" style="border-top:none"> </td><td class="xl1294813" style="border-top:none"> </td><td class="xl1294813" style="border-top:none"> </td><td class="xl1294813" style="border-top:none"> </td><td class="xl1294813" style="border-top:none"> </td><td class="xl1294813" style="border-top:none"> </td><td class="xl1294813" style="border-top:none"> </td><td class="xl1294813" style="border-top:none"> </td><td class="xl1294813" style="border-top:none"> </td><td class="xl1294813" style="border-top:none"> </td><td class="xl1294813" style="border-top:none"> </td><td class="xl1294813" style="border-top:none"> </td><td class="xl1294813" style="border-top:none"> </td><td class="xl1294813" style="border-top:none"> </td><td class="xl1294813" style="border-top:none"> </td><td class="xl1294813" style="border-top:none"> </td><td class="xl1294813" style="border-top:none"> </td><td class="xl1294813" style="border-top:none"> </td><td class="xl1294813" style="border-top:none"> </td><td class="xl1294813" style="border-top:none"> </td><td class="xl1294813" style="border-top:none"> </td><td class="xl1294813" style="border-top:none"> </td><td class="xl1294813" style="border-top:none"> </td><td class="xl1294813" style="border-top:none"> </td><td class="xl1294813" style="border-top:none"> </td><td class="xl1294813" style="border-top:none"> </td><td class="xl1294813" style="border-top:none"> </td><td class="xl1294813" style="border-top:none"> </td><td class="xl1294813" style="border-top:none"> </td><td class="xl1294813" style="border-top:none"> </td><td class="xl1294813" style="border-top:none"> </td><td class="xl1294813" style="border-top:none"> </td><td class="xl1294813" style="border-top:none"> </td><td class="xl1304813" style="border-top:none"> </td><td class="xl744813"> </td></tr><tr height="16" style="mso-height-source:userset;height:12.0pt"><td height="16" class="xl714813" style="height:12.0pt"> </td><td class="xl154813"/><td colspan="29" class="xl1864813"><xsl:apply-templates select="key('data','Proveedor/Nombre')" mode="container"/><div xmlns="" class="Proveedor/Nombre"><xsl:apply-templates select="key('data','Proveedor/Nombre')"/></div></td><td class="xl1194813"> </td><td class="xl1204813" colspan="2">RFC:</td><td class="xl1194813"> </td><td colspan="11" class="xl1874813" style="border-right:1.0pt solid black"><xsl:apply-templates select="key('data','Proveedor/RFC')" mode="container"/><div xmlns="" class="Proveedor/RFC"><xsl:apply-templates select="key('data','Proveedor/RFC')"/></div></td><td class="xl744813"> </td></tr><tr height="17" style="height:12.75pt"><td height="17" class="xl714813" style="height:12.75pt"> </td><td class="xl154813"/><td class="xl1544813" style="border-top:none"> </td><td colspan="27" class="xl1624813">APELLIDO PATERNO, MATERNO, NOMBRE(S) O<span style="mso-spacerun:yes">  </span>RAZON SOCIAL<span style="mso-spacerun:yes"> </span></td><td class="xl1174813" style="border-top:none"> </td><td class="xl1174813" style="border-top:none"> </td><td class="xl1174813" style="border-top:none"> </td><td class="xl1174813" style="border-top:none"> </td><td class="xl1174813" style="border-top:none"> </td><td class="xl1174813" style="border-top:none"> </td><td class="xl1174813" style="border-top:none"> </td><td class="xl1174813" style="border-top:none"> </td><td class="xl1174813" style="border-top:none"> </td><td class="xl1174813" style="border-top:none"> </td><td class="xl1174813" style="border-top:none"> </td><td class="xl1174813" style="border-top:none"> </td><td class="xl1174813" style="border-top:none"> </td><td class="xl1174813" style="border-top:none"> </td><td class="xl1174813" style="border-top:none"> </td><td class="xl1414813" style="border-top:none"> </td><td class="xl744813"> </td></tr><tr height="16" style="mso-height-source:userset;height:12.0pt"><td height="16" class="xl714813" style="height:12.0pt"> </td><td class="xl154813"/><td class="xl1404813"> </td><td colspan="18" class="xl1594813"><xsl:apply-templates select="key('data','Calle')" mode="container"/><div xmlns="" class="Calle"><xsl:apply-templates select="key('data','Calle')"/></div></td><td class="xl1394813" colspan="7"><xsl:apply-templates select="key('data','NumeroExterior')" mode="container"/><div xmlns="" class="NumeroExterior"><xsl:apply-templates select="key('data','NumeroExterior')"/></div></td><td class="xl834813"> </td><td class="xl834813"> </td><td class="xl834813"> </td><td class="xl834813"> </td><td class="xl834813"> </td><td class="xl834813"> </td><td class="xl1394813" colspan="8"><xsl:apply-templates select="key('data','NumeroInterior')" mode="container"/><div xmlns="" class="NumeroInterior"><xsl:apply-templates select="key('data','NumeroInterior')"/></div></td><td class="xl834813"> </td><td class="xl834813"> </td><td class="xl834813"> </td><td class="xl844813"> </td><td class="xl744813"> </td></tr><tr height="17" style="height:12.75pt"><td height="17" class="xl714813" style="height:12.75pt"> </td><td class="xl154813"/><td class="xl1544813" style="border-top:none"> </td><td colspan="17" class="xl1624813">CALLE</td><td class="xl1174813" style="border-top:none"> </td><td colspan="12" class="xl1894813">NUMERO Y/O LETRA EXTERIOR</td><td class="xl1174813" style="border-top:none"> </td><td class="xl1174813" colspan="12" style="border-right:1.0pt solid black">NUMERO
  Y/O LETRA INTERIOR</td><td class="xl744813"> </td></tr><tr height="16" style="mso-height-source:userset;height:12.0pt"><td height="16" class="xl714813" style="height:12.0pt"> </td><td class="xl154813"/><td class="xl1404813"> </td><td colspan="17" class="xl1594813"><xsl:apply-templates select="key('data','Colonia')" mode="container"/><div xmlns="" class="Colonia"><xsl:apply-templates select="key('data','Colonia')"/></div></td><td class="xl1194813"> </td><td class="xl1194813"> </td><td colspan="9" class="xl1874813"><xsl:apply-templates select="key('data','CP')" mode="container"/><div xmlns="" class="CP"><xsl:apply-templates select="key('data','CP')"/></div></td><td colspan="14" class="xl1874813"><xsl:apply-templates select="key('data','Telefono')" mode="container"/><div xmlns="" class="Telefono"><xsl:apply-templates select="key('data','Telefono')"/></div></td><td class="xl844813"> </td><td class="xl744813"> </td></tr><tr height="17" style="height:12.75pt"><td height="17" class="xl714813" style="height:12.75pt"> </td><td class="xl154813"/><td class="xl804813"> </td><td class="xl814813"/><td colspan="17" class="xl1624813">COLONIA</td><td class="xl154813"/><td colspan="9" class="xl1894813">CODIGO POSTAL</td><td class="xl1114813"/><td colspan="13" class="xl1894813">TELEFONO</td><td class="xl824813"> </td><td class="xl744813"> </td></tr><tr height="17" style="mso-height-source:userset;height:12.75pt"><td height="17" class="xl714813" style="height:12.75pt"> </td><td class="xl154813"/><td class="xl1424813"> </td><td class="xl1394813" colspan="6"><xsl:apply-templates select="key('data','Localidad')" mode="container"/><div xmlns="" class="Localidad"><xsl:apply-templates select="key('data','Localidad')"/></div></td><td class="xl834813"> </td><td class="xl834813"> </td><td class="xl834813"> </td><td class="xl834813"> </td><td class="xl834813"> </td><td class="xl834813"> </td><td class="xl834813"> </td><td class="xl834813"> </td><td class="xl1394813" colspan="5"><xsl:apply-templates select="key('data','Municipio')" mode="container"/><div xmlns="" class="Municipio"><xsl:apply-templates select="key('data','Municipio')"/></div></td><td class="xl834813"> </td><td class="xl834813"> </td><td class="xl834813"> </td><td class="xl834813"> </td><td class="xl834813"> </td><td class="xl834813"> </td><td class="xl834813"> </td><td class="xl834813"> </td><td class="xl834813"> </td><td class="xl834813"> </td><td class="xl834813"> </td><td class="xl834813"> </td><td class="xl1394813" colspan="9"><xsl:apply-templates select="key('data','EntidadFederativa')" mode="container"/><div xmlns="" class="EntidadFederativa"><xsl:apply-templates select="key('data','EntidadFederativa')"/></div></td><td class="xl834813"> </td><td class="xl834813"> </td><td class="xl844813"> </td><td class="xl744813"> </td></tr><tr height="17" style="height:12.75pt"><td height="17" class="xl714813" style="height:12.75pt"> </td><td class="xl154813"/><td class="xl804813"> </td><td class="xl814813"/><td colspan="12" class="xl1624813">LOCALIDAD</td><td class="xl1114813"/><td colspan="11" class="xl1624813">MUNICIPIO O DELEGACION</td><td class="xl1114813"/><td class="xl1114813"/><td class="xl1114813"/><td class="xl1114813"/><td class="xl1114813"/><td class="xl1114813"/><td colspan="9" class="xl1624813">ENTIDAD FEDERATIVA</td><td class="xl814813"/><td class="xl814813"/><td class="xl824813"> </td><td class="xl744813"> </td></tr><tr height="16" style="mso-height-source:userset;height:12.0pt"><td height="16" class="xl714813" style="height:12.0pt"> </td><td class="xl154813"/><td class="xl804813"> </td><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl824813"> </td><td class="xl744813"> </td></tr><tr height="17" style="mso-height-source:userset;height:12.75pt"><td height="17" class="xl714813" style="height:12.75pt"> </td><td class="xl154813"/><td class="xl1094813" colspan="24">NOMBRE DE CONTACTO QUE TENDRA ACCESO AL PORTAL
  DE PROVEEDORES:</td><td class="xl1314813" width="19" style="width:14pt"/><td class="xl1314813" width="19" style="width:14pt"/><td class="xl1314813" width="19" style="width:14pt"/><td class="xl1314813" width="19" style="width:14pt"/><td class="xl1314813" width="19" style="width:14pt"/><td class="xl1314813" width="19" style="width:14pt"/><td class="xl1314813" width="19" style="width:14pt"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl824813"> </td><td class="xl744813"> </td></tr><tr height="17" style="mso-height-source:userset;height:12.75pt"><td height="17" class="xl714813" style="height:12.75pt"> </td><td class="xl154813"/><td class="xl854813"><span style="mso-spacerun:yes"> </span></td><td colspan="3" class="xl1824813">E-MAIL:</td><td class="xl814813"/><td class="xl1214813"/><td colspan="14" class="xl1594813"><xsl:apply-templates select="key('data','Email')" mode="container"/><div xmlns="" class="Email"><xsl:apply-templates select="key('data','Email')"/></div></td><td class="xl154813"/><td class="xl1324813" colspan="7">MATERIAL Y/O SERVICIO QUE SURTE:</td><td colspan="15" class="xl2114813"><xsl:apply-templates select="key('data','ProductoServicio')" mode="container"/><div xmlns="" class="ProductoServicio"><xsl:apply-templates select="key('data','ProductoServicio')"/></div></td><td class="xl824813"> </td><td class="xl744813"> </td></tr><tr height="17" style="mso-height-source:userset;height:12.75pt"><td height="17" class="xl714813" style="height:12.75pt"> </td><td class="xl154813"/><td class="xl854813"> </td><td colspan="5" class="xl1334813">CONTACTO</td><td colspan="14" class="xl1674813"><xsl:apply-templates select="key('data','Contacto')" mode="container"/><div xmlns="" class="Contacto"><xsl:apply-templates select="key('data','Contacto')"/></div></td><td class="xl154813"/><td class="xl1114813" colspan="6">DIAS DE CREDITO:</td><td class="xl1394813" colspan="3"><xsl:apply-templates select="key('data','Dias')" mode="container"/><div xmlns="" class="Dias"><xsl:apply-templates select="key('data','Dias')"/></div></td><td class="xl1394813"> </td><td class="xl924813" colspan="3">MONTO:</td><td class="xl1394813" colspan="8"><xsl:apply-templates select="key('data','MontoCredito')" mode="container"/><div xmlns="" class="MontoCredito"><xsl:apply-templates select="key('data','MontoCredito')"/></div></td><td class="xl1194813"> </td><td class="xl844813"> </td><td class="xl744813"> </td></tr><tr height="17" style="mso-height-source:userset;height:12.75pt"><td height="17" class="xl714813" style="height:12.75pt"> </td><td class="xl154813"/><td class="xl854813"> </td><td class="xl1334813"/><td class="xl1334813"/><td class="xl1334813"/><td class="xl1334813"/><td class="xl1334813"/><td class="xl1354813"/><td class="xl1354813"/><td class="xl1354813"/><td class="xl1354813"/><td class="xl1354813"/><td class="xl1354813"/><td class="xl1354813"/><td class="xl1354813"/><td class="xl1354813"/><td class="xl1354813"/><td class="xl1354813"/><td class="xl1354813"/><td class="xl1354813"/><td class="xl1354813"/><td class="xl154813"/><td class="xl1114813"/><td class="xl154813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl924813"/><td class="xl924813"/><td class="xl924813"/><td class="xl924813"/><td class="xl924813"/><td class="xl924813"/><td class="xl924813"/><td class="xl924813"/><td class="xl924813"/><td class="xl924813"/><td class="xl924813"/><td class="xl924813"/><td class="xl824813"> </td><td class="xl744813"> </td></tr><tr height="17" style="mso-height-source:userset;height:12.75pt"><td height="17" class="xl714813" style="height:12.75pt"> </td><td class="xl154813"/><td class="xl1344813" colspan="5">CONTACTO</td><td class="xl1334813"/><td class="xl1354813"/><td class="xl1354813"/><td class="xl1354813"/><td class="xl1354813"/><td class="xl1354813"/><td class="xl1354813"/><td class="xl1354813"/><td class="xl1354813"/><td class="xl1354813"/><td class="xl1354813"/><td class="xl1354813"/><td class="xl1354813"/><td class="xl1354813"/><td class="xl1354813"/><td class="xl154813"/><td class="xl1114813"/><td class="xl154813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl924813"/><td class="xl924813"/><td class="xl924813"/><td class="xl924813"/><td class="xl924813"/><td class="xl924813"/><td class="xl924813"/><td class="xl924813"/><td class="xl924813"/><td class="xl924813"/><td class="xl924813"/><td class="xl924813"/><td class="xl824813"> </td><td class="xl744813"> </td></tr><tr height="17" style="mso-height-source:userset;height:12.75pt"><td height="17" class="xl714813" style="height:12.75pt"> </td><td class="xl154813"/><td colspan="11" class="xl1604813">NOMBRE</td><td colspan="5" class="xl1614813" style="border-left:none">TELEFONO</td><td colspan="5" class="xl1614813" style="border-left:none">EXT</td><td colspan="10" class="xl1614813" style="border-left:none">PUESTO<span style="mso-spacerun:yes"> </span></td><td colspan="13" class="xl1614813" style="border-right:1.0pt solid black;   border-left:none">EMAIL</td><td class="xl744813"> </td></tr><tr height="17" style="mso-height-source:userset;height:12.75pt"><td height="17" class="xl714813" style="height:12.75pt"> </td><td class="xl154813"/><td colspan="11" class="xl1664813" style="border-right:.5pt solid black"><xsl:apply-templates select="key('data','Contacto[1]/Nombre')" mode="container"/><div xmlns="" class="Contacto[1]/Nombre"><xsl:apply-templates select="key('data','Contacto[1]/Nombre')"/></div></td><td colspan="5" class="xl1634813" style="border-right:.5pt solid black;   border-left:none"><xsl:apply-templates select="key('data','Contacto[1]/Telefono')" mode="container"/><div xmlns="" class="Contacto[1]/Telefono"><xsl:apply-templates select="key('data','Contacto[1]/Telefono')"/></div></td><td colspan="5" class="xl1634813" style="border-right:.5pt solid black;   border-left:none"><xsl:apply-templates select="key('data','Contacto[1]/Extensión')" mode="container"/><div xmlns="" class="Contacto[1]/Extensión"><xsl:apply-templates select="key('data','Contacto[1]/Extensión')"/></div></td><td colspan="10" class="xl1634813" style="border-right:.5pt solid black;   border-left:none"><xsl:apply-templates select="key('data','Contacto[1]/Puesto')" mode="container"/><div xmlns="" class="Contacto[1]/Puesto"><xsl:apply-templates select="key('data','Contacto[1]/Puesto')"/></div></td><td colspan="13" class="xl1634813" style="border-right:1.0pt solid black;   border-left:none"><xsl:apply-templates select="key('data','Contacto[1]/Email')" mode="container"/><div xmlns="" class="Contacto[1]/Email"><xsl:apply-templates select="key('data','Contacto[1]/Email')"/></div></td><td class="xl744813"> </td></tr><tr height="17" style="mso-height-source:userset;height:12.75pt"><td height="17" class="xl714813" style="height:12.75pt"> </td><td class="xl154813"/><td colspan="11" class="xl1664813" style="border-right:.5pt solid black"><xsl:apply-templates select="key('data','Contacto[2]/Nombre')" mode="container"/><div xmlns="" class="Contacto[2]/Nombre"><xsl:apply-templates select="key('data','Contacto[2]/Nombre')"/></div></td><td colspan="5" class="xl1634813" style="border-right:.5pt solid black;   border-left:none"><xsl:apply-templates select="key('data','Contacto[2]/Telefono')" mode="container"/><div xmlns="" class="Contacto[2]/Telefono"><xsl:apply-templates select="key('data','Contacto[2]/Telefono')"/></div></td><td colspan="5" class="xl1634813" style="border-right:.5pt solid black;   border-left:none"><xsl:apply-templates select="key('data','Contacto[2]/Extensión')" mode="container"/><div xmlns="" class="Contacto[2]/Extensión"><xsl:apply-templates select="key('data','Contacto[2]/Extensión')"/></div></td><td colspan="10" class="xl1634813" style="border-right:.5pt solid black;   border-left:none"><xsl:apply-templates select="key('data','Contacto[2]/Puesto')" mode="container"/><div xmlns="" class="Contacto[2]/Puesto"><xsl:apply-templates select="key('data','Contacto[2]/Puesto')"/></div></td><td colspan="13" class="xl1634813" style="border-right:1.0pt solid black;   border-left:none"><xsl:apply-templates select="key('data','Contacto[2]/Email')" mode="container"/><div xmlns="" class="Contacto[2]/Email"><xsl:apply-templates select="key('data','Contacto[2]/Email')"/></div></td><td class="xl744813"> </td></tr><tr height="17" style="mso-height-source:userset;height:12.75pt"><td height="17" class="xl714813" style="height:12.75pt"> </td><td class="xl154813"/><td colspan="11" class="xl1664813" style="border-right:.5pt solid black"><xsl:apply-templates select="key('data','Contacto[3]/Nombre')" mode="container"/><div xmlns="" class="Contacto[3]/Nombre"><xsl:apply-templates select="key('data','Contacto[3]/Nombre')"/></div></td><td colspan="5" class="xl1634813" style="border-right:.5pt solid black;   border-left:none"><xsl:apply-templates select="key('data','Contacto[3]/Telefono')" mode="container"/><div xmlns="" class="Contacto[3]/Telefono"><xsl:apply-templates select="key('data','Contacto[3]/Telefono')"/></div></td><td colspan="5" class="xl1634813" style="border-right:.5pt solid black;   border-left:none"><xsl:apply-templates select="key('data','Contacto[3]/Extensión')" mode="container"/><div xmlns="" class="Contacto[3]/Extensión"><xsl:apply-templates select="key('data','Contacto[3]/Extensión')"/></div></td><td colspan="10" class="xl1634813" style="border-right:.5pt solid black;   border-left:none"><xsl:apply-templates select="key('data','Contacto[3]/Puesto')" mode="container"/><div xmlns="" class="Contacto[3]/Puesto"><xsl:apply-templates select="key('data','Contacto[3]/Puesto')"/></div></td><td colspan="13" class="xl1634813" style="border-right:1.0pt solid black;   border-left:none"><xsl:apply-templates select="key('data','Contacto[3]/Email')" mode="container"/><div xmlns="" class="Contacto[3]/Email"><xsl:apply-templates select="key('data','Contacto[3]/Email')"/></div></td><td class="xl744813"> </td></tr><tr height="17" style="mso-height-source:userset;height:12.75pt"><td height="17" class="xl714813" style="height:12.75pt"> </td><td class="xl154813"/><td colspan="11" class="xl1664813" style="border-right:.5pt solid black"><xsl:apply-templates select="key('data','Contacto[4]/Nombre')" mode="container"/><div xmlns="" class="Contacto[4]/Nombre"><xsl:apply-templates select="key('data','Contacto[4]/Nombre')"/></div></td><td colspan="5" class="xl1634813" style="border-right:.5pt solid black;   border-left:none"><xsl:apply-templates select="key('data','Contacto[4]/Telefono')" mode="container"/><div xmlns="" class="Contacto[4]/Telefono"><xsl:apply-templates select="key('data','Contacto[4]/Telefono')"/></div></td><td colspan="5" class="xl1634813" style="border-right:.5pt solid black;   border-left:none"><xsl:apply-templates select="key('data','Contacto[4]/Extensión')" mode="container"/><div xmlns="" class="Contacto[4]/Extensión"><xsl:apply-templates select="key('data','Contacto[4]/Extensión')"/></div></td><td colspan="10" class="xl1634813" style="border-right:.5pt solid black;   border-left:none"><xsl:apply-templates select="key('data','Contacto[4]/Puesto')" mode="container"/><div xmlns="" class="Contacto[4]/Puesto"><xsl:apply-templates select="key('data','Contacto[4]/Puesto')"/></div></td><td colspan="13" class="xl1634813" style="border-right:1.0pt solid black;   border-left:none"><xsl:apply-templates select="key('data','Contacto[4]/Email')" mode="container"/><div xmlns="" class="Contacto[4]/Email"><xsl:apply-templates select="key('data','Contacto[4]/Email')"/></div></td><td class="xl744813"> </td></tr><tr height="14" style="mso-height-source:userset;height:10.5pt"><td height="14" class="xl714813" style="height:10.5pt"> </td><td class="xl154813"/><td class="xl864813"> </td><td class="xl874813"><span style="mso-spacerun:yes"> </span></td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl894813"> </td><td class="xl744813"> </td></tr><tr height="16" style="mso-height-source:userset;height:12.0pt"><td height="16" class="xl714813" style="height:12.0pt"> </td><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl744813"> </td></tr><tr height="17" style="height:12.75pt"><td height="17" class="xl714813" style="height:12.75pt"> </td><td class="xl154813"/><td colspan="44" class="xl904813" align="center" style="border-right:1.0pt solid black">DATOS
  PARA EL PAGO ELÉCTRONICO</td><td class="xl744813"> </td></tr><tr height="19" style="mso-height-source:userset;height:14.25pt"><td height="19" class="xl714813" style="height:14.25pt"> </td><td class="xl154813"/><td colspan="10" class="xl1564813" style="border-right:.5pt solid black">NOMBRE
  DEL BANCO</td><td colspan="33" class="xl2084813" style="border-left:none"><xsl:apply-templates select="key('data','Banco')" mode="container"/><div xmlns="" class="Banco"><xsl:apply-templates select="key('data','Banco')"/></div></td><td class="xl1434813"> </td><td class="xl744813"> </td></tr><tr height="17" style="height:12.75pt"><td height="17" class="xl714813" style="height:12.75pt"> </td><td class="xl154813"/><td colspan="10" class="xl1564813" style="border-right:.5pt solid black">CIUDAD</td><td colspan="33" class="xl2084813" style="border-left:none"><xsl:apply-templates select="key('data','CuentaBancaria/Ciudad')" mode="container"/><div xmlns="" class="CuentaBancaria/Ciudad"><xsl:apply-templates select="key('data','CuentaBancaria/Ciudad')"/></div></td><td class="xl1434813" style="border-top:none"> </td><td class="xl744813"> </td></tr><tr height="17" style="height:12.75pt"><td height="17" class="xl714813" style="height:12.75pt"> </td><td class="xl154813"/><td colspan="10" class="xl1564813" style="border-right:.5pt solid black">ESTADO</td><td colspan="33" class="xl2084813" style="border-left:none"><xsl:apply-templates select="key('data','CuentaBancaria/Estado')" mode="container"/><div xmlns="" class="CuentaBancaria/Estado"><xsl:apply-templates select="key('data','CuentaBancaria/Estado')"/></div></td><td class="xl1434813" style="border-top:none"> </td><td class="xl744813"> </td></tr><tr height="17" style="height:12.75pt"><td height="17" class="xl714813" style="height:12.75pt"> </td><td class="xl154813"/><td colspan="10" class="xl2004813" style="border-right:.5pt solid black">NUMERO
  DE CUENTA (10digitos)</td><td colspan="20" class="xl2084813" style="border-left:none"><xsl:apply-templates select="key('data','NumeroCuenta')" mode="container"/><div xmlns="" class="NumeroCuenta"><xsl:apply-templates select="key('data','NumeroCuenta')"/></div></td><td class="xl1444813" style="border-top:none"> </td><td class="xl1124813" colspan="3">MONEDA:</td><td colspan="9" class="xl2104813"><xsl:apply-templates select="key('data','Moneda')" mode="container"/><div xmlns="" class="Moneda"><xsl:apply-templates select="key('data','Moneda')"/></div></td><td class="xl1004813" style="border-top:none"> </td><td class="xl744813"> </td></tr><tr height="17" style="height:12.75pt"><td height="17" class="xl714813" style="height:12.75pt"> </td><td class="xl154813"/><td colspan="8" class="xl1344813">SUCURSAL</td><td class="xl1114813"/><td class="xl814813"/><td colspan="4" class="xl1964813">NUMERO:</td><td colspan="16" class="xl1674813"><xsl:apply-templates select="key('data','Sucursal/Numero')" mode="container"/><div xmlns="" class="Sucursal/Numero"><xsl:apply-templates select="key('data','Sucursal/Numero')"/></div></td><td class="xl994813" style="border-top:none"> </td><td class="xl1124813" colspan="3">ENTIDAD:</td><td colspan="9" class="xl2104813"><xsl:apply-templates select="key('data','Sucursal/Entidad')" mode="container"/><div xmlns="" class="Sucursal/Entidad"><xsl:apply-templates select="key('data','Sucursal/Entidad')"/></div></td><td class="xl1434813" style="border-top:none"> </td><td class="xl744813"> </td></tr><tr height="16" style="mso-height-source:userset;height:12.0pt"><td height="16" class="xl714813" style="height:12.0pt"> </td><td class="xl154813"/><td colspan="8" class="xl1344813">PLAZA</td><td class="xl1114813"/><td class="xl814813"/><td colspan="4" class="xl1964813">NUMERO:</td><td colspan="16" class="xl1674813"><xsl:apply-templates select="key('data','Sucursal/Plaza')" mode="container"/><div xmlns="" class="Sucursal/Plaza"><xsl:apply-templates select="key('data','Sucursal/Plaza')"/></div></td><td class="xl1454813" style="border-top:none"> </td><td class="xl1124813" colspan="3">ENTIDAD:</td><td colspan="9" class="xl2104813"><xsl:apply-templates select="key('data','Plaza/Entidad')" mode="container"/><div xmlns="" class="Plaza/Entidad"><xsl:apply-templates select="key('data','Plaza/Entidad')"/></div></td><td class="xl1434813" style="border-top:none"> </td><td class="xl744813"> </td></tr><tr height="16" style="mso-height-source:userset;height:12.0pt"><td height="16" class="xl714813" style="height:12.0pt"> </td><td class="xl154813"/><td colspan="7" class="xl1814813">PAGO NACIONAL :<span style="mso-spacerun:yes">                              </span></td><td class="xl1114813"/><td class="xl1114813"/><td class="xl814813"/><td class="xl1114813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl1164813"/><td class="xl814813"/><td class="xl154813"/><td class="xl814813"/><td class="xl1114813"/><td class="xl814813"/><td class="xl1014813" style="border-top:none"> </td><td class="xl1014813" style="border-top:none"> </td><td class="xl1014813" style="border-top:none"> </td><td class="xl1014813" style="border-top:none"> </td><td class="xl1014813" style="border-top:none"> </td><td class="xl1014813" style="border-top:none"> </td><td class="xl1014813" style="border-top:none"> </td><td class="xl1014813" style="border-top:none"> </td><td class="xl1014813" style="border-top:none"> </td><td class="xl1014813" style="border-top:none"> </td><td class="xl1014813" style="border-top:none"> </td><td class="xl1034813" style="border-top:none"> </td><td class="xl1014813" style="border-top:none"> </td><td class="xl1174813"> </td><td class="xl1014813"> </td><td class="xl1014813"> </td><td class="xl1014813"> </td><td class="xl1014813"> </td><td class="xl1014813"> </td><td class="xl1014813"> </td><td class="xl1014813"> </td><td class="xl1014813"> </td><td class="xl1184813" style="border-top:none"> </td><td class="xl744813"> </td></tr><tr height="22" style="mso-height-source:userset;height:16.5pt"><td height="22" class="xl714813" style="height:16.5pt"> </td><td class="xl154813"/><td colspan="4" class="xl1944813">CLABE</td><td colspan="5" class="xl2164813">(18 dígitos)</td><td class="xl814813"/><td class="xl1464813"><span style="display:none"><xsl:apply-templates select="key('data','CLABE')" mode="container"/><div xmlns="" class="CLABE"><xsl:apply-templates select="key('data','CLABE')"/></div></span></td><td class="xl1244813" style="border-left:none"> </td><td class="xl1244813" style="border-left:none"> </td><td class="xl1244813" style="border-left:none"> </td><td class="xl1224813"> </td><td class="xl1244813"> </td><td class="xl1244813" style="border-left:none"> </td><td class="xl1244813" style="border-left:none"> </td><td class="xl1244813" style="border-left:none"> </td><td class="xl1224813"> </td><td class="xl1244813"> </td><td class="xl1244813" style="border-left:none"> </td><td class="xl1244813" style="border-left:none"> </td><td class="xl1244813" style="border-left:none"> </td><td class="xl1224813"> </td><td class="xl1244813"> </td><td class="xl1244813" style="border-left:none"> </td><td class="xl1244813" style="border-left:none"> </td><td class="xl1244813" style="border-left:none"> </td><td class="xl1224813"> </td><td class="xl1244813"> </td><td class="xl1244813" style="border-left:none"> </td><td class="xl1254813"> </td><td class="xl1234813"/><td class="xl1234813"/><td class="xl1234813"/><td class="xl1234813"/><td class="xl1234813"/><td class="xl1234813"/><td class="xl1234813"/><td class="xl1234813"/><td class="xl154813"/><td class="xl154813"/><td class="xl744813"> </td><td class="xl744813"> </td></tr><tr height="17" style="height:12.75pt"><td height="17" class="xl714813" style="height:12.75pt"> </td><td class="xl154813"/><td class="xl1104813"> </td><td class="xl1114813"/><td class="xl1114813"/><td class="xl1114813"/><td class="xl1114813"/><td class="xl1114813"/><td class="xl1114813"/><td class="xl1114813"/><td class="xl1114813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl154813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl924813"/><td class="xl814813"/><td class="xl154813"/><td class="xl154813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl814813"/><td class="xl154813"/><td class="xl154813"/><td class="xl824813"> </td><td class="xl744813"> </td></tr><tr height="15" style="mso-height-source:userset;height:11.25pt"><td height="15" class="xl714813" style="height:11.25pt"> </td><td class="xl154813"/><td colspan="9" class="xl1814813">INFORMACION ADICIONAL</td><td class="xl814813"/><td colspan="33" rowspan="2" class="xl2174813" width="584" style="border-right:.5pt solid black;   width:435pt"><xsl:apply-templates select="key('data','InformacionAdicional')" mode="container"/><div xmlns="" class="InformacionAdicional"><xsl:apply-templates select="key('data','InformacionAdicional')"/></div></td><td class="xl824813"> </td><td class="xl744813"> </td></tr><tr height="13" style="mso-height-source:userset;height:9.75pt"><td height="13" class="xl714813" style="height:9.75pt"> </td><td class="xl154813"/><td class="xl934813"> </td><td class="xl784813"/><td class="xl784813"/><td class="xl154813"/><td class="xl944813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl744813"> </td><td class="xl744813"> </td></tr><tr height="0" style="display:none;mso-height-source:userset;mso-height-alt:   165"><td class="xl714813"> </td><td class="xl154813"/><td class="xl934813"> </td><td class="xl784813"/><td class="xl784813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl1484813"> </td><td class="xl1494813"/><td class="xl1494813"/><td class="xl1494813"/><td class="xl1494813"/><td class="xl1494813"/><td class="xl1494813"/><td class="xl1494813"/><td class="xl1494813"/><td class="xl1494813"/><td class="xl1494813"/><td class="xl1494813"/><td class="xl1494813"/><td class="xl1494813"/><td class="xl1494813"/><td class="xl1494813"/><td class="xl1494813"/><td class="xl1494813"/><td class="xl1494813"/><td class="xl1494813"/><td class="xl1494813"/><td class="xl1494813"/><td class="xl1494813"/><td class="xl1494813"/><td class="xl1494813"/><td class="xl1494813"/><td class="xl1494813"/><td class="xl1494813"/><td class="xl1494813"/><td class="xl1494813"/><td class="xl1494813"/><td class="xl1494813"/><td class="xl1504813"> </td><td class="xl744813"> </td><td class="xl744813"> </td></tr><tr height="16" style="mso-height-source:userset;height:12.0pt"><td height="16" class="xl714813" style="height:12.0pt"> </td><td class="xl154813"/><td class="xl954813"> </td><td class="xl784813"/><td class="xl784813"/><td class="xl784813"/><td class="xl784813"/><td class="xl784813"/><td class="xl784813"/><td class="xl784813"/><td class="xl784813"/><td class="xl784813"/><td class="xl1514813"> </td><td class="xl1524813"> </td><td class="xl1524813"> </td><td class="xl1524813"> </td><td class="xl1524813"> </td><td class="xl1524813"> </td><td class="xl1524813"> </td><td class="xl1524813"> </td><td class="xl1524813"> </td><td class="xl1524813"> </td><td class="xl1524813"> </td><td class="xl1524813"> </td><td class="xl1524813"> </td><td class="xl1524813"> </td><td class="xl1524813"> </td><td class="xl1524813"> </td><td class="xl1524813"> </td><td class="xl1524813"> </td><td class="xl1524813"> </td><td class="xl1524813"> </td><td class="xl1524813"> </td><td class="xl1524813"> </td><td class="xl1524813"> </td><td class="xl1524813"> </td><td class="xl1524813"> </td><td class="xl1524813"> </td><td class="xl1524813"> </td><td class="xl1524813"> </td><td class="xl1524813"> </td><td class="xl1524813"> </td><td class="xl1524813"> </td><td class="xl1524813"> </td><td class="xl1534813"> </td><td class="xl794813"> </td><td class="xl744813"> </td></tr><tr height="12" style="mso-height-source:userset;height:9.0pt"><td height="12" class="xl714813" style="height:9.0pt"> </td><td class="xl154813"/><td class="xl964813"> </td><td class="xl974813"> </td><td class="xl974813"> </td><td class="xl974813"> </td><td class="xl974813"> </td><td class="xl974813"> </td><td class="xl974813"> </td><td class="xl974813"> </td><td class="xl974813"> </td><td class="xl974813"> </td><td class="xl974813"> </td><td class="xl974813"> </td><td class="xl974813"> </td><td class="xl884813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl874813"> </td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl884813"> </td><td class="xl894813"> </td><td class="xl824813"> </td></tr><tr height="13" style="mso-height-source:userset;height:9.75pt"><td height="13" class="xl714813" style="height:9.75pt"> </td><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl744813"> </td></tr><tr height="6" style="mso-height-source:userset;height:4.5pt"><td height="6" class="xl714813" style="height:4.5pt"> </td><td class="xl154813"/><td class="xl674813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl684813"> </td><td class="xl704813"> </td><td class="xl744813"> </td></tr><tr height="52" style="mso-height-source:userset;height:39.0pt"><td height="52" class="xl714813" style="height:39.0pt"> </td><td class="xl154813"/><td class="xl854813"> </td><td colspan="42" class="xl2074813" width="731" style="width:549pt">Por medio de la
  presente autorizamos expresamente a GRUPO PETRO<span style="mso-spacerun:yes">  </span>a que realicen el pago por los productos
  y/o servicios prestados por nosotros, mediante transferencia eléctronica de
  fondos de acuerdo a las instrucciones que aquí detallamos. Con la firma y
  sello de la presente, certificamos que la información que proporcionamos es
  válida y correcta.<span style="mso-spacerun:yes"> </span></td><td class="xl1474813"> </td><td class="xl744813"> </td></tr><tr height="9" style="mso-height-source:userset;height:6.75pt"><td height="9" class="xl714813" style="height:6.75pt"> </td><td class="xl154813"/><td class="xl1064813"> </td><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl744813"> </td><td class="xl744813"> </td></tr><tr height="17" style="height:12.75pt"><td height="17" class="xl714813" style="height:12.75pt"> </td><td class="xl154813"/><td class="xl1064813"> </td><td class="xl154813"/><td class="xl154813"/><td class="xl1074813"> </td><td class="xl1024813"> </td><td class="xl1024813"> </td><td class="xl1024813"> </td><td class="xl1024813"> </td><td class="xl1024813"> </td><td colspan="12" rowspan="3" class="xl2044813" style="border-bottom:.5pt solid black"> </td><td class="xl1024813"> </td><td class="xl1024813"> </td><td class="xl1024813"> </td><td class="xl1024813"> </td><td class="xl1024813"> </td><td class="xl1024813"> </td><td class="xl1024813"> </td><td class="xl1024813"> </td><td class="xl1024813"> </td><td class="xl1024813"> </td><td class="xl1024813"> </td><td class="xl1024813"> </td><td class="xl1024813"> </td><td class="xl1024813"> </td><td class="xl1024813"> </td><td class="xl1024813"> </td><td class="xl1024813"> </td><td class="xl1024813"> </td><td class="xl1024813"> </td><td class="xl1084813"> </td><td class="xl154813"/><td class="xl154813"/><td class="xl744813"> </td><td class="xl744813"> </td></tr><tr height="17" style="height:12.75pt"><td height="17" class="xl714813" style="height:12.75pt"> </td><td class="xl154813"/><td class="xl1064813"> </td><td class="xl154813"/><td class="xl154813"/><td class="xl1044813"> </td><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl1054813"> </td><td class="xl154813"/><td class="xl154813"/><td class="xl744813"> </td><td class="xl744813"> </td></tr><tr height="17" style="height:12.75pt"><td height="17" class="xl714813" style="height:12.75pt"> </td><td class="xl154813"/><td class="xl804813"> </td><td class="xl154813"/><td class="xl154813"/><td colspan="5" class="xl2064813"> </td><td class="xl154813"/><td class="xl154813"/><td class="xl724813"/><td colspan="15" class="xl2054813"> </td><td class="xl154813"/><td class="xl154813"/><td class="xl1054813"> </td><td class="xl154813"/><td class="xl154813"/><td class="xl744813"> </td><td class="xl744813"> </td></tr><tr height="17" style="height:12.75pt"><td height="17" class="xl714813" style="height:12.75pt"> </td><td class="xl154813"/><td class="xl714813"> </td><td class="xl154813"/><td class="xl154813"/><td class="xl644813" style="border-top:none"> </td><td class="xl1134813" colspan="3">FECHA</td><td class="xl1144813" style="border-top:none"> </td><td class="xl1144813"> </td><td class="xl1144813" style="border-top:none"> </td><td class="xl1134813" colspan="6">SELLO DE LA COMPAÑIA</td><td class="xl1144813" style="border-top:none"> </td><td class="xl1144813" style="border-top:none"> </td><td class="xl1144813" style="border-top:none"> </td><td class="xl1144813" style="border-top:none"> </td><td class="xl1144813" style="border-top:none"> </td><td class="xl1144813"> </td><td class="xl1144813"> </td><td class="xl1134813" colspan="11">NOMBRE Y FIRMA DE LA PERSONA AUTORIZADA</td><td class="xl664813" style="border-top:none"> </td><td class="xl664813" style="border-top:none"> </td><td class="xl664813" style="border-top:none"> </td><td class="xl664813" style="border-top:none"> </td><td class="xl664813"> </td><td class="xl664813"> </td><td class="xl654813"> </td><td class="xl154813"/><td class="xl154813"/><td class="xl744813"> </td><td class="xl744813"> </td></tr><tr height="18" style="mso-height-source:userset;height:13.5pt"><td height="18" class="xl714813" style="height:13.5pt"> </td><td class="xl154813"/><td class="xl754813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl984813"> </td><td class="xl974813"> </td><td class="xl974813"> </td><td class="xl974813"> </td><td class="xl974813"> </td><td class="xl974813"> </td><td class="xl974813"> </td><td class="xl974813"> </td><td class="xl974813"> </td><td class="xl974813"> </td><td class="xl974813"> </td><td class="xl974813"> </td><td class="xl974813"> </td><td class="xl974813"> </td><td class="xl974813"> </td><td class="xl974813"> </td><td class="xl974813"> </td><td class="xl974813"> </td><td class="xl974813"> </td><td class="xl974813"> </td><td class="xl974813"> </td><td class="xl974813"> </td><td class="xl974813"> </td><td class="xl974813"> </td><td class="xl974813"> </td><td class="xl974813"> </td><td class="xl974813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl774813"> </td><td class="xl744813"> </td></tr><tr height="8" style="mso-height-source:userset;height:6.0pt"><td height="8" class="xl714813" style="height:6.0pt"> </td><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl744813"> </td></tr><tr height="6" style="mso-height-source:userset;height:4.5pt"><td height="6" class="xl754813" style="height:4.5pt"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl764813"> </td><td class="xl774813"> </td></tr><tr height="6" style="mso-height-source:userset;height:4.5pt"><td height="6" class="xl154813" style="height:4.5pt"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/></tr><tr height="38" style="mso-height-source:userset;height:28.5pt"><td height="38" class="xl674813" style="height:28.5pt"> </td><td class="xl684813"> </td><td colspan="44" class="xl2134813" width="759" style="width:571pt"><span style="mso-spacerun:yes"> </span><font class="font64813">Anexar la siguiente
  documentacion</font><font class="font54813">: RFC, opinion positiva por parte
  del SAT del mes vigente, Comprobante de domicilio de la empresa (no mayor a 3
  meses de antigüedad) y caratula del banco (no mayor a 3 meses de antigüedad).</font></td><td class="xl704813"> </td></tr><tr height="73" style="mso-height-source:userset;height:54.75pt"><td height="73" class="xl754813" style="height:54.75pt"> </td><td class="xl764813"> </td><td colspan="44" class="xl2154813" width="759" style="width:571pt"><font class="font64813">APLICA SOLAMENTE PARA FLIMSA:</font><font class="font54813">
  Contrato y/o acuerdo de confidencialidad, politicas de seguridad patrimonial
  (cuando aplique), referencias comerciales, datos del representante legal,
  número de certificación en programas de seguridad para proveedores críticos
  (cuando aplique), convenio de cumplimiento de requisitos de seguridad física
  y carta compromiso del cumplimiento de requisitos de seguridad física dentro
  de las instalaciones (FO-FL-CPR-005) (cuando aplique).</font></td><td class="xl774813"> </td></tr><tr height="17" style="height:12.75pt"><td height="17" class="xl154813" style="height:12.75pt"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td class="xl154813"/><td colspan="13" class="xl2124813"><span style="mso-spacerun:yes"> </span>FO-CO-TES-004/REV00/NOV´16</td><td class="xl154813"/></tr><xsl:comment><xsl:text/>[if supportMisalignedColumns]&gt;<xsl:text/></xsl:comment><tr height="0" style="display:none"><td width="7" style="width:5pt"/><td width="0"/><td width="14" style="width:11pt"/><td width="14" style="width:11pt"/><td width="14" style="width:11pt"/><td width="18" style="width:14pt"/><td width="14" style="width:11pt"/><td width="14" style="width:11pt"/><td width="14" style="width:11pt"/><td width="26" style="width:20pt"/><td width="20" style="width:15pt"/><td width="13" style="width:10pt"/><td width="19" style="width:14pt"/><td width="19" style="width:14pt"/><td width="19" style="width:14pt"/><td width="19" style="width:14pt"/><td width="19" style="width:14pt"/><td width="19" style="width:14pt"/><td width="19" style="width:14pt"/><td width="19" style="width:14pt"/><td width="19" style="width:14pt"/><td width="19" style="width:14pt"/><td width="19" style="width:14pt"/><td width="19" style="width:14pt"/><td width="19" style="width:14pt"/><td width="19" style="width:14pt"/><td width="19" style="width:14pt"/><td width="19" style="width:14pt"/><td width="19" style="width:14pt"/><td width="19" style="width:14pt"/><td width="19" style="width:14pt"/><td width="19" style="width:14pt"/><td width="19" style="width:14pt"/><td width="19" style="width:14pt"/><td width="19" style="width:14pt"/><td width="19" style="width:14pt"/><td width="19" style="width:14pt"/><td width="14" style="width:11pt"/><td width="14" style="width:11pt"/><td width="18" style="width:14pt"/><td width="14" style="width:11pt"/><td width="14" style="width:11pt"/><td width="14" style="width:11pt"/><td width="7" style="width:5pt"/><td width="14" style="width:11pt"/><td width="14" style="width:11pt"/><td width="5" style="width:4pt"/></tr><xsl:comment><xsl:text/>[endif]&gt;<xsl:text/></xsl:comment></table></div><xsl:comment><xsl:text/><xsl:text/></xsl:comment><xsl:comment><xsl:text/> END OF OUTPUT FROM EXCEL PUBLISH AS WEB PAGE WIZARD <xsl:text/></xsl:comment><xsl:comment><xsl:text/><xsl:text/></xsl:comment></body></html></xsl:template></xsl:stylesheet>