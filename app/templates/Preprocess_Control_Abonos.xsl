<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:msxsl="urn:schemas-microsoft-com:xslt"
	xmlns:math="http://exslt.org/math"
	xmlns:exsl="http://exslt.org/functions"
	xmlns:set="http://exslt.org/sets"
	xmlns:session="urn:session"
	extension-element-prefixes="math exsl msxsl"
	exclude-result-prefixes="set"
  xmlns="http://www.w3.org/1999/xhtml"
	>
  <xsl:strip-space elements="*"/>
  <xsl:output method="html"
	 omit-xml-declaration="yes"
	 standalone="yes"
	 indent="yes"
	 media-type="string"/>

  <xsl:decimal-format
		name="money"
		grouping-separator=","
		decimal-separator="."/>

  <xsl:key name="Pivot.Base" match="/ControlDeAbonos/CentrosDeCostos/CentroDeCostos[@Id=/ControlDeAbonos/Ubicaciones/Ubicacion/@IdFracc]" use="'filtered'" />

  <xsl:key name="Prospectos" match="Prospectos/*" use="concat(@IdFracc,'_all')" />
  <xsl:key name="Total_Prospectos" match="Prospectos/*[@IdFracc=/ControlDeAbonos/CentrosDeCostos/CentroDeCostos/@Id]" use="'all'" />

  <xsl:key name="Status" match="Status//Status" use="generate-id(../..)" />
  <xsl:key name="Creditos" match="/ControlDeAbonos/Creditos/*" use="@Id" />

  <xsl:key name="Gestoria" match="Ubicaciones/Ubicacion[@status='autorizacion_ingreso_promesa']" use="concat(@IdFracc,'_gestoria')" />
  <xsl:key name="Gestoria" match="Ubicaciones/Ubicacion[@status='carta_autorizacion_recursos']" use="concat(@IdFracc,'_gestoria')" />
  <xsl:key name="Gestoria" match="Ubicaciones/Ubicacion[@status='dictamen_juridico']" use="concat(@IdFracc,'_gestoria')" />
  <xsl:key name="Gestoria" match="Ubicaciones/Ubicacion[@status='instruccion_notarial']" use="concat(@IdFracc,'_gestoria')" />
  <xsl:key name="Gestoria" match="Ubicaciones/Ubicacion[@status='notaria_firma']" use="concat(@IdFracc,'_gestoria')" />
  <xsl:key name="Gestoria" match="Ubicaciones/Ubicacion[@status='reportado']" use="concat(@IdFracc,'_gestoria')" />

  <xsl:key name="Total_Gestoria" match="Ubicaciones/Ubicacion[@añoOperacion=number(/ControlDeAbonos/@año)][@IdFracc=/ControlDeAbonos/CentrosDeCostos/CentroDeCostos/@Id][@status='autorizacion_ingreso_promesa']" use="'gestoria'" />
  <xsl:key name="Total_Gestoria" match="Ubicaciones/Ubicacion[@añoOperacion=number(/ControlDeAbonos/@año)][@IdFracc=/ControlDeAbonos/CentrosDeCostos/CentroDeCostos/@Id][@status='carta_autorizacion_recursos']" use="'gestoria'" />
  <xsl:key name="Total_Gestoria" match="Ubicaciones/Ubicacion[@añoOperacion=number(/ControlDeAbonos/@año)][@IdFracc=/ControlDeAbonos/CentrosDeCostos/CentroDeCostos/@Id][@status='dictamen_juridico']" use="'gestoria'" />
  <xsl:key name="Total_Gestoria" match="Ubicaciones/Ubicacion[@añoOperacion=number(/ControlDeAbonos/@año)][@IdFracc=/ControlDeAbonos/CentrosDeCostos/CentroDeCostos/@Id][@status='instruccion_notarial']" use="'gestoria'" />
  <xsl:key name="Total_Gestoria" match="Ubicaciones/Ubicacion[@añoOperacion=number(/ControlDeAbonos/@año)][@IdFracc=/ControlDeAbonos/CentrosDeCostos/CentroDeCostos/@Id][@status='notaria_firma']" use="'gestoria'" />
  <xsl:key name="Total_Gestoria" match="Ubicaciones/Ubicacion[@añoOperacion=number(/ControlDeAbonos/@año)][@IdFracc=/ControlDeAbonos/CentrosDeCostos/CentroDeCostos/@Id][@status='reportado']" use="'gestoria'" />

  <xsl:key name="X_Liberar" match="Ubicaciones/Ubicacion[@xLiberar=1]" use="concat(@IdFracc,'all')" />
  <xsl:key name="Total_X_Liberar" match="Ubicaciones/Ubicacion[@xLiberar=1]" use="'all'" />

  <xsl:key name="X_Entregar" match="Ubicaciones/Ubicacion[@xEntregar=1]" use="'all'" />
  <xsl:key name="Total_X_Entregar" match="Ubicaciones/Ubicacion[@xEntregar=1]" use="'all'" />

  <xsl:key name="Ubicaciones" match="Ubicaciones/Ubicacion" use="concat(@IdFracc,'_',@tipo,'_',@status,'_',@avance)" />
  <xsl:key name="Ubicaciones" match="Ubicaciones/Ubicacion" use="concat(@IdFracc,'_',@tipo,'_',@status,'_',@comportamientoCredito)" />
  <xsl:key name="Ubicaciones" match="Ubicaciones/Ubicacion" use="concat(@IdFracc,'_',@tipo,'_',@status)" />

  <xsl:key name="Total_Ubicaciones" match="Ubicaciones/Ubicacion[@añoOperacion=number(/ControlDeAbonos/@año)][@IdFracc=/ControlDeAbonos/CentrosDeCostos/CentroDeCostos/@Id]" use="concat(@tipo,'_',@status,'_',@avance)" />
  <xsl:key name="Total_Ubicaciones" match="Ubicaciones/Ubicacion[@añoOperacion=number(/ControlDeAbonos/@año)][@IdFracc=/ControlDeAbonos/CentrosDeCostos/CentroDeCostos/@Id]" use="concat(@tipo,'_',@status,'_',@comportamientoCredito)" />
  <xsl:key name="Total_Ubicaciones" match="Ubicaciones/Ubicacion[@añoOperacion=number(/ControlDeAbonos/@año)][@IdFracc=/ControlDeAbonos/CentrosDeCostos/CentroDeCostos/@Id]" use="concat(@tipo,'_',@status)" />


  <xsl:key name="Ubicaciones_Presentes" match="Ubicaciones/Ubicacion[@IdFracc=/ControlDeAbonos/CentrosDeCostos/CentroDeCostos/@Id]" use="'all'" />
  <xsl:key name="Ubicaciones_No_Presentes" match="Ubicaciones/Ubicacion[not(@IdFracc=/ControlDeAbonos/CentrosDeCostos/CentroDeCostos/@Id)]" use="'all'" />

  <xsl:template match="/">
    <html>
      <head>
        <title>Promociones Habi</title>
        <!--#include file="../Scripts/librerias_js.asp"-->
        <!--#include file="../Scripts/estilos.asp"-->
        <script language="JavaScript">
          var valAnterior=''
          var externo="0"
          var oMyObject = window.dialogArguments;
          if (oMyObject!=null)
          {
          externo="1"
          }

        </script>
        <style>
          INPUT {behavior: url(''); border:'';}
          TABLE { border:1px solid black;	border-collapse:collapse; border-spacing:0 !important; }
          TH { overflow: hidden;
          text-overflow: ellipsis;
          -o-text-overflow: ellipsis; background-color:navy; color:white; }
          TD.money, TH.money { text-align:right; }
          TD, TH { border-collapse:collapse; padding:0 5px;}
          /* TBODY.collapsed TR { display:none; } */
          STRONG, TH {font-size:110%}
          TR.avanceActual TH {background-color:green; color:white;}
          TR.Estacion TH  {background-color:orange; color:black;}
          TR.Actividad TH  {background-color:silver; color:black;}
          TR.Estacion .Field_RowNumber {width:80;}
          TD.Field_RowNumber {background-color:white;}
          TD.Field_Porcentaje {background-color:white;}
          TD.Field_Concepto {background-color:white;}
          .Field_RowNumber {width:30;}
          .Field_Porcentaje {width:30;}
          .Field_Concepto {width:350;}
          .Field_Anotacion {width:150;}

        </style>
      </head>
      <body onKeyDown="teclado();" STYLE="margin-top:.00cm; margin-left:.00cm;">
        <xsl:apply-templates select="/" mode="ControlAbonos"/>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="*" mode="ControlAbonos"></xsl:template>

  <xsl:template match="*[name(.)='OC']" mode="ControlAbonos">White</xsl:template>
  <xsl:template match="*[name(.)='OC'][position()-1 mod 84=00]" mode="ControlAbonos">Aquamarine</xsl:template>

  <xsl:template match="/" mode="ControlAbonos.script">
    <script xml:space="preserve">
<![CDATA[
function toggleIniciadas(srcButton) {
	
}

function toggleLotes(srcButton) {

}
]]>
		</script>
  </xsl:template>

  <xsl:template match="/" mode="ControlAbonos.style">
    <xsl:element name="style" namespace="">
      <xsl:text>
			TBODY TD, TFOOT TD {border:solid 1pt black; padding-top:3px; padding-bottom:3px; text-align:center; font-size:9pt !important;}
			</xsl:text>
    </xsl:element>
  </xsl:template>

  <!--<xsl:template match="/" mode="ControlAbonos.style">
		<xsl:element name="style" namespace="">
			<xsl:text>border-left-width:'3'; border-left-style:'solid'; border-left-color:'gray';</xsl:text>
		</xsl:element>
	</xsl:template>-->

  <xsl:template name="removeZeros">
    <xsl:param name="quantity"/>
    <xsl:if test="$quantity!=0">
      <xsl:value-of select="$quantity"/>
    </xsl:if>
  </xsl:template>

  <xsl:template match="*|text()" mode="ControlAbonos"></xsl:template>
  <xsl:template match="*|text()" mode="ControlAbonos.body"></xsl:template>

  <xsl:template match="/" mode="ControlAbonos">
    <html>
      <body>
        <xsl:apply-templates select="/" mode="ControlAbonos.script"/>
        <xsl:apply-templates select="/" mode="ControlAbonos.style"/>
        <form name="formulario" action="" method="post">
          <table>
            <xsl:apply-templates select="ControlDeAbonos" mode="ControlAbonos.head"/>
            <tbody>
              <xsl:apply-templates select="*" mode="ControlAbonos.body"/>
            </tbody>
            <xsl:apply-templates select="/" mode="ControlAbonos.foot"/>
          </table>
          <xsl:apply-templates select="/" mode="ControlAbonos.foot.floating"/>
        </form>
        <br/>
        <div id="details"></div>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="CentrosDeCostos|Inmobiliarias" mode="ControlAbonos.head">
    <thead>
      <tr id="header">
        <td colspan="60" bordercolor="white">
          <span id="freeze">
            <img id="flecha" src="imagenes/flecha izq.gif" width="32" height="24" border="0" alt="REGRESAR" onClick="externo=='1'?window.close():history.go(-1);" style="cursor:'hand'; position:'relative'; display:'none';"/>
            <span style="display:'none'">
              <img src="imagenes/freeze.gif" width="24" height="24" alt="CONGELAR COLUMNAS" border="0" id="freezer" onClick="body.attachEvent('onclick', congelaColumnas); body.attachEvent('onmousemove', escogeColumnas);/* alert('Seleccione hasta cuál columna quiere congelar columnas')*/" style="cursor:'hand'; position:'relative'; display:'none';"/>
            </span>
          </span>
        </td>
      </tr>
      <tr id="header">
        <td colspan="100" bgcolor="white">
          <table border="1">
            <tr>
              <td width="40" bgcolor="#99CCFF">
                &#160;
              </td>
              <td width="100">
                <b style="font-size:10;">INFONAVIT</b>
              </td>
              <td width="40" bgcolor="#99FF99">
                &#160;
              </td>
              <td width="100">
                <b style="font-size:10;">FOVISSSTE ADQ.</b>
              </td>
              <td width="40" bgcolor="#FFFF99">
                &#160;
              </td>
              <td width="100">
                <b style="font-size:10;">
                  SHF / PROSAVI / <br/>ISSEG / BANCARIO
                </b>
              </td>
              <td width="40" bgcolor="#6699FF">
                &#160;
              </td>
              <td width="100">
                <b style="font-size:10;">LOTES</b>
              </td>
              <td width="40" bgcolor="#14DB0F">
                &#160;
              </td>
              <td width="100">
                <b style="font-size:10;">FOVISSSTE CONSTR.</b>
              </td>
              <td width="40" bgcolor="#FFCC66">
                &#160;
              </td>
              <td width="10%">
                <b style="font-size:10;">COFINAVIT/ALIA2</b>&#160;
              </td>
            </tr>
          </table>
        </td>
      </tr>

      <tr id="header">
        <th align="center" style="border-bottom-width:'3'; border-bottom-style:'solid'; border-bottom-color:'gray'; border-right-width:'3'; border-right-style:'solid'; border-right-color:'navy'; border-left-width:'3'; border-left-style:'solid'; border-left-color:'gray'; ">
          &#160;
        </th>
        <xsl:apply-templates mode="ControlAbonos.head.header" select="/ControlDeAbonos/Status/GrupoMayor"/>
        <th align="center" style="border-bottom-width:'3'; border-bottom-style:'solid'; border-bottom-color:'gray'; border-right-width:'3'; border-right-style:'solid'; border-right-color:'navy'; border-left-width:'3'; border-left-style:'solid'; border-left-color:'gray'; ">
          &#160;
        </th>
      </tr>

      <tr id="header">
        <th align="center" style="border-bottom-width:'3'; border-bottom-style:'solid'; border-bottom-color:'gray'; border-right-width:'3'; border-right-style:'solid'; border-right-color:'navy'; border-left-width:'3'; border-left-style:'solid'; border-left-color:'gray'; ">
          <b>
            <xsl:apply-templates mode="ControlAbonos.head.headerText" select="."/>
          </b>
        </th>
        <xsl:apply-templates mode="ControlAbonos.head.header" select="/ControlDeAbonos/Status/GrupoMayor/Headers"/>
        <th align="center" style="border-bottom-width:'3'; border-bottom-style:'solid'; border-bottom-color:'gray'; border-right-width:'3'; border-right-style:'solid'; border-right-color:'navy'; border-left-width:'3'; border-left-style:'solid'; border-left-color:'gray'; ">
          <b>
            <xsl:apply-templates mode="ControlAbonos.head.headerText" select="."/>
          </b>
        </th>
      </tr>
    </thead>
  </xsl:template>

  <xsl:template match="*" mode="ControlAbonos.body.tr.head">
    <td align="center" style="border-left-width:'3'; border-left-style:'solid'; border-left-color:'gray'; ">
      <a href="javascript: abreConsulta('CONTROL DE ABONOS', '104', '0', 'todas', 'todos', '', '29/12/2015')">
        <xsl:value-of select="@Nombre"/>
      </a>
    </td>
  </xsl:template>

  <xsl:template match="*" mode="ControlAbonos.body.tr.body">
  </xsl:template>

  <xsl:template match="*" mode="ControlAbonos.body.tr.foot">
    <td align="center" style="border-left-width:'3'; border-left-style:'solid'; border-left-color:'gray'; ">
      <a href="javascript: abreConsulta('CONTROL DE ABONOS', '104', '0', 'todas', 'todos', '', '29/12/2015')">
        <xsl:value-of select="@Nombre"/>
      </a>
    </td>
  </xsl:template>

  <xsl:template match="Status//Status|Status//GrupoMayor|Status//Headers" mode="ControlAbonos.head.header">
    <th align="center">
      <xsl:attribute name="style">
        <xsl:text>border-bottom-width:'3'; border-bottom-style:'solid'; border-bottom-color:'gray'; border-right-width:'3'; border-right-style:'solid'; border-right-color:'navy'; border-left-width:'3'; border-left-style:'solid'; border-left-color:'gray';</xsl:text>
      </xsl:attribute>
      <xsl:attribute name="colspan">
        <xsl:value-of select="count(.//Status)"/>
      </xsl:attribute>
      <xsl:copy-of select="@style"/>
      <b style="font-size: 7.5pt;">
        <xsl:value-of select="@headerText" disable-output-escaping="yes"/>
      </b>
    </th>
  </xsl:template>

  <xsl:template match="Status" mode="ControlAbonos.bgcolor">
  </xsl:template>

  <xsl:template match="Status[@Agrupacion='lote']" mode="ControlAbonos.bgcolor">
    <xsl:attribute name="bgcolor">
      <xsl:text>#6699ff</xsl:text>
    </xsl:attribute>
  </xsl:template>

  <xsl:template match="Status[@Agrupacion='cofinanciamiento']" mode="ControlAbonos.bgcolor">
    <xsl:attribute name="bgcolor">
      <xsl:text>#ffcc66</xsl:text>
    </xsl:attribute>
  </xsl:template>

  <xsl:template match="Status[@Agrupacion='infonavit']" mode="ControlAbonos.bgcolor">
    <xsl:attribute name="bgcolor">
      <xsl:text>#99ccff</xsl:text>
    </xsl:attribute>
  </xsl:template>

  <xsl:template match="Status[@Agrupacion='fovissste']" mode="ControlAbonos.bgcolor">
    <xsl:attribute name="bgcolor">
      <xsl:text>#99ff99</xsl:text>
    </xsl:attribute>
  </xsl:template>

  <xsl:template match="Status[@Agrupacion='fovissste_const']" mode="ControlAbonos.bgcolor">
    <xsl:attribute name="bgcolor">
      <xsl:text>#14db0f</xsl:text>
    </xsl:attribute>
  </xsl:template>

  <xsl:template match="Status[@Agrupacion='shf']" mode="ControlAbonos.bgcolor">
    <xsl:attribute name="bgcolor">
      <xsl:text>#ffff99</xsl:text>
    </xsl:attribute>
  </xsl:template>

  <xsl:template match="*" mode="ControlAbonos.body.content.style">
    <xsl:text>color:black;</xsl:text>
  </xsl:template>

  <xsl:template match="Status[@Binding='lote_xv' or @Binding='viv_xv_no_inic' or @Binding='viv_xv_inic' or @Binding='viv_xv_casi_term']" mode="ControlAbonos.body.content.style">
    <xsl:text>color:red;</xsl:text>
  </xsl:template>

  <xsl:template match="ControlDeAbonos/Status//Status" mode="ControlAbonos.body.content.warning">
    <xsl:param name="index"/>
    <xsl:if test="key(@IndexName,concat($index,'_',@Binding))[@añoOperacion&lt;number(/ControlDeAbonos/@año)][@status!='abonado_ant']">
      &#160;
      <xsl:variable name="title">
        <xsl:text>&lt;img src="../../../imagenes/advise/vbExclamation.gif" width="15"/&gt; &lt;strong&gt;Ubicaciones con problema&lt;/strong&gt;</xsl:text>
      </xsl:variable>
      <xsl:variable name="content">
        <!--<xsl:text>&lt;ul&gt;&lt;li&gt;Hola a todos&lt;/li&gt;&lt;li&gt;Here is a tooltip!&lt;br /&gt;Here is a tooltip!&lt;br /&gt;Here is a tooltip!&lt;/li&gt;&lt;/ul&gt;</xsl:text>-->
        <xsl:choose>
          <xsl:when test="count(key(@IndexName,concat($index,'_',@Binding))[@añoOperacion&lt;number(/ControlDeAbonos/@año)])=1">
            <xsl:choose>
              <xsl:when test="count(key(@IndexName,concat($index,'_',@Binding))[@añoOperacion&lt;number(/ControlDeAbonos/@año)])=count(key(@IndexName,concat($index,'_',@Binding)))">
                <xsl:text>La ubicación </xsl:text>
              </xsl:when>
              <xsl:otherwise>
                <xsl:text>De las </xsl:text>
                <xsl:value-of select="count(key(@IndexName,concat($index,'_',@Binding)))"/>
                <xsl:text>, la ubicación </xsl:text>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:text>está marcada como operación de otro año: </xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:choose>
              <xsl:when test="count(key(@IndexName,concat($index,'_',@Binding))[@añoOperacion&lt;number(/ControlDeAbonos/@año)])=count(key(@IndexName,concat($index,'_',@Binding)))">
                <xsl:text>Las </xsl:text>
              </xsl:when>
              <xsl:otherwise>
                <xsl:text>De las </xsl:text>
                <xsl:value-of select="count(key(@IndexName,concat($index,'_',@Binding)))"/>
                <xsl:text>, las siguientes </xsl:text>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:value-of select="count(key(@IndexName,concat($index,'_',@Binding))[@añoOperacion&lt;number(/ControlDeAbonos/@año)])"/>
            <xsl:text> ubicaciones están marcadas como operaciones de otros años: </xsl:text>
          </xsl:otherwise>
        </xsl:choose>
        <xsl:text>&lt;ol style="text-align:left;"&gt;</xsl:text>
        <xsl:apply-templates select="key(@IndexName,concat($index,'_',@Binding))[@añoOperacion&lt;number(/ControlDeAbonos/@año)]" mode="ControlAbonos.body.content.tooltip.content.record">
          <xsl:sort select="@añoOperacion" order="descending"/>
          <xsl:sort select="@Cliente" order="ascending"/>
        </xsl:apply-templates>
        <xsl:text>&lt;/ol&gt;</xsl:text>
        <xsl:if test="key(@IndexName,concat($index,'_',@Binding))[@añoOperacion&lt;number(/ControlDeAbonos/@año)-1]">
          <xsl:text>&lt;strong class="warning"&gt;Favor de reportarlo al administrador del sistema &lt;br/&gt;para confirmar que el caso ya haya sido contemplado.&lt;/strong&gt;</xsl:text>
        </xsl:if>
      </xsl:variable>
      <img src="../../../imagenes/advise/vbExclamation.gif" width="15" onMouseout="hideddrivetip()">
        <xsl:attribute name="onMouseover">
          <xsl:text>ddrivetip('</xsl:text>
          <xsl:value-of select="$title" disable-output-escaping="yes"/>
          <xsl:text>', '</xsl:text>
          <xsl:value-of select="$content" disable-output-escaping="yes"/>
          <xsl:text>', 100)</xsl:text>
        </xsl:attribute>
      </img>
    </xsl:if>
  </xsl:template>

  <xsl:template match="ControlDeAbonos/Detalles" mode="ControlAbonos.details.grid">
    <xsl:param name="recordSet"/>
    <xsl:text>&lt;table&gt;</xsl:text>
    <xsl:apply-templates select="." mode="ControlAbonos.details.grid.header">
      <xsl:with-param name="recordSet" select="$recordSet"/>
    </xsl:apply-templates>
    <xsl:apply-templates select="$recordSet" mode="ControlAbonos.details.grid.body"/>
    <xsl:text>&lt;/table&gt;</xsl:text>
  </xsl:template>

  <xsl:template match="ControlDeAbonos/Detalles" mode="ControlAbonos.details.grid.header">
    <xsl:param name="recordSet"/>
    <xsl:text>&lt;tr&gt;</xsl:text>
    <xsl:text>&lt;th&gt;#&lt;/th&gt;</xsl:text>
    <xsl:apply-templates select="*" mode="ControlAbonos.details.grid.header"/>
    <xsl:text>&lt;/tr&gt;</xsl:text>
  </xsl:template>

  <xsl:template match="Status//Status[@IndexName='Prospectos']" mode="ControlAbonos.details.grid.header">
    <xsl:text>&lt;tr&gt;</xsl:text>
    <xsl:text>&lt;th&gt;#&lt;/th&gt;</xsl:text>
    <xsl:text>&lt;th&gt;Fecha Asignacion&lt;/th&gt;</xsl:text>
    <xsl:text>&lt;th&gt;Prospecto&lt;/th&gt;</xsl:text>
    <xsl:text>&lt;th&gt;Ultimo Seguimiento&lt;/th&gt;</xsl:text>
    <xsl:text>&lt;/tr&gt;</xsl:text>
  </xsl:template>

  <!--<xsl:template match="ControlDeAbonos/Detalles/Column" mode="ControlAbonos.details.grid.header">
		<xsl:param name="recordSet"/>
		<xsl:variable name="currentColumn" select="."/>
		<xsl:if test="$recordSet/@*[local-name()=$currentColumn/@Binding]">
			<th>
				<xsl:copy-of select="@Binding"/>
				<xsl:value-of select="@headerText"/>
			</th>
		</xsl:if>
	</xsl:template>-->

  <xsl:template match="ControlDeAbonos/Detalles/Column" mode="ControlAbonos.details.grid.header">
    <xsl:text>&lt;th&gt;</xsl:text>
    <xsl:value-of select="@headerText"/>
    <xsl:text>&lt;/th&gt;</xsl:text>
  </xsl:template>

  <xsl:template match="*" mode="ControlAbonos.details.grid.cellContent">
    <xsl:text>--</xsl:text>
  </xsl:template>

  <xsl:template match="Ubicaciones/Ubicacion/@*" mode="ControlAbonos.details.grid.cellContent">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="Ubicaciones/Ubicacion/@precioVenta" mode="ControlAbonos.details.grid.cellContent">
    <xsl:value-of select="format-number(., '$###,##0.00', 'money')"/>
  </xsl:template>

  <xsl:template match="Ubicaciones/Ubicacion[@status='xv']/@precioVenta" mode="ControlAbonos.details.grid.cellContent">
    <xsl:apply-templates select="../PreciosAutorizados" mode="ControlAbonos.details.grid.cellContent"/>
  </xsl:template>

  <xsl:template match="PreciosAutorizados" mode="ControlAbonos.details.grid.cellContent">
    <xsl:text>&lt;table&gt;</xsl:text>
    <xsl:text>&lt;tr&gt;</xsl:text>
    <xsl:text>&lt;th&gt;</xsl:text>
    <xsl:text>Credito</xsl:text>
    <xsl:text>&lt;/th&gt;</xsl:text>
    <xsl:text>&lt;th&gt;</xsl:text>
    <xsl:text>Precio</xsl:text>
    <xsl:text>&lt;/th&gt;</xsl:text>
    <xsl:text>&lt;/tr&gt;</xsl:text>
    <xsl:apply-templates select="*" mode="ControlAbonos.details.grid.cellContent"/>
    <xsl:text>&lt;/table&gt;</xsl:text>
  </xsl:template>

  <xsl:template match="PreciosAutorizados/*" mode="ControlAbonos.details.grid.cellContent">
    <xsl:variable name="credito" select="key('Creditos',@IdCredito)"/>
    <xsl:text>&lt;tr&gt;</xsl:text>
    <xsl:text>&lt;td bgcolor="</xsl:text>
    <xsl:value-of select="$credito/@Color"/>
    <xsl:text>"&gt;</xsl:text>
    <xsl:value-of select="$credito/@Abreviatura"/>
    <xsl:text>&lt;/td&gt;</xsl:text>
    <xsl:text>&lt;td bgcolor="</xsl:text>
    <xsl:value-of select="$credito/@Color"/>
    <xsl:text>"&gt;</xsl:text>
    <xsl:value-of select="format-number(@Precio, '$###,##0.00', 'money')"/>
    <xsl:text>&lt;/td&gt;</xsl:text>
    <xsl:text>&lt;/tr&gt;</xsl:text>
  </xsl:template>

  <xsl:template match="Ubicaciones/Ubicacion/@porcAvance" mode="ControlAbonos.details.grid.cellContent">
    <xsl:value-of select="."/>
    <xsl:text>%</xsl:text>
  </xsl:template>

  <xsl:template match="Ubicaciones/Ubicacion" mode="ControlAbonos.details.grid.body">
    <xsl:variable name="currentRecord" select="."/>
    <xsl:text>&lt;tr&gt;</xsl:text>
    <xsl:text>&lt;th&gt;</xsl:text>
    <xsl:value-of select="position()"/>
    <xsl:text>&lt;/th&gt;</xsl:text>
    <xsl:for-each select="/ControlDeAbonos/Detalles/Column">
      <xsl:variable name="binding" select="@Binding"/>
      <xsl:text>&lt;td&gt;</xsl:text>
      <xsl:apply-templates select="$currentRecord/@*[local-name()=$binding]" mode="ControlAbonos.details.grid.cellContent"/>
      <xsl:text>&lt;/td&gt;</xsl:text>
    </xsl:for-each>
    <xsl:text>&lt;/tr&gt;</xsl:text>
  </xsl:template>

  <xsl:template match="Ubicaciones/Ubicacion" mode="ControlAbonos.details.grid.record">
    <xsl:param name="statusConsultado"/>
    <xsl:text>&lt;tr&gt;</xsl:text>
    <xsl:text>&lt;td&gt;</xsl:text>
    <xsl:value-of select="@añoOperacion"/>
    <xsl:text>&lt;/td&gt;&lt;td&gt;</xsl:text>
    <!--<xsl:text>E:</xsl:text>-->
    <xsl:value-of select="@NoEtapa"/>
    <xsl:text>&lt;/td&gt;&lt;td&gt;</xsl:text>
    <!--<xsl:text> M:</xsl:text>-->
    <xsl:value-of select="@Manzana"/>
    <xsl:text>&lt;/td&gt;&lt;td&gt;</xsl:text>
    <!--<xsl:text> L:</xsl:text>-->
    <xsl:value-of select="@Lote"/>
    <xsl:text>&lt;/td&gt;&lt;td&gt;</xsl:text>
    <xsl:value-of select="@Cliente"/>
    <xsl:text>&lt;/td&gt;&lt;td&gt;</xsl:text>
    <xsl:value-of select="format-number(@precioVenta, '$###,##0.00', 'money')"/>
    <xsl:text>&lt;/td&gt;&lt;td&gt;</xsl:text>
    <xsl:value-of select="@credito"/>
    <xsl:text>&lt;/td&gt;&lt;td&gt;</xsl:text>
    <xsl:value-of select="@porcAvance"/>
    <xsl:text>%</xsl:text>
    <xsl:text>&lt;/td&gt;&lt;td&gt;</xsl:text>
    <xsl:value-of select="@statusReal"/>
    <xsl:text>&lt;/td&gt;</xsl:text>
    <xsl:text>&lt;/tr&gt;</xsl:text>
  </xsl:template>

  <xsl:template match="Prospectos/Prospecto" mode="ControlAbonos.details.grid.record">
    <xsl:param name="statusConsultado"/>
    <xsl:text>&lt;tr&gt;</xsl:text>
    <xsl:text>&lt;th&gt;</xsl:text>
    <xsl:value-of select="position()"/>
    <xsl:text>&lt;/th&gt;&lt;td style="text-align:left;"&gt;</xsl:text>
    <xsl:value-of select="translate(@FechaAsignacion,'T',' ')"/>
    <xsl:text>&lt;/td&gt;&lt;td style="text-align:left;"&gt;</xsl:text>
    <xsl:value-of select="@NombreCompleto"/>
    <xsl:text>&lt;/td&gt;&lt;td style="text-align:left;"&gt;</xsl:text>
    <xsl:value-of select="translate(@FechaSeguimiento,'T',' ')"/>
    <xsl:text> - </xsl:text>
    <xsl:value-of select="@UltimoSeguimiento"/>
    <xsl:text>&lt;/td&gt;</xsl:text>
    <xsl:text>&lt;/tr&gt;</xsl:text>
  </xsl:template>

  <xsl:template match="Ubicaciones/Ubicacion" mode="ControlAbonos.body.content.tooltip.content.record">
    <xsl:text>&lt;li&gt;</xsl:text>
    <xsl:text>&lt;strong&gt;</xsl:text>
    <xsl:text>[</xsl:text>
    <xsl:value-of select="@añoOperacion"/>
    <xsl:text>]</xsl:text>
    <xsl:text> - E:</xsl:text>
    <xsl:value-of select="@NoEtapa"/>
    <xsl:text> M:</xsl:text>
    <xsl:value-of select="@Manzana"/>
    <xsl:text> L:</xsl:text>
    <xsl:value-of select="@Lote"/>
    <xsl:text> - &lt;/strong&gt;</xsl:text>
    <xsl:value-of select="@Cliente"/>
    <xsl:text> - </xsl:text>
    <xsl:value-of select="format-number(@precioVenta, '$###,##0.00', 'money')"/>
    <xsl:text> - </xsl:text>
    <xsl:value-of select="@credito"/>
    <xsl:text> - </xsl:text>
    <xsl:text>&lt;span style="color:blue"&gt; (</xsl:text>
    <xsl:value-of select="@porcAvance"/>
    <xsl:text>% / </xsl:text>
    <xsl:value-of select="@statusReal"/>
    <xsl:text>)&lt;/span&gt;</xsl:text>
    <xsl:text>&lt;/li&gt;</xsl:text>
  </xsl:template>

  <xsl:template match="Ubicaciones/Ubicacion[@status='xv']" mode="ControlAbonos.body.content.tooltip.content.record">
    <xsl:text>&lt;li&gt;</xsl:text>
    <xsl:text>E:</xsl:text>
    <xsl:value-of select="@NoEtapa"/>
    <xsl:text> M:</xsl:text>
    <xsl:value-of select="@Manzana"/>
    <xsl:text> L:</xsl:text>
    <xsl:value-of select="@Lote"/>
    <xsl:text> </xsl:text>
    <xsl:value-of select="format-number(@precioVenta, '$###,##0.00', 'money')"/>
    <xsl:text>&lt;span style="color:blue"&gt; (</xsl:text>
    <xsl:value-of select="@porcAvance"/>
    <xsl:text>%)&lt;/span&gt;</xsl:text>
    <xsl:text>&lt;/li&gt;</xsl:text>
  </xsl:template>

  <xsl:template match="ControlDeAbonos/Status//Status" mode="ControlAbonos.details.grid">
    <xsl:param name="index"/>
    <xsl:param name="limit" select="count(key(@IndexName,concat($index,'_',@Binding)))"/>
    <xsl:apply-templates select="/ControlDeAbonos/Detalles" mode="ControlAbonos.details.grid">
      <xsl:with-param name="recordSet" select="key(@IndexName,concat($index,'_',@Binding))[position()&lt;=$limit]"/>
    </xsl:apply-templates>
    <!--<xsl:if test="count(key(@IndexName,concat($index,'_',@Binding)))&gt;$limit">
			<xsl:value-of select="concat(count(key(@IndexName,concat($index,'_',@Binding)))-$limit,' ubicaciones más...')"/>
		</xsl:if>-->
  </xsl:template>

  <xsl:template match="ControlDeAbonos/Status//Status[@IndexName='Prospectos']" mode="ControlAbonos.details.grid">
    <xsl:param name="index"/>
    <xsl:param name="limit" select="count(key(@IndexName,concat($index,'_',@Binding)))"/>
    <xsl:text>&lt;table&gt;</xsl:text>
    <xsl:apply-templates select="." mode="ControlAbonos.details.grid.header"/>
    <xsl:apply-templates select="key(@IndexName,concat($index,'_',@Binding))[position()&lt;=$limit]" mode="ControlAbonos.details.grid.record">
      <xsl:sort select="@FechaSeguimiento" order="descending"/>
      <xsl:sort select="@FechaAsignacion" order="descending"/>
      <xsl:sort select="@NombreCompleto" order="ascending"/>
      <xsl:with-param name="statusConsultado" select="."/>
    </xsl:apply-templates>
    <xsl:text>&lt;/table&gt;</xsl:text>
    <!--<xsl:if test="count(key(@IndexName,concat($index,'_',@Binding)))&gt;$limit">
			<xsl:value-of select="concat(count(key(@IndexName,concat($index,'_',@Binding)))-$limit,' ubicaciones más...')"/>
		</xsl:if>-->
  </xsl:template>

  <xsl:template match="ControlDeAbonos/Status//Status" mode="ControlAbonos.body.content.tooltip.content">
    <xsl:param name="index"/>
    <xsl:param name="limit" select="count(key(@IndexName,concat($index,'_',@Binding)))"/>
    <xsl:text>&lt;ol style="text-align:left;"&gt;</xsl:text>
    <xsl:apply-templates select="key(@IndexName,concat($index,'_',@Binding))[position()&lt;=$limit]" mode="ControlAbonos.body.content.tooltip.content.record">
      <xsl:sort select="@añoOperacion" order="descending"/>
      <xsl:sort select="@Cliente" order="ascending"/>
    </xsl:apply-templates>
    <xsl:text>&lt;/ol&gt;</xsl:text>
    <xsl:if test="count(key(@IndexName,concat($index,'_',@Binding)))&gt;$limit">
      <xsl:value-of select="concat(count(key(@IndexName,concat($index,'_',@Binding)))-$limit,' ubicaciones más...')"/>
    </xsl:if>
  </xsl:template>

  <xsl:template match="ControlDeAbonos/Status//Status" mode="ControlAbonos.body.content.tooltip">
    <xsl:param name="index"/>
    <!--<xsl:variable name="limit" select="30"/>-->
    <xsl:variable name="limit" select="count(key(@IndexName,concat($index,'_',@Binding)))"/>
    <xsl:if test="key(@IndexName,concat($index,'_',@Binding))">
      <xsl:variable name="title">
        <xsl:text>&lt;img src="../../../imagenes/advise/vbInformation.gif" width="15"/&gt; &lt;strong&gt;</xsl:text>
        <xsl:choose>
          <xsl:when test="count(key(@IndexName,concat($index,'_',@Binding)))=1">
            <xsl:text>Esta ubicacion es: </xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text>Las </xsl:text>
            <xsl:value-of select="count(key(@IndexName,concat($index,'_',@Binding)))"/>
            <xsl:text> ubicaciones son: </xsl:text>
          </xsl:otherwise>
        </xsl:choose>
        <xsl:text>&lt;/strong&gt;</xsl:text>
      </xsl:variable>
      <xsl:variable name="content">
        <xsl:text>&lt;ol style="text-align:left;"&gt;</xsl:text>
        <xsl:apply-templates select="key(@IndexName,concat($index,'_',@Binding))[position()&lt;=$limit]" mode="ControlAbonos.body.content.tooltip.content.record">
          <xsl:sort select="@añoOperacion" order="descending"/>
          <xsl:sort select="@Cliente" order="ascending"/>
        </xsl:apply-templates>
        <xsl:text>&lt;/ol&gt;</xsl:text>
        <xsl:if test="count(key(@IndexName,concat($index,'_',@Binding)))&gt;$limit">
          <xsl:value-of select="concat(count(key(@IndexName,concat($index,'_',@Binding)))-$limit,' ubicaciones más...')"/>
        </xsl:if>
      </xsl:variable>
      <xsl:attribute name="onMouseout">hideddrivetip()</xsl:attribute>
      <xsl:attribute name="onMouseover">
        <xsl:text>ddrivetip('</xsl:text>
        <xsl:value-of select="$title" disable-output-escaping="yes"/>
        <xsl:text>', '</xsl:text>
        <xsl:value-of select="$content" disable-output-escaping="yes"/>
        <xsl:text>', 100,'green')</xsl:text>
      </xsl:attribute>
    </xsl:if>
  </xsl:template>

  <!--<xsl:template match="ControlDeAbonos/Status//Status[@Binding='viv_abonado_ant' or @Binding='lote_abonado_ant']" mode="ControlAbonos.body.content.tooltip">
	</xsl:template>-->
  <xsl:template match="ControlDeAbonos/Status//*" mode="ControlAbonos.details.title">
    <xsl:param name="grupo"/>
    <xsl:apply-templates select=".." mode="ControlAbonos.details.title">
      <xsl:with-param name="grupo" select="$grupo"/>
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template match="ControlDeAbonos/Status//*[string(@headerText)!='']" mode="ControlAbonos.details.title">
    <xsl:param name="grupo"/>
    <xsl:apply-templates select=".." mode="ControlAbonos.details.title">
      <xsl:with-param name="grupo" select="$grupo"/>
    </xsl:apply-templates>
    <xsl:text> / </xsl:text>
    <xsl:value-of select="@headerText"/>
  </xsl:template>

  <xsl:template match="ControlDeAbonos/Status" mode="ControlAbonos.details.title">
    <xsl:param name="grupo"/>
    <xsl:value-of select="$grupo"/>
  </xsl:template>

  <xsl:template match="ControlDeAbonos/Status//Status" mode="ControlAbonos.class">
  </xsl:template>

  <xsl:template match="ControlDeAbonos/Status//Status[@Agrupacion='lote']" mode="ControlAbonos.class">
    <xsl:attribute name="class">
      <xsl:text>lote</xsl:text>
    </xsl:attribute>
  </xsl:template>

  <xsl:template match="ControlDeAbonos/Status//Status" mode="ControlAbonos.body.content">
    <xsl:param name="index"/>
    <xsl:param name="grupo"/>
    <td align="center">
      <xsl:attribute name="style">
        <xsl:if test="count(key('Status',generate-id(../..))[1]|.)=1">
          <xsl:text>border-left-width:'3'; border-left-style:'solid'; border-left-color:'gray';</xsl:text>
        </xsl:if>
        <xsl:if test="count(key('Status',generate-id(../..))[last()]|.)=1">
          <xsl:text>border-right-width:'3'; border-right-style:'solid'; border-right-color:'gray';</xsl:text>
        </xsl:if>
        <xsl:if test="key(@IndexName,concat($index,'_',@Binding))[@añoOperacion&lt;number(/ControlDeAbonos/@año)]">
          <xsl:text>color:red;</xsl:text>
        </xsl:if>
      </xsl:attribute>
      <xsl:apply-templates mode="ControlAbonos.bgcolor" select="."/>
      <xsl:apply-templates mode="ControlAbonos.class" select="."/>
      <xsl:copy-of select="@style"/>
      <span>
        <xsl:if test="@IndexName='Ubicaciones' or @IndexName='Gestoria'">
          <xsl:apply-templates select="." mode="ControlAbonos.body.content.tooltip">
            <xsl:with-param name="index" select="$index"/>
          </xsl:apply-templates>
        </xsl:if>
        <xsl:attribute name="style">
          <xsl:text>cursor:hand;</xsl:text>
        </xsl:attribute>
        <xsl:attribute name="onclick">
          <xsl:text>$('#details').html('</xsl:text>
          <xsl:text>&lt;h3&gt;</xsl:text>
          <xsl:apply-templates select="." mode="ControlAbonos.details.title">
            <xsl:with-param name="grupo" select="$grupo"/>
          </xsl:apply-templates>
          <xsl:text>&lt;/h3&gt;</xsl:text>
          <xsl:apply-templates select="." mode="ControlAbonos.details.grid">
            <xsl:with-param name="index" select="$index"/>
          </xsl:apply-templates>
          <xsl:text>')</xsl:text>
        </xsl:attribute>
        &#160;
        <b class="num_viviendas" style="font: 8pt; color:red;">
          <xsl:attribute name="style">
            <xsl:apply-templates mode="ControlAbonos.body.content.style" select="."/>
          </xsl:attribute>
          <xsl:choose>
            <xsl:when test="/ControlDeAbonos/@TipoConsulta='conteo'">
              <xsl:call-template name="removeZeros">
                <xsl:with-param name="quantity" select="count(key(@IndexName,concat($index,'_',@Binding)))"/>
              </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
              <xsl:variable name="quantity">
                <xsl:call-template name="removeZeros">
                  <xsl:with-param name="quantity" select="sum(key(@IndexName,concat($index,'_',@Binding))/@montoOperacion)"/>
                </xsl:call-template>
              </xsl:variable>
              <xsl:value-of select="format-number($quantity, '$###,##0.00', 'money')"/>
            </xsl:otherwise>
          </xsl:choose>
        </b>
        &#160;
      </span>
      <xsl:apply-templates select="." mode="ControlAbonos.body.content.warning">
        <xsl:with-param name="index" select="$index"/>
      </xsl:apply-templates>
    </td>
  </xsl:template>

  <!--<xsl:template match="ControlDeAbonos/Status//Status[1=0]" mode="ControlAbonos.body.content">
		<xsl:param name="index"/>
		<td align="center">
			<xsl:attribute name="style">
				<xsl:if test="count(key('Status',generate-id(../..))[1]|.)=1">
					<xsl:text>border-left-width:'3'; border-left-style:'solid'; border-left-color:'gray';</xsl:text>
				</xsl:if>
				<xsl:if test="count(key('Status',generate-id(../..))[last()]|.)=1">
					<xsl:text>border-right-width:'3'; border-right-style:'solid'; border-right-color:'gray';</xsl:text>
				</xsl:if>
				<xsl:text>width:200px;</xsl:text>
			</xsl:attribute>
			<xsl:apply-templates mode="ControlAbonos.bgcolor" select="."/>
			<xsl:copy-of select="@style"/>
			<b class="num_viviendas" style="font: 8pt; color: Red;">
				<xsl:attribute name="style">
					<xsl:apply-templates mode="ControlAbonos.body.content.style" select="."/>
				</xsl:attribute>
				<xsl:call-template name="formatCurrency">
					<xsl:with-param name="currency">
						<xsl:call-template name="removeZeros">
							<xsl:with-param name="quantity" select="sum(key(@IndexName,concat($index,'_',@Binding))/@montoOperacion)"/>
						</xsl:call-template>
					</xsl:with-param>
				</xsl:call-template>
			</b>
		</td>
	</xsl:template>-->

  <xsl:template match="ControlDeAbonos/Status//Status" mode="ControlAbonos.foot.footer">
    <td align="center">
      <b>
        <xsl:call-template name="removeZeros">
          <xsl:with-param name="quantity" select="count(key(concat('Total_',@IndexName),@Binding))"/>
        </xsl:call-template>
      </b>
    </td>
  </xsl:template>

  <xsl:template match="ControlDeAbonos[@TipoConsulta='montos']/Status//Status" mode="ControlAbonos.foot.footer">
    <td align="center">
      <b>
        <xsl:variable name="quantity">
          <xsl:call-template name="removeZeros">
            <xsl:with-param name="quantity" select="sum(key(concat('Total_',@IndexName),@Binding)/@montoOperacion)"/>
          </xsl:call-template>
        </xsl:variable>
        <xsl:value-of select="format-number($quantity, '$###,##0.00', 'money')"/>
      </b>
    </td>
  </xsl:template>

  <xsl:template match="ControlDeAbonos/Status//Status" mode="ControlAbonos.foot.footer.acumulado">
    <xsl:param name="index"/>
    <xsl:variable name="Id" select="@Id"/>
    <td align="center">
      <b>
        <xsl:apply-templates mode="ControlAbonos.foot.footer.acumulado.suma" select="."/>
        <!--/ <xsl:value-of select="count(//Status//Status[@Id&gt;$Id][@acumulable=1])"/>-->
      </b>
    </td>
  </xsl:template>

  <xsl:template match="ControlDeAbonos/Status//Status[@acumulable=0]" mode="ControlAbonos.foot.footer.acumulado">
    <xsl:param name="index"/>
    <td align="center">
      &#160;
    </td>
  </xsl:template>

  <xsl:template match="ControlDeAbonos/Status//Status" mode="ControlAbonos.foot.footer.acumulado.suma">
    <xsl:param name="acumulado">0</xsl:param>
    <xsl:variable name="Id" select="@Id"/>
    <xsl:variable name="nextNode" select="(//Status//Status[@Id&gt;$Id][@acumulable=1])[1]"/>
    <xsl:variable name="nuevo_acumulado" select="count(key(concat('Total_',@IndexName),@Binding))+$acumulado"/>
    <xsl:choose>
      <xsl:when test="$nextNode">
        <xsl:apply-templates mode="ControlAbonos.foot.footer.acumulado.suma" select="$nextNode">
          <xsl:with-param name="acumulado" select="$nuevo_acumulado"/>
        </xsl:apply-templates>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$nuevo_acumulado"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="ControlDeAbonos[@TipoConsulta='montos']/Status//Status" mode="ControlAbonos.foot.footer.acumulado.suma">
    <xsl:param name="acumulado">0</xsl:param>
    <xsl:variable name="Id" select="@Id"/>
    <xsl:variable name="nextNode" select="(//Status//Status[@Id&gt;$Id][@acumulable=1])[1]"/>
    <xsl:variable name="nuevo_acumulado" select="sum(key(concat('Total_',@IndexName),@Binding)/@montoOperacion)+$acumulado"/>
    <xsl:choose>
      <xsl:when test="$nextNode">
        <xsl:apply-templates mode="ControlAbonos.foot.footer.acumulado.suma" select="$nextNode">
          <xsl:with-param name="acumulado" select="$nuevo_acumulado"/>
        </xsl:apply-templates>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="format-number($nuevo_acumulado, '$###,##0.00', 'money')"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="CentrosDeCostos" mode="ControlAbonos.head.headerText">
    <xsl:text>Centro de Costos</xsl:text>
  </xsl:template>

  <xsl:template match="Inmobiliarias" mode="ControlAbonos.head.headerText">
    <xsl:text>Inmobiliaria</xsl:text>
  </xsl:template>

  <xsl:template match="CentrosDeCostos/CentroDeCostos|Inmobiliarias/Inmobiliaria" mode="ControlAbonos.body">
    <tr>
      <xsl:apply-templates mode="ControlAbonos.body.tr.head" select="."/>
      <xsl:apply-templates mode="ControlAbonos.body.content" select="//Status//Status">
        <xsl:with-param name="index" select="@Id"/>
        <xsl:with-param name="grupo" select="@Nombre"/>
      </xsl:apply-templates>
      <xsl:apply-templates mode="ControlAbonos.body.tr.foot" select="."/>
    </tr>
  </xsl:template>

  <xsl:template match="/" mode="ControlAbonos.foot">
    <td align="center" id="lote">
      &#160;
    </td>
  </xsl:template>

  <xsl:template match="/" mode="ControlAbonos.foot">
    <tfoot>
      <tr>
        <td align="right">
          <font size="-1">
            <b>Total</b>
            <xsl:if test="key('Ubicaciones_No_Presentes','all')">
              <b style="color:red">
                <xsl:text> (</xsl:text>
                <xsl:value-of select="count(key('Ubicaciones_Presentes','all'))"/>
                <xsl:text> / </xsl:text>
                <xsl:value-of select="count(//Ubicaciones/Ubicacion)"/>
                <xsl:text>) - </xsl:text>
                <xsl:value-of select="key('Ubicaciones_No_Presentes','all')[1]/@Fracc"/>
              </b>
            </xsl:if>
          </font>
        </td>
        <xsl:apply-templates mode="ControlAbonos.foot.footer" select="//Status//Status"/>
        <td align="left">
          <font size="-1">
            <b>Total</b>
          </font>
        </td>
      </tr>
      <tr>
        <td colspan="" align="right">
          <font size="-1">
            <b>Acumulado</b>
          </font>
        </td>
        <xsl:apply-templates mode="ControlAbonos.foot.footer.acumulado" select="/ControlDeAbonos/Status//Status"/>
        <td colspan="" align="left">
          <font size="-1">
            <b>Acumulado</b>
          </font>
        </td>
      </tr>
    </tfoot>
  </xsl:template>

  <xsl:template match="/" mode="ControlAbonos.foot.floating">
    <span style="position:absolute" id="botones">
      <input type="button" name="mostrar" value="Ocultar No Iniciadas" onClick="cambiaContenido(this)"/>
      <input type="button" name="mostrarLotes" value="Ocultar Lotes" onClick="muestraLotes(this)"/>
      <!--<br/>
			<input type="button" value="Plan de Abonos Semanal" onClick="AbrirPagina('plan_abonos.asp', 'CONTROL DE ABONOS', '&amp;Estado=Todos&amp;Fraccionamiento=todos&amp;Anio=2015&amp;strPROYECTOS=&amp;Flecha=SI');"/>-->
    </span>
  </xsl:template>

  <xsl:template match="ControlDeAbonos" mode="ControlAbonos.head">
    <xsl:apply-templates mode="ControlAbonos.head" select="Inmobiliarias"/>
  </xsl:template>

  <xsl:template match="ControlDeAbonos" mode="ControlAbonos.head">
    <xsl:apply-templates mode="ControlAbonos.head" select="CentrosDeCostos"/>
  </xsl:template>

  <xsl:template match="ControlDeAbonos" mode="ControlAbonos.body">
    <xsl:apply-templates mode="ControlAbonos.body" select="key('Pivot.Base','filtered')"/>
  </xsl:template>
  <!--<xsl:template match="ControlDeAbonos" mode="ControlAbonos.body">
		<xsl:apply-templates mode="ControlAbonos.body" select="Inmobiliarias/*"/>
	</xsl:template>-->

</xsl:stylesheet>