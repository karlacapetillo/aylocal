<!--#include file="../server/vbscript.asp"-->
<%
DIM content_type: content_type=Request.ServerVariables("HTTP_ACCEPT")
IF session("StrCnn")="" THEN 
    IF (INSTR(UCASE(content_type),"JSON")>0) THEN
    Response.ContentType = "application/json"
    Response.CharSet = "ISO-8859-1"
    Response.Status = "401 Unauthorized" %>
    {
        "status":"unauthorized"
      , "message":""
    }
    <% 
    ELSE %>
<html><body>Usuario no autorizado</body></html>
<%  END IF
    response.end
END IF

Set cn = Server.CreateObject("ADODB.Connection")
cn.ConnectionTimeout = 0
cn.CommandTimeout = 0
cn.Open session("StrCnn")
IF Err.Number<>0 THEN
    Response.ContentType = "application/javascript"
    Response.CharSet = "ISO-8859-1"
    Response.Clear()
    Response.Status = "401 Unauthorized"
	ErrorDesc=Err.Description
    %>
    this.status='exception';
    this.statusType='unauthorized';
    this.message="<%= REPLACE(REPLACE(ErrorDesc, "[Microsoft][ODBC SQL Server Driver][SQL Server]", ""), """", "\""") %>";
    <%
    response.end
END IF
cn.Execute("SET LANGUAGE SPANISH")

Dim RegEx: Set RegEx = New RegExp
With RegEx
    .Pattern = "(\[[^\[]*\])+"
    .IgnoreCase = True
    .Global = True
    .MultiLine = True
End With

Dim objConn

target_file = REPLACE(Request.QueryString("target_file"),"/","\")

IF (target_file="1") THEN
    target_file="Proveedor_xdom.xml"
END IF
        
strFullPath="D:\Dropbox (Personal)\Proyectos\Petro\Version\Beta_12.0\xdom\custom\sessions\" & target_file
xslFile="D:\Dropbox (Personal)\Proyectos\Petro\Version\Beta_12.0\xdom\templates\alta_proveedores.xslt"
    
DIM xmlDoc:	set xmlDoc = Server.CreateObject("Microsoft.XMLDOM")
xmlDoc.Async = false
xmlDoc.SetProperty "SelectionLanguage", "XPath"
xmlDoc.Load(strFullPath)

'xslLimpiarNamespaces="D:\Dropbox (Personal)\Panax\PanaxDB\src\Panax 1.1\Transformations\remove namespace.xslt"
'Set xslDoc=Server.CreateObject("Microsoft.XMLDOM")
'xslDoc.async="false"
'xslDoc.load(xslLimpiarNamespaces)
'xmlDoc.loadXML(xmlDoc.transformNode(xslDoc))

Response.CharSet = "ISO-8859-1"
response.ContentType = "text/html" 
Set xslDoc=Server.CreateObject("Microsoft.XMLDOM")
xslDoc.async="false"
xslDoc.load(xslFile)
xmlDoc.loadXML(xmlDoc.transformNode(xslDoc))%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
          "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=10" />
    <meta name="viewport" content="width=device-width, initial-scale=.5, maximum-scale=12.0, minimum-scale=.10, user-scalable=yes" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" />
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css" />
    <link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-blue.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
    <link href="../resources/classic.css" type="text/css" rel="stylesheet" />
    <!--<script src="../resources/app.js"></script>-->
    <script src="../scripts/md5.js"></script>
    <link rel="stylesheet" href="../shell.css" />
    <link rel="stylesheet" href="../xdom.css" />
    <link rel="stylesheet" href="../login.css" />

    <script language="JavaScript" src="../xdom.js?id=9"></script>
    <script language="JavaScript" src="../custom.js?id=8"></script>
    <script language="JavaScript">
        function myAccFunc(target) {
            var x = document.getElementById(target);
            if (x.className.indexOf("w3-show") == -1) {
                x.className += " w3-show";
            } else {
                x.className = x.className.replace(" w3-show", "");
            }
        }

        function toggle_sidebar() {
            $(".sidebar").toggleClass("toggled").one("transitionend", function () { setTimeout(function () { window.dispatchEvent(new Event("resize")) }, 100) });
        }

        function toggle_settings() {
            $(".settings").toggleClass("open").one("transitionend", function () { setTimeout(function () { window.dispatchEvent(new Event("resize")) }, 100) });
        }
    </script>
    <style>
        th {
            background-color: #4E78B1;
            color: white
        }

        td {
            border-color: black;
        }

        td.changed {
            border-color: lime;
        }

        td.required {
            border-color: red;
        }

        tr.selected {
            background-color: lime;
        }

        input {
            vertical-align: top;
        }

        .money {
            text-align: right;
        }

        body {
            line-height: 1.2;
        }

        table {
            border-spacing: 1px;
        }

        * {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        tr td:last-child {
            width: 1%;
            white-space: nowrap;
        }

        /*main {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }*/
    </style>
    <title>Sistema en línea</title>
</head>
<body onload="relative_path='../'; xdom.init();">
<%response.write xmlDoc.xml

If (xmlDoc.parseError.errorCode <> 0) Then  
    Set myErr = xmlDoc.parseError  
    response.write myErr.reason
    response.end
End If
%>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>