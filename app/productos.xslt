﻿<xsl:stylesheet version="1.0" 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
xmlns="http://www.w3.org/1999/xhtml"
exclude-result-prefixes="xsl"
>
  <xsl:template match="busqueda">
    <div class="container">
                  <div class="row">
                    <div class="col-lg-3 col-md-5">
                      <div class="border p-4 rounded mb-4" >
                        <h3 class="mb-3 h6 text-uppercase text-black d-block">Categorias</h3>


                        <ul class="list-unstyled mb-0">

                          <li class="mb-1">
                            <a href="#" class="d-flex">
                              <span>Women</span>
                              <span class="text-black ml-auto">(2,550)</span>
                            </a>
                          </li>
                          <li class="mb-1">
                            <a href="#" class="d-flex">
                              <span>Children</span>
                              <span class="text-black ml-auto">(2,124)</span>
                            </a>
                          </li>
                        </ul>
                      </div>


                    </div>

                    <div class="col-lg-9 col-md-7">

                      <div class="filter__item">
                        <div class="row">
                          <div class="col-lg-4 col-md-4">
                            <div class="filter__found">
                              <h6>
                                <span>
                                  <xsl:value-of select="count(producto)"/>
                                </span> Productos encontrados
                              </h6>
                            </div>
                          </div>
                          <div class="col-lg-4 col-md-3">
                            <div class="filter__sort">
                              <span>Ordenar por</span>
                              <select>
                                <option value="0">Destacados</option>
                                <option value="0">A - Z</option>
                                <option value="0">Z - A</option>
                                <option value="0">$ - $$$</option>
                                <option value="0">$$$ - $</option>
                                <option value="0">Más vendido</option>
                                <option value="0">Más nuevo</option>
                              </select>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <xsl:apply-templates/>
                      </div>
                      <div class="product__pagination">
                        <a href="#">1</a>
                        <a href="#">2</a>
                        <a href="#">3</a>
                        <a href="#">
                          <i class="fa fa-long-arrow-right"></i>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
  </xsl:template>
  
  <xsl:template match="producto">
    <div class="col-lg-4 col-md-6 col-sm-6">
      <div class="product__item">
        <div class="product__item__pic set-bg">
          <img src="./custom/images/productos/producto{Id/text()}.jpg" />
          <ul class="product__item__pic__hover">
            <li>
              <a href="#">
                <i class="fa fa-heart"></i>
              </a>
            </li>
            <li>
              <a href="#" onclick="cart.add({Id/text()})">
                <i class="fa fa-shopping-cart"></i>
              </a>
            </li>
          </ul>
        </div>
        <div class="product__item__text">
          <h6>
            <a href="#">
              <xsl:apply-templates select="Nombre"/>
            </a>
          </h6>
          <h5>
            $<xsl:value-of select="Precio"/>
          </h5>
        </div>
      </div>
    </div>
  </xsl:template>
</xsl:stylesheet>