﻿<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml" xmlns:session="http://panax.io/session"
xmlns:sitemap="http://panax.io/sitemap"
xmlns:shell="http://panax.io/shell"
xmlns:state="http://panax.io/state"
exclude-result-prefixes="#default session sitemap shell"
>
  <xsl:output method="xml"
     omit-xml-declaration="yes"
     indent="yes"/>
  <xsl:template match="/*">
    <div class="wrapper">
      <!--Hacer que el código se ejecute cuando se carge. Provisionalmente se agrega a la página principal-->
      <!--<script>
        <![CDATA[
        function toggle_sidebar() {
            $(".sidebar").toggleClass("toggled").one("transitionend", function () { setTimeout(function () { window.dispatchEvent(new Event("resize")) }, 100) });
        }

        function toggle_settings() {
            $(".settings").toggleClass("open").one("transitionend", function () { setTimeout(function () { window.dispatchEvent(new Event("resize")) }, 100) });
        }]]>
      </script>-->
      <div class="wrapper">
        <nav class="sidebar" style="background: #00B0AD">
          <xsl:if test="not(//sitemap:*)">
            <xsl:attribute name="class">sidebar toggled</xsl:attribute>
          </xsl:if>
          <div class="sidebar-content " style="background: #00B0AD">
            <strong>
              <a class="sidebar-brand" href="default.html">
                <img src="custom/images/viva.png" height="39px" />
              </a>
            </strong>

            <ul class="sidebar-nav">
              <xsl:apply-templates mode="sitemap" select="/"/>
            </ul>

            <div class="sidebar-bottom d-none d-lg-block" style="background: #006C6C">
              <div class="media">
                <xsl:choose>
                  <xsl:when test="@session:user_id=-1">
                    <img class="rounded-circle mr-3" src="./custom/images/jacki.jpg" alt="{@session:user_login}" width="50" height="50"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <span class="fas fa-user-circle rounded-circle mr-3" style="font-size: 4em;"></span>
                  </xsl:otherwise>
                </xsl:choose>

                <div class="media-body">
                  <h5 class="mb-1">
                    <xsl:choose>
                      <xsl:when test="@session:user_id=-1">
                        <xsl:value-of select="@session:user_login"/>
                      </xsl:when>
                      <xsl:when test="@session:user_login">
                        <xsl:value-of select="@session:user_login"/>
                      </xsl:when>
                      <xsl:otherwise>Invitado</xsl:otherwise>
                    </xsl:choose>
                  </h5>
                  <div>
                    <i class="fas fa-circle text-success"></i> Online
                  </div>
                </div>
              </div>


            </div>
          </div>
        </nav>

        <div class="main">
          <nav class="navbar navbar-expand navbar-light bg-white" style="padding:.6rem 1.25rem;">
            <xsl:if test="//sitemap:*">
              <a class="sidebar-toggle d-flex mr-2" onclick="toggle_sidebar()">
                <i class="hamburger align-self-center"></i>
              </a>
            </xsl:if>
            <h2 id="main_title" style="margin:0;"></h2>

            <!--<form class="form-inline d-none d-sm-inline-block">
                    <input class="form-control form-control-no-border mr-sm-2" type="text" placeholder="Search projects..." aria-label="Search">
                </form>-->

            <div class="navbar-collapse collapse">

              <!--Barra de búsqueda-->
              <img src="custom/images/EmblemaAyLocal_VHorizontal.png" height="40px" />
              <!-- <div class="col-md-3 offset-md-1 mt-2">
                <div class="input-group mb-2">
                  <input type="text" class="form-control" placeholder="Prueba... productos disponibles"/>
                  <div class="input-group-append">
                    <span class="input-group-text"><i class="fa fa-search"></i></span>
                  </div>
                </div>
              </div>-->
              <form style=" width: 400px;" class="d-none d-sm-inline-block form-inline mr-auto ml-md-5 my-2 my-md-0 mw-100 navbar-search" >
                <div class="input-group">
                  <input type="text" class="form-control bg-light border-0 small" placeholder="Buscar un producto o servicio..." aria-label="Search" aria-describedby="basic-addon2"/>
                  <div class="input-group-append">
                    <button class="btn btn-primary" type="button">
                      <i class="fas fa-search fa-sm"></i>
                    </button>
                  </div>
                </div>
              </form>


              <ul class="navbar-nav ml-auto">
                <!--<li class="nav-item dropdown">
                  <a class="nav-icon dropdown-toggle" href="#" id="messagesDropdown" data-toggle="dropdown">
                    <div class="position-relative">
                      <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-message-circle align-middle">
                        <path d="M21 11.5a8.38 8.38 0 0 1-.9 3.8 8.5 8.5 0 0 1-7.6 4.7 8.38 8.38 0 0 1-3.8-.9L3 21l1.9-5.7a8.38 8.38 0 0 1-.9-3.8 8.5 8.5 0 0 1 4.7-7.6 8.38 8.38 0 0 1 3.8-.9h.5a8.48 8.48 0 0 1 8 8v.5z"></path>
                      </svg>
                      <span class="indicator">4</span>
                    </div>
                  </a>
                  <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right py-0" aria-labelledby="messagesDropdown">
                    <div class="dropdown-menu-header">
                      <div class="position-relative">
                        4 New Messages
                      </div>
                    </div>
                    <div class="list-group">
                      <a href="#" class="list-group-item">
                        <div class="row no-gutters align-items-center">
                          <div class="col-2">
                            <img src="./resources/avatar-5.jpg" class="avatar img-fluid rounded-circle" alt="Ashley Briggs"/>
                          </div>
                          <div class="col-10 pl-2">
                            <div class="text-dark">Ashley Briggs</div>
                            <div class="text-muted small mt-1">Nam pretium turpis et arcu. Duis arcu tortor.</div>
                            <div class="text-muted small mt-1">15m ago</div>
                          </div>
                        </div>
                      </a>
                      <a href="#" class="list-group-item">
                        <div class="row no-gutters align-items-center">
                          <div class="col-2">
                            <img src="./resources/avatar-2.jpg" class="avatar img-fluid rounded-circle" alt="Carl Jenkins"/>
                          </div>
                          <div class="col-10 pl-2">
                            <div class="text-dark">Carl Jenkins</div>
                            <div class="text-muted small mt-1">Curabitur ligula sapien euismod vitae.</div>
                            <div class="text-muted small mt-1">2h ago</div>
                          </div>
                        </div>
                      </a>
                      <a href="#" class="list-group-item">
                        <div class="row no-gutters align-items-center">
                          <div class="col-2">
                            <img src="./resources/avatar-4.jpg" class="avatar img-fluid rounded-circle" alt="Stacie Hall"/>
                          </div>
                          <div class="col-10 pl-2">
                            <div class="text-dark">Stacie Hall</div>
                            <div class="text-muted small mt-1">Pellentesque auctor neque nec urna.</div>
                            <div class="text-muted small mt-1">4h ago</div>
                          </div>
                        </div>
                      </a>
                      <a href="#" class="list-group-item">
                        <div class="row no-gutters align-items-center">
                          <div class="col-2">
                            <img src="./resources/avatar-3.jpg" class="avatar img-fluid rounded-circle" alt="Bertha Martin"/>
                          </div>
                          <div class="col-10 pl-2">
                            <div class="text-dark">Bertha Martin</div>
                            <div class="text-muted small mt-1">Aenean tellus metus, bibendum sed, posuere ac, mattis non.</div>
                            <div class="text-muted small mt-1">5h ago</div>
                          </div>
                        </div>
                      </a>
                    </div>
                    <div class="dropdown-menu-footer">
                      <a href="#" class="text-muted">Show all messages</a>
                    </div>
                  </div>
                </li>-->
                <!--<li class="nav-item dropdown">
                  <a class="nav-icon dropdown-toggle" href="#" id="alertsDropdown" data-toggle="dropdown">
                    <div class="position-relative">
                      <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-bell-off align-middle">
                        <path d="M13.73 21a2 2 0 0 1-3.46 0"></path>
                        <path d="M18.63 13A17.89 17.89 0 0 1 18 8"></path>
                        <path d="M6.26 6.26A5.86 5.86 0 0 0 6 8c0 7-3 9-3 9h14"></path>
                        <path d="M18 8a6 6 0 0 0-9.33-5"></path>
                        <line x1="1" y1="1" x2="23" y2="23"></line>
                      </svg>
                    </div>
                  </a>
                  <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right py-0" aria-labelledby="alertsDropdown">
                    <div class="dropdown-menu-header">
                      4 New Notifications
                    </div>
                    <div class="list-group">
                      <a href="#" class="list-group-item">
                        <div class="row no-gutters align-items-center">
                          <div class="col-2">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-alert-circle text-danger">
                              <circle cx="12" cy="12" r="10"></circle>
                              <line x1="12" y1="8" x2="12" y2="12"></line>
                              <line x1="12" y1="16" x2="12" y2="16"></line>
                            </svg>
                          </div>
                          <div class="col-10">
                            <div class="text-dark">Update completed</div>
                            <div class="text-muted small mt-1">Restart server 12 to complete the update.</div>
                            <div class="text-muted small mt-1">2h ago</div>
                          </div>
                        </div>
                      </a>
                      <a href="#" class="list-group-item">
                        <div class="row no-gutters align-items-center">
                          <div class="col-2">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-bell text-warning">
                              <path d="M18 8A6 6 0 0 0 6 8c0 7-3 9-3 9h18s-3-2-3-9"></path>
                              <path d="M13.73 21a2 2 0 0 1-3.46 0"></path>
                            </svg>
                          </div>
                          <div class="col-10">
                            <div class="text-dark">Lorem ipsum</div>
                            <div class="text-muted small mt-1">Aliquam ex eros, imperdiet vulputate hendrerit et.</div>
                            <div class="text-muted small mt-1">6h ago</div>
                          </div>
                        </div>
                      </a>
                      <a href="#" class="list-group-item">
                        <div class="row no-gutters align-items-center">
                          <div class="col-2">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home text-primary">
                              <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                              <polyline points="9 22 9 12 15 12 15 22"></polyline>
                            </svg>
                          </div>
                          <div class="col-10">
                            <div class="text-dark">Login from 192.186.1.1</div>
                            <div class="text-muted small mt-1">8h ago</div>
                          </div>
                        </div>
                      </a>
                      <a href="#" class="list-group-item">
                        <div class="row no-gutters align-items-center">
                          <div class="col-2">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user-plus text-success">
                              <path d="M16 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path>
                              <circle cx="8.5" cy="7" r="4"></circle>
                              <line x1="20" y1="8" x2="20" y2="14"></line>
                              <line x1="23" y1="11" x2="17" y2="11"></line>
                            </svg>
                          </div>
                          <div class="col-10">
                            <div class="text-dark">New connection</div>
                            <div class="text-muted small mt-1">Anna accepted your request.</div>
                            <div class="text-muted small mt-1">12h ago</div>
                          </div>
                        </div>
                      </a>
                    </div>
                    <div class="dropdown-menu-footer">
                      <a href="#" class="text-muted">Show all notifications</a>
                    </div>
                  </div>
                </li>-->
                <!--<li class="nav-item dropdown">
                  <a class="nav-flag dropdown-toggle" href="#" id="languageDropdown" data-toggle="dropdown">
                    <img src="./resources/us.png" alt="English"/>
                  </a>
                  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="languageDropdown">
                    <a class="dropdown-item" href="#">
                      <img src="./resources/us.png" alt="English" width="20" class="align-middle mr-1"/>
                      <span class="align-middle">English</span>
                    </a>
                    <a class="dropdown-item" href="#">
                      <img src="./resources/es.png" alt="Spanish" width="20" class="align-middle mr-1"/>
                      <span class="align-middle">Spanish</span>
                    </a>
                    <a class="dropdown-item" href="#">
                      <img src="./resources/de.png" alt="German" width="20" class="align-middle mr-1"/>
                      <span class="align-middle">German</span>
                    </a>
                    <a class="dropdown-item" href="#">
                      <img src="./resources/nl.png" alt="Dutch" width="20" class="align-middle mr-1"/>
                      <span class="align-middle">Dutch</span>
                    </a>
                  </div>
                </li>-->
                <li class="nav-item dropdown">
                  <a class="nav-icon dropdown-toggle d-inline-block d-sm-none" href="#" data-toggle="dropdown">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-settings align-middle">
                      <circle cx="12" cy="12" r="3"></circle>
                      <path d="M19.4 15a1.65 1.65 0 0 0 .33 1.82l.06.06a2 2 0 0 1 0 2.83 2 2 0 0 1-2.83 0l-.06-.06a1.65 1.65 0 0 0-1.82-.33 1.65 1.65 0 0 0-1 1.51V21a2 2 0 0 1-2 2 2 2 0 0 1-2-2v-.09A1.65 1.65 0 0 0 9 19.4a1.65 1.65 0 0 0-1.82.33l-.06.06a2 2 0 0 1-2.83 0 2 2 0 0 1 0-2.83l.06-.06a1.65 1.65 0 0 0 .33-1.82 1.65 1.65 0 0 0-1.51-1H3a2 2 0 0 1-2-2 2 2 0 0 1 2-2h.09A1.65 1.65 0 0 0 4.6 9a1.65 1.65 0 0 0-.33-1.82l-.06-.06a2 2 0 0 1 0-2.83 2 2 0 0 1 2.83 0l.06.06a1.65 1.65 0 0 0 1.82.33H9a1.65 1.65 0 0 0 1-1.51V3a2 2 0 0 1 2-2 2 2 0 0 1 2 2v.09a1.65 1.65 0 0 0 1 1.51 1.65 1.65 0 0 0 1.82-.33l.06-.06a2 2 0 0 1 2.83 0 2 2 0 0 1 0 2.83l-.06.06a1.65 1.65 0 0 0-.33 1.82V9a1.65 1.65 0 0 0 1.51 1H21a2 2 0 0 1 2 2 2 2 0 0 1-2 2h-.09a1.65 1.65 0 0 0-1.51 1z"></path>
                    </svg>
                  </a>




                  <ul class="navbar-nav ml-auto">

                    <!--Idiomas Menu-->
                    <li class="nav-item dropdown no-arrow d-sm-none">
                      <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-globe"></i>
                      </a>

                      <!-- Dropdown - Idiomas -->
                      <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchdropdown">
                        <form class="form-inline mr-auto w-100 navbar-search">
                          <div class="input-group">
                            <input type="text" class="form-control bg-light border-0 small" placeholder="search for..." aria-label="search" aria-describedby="basic-addon2"/>
                            <div class="input-group-append">
                              <button class="btn btn-primary" type="button">
                                <i class="fas fa-globe"></i>
                              </button>
                            </div>
                          </div>
                        </form>
                      </div>
                    </li>
                    <!-- Nav Item - Idiomas -->
                    <li class="nav-item dropdown no-arrow mx-1">
                      <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-globe"></i>

                      </a>
                      <!-- Dropdown - Idiomas -->
                      <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
                        <h6 class="dropdown-header">
                          Idiomas
                        </h6>
                        <a class="dropdown-item d-flex align-items-center" href="#">
                          <div class="mr-3">
                            <div>
                              <img src="./custom/images/me.png" width="20" height="20"/>
                            </div>
                          </div>
                          <div>
                            <span class="font-weight-bold">Español (México)</span>
                          </div>
                        </a>

                        <a class="dropdown-item d-flex align-items-center" href="#">
                          <div class="mr-3">
                            <div>
                              <img src="./custom/images/Eua.png" width="20" height="20"/>
                            </div>
                          </div>
                          <div>
                            <span class="font-weight-bold">English (US)</span>
                          </div>
                        </a>
                      </div>
                    </li>



                    <!--Compras Menu-->
                    <li class="nav-item dropdown no-arrow d-sm-none">
                      <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-shopping-cart"></i>
                      </a>

                      <!-- Dropdown - Compras -->
                      <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchdropdown">
                        <form class="form-inline mr-auto w-100 navbar-search">
                          <div class="input-group">
                            <input type="text" class="form-control bg-light border-0 small" placeholder="search for..." aria-label="search" aria-describedby="basic-addon2"/>
                            <div class="input-group-append">
                              <button class="btn btn-primary" type="button">
                                <i class="fas fa-shopping-cart"></i>
                              </button>
                            </div>
                          </div>
                        </form>
                      </div>
                    </li>
                    <!-- Nav Item - Compras -->
                    <xsl:variable name="cart" select="//shell:cart/*"/>
                    <li class="nav-item dropdown no-arrow mx-1">
                      <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-shopping-cart"></i>
                        <xsl:if test="$cart">
                          <!-- Counter - Cart -->
                          <span class="badge badge-danger badge-counter">
                            <xsl:value-of select="count($cart)"/>
                          </span>
                        </xsl:if>
                      </a>
                      <!-- Dropdown - Compras -->
                      <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
                        <xsl:choose>
                          <xsl:when test="$cart">
                            <h6 class="dropdown-header">
                              Mis pedidos
                            </h6>
                            <xsl:apply-templates mode="shell_messages" select="$cart"/>
                            <a class="dropdown-item text-center small text-gray-500" href="#" onclick="cart.empty()">Vaciar tu carrito de compras</a>
                          </xsl:when>
                          <xsl:otherwise>
                            <a class="dropdown-item text-center small text-gray" href="#">Carrito Vacío</a>
                          </xsl:otherwise>
                        </xsl:choose>
                      </div>
                    </li>

                    <!--Mensajes Menu-->
                    <li class="nav-item dropdown no-arrow d-sm-none">
                      <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-search fa-fw"></i>
                      </a>

                      <!-- Dropdown - Messages -->
                      <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchdropdown">
                        <form class="form-inline mr-auto w-100 navbar-search">
                          <div class="input-group">
                            <input type="text" class="form-control bg-light border-0 small" placeholder="search for..." aria-label="search" aria-describedby="basic-addon2"/>
                            <div class="input-group-append">
                              <button class="btn btn-primary" type="button">
                                <i class="fas fa-search fa-sm"></i>
                              </button>
                            </div>
                          </div>
                        </form>
                      </div>
                    </li>

                    <!-- Nav Item - Alerts -->
                    <li class="nav-item dropdown no-arrow mx-1">
                      <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-bell fa-fw"></i>
                        <xsl:variable name="notifications" select="//shell:messages/notifications/*"/>
                        <xsl:if test="$notifications">
                          <!-- Counter - Alerts -->
                          <span class="badge badge-danger badge-counter">
                            <xsl:value-of select="count($notifications)"/>
                          </span>
                        </xsl:if>
                      </a>
                      <!-- Dropdown - Alerts -->
                      <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
                        <h6 class="dropdown-header">
                          Notificaciones
                        </h6>
                        <a class="dropdown-item d-flex align-items-center" href="#">
                          <div class="mr-3">
                            <div class="icon-circle bg-primary">
                              <i class="fas fa-file-alt text-white"></i>
                            </div>
                          </div>
                          <div>
                            <div class="small text-gray-500">13 de Abril de 2020</div>
                            <span class="font-weight-bold">Un nuevo proveedor se ha unido</span>
                          </div>
                        </a>
                        <a class="dropdown-item text-center small text-gray-500" href="#">Mostrar todas las notificaciones</a>
                      </div>
                    </li>

                    <!-- Nav Item - Messages -->
                    <li class="nav-item dropdown no-arrow mx-1">
                      <xsl:variable name="messages" select="//shell:messages/messages/*"/>
                      <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-envelope fa-fw"></i>
                        <xsl:if test="$messages">
                          <!-- Counter - Messages -->
                          <span class="badge badge-danger badge-counter">
                            <xsl:value-of select="count($messages)"/>
                          </span>
                        </xsl:if>
                      </a>
                      <!-- Dropdown - Messages -->
                      <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="messagesDropdown">
                        <h6 class="dropdown-header">
                          Mensajes
                        </h6>
                        <xsl:apply-templates mode="shell_messages" select="$messages"/>
                        <a class="dropdown-item text-center small text-gray-500" href="#">Leer más mensajes</a>
                      </div>
                    </li>


                    <div class="topbar-divider d-none d-sm-block"></div>

                    <li class="nav-item dropdown no-arrow">
                      <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="mr-2 d-none d-lg-inline text-gray-600 small">
                          <xsl:choose>
                            <xsl:when test="@session:user_id=-1">
                              <xsl:value-of select="@session:user_login"/>
                            </xsl:when>
                            <xsl:when test="@session:user_login">
                              <xsl:value-of select="@session:user_login"/>
                            </xsl:when>
                            <xsl:otherwise>Invitado</xsl:otherwise>
                          </xsl:choose>
                        </span>
                        <xsl:choose>
                          <xsl:when test="@session:user_id=-1">
                            <img src="./custom/images/jacki.jpg" class="avatar img-fluid rounded-circle mr-1" alt="{@session:user_login}"/>
                          </xsl:when>
                          <xsl:otherwise>
                            <span class="fas fa-user-circle rounded-circle mr-3" style="font-size: 1.7em;"></span>
                          </xsl:otherwise>
                        </xsl:choose>
                      </a>
                      <!-- Dropdown - User Information -->
                      <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                        <a class="dropdown-item" href="pages-profile.html">
                          <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                          Perfil
                        </a>
                        <a class="dropdown-item" href="#">
                          <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                          Ajustes
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#" onclick="xdom.session.logout();">
                          <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                          Salir
                        </a>
                      </div>
                    </li>

                  </ul>



                  <!--<a class="nav-link dropdown-toggle d-none d-sm-inline-block" href="#" data-toggle="dropdown">
                    
                      
                   <xsl:choose>
                      <xsl:when test="@session:user_id=-1">
                        <img src="./custom/images/jacki.jpg" class="avatar img-fluid rounded-circle mr-1" alt="{@session:user_login}"/>
                      </xsl:when>
                      <xsl:otherwise>
                        <span class="fas fa-user-circle rounded-circle mr-3" style="font-size: 1.7em;"></span>
                      </xsl:otherwise>
                    </xsl:choose>
                    
                    <span class="text-dark">
                      <xsl:choose>
                        <xsl:when test="@session:user_id=-1">
                          <xsl:value-of select="@session:user_login"/>
                        </xsl:when>
                        <xsl:when test="@session:user_login">
                          <xsl:value-of select="@session:user_login"/>
                        </xsl:when>
                        <xsl:otherwise>Invitado</xsl:otherwise>
                      </xsl:choose>
                    </span>                
                  </a>-->


                  <!--<div class="dropdown-menu dropdown-menu-right">
                    
                <a class="dropdown-item" href="pages-profile.html">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Perfil
                </a>
                    
                <a class="dropdown-item" href="#">
                  <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                  Ajustes
                </a>
                 
                    -->
                  <!--<a class="dropdown-item disabled" href="pages-profile.html">
                      <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user align-middle mr-1">
                        <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path>
                        <circle cx="12" cy="7" r="4"></circle>
                      </svg> Perfil
                    </a>-->
                  <!--
                    <div class="dropdown-divider"></div>
                    -->
                  <!--<a class="dropdown-item" href="pages-settings.html">Settings &amp; Privacy</a>
                                <a class="dropdown-item" href="#">Help</a>-->
                  <!--
                    <a class="dropdown-item" href="#" onclick="xdom.session.logout();"><i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                    Salir</a>
                  </div>-->


                </li>
              </ul>
            </div>
          </nav>

          <main>

    <!-- Productos para mostrar -->
    <section class="product spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-5">
                  <div class="border p-4 rounded mb-4" >                 
              <h3 class="mb-3 h6 text-uppercase text-black d-block">Categorias</h3>
              
                    
              <ul class="list-unstyled mb-0">
            
                <li class="mb-1"><a href="#" class="d-flex"><span>Women</span> <span class="text-black ml-auto">(2,550)</span></a></li>
                <li class="mb-1"><a href="#" class="d-flex"><span>Children</span> <span class="text-black ml-auto">(2,124)</span></a></li>
              </ul>
            </div>
                
              
                </div>

                <div class="col-lg-9 col-md-7">
                  
                    <div class="filter__item">
                        <div class="row">
                            <div class="col-lg-4 col-md-4">
                                <div class="filter__found">
                                    <h6><span>8</span> Productos encontrados</h6>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-3">
                                <div class="filter__sort">
                                    <span>Ordenar por</span>
                                    <select>
                                        <option value="0">Destacados</option>
                                        <option value="0">A - Z</option>
                                        <option value="0">Z - A</option>
                                        <option value="0">$ - $$$</option>
                                        <option value="0">$$$ - $</option>
                                        <option value="0">Más vendido</option>
                                        <option value="0">Más nuevo</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="product__item">
                                <div class="product__item__pic set-bg">
                                  <img  src="./custom/images/producto3.jpg" />
                                    <ul class="product__item__pic__hover">
                                        <li><a href="#"><i class="fa fa-heart"></i></a></li>
                                        <li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
                                    </ul>
                                </div>
                                <div class="product__item__text">
                                    <h6><a href="#">Crab Pool Security</a></h6>
                                    <h5>$30.00</h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="product__item">
                                <div class="product__item__pic set-bg">
                                  <img  src="./custom/images/producto3.jpg" />
                                    <ul class="product__item__pic__hover">
                                        <li><a href="#"><i class="fa fa-heart"></i></a></li>
                                        <li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
                                    </ul>
                                </div>
                                <div class="product__item__text">
                                    <h6><a href="#">Crab Pool Security</a></h6>
                                    <h5>$30.00</h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="product__item">
                                <div class="product__item__pic set-bg">
                                  <img  src="./custom/images/producto3.jpg" />
                                    <ul class="product__item__pic__hover">
                                        <li><a href="#"><i class="fa fa-heart"></i></a></li>
                                        <li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
                                    </ul>
                                </div>
                                <div class="product__item__text">
                                    <h6><a href="#">Crab Pool Security</a></h6>
                                    <h5>$30.00</h5>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="product__item">
                                <div class="product__item__pic set-bg">
                                  <img  src="./custom/images/producto3.jpg" />
                                    <ul class="product__item__pic__hover">
                                        <li><a href="#"><i class="fa fa-heart"></i></a></li>
                                        <li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
                                    </ul>
                                </div>
                                <div class="product__item__text">
                                    <h6><a href="#">Crab Pool Security</a></h6>
                                    <h5>$30.00</h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="product__item">
                                <div class="product__item__pic set-bg">
                                  <img  src="./custom/images/producto3.jpg" />
                                    <ul class="product__item__pic__hover">
                                        <li><a href="#"><i class="fa fa-heart"></i></a></li>
                                        <li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
                                    </ul>
                                </div>
                                <div class="product__item__text">
                                    <h6><a href="#">Crab Pool Security</a></h6>
                                    <h5>$30.00</h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="product__item">
                                <div class="product__item__pic set-bg">
                                  <img  src="./custom/images/producto3.jpg" />
                                    <ul class="product__item__pic__hover">
                                        <li><a href="#"><i class="fa fa-heart"></i></a></li>
                                        <li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
                                    </ul>
                                </div>
                                <div class="product__item__text">
                                    <h6><a href="#">Crab Pool Security</a></h6>
                                    <h5>$30.00</h5>
                                </div>
                            </div>
                        </div>
                    
                    </div>
                    <div class="product__pagination">
                        <a href="#">1</a>
                        <a href="#">2</a>
                        <a href="#">3</a>
                        <a href="#"><i class="fa fa-long-arrow-right"></i></a>
                    </div>
                </div>




            </div>
        </div>
    </section>
          
          </main>


          <footer class="footer" style="position: static; bottom: 0; width: 100%; justify-content: center; align-items: center;">
            <div class="container-fluid">
              <div class="row text-muted">

                <div class="col-md-3 mb-2" style="justify-content: center; align-items: center;">
                  <h4 class="hfoot">Acerca de nosotros</h4>
                  <ul class="list-unstyled">
                    <li>
                      <a href="#">Proveedores</a>
                    </li>
                    <li>
                      <a href="#" >Categorias</a>
                    </li>
                    <li>
                      <a href="#" >Ayuda</a>
                    </li>
                  </ul>
                </div>
                <div class="col-md-3 mb-2">
                  <h4 class="hfoot">Descubre</h4>
                  <ul class="list-unstyled">
                    <li>
                      <a href="#" >¿Porqué comprar en AyLocal?</a>
                    </li>
                    <li>
                      <a href="#">¿Porqué vender en AyLocal?</a>
                    </li>
                  </ul>
                </div>
                <div class="col-md-3 mb-2">
                  <h4 class="hfoot">Servicio al cliente</h4>
                  <ul class="list-unstyled">
                    <li>
                      <a href="#" >Contáctanos</a>
                    </li>
                    <li>
                      <a href="#" >Terminos y condiciones</a>
                    </li>
                  </ul>
                </div>
                <div class="col-md-3 mb-2">
                  <h4 class="hfoot text-center">Siguenos en</h4>
                  <ul class="sci" style="margin: 0;	padding: 0;	display: flex;	justify-content: center;	align-items: center; ">
                    <li style="list-style: none;	margin: 0 2px;">
                      <a href="#">
                        <img class="rounded-circle mr-3"  width="25" height="25" src="./custom/images/f.png"/>
                      </a>
                    </li>
                    <li style="list-style: none;	margin: 0 2px;">
                      <a href="#">
                        <img class="rounded-circle mr-3"  width="25" height="25" src="./custom/images/i.png"/>
                      </a>
                    </li>
                    <li style="list-style: none;	margin: 0 2px;">
                      <a href="#">
                        <img class="rounded-circle mr-3"  width="25" height="25" src="./custom/images/t.png"/>
                      </a>
                    </li>
                  </ul>
                </div>

              </div>
            </div>
            <hr class="line"/>

            <!--Derechos reservados-->
            <div class="container-fluid">
              <div class="row text-muted" style="justify-content: center; align-items: center;">
                <span style="padding: 15px;" >
                  Copyright <i class="far fa-copyright"></i> AyLocal. Todos los derechos reservados
                </span>
              </div>
            </div>

          </footer>





        </div>
      </div>




      <div class="settings">
        <div class="settings-toggle toggle-settings" onclick="toggle_settings()">
          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-settings align-middle">
            <circle cx="12" cy="12" r="3"></circle>
            <path d="M19.4 15a1.65 1.65 0 0 0 .33 1.82l.06.06a2 2 0 0 1 0 2.83 2 2 0 0 1-2.83 0l-.06-.06a1.65 1.65 0 0 0-1.82-.33 1.65 1.65 0 0 0-1 1.51V21a2 2 0 0 1-2 2 2 2 0 0 1-2-2v-.09A1.65 1.65 0 0 0 9 19.4a1.65 1.65 0 0 0-1.82.33l-.06.06a2 2 0 0 1-2.83 0 2 2 0 0 1 0-2.83l.06-.06a1.65 1.65 0 0 0 .33-1.82 1.65 1.65 0 0 0-1.51-1H3a2 2 0 0 1-2-2 2 2 0 0 1 2-2h.09A1.65 1.65 0 0 0 4.6 9a1.65 1.65 0 0 0-.33-1.82l-.06-.06a2 2 0 0 1 0-2.83 2 2 0 0 1 2.83 0l.06.06a1.65 1.65 0 0 0 1.82.33H9a1.65 1.65 0 0 0 1-1.51V3a2 2 0 0 1 2-2 2 2 0 0 1 2 2v.09a1.65 1.65 0 0 0 1 1.51 1.65 1.65 0 0 0 1.82-.33l.06-.06a2 2 0 0 1 2.83 0 2 2 0 0 1 0 2.83l-.06.06a1.65 1.65 0 0 0-.33 1.82V9a1.65 1.65 0 0 0 1.51 1H21a2 2 0 0 1 2 2 2 2 0 0 1-2 2h-.09a1.65 1.65 0 0 0-1.51 1z"></path>
          </svg>
        </div>

        <div class="settings-panel">
          <div class="settings-content js-simplebar" data-simplebar="init">
            <div class="simplebar-wrapper" style="margin: 0px;">
              <div class="simplebar-height-auto-observer-wrapper">
                <div class="simplebar-height-auto-observer"></div>
              </div>
              <div class="simplebar-mask">
                <div class="simplebar-offset" style="right: -17px; bottom: 0px;">
                  <div class="simplebar-content-wrapper" style="height: 100%; overflow: hidden scroll;">
                    <div class="simplebar-content" style="padding: 0px;">
                      <div class="settings-title">
                        <button type="button" class="close float-right" aria-label="Close" onclick="toggle_settings()">
                          <span aria-hidden="true">×</span>
                        </button>
                        <h4>Herramientas</h4>
                      </div>

                      <div class="settings-section">
                        <small class="d-block text-uppercase font-weight-bold text-muted mb-2">Sesión</small>
                        <div class="list-group">
                          <button type="button" class="list-group-item list-group-item-action" onclick="xdom.dom.refresh();">Actualizar</button>
                          <button type="button" class="list-group-item list-group-item-action" onclick="xdom.session.saveSession();">Guardar sesión</button>
                          <button type="button" class="list-group-item list-group-item-action" onclick="xdom.session.loadSession()">Restaurar sesión</button>
                        </div>
                      </div>

                      <div class="settings-section">
                        <small class="d-block text-uppercase font-weight-bold text-muted mb-2">Edición</small>
                        <div class="list-group">
                          <button type="button" class="list-group-item list-group-item-action" onclick="xdom.data.undo();">Deshacer</button>
                          <button type="button" class="list-group-item list-group-item-action" onclick="xdom.data.redo()">Rehacer</button>
                          <button type="button" class="list-group-item list-group-item-action" onclick="xdom.dom.print()">Imprimir</button>
                        </div>
                      </div>

                      <div class="settings-section">
                        <small class="d-block text-uppercase font-weight-bold text-muted mb-2">Desarrollador</small>
                        <div class="list-group">
                          <button type="button" class="list-group-item list-group-item-action" onclick="xdom.library.reload(); /*window.location.reload(true);*/">Actualizar librerías</button>
                          <button type="button" class="list-group-item list-group-item-action" onclick="xdom.devTools.debug();">Debug</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="simplebar-placeholder" style="width: 239px; height: 854px;"></div>
            </div>
          </div>
        </div>
      </div>



    </div>
  </xsl:template>

  <xsl:template mode="sitemap" match="text()"/>

  <xsl:template mode="sitemap" match="sitemap:menu">
    <li class="sidebar-item">
      <a href="#{generate-id()}" data-toggle="collapse" class="text-white sidebar-link collapsed" style="background: #00B0AD; font-size:16px">

        <!--<img class="rounded-circle mr-3" src="./custom/images/EmblemaAyLocal_A.png" alt="{@session:user_login}" width="40" height="20"/>-->
        <!--<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-layout align-middle">
          <rect x="3" y="3" width="18" height="18" rx="2" ry="2"></rect>
          <line x1="3" y1="9" x2="21" y2="9"></line>
          <line x1="9" y1="21" x2="9" y2="9"></line>
        </svg>-->
        <span class="align-middle">
          <xsl:value-of select="@title"/>
        </span>
      </a>
      <ul id="{generate-id()}" class="sidebar-dropdown list-unstyled collapse">
        <xsl:apply-templates mode="sitemap"/>
      </ul>
    </li>
  </xsl:template>

  <xsl:template mode="sitemap" match="sitemap:item|sitemap:catalog">
    <li class="sidebar-item text-white ">
      <strong>
        <a href="#{generate-id((ancestor::sitemap:menu)[1])}" class="text-dark sidebar-link" style=" font-size:16px; background: white;">
          <xsl:attribute name="onclick">
            <xsl:apply-templates mode="sitemap.script" select="."/><![CDATA[;toggle_sidebar();]]>
          </xsl:attribute>
          <xsl:value-of select="@title"/>

        </a>
      </strong>
    </li>
  </xsl:template>

  <xsl:template mode="sitemap" match="sitemap:sitemap">
    <li class="sidebar-header">
      <xsl:value-of select="@title"/>
    </li>
    <xsl:apply-templates mode="sitemap"/>
  </xsl:template>

  <xsl:template mode="sitemap.script" match="sitemap:catalog">
    <xsl:text/>px.request("<xsl:value-of select="@catalogName"/>")<xsl:text/>
  </xsl:template>

  <xsl:template mode="dashboard" match="text()"/>

  <xsl:template mode="dashboard" match="shell:dashboard//producto">
    <div class="col-lg-4">
      <div class="testimonial-item mx-auto mb-5 mb-lg-0">
        <img class="rounded-circle mr-3" src="{img/text()}" width="150" height="150"/>
        <h5>
          <xsl:value-of select="Nombre"/>
        </h5>
        <p class="font-weight-light mb-0">
          <xsl:value-of select="Descripcion"/>
        </p>
      </div>
    </div>
  </xsl:template>

  <xsl:template mode="shell_messages_status" match="*"/>

  <xsl:template mode="shell_messages_status" match="*[@type='new']">
    <xsl:text>bg-success</xsl:text>
  </xsl:template>

  <xsl:template mode="shell_messages_status" match="*[@type='old']">
    <xsl:text>bg-warning</xsl:text>
  </xsl:template>

  <xsl:template mode="shell_messages" match="shell:cart/*">
    <a class="dropdown-item d-flex align-items-center" href="#">
      <div class="mr-3">
        <div>
          <img src="./custom/images/{@pic}" width="40" height="40"/>
        </div>
      </div>
      <div>
        <span class="font-weight-bold">
          <xsl:value-of select="text()"/>
        </span>
      </div>
    </a>
  </xsl:template>

  <xsl:template mode="shell_messages" match="shell:messages//*">
    <a class="dropdown-item d-flex align-items-center" href="#">
      <div class="dropdown-list-image mr-3">
        <img class="rounded-circle" width="50" height="50" src="./custom/images/{@pic}.jpg"  alt=""/>
        <div>
          <xsl:attribute name="class">
            <xsl:text>status-indicator </xsl:text>
            <xsl:apply-templates mode="shell_messages_status" select="."/>
          </xsl:attribute>
        </div>
      </div>
      <div>
        <xsl:if test="@type='new'">
          <xsl:attribute name="class">font-weight-bold</xsl:attribute>
        </xsl:if>
        <div class="text-truncate">
          <xsl:value-of select="text()"/>
        </div>
        <div class="small text-gray-500">
          <xsl:value-of select="@user"/> · 58m
        </div>
      </div>
    </a>
  </xsl:template>

</xsl:stylesheet>
