﻿<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:msxsl="urn:schemas-microsoft-com:xslt"
	xmlns:math="http://exslt.org/math"
	xmlns:exsl="http://exslt.org/functions"
	xmlns:set="http://exslt.org/sets"
	xmlns:session="http://panax.io/session"
  xmlns:x="http://panax.io/xdom"
  xmlns:xdom="http://panax.io/xdom"
  xmlns:custom="http://panax.io/custom"
  xmlns:prev="http://panax.io/xdom/values/previous"
  xmlns:initial="http://panax.io/xdom/values/initial"
  xmlns:source="http://panax.io/xdom/binding/source"
  xmlns:bind="http://panax.io/xdom/binding"
  xmlns:query="http://panax.io/xdom/binding/query"
  xmlns:xhr="http://panax.io/xdom/xhr"
  xmlns:js="http://panax.io/languages/javascript"
  xmlns:router="http://panax.io/xdom/router"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xmlns="http://www.w3.org/1999/xhtml"
  extension-element-prefixes="math exsl msxsl"
  exclude-result-prefixes="xsl msxsl math exsl set session x xdom custom prev initial source query xhr js router xsi bind"
>


  <xsl:output method="xml" omit-xml-declaration="yes" indent="yes" standalone="no"/>

  <xsl:template match="/*">


    <div class="d-flex justify-content-center h-100">


  <div class="user_card" style="height: 550px; width: 350px;margin-top: auto;margin-bottom: auto;background: white;position: relative;display: flex;
			justify-content: center;flex-direction: column;	padding: 10px;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);-webkit-box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
			-moz-box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
			border-radius: 8px;">
    
					<div class="d-flex justify-content-center h-100">
						<div class="user_card" style="height: 450px; width: 350px;margin-top: auto;margin-bottom: auto;background: white;position: relative;display: flex;	justify-content: center;flex-direction: column;	padding: 10px;">
							<div class="d-flex justify-content-center">
								<div class="brand_logo_container" style="	position: absolute; height: 170px; width: 170px; top: 10px; padding: 10px;	text-align: center;">
									<img src="custom/images/EmblemaAyLocal.png" class="brand_logo" style=" height: 130px; width: 130px; " alt="Logo" />
								</div>
							</div>
							<div class="d-flex justify-content-center form_container" style="margin-top: 100px;">
								<form>
									<div class="input-group mb-3">
										<div class="input-group-append">
											<span class="input-group-text" style="background: #49CCBE !important;color: white !important;border: 0 !important;border-radius: 0.25rem 0 0 0.25rem !important;">
												<i class="fas fa-user"></i>
											</span>
										</div>
										<input type="text" id="username" class="form-control input_user" value="" placeholder="Usuario">

										<xsl:attribute name="onkeyup">
											if (event.keyCode !== 13) {return;}; event.preventDefault(); login_button.click()
										</xsl:attribute>

										</input>
									</div>

									<div class="input-group mb-2">
										<xsl:if test="//@session:status='authorizing'">
											<xsl:attribute name="style">visibility:hidden;</xsl:attribute>
										</xsl:if>
										<div class="input-group-append">
											<span class="input-group-text" style="background: #49CCBE !important;color: white !important;border: 0 !important;border-radius: 0.25rem 0 0 0.25rem !important;">
												<i class="fas fa-key"></i>
											</span>
										</div>
										<input type="password" id="password" class="form-control input_pass" value="" placeholder="password">

										<xsl:attribute name="onkeyup">if (event.keyCode !== 13 || !login_button) {return;}; event.preventDefault(); login_button.click()</xsl:attribute>

										</input>
									</div>
									<div class="form-group">
										<div class="custom-control custom-checkbox">
											<input type="checkbox" class="custom-control-input" id="customControlInline" />
											<label class="custom-control-label" for="customControlInline">Recordarme</label>
										</div>
									</div>
									<div class="d-flex justify-content-center mt-3 login_container" style="padding: 0 2rem;">
                    <!--<input type="button" value = "Test the alert" onclick="alert('CONTRASEÑA INVALIDA');" />-->
                       <!-- Button trigger modal -->

            
										<button type="button" value = "Test the alert" class="btn login_btn" style="width: 100%;background: #49CCBE !important;color: white !important;" onclick="var username=document.getElementById('username'); var password=document.getElementById('password'); xdom.session.login(username.value, calcMD5(password.value), 'main');" id="login_button">
											<xsl:choose>
												<xsl:when test="//@session:status='authorizing'">
													Autorizando... <i class="fas fa-spinner fa-spin"></i>
												</xsl:when>
												<xsl:otherwise>Ingresar</xsl:otherwise>
											</xsl:choose>
										</button>
									</div>
								</form>
							</div>

							<div class="mt-4">
								<div class="d-flex justify-content-center links">
									¿No tienes una cuenta? <a href="#" class="ml-2">Crea una cuenta</a>
								</div>
								<div class="d-flex justify-content-center links">
									<a href="#">¿Olvidaste tu contraseña?</a>
								</div>
							</div>
						</div>
					</div>


</div>

      <xsl:apply-templates select=".//x:message"/>
    </div>



  </xsl:template>
</xsl:stylesheet>