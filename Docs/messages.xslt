﻿<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:msxsl="urn:schemas-microsoft-com:xslt"
  xmlns:debug="http://panax.io/debug"
  xmlns:x="http://panax.io/xdom"
  xmlns:xhr="http://panax.io/xdom/xhr"
  xmlns="http://www.w3.org/1999/xhtml"
  exclude-result-prefixes="xsl msxsl x debug xhr"
>
  <xsl:output method="xml"
     omit-xml-declaration="yes"
     indent="yes" standalone="no"/>

  <xsl:template match="x:message">
    <style>
      <![CDATA[
     .messages{ 
                position: fixed;
                top: 0;
                bottom: 0;
                left: 0;
                right: 0;  
                background: rgba(0, 0, 0, 0.7);
                
                 }


     .pop_up {
                 margin: 290px auto !important;
                  padding: 20px;
                  background: #fff;
                  border-radius: 5px;
                  width: 30%;
                  position: relative;}
   
  
  .w3-button{ position: absolute;
  top: 20px;
  right: 30px;
  transition: all 200ms;
  font-size: 50px !important;
  font-weight: bold;
  text-decoration: none;
  color: #fff;
   background: none !important; }
   
   .w3-button:hover{
    color: #fff !important;
   background: none !important;}

    ]]>
    </style>

    
      <div class="{@type}">
      <div class="messages">  
        <span onclick="this.parentElement.style.display='none'; xdom.data.remove('{@x:id}')"
        class="w3-button w3-large w3-display-topright" >&#215;</span>
        <div class="pop_up">
          <h3>¡Aviso!</h3>
        <p>
          <xsl:value-of select="."/>
        </p>
        </div>
      </div>
    </div>
  </xsl:template>

  <xsl:template match="x:message" mode="messages.popover">
    <span style="position:relative">
      <div class="popover {@type} fade show bs-popover-right" role="tooltip" x-placement="right" style="position: absolute; will-change: transform; top: 0px; left: 0px; margin-top: -45px; width:20px;">
        <div class="arrow" style="top: 34px;"></div>
        <h3 class="popover-header" onclick="this.parentElement.style.display='none'; xdom.data.remove('{@x:id}')">Mensaje</h3>
        <div class="popover-body">
          <xsl:value-of select="text()"/>
        </div>
      </div>
    </span>
  </xsl:template>
</xsl:stylesheet>
