<xsl:stylesheet id="panax_installer" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:msxsl="urn:schemas-microsoft-com:xslt"
	xmlns="http://www.panaxit.com/release"
	xmlns:px="http://www.panaxit.com/release"
	xmlns:metadata="http://www.panaxit.com/xddl/metadata"
	xmlns:ddl="http://www.panaxit.com/xddl"
	xmlns:return="http://www.panaxit.com/xddl/returntypes"
	xmlns:mssql="http://www.panaxit.com/xddl/mssqlserver"
	xmlns:oracle="http://www.panaxit.com/xddl/oracle"
	xmlns:session="http://www.panaxit.com/xddl/session"
	xmlns:doc="http://www.panaxit.com/documentation/v1"
	xmlns:x="http://www.w3.org/2001/XMLSchema-instance"
	version="1.0" exclude-result-prefixes="ddl msxsl session doc ddl">
	<xsl:output method="text"/>
	<xsl:strip-space elements="*"/>
	<xsl:variable name="smallcase" select="'abcdefghijklmnopqrstuvwxyz'" />
	<xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />
	<xsl:key name="identity_key" match="ddl:Table/ddl:Columns/ddl:Column[@Name=../../@IdentityKey]" use="generate-id(.)"/>
	<xsl:key name="schemas" match="ddl:*[@Schema!='']" use="/*"/>
	<xsl:key name="schemas" match="ddl:*[@DefaultSchema!='']" use="/*"/>
	<xsl:key name="schemas" match="ddl:*[@Schema!='']" use="@Schema"/>
	<xsl:key name="schemas" match="ddl:*[@DefaultSchema!='']" use="@DefaultSchema"/>
	<xsl:key name="schemas_explicit" match="ddl:Schema" use="@Name"/>
	<xsl:key name="nodesToProcess" match="text()|processing-instruction()" use="generate-id(..)"/>
	<xsl:key name="nodesToProcess" match="ddl:*" use="generate-id(..)"/>
	<xsl:key name="nodesToProcess" match="session:*" use="generate-id(..)"/>
	<xsl:key name="nodesToProcess" match="*[namespace-uri()=namespace-uri(/*)]" use="generate-id(..)"/>
	<!--<xsl:key name="nodesToProcess" match="/*//*[substring-before(@x:type, ':')=name(/*/namespace::*[.=namespace-uri(/*)][concat(name(),':',.)!=concat(substring-before(name(/*),':'),':',namespace-uri(/*))][1])]" use="generate-id(..)"/>-->
	
	<xsl:key name="nodesToProcess" match="/*[not(@mode)]/px:*" use="generate-id(..)"/>
	<xsl:key name="nodesToProcess" match="/*[@mode='setup']/px:Setup" use="generate-id(..)"/>
	<xsl:key name="nodesToProcess" match="/*[@mode='install']/px:*[not(local-name()='Setup')]" use="generate-id(..)"/>
	
	<!--Retrieve all attributes for selected dbms-->
	<xsl:key name="dbmsAttribute" match="@*[namespace-uri()=namespace-uri(/*)]" use="generate-id(..)"/>
	<xsl:template match="text()" mode="escape">
		<xsl:call-template name="escape-xml">
			<xsl:with-param name="text" select="." />
		</xsl:call-template>
	</xsl:template>
	<xsl:template match="processing-instruction()" mode="escape">
		<xsl:text>&lt;?</xsl:text>
		<xsl:value-of select="name()" />
		<xsl:text />
		<xsl:call-template name="escape-xml">
			<xsl:with-param name="text" select="." />
		</xsl:call-template>
		<xsl:text>?&gt;</xsl:text>
	</xsl:template>

	<xsl:template name="escape-xml">
		<xsl:param name="text" />
		<xsl:if test="$text != ''">
			<xsl:variable name="head" select="substring($text, 1, 1)" />
			<xsl:variable name="tail" select="substring($text, 2)" />
			<xsl:choose>
				<xsl:when test="$head = '&amp;'">&amp;amp;</xsl:when>
				<xsl:when test="$head = '&lt;'">&amp;lt;</xsl:when>
				<xsl:when test="$head = '&gt;'">&amp;gt;</xsl:when>
				<xsl:when test="$head = '&quot;'">&amp;quot;</xsl:when>
				<xsl:when test="$head = &quot;'&quot;">&amp;apos;</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$head" />
				</xsl:otherwise>
			</xsl:choose>
			<xsl:call-template name="escape-xml">
				<xsl:with-param name="text" select="$tail" />
			</xsl:call-template>
		</xsl:if>
	</xsl:template>

	<xsl:template name="escape-apos">
		<xsl:param name="string" />
		<xsl:choose>
			<xsl:when test='contains($string, "&apos;")'>
				<xsl:value-of select='substring-before($string, "&apos;")' />
				<xsl:text>''</xsl:text>
				<xsl:call-template name="escape-apos">
					<xsl:with-param name="string"
					   select='substring-after($string, "&apos;")' />
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$string" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="/*">
		<xsl:variable name="curr" select="."/>
		<xsl:element name="statement">
			<!--/*<xsl:value-of select="substring-before(name(/*),':')"/>::<xsl:value-of select="namespace-uri(/*)"/>*/
			/*<xsl:value-of select="name(/*/namespace::*[.=namespace-uri(/*)][concat(name(),':',.)!=concat(substring-before(name(/*),':'),':',namespace-uri(/*))][1])"/>*/
			<xsl:for-each select="/*/namespace::*[.=namespace-uri(/*)][concat(name(),':',.)!=concat(substring-before(name(/*),':'),':',namespace-uri(/*))]">
				<xsl:value-of select="concat(' ',name(),':',.)"/>
				<xsl:text>.
</xsl:text>
			</xsl:for-each>-->
			<xsl:apply-templates mode="process" select="key('nodesToProcess',generate-id(.))"/>
		</xsl:element>
	</xsl:template>

	<xsl:template mode="create" match="*">
		<xsl:text>/*--NO CREATE DEFINITION FOR </xsl:text>
		<xsl:value-of select="name()"/>
		<xsl:text>*/
</xsl:text>
	</xsl:template>

	<xsl:template mode="process" match="*">
		<xsl:text>/*--NO SPECIFIC DEFINITION FOR </xsl:text>
		<xsl:value-of select="name()"/>
		<xsl:text>*/
</xsl:text>
		<xsl:apply-templates mode="process" select="key('nodesToProcess',generate-id(.))"/>
	</xsl:template>

	<!--<xsl:template mode="process" match="ddl:Create/*/*"/>
	-->
	<!--Por default, los elementos de un elemento hijo de un create no se procesa, éste debe ser procesado por medio de cada nodo.-->

	<xsl:template mode="process" match="ddl:Definition|px:*|session:*|ddl:Set|ddl:Value|ddl:Scripts|ddl:Settings">
		<xsl:apply-templates mode="process" select="key('nodesToProcess',generate-id(.))"/>
	</xsl:template>

	<xsl:template mode="process" match="text()|processing-instruction()">
		<xsl:copy-of select="." />
	</xsl:template>

	<xsl:template mode="process" match="session:DATABASE">
		<xsl:text>[</xsl:text>
		<xsl:value-of select="/*/@targetDB" />
		<xsl:text>]</xsl:text>
	</xsl:template>

	<xsl:template mode="process" match="ddl:Concat">
		<xsl:for-each select="key('nodesToProcess',generate-id(.))">
			<xsl:if test="position()&gt;1">+</xsl:if>
			<xsl:apply-templates mode="process" select="."/>
		</xsl:for-each>
	</xsl:template>

	<xsl:template mode="process" match="ddl:When|ddl:Then|ddl:Else|ddl:From">
		<xsl:value-of select="concat(' ',translate(local-name(), $smallcase, $uppercase),' ')"/>
		<xsl:apply-templates mode="process" select="key('nodesToProcess',generate-id(.))"/>
	</xsl:template>

	<xsl:template mode="process" match="ddl:Case">
		<xsl:text>CASE</xsl:text>
		<xsl:apply-templates mode="process" select="key('nodesToProcess',generate-id(.))"/>
		<xsl:text> END</xsl:text>
	</xsl:template>

	<xsl:template mode="process" match="ddl:Not">
		<xsl:text>NOT(</xsl:text>
		<xsl:apply-templates mode="process" select="key('nodesToProcess',generate-id(.))"/>
		<xsl:text>)</xsl:text>
	</xsl:template>

	<xsl:template mode="process" match="ddl:SystemFunction">
		<xsl:apply-templates mode="function_name" select="."/>
		<xsl:text>(</xsl:text>
		<xsl:for-each select="key('nodesToProcess',generate-id(.))">
			<xsl:if test="position()&gt;1">, </xsl:if>
			<xsl:apply-templates mode="process" select="."/>
		</xsl:for-each>
		<xsl:text>)</xsl:text>
	</xsl:template>

	<xsl:template mode="process" match="ddl:Equals">
		<xsl:apply-templates mode="process" select="key('nodesToProcess',generate-id(.))[1]"/>
		<xsl:apply-templates mode="operator" select="."/>
		<xsl:apply-templates mode="process" select="key('nodesToProcess',generate-id(.))[2]"/>
	</xsl:template>

	<xsl:template mode="operator" match="ddl:Equals">
		<xsl:text>=</xsl:text>
	</xsl:template>

	<!--<xsl:template mode="operator" match="ddl:Not/ddl:Equals">
		<xsl:text><![CDATA[<>]]></xsl:text>
	</xsl:template>-->

	<!--<xsl:template mode="process" match="/*[string(@targetDB)!='']//session:DATABASE">
		<xsl:text>[</xsl:text>
		<xsl:value-of select="/*/@targetDB" />
		<xsl:text>]</xsl:text>
	</xsl:template>-->

	<xsl:template mode="drop" match="*">
		<xsl:param name="object_name"/>
		IF <xsl:apply-templates mode="object.checkExistence" select=".">
			<xsl:with-param name="object_name" select="$object_name"/>
		</xsl:apply-templates> BEGIN
		DROP <xsl:value-of select="concat(translate(local-name(), $smallcase, $uppercase),' ')"/> <xsl:apply-templates mode="format_object_name" select="."/>
		END
		GO
	</xsl:template>

	<xsl:template mode="drop" match="ddl:Schemas">
		<xsl:param name="object_name"/>
		IF <xsl:apply-templates mode="object.checkExistence" select=".">
			<xsl:with-param name="object_name" select="$object_name"/>
		</xsl:apply-templates> BEGIN
		DROP SCHEMA [<xsl:value-of select="$object_name"/>]
		END
		GO
	</xsl:template>

	<xsl:template mode="drop" match="ddl:DataType">
		<xsl:param name="object_name"/>
		IF <xsl:apply-templates mode="object.checkExistence" select=".">
			<xsl:with-param name="object_name" select="$object_name"/>
		</xsl:apply-templates> BEGIN
		DROP TYPE  <xsl:apply-templates mode="format_object_name" select="."/>
		END
		GO
	</xsl:template>

	<xsl:template mode="if_not_exists" match="ddl:Create//*">
		<xsl:param name="object_name"/>
		<xsl:param name="content"/>
		<xsl:value-of select="$content"/>
	</xsl:template>

	<xsl:template mode="if_not_exists" match="ddl:Create[@IfExists='skip']//*">
		<xsl:param name="object_name"/>
		<xsl:param name="content"/>
		IF NOT <xsl:apply-templates mode="object.checkExistence" select=".">
			<xsl:with-param name="object_name" select="$object_name"/>
		</xsl:apply-templates> BEGIN
		<xsl:apply-templates mode="script" select=".">
			<xsl:with-param name="content">
				<xsl:value-of select="$content"/>
			</xsl:with-param>
		</xsl:apply-templates>
		END
	</xsl:template>

	<xsl:template mode="if_not_exists" match="
		ddl:Create[@IfExists='skip']//ddl:DataType|
		ddl:Create[@IfExists='skip']//ddl:User
">
		<xsl:param name="object_name"/>
		<xsl:param name="content"/>
		IF NOT <xsl:apply-templates mode="object.checkExistence" select=".">
			<xsl:with-param name="object_name" select="$object_name"/>
		</xsl:apply-templates> BEGIN
		<xsl:value-of select="$content"/>
		END
	</xsl:template>

	<xsl:template mode="if_not_exists" match="
		ddl:Create[@IfExists='drop']//*
">
		<xsl:param name="object_name"/>
		<xsl:param name="content"/>
		<xsl:apply-templates mode="drop" select="ddl:Synonyms/ddl:Synonym"/>
		<xsl:apply-templates mode="drop" select=".">
			<xsl:with-param name="object_name" select="$object_name"/>
		</xsl:apply-templates>
		<xsl:value-of select="$content"/>
	</xsl:template>

	<!--<xsl:template mode="process" match="mssql:*|oracle:*">
	</xsl:template>-->

	<!--<xsl:template mode="process" match="ddl:Execute|ddl:Declare|ddl:Definition|ddl:Set|ddl:Schemas|ddl:Create">
		<xsl:apply-templates mode="process" select="key('nodesToProcess',generate-id(.))"/>
	</xsl:template>-->


	<xsl:template mode="batch_block" match="*">
		<xsl:param name="content"/>
		<xsl:value-of select="$content"/>
		<xsl:text>
GO
</xsl:text>
	</xsl:template>

	<!--<xsl:template mode="batch_block" match="*[local-name()='Script']//*[local-name()='Script']|ddl:Procedure//ddl:Procedure|ddl:Procedure//*[local-name()='Script']|ddl:Function//*[local-name()='Script']">
		-->
	<!--Un script dentro de otro script no tiene la cláusla "GO"-->
	<!--
		<xsl:param name="content"/>
		<xsl:value-of select="$content"/>
	</xsl:template>-->

	<xsl:template mode="batch_block" match="*[local-name()='Batch']//*|ddl:Definition//*">
		<!--Cualquier elemento dentro de un Batch, no lleva la cláusla go "GO"-->
		<xsl:param name="content"/>
		<xsl:value-of select="$content"/>
	</xsl:template>

	<xsl:template mode="process" match="ddl:Execute">
		<xsl:for-each select="key('nodesToProcess',generate-id(.))">
			<xsl:apply-templates mode="batch_block" select=".">
				<xsl:with-param name="content">
					<xsl:apply-templates mode="process" select="."/>
				</xsl:with-param>
			</xsl:apply-templates>
		</xsl:for-each>
	</xsl:template>

	<xsl:template mode="process" match="*[local-name()='Batch']|ddl:Batch|mssql:Batch">
		<xsl:apply-templates mode="batch_block" select=".">
			<xsl:with-param name="content">
				<xsl:apply-templates mode="process" select="key('nodesToProcess',generate-id(.))"/>
			</xsl:with-param>
		</xsl:apply-templates>
	</xsl:template>

	<xsl:template mode="process" match="ddl:Function">
		<xsl:apply-templates mode="format_object_name" select="."/>
		<xsl:text>(</xsl:text>
		<xsl:apply-templates mode="process" select="ddl:Parameter"/>
		<xsl:text>)</xsl:text>
	</xsl:template>

	<xsl:template mode="process" match="ddl:Procedure">
		<!--/*[namespace-uri()='http://www.panaxit.com/xddl/mssqlserver']//ddl:Execute/ddl:Procedure-->
		EXEC <xsl:apply-templates mode="format_object_name" select="."/>
		<xsl:apply-templates mode="process" select="ddl:Parameter"/>
	</xsl:template>

	<xsl:template mode="process" match="ddl:Variable[@Name]">
		<xsl:value-of select="@Name"/>
	</xsl:template>

	<xsl:template mode="process" match="ddl:Parameter">
		<xsl:if test="position()&gt;1">, </xsl:if>
		<!--<xsl:text>@</xsl:text>-->
		<xsl:if test="@Name">
			<xsl:value-of select="@Name"/>
			<xsl:if test="text()|*">
				<xsl:text>=</xsl:text>
			</xsl:if>
		</xsl:if>
		<xsl:apply-templates mode="process" select="key('nodesToProcess',generate-id(.))"/>
		<!--<xsl:choose>
			<xsl:when test="string(text())!=''">
				<xsl:value-of select="text()"/>
			</xsl:when>
			<xsl:otherwise>DEFAULT</xsl:otherwise>
		</xsl:choose>-->
		<xsl:if test="@Output='true'">
			<xsl:text> OUTPUT</xsl:text>
		</xsl:if>
	</xsl:template>

	<xsl:template mode="systemobject" match="*|text()"/>

	<xsl:template mode="systemobject" match="*[namespace-uri(/*)='http://www.panaxit.com/xddl/mssqlserver']">
		EXEC #Panax.SetExtendedProperty '<xsl:apply-templates mode="format_object_name" select="."/>', 'microsoft_database_tools_support', '1'
	</xsl:template>

	<xsl:template mode="systemobject" match="ddl:User[namespace-uri(/*)='http://www.panaxit.com/xddl/mssqlserver']">
		<!--EXEC #Panax.SetExtendedProperty '<xsl:apply-templates mode="format_object_name" select="."/>', 'microsoft_database_tools_support', '1', 'USER'-->
	</xsl:template>

	<xsl:template mode="systemobject" match="ddl:DataType[namespace-uri(/*)='http://www.panaxit.com/xddl/mssqlserver']">
		<!--EXEC #Panax.SetExtendedProperty '<xsl:apply-templates mode="format_object_name" select="."/>', 'microsoft_database_tools_support', '1', 'TYPE'-->
	</xsl:template>

	<xsl:template mode="systemobject" match="ddl:Schema[namespace-uri(/*)='http://www.panaxit.com/xddl/mssqlserver']">
		<!--EXEC #Panax.SetExtendedProperty '<xsl:apply-templates mode="format_object_name" select="."/>', 'microsoft_database_tools_support', '1', 'SCHEMA'-->
	</xsl:template>

	<xsl:template mode="persist" match="*|text()"/>

	<xsl:template mode="persist" match="ddl:Create/*[@Persists='false']">
		EXEC #Panax.SetExtendedProperty '<xsl:apply-templates mode="format_object_name" select="."/>', 'px:IsPersistent', NULL
	</xsl:template>

	<xsl:template mode="persist" match="ddl:Create/*[@Persists='true']">
		EXEC #Panax.SetExtendedProperty '<xsl:apply-templates mode="format_object_name" select="."/>', 'px:IsPersistent', '1'
	</xsl:template>

	<xsl:template mode="metadata" match="metadata:*">
		EXEC #Panax.SetExtendedProperty '<xsl:apply-templates mode="format_object_name" select=".."/>', '<xsl:value-of select="@Name"/>', '<xsl:value-of select="."/>'
	</xsl:template>

	<xsl:template mode="create.block" match="*">
		<xsl:apply-templates mode="batch_block" select=".">
			<xsl:with-param name="content">
				<xsl:apply-templates mode="if_not_exists" select=".">
					<xsl:with-param name="content">
						<xsl:apply-templates mode="create" select="."/>
					</xsl:with-param>
				</xsl:apply-templates>
			</xsl:with-param>
		</xsl:apply-templates>
		<xsl:apply-templates mode="batch_block" select=".">
			<xsl:with-param name="content">
				<xsl:apply-templates mode="persist" select="."/>
				<xsl:apply-templates mode="systemobject" select="."/>
				<xsl:apply-templates mode="metadata" select="metadata:*"/>
			</xsl:with-param>
		</xsl:apply-templates>
		<xsl:apply-templates mode="create.block" select="ddl:Synonyms/ddl:Synonym"/>
	</xsl:template>


	<xsl:template mode="create.block" match="ddl:Create/ddl:Schemas[@x:type='auto']">
		<xsl:variable name="currentNode" select="current()"/>
		/*Definición automática de schemas en base a los objetos declarados: */
		<xsl:for-each select="key('schemas',/*)[not(key('schemas_explicit',@Schema))][not(key('schemas_explicit',@DefaultSchema))][count(.|key('schemas',@Schema)[1]|key('schemas',@DefaultSchema)[1])=1]">
			<xsl:apply-templates mode="batch_block" select="$currentNode">
				<xsl:with-param name="content">
					<xsl:apply-templates mode="if_not_exists" select="$currentNode">
						<xsl:with-param name="object_name" select="@Schema|@DefaultSchema"/>
						<xsl:with-param name="content">
							<xsl:call-template name="create.Schema">
								<xsl:with-param name="Name" select="@Schema|@DefaultSchema"/>
							</xsl:call-template>
						</xsl:with-param>
					</xsl:apply-templates>
				</xsl:with-param>
			</xsl:apply-templates>
		</xsl:for-each>
	</xsl:template>

	<xsl:template mode="create.block" match="ddl:Schemas">
		<xsl:apply-templates mode="create.block" select="key('nodesToProcess',generate-id(.))"/>
	</xsl:template>

	<xsl:template mode="process" match="ddl:Create">
		<xsl:apply-templates mode="create.block" select="key('nodesToProcess',generate-id(.))"/>
	</xsl:template>

	<!--<xsl:template mode="process" match="ddl:Create/ddl:Schemas/ddl:Schema" name="process.schema">
		<xsl:param name="Name" select="@Name"/>
		<xsl:param name="Owner">
			<xsl:choose>
				<xsl:when test="string(@Owner)!=''">
					<xsl:value-of select="@Owner"/>
				</xsl:when>
				<xsl:otherwise>Panax</xsl:otherwise>
			</xsl:choose>
		</xsl:param>
		/*processing: <xsl:value-of select="name(.)"/>*/
		IF NOT <xsl:call-template name="schema.checkExistence">
			<xsl:with-param name="Name" select="$Name"/>
		</xsl:call-template> BEGIN
		<xsl:apply-templates mode="script" select=".">
			<xsl:with-param name="content">
				<xsl:apply-templates mode="create" select="."/>
			</xsl:with-param>
		</xsl:apply-templates>
		END
		<xsl:text>GO
</xsl:text>
	</xsl:template>-->


	<!--<xsl:template mode="process" match="
		ddl:Procedure//ddl:Create/*|
		ddl:Create/ddl:Procedure">
		<xsl:apply-templates mode="script" select=".">
			<xsl:with-param name="content">
				<xsl:apply-templates mode="create" select="."/>
			</xsl:with-param>
		</xsl:apply-templates>
	</xsl:template>-->

	<xsl:template mode="process" match="ddl:Execute/*[local-name()='Script']/ddl:Create/*">
		<xsl:apply-templates mode="create" select="."/>
	</xsl:template>

	<!--<xsl:template mode="process" match="px:InitialConfig">
		<xsl:apply-templates mode="process" select="key('nodesToProcess',generate-id(.))"/>
	</xsl:template>-->

	<xsl:template mode="process" match="ddl:Parameters">
	</xsl:template>

	<xsl:template mode="process" match="ddl:Set/ddl:Variable">
		<xsl:apply-templates mode="define.set" select="."/>
	</xsl:template>

	<xsl:template mode="process" match="ddl:Declare[ddl:Variable]">
		DECLARE <xsl:apply-templates mode="process" select="*"/>
		<xsl:text>;
</xsl:text>
		<xsl:apply-templates mode="define.set" select="."/>
	</xsl:template>

	<xsl:template mode="process" match="ddl:Declare/ddl:Variable">
		<xsl:if test="position()&gt;1">, </xsl:if>
		<xsl:value-of select="@Name"/>
		<xsl:apply-templates mode="dataType" select="."/>
	</xsl:template>

	<xsl:template mode="process" match="ddl:Cast">
		<xsl:text>CONVERT(</xsl:text>
		<xsl:apply-templates mode="dataType" select="."/>
		<xsl:text>, </xsl:text>
		<xsl:apply-templates mode="process" select="key('nodesToProcess',generate-id(.))"/>
		<xsl:apply-templates mode="function.options" select="key('dbmsAttribute',generate-id(.))"/>
		<xsl:text>)</xsl:text>
	</xsl:template>

	<xsl:template mode="process" match="*[local-name()='Script']">
		<!--/*<xsl:value-of select="@x:type"/>*/-->
		<xsl:apply-templates mode="process" select="key('nodesToProcess',generate-id(.))"/>
	</xsl:template>

	<xsl:template mode="process" match="*[local-name()='Script'][ddl:Parameters or ddl:Create]">
		<!--[@x:type='object']-->
		IF NOT <xsl:apply-templates mode="object.checkExistence" select="ddl:Create/*"/> BEGIN
		<xsl:text>EXEC sp_executesql N'</xsl:text>
		<xsl:call-template name="escape-apos">
			<xsl:with-param name="string">
				<xsl:apply-templates mode="process" select="key('nodesToProcess',generate-id(ddl:Create))|text()"/>
			</xsl:with-param>
		</xsl:call-template>
		<xsl:text>'</xsl:text>
		<xsl:apply-templates mode="procedure.parameters.definition" select="ddl:Parameters"/>
		END
	</xsl:template>

	<xsl:template mode="select.top" match="*"/>

	<xsl:template mode="select.top" match="ddl:From[@Top]">
		<xsl:text> TOP </xsl:text>
		<xsl:value-of select="concat(@Top,' ')"/>
	</xsl:template>

	<xsl:template mode="define.set" match="ddl:Declare/ddl:Variable[text()|*]|ddl:Set/ddl:Variable">
		<xsl:choose>
			<xsl:when test="position()=1">
				SELECT <xsl:apply-templates mode="select.top" select="../ddl:From"/>
			</xsl:when>
			<xsl:otherwise>, </xsl:otherwise>
		</xsl:choose>
		<xsl:value-of select="@Name"/>
		<xsl:text>=</xsl:text>
		<xsl:apply-templates mode="process" select="key('nodesToProcess',generate-id(.))"/>
		<xsl:choose>
			<xsl:when test="../ddl:From"></xsl:when>
			<xsl:when test="position()=last()">
				<xsl:text>;
</xsl:text>
			</xsl:when>
			<xsl:otherwise></xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template mode="function.options" match="*|text()|@*"/>

	<xsl:template mode="function.options" match="ddl:Cast/@mssql:format">
		<xsl:text>, </xsl:text>
		<xsl:value-of select="."/>
	</xsl:template>

	<xsl:template mode="procedure.parameters.definition" match="ddl:Parameters/ddl:Parameter">
		<xsl:if test="position()&gt;1">, </xsl:if>
		<xsl:value-of select="@Name"/>
		<xsl:apply-templates mode="dataType" select="."/>
		<xsl:if test="string(text())!=''">
			<xsl:text>=</xsl:text>
			<xsl:value-of select="text()"/>
		</xsl:if>
		<xsl:if test="@Output='true'">
			<xsl:text> OUTPUT</xsl:text>
		</xsl:if>
	</xsl:template>

	<xsl:template mode="procedure.parameters.definition" match="*[local-name()='Script']/ddl:Parameters">
		<xsl:text>, N'</xsl:text>
		<xsl:call-template name="escape-apos">
			<xsl:with-param name="string">
				<xsl:apply-templates mode="procedure.parameters.definition" select="ddl:Parameter"/>
			</xsl:with-param>
		</xsl:call-template>
		<xsl:text>', </xsl:text>
		<xsl:apply-templates mode="process" select="ddl:Parameter"/>
	</xsl:template>

	<xsl:template mode="table.elements" match="/*[namespace-uri()='http://www.panaxit.com/xddl/mssqlserver']//ddl:Table/ddl:Columns/*">
		<xsl:if test="position()&gt;1">
			<xsl:text>, 
</xsl:text>
		</xsl:if>
		<xsl:apply-templates mode="format_object_name" select="."/>
		<xsl:apply-templates mode="dataType" select="."/>
		<xsl:choose>
			<xsl:when test="key('identity_key',generate-id(.))">
				<xsl:text> IDENTITY(1,1) NOT NULL</xsl:text>
			</xsl:when>
			<xsl:when test="@nullable='true'">
				<xsl:text> NULL</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text> NOT NULL</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template mode="table.elements" match="/*[namespace-uri()='http://www.panaxit.com/xddl/mssqlserver']//ddl:Return/return:Column">
		<xsl:if test="position()&gt;1">
			<xsl:text>, 
</xsl:text>
		</xsl:if>
		<xsl:apply-templates mode="format_object_name" select="."/>
		<xsl:apply-templates mode="dataType" select="."/>
	</xsl:template>

	<xsl:template mode="table.indexes" match="/*[namespace-uri()='http://www.panaxit.com/xddl/mssqlserver']//ddl:Table/ddl:Indexes/ddl:Index">
		GO
		CREATE <xsl:if test="@Unique='true'">
			<xsl:text>UNIQUE </xsl:text>
		</xsl:if>NONCLUSTERED INDEX <xsl:apply-templates mode="format_object_name" select="."/> ON <xsl:apply-templates mode="format_object_name" select="ancestor-or-self::ddl:Table[1]"/>
		(
		<xsl:apply-templates mode="format_object_name" select="ddl:Column"/>
		) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF<xsl:if test="@Unique='true'">, IGNORE_DUP_KEY = OFF</xsl:if>, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON<xsl:if test="@Unique='true'">, FILLFACTOR = 80</xsl:if>) ON [PRIMARY]
	</xsl:template>

	<xsl:template mode="table.elements" match="/*[namespace-uri()='http://www.panaxit.com/xddl/mssqlserver']//ddl:Table/ddl:PrimaryKey|/*[namespace-uri()='http://www.panaxit.com/xddl/mssqlserver']//ddl:Table/ddl:UniqueKeys/ddl:UniqueKey">
		<xsl:text>, 
		CONSTRAINT </xsl:text>
		<xsl:apply-templates mode="format_object_name" select="."/>
		<xsl:apply-templates mode="format_object_name.indextype" select="."/><xsl:if test="string(@Clustered)!='true'">NON</xsl:if><xsl:text>CLUSTERED </xsl:text>
		<xsl:text> (</xsl:text>
		<xsl:apply-templates mode="format_object_name" select="ddl:Column"/>
		) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	</xsl:template>

	<xsl:template mode="function.returns" match="/*[namespace-uri()='http://www.panaxit.com/xddl/mssqlserver']//ddl:Return">
		<xsl:text>RETURNS</xsl:text>
		<xsl:apply-templates mode="dataType" select="."/>
		<xsl:if test="return:Column">
			<xsl:text> (</xsl:text>
			<xsl:apply-templates mode="table.elements" select="return:Column"/>
			<xsl:text>) </xsl:text>
		</xsl:if>
	</xsl:template>

	<xsl:template mode="process" match="/*[namespace-uri()='http://www.panaxit.com/xddl/mssqlserver']//ddl:External/ddl:Assembly">
		<xsl:text> EXTERNAL NAME </xsl:text>
		<xsl:value-of select="@Name"/>
		<xsl:text>.</xsl:text>
		<xsl:value-of select="ddl:Function/@Name"/>
	</xsl:template>

	<xsl:template mode="create" match="ddl:Assembly">
		CREATE ASSEMBLY <xsl:apply-templates mode="format_object_name" select="."/> <xsl:apply-templates mode="process" select="ddl:From"/> <xsl:apply-templates mode="settings" select="."/>
	</xsl:template>

	<xsl:template mode="create" match="ddl:Synonym">
		CREATE SYNONYM <xsl:apply-templates mode="format_object_name" select="."/> FOR <xsl:apply-templates mode="format_object_name" select="../.."/>
	</xsl:template>

	<xsl:template mode="create" match="ddl:DataType">
		CREATE TYPE <xsl:apply-templates mode="format_object_name" select="."/> FROM <xsl:apply-templates mode="dataType" select=".">
			<xsl:with-param name="Type" select="@Base"/>
		</xsl:apply-templates> NOT NULL
	</xsl:template>

	<xsl:template mode="create" match="ddl:Schema" name="create.Schema">
		<xsl:param name="Name" select="@Name"/>
		<xsl:param name="Owner">
			<xsl:choose>
				<xsl:when test="string(@Owner)!=''">
					<xsl:value-of select="@Owner"/>
				</xsl:when>
				<xsl:otherwise>Panax</xsl:otherwise>
			</xsl:choose>
		</xsl:param>
		<xsl:text>CREATE SCHEMA [</xsl:text>
		<xsl:value-of select="$Name"/>
		<xsl:text>] AUTHORIZATION [</xsl:text>
		<xsl:value-of select="$Owner"/>
		<xsl:text>]</xsl:text>
	</xsl:template>

	<xsl:template mode="process" match="ddl:Function/ddl:Return">
		RETURN <xsl:apply-templates mode="process" select="key('nodesToProcess',generate-id(.))"/>
	</xsl:template>

	<xsl:template mode="process" match="ddl:Function/ddl:Return[*[local-name()='Script']]">
		RETURN (<xsl:apply-templates mode="process" select="key('nodesToProcess',generate-id(.))"/>)
	</xsl:template>

	<xsl:template mode="xtype" match="@x:type">
		<xsl:value-of select="."/>
	</xsl:template>

	<xsl:template mode="xtype" match="@x:type[contains(.,':')]">
		<xsl:value-of select="substring-after(.,':')"/>
	</xsl:template>

	<xsl:template mode="create" match="ddl:Procedure|ddl:Function">
		<xsl:param name="operation">CREATE</xsl:param>
		<xsl:value-of select="concat($operation,' ')"/>
		<xsl:value-of select="concat(translate(local-name(.), $smallcase, $uppercase),' ')"/>
		<xsl:apply-templates mode="format_object_name" select="."/>
		<xsl:if test="ddl:Parameters or local-name(.)='Function'">
			<xsl:text>(</xsl:text>
			<xsl:apply-templates mode="procedure.parameters.definition" select="ddl:Parameters"/>
			<xsl:text>) </xsl:text>
		</xsl:if>
		<xsl:apply-templates mode="function.returns" select="ddl:Return"/>
		<xsl:apply-templates mode="settings" select="."/>
		<xsl:text> AS </xsl:text>
		<xsl:apply-templates mode="procedure.definition" select="."/>
	</xsl:template>

	<xsl:template mode="procedure.definition" match="ddl:Procedure|ddl:Function">
		<xsl:text>BEGIN </xsl:text>
		<xsl:apply-templates mode="process" select="ddl:Definition"/>
		<xsl:apply-templates mode="process" select="ddl:Return"/>
		END
	</xsl:template>

	<xsl:template mode="create" match="ddl:External">
		<xsl:param name="operation">CREATE</xsl:param>
		<xsl:value-of select="concat($operation,' ')"/>
		<xsl:variable name="xtype">
			<xsl:apply-templates mode="xtype" select="@x:type"/>
		</xsl:variable>
		<xsl:value-of select="concat(translate($xtype, $smallcase, $uppercase),' ')"/>
		<xsl:apply-templates mode="format_object_name" select="."/>
		<xsl:if test="ddl:Parameters or $xtype='function'">
			<xsl:text>(</xsl:text>
			<xsl:apply-templates mode="procedure.parameters.definition" select="ddl:Parameters"/>
			<xsl:text>) </xsl:text>
		</xsl:if>
		<xsl:apply-templates mode="function.returns" select="ddl:Return"/>
		<xsl:text> AS </xsl:text>
		<xsl:apply-templates mode="procedure.definition" select="."/>
	</xsl:template>

	<xsl:template mode="procedure.definition" match="ddl:External">
		<xsl:apply-templates mode="process" select="ddl:Assembly"/>
	</xsl:template>

	<xsl:template mode="procedure.definition" match="ddl:Function[contains(substring-after(ddl:Return/@x:type,':'),'table')]">
		<xsl:apply-templates mode="process" select="ddl:Return"/>
	</xsl:template>

	<xsl:template mode="create" match="/*[namespace-uri()='http://www.panaxit.com/xddl/mssqlserver']//ddl:User">
		CREATE USER <xsl:value-of select="@Name"/><xsl:if test="@mssql:withoutLogin='true'"> WITHOUT LOGIN</xsl:if>
	</xsl:template>


	<xsl:template mode="create" match="/*[namespace-uri()='http://www.panaxit.com/xddl/mssqlserver']//ddl:Table">
		CREATE TABLE <xsl:apply-templates mode="format_object_name" select="."/>
		(
		<xsl:apply-templates mode="table.elements" select="*"/>
		) ON [PRIMARY]
		<xsl:apply-templates mode="table.indexes" select="*"/>
	</xsl:template>

	<!--mode="dataType"-->
	<xsl:template mode="dataType" match="ddl:*">
		<xsl:param name="Type">
			<xsl:apply-templates mode="xtype" select="@x:type"/>
		</xsl:param>
		<xsl:variable name="dataTypes" select="key('dbmsAttribute',concat(generate-id(.),'::',name(@mssql:dataType))) | key('dbmsAttribute',concat(generate-id(.),'::',name(@oracle:dataType)))"/>
		<!--/*<xsl:value-of select="@x:type"/>*/-->
		<xsl:choose>
			<xsl:when test="$dataTypes">
				<xsl:for-each select="$dataTypes">
					<xsl:value-of select="concat(' ',.)"/>
				</xsl:for-each>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="concat(' ',$Type)"/>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:if test="@Length">
			<xsl:text>(</xsl:text>
			<xsl:choose>
				<xsl:when test="@Decimals">
					<xsl:value-of select="@Length+@Decimals"/>
					<xsl:text>,</xsl:text>
					<xsl:value-of select="@Decimals"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="@Length"/>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:text>)</xsl:text>
		</xsl:if>
	</xsl:template>

	<xsl:template mode="dataType" match="*[@x:type]">
		<xsl:variable name="typeAttribute">
			<xsl:apply-templates mode="dataType.name" select="key('dbmsAttribute',generate-id(.))"/>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="string($typeAttribute)!=''">
				<xsl:value-of select="concat(' ',$typeAttribute)"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="concat(' ',substring-after(@x:type, ':'))"/>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:apply-templates mode="dataType.specific" select="."/>
	</xsl:template>

	<xsl:template mode="dataType.name" match="*/@*"/>

	<xsl:template mode="dataType.name" match="*[@x:type]/@*[contains(concat(local-name(.),'$'),'Type$')]">
		<xsl:value-of select="."/>
	</xsl:template>

	<xsl:template mode="dataType.specific" match="*"/>

	<xsl:template mode="dataType.specific" match="*[@Length]">
		<xsl:text>(</xsl:text>
		<xsl:value-of select="@Length"/>
		<xsl:text>)</xsl:text>
	</xsl:template>

	<xsl:template mode="dataType.specific" match="*[@Precision]">
		<xsl:text>(</xsl:text>
		<xsl:value-of select="@Precision"/>
		<xsl:text>,</xsl:text>
		<xsl:value-of select="@Scale"/>
		<xsl:text>)</xsl:text>
	</xsl:template>

	<!--<xsl:template mode="process" match="/*[namespace-uri()='http://www.panaxit.com/xddl/mssqlserver']//ddl:Procedure/ddl:Definition|/*[namespace-uri()='http://www.panaxit.com/xddl/mssqlserver']//ddl:Function/ddl:Definition">
		<xsl:apply-templates mode="process" select="key('nodesToProcess',generate-id(.))"/>
	</xsl:template>-->
	<!--mode="script"-->
	<xsl:template mode="script" match="*">
		<xsl:param name="content"/>
		<xsl:text>EXEC sp_executesql N'</xsl:text>
		<xsl:call-template name="escape-apos">
			<xsl:with-param name="string">
				<xsl:value-of select="$content"/>
			</xsl:with-param>
		</xsl:call-template>
		<xsl:text>'</xsl:text>
	</xsl:template>

	<!--mode="object.checkExistence"-->
	<xsl:template mode="object.checkExistence" match="*">
		<xsl:param name="object_name"/>
		<xsl:variable name="formated_object_name">
			<xsl:choose>
				<xsl:when test="string($object_name)!=''">
					<xsl:value-of select="$object_name"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates mode="format_object_name" select="."/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:text>EXISTS (SELECT 1 WHERE OBJECT_ID('</xsl:text>
		<xsl:value-of select="$formated_object_name"/>
		<xsl:text>') IS NOT NULL)</xsl:text>
	</xsl:template>

	<xsl:template mode="object.checkExistence" match="ddl:Assembly">
		<xsl:param name="object_name"/>
		<xsl:variable name="formated_object_name">
			<xsl:choose>
				<xsl:when test="string($object_name)!=''">
					<xsl:value-of select="$object_name"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates mode="format_object_name" select="."/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:text>EXISTS (SELECT 1 WHERE ASSEMBLYPROPERTY('</xsl:text>
		<xsl:value-of select="@Name"/>
		<xsl:text>','SimpleName') IS NOT NULL)</xsl:text>
	</xsl:template>

	<xsl:template mode="object.checkExistence" match="ddl:User">
		<xsl:param name="object_name"/>
		<xsl:variable name="formated_object_name">
			<xsl:choose>
				<xsl:when test="string($object_name)!=''">
					<xsl:value-of select="$object_name"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates mode="format_object_name" select="."/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:text>EXISTS (SELECT 1 WHERE USER_ID('</xsl:text>
		<xsl:value-of select="$formated_object_name"/>
		<xsl:text>') IS NOT NULL)</xsl:text>
	</xsl:template>

	<xsl:template mode="object.checkExistence" match="ddl:DataType">
		<xsl:param name="object_name"/>
		<xsl:variable name="formated_object_name">
			<xsl:choose>
				<xsl:when test="string($object_name)!=''">
					<xsl:value-of select="$object_name"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates mode="format_object_name" select="."/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:text>EXISTS (SELECT 1 WHERE TYPE_ID('</xsl:text>
		<xsl:value-of select="$formated_object_name"/>
		<xsl:text>') IS NOT NULL)</xsl:text>
	</xsl:template>

	<xsl:template mode="object.checkExistence" match="ddl:Schema|ddl:Schemas" name="schema.checkExistence">
		<xsl:param name="object_name"/>
		<xsl:variable name="formated_object_name">
			<xsl:choose>
				<xsl:when test="string($object_name)!=''">
					<xsl:value-of select="$object_name"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates mode="format_object_name" select="."/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:text>EXISTS (SELECT 1 WHERE SCHEMA_ID('</xsl:text>
		<xsl:value-of select="$formated_object_name"/>
		<xsl:text>') IS NOT NULL)</xsl:text>
	</xsl:template>

	<!--mode="format_object_name.indextype"-->
	<xsl:template mode="format_object_name.indextype" match="ddl:Table/ddl:PrimaryKey">
		<xsl:text> PRIMARY KEY </xsl:text>
	</xsl:template>

	<xsl:template mode="format_object_name.indextype" match="ddl:Table/ddl:UniqueKeys/ddl:UniqueKey">
		<xsl:text> UNIQUE </xsl:text>
	</xsl:template>

	<!--mode="format_object_name.shortName"-->
	<xsl:template mode="format_object_name.shortName" match="ddl:Table/ddl:PrimaryKey">
		<xsl:text>PK</xsl:text>
	</xsl:template>

	<xsl:template mode="format_object_name.shortName" match="ddl:Table/ddl:Indexes/ddl:*">
		<xsl:text>IX</xsl:text>
	</xsl:template>

	<xsl:template mode="format_object_name.shortName" match="ddl:Table/ddl:UniqueKeys/ddl:*">
		<xsl:text>UK</xsl:text>
	</xsl:template>

	<!--mode="format_object_name"-->
	<xsl:template mode="format_object_name" match="*">
		<xsl:variable name="Schema">
			<xsl:choose>
				<xsl:when test="@Schema">
					<xsl:value-of select="@Schema"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="ancestor::*[@DefaultSchema][1]/@DefaultSchema"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:if test="$Schema">
			<xsl:text>[</xsl:text>
			<xsl:value-of select="$Schema"/>
			<xsl:text>].</xsl:text>
		</xsl:if>
		<xsl:text>[</xsl:text>
		<xsl:value-of select="@Name"/>
		<xsl:text>]</xsl:text>
	</xsl:template>

	<xsl:template mode="format_object_name" match="ddl:Column|return:Column">
		<xsl:text>[</xsl:text>
		<xsl:value-of select="@Name"/>
		<xsl:text>]</xsl:text>
	</xsl:template>

	<xsl:template mode="format_object_name" match="ddl:Synonym">
		<xsl:variable name="Schema">
			<xsl:choose>
				<xsl:when test="@Schema">
					<xsl:value-of select="@Schema"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="ancestor::*[@DefaultSchema][1]/@DefaultSchema"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="Name">
			<xsl:choose>
				<xsl:when test="@Name">
					<xsl:value-of select="@Name"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="../../@Name"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:if test="$Schema">
			<xsl:text>[</xsl:text>
			<xsl:value-of select="$Schema"/>
			<xsl:text>].</xsl:text>
		</xsl:if>
		<xsl:text>[</xsl:text>
		<xsl:value-of select="$Name"/>
		<xsl:text>]</xsl:text>
	</xsl:template>

	<xsl:template mode="format_object_name" match="ddl:User|ddl:Schema|ddl:Assembly">
		<xsl:value-of select="@Name"/>
	</xsl:template>

	<xsl:template mode="format_object_name" match="/*[namespace-uri()='http://www.panaxit.com/xddl/mssqlserver']//ddl:Table/ddl:Indexes/*/ddl:Column|/*[namespace-uri()='http://www.panaxit.com/xddl/mssqlserver']//ddl:Table/ddl:UniqueKeys/*/ddl:Column|ddl:Table/ddl:PrimaryKey/ddl:Column">
		<xsl:text>[</xsl:text>
		<xsl:value-of select="@Name"/>
		<xsl:text>] ASC </xsl:text>
		<xsl:if test="following-sibling::*">
			<xsl:text>
		, </xsl:text>
		</xsl:if>
	</xsl:template>

	<xsl:template mode="format_object_name" match="ddl:Table/ddl:UniqueKeys/ddl:*|ddl:Table/ddl:Indexes/ddl:*|ddl:Table/ddl:PrimaryKey">
		<xsl:text>[</xsl:text>
		<xsl:apply-templates mode="format_object_name.shortName" select="."/>
		<xsl:text>_</xsl:text>
		<xsl:value-of select="translate(ancestor-or-self::ddl:Table[1]/@Name,' ','_')"/>
		<xsl:value-of select="translate(count(preceding-sibling::ddl:*),'0','')"/>
		<xsl:text>] </xsl:text>
	</xsl:template>

	<xsl:template mode="format_object_name" match="ddl:Table/ddl:Indexes/ddl:*[@Name]|ddl:Table/ddl:UniqueKeys/ddl:*[@Name]|ddl:Table/ddl:PrimaryKey[@Name]">
		<xsl:text>[</xsl:text>
		<xsl:value-of select="@Name"/>
		<xsl:text>] </xsl:text>
	</xsl:template>

	<!--mode="function_name"-->
	<xsl:template mode="function_name" match="*">
		<xsl:value-of select="translate(@x:type, $smallcase, $uppercase)"/>
	</xsl:template>

	<xsl:template mode="function_name" match="*[contains(@x:type, ':')]">
		<xsl:value-of select="translate(substring-after(string(@x:type), ':'), $smallcase, $uppercase)"/>
	</xsl:template>

	<xsl:template mode="function_name" match="ddl:SystemFunction[substring-after(string(@x:type), ':')='trim'][@Type='both']">
		<xsl:text>TRIM</xsl:text>
	</xsl:template>

	<xsl:template mode="function_name" match="ddl:SystemFunction[substring-after(string(@x:type), ':')='trim'][@Type='trailing']">
		<xsl:text>RTRIM</xsl:text>
	</xsl:template>

	<xsl:template mode="function_name" match="ddl:SystemFunction[substring-after(string(@x:type), ':')='trim'][@Type='leading']">
		<xsl:text>LTRIM</xsl:text>
	</xsl:template>

	<!--mode="settings"-->
	<xsl:template mode="settings" match="text()|*"/>

	<xsl:template mode="settings" match="/*[namespace-uri()='http://www.panaxit.com/xddl/mssqlserver']//ddl:Create/*">
		<xsl:text> WITH ENCRYPTION </xsl:text>
		<xsl:apply-templates mode="settings" select="key('nodesToProcess',generate-id(.))"/>
	</xsl:template>

	<xsl:template mode="settings" match="/*[namespace-uri()='http://www.panaxit.com/xddl/mssqlserver']//ddl:Create/ddl:Assembly">
		<xsl:text> WITH PERMISSION_SET = SAFE</xsl:text>
		<xsl:apply-templates mode="settings" select="key('nodesToProcess',generate-id(.))"/>
	</xsl:template>

	<xsl:template mode="settings" match="*[ddl:Settings]">
		<xsl:apply-templates mode="settings" select="key('nodesToProcess',generate-id(.))"/>
	</xsl:template>

	<xsl:template mode="settings" match="ddl:Settings">
		<xsl:text> WITH </xsl:text>
		<xsl:apply-templates mode="settings" select="key('nodesToProcess',generate-id(.))"/>
	</xsl:template>

	<xsl:template mode="settings" match="ddl:Settings/*">
		<xsl:if test="position()&gt;1">
			<xsl:text>, </xsl:text>
		</xsl:if>
		<xsl:value-of select="@Name"/>
		<xsl:value-of select="concat(' ',@Operator,' ')"/>
		<xsl:value-of select="."/>
	</xsl:template>
</xsl:stylesheet>