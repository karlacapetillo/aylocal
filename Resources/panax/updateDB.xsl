<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:px="urn:panax" xmlns:set="http://exslt.org/sets" version="1.0" extension-element-prefixes="msxsl" exclude-result-prefixes="set">
	<xsl:key name="keys" match="dataRow/dataField[@isPK='true']" use="generate-id(..)"/>
	<xsl:key name="keys" match="deleteRow/dataField[@isPK='true']" use="generate-id(..)"/>
	<xsl:key name="outputfields" match="dataRow/dataField[@isPK='true' or @out='true']" use="generate-id(..)"/>
	<xsl:key name="outputfields" match="deleteRow/dataField[@isPK='true' or @out='true']" use="generate-id(..)"/>
	<xsl:key name="submitable_fields" match="dataRow[@action='insert']/dataField[.!='NULL']" use="generate-id(..)"/>
	<xsl:key name="submitable_fields" match="dataRow[string(@action)!='insert']/dataField" use="generate-id(..)"/>
	<xsl:key name="submitable_fields" match="foreignTable/dataRow/foreignField" use="generate-id(..)"/>
	<xsl:key name="foreignkey_fields" match="foreignTable/dataRow/foreignField" use="generate-id(..)"/>
	<xsl:key name="foreignkey_fields" match="foreignTable/deleteRow/foreignField" use="generate-id(..)"/>
	<xsl:key name="parentTable" match="dataTable[dataRow/foreignTable]" use="generate-id(dataRow/foreignTable)"/>
	<xsl:key name="parentDataRow" match="dataRow[foreignTable]" use="generate-id(foreignTable)"/>
	<xsl:output method="text" />
	<xsl:variable name="smallcase" select="'abcdefghijklmnopqrstuvwxyz'" />
	<xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />
	<xsl:variable name="userId"/>
	<xsl:template name="set:distinct">
		<xsl:param name="nodes" select="/.." />
		<xsl:param name="distinct" select="/.." />
		<xsl:choose>
			<xsl:when test="$nodes">
				<xsl:call-template name="set:distinct">
					<xsl:with-param name="distinct" select="$distinct | $nodes[1][not(. = $distinct)]" />
					<xsl:with-param name="nodes" select="$nodes[position() &gt; 1]" />
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates select="$distinct" mode="set:distinct" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="node()|@*" mode="set:distinct">
		<!-- <xsl:copy-of select="." /> -->
		<xsl:element name="distinct">
			<xsl:value-of select="." />
		</xsl:element>
	</xsl:template>
	<xsl:template name="replace">
		<xsl:param name="inputString" />
		<xsl:param name="searchText" />
		<xsl:param name="replaceBy" />
		<xsl:choose>
			<xsl:when test="contains($inputString, $searchText)">
				<xsl:value-of disable-output-escaping="yes" select="substring-before($inputString, $searchText)" />
				<xsl:value-of disable-output-escaping="yes" select="$replaceBy" />
				<xsl:call-template name="replace">
					<xsl:with-param name="inputString" select="substring-after($inputString, $searchText)" />
					<xsl:with-param name="searchText" select="$searchText" />
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="$inputString='probando'">
						<xsl:text />
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of disable-output-escaping="yes" select="$inputString" />
						<xsl:text />
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="formatTableName">
		<xsl:param name="inputString" />
		<xsl:choose>
			<xsl:when test="contains($inputString, '.')">
				<xsl:text>[</xsl:text>
				<xsl:value-of disable-output-escaping="yes" select="substring-before($inputString, '.')" />
				<xsl:text>].[</xsl:text>
				<xsl:value-of disable-output-escaping="yes" select="substring-after($inputString, '.')" />
				<xsl:text>]</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>[</xsl:text>
				<xsl:value-of disable-output-escaping="yes" select="$inputString" />
				<xsl:text>]</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="*" mode="escape">
		<!-- Begin opening tag -->
		<xsl:text>&lt;</xsl:text>
		<xsl:value-of select="name()" />
		<!-- Namespaces -->
		<xsl:for-each select="namespace::*[not(local-name(.)='xml')]">
			<xsl:text> xmlns</xsl:text>
			<xsl:if test="name() != ''">
				<xsl:text>:</xsl:text>
				<xsl:value-of select="name()" />
			</xsl:if>
			<xsl:text>="</xsl:text>
			<xsl:call-template name="escape-xml">
				<xsl:with-param name="text" select="." />
			</xsl:call-template>
			<xsl:text>"</xsl:text>
		</xsl:for-each>
		<!-- Attributes -->
		<xsl:for-each select="@*">
			<xsl:value-of select="concat(' ',name())" />
			<xsl:text>="</xsl:text>
			<xsl:call-template name="escape-xml">
				<xsl:with-param name="text" select="." />
			</xsl:call-template>
			<xsl:text>"</xsl:text>
		</xsl:for-each>
		<!-- End opening tag -->
		<xsl:text>&gt;</xsl:text>
		<!-- Content (child elements, text nodes, and PIs) -->
		<xsl:apply-templates select="node()" mode="escape" />
		<!-- Closing tag -->
		<xsl:text>&lt;/</xsl:text>
		<xsl:value-of select="name()" />
		<xsl:text>&gt;</xsl:text>
	</xsl:template>
	<xsl:template match="text()" mode="escape">
		<xsl:call-template name="escape-xml">
			<xsl:with-param name="text" select="." />
		</xsl:call-template>
	</xsl:template>
	<xsl:template match="processing-instruction()" mode="escape">
		<xsl:text>&lt;?</xsl:text>
		<xsl:value-of select="name()" />
		<xsl:text />
		<xsl:call-template name="escape-xml">
			<xsl:with-param name="text" select="." />
		</xsl:call-template>
		<xsl:text>?&gt;</xsl:text>
	</xsl:template>
	<xsl:template name="escape-xml">
		<xsl:param name="text" />
		<xsl:if test="$text != ''">
			<xsl:variable name="head" select="substring($text, 1, 1)" />
			<xsl:variable name="tail" select="substring($text, 2)" />
			<xsl:choose>
				<xsl:when test="$head = '&amp;'">&amp;amp;</xsl:when>
				<xsl:when test="$head = '&lt;'">&amp;lt;</xsl:when>
				<xsl:when test="$head = '&gt;'">&amp;gt;</xsl:when>
				<xsl:when test="$head = '&quot;'">&amp;quot;</xsl:when>
				<xsl:when test="$head = &quot;'&quot;">&amp;apos;</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$head" />
				</xsl:otherwise>
			</xsl:choose>
			<xsl:call-template name="escape-xml">
				<xsl:with-param name="text" select="$tail" />
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
	<xsl:template match="/">
		<xsl:element name="query">
			-- DECLARE @xmlResult XML; SELECT @xmlResult=(SELECT NULL FOR XML PATH('results'))
			SET NOCOUNT ON
			DECLARE @tableName nvarchar(MAX), @ColumnName nvarchar(MAX), @identityTable nvarchar(MAX)
			DECLARE @result xml, @Success bit, @ErrorMessage nvarchar(MAX)
			SET @result=@xmlResult;
			<xsl:apply-templates select="*" />
			SELECT @xmlResult=@result
			--SELECT @xmlResult
		</xsl:element>
	</xsl:template>

	<xsl:template match="root">
		<xsl:apply-templates select="*" />
	</xsl:template>

	<xsl:template match="dataTable|foreignTable">
		<xsl:variable name="table" select="." />
		<xsl:variable name="primaryKey" select="$table/@primaryKey"/>
		<xsl:variable name="identityKey" select="$table/@identityKey"/>
		<xsl:variable name="tableName">
			<xsl:call-template name="formatTableName">
				<xsl:with-param name="inputString" select="$table/@name" />
			</xsl:call-template>
		</xsl:variable>
		SELECT @identityTable=RTRIM(VIT.TABLE_SCHEMA)+'.'+RTRIM(VIT.TABLE_NAME) FROM [$Views].identityTable('<xsl:value-of disable-output-escaping="yes" select="$table/@name" />') VIT;
		IF @identityTable IS NOT NULL BEGIN EXEC [$Metadata].SetExtendedProperty @identityTable, NULL, 'currentUserId', <xsl:value-of select="$userId" />; END;
		<xsl:apply-templates select="dataRow|deleteRow">
			<xsl:with-param name="tableName" select="$tableName"/>
		</xsl:apply-templates>
		<xsl:apply-templates select="foreignTable"/>
		IF @identityTable IS NOT NULL BEGIN EXEC [$Metadata].SetExtendedProperty @identityTable, NULL, 'currentUserId', NULL; END;
		EXEC [$Metadata].SetExtendedProperty '<xsl:value-of disable-output-escaping="yes" select="$table/@name" />', NULL, 'currentUserId', NULL
	</xsl:template>

	<xsl:template match="dataRow|deleteRow">
		<xsl:param name="tableName"/>
		<xsl:variable name="table" select=".." />
		<xsl:variable name="primaryKey" select="$table/@primaryKey"/>
		<xsl:variable name="identityKey" select="$table/@identityKey"/>
		<xsl:variable name="keys">
			<xsl:text>#Success bit</xsl:text>
			<xsl:apply-templates mode="dataRow.primaryKeyFields.declaration" select="."/>
		</xsl:variable>
		<xsl:variable name="row" select="current()"/>
		<xsl:variable name="parentTable" select="key('parentTable',generate-id(..))" />
		<xsl:variable name="parentDataRow" select="key('parentDataRow',generate-id($table))" />
		<xsl:variable name="resultsTable">
			<xsl:text>@#T_</xsl:text>
			<xsl:value-of select="generate-id($row)" />
		</xsl:variable>
		BEGIN TRY
		DECLARE @currentNode_<xsl:value-of select="generate-id($row)" /> XML
		DECLARE @#T_<xsl:value-of select="generate-id($row)"/> TABLE(<xsl:value-of select="$keys"/>)
		DECLARE @#T_<xsl:value-of select="generate-id($row)"/>_Temp TABLE(<xsl:value-of select="$keys"/>)
		<xsl:variable name="action">
			<xsl:choose>
				<xsl:when test="@action">
					<xsl:value-of select="translate(@action, $smallcase, $uppercase)"/>
				</xsl:when>
				<xsl:when test="local-name(.)='deleteRow'">DELETE</xsl:when>
				<xsl:when test="string(@action)='' and (translate(@identityValue, $smallcase, $uppercase)='NULL' or translate(@primaryValue, $smallcase, $uppercase)='NULL')">INSERT</xsl:when>
				<xsl:when test="string(@action)='' and not(translate(@identityValue, $smallcase, $uppercase)='NULL' or translate(@primaryValue, $smallcase, $uppercase)='NULL')">UPDATE</xsl:when>
				<xsl:otherwise>ERROR</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="virtual_table">
			<xsl:choose>
				<xsl:when test="$action='DELETE'">deleted</xsl:when>
				<xsl:otherwise>inserted</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="output_values">
			<xsl:text>1</xsl:text>
			<xsl:if test="$identityKey">
				<xsl:text>, </xsl:text>
				<xsl:value-of select="$virtual_table"/>
				<xsl:text>.[</xsl:text>
				<xsl:value-of select="$identityKey"/>
				<xsl:text>]</xsl:text>
			</xsl:if>
			<xsl:if test="$primaryKey">
				<xsl:text>, </xsl:text>
				<xsl:value-of select="$virtual_table"/>
				<xsl:text>.[</xsl:text>
				<xsl:value-of select="$primaryKey"/>
				<xsl:text>]</xsl:text>
			</xsl:if>
			<xsl:for-each select="key('outputfields', generate-id($row))[@name!=string($primaryKey)]">
				<xsl:text>, </xsl:text>
				<xsl:value-of select="$virtual_table"/>
				<xsl:text>.[</xsl:text>
				<xsl:value-of select="@name"/>
				<xsl:text>]</xsl:text>
			</xsl:for-each>
		</xsl:variable>

		<xsl:choose>
			<xsl:when test="1=0 and string($table/@primaryKey)='' and string($table/@identityKey)=''">RAISERROR ('No se puede guardar si no se define la identity key o la primary key', 16, 1); </xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="$action='DELETE' or dataField or foreignTable">
						<xsl:if test="$action='INSERT'">
							<xsl:text>INSERT INTO 
		</xsl:text>
							<xsl:call-template name="formatTableName">
								<xsl:with-param name="inputString" select="$table/@name" />
							</xsl:call-template>
							<xsl:text> ( </xsl:text>
							<xsl:if test="$table/@foreignKey!=''">
								<xsl:text>[</xsl:text>
								<xsl:value-of select="$table/@foreignKey" />
								<xsl:text>]</xsl:text>
								<xsl:if test="dataField">,</xsl:if>
							</xsl:if>
							<xsl:for-each select="key('submitable_fields',generate-id($row))">
								<xsl:if test="position()&gt;1">,</xsl:if>
								<xsl:text>[</xsl:text>
								<xsl:value-of select="@name" />
								<xsl:text>]</xsl:text>
							</xsl:for-each>
							<xsl:text>)
			</xsl:text>
						</xsl:if>
						<xsl:choose>
							<xsl:when test="$action='INSERT'">
								<xsl:if test="string($keys)!=''">
									<xsl:text>	OUTPUT </xsl:text>
									<xsl:value-of select="$output_values"/>
									<xsl:text> INTO @#T_</xsl:text>
									<xsl:value-of select="generate-id($row)"/>
									<xsl:text>_Temp
			</xsl:text>
								</xsl:if>
								<xsl:text>SELECT </xsl:text>
							</xsl:when>
							<xsl:when test="$action='DELETE'">
								<xsl:text>DELETE [$Table]
			</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>UPDATE [$Table] SET 
			</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:if test="$action!='DELETE'">
							<xsl:if test="$table/@foreignKey!=''">
								[<xsl:value-of select="$table/@foreignKey" /><xsl:text>]=</xsl:text>
								<xsl:text>PT.[</xsl:text>
								<xsl:value-of select="$table/@foreignKey"/>
								<xsl:text>]</xsl:text>
								<xsl:if test="dataField">,</xsl:if>
							</xsl:if>
							<xsl:for-each select="key('submitable_fields',generate-id($row))">
								<xsl:if test="position()&gt;1">
									<xsl:text>,
			</xsl:text>
								</xsl:if>[<xsl:value-of select="@name" /><xsl:text>]=</xsl:text>
								<xsl:choose>
									<xsl:when test="@maps">
										<xsl:text>PT.[</xsl:text>
										<xsl:value-of select="@maps"/>
										<xsl:text>]</xsl:text>
									</xsl:when>
									<xsl:when test="value">
										<xsl:for-each select="value">
											<xsl:choose>
												<xsl:when test="text()">
													<xsl:value-of select="text()" />
												</xsl:when>
												<xsl:otherwise>NULL</xsl:otherwise>
											</xsl:choose>
										</xsl:for-each>
									</xsl:when>
									<xsl:when test="*">
										'<xsl:apply-templates mode="escape" select="*" />'
									</xsl:when>
									<xsl:when test="text()">
										<xsl:value-of select="text()" />
									</xsl:when>
									<xsl:otherwise>NULL</xsl:otherwise>
								</xsl:choose>
							</xsl:for-each>
						</xsl:if>
						<xsl:if test="$action='DELETE' or $action='UPDATE' and string($keys)!=''">
							<xsl:text>	OUTPUT </xsl:text>
							<xsl:value-of select="$output_values"/>
							<xsl:text> INTO @#T_</xsl:text>
							<xsl:value-of select="generate-id($row)"/>
							<xsl:text>_Temp
			</xsl:text>
						</xsl:if>

						<xsl:if test="$action='DELETE' or $action='UPDATE' or $parentDataRow">
							<xsl:text> FROM </xsl:text>
							<xsl:if test="$parentDataRow">
								<xsl:text> @#T_</xsl:text>
								<xsl:value-of select="generate-id($parentDataRow)"/>
								<xsl:text> PT </xsl:text>
							</xsl:if>
							<xsl:if test="$action='UPDATE' or $action='DELETE'">
								<xsl:if test="$parentDataRow">
									<xsl:text> CROSS JOIN </xsl:text>
								</xsl:if>
								<xsl:call-template name="formatTableName">
									<xsl:with-param name="inputString" select="$table/@name" />
								</xsl:call-template>
								<xsl:text> [$Table] </xsl:text>
							</xsl:if>
							<xsl:text> WHERE 1=1 </xsl:text>
							<xsl:if test="$action='DELETE' or $action='UPDATE'">
								<xsl:choose>
									<xsl:when test="$identityKey and @identityValue">
										<xsl:text> AND [$Table].[</xsl:text>
										<xsl:value-of select="$identityKey"/>
										<xsl:text>]=</xsl:text>
										<xsl:value-of select="@identityValue"/>
									</xsl:when>
									<xsl:when test="key('foreignkey_fields',generate-id($row))">
										<xsl:for-each select="key('foreignkey_fields',generate-id($row))">
											<xsl:text> AND [$Table].[</xsl:text>
											<xsl:value-of select="@name"/>
											<xsl:text>]=</xsl:text>
											<xsl:text>PT.[</xsl:text>
											<xsl:value-of select="@maps"/>
											<xsl:text>]</xsl:text>
										</xsl:for-each>
									</xsl:when>
									<xsl:when test="$primaryKey or key('keys',generate-id($row))">
										<xsl:if test="$primaryKey">
											<xsl:text> AND [$Table].[</xsl:text>
											<xsl:value-of select="$primaryKey"/>
											<xsl:text>]=</xsl:text>
											<xsl:value-of select="@primaryValue"/>
										</xsl:if>
										<xsl:for-each select="key('keys', generate-id($row))[@name!=string($primaryKey)]">
											<xsl:text> AND [$Table].[</xsl:text>
											<xsl:value-of select="@name"/>
											<xsl:text>]=</xsl:text>
											<xsl:choose>
												<xsl:when test="@oldValue">
													<xsl:value-of select="@oldValue"/>
												</xsl:when>
												<xsl:otherwise>
													<xsl:value-of select="."/>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:for-each>
									</xsl:when>
								</xsl:choose>
								<!--<xsl:if test="$parentTable">
												<xsl:apply-templates mode="dataRow.primaryKeyMatches" select="." >
													<xsl:with-param name="refTable">PT</xsl:with-param>
												</xsl:apply-templates>
											</xsl:if>-->
							</xsl:if>
						</xsl:if>

						IF NOT EXISTS(SELECT 1 FROM @#T_<xsl:value-of select="generate-id($row)"/>_Temp) BEGIN
						<xsl:text>RAISERROR('El registro no pudo ser </xsl:text>
						<xsl:choose>
							<xsl:when test="$action='INSERT'">insertado, algo impidió que se completara la operación. Revise sus desencadenadores.'</xsl:when>
							<xsl:otherwise>actualizado porque no se pudo localizar.'</xsl:otherwise>
						</xsl:choose>
						<xsl:text>,16,1)</xsl:text>
						END

						--Intentaremos recuperar el registro insertado. también se recuperan todas las columnas y se devuelven todos sus valores, esto será util cuando las columnas son modificadas entre el envío de datos y la persistencia de la base de datos o por algún default en la columna.
						INSERT INTO @#T_<xsl:value-of select="generate-id($row)"/>
						SELECT
						##Success=1 --Si tenemos la tabla temporal pivote, el registro debió haberse ingresado correctamente y tendremos un status de @success=1, de lo contrario @success va a ser null
						--, @Warnings= TODO: Si tenemos el registro en la tabla temporal pivote pero no pudimos recuperar el registro real vamos a avisar que no se pudo recuperar el registro
						<xsl:if test="$identityKey">
							<xsl:text>, #Id=COALESCE(I.#Id, A.#Id, T.#Id, R.#Id)</xsl:text>
						</xsl:if>
						<xsl:apply-templates mode="dataRow.outputFields" select="." />
						FROM @#T_<xsl:value-of select="generate-id($row)"/>_Temp R --Tabla temporal pivote. Forzosamente debemos tener algún registro aquí.
						<xsl:choose>
							<xsl:when test="$identityKey">
								OUTER APPLY (
								SELECT ##Success=1<xsl:apply-templates mode="dataRow.primaryKeyFields" select="."/>
								FROM <xsl:value-of select="$tableName"/> [$Table]
								WHERE 1=1
								AND ISNULL(R.#Id,-1) NOT IN (0)
								AND [$Table].[<xsl:value-of select="$identityKey"/>]=R.#Id --Si tiene Identity usamos este método
								) T --Si no tiene id
							</xsl:when>
							<xsl:otherwise>
								OUTER APPLY (--Si no tiene id así se ve el bloque
								SELECT ##Success=NULL<xsl:apply-templates mode="dataRow.primaryKeyFields" select=".">
									<xsl:with-param name="currTable">R</xsl:with-param>
								</xsl:apply-templates>
								) T
							</xsl:otherwise>
						</xsl:choose>
						OUTER APPLY (
						SELECT ##Success=1<xsl:apply-templates mode="dataRow.primaryKeyFields" select="."/>
						FROM <xsl:value-of select="$tableName"/> [$Table]
						WHERE 1=1
						AND T.##Success IS NULL --Si no se pudo recuperar en la tabla T, intentaremos recuperarla en este outer apply
						<!--<xsl:if test="$primaryKey">
										<xsl:text> AND R.[</xsl:text>
										<xsl:value-of select="$primaryKey"/>
										<xsl:text>] IS NOT NULL </xsl:text>
									</xsl:if>
									<xsl:for-each select="key('keys', generate-id($row))[@name!=string($primaryKey)]">
										<xsl:text> AND R.[</xsl:text>
										<xsl:value-of select="@name"/>
										<xsl:text>] IS NOT NULL </xsl:text>
									</xsl:for-each>-->
						<xsl:apply-templates mode="dataRow.primaryKeyMatches" select="." >
							<xsl:with-param name="refTable">R</xsl:with-param>
						</xsl:apply-templates>
						--Lo buscamos por su(s) primary key(s)
						) A
						OUTER APPLY (
						SELECT ##Success=1<xsl:apply-templates mode="dataRow.primaryKeyFields" select="."/>
						FROM <xsl:value-of select="$tableName"/> [$Table]
						WHERE 1=1
						AND A.##Success IS NULL --Si no se pudo recuperar en la tabla I, intentaremos recuperarla en este outer apply
						<xsl:choose>
							<xsl:when test="$identityKey and $action='INSERT'">
								<xsl:text>AND R.#Id=0 --Cuando tiene un desencadenador "instead of" regresa 0 el Id ..
									AND [$Table].Id=ISNULL(SCOPE_IDENTITY(),IDENT_CURRENT(@identityTable)) -- ... Por eso utilizamos el SCOPE_IDENTITY O EN SU DEFECTO, el último insertado</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>AND 1=0</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
						) I

						--RAISERROR('Test',16,1)
						--TODO: Si no se encontró el registro pero estamos en este punto (Es decir, no marcó error), enviamos un warning de que no se pudo comprobar el registro
						--SELECT * FROM @#T_<xsl:value-of select="generate-id($row)"/>

						SELECT @currentNode_<xsl:value-of select="generate-id($row)" />=(
						SELECT
						[record/@tableName]='<xsl:value-of select="$tableName"/>'
						<xsl:if test="$identityKey">
							<xsl:text>, [record/@identity]=T.#Id</xsl:text>
						</xsl:if>
						/*##region 'variables' --variables del origen y pueden ser específicas a cada output, por ejemplo estas son de extjs*/
						, [record/@action]='<xsl:value-of select="translate($action, $uppercase, $smallcase)"/>'
						, [record/@status]='success'
						<xsl:apply-templates select="@*" mode="dataRow.outputAttributes"/>
						/*##endregion 'variables'*/
						<xsl:apply-templates mode="dataRow.resultFields" select="."/>
						FROM @#T_<xsl:value-of select="generate-id($row)"/> T
						FOR XML PATH(''), TYPE
						)

						<xsl:if test="$action!='DELETE'">
							<xsl:apply-templates select="foreignTable" />
						</xsl:if>
						EXEC [$Tools].insertIntoXML <xsl:choose>
							<xsl:when test="$parentDataRow">
								@currentNode_<xsl:value-of select="generate-id($parentDataRow)" />
							</xsl:when>
							<xsl:otherwise>@result</xsl:otherwise>
						</xsl:choose><xsl:text> OUTPUT, @XPath='/*', @XNew=@currentNode_</xsl:text><xsl:value-of select="generate-id($row)" /><xsl:text>, @Position='last'</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						RAISERROR ('No se pudo insertar un nuevo registro porque faltan campos de llenar', 16, 1)
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
		END TRY
		BEGIN CATCH
		SELECT @ErrorMessage=ERROR_MESSAGE();
		EXEC [$Table].getCustomMessage @Message=@ErrorMessage OUTPUT, @SourceObjectName='<xsl:value-of select="$tableName"/>'

		SELECT @currentNode_<xsl:value-of select="generate-id($row)" />=(
		SELECT
		[record/@tableName]='<xsl:value-of select="$tableName"/>'
		/*##region 'variables' --variables del origen y pueden ser específicas a cada output, por ejemplo estas son de extjs*/
		<xsl:apply-templates select="@*" mode="dataRow.outputAttributes"/>
		/*##endregion 'variables'*/
		, [record/@status]='error'
		, [record/@statusId]=ltrim(str(error_number()))
		, [record/@statusMessage]=@ErrorMessage
		FOR XML PATH(''), TYPE
		)
		EXEC [$Tools].insertIntoXML <xsl:choose>
			<xsl:when test="$parentDataRow">
				@currentNode_<xsl:value-of select="generate-id($parentDataRow)" />
			</xsl:when>
			<xsl:otherwise>@result</xsl:otherwise>
		</xsl:choose><xsl:text> OUTPUT, @XPath='/*', @XNew=@currentNode_</xsl:text><xsl:value-of select="generate-id($row)" /><xsl:text>, @Position='last'</xsl:text>
		END CATCH;
	</xsl:template>
	<xsl:template mode="dataRow.outputAttributes" match="dataRow/@*|deleteRow/@*">
		<xsl:text>, [record/@</xsl:text>
		<xsl:value-of select="name(.)"/>
		<xsl:text>]='</xsl:text>
		<xsl:value-of select="."/>
		<xsl:text>'</xsl:text>
	</xsl:template>
	<xsl:template mode="dataRow.outputAttributes" match="dataRow/@identityValue|dataRow/@primaryValue|dataRow/@action|deleteRow/@identityValue|deleteRow/@primaryValue|deleteRow/@action">
		<xsl:text/>
	</xsl:template>
	<xsl:template mode="dataRow.primaryKeyFields.declaration" match="dataRow|deleteRow">
		<xsl:variable name="table" select=".."/>
		<xsl:variable name="row" select="current()"/>
		<xsl:variable name="primaryKey" select="$table/@primaryKey"/>
		<xsl:variable name="identityKey" select="$table/@identityKey"/>
		<xsl:if test="$identityKey">
			<xsl:text>, #id int</xsl:text>
		</xsl:if>
		<xsl:if test="$primaryKey">
			<xsl:text>, [</xsl:text>
			<xsl:value-of select="$primaryKey"/>
			<xsl:text>] nvarchar(MAX)</xsl:text>
		</xsl:if>
		<xsl:for-each select="key('outputfields', generate-id($row))[@name!=string($primaryKey)]">
			<xsl:text>, [</xsl:text>
			<xsl:value-of select="@name"/>
			<xsl:text>] nvarchar(MAX)</xsl:text>
		</xsl:for-each>
	</xsl:template>
	<xsl:template mode="dataRow.primaryKeyFields" match="dataRow|deleteRow">
		<xsl:param name="currTable">[$Table]</xsl:param>
		<xsl:variable name="table" select=".."/>
		<xsl:variable name="row" select="current()"/>
		<xsl:variable name="primaryKey" select="$table/@primaryKey"/>
		<xsl:variable name="identityKey" select="$table/@identityKey"/>
		<xsl:if test="$identityKey">
			<xsl:text>, #Id=</xsl:text>
			<xsl:value-of select="$currTable"/>
			<xsl:text>.[</xsl:text>
			<xsl:value-of select="$identityKey"/>
			<xsl:text>]</xsl:text>
		</xsl:if>
		<xsl:if test="$primaryKey">
			<xsl:apply-templates mode="format.sentence" select=".">
				<xsl:with-param name="currTable" select="$currTable"/>
				<xsl:with-param name="separator">,</xsl:with-param>
				<xsl:with-param name="key" select="$primaryKey"/>
			</xsl:apply-templates>
		</xsl:if>
		<xsl:apply-templates mode="format.sentence" select="key('outputfields', generate-id($row))[@name!=string($primaryKey)]">
			<xsl:with-param name="currTable" select="$currTable"/>
			<xsl:with-param name="separator">,</xsl:with-param>
		</xsl:apply-templates>
	</xsl:template>
	<xsl:template mode="dataRow.primaryKeyMatches" match="dataRow|deleteRow" >
		<xsl:param name="refTable"/>
		<xsl:variable name="table" select=".."/>
		<xsl:variable name="row" select="current()"/>
		<xsl:variable name="primaryKey" select="$table/@primaryKey"/>
		<xsl:variable name="identityKey" select="$table/@identityKey"/>
		<xsl:if test="$primaryKey">
			<xsl:apply-templates mode="format.sentence" select=".">
				<xsl:with-param name="key" select="$primaryKey"/>
				<xsl:with-param name="refTable" select="$refTable"/>
			</xsl:apply-templates>
		</xsl:if>
		<xsl:apply-templates mode="format.sentence" select="key('keys', generate-id($row))[@name!=string($primaryKey)]">
			<xsl:with-param name="refTable" select="$refTable"/>
		</xsl:apply-templates>
		<!--<xsl:for-each select="key('keys', generate-id($row))[@name!=string($primaryKey)]">
			<xsl:variable name="key" select="@name"/>
			<xsl:variable name="currTable">[$Table]</xsl:variable>
			<xsl:variable name="separator">AND</xsl:variable>
			<xsl:value-of select="concat(' ',$separator,' ')"/>
			<xsl:value-of select="$currTable"/>
			<xsl:text>.[</xsl:text>
			<xsl:value-of select="$key"/>
			<xsl:text>]</xsl:text>
			<xsl:if test="$refTable">
				<xsl:text>=</xsl:text>
				<xsl:value-of select="$refTable"/>
				<xsl:text>.[</xsl:text>
				<xsl:value-of select="$key"/>
				<xsl:text>]</xsl:text>
			</xsl:if>
		</xsl:for-each>-->
	</xsl:template>
	<xsl:template mode="format.sentence" match="*" >
		<xsl:param name="key" select="@name"/>
		<xsl:param name="refTable"/>
		<xsl:param name="currTable">[$Table]</xsl:param>
		<xsl:param name="separator">AND</xsl:param>
		<xsl:value-of select="concat(' ',$separator,' ')"/>
		<xsl:value-of select="$currTable"/>
		<xsl:text>.[</xsl:text>
		<xsl:value-of select="$key"/>
		<xsl:text>]</xsl:text>
		<xsl:if test="$refTable">
			<xsl:text>=</xsl:text>
			<xsl:value-of select="$refTable"/>
			<xsl:text>.[</xsl:text>
			<xsl:value-of select="$key"/>
			<xsl:text>]</xsl:text>
		</xsl:if>
	</xsl:template>
	<xsl:template mode="format.resultFields" match="*" >
		<xsl:param name="key" select="@name"/>
		, [record/field/@name]='<xsl:value-of select="$key"/>'
		, [record/field]=QUOTENAME(RTRIM([<xsl:value-of select="$key"/>]),'''')
		, [record]=NULL
	</xsl:template>
	<xsl:template mode="format.outputFields" match="*" >
		<xsl:param name="key" select="@name"/>
		<xsl:text>, [</xsl:text>
		<xsl:value-of select="$key"/>
		<xsl:text>]=COALESCE(</xsl:text>
		<xsl:text>I.[</xsl:text>
		<xsl:value-of select="$key"/>
		<xsl:text>],A.[</xsl:text>
		<xsl:value-of select="$key"/>
		<xsl:text>],T.[</xsl:text>
		<xsl:value-of select="$key"/>
		<xsl:text>],R.[</xsl:text>
		<xsl:value-of select="$key"/>
		<xsl:text>])</xsl:text>
	</xsl:template>
	<xsl:template mode="dataRow.outputFields" match="dataRow|deleteRow" >
		<xsl:param name="refTable"/>
		<xsl:variable name="table" select=".."/>
		<xsl:variable name="row" select="current()"/>
		<xsl:variable name="primaryKey" select="$table/@primaryKey"/>
		<xsl:variable name="identityKey" select="$table/@identityKey"/>
		<xsl:if test="$primaryKey">
			<xsl:apply-templates mode="format.outputFields" select=".">
				<xsl:with-param name="key" select="$primaryKey"/>
			</xsl:apply-templates>
		</xsl:if>
		<xsl:apply-templates mode="format.outputFields" select="key('outputfields', generate-id($row))[@name!=string($primaryKey)]"/>
	</xsl:template>
	<xsl:template mode="dataRow.resultFields" match="dataRow|deleteRow" >
		<xsl:param name="refTable"/>
		<xsl:variable name="table" select=".."/>
		<xsl:variable name="row" select="current()"/>
		<xsl:variable name="primaryKey" select="$table/@primaryKey"/>
		<xsl:variable name="identityKey" select="$table/@identityKey"/>
		<xsl:if test="$primaryKey">
			<xsl:apply-templates mode="format.resultFields" select=".">
				<xsl:with-param name="key" select="$primaryKey"/>
			</xsl:apply-templates>
		</xsl:if>
		<xsl:apply-templates mode="format.resultFields" select="key('outputfields', generate-id($row))[@name!=string($primaryKey)]"/>
	</xsl:template>
	<xsl:template mode="value" match="*">
		<xsl:choose>
			<xsl:when test="value">
				<xsl:for-each select="value">
					<xsl:choose>
						<xsl:when test="text()">
							<xsl:value-of select="text()" />
						</xsl:when>
						<xsl:otherwise>NULL</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</xsl:when>
			<xsl:when test="*">
				'<xsl:apply-templates mode="escape" select="*" />'
			</xsl:when>
			<xsl:when test="text()">
				<xsl:value-of select="text()" />
			</xsl:when>
			<xsl:otherwise>NULL</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>