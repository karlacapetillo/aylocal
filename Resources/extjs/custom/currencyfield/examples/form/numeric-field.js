/*
 This file is part of Ext JS 4
 Copyright (c) 2011 Sencha Inc
 Contact:  http://www.sencha.com/contact
 GNU General Public License Usage
 This file may be used under the terms of the GNU General Public License version 3.0 as published by the Free Software Foundation and appearing in the file LICENSE included in the packaging of this file.  Please review the following information to ensure the GNU General Public License version 3.0 requirements will be met: http://www.gnu.org/copyleft/gpl.html.
 If you are unsure which license is appropriate for your use, please contact the sales department at http://www.sencha.com/contact.
 */
Ext.require(['Ext.form.*', 'Ext.layout.container.Column', 'Ext.tab.Panel', 'Ext.String', 'Ext.util.*']);

Ext.Loader.setConfig(
{
    enabled: true
});

Ext.Loader.setPath('Ext.ux', '../ux');

Ext.onReady(function(){

    Ext.QuickTips.init();
    
    var form = new Ext.FormPanel(
    {
        title: 'Simple Form',
        renderTo: 'form',
        frame: true,
        layout: 'column',
        defaults: 
        {
            columnWidth: .33,
            xtype: 'fieldset'
        },
        bodyStyle: 'padding:5px 5px 0',
        width: '100%',
        layout: 'column',
        defaults: 
        {
            columnWidth: .33,
            xtype: 'fieldset',
            labelWidth: 600
        },
        items: [
        {
            defaults: 
            {
                anchor: '96%'
            },
            items: [Ext.create('Ext.ux.form.NumericField', 
            {
                fieldLabel: 'Int',
                allowDecimals: false,
                labelWidth: 250,
                useThousandSeparator: false,
                value: 230000
            }), Ext.create('Ext.ux.form.NumericField', 
            {
                fieldLabel: 'Int, comma',
                allowDecimals: false,
                labelWidth: 250,
                value: 3000000
            
            }), Ext.create('Ext.ux.form.NumericField', 
            {
                fieldLabel: 'Int negative',
                value: -1000000,
                labelWidth: 250,
                allowDecimals: false,
                useThousandSeparator: false,
                allowNegative: true
            }), Ext.create('Ext.ux.form.NumericField', 
            {
                fieldLabel: 'Int negative with comma',
                allowNegative: true,
                allowDecimals: false,
                labelWidth: 250,
                value: -10000
            }), Ext.create('Ext.ux.form.NumericField', 
            {
                fieldLabel: 'Int, comma, international',
                decimalSeparator: ',',
                allowNegative: false,
                allowDecimals: false,
                labelWidth: 250,
                value: 100000
            }), Ext.create('Ext.ux.form.NumericField', 
            {
                fieldLabel: 'Int negative with comma international',
                allowNegative: true,
                decimalSeparator: ',',
                allowDecimals: false,
                labelWidth: 250,
                value: -30000
            }), Ext.create('Ext.ux.form.NumericField', 
            {
                fieldLabel: 'Int with currency',
                allowNegative: false,
                labelWidth: 250,
                allowDecimals: false,
                useThousandSeparator: false,
                value: 300000,
                currencySymbol: 'C$'
            }), Ext.create('Ext.ux.form.NumericField', 
            {
                fieldLabel: 'Int with comma and currency',
                allowNegative: false,
                allowDecimals: false,
                currencySymbol: 'C$',
                labelWidth: 250,
                value: 333000
            }), Ext.create('Ext.ux.form.NumericField', 
            {
                fieldLabel: 'Int negative with currency',
                allowNegative: true,
                useThousandSeparator: false,
                allowDecimals: false,
                currencySymbol: 'C$',
                labelWidth: 250,
                value: -33330000
            }), Ext.create('Ext.ux.form.NumericField', 
            {
                fieldLabel: 'Int negative with comma and currency',
                allowNegative: true,
                allowDecimals: false,
                currencySymbol: 'C$',
                labelWidth: 250,
                value: -33339999
            }), Ext.create('Ext.ux.form.NumericField', 
            {
                fieldLabel: 'Int with comma international and currency',
                allowNegative: false,
                allowDecimals: false,
                decimalSeparator: ',',
                currencySymbol: 'C$',
                labelWidth: 250,
                value: 33339999
            }), Ext.create('Ext.ux.form.NumericField', 
            {
                fieldLabel: 'Int negative with comma international and currency',
                allowNegative: true,
                allowDecimals: false,
                decimalSeparator: ',',
                currencySymbol: 'C$',
                labelWidth: 250,
                value: -33339999
            })]
        }, 
        {
            defaults: 
            {
                anchor: '96%'
            },
            items: [Ext.create('Ext.ux.form.NumericField', 
            {
                fieldLabel: 'float',
                useThousandSeparator: false,
                decimalPrecision: 4,
                labelWidth: 250,
                alwaysDisplayDecimals: false,
                allowNegative: false,
                value: 33339999.1230
            }), Ext.create('Ext.ux.form.NumericField', 
            {
                fieldLabel: 'float with comma',
                decimalPrecision: 4,
                allowNegative: false,
                labelWidth: 250,
                value: 33339999.1230
            }), Ext.create('Ext.ux.form.NumericField', 
            {
                fieldLabel: 'float negative',
                decimalPrecision: 4,
                useThousandSeparator: false,
                allowNegative: true,
                labelWidth: 250,
                value: -33339999.1230
            }), Ext.create('Ext.ux.form.NumericField', 
            {
                fieldLabel: 'float negative with comma',
                decimalPrecision: 4,
                allowNegative: true,
                labelWidth: 250,
                value: -33339999.1230
            }), Ext.create('Ext.ux.form.NumericField', 
            {
                fieldLabel: 'float with comma international',
                decimalPrecision: 4,
                decimalSeparator: ',',
                allowNegative: false,
                labelWidth: 250,
                value: 33339999.1230
            }), Ext.create('Ext.ux.form.NumericField', 
            {
                fieldLabel: 'float negative with comma international',
                decimalPrecision: 4,
                decimalSeparator: ',',
                allowNegative: true,
                labelWidth: 250,
                value: -33339999.1230
            }), Ext.create('Ext.ux.form.NumericField', 
            {
                fieldLabel: 'float with currency',
                decimalPrecision: 4,
                allowNegative: true,
                currencySymbol: 'C$',
                useThousandSeparator: false,
                labelWidth: 250,
                value: 33339999.1230
            }), Ext.create('Ext.ux.form.NumericField', 
            {
                fieldLabel: 'float with comma and currency',
                decimalPrecision: 4,
                allowNegative: false,
                currencySymbol: 'C$',
                labelWidth: 250,
                value: 200000.1230
            }), Ext.create('Ext.ux.form.NumericField', 
            {
                fieldLabel: 'float negative with currency',
                decimalPrecision: 4,
                allowNegative: true,
                currencySymbol: 'C$',
                useThousandSeparator: false,
                labelWidth: 250,
                value: -33339999.1230
            }), Ext.create('Ext.ux.form.NumericField', 
            {
                fieldLabel: 'float negative with comma and currency',
                decimalPrecision: 4,
                currencySymbol: 'C$',
                allowNegative: true,
                labelWidth: 250,
                value: -33339999.1230
            }), Ext.create('Ext.ux.form.NumericField', 
            {
                fieldLabel: 'float with comma international and currency',
                decimalPrecision: 4,
                decimalSeparator: ',',
                currencySymbol: 'C$',
                allowNegative: true,
                labelWidth: 250,
                value: 33339999.1230
            }), Ext.create('Ext.ux.form.NumericField', 
            {
                fieldLabel: 'float negative with comma international and currency',
                decimalPrecision: 4,
                decimalSeparator: ',',
                currencySymbol: 'C$',
                allowNegative: true,
                labelWidth: 250,
                value: -33339999.1230
            })]
        }, 
        {
            defaults: 
            {
                anchor: '96%',
            },
            items: [Ext.create('Ext.ux.form.NumericField', 
            {
                fieldLabel: 'float',
                useThousandSeparator: false,
                decimalPrecision: 4,
                alwaysDisplayDecimals: true,
                labelWidth: 250,
                allowNegative: false,
                value: 33339999.1230
            }), Ext.create('Ext.ux.form.NumericField', 
            {
                fieldLabel: 'float with comma',
                decimalPrecision: 4,
                alwaysDisplayDecimals: true,
                labelWidth: 250,
                allowNegative: false,
                value: 33339999.1230
            }), Ext.create('Ext.ux.form.NumericField', 
            {
                fieldLabel: 'float negative',
                decimalPrecision: 4,
                useThousandSeparator: false,
                alwaysDisplayDecimals: true,
                labelWidth: 250,
                allowNegative: true,
                value: -33339999.1230
            }), Ext.create('Ext.ux.form.NumericField', 
            {
                fieldLabel: 'float negative with comma',
                decimalPrecision: 4,
                alwaysDisplayDecimals: true,
                labelWidth: 250,
                allowNegative: true,
                value: -33339999.1230
            }), Ext.create('Ext.ux.form.NumericField', 
            {
                fieldLabel: 'float with comma international',
                decimalPrecision: 4,
                decimalSeparator: ',',
                alwaysDisplayDecimals: true,
                labelWidth: 250,
                allowNegative: false,
                value: 33339999.1230
            }), Ext.create('Ext.ux.form.NumericField', 
            {
                fieldLabel: 'float negative with comma international',
                decimalPrecision: 4,
                decimalSeparator: ',',
                allowNegative: true,
                alwaysDisplayDecimals: true,
                labelWidth: 250,
                value: -33339999.1230
            }), Ext.create('Ext.ux.form.NumericField', 
            {
                fieldLabel: 'float with currency',
                decimalPrecision: 4,
                allowNegative: true,
                alwaysDisplayDecimals: true,
                labelWidth: 250,
                currencySymbol: 'C$',
                useThousandSeparator: false,
                value: 33339999.1230
            }), Ext.create('Ext.ux.form.NumericField', 
            {
                fieldLabel: 'float with comma and currency',
                decimalPrecision: 4,
                currencySymbol: 'C$',
                alwaysDisplayDecimals: true,
                allowNegative: false,
                labelWidth: 250,
                value: 200000.1230
            }), Ext.create('Ext.ux.form.NumericField', 
            {
                fieldLabel: 'float negative with currency',
                decimalPrecision: 4,
                currencySymbol: 'C$',
                allowNegative: true,
                alwaysDisplayDecimals: true,
                labelWidth: 250,
                useThousandSeparator: false,
                value: -33339999.1230
            }), Ext.create('Ext.ux.form.NumericField', 
            {
                fieldLabel: 'float negative with comma and currency',
                decimalPrecision: 4,
                currencySymbol: 'C$',
                allowNegative: true,
                alwaysDisplayDecimals: true,
                labelWidth: 250,
                value: -33339999.1230
            }), Ext.create('Ext.ux.form.NumericField', 
            {
                fieldLabel: 'float with comma international and currency',
                decimalPrecision: 4,
                decimalSeparator: ',',
                currencySymbol: 'C$',
                allowNegative: true,
                alwaysDisplayDecimals: true,
                labelWidth: 250,
                value: 33339999.1230
            }), Ext.create('Ext.ux.form.NumericField', 
            {
                fieldLabel: 'float negative with comma international and currency',
                decimalPrecision: 4,
                decimalSeparator: ',',
                currencySymbol: 'C$',
                alwaysDisplayDecimals: true,
                labelWidth: 250,
                allowNegative: true,
                value: -33339999.1230
            })]
        }],
        buttons: [
        {
            text: 'Save'
        }, 
        {
            text: 'Cancel'
        }]
    });
});



