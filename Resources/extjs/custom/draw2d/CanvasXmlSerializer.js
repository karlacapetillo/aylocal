draw2d.CanvasXmlSerializer=function(){
};
draw2d.CanvasXmlSerializer.prototype.type="CanvasXmlSerializer";
draw2d.CanvasXmlSerializer.prototype.diagramTag="diagram";
draw2d.CanvasXmlSerializer.prototype.connectionsTag="connections";
draw2d.CanvasXmlSerializer.prototype.toXML=function(canvas){
var xml="<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n";
xml=xml+"<"+this.diagramTag+">\n";
var figures=canvas.getFigures();
for(var i=0;i<figures.getSize();i++){
	var singleFigure=figures.get(i);
	xml=xml+"   <"+(singleFigure.tagName||singleFigure.type)+this.getObjectAttributes(singleFigure,{x:singleFigure.getX(), y:singleFigure.getY(), id:singleFigure.getId()})+">\n";
	var ports=singleFigure.getPorts();
	xml=xml+"<"+this.connectionsTag+">"
	for(var j=0;j<ports.getSize();j++){
		var singlePort=ports.get(j);
		xml=xml+this.getConnectionXML(singlePort,"   ");
	}
	xml=xml+"</"+this.connectionsTag+">"
	if(singleFigure instanceof draw2d.CompartmentFigure){
		xml=xml+this.getChildXML(singleFigure,"   ");
	}
	xml=xml+"</"+(singleFigure.tagName||singleFigure.type)+">\n";
}
xml=xml+"</"+this.diagramTag+">\n";
return xml;
};

/* draw2d.CanvasXmlSerializer.prototype.getChildXML=function(singleFigure, xmlSeparator){
var xml="";
var figureChildren=singleFigure.getChildren();
for (var i=0;i<figureChildren.getSize();i++){
	var figureChild=figureChildren.get(i);
	xml=xml+xmlSeparator+"<"+figureChild.type+" x=\""+figureChild.getX()+"\" y=\""+figureChild.getY()+"\" id=\""+figureChild.getId()+"\">\n";
	xml=xml+this.getPropertyXML(figureChild,"   "+xmlSeparator);
	if (figureChild instanceof draw2d.CompartmentFigure){
		xml=xml+this.getChildXML(figureChild,"   "+xmlSeparator);
	}
	xml=xml+xmlSeparator+"</"+figureChild.type+">\n";
}
return xml;
}; */

draw2d.CanvasXmlSerializer.prototype.getObjectAttributes=function(singleObject,defaults,xmlSeparator){
	var xml="";
	var objectProperties=singleObject.getProperties();
	xml=xml+this.getAttributes(defaults);
	xml=xml+this.getAttributes(objectProperties);
return xml;
};

draw2d.CanvasXmlSerializer.prototype.getAttributes=function(attribs) {
var xml="";
for(var key in attribs){
	var value=attribs[key];
	if(value!==null){
		xml=xml+" "+key+"=\""+value+"\"";
	}
}
return xml;
}


draw2d.CanvasXmlSerializer.prototype.getConnectionXML=function(singlePort,xmlSeparator){
var xml="";
var connections = singlePort.getConnections();
if (singlePort instanceof draw2d.InputPort) { return ''; }
for(var i=0;i<connections.getSize();i++)
	{
	var c = connections.get(i)
	xml=xml+"   <"+(c.tagName||c.type)+this.getObjectAttributes(c,{sourcePort: c.sourcePort.getName(), target: c.targetPort.parentNode.id, targetPort: c.targetPort.getName(), router: c.router.type, stroke: c.stroke, sourceDecorator:  function() { return c.sourceDecorator?c.sourceDecorator.type:'null' }(), targetDecorator: function() { return c.targetDecorator?c.targetDecorator.type:'null' }()})+"/>\n";
	}
return xml;
};