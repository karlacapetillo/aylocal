{"text":".","children": [
    {
        task:{text:'Project: Shopping'},
        duration:13.25,
        user:'Tommy Maintz',
        iconCls:'task-folder',
        expanded: true,
        children:[{
            task:{text:'Housewares'},
            duration:1.25,
            user:'Tommy Maintz',
            iconCls:'task-folder',
            children:[{
                task:{text:'Kitchen supplies'},
                duration:0.25,
                user:'Tommy Maintz',
                leaf:true,
                iconCls:'task'
            },{
                task:{text:'Groceries'},
                duration:.4,
                user:'Tommy Maintz',
                leaf:true,
                iconCls:'task',
                done: true
            },{
                task:{text:'Cleaning supplies'},
                duration:.4,
                user:'Tommy Maintz',
                leaf:true,
                iconCls:'task'
            },{
                task:{text: 'Office supplies'},
                duration: .2,
                user: 'Tommy Maintz',
                leaf: true,
                iconCls: 'task'
            }]
        }, {
            task:{text:'Remodeling'},
            duration:12,
            user:'Tommy Maintz',
            iconCls:'task-folder',
            expanded: true,
            children:[{
                task:{text:'Retile kitchen'},
                duration:6.5,
                user:'Tommy Maintz',
                leaf:true,
                iconCls:'task'
            },{
                task:{text:'Paint bedroom'},
                duration: 2.75,
                user:'Tommy Maintz',
                iconCls:'task-folder',
                children: [{
                    task:{text: 'Ceiling'},
                    duration: 1.25,
                    user: 'Tommy Maintz',
                    iconCls: 'task',
                    leaf: true
                }, {
                    task:{text: 'Walls'},
                    duration: 1.5,
                    user: 'Tommy Maintz',
                    iconCls: 'task',
                    leaf: true
                }]
            },{
                task:{text:'Decorate living room'},
                duration:2.75,
                user:'Tommy Maintz',
                leaf:true,
                iconCls:'task',
                done: true
            },{
                task:{text: 'Fix lights'},
                duration: .75,
                user: 'Tommy Maintz',
                leaf: true,
                iconCls: 'task',
                done: true
            }, {
                task:{text: 'Reattach screen door'},
                duration: 2,
                user: 'Tommy Maintz',
                leaf: true,
                iconCls: 'task'
            }]
        }]
    },{
        task:{text:'Project: Testing'},
        duration:2,
        user:'Core Team',
        iconCls:'task-folder',
        children:[{
            task:{text: 'Mac OSX'},
            duration: 0.75,
            user: 'Tommy Maintz',
            iconCls: 'task-folder',
            children: [{
                task:{text: 'FireFox'},
                duration: 0.25,
                user: 'Tommy Maintz',
                iconCls: 'task',
                leaf: true
            }, {
                task:{text: 'Safari'},
                duration: 0.25,
                user: 'Tommy Maintz',
                iconCls: 'task',
                leaf: true
            }, {
                task:{text: 'Chrome'},
                duration: 0.25,
                user: 'Tommy Maintz',
                iconCls: 'task',
                leaf: true
            }]
        },{
            task:{text: 'Windows'},
            duration: 3.75,
            user: 'Darrell Meyer',
            iconCls: 'task-folder',
            children: [{
                task:{text: 'FireFox'},
                duration: 0.25,
                user: 'Darrell Meyer',
                iconCls: 'task',
                leaf: true
            }, {
                task:{text: 'Safari'},
                duration: 0.25,
                user: 'Darrell Meyer',
                iconCls: 'task',
                leaf: true
            }, {
                task:{text: 'Chrome'},
                duration: 0.25,
                user: 'Darrell Meyer',
                iconCls: 'task',
                leaf: true
            },{
                task:{text: 'Internet Exploder'},
                duration: 3,
                user: 'Darrell Meyer',
                iconCls: 'task',
                leaf: true
            }]
        },{
            task:{text: 'Linux'},
            duration: 0.5,
            user: 'Aaron Conran',
            iconCls: 'task-folder',
            children: [{
                task:{text: 'FireFox'},
                duration: 0.25,
                user: 'Aaron Conran',
                iconCls: 'task',
                leaf: true
            }, {
                task:{text: 'Chrome'},
                duration: 0.25,
                user: 'Aaron Conran',
                iconCls: 'task',
                leaf: true
            }]
        }]
    }
]}