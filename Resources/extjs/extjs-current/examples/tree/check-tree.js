Ext.require([
    'Ext.tree.*',
    'Ext.data.*',
    'Ext.window.MessageBox'
]);

Ext.onReady(function() {
    var store = Ext.create('Ext.data.TreeStore', {
        root: {
			text:'.',
            children:[{
    "text": "To Do", 
    "cls": "folder",
    "expanded": true,
    "children": [{
        "text": "Go jogging",
        "leaf": true,
        "checked": true
    },{
        "text": "Take a nap",
        "leaf": true,
        "checked": false
    },{
        "text": "Climb Everest",
        "leaf": true,
        "checked": false
    }]
},{
    "text": "Grocery List",
    "cls": "folder",
    "children": [{
        "text": "Bananas",
        "leaf": true,
        "checked": false
    },{
        "text": "Milk",
        "leaf": true,
        "checked": false
    },{
        "text": "Cereal",
        "leaf": true,
        "checked": false
    },{
        "text": "Energy foods",
        "cls": "folder",
        "children": [{
            "text": "Coffee",
            "leaf": true,
            "checked": false
        },{
            "text": "Red Bull",
            "leaf": true,
            "checked": false
        }]
    }]
},{
    "text": "Remodel Project", 
    "cls": "folder",
    "children": [{
        "text": "Finish the budget",
        "leaf": true,
        "checked": false
    },{
        "text": "Call contractors",
        "leaf": true,
        "checked": false
    },{
        "text": "Choose design",
        "leaf": true,
        "checked": false
    }]
}]
        },
        sorters: [{
            property: 'leaf',
            direction: 'ASC'
        }, {
            property: 'text',
            direction: 'ASC'
        }]
    });

    var tree = Ext.create('Ext.tree.Panel', {
        store: store,
        rootVisible: false,
        useArrows: true,
        frame: true,
        title: 'Check Tree',
        renderTo: 'tree-div',
        width: 200,
        height: 250,
        dockedItems: [{
            xtype: 'toolbar',
            items: {
                text: 'Get checked nodes',
                handler: function(){
                    var records = tree.getView().getChecked(),
                        names = [];
                    
                    Ext.Array.each(records, function(rec){
                        names.push(rec.get('text'));
                    });
                    
                    Ext.MessageBox.show({
                        title: 'Selected Nodes',
                        msg: names.join('<br />'),
                        icon: Ext.MessageBox.INFO
                    });
                }
            }
        }]
    });
	
	tree.getStore().setRootNode(store.tree.root)
	
});
