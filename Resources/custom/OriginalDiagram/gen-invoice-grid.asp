var grid={
	xtype: 'grid',
	title: 'Invoice Report',
	store: {
		model: 'Invoice',
		proxy: {
			url: 'get-invoice-data.asp',
			type: 'ajax',
			reader: {
				type: 'json',
				root: 'transaction',
				idProperty: 'id',
				totalRecords: 'total'
			}
		},
		autoLoad: {
			params: {
				startDate: '01/01/2008',
				endDate: '01/31/2008'
			}
		}
	},
	columns: [
		{header: 'Customer', width: 250, dataIndex: 'customer', sortable: true},
		{header: 'Invoice Number', width: 120, dataIndex: 'invNo', sortable: true},
		{header: 'Invoice Date', width: 100, dataIndex: 'date', renderer: Ext.util.Format.dateRenderer('M d, y'), sortable: true},
		{header: 'Value', width: 120, dataIndex: 'value', renderer: 'usMoney', sortable: true}
	]
}


