<% 
Function [$Tag](ByVal sTagName)', ByRef oContent)
	DIM oTag:	Set oTag = new Tag
	oTag.TagName = sTagName
'	oTag.AddContent(oContent)
	Set [$Tag]=oTag
End Function
Class Tag
	'Properties
	Private sTagName, sId, sCssClass', sOtherProperties
	
	'Settings
	Private bHasClosingTag, bRender, bVisible
	
	'Objects
	Private oStyle, oProperties, oMethods, oContainer, oRelatives, oContent
	
	'Private Variables
	Private iElementIndex, sReturnString, sKey, i
	
	Private Sub Class_Terminate()
		Set oRelatives = nothing
		Set oStyle = nothing
		Set oMethods = nothing
		Set oProperties = nothing
		Set oContent = nothing
	End Sub
	
	Private Sub Class_Initialize()
		Set oStyle = nothing
		Set oMethods = nothing
		Set oProperties = nothing
'		Set oRelatives = [$Relatives](Me)
'		oRelatives.Content = new Collection
		Set oContent = new Collection 'oRelatives.Content
		Me.Visible=TRUE
		Me.Render=TRUE
		Me.HasClosingTag=TRUE
	End Sub
	
	'Output objects
	Public Property Get Style()
		IF oStyle IS NOTHING THEN Set oStyle = new Styles
		Set Style = oStyle
	End Property
	Public Property Let Style(input)
		Set oStyle = input
	End Property
	
	Public Property Get Methods()
		IF oMethods IS NOTHING THEN Set oMethods = new MethodsCollection
		Set Methods = oMethods
	End Property
	
	Public Property Get Properties()
		IF oProperties IS NOTHING THEN Set oProperties = new PropertiesCollection
		Set Properties = oProperties
	End Property
	Public Property Let Properties(input)
		Set oProperties = input
	End Property
	
	Public Property Get Attributes()
		Set Attributes = Me.Properties
	End Property
	
	Public Property Get Relatives()
		Set Relatives = oRelatives
	End Property
	
	Public Property Get Container()
		Set Container = oRelatives.Parent
	End Property
	Public Property Let Container(input)
		oRelatives.Parent = input
	End Property

	Public Property Get Text()
		Text = oContent.GetCode()
	End Property
	Public Property Let Text(input)
		Me.ClearContent()
		Me.AddContent(input)
	End Property


	'Output Properties 
	Public Default Property Get NamedItem(ByVal sItemName)
		Set NamedItem=oContent.NamedItem(sItemName)
	End Property

	Public Property Get Render()
		Render = bRender
	End Property
	Public Property Let Render(input)
		bRender = input
	End Property

	Public Property Get Visible()
		Visible = bVisible
	End Property
	Public Property Let Visible(input)
		bVisible = input
		IF Me.Visible THEN 
			Me.Style.Display=""
		ELSE
			Me.Style.Display="none"
		END IF
	End Property

	Public Property Get TagName()
		TagName = sTagName
	End Property
	Public Property Let TagName(input)
		sTagName = input
		SELECT CASE UCASE(sTagName)
		CASE UCASE("input")
			Me.HasClosingTag=FALSE
		CASE ELSE
			Me.HasClosingTag=TRUE
		END SELECT
	End Property
	
	Public Property Get Id()
		Id = sId
	End Property
	Public Property Let Id(input)
		sId = input
		Me.Properties.SetProperty "id", input
	End Property
	
	Public Property Get CssClass()
		CssClass = Me.Properties.GetProperty("class")
	End Property
	Public Property Let CssClass(input)
		Me.Properties.SetProperty "class", input
	End Property
	
	Public Property Get IsReadonly()
		IsReadonly = oContent.IsReadonly
	End Property
 	Public Property Let IsReadonly(input)
		oContent.IsReadonly = input
	End Property

	Public Property Get ItemsCount()
		ItemsCount = oContent.ItemsCount
	End Property

	'Input Properties 
	Public Property Let ParentCell(input)
		Set oParentCell = input
	End Property

	Public Property Let HasClosingTag(input)
		bHasClosingTag = input
	End Property
	
	'Public methods
	Public Property Get Content()
		Set Content = oContent
	End Property

	Public Sub Add(ByRef oElement)
		Me.AddContent oElement
	End Sub

	Public Sub AddItemAt(ByRef oElement, ByVal iPosition)
		oContent.AddItemAt oElement, iPosition
	End Sub

	Public Sub AddContent(ByRef oElement)
		oContent.AddItem oElement	'oRelatives.AddChildren oElement '
	End Sub

	Public Sub AddNamedItem(ByRef oElement, ByVal sItemName)
		oContent.AddNamedItem oElement, sItemName
	End Sub
	
	Public Function ExistsItem(ByVal sItemName)
		ExistsItem=oContent.ExistsItem(sItemName)
	End Function

	Public Sub ClearContent()
		oContent.ClearContent()
	End Sub

	Public Property Get All()
		Set All = oContent
	End Property

	Public Function GetItems()
		GetItems=oContent.GetItems()
	End Function
	
	Public Function GetCode()
		IF NOT Me.Render THEN 
			GetCode=""
			EXIT Function
		END IF
		sReturnString=""
		IF NOT(TRIM(sTagName)<>"") THEN
			Err.Raise 1, "ASP 101", "TagName"&sErrPropertyNotSet
		END IF
		sReturnString=sReturnString& "<"&sTagName
		IF NOT oProperties IS NOTHING THEN sReturnString=sReturnString& Me.Properties.GetCode()
		IF NOT oMethods IS NOTHING THEN sReturnString=sReturnString& Me.Methods.GetCode()
		IF NOT oStyle IS NOTHING THEN sReturnString=sReturnString& Me.Style.GetCode()
		sReturnString=sReturnString& ">"
		sReturnString=sReturnString& oContent.GetCode()
		IF bHasClosingTag THEN sReturnString=sReturnString& "</"&sTagName&">" & vbCrLf
		GetCode=sReturnString
	End Function

	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub
End Class 
 %>