<% 
Function [$DataValue](ByRef oDataField)
	Set [$DataValue] = new DataValue
	[$DataValue].DataField = oDataField
End Function
Class DataValue
	'Properties
	Private sFieldName, vValue, sFormat, iDecimalPositions, iIndex, iFixedLength, sTreatNullsAs, sText, iArrayValueIndex, sArrayValueSeparator
	'Objects
	Private aValues, oValue, oField, oDataField, oDataSource, oIndex
	
	Private Sub Class_Initialize()
		iFixedLength=800000 'Se pone a un n�mero muy grande
		iDecimalPositions=NULL
		Set oIndex = nothing
		Set oValue = nothing
		Set oField = nothing
	End Sub
	Private Sub Class_Terminate()
		Set oIndex = nothing
		Set oValue = nothing
		Set oField = nothing
	End Sub
	
	Public Property Get Object()
		Set Object = oValue
	End Property

	Public Property Get DataField()
		Set DataField = oDataField
	End Property
	Public Property Let DataField(input)
		Set oDataField = input
	End Property

	Public Property Get DataSource()
		Set DataSource = oDataSource
	End Property
	Public Property Let DataSource(input)
		Set oDataSource = input
		Set oIndex = oDataSource.Index
		'Debugger Me, oDataSource.Metadata.Mode&": "&oDataSource.Metadata.SupportsInsert
		IF NOT(oDataSource.RecordSet.BOF AND oDataSource.RecordSet.EOF) THEN
			aValues = oDataSource.RecordSet.GetRows(-1, 1, oField.Name)
			SELECT CASE oField.Type
			CASE 20	'20: BigInt -- no soporta [&TypeName]
				DIM i
				FOR i=0 TO UBOUND(aValues, 2)
					aValues(0, i)=CINT(aValues(0, i))
				NEXT
			END SELECT
		END IF
		IF IsEmpty(aValues) THEN 
			REDIM aValues(0, 2)
			aValues(0, 0)=NULL
		END IF
	End Property

	Public Property Get Field()
		Set Field = oField
	End Property
	Public Property Let Field(input)
		Set oField = input
	End Property

	Public Property Get [Array]()
		[Array] = aValues
	End Property

	Public Property Get ArrayValueIndex()
		ArrayValueIndex = iArrayValueIndex
	End Property
	Public Property Let ArrayValueIndex(input)
		iArrayValueIndex = input
	End Property

	Public Property Get ArrayValueSeparator()
		ArrayValueSeparator = sArrayValueSeparator
	End Property
	Public Property Let ArrayValueSeparator(input)
		sArrayValueSeparator = input
	End Property

	Public Property Get FieldName()
		FieldName = oField.Name
	End Property

	Public Property Get Index()
		Index = oIndex.Value
	End Property

	Public Property Get RecordCount()
		RecordCount = ubound(aValues, 2)
	End Property

	Public Property Get FixedLength()
		FixedLength = iFixedLength
	End Property
	Public Property Let FixedLength(input)
		iFixedLength = input
	End Property

	Public Property Get Text()
		DIM sTempText
		IF IsEmpty(sText) THEN sTempText=Me.Value ELSE sTempText=sText END IF
		sTempText = FormatValue(sTempText, sFormat, iDecimalPositions)
		IF LEN(TRIM(sTempText))>iFixedLength THEN sTempText=LEFT(TRIM(sTempText), iFixedLength)&"..."
		Text = RTRIM(sTempText)
	End Property
	Public Property Let Text(input)
		sText=input
	End Property

	Public Sub TreatNullsAs(ByVal sTreatNullsAs)
		DIM i
		FOR i=0 TO UBOUND(aValues, 2)
			IF ISNULL(aValues(0, i)) THEN aValues(0, i)=sTreatNullsAs
		NEXT
	End Sub

	Public Default Property Get Value()
'		ON ERROR  RESUME NEXT
		iIndex=Me.Index
		IF TypeName(aValues(0, iIndex))="ArrayList" THEN
			'Set Value = aValues(0, iIndex)
		ELSEIF TRIM(sArrayValueSeparator)<>"" THEN
			'Set aValues(0, iIndex) = [$ArrayList](SPLIT(aValues(0, iIndex), sArrayValueSeparator))
		END IF
		IF IsObject(aValues(0, iIndex)) THEN
			IF TypeName(aValues(0, iIndex))="ArrayList" THEN
				Value=aValues(0, iIndex)(iArrayValueIndex)
			ELSE
				Set Value=aValues(0, iIndex)
			END IF
		ELSE
			Value=aValues(0, iIndex)
		END IF
		
'		Value=oField.Value
'		IF NOT oField IS NOTHING THEN
'			Value=CString(oField.Value)
'		ELSE
'			IF UCASE(TypeName(oRecordSet))="RECORDSET" THEN
'				IF NOT(oRecordSet.BOF AND oRecordSet.EOF) THEN
'					Debugger Me, aValues(0, iIndex)
'					Value=aValues(0, iIndex)
'				ELSE
'					Value = NULL
'				END IF
'			ELSE
'				Debugger Me, TypeName(oRecordSet)
'				Value = oRecordSet
'			END IF
'		[&Catch] TRUE, Me, "Value", Me.FieldName&": "&iIndex
'		END IF
'		ON ERROR  GOTO 0
	End Property
	Public Property Let Value(input)
		aValues(0, iIndex)=input
	End Property

	Public Property Get PrevValue()
		IF iIndex-1>=0 THEN
			PrevValue = aValues(0, iIndex-1)
		ELSE
			PrevValue = Empty
		END IF
	End Property

	Public Property Get NextValue()
		IF iIndex+1<=Me.RecordCount THEN
			NextValue = aValues(0, iIndex+1)
		ELSE
			NextValue = Empty
		END IF
	End Property

	Public Property Get Format()
		Format = sFormat
	End Property
	Public Property Let Format(input)
		sFormat = input
		IF CString(UCASE(sFormat)).ExistsIn("MONEY, PERCENT") THEN
			IF oValue IS NOTHING THEN 
				Set oValue = new NumericValue
			END IF
			oValue.Format = sFormat
		END IF
	End Property

	Public Property Get DecimalPositions()
		DecimalPositions = iDecimalPositions
	End Property
	Public Property Let DecimalPositions(input)
		iDecimalPositions=CINT(ZeroIfInvalid(input))
	End Property

	Public Function GetCode() 'Function Generate()
		GetCode = Me.Text()
	End Function
	
	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub
	
End Class
 %>