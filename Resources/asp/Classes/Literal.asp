<% 
Class Literal
	'Properties
	Private bVisible
	
	'Settings
	Private bRender
	
	'Objects
	Private oParentCell, oContainer, oRelatives, oValue
	
	'Private Variables
	Public Property Get Object()
		Set Object = Me
	End Property

	Private Sub Class_Terminate()
		Set oValue = nothing
		Set oRelatives = nothing
	End Sub
	Private Sub Class_Initialize()
		Set oValue = nothing
		Set oRelatives = [$Relatives](Me)
		Me.Render=TRUE
		bVisible=TRUE
	End Sub
	
	'Output objects

	'Output Properties 
	Public Property Get Visible()
		Visible = bVisible
	End Property
	Public Property Let Visible(input)
		bVisible = input
	End Property

	Public Property Get Value()
		Set Value = oValue
	End Property

	Public Sub CreateValueObject()
		IF oValue IS NOTHING THEN
			Set oValue = new VariantValue
		END IF
	End Sub
	
	Public Property Get Text()
		IF oValue IS NOTHING THEN
			Text = Empty
		ELSE
			Text = oValue.Text
		END IF
	End Property
	Public Property Let Text(input)
		IF IsObject(Input) THEN
			Set oValue = input
		ELSE 
			CreateValueObject()
			oValue.Value = input
		END IF
	End Property

	Public Property Get Render()
		Render = bRender
	End Property

	Public Property Let Render(input)
		bRender = input
	End Property


	'Input Properties 

	Public Property Get Relatives()
		Set Relatives = oRelatives
	End Property
	
	Public Property Get Container()
		Set Container = oRelatives.Parent
	End Property
	Public Property Let Container(input)
		oRelatives.Parent = input
	End Property

	Public Property Get ParentCell()
		Set ParentCell = oParentCell
	End Property
	Public Property Let ParentCell(input)
		Set oParentCell = input
	End Property
'	Public Property Let OtherProperties(input)
'		sOtherProperties = input
'	End Property
	
	'Public methods
	Public Sub AddContent(ByRef oElement)
		oRelatives.AddChildren oElement
	End Sub

'	Public Sub ClearContent()
'		oContent.ClearContent()
'	End Sub

	Public Function GetCode()
		IF NOT Me.Render THEN 
			GetCode=""
			EXIT Function
		END IF
		IF oValue IS NOTHING THEN
			GetCode=Empty
		ELSE
			GetCode=oValue.Text
		END IF
	End Function

	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub
End Class 
 %>