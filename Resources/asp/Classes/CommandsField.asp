<% 
Function [$CommandsField](ByRef oDataSource)
	Set [$CommandsField] = new CommandsField
	[$CommandsField].DataSource = oDataSource
End Function
Class CommandsField
	'Properties
	'Settings
	Private bRender
	'Objects
	Private oRelatives, oDataSource, oCommands
	
	Private Sub Class_Initialize()
		Set oRelatives = [$Relatives](Me)
		Set oCommands = new Collection
		bRender=TRUE
	End Sub
	Private Sub Class_Terminate()
		Set oRelatives = nothing
		Set oCommands = nothing
	End Sub

	'Interfaces
	Public Property Get Metadata()
		Set Metadata = dMetadata
	End Property
	
	Public Property Get DataSource()
		Set DataSource = oDataSource
	End Property
	Public Property Let DataSource(input)
		oDataSource = input
	End Property

	'Properties
	Public Default Property Get Command(sCommandName)
		Set Command = oCommands(sCommandName)
	End Property


	Public Property Get Commands()
		Set Commands = oCommands
	End Property

	Public Property Get Container()
		Set Container = oRelatives.Parent
	End Property
	Public Property Let Container(input)
		oRelatives.Parent = input
	End Property

	Public Property Get Render()
		Render = bRender
	End Property
	Public Property Let Render(input)
		bRender = input
	End Property

	Public Property Get Visible()
		Visible=oCommands.Visible
	End Property
	Public Property Let Visible(input)
		oCommands.Visible=input
	End Property

	'Methods
	Public Sub GetDBMetadata(sTableName)
		Dim oRecordSet, rsDataSource, i
		Set oRecordSet = new RecordSet
		oRecordSet.SQLQueryString="SELECT * FROM Metadata WHERE TableName='"&sTableName&"' AND ColumnName='"&sFieldName&"'"
		oRecordSet.DataBind()
		IF oRecordSet.RecordSet.BOF AND oRecordSet.RecordSet.EOF THEN 
			IF session("Debugging") THEN RESPONSE.WRITE "<strong>No hay informacion disponible para "&sFieldName&"</strong><br>"
			Exit Sub
		END IF
		DIM sColumnName
		
		DIM oColumn, sTempMetadataString
		Set rsDataSource=oRecordSet.RecordSet
		
'		oRecordSet.ShowProperties()
		response.write "&nbsp;&nbsp;&nbsp;<b>Metadata de la base de datos para "&sFieldName&": </b><br>"
'		FOR i=0 TO oRecordSet.RecordSet.fields.count-1
		FOR EACH oColumn IN rsDataSource.Fields
			IF TRIM(oColumn.Name)<>"ColumnName" THEN
				sColumnName=oColumn.name
				sTempMetadataString=sTempMetadataString&sColumnName&":"&RTRIM(oColumn.Value)&";"
			END IF
		NEXT
		Me.MetadataString=sTempMetadataString
	End Sub
	
	Public Function GetCode()
		IF NOT(Me.Render) THEN GetCode="" : Exit Function END IF
		GetCode=oCommands.GetCode()
	End Function
	
	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub
End Class
 %>