<%
Class TableRelationships
	'Properties
'	Private 
	
	'Settings
	Private bSupportsPaging, bSupportsInsert, bSupportsUpdate, bSupportsDelete

	'Private 
	
	'Objects
	Private oTable, oContainer, oRelatives, oProgress, oDisplayTime, oTabContainer
	
	'Private Properties
	Private sTableCode
	
	Private Sub Class_Initialize()
		Set oDisplayTime = new StopWatch
		Set oTable = new DetailsView
		bSupportsUpdate=TRUE
		bSupportsInsert=TRUE
		bSupportsDelete=TRUE
		Set oTabContainer = new TabContainer
	End Sub
    
	Private Sub Class_Terminate()
		Set oDisplayTime = nothing
		Set oTable = nothing
		Set oContainer = nothing
		Set oTabContainer = nothing
	End Sub
	
	'Interfaces
	Public Property Get Style()
		Set Style = oTable.Style
	End Property

	Public Property Get Properties()
		Set Properties = oTable.Properties
	End Property

	Public Property Get ProgressBar()
		Set ProgressBar = oTable.ProgressBar
	End Property

	Public Property Get DataSource()
		Set DataSource = oTable.DataSource
	End Property
	
	Public Property Get Fields() 'Recupera todos los campos una vez que se hizo DataBind(), si se utiliza antes marca error
		Set Fields = oTable.DataSource.Fields
	End Property

	Public Property Get Field(sFieldName) 'Recupera un campo en espec�fico una vez que se hizo DataBind(), si se utiliza antes marca error. Tambi�n puede acceder al campo por medio del m�todo Fields.Field("FieldName")
		Set Field = oTable.DataSource.Fields.Field(sFieldName)
	End Property

	Public Property Get Relatives()
		Set Relatives = oRelatives
	End Property
	
	Public Property Get Container()
		Set Container = oContainer
	End Property
	Public Property Let Container(input)
		Set oContainer = input
	End Property

	'Properties
	Public Property Get TableIdentifier()
		TableIdentifier = oTable.TableIdentifier
	End Property
	Public Property Let TableIdentifier(input)
		oTable.TableIdentifier = input
	End Property

	Public Property Get ShowRowNumber()
		ShowRowNumber = oTable.DataSource.ShowRowNumber
	End Property
	Public Property Let ShowRowNumber(input)
		oTable.DataSource.ShowRowNumber = input
	End Property

	Public Property Get PageSize()
		PageSize = oTable.DataSource.PageSize
	End Property

	Public Property Get ProgressBarControlId()
		ProgressBarControlId = oTable.ProgressBarControlId
	End Property
	Public Property Let ProgressBarControlId(input)
		oTable.ProgressBarControlId = input
	End Property

	Public Property Get SupportsPaging()
		SupportsPaging = bSupportsPaging
	End Property
	Public Property Let SupportsPaging(input)
		bSupportsPaging = input
	End Property

	Public Property Get SupportsInsert()
		SupportsInsert = bSupportsInsert
	End Property
	Public Property Let SupportsInsert(input)
		bSupportsInsert = input
	End Property

	Public Property Get SupportsUpdate()
		SupportsUpdate = bSupportsUpdate
	End Property
	Public Property Let SupportsUpdate(input)
		bSupportsUpdate = input
	End Property

	Public Property Get SupportsDelete()
		SupportsDelete = bSupportsDelete
	End Property
	Public Property Let SupportsDelete(input)
		bSupportsDelete = input
	End Property

	Public Property Get DataBound()
		DataBound = oTable.DataSource.DataBound
	End Property

	Public Property Get Mode()
		Mode = oTable.Mode
	End Property
	Public Property Let Mode(input)
		oTable.Mode = input
	End Property

	Public Property Get IsReadonly()
		IsReadonly = (UCASE(Me.Mode)="READONLY")
	End Property

	Public Property Get Id()
		Id = oTable.Id
	End Property
	Public Property Let Id(input)
		oTable.Id = input
	End Property

	Public Property Get CssClass()
		CssClass = oTable.CssClass
	End Property
	Public Property Let CssClass(input)
		oTable.CssClass = input
	End Property

	Public Property Get Render()
		Render = oTable.Render
	End Property
	Public Property Let Render(input)
		oTable.Render = input
	End Property

	Public Property Get DBTableName()
		DBTableName = oTable.DBTableName
	End Property
	Public Property Let DBTableName(input)
		oTable.DBTableName=input
	End Property

	Public Property Get FieldNull()
		FieldNull = oTable.DataSource.FieldNull
	End Property
	Public Property Let FieldNull(input)
		oTable.DataSource.FieldNull = input
	End Property

	Public Property Get FieldBlank()
		FieldBlank = oTable.FieldBlank
	End Property
	Public Property Let FieldBlank(input)
		oTable.FieldBlank = input
	End Property

	Public Property Get IsMainTable()
		IsMainTable = oTable.IsMainTable
	End Property
	Public Property Let IsMainTable(input)
		oTable.IsMainTable = input
	End Property

	'Methods
	Public Sub AddRow(ByRef oRow)
		oTable.AddRow(oRow)
	End Sub

	Public Sub DataBind()
		oTable.DataBind()
	End Sub
	
	Public Sub BuildTable()
		IF NOT Me.DataBound THEN Me.DataBind()
		oDisplayTime.StartTimer()

		DIM oTabPanel: Set oTabPanel = new TabPanel
		oTabPanel.CssClass="selected"
		oTabPanel.Id="Panel_"&Me.DBTableName&"_"&Me.TableIdentifier&"_"&RandomNumber(1000)
		oTabPanel.HeaderText=ToTitleFromPascal(Me.DBTableName)
		oTabPanel.AddContent(oTable)
		oTabContainer.AddTab(oTabPanel)
		DIM oField
		FOR EACH oField IN oTable.Fields.ToArray()
			IF TypeName(oField.Control)="TabPanel" THEN
				oTabContainer.AddTab oField.Control
			END IF
		NEXT
		oTabContainer.SetActiveTab(0)

		IF IsObject(oProgress) THEN oProgress.SetComplete()
		oDisplayTime.StopTimer()
	End Sub
	
	Public Function GetCode()
		IF sTableCode="" THEN
			Me.BuildTable()
			sTableCode=oTabContainer.GetCode()
		END IF
		GetCode=sTableCode
	End Function
	
	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub
	
	Public Sub ShowStatistics()
		response.write "<center>"
		response.write "********* <strong>Statistics for "&Me.DBTableName&" (Id "&Me.Id&")</strong> ************* " &"<br>"
		response.write "Open Connection: "&ROUND(oTable.DataSource.TimeOpenConn, 3) &"<br>"
		response.write "Retrieving metadata: "&ROUND(oTable.DataSource.TimeMetadata, 3) &"<br>"
		response.write "Querying: "&ROUND(oTable.DataSource.TimeQuery, 3) &"<br>"
		response.write "Fetching: "&ROUND(oTable.DataSource.TimeFetching, 3) &"<br>"
'		response.write "Formating: "& ROUND(tStartDisplay-tEndFetch, 3) &"<br>"
		response.write "Displaying: "&ROUND(oDisplayTime.TotalTime, 3) &"<br>"
		response.write "<strong>Total: "&ROUND((oTable.DataSource.TimeOpenConn+oTable.DataSource.TimeMetadata+oTable.DataSource.TimeQuery+oTable.DataSource.TimeFetching+oDisplayTime.TotalTime), 3)&"</strong><br>"
		response.write "**************************************** " &"<br><br>"
		response.write "</center>"
	End Sub
End Class
%>