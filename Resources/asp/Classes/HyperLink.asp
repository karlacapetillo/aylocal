<% 
Class HyperLink
	Private sCommandText, sId, sCssClass, sImageURL, sNavigateURL
	
	'Objects 
	Private oTag, oContainer, oRelatives
	
	Public Property Get Visible()
		Visible = oTag.Visible
	End Property
	Public Property Let Visible(input)
		oTag.Visible = input
	End Property

	Private Sub Class_Initialize()
		Set oRelatives = [$Relatives](Me)
		Set oTag = new Tag
		oTag.TagName="label"
		oTag.HasClosingTag=TRUE
		Me.Style.Cursor="hand"
		Me.Style.TextDecoration="underline"
	End Sub

	Public Property Get OnClick()
		OnClick = oTag.Methods.OnClick
	End Property
	Public Property Let OnClick(input)
		oTag.Methods.OnClick = input
	End Property
	
	'Output objects
	Public Property Get Properties()
		Set Properties = oTag.Properties
	End Property

	Public Property Get Style()
		Set Style = oTag.Style
	End Property
	
	'Output Interfaces
	Public Property Get Relatives()
		Set Relatives = oRelatives
	End Property
	
	Public Property Get Container()
		Set Container = oRelatives.Parent
	End Property
	Public Property Let Container(input)
		oRelatives.Parent = input
	End Property

	Public Property Get ParentCell()
		ParentCell = oTag.ParentCell
	End Property
	
	'Input Interfaces
	Public Property Let ParentCell(input)
		oTag.ParentCell = input
	End Property

	Public Property Get Value()
		Set Value = oTag.Value
	End Property

	Public Property Get Text()
		Text=oTag.Text
	End Property
	Public Property Let Text(input)
		oTag.Text=input
	End Property

	'Output Properties 
	Public Property Get CommandText()
		CommandText = sCommandText
	End Property
	
	Public Property Get Id()
		Id = sId
		oTag.Id = sId
	End Property
	
	Public Property Get NavigateURL()
		NavigateURL = sNavigateURL
	End Property
	Public Property Let NavigateURL(input)
		sNavigateURL = input
		oTag.Properties.SetProperty "NavigateURL", "1.X.X_Beta/"&sNavigateURL
		oTag.Methods.OnClick="openModalPage(eval(translateFormula(this.NavigateURL)))"
	End Property

	Public Property Get ImageURL()
		ImageURL = sImageURL
	End Property
	
	Public Property Get CssClass()
		CssClass = sCssClass
		oTag.CssClass = sCssClass
	End Property
	
	'Input Properties 
'	Public Property Let Text(input)
'		sText = input
'		IF TRIM(sText)<>"" THEN 
'			oTag.ClearContent()
'			oTag.AddContent(sText)
'		END IF
'	End Property

	Public Property Let CommandText(input)
		sCommandText = input
		IF TRIM(sCommandText)<>"" THEN oTag.Methods.OnClick "document.location='"&sCommandText&"'"
	End Property
	
	Public Property Let Id(input)
		sId = input
	End Property
	
	Public Property Let ImageURL(input)
		sImageURL = input
	End Property

	Public Property Let CssClass(input)
		sCssClass = input
	End Property
	
	'Public methods
	Public Function GetCode()
'		IF NOT(TRIM(sImageURL)<>"") THEN
'			Err.Raise 1, "ASP 101", "ImageURL"&sErrPropertyNotSet
'		END IF
		GetCode=oTag.GetCode()
	End Function

	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub
End Class 
 %>