<% 
Class TableCell
	'Member Variables
	Private bIsData
	Private iColSpan, iRowSpan
	Private iElementIndex, iRowCount, iPosition, iColumnNumber
	Private oCell, oParentRow, oPrevSibling, oNextSibling, oPrevCell, oNextCell, oTemp
	Private returnString
'	Private sDataField
	Private sDataType, bVisible, bStopFreezing
	Private bNoWrap, sAlign, sVAlign
	Private bAutoRowSpan
	
	Private bIsMaster, iCurrentRecord
    
	'Objects
	Private oStyle
	
	Private Sub Class_Initialize()
		Set oCell = new Tag
		Me.CellType="TD"
'	    bIsData = False
		Me.Visible=TRUE
		Me.Render=TRUE
		Me.NoWrap=TRUE
		Me.Align="left"
		Me.VAlign="top"
		iColSpan=1
		iRowSpan=1
		iPosition=-1
		iColumnNumber=-1
		iCurrentRecord=NULL
		Set oStyle = new Styles
'		bAutoRowSpan=TRUE
	End Sub
    
'	Related Objects
	Public Property Get CurrentRecord()
		CurrentRecord = iCurrentRecord
	End Property

	Public Property Get ParentRow()
		IF IsObject(oParentRow) THEN
			Set ParentRow = oParentRow
		ELSE
			Set ParentRow = nothing
		END IF
	End Property

	Public Sub SetParentRow(ByRef input)
		Set oParentRow = input
	End Sub

	Public Property Get PrevSibling()
		IF IsObject(oPrevSibling) THEN 
			Set PrevSibling = oPrevSibling
		ELSE
			Set PrevSibling = nothing
		END IF
	End Property
	Public Sub SetPrevSibling(ByRef input)
		IF IsObject(input) THEN
			Set oPrevSibling = input
		END IF
	End Sub

	Public Property Get NextSibling()
		IF IsObject(oNextSibling) THEN 
			Set NextSibling = oNextSibling
		ELSE
			Set NextSibling = nothing
		END IF
	End Property
	Public Sub SetNextSibling(ByRef input)
		IF IsObject(input) THEN
			Set oNextSibling = input
		END IF
	End Sub

	Public Property Get PrevCell()
		IF IsObject(oPrevCell) THEN 
			Set PrevCell = oPrevCell
		ELSE
			Set PrevCell = nothing
		END IF
	End Property
	Public Sub SetPrevCell(input)
		IF IsObject(input) THEN
			Set oPrevCell = input
		END IF
	End Sub

	Public Property Get NextCell()
		IF IsObject(oNextCell) THEN 
			Set NextCell = oNextCell
		ELSE
			Set NextCell = nothing
		END IF
	End Property
	Public Sub SetNextCell(input)
		IF IsObject(input) THEN
			Set oNextCell = input
		END IF
	End Sub

	'Output Objects
	Public Property Get Style()
		Set Style = oStyle
	End Property
	
	'Properties
	Public Property Get AutoRowSpan()
		AutoRowSpan = bAutoRowSpan
	End Property
	Public Property Let AutoRowSpan(input)
		bAutoRowSpan = input
	End Property

	Public Property Get Id()
		Id = oCell.Id
	End Property
	Public Property Let Id(input)
		oCell.Id = input
    End Property
    
    Public Property Get CssClass()
		CssClass=oCell.CssClass
    End Property

	Public Property Get Mode()
		Mode = oCell.Mode
	End Property
	Public Property Let Mode(input)
		oCell.Mode = input
	End Property
	
	Public Property Get CellType()
		CellType = oCell.TagName
	End Property

	Public Property Let CellType(input)
		SELECT CASE UCASE(input)
		CASE "TH", "TD"
			oCell.TagName=input
		CASE ELSE 
			Err.Raise 1, "ASP 101", "Invalid CellType"
			response.end
		END SELECT
	End Property
	
'	Public Property Let DataField(var_sDataField)
'		sDataField=var_sDataField
'	End Property
'	Public Property Get DataField()
'		DataField=sDataField
'	End Property

	Public Property Let DataType(input)
		sDataType=input
	End Property
	Public Property Get DataType()
		DataType=sDataType
	End Property
	
	Public Property Let Render(input)
		oCell.Render=input
	End Property
	Public Property Get Render()
		Render=oCell.Render
	End Property
	
	Public Property Let Visible(input)
		oCell.Visible=input
	End Property
	Public Property Get Visible()
		Visible=oCell.Visible
	End Property	

	Public Property Let StopFreezing(var_bStopFreezing)
		bStopFreezing=var_bStopFreezing
	End Property
	Public Property Get StopFreezing()
		StopFreezing=bStopFreezing
	End Property
	
	Public Property Let CurrentRecord(input)
		iCurrentRecord = input
	End Property
	
	Public Property Let Position(input)
		iPosition=input
	End Property
	Public Property Get Position()
		Position=iPosition
	End Property
	
	Public Property Get ColSpan()
		ColSpan = iColSpan
	End Property

	Public Property Let ColSpan(input)
		iColSpan=input
		IF TRIM(iColSpan)>1 THEN 
			oCell.Properties.SetProperty "colspan", iColSpan
		ELSE
			oCell.Properties.RemoveProperty("colspan")
		END IF
	End Property
	
	Public Property Get RowSpan()
		RowSpan = iRowSpan
	End Property
	Public Property Let RowSpan(input)
		iRowSpan=input
		IF TRIM(iRowSpan)>1 THEN 
			oCell.Properties.SetProperty "rowspan", iRowSpan
		ELSE
			oCell.Properties.RemoveProperty("rowspan")
		END IF
	End Property
	
	Public Property Get NoWrap()
		NoWrap = bNoWrap
	End Property

	Public Property Let NoWrap(input)
		bNoWrap=input
		IF CBOOL(bNoWrap) THEN 
			oCell.Properties.SetProperty "nowrap", null
		ELSE
			oCell.Properties.RemoveProperty "nowrap"
		END IF
	End Property
	
	Public Property Get Align()
		Align = sAlign
	End Property
	Public Property Let Align(input)
		sAlign=input
		oCell.Properties.SetProperty "align", sAlign
	End Property	
	
	Public Property Get VAlign()
		VAlign = sAlign
	End Property
	Public Property Let VAlign(input)
		sVAlign=input
		oCell.Properties.SetProperty "valign", sVAlign
	End Property	
	
    Public Property Let CssClass(input)
		oCell.CssClass = input
    End Property
       
	Public Property Let ColumnNumber(var_iColumnNumber)
		iColumnNumber=var_iColumnNumber
	End Property
	Public Property Get ColumnNumber()
		ColumnNumber=iColumnNumber
	End Property
	
	'Public methods
	Public Sub AddContent(ByRef oElement)
		oCell.AddContent oElement
		IF IsObject(oElement) THEN
			''@'ON ERROR RESUME NEXT
			oElement.ParentCell=Me
			IF ERR.Number<>0 THEN 
				RESPONSE.WRITE "la propiedad ParentCell no est� definida para el tipo de objeto "&TypeName(oElement)
				response.end
			END IF
		END IF
	'	''@'ON ERROR RESUME NEXT

'			IF Err.Number<>0 THEN 
'		'ON ERROR GOTO 0
		bIsData=TRUE
	End Sub

'	Public Sub SetRelatives()
'		Set oTemp=oParentRow.PrevRow
'		IF IsObject(oTemp) THEN
'			IF NOT oTemp IS NOTHING THEN
'				Set oTemp=oParentRow.PrevRow.GetCellByPosition(iPosition)
'				Me.SetPrevCell(oTemp)
'				IF IsObject(oTemp) THEN
'					IF NOT oTemp IS NOTHING THEN
'						oTemp.SetNextCell(Me)
'					END IF
'				END IF
'			END IF
'		END IF
'	End Sub
	
	Public Sub SetCurrentRecordForDynamicData()
		DIM oItem, oCollection, i
		oCollection=Me.GetItemsByType("DynamicField")
		FOR i=1 TO oCollection(0)
			oCollection(i).CurrentRecord=Me.CurrentRecord
		NEXT
	End Sub
	
	Public Function GetItemsByType(sType)
		DIM i, aItems(2000), oItem
		FOR EACH oItem IN oCell.GetItems()
			IF IsObject(oItem) THEN
				IF NOT oItem IS nothing THEN
					IF TypeName(oItem)=sType THEN
						aItems(0)=aItems(0)+1
						Set aItems(aItems(0))=oItem
					END IF
				END IF
			END IF
		NEXT
		GetItemsByType=aItems
	End Function
	
	Public Function GetCode() 'Function Generate()
		GetCode = oCell.GetCode()
	End Function
	
	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub

	Private Sub Class_Terminate()
	End Sub
End Class 
%>