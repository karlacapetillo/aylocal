<% 
Class RSField
	'Properties
	Private sFieldName, iIndex, vValue, sFormat, iDecimalPositions
	'Objects
	Private aValues, oValue, oRecordSet, oField
	
	Private Sub Class_Initialize()
		iDecimalPositions=NULL
		iIndex=0
		Set oValue = nothing
		Set oField = nothing
	End Sub
	Private Sub Class_Terminate()
		Set oRecordSet = nothing
		Set oValue = nothing
		Set oField = nothing
	End Sub
	
	Public Property Get Object()
		Set Object = oValue
	End Property

	Public Property Get RecordSet()
		Assign RecordSet, oRecordSet
	End Property
	Public Property Let RecordSet(input)
		aValues = input.GetRows(-1, 1, sFieldName)
		Assign oRecordSet, input
	End Property

	Public Property Get Field()
		Set Field = oField
	End Property
	Public Property Let Field(input)
		Debugger Me, " <--"
		Set oField = input
	End Property

	Public Property Get Array()
		Array = aValues
	End Property

	Public Property Get FieldName()
		FieldName = sFieldName
	End Property
	Public Property Let FieldName(input)
		sFieldName = input
	End Property

	Public Property Get Index()
		Index = iIndex
	End Property
	Public Property Let Index(input)
		iIndex = input
	End Property

	Public Property Get RecordCount()
		RecordCount = ubound(aValues, 2)
	End Property

	Public Function MoveNext()
		iIndex=iIndex+1
		MoveNext=aValues(0, iIndex)
	End Function

	Public Function MoveTo(input)
		iIndex=input
		MoveTo=aValues(0, iIndex)
	End Function

	Public Function MoveFirst()
		iIndex=0
		MoveFirst=aValues(0, iIndex)
	End Function

	Public Function MoveLast()
		iIndex=UBOUND(aValues)-1
		MoveFirst=aValues(0, iIndex)
	End Function

	Public Function MovePrevious()
		iIndex=iIndex-1
		MovePrevious=aValues(0, iIndex)
	End Function

	Public Default Property Get Text()
		Text = FormatValue(Me.Value, sFormat, iDecimalPositions)
	End Property
'	Public Property Let Text(input)
'		'NOT AVAILABLE
'	End Property

	Public Property Get Value()
'		ON ERROR  RESUME NEXT
		IF NOT oField IS NOTHING THEN
			Value=CString(oField)
		ELSE
			IF UCASE(TypeName(oRecordSet))="RECORDSET" THEN
				IF NOT(oRecordSet.BOF AND oRecordSet.EOF) THEN
					Debugger Me, aValues(0, iIndex)
					Value=aValues(0, iIndex)
				ELSE
					Value = NULL
				END IF
			ELSE
				Debugger Me, TypeName(oRecordSet)
				Value = oRecordSet
			END IF
		[&Catch] TRUE, TypeName(Me)&".Value", ""
		END IF
		ON ERROR  GOTO 0
	End Property
'	Public Property Let Value(input)
'		'NOT AVAILABLE
'	End Property

	Public Property Get Format()
		Format = sFormat
	End Property
	Public Property Let Format(input)
		sFormat = input
		IF CString(UCASE(sFormat)).ExistsIn("MONEY, PERCENT") THEN
			IF oValue IS NOTHING THEN 
				Set oValue = new NumericValue
			END IF
			oValue.Format = sFormat
		END IF
	End Property

	Public Property Get DecimalPositions()
		DecimalPositions = iDecimalPositions
	End Property
	Public Property Let DecimalPositions(input)
		iDecimalPositions=CINT(ZeroIfInvalid(input))
	End Property

	Public Function GetCode() 'Function Generate()
		GetCode = Me.Text()
	End Function
	
	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub
	
End Class
 %>