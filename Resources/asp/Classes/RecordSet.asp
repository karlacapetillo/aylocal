<% 
Class RecordSet
	Private sFieldNull, bShowRowNumber, bDataBound
	Private sConn, oCn, oLCn, sSQLQueryString, sPagedSQLQueryString, sConnectionString, rsRecordSet, aRecords, oMetadata, oFields
	Private bEnablePaging, iPageSize, iPageIndex, sRankORDERBY, rsAllOriginalRecords, sAllOriginalRecords, iTotalRecords, sDBLanguage, sTableViewMode, sRenderMode, sQueryFilter, dFilterMembers
	Private oPageSelector
    Private oTimerOpenConection, oTimerQuery, oTimeFetching, oTimeMetadata, dParameters
	
	Private sMode
	
	Private Sub Class_Terminate()
		IF NOT oCn IS NOTHING THEN oCn.Close
		set oCn=nothing
		set rsAllOriginalRecords=nothing
		set rsRecordSet=nothing
		Set dFilterMembers = NOTHING
		Set oTimerOpenConection = nothing
		Set oTimerQuery = nothing
		Set oTimeFetching = nothing
		Set oTimeMetadata = nothing
		Set oPageSelector = nothing
	End Sub

	Private Sub Class_Initialize()
		Set oTimerOpenConection = new StopWatch
		Set oTimerQuery = new StopWatch
		Set oTimeFetching = new StopWatch
		Set oTimeMetadata = new StopWatch
		set oCn=server.createobject("adodb.connection")
		set oLCn=server.createobject("adodb.connection")
		Set rsRecordSet = Server.CreateObject("ADODB.RecordSet")
		rsRecordSet.CursorLocation 	= 3
		rsRecordSet.CursorType 		= 3
		Set oMetadata = new TableMetadata
		Set dFilterMembers = server.CreateObject("Scripting.Dictionary")
		Set dParameters = server.CreateObject("Scripting.Dictionary")
		sConn=application("StrCnn")
		OpenConnection()
		IF request.querystring("PageIndex")<>"" THEN
			iPageIndex = CINT(request.querystring("PageIndex"))
		ELSEIF request.form("PageIndex")<>"" THEN
			iPageIndex = CINT(request.form("PageIndex"))
		ELSE
			iPageIndex=1
		END IF
		sFieldNull="&nbsp;"
		bShowRowNumber=TRUE
		Set oPageSelector = nothing
	End Sub
	
	Public Property Get QueryFilter()
		QueryFilter = sQueryFilter
	End Property
	Public Property Let QueryFilter(input)
		sQueryFilter = input
	End Property

	Public Property Get Parameters()
		Set Parameters = dParameters
	End Property

	Public Sub ShowProperties()
		response.write "Properties for recordset: <br>"
		DIM oColumn
		FOR EACH oColumn IN rsRecordSet.Properties
			response.write "<b>"&oColumn.Name&"</b>: "&oColumn.Value&"<br>"
		NEXT
	End Sub

	Public Sub GetMetadataFromDB(ByVal sDBTableName, ByVal sTableViewMode, ByVal sRenderMode)
		oMetadata.GetMetadataFromDB sDBTableName, sTableViewMode, sRenderMode
		IF UCASE(sRenderMode)="FILTER" OR UCASE(sRenderMode)="INSERT" THEN 
			Me.PageSize=0
		ELSEIF IsNullOrEmpty(Me.PageSize) THEN 
			Me.PageSize=oMetadata.RecordsPerPage
		END IF
		IF oMetadata.AppTableFilter THEN Me.QueryFilter=Me.QueryFilter&EVAL(REPLACE(oMetadata.AppTableFilter, "", """"))
	End Sub
	
	Public Sub AutoCreate(ByVal sDBTableName, ByVal sTableViewMode, ByVal sRenderMode)
		oTimeMetadata.StartTimer()
'		Debugger Me, "DEBUGGING... ": RESPONSE.END
		Me.GetMetadataFromDB sDBTableName, sTableViewMode, sRenderMode
'		Debugger Me, "DEBUGGING... "
		DIM sParameter, sTableParameters: sTableParameters=oMetadata.TableParameters
		DIM bUseParameters
		IF NOT(IsNullOrEmpty(TRIM(sTableParameters))) THEN
			FOR EACH sParameter IN SPLIT(sTableParameters, ",")
			'Debugger Me, sParameter
				sParameter=replaceMatch(sParameter, "(\[|\])", "")
				DIM bParameterInQuerystring: bParameterInQuerystring=(INSTR("&@"&TRIM(request.querystring)&"=", "&"&TRIM(sParameter)&"=")>0)
				DIM sParameterValue: sParameterValue=request.querystring(TRIM(sParameter))
				sParameterValue=Try((NOT(IsNumeric(sParameterValue)) AND NOT(IsNullOrEmpty(sParameterValue)) ), QuoteName(sParameterValue), sParameterValue)
				bUseParameters=Try((NOT(bUseParameters) AND bParameterInQuerystring), TRUE, CBOOL(bUseParameters))
				dParameters(sParameter)=Try((bParameterInQuerystring AND NOT(IsNullOrEmpty(sParameterValue))), sParameterValue, "NULL")
			NEXT
		END IF
		DIM sColumn
		FOR EACH sColumn IN SPLIT(oMetadata.AllColumns, ",")
			sColumn=replaceMatch(sColumn, "(\[|\])", "")
			IF INSTR("&"&TRIM(request.querystring)&"=", "&"&TRIM(sColumn)&"=")>0 THEN 
				dFilterMembers(TRIM(sColumn))=request.querystring(TRIM(sColumn))
				IF TRIM(request.querystring(TRIM(sColumn)))="NULL" THEN 
					Me.QueryFilter=Me.QueryFilter&" AND "&TRIM(sColumn)&" IS "&request.querystring(TRIM(sColumn))&"" 
				ELSE
					Me.QueryFilter=Me.QueryFilter&" AND "&TRIM(sColumn)&"='"&request.querystring(TRIM(sColumn))&"'" 
				END IF
			END IF
		NEXT
		
'		RESPONSE.WRITE "QueryFilter: "&Me.QueryFilter
'		RESPONSE.END
		oTimeMetadata.StopTimer()

		DIM sParameters: sParameters=REPLACE(JOIN(dParameters.items, ","), "'", "''")
		Me.SQLQueryString="dbo.__GetTableData @TableName='"& Try(NOT(bUseParameters), sDBTableName, oMetadata.UnderlayingTable) &"', @Parameters="& Try(bUseParameters, QuoteName(sParameters), "NULL") &", @ViewMode='"&sTableViewMode&"', @Mode='"&sRenderMode&"', @QueryFilters='"&REPLACE(Me.QueryFilter, "'", "''")&"', @InitialRecord="& NullToText(NullIfEmptyOrNullText(Me.InitialRecord)) &", @PageSize="& NullToText(NullIfEmptyOrNullText(iPageSize)) &""
		response.write Me.SQLQueryString&"<br>"
		response.end
		Me.DataBind()
		IF IsObjectReady(Me.Metadata) THEN
			oTimeFetching.StartTimer()
			DIM oField, i, sFieldName, sMetadataString
			Set oFields = new DataFieldCollection
			oFields.DataSource=Me.RecordSet
			sMetadataString=Me.Metadata.ColumnsMetadataString
'			Debugger Me, sMetadataString&"<br><br><br>"
			FOR i=0 TO rsRecordSet.Fields.Count-1
				IF NOT( rsRecordSet(i).name="SQLRowCount" OR (rsRecordSet(i).name="SQLRowNumber" AND NOT(Me.ShowRowNumber)) ) THEN
					Set oField = new DataField
					sFieldName=rsRecordSet(i).name 'getDisplayName(rsRecordSet(i).name)
					oField.FieldName=sFieldName
					oField.FieldNull=Me.FieldNull
					oField.Data=Me.RecordSet
					oField.Mode=Me.Mode
					oField.MetadataString=getMetadataString(sMetadataString, sFieldName)
'					Debugger Me, "<strong>"&sFieldName&"</strong>: "&oField.MetadataString
					IF dFilterMembers.Exists(TRIM(sFieldName)) THEN oField.Visible=FALSE
					IF request.querystring("ParentIdentifier")=TRIM(sFieldName) THEN oField.Render=FALSE 'IsSubmitable=FALSE
					oFields.AddDataField(oField)
	'				oField.GetDBMetadata(oField.TableName)
				END IF
			NEXT
			oTimeFetching.StopTimer()
		END IF
		IF oFields.ExistsItem("SQLRowNumber") THEN oFields("SQLRowNumber").HeaderText="#"
	End Sub

	Public Property Get Fields()
		Set Fields = oFields'.GetItems()
	End Property

	Public Property Get Metadata()
		Set Metadata = oMetadata
	End Property

	Public Property Get PageSelector()
		Set PageSelector = oPageSelector
	End Property

	Public Property Get DBLanguage()
		DBLanguage = sDBLanguage
	End Property
	Public Property Let DBLanguage(input)
		sDBLanguage = input
		Me.SetDBLanguage(sDBLanguage)
	End Property
	
	Public Property Get FieldNull()
		FieldNull = sFieldNull
	End Property
	Public Property Let FieldNull(input)
		sFieldNull = input
	End Property

   Public Property Get RecordSet()
		Set RecordSet = rsRecordSet
    End Property

	Public Property Get ShowRowNumber()
		ShowRowNumber = bShowRowNumber
	End Property
	Public Property Let ShowRowNumber(input)
		bShowRowNumber = input
	End Property

	Public Property Get DataBound()
		DataBound = bDataBound
	End Property

	Public Property Get Mode()
		Mode = sMode
	End Property
	Public Property Let Mode(input)
		sMode = input
	End Property

	Public Property Get FilterMembers()
		Set FilterMembers = dFilterMembers
	End Property

    Public Property Get TotalRecords()
		TotalRecords = iTotalRecords
    End Property

    Public Property Get EnablePaging()
		EnablePaging = bEnablePaging
    End Property
    Public Property Let EnablePaging(input)
		bEnablePaging = input
    End Property

	Public Property Get InitialRecord()
		InitialRecord = iPageSize*(iPageIndex-1)
	End Property

    Public Property Get PageSize()
		PageSize = iPageSize
    End Property
    Public Property Let PageSize(input)
		iPageSize = input
    End Property

    Public Property Get PageIndex()
		PageIndex = iPageIndex
    End Property
    Public Property Let PageIndex(input)
		iPageIndex = input
    End Property

    Public Default Property Get SQLQueryString()
		SQLQueryString = sSQLQueryString
    End Property
    Public Property Let SQLQueryString(input)
		sSQLQueryString = input
    End Property
	
    Public Property Get PagedSQLQueryString()
		PagedSQLQueryString = sPagedSQLQueryString
    End Property

    Public Property Get ConnectionString()
		SQLQueryString = sConnectionString
    End Property
    Public Property Let ConnectionString(input)
		sConnectionString = input
    End Property
	
    Public Property Get TimeOpenConn()
		TimeOpenConn = 	oTimerOpenConection.TotalTime()
    End Property
	
	Public Property Get TimeQuery()
		TimeQuery = oTimerQuery.TotalTime
    End Property
       
	Public Property Get TimeFetching()
		TimeFetching = oTimeFetching.TotalTime
	End Property

	Public Property Get TimeMetadata()
		TimeMetadata = oTimeMetadata.TotalTime
	End Property

	Public Sub OpenConnection()
		oTimerOpenConection.StartTimer()
		oCn.open sConn
		oCn.CommandTimeout = 120 
		oTimerOpenConection.StopTimer()
	End Sub
	
	Public Sub Paginate()
		sPagedSQLQueryString=TRIM(sSQLQueryString)
		sRankORDERBY=RIGHT(sPagedSQLQueryString, LEN(TRIM(sPagedSQLQueryString))-INSTRREV(UCASE(sPagedSQLQueryString), "ORDER BY")+1)
		sPagedSQLQueryString=REPLACE(">"&sPagedSQLQueryString, ">SELECT ", "SELECT * FROM ( SELECT TOP 100000 ROW_NUMBER() OVER("&sRankORDERBY&") AS SQLRowNumber, ")&") AS SQLQuery WHERE SQLRowNumber BETWEEN "&Me.InitialRecord+1&" AND "&Me.InitialRecord+iPageSize
		sAllOriginalRecords=REPLACE(">"&sSQLQueryString, ">SELECT ", "SELECT COUNT(*) Counted FROM ( SELECT TOP 100000 ")&") AS SQLQuery"
'		''@'ON ERROR RESUME NEXT
		Set rsAllOriginalRecords = oCn.Execute(sAllOriginalRecords)
		IF Err.Number<>0 THEN
			RESPONSE.WRITE Err.Description &"<br><br>"
			IF application("debug_system") THEN
				RESPONSE.WRITE "SQL: "& sAllOriginalRecords
			END IF
			RESPONSE.END
		END IF
		
		IF NOT(rsAllOriginalRecords.BOF AND rsAllOriginalRecords.EOF) THEN
			iTotalRecords=rsAllOriginalRecords("Counted")
		ELSE
			iTotalRecords=0
		END IF 
	End Sub
	
	Public Sub SetDBLanguage(ByVal sLanguage)
'		IF sLanguage<>sDBLanguage THEN
		oLCn.open sConn
		oLCn.Execute("sp_defaultlanguage @loginame = 'sa' ,@language = '"&sLanguage&"'")
		oLCn.Execute("SET LANGUAGE "&sLanguage)
'		response.write "Language set to "&sLanguage&"<br>"
		oLCn.close
'		END IF
	End Sub
	
	Public Sub DataBind()
		IF sSQLQueryString="" THEN 
			Err.Raise 1, "ASP 101", "SQL Query is not defined"
			response.end
		END IF
'		response.write sSQLQueryString&"<br>"
		oTimerQuery.StartTimer()
		IF bEnablePaging THEN
			Me.Paginate()
			set rsRecordSet=oCn.execute(sPagedSQLQueryString)
		ELSE
'			set rsRecordSet=oCn.execute(sSQLQueryString)
			ON ERROR  RESUME NEXT
		Set rsRecordSet = Server.CreateObject("ADODB.RecordSet")
		rsRecordSet.CursorLocation 	= 3
		rsRecordSet.CursorType 		= 3
			rsRecordSet.open sSQLQueryString, oCn

			IF Err.Number<>0 THEN
				Debugger Me, Err.Description 
				Debugger Me, "SQL Query: "&sSQLQueryString
				response.end
			END IF
			DIM sFieldName, i, bSQLRowCountExists: bSQLRowCountExists=FALSE
			FOR i=0 TO rsRecordSet.Fields.Count-1
				IF rsRecordSet(i).name="SQLRowCount" THEN 
					bSQLRowCountExists=TRUE
					EXIT FOR
				END IF
			NEXT
			
			IF rsRecordSet.BOF AND rsRecordSet.EOF THEN
				iTotalRecords=0
			ELSEIF bSQLRowCountExists THEN
				iTotalRecords = rsRecordSet("SQLRowCount") 'rsRecordSet.RecordCount
			ELSE
				iTotalRecords = rsRecordSet.RecordCount
			END IF
			
'			iTotalRecords= rsRecordSet.QueryTotalRecord
			'response.write rsRecordSet.RecordCount
		END IF
		oTimerQuery.StopTimer()
'		response.write "<br>"&sMetadataString
		IF Me.PageSize>0 THEN
			Set oPageSelector = new Div
			oPageSelector.Id="freeze"
			DIM oImgLArrow: Set oImgLArrow=new ImageButton
			oImgLArrow.ImageURL="\"&application("btn_LArrow_path")
			oImgLArrow.Disabled=TRUE
			oImgLArrow.Width=20
			oImgLArrow.Height=20
			DIM oImgRArrow: Set oImgRArrow=new ImageButton
			oImgRArrow.ImageURL="\"&application("btn_RArrow_path")
			oImgRArrow.Disabled=TRUE
			oImgRArrow.Width=20
			oImgRArrow.Height=20
			
			DIM sCurrentURL: sCurrentURL=Request.serverVariables("PATH_INFO")
			IF Request.ServerVariables("QUERY_STRING")<>"" THEN
				sCurrentURL=sCurrentURL&"?"&REPLACE(Request.ServerVariables("QUERY_STRING"), "'", "\'")
			END IF
			DIM sGoBackURL: sGoBackURL=updateURLString(sCurrentURL, "PageIndex", Me.PageIndex-1)
			DIM sGoForwardURL: sGoForwardURL=updateURLString(sCurrentURL, "PageIndex", Me.PageIndex+1)
			IF Me.PageIndex-1>0 THEN
				oImgLArrow.CommandText="OpenLink(encodeURI('"&REPLACE(REPLACE(sGoBackURL, "'", "\'"), "\\'", "\'")&"'), false, true)"
				oImgLArrow.Disabled=FALSE
			END IF
			DIM iTotalPages: iTotalPages=ROUND((Me.TotalRecords/Me.PageSize)+.5, 0)
			IF Me.PageIndex+1<=iTotalPages THEN 
				oImgRArrow.CommandText="OpenLink(encodeURI('"&REPLACE(REPLACE(sGoForwardURL, "'", "\'"), "\\'", "\'")&"'), false, true)"
				oImgRArrow.Disabled=FALSE
			END IF
			DIM oDropDownSelector: Set oDropDownSelector = new NumberList
			oDropDownSelector.Control.NewOption CSTR(Me.PageIndex), CSTR(Me.PageIndex)
			oDropDownSelector.Control.OptionChoose=FALSE
			oDropDownSelector.Control.Properties.SetProperty "isSubmitable", "false"
			oDropDownSelector.MinValue=1
			oDropDownSelector.MaxValue=iTotalPages
			oDropDownSelector.Methods.OnChange="OpenLink(encodeURI('"&updateURLString(REPLACE(REPLACE(sCurrentURL, "'", "\'"), "\\'", "\'"), "PageIndex", "'+this.value+'")&"'), false, true)"
			oDropDownSelector.Value=CSTR(Me.PageIndex)&"@:@"&CSTR(Me.PageIndex)
			oPageSelector.AddContent(oImgLArrow)
			oPageSelector.AddContent("&nbsp;<b>P�gina: ")
			oPageSelector.AddContent(oDropDownSelector)
			oPageSelector.AddContent(" / "&iTotalPages&"</b>&nbsp;")
			oPageSelector.AddContent(oImgRArrow)
		END IF
		bDataBound=TRUE
	End Sub
End Class
 %>