<%
class ClassHelper
	private dict 

	public default function addMethod(name)
		set dict(name) = getRef(dict("type") & name)
	end function
	
	private function createDict()
		set createDict = server.CreateObject("scripting.dictionary")
	end function
	
	private function copyDict(super,child)
		dim key
		
		for each key in super
			if isObject(super(key)) then
				set child(key) = super(key)
			else
				child(key) = super(key)
			end if
		next
		
		set copyDict = child
	end function
	
	public function newClass(name)
		set dict = createDict()
		dict("type") = name
		set newClass = dict
	end function
	
	public function extends(super,childType)
		set dict = copyDict(super,createDict())
		set dict("parent") = super
		dict("type") = childType		
		set extends = dict
	end function
	
	public function [new](super,arrParams)	
		dim dict : set dict = copyDict(super,createDict())
		call dict("initialize")(dict,arrParams)
		set [new] = dict
	end function
end class

dim klass : set klass = new ClassHelper

'START NEW CLASS
	dim Fighter : set Fighter = klass.newClass("Fighter")

	'PROPERTIES
	Fighter("name") = "unkown"
	Fighter("power") = 100
	Fighter("speed") = 100
	
	'METHODS
	function FighterInitialize(this,arrParams)
		this("name") = arrParams(0)
	end function : klass "initialize"
	
	function FighterSwingSword()
		FighterSwingSword = "slash slash"
	end function : klass "swingSword"
	
	function FighterIncreasePower(this,power)
		dim thisPower
		thisPower = this("power")
		thisPower = thisPower + power
		this("power") = thisPower 
	end function : klass "increasePower"
'END CLASS

'START NEW CLASS
	dim Ninja : set Ninja = klass.extends(Fighter,"Ninja")
	
	'PROPERTIES
	Ninja("stealth") = 100
	Ninja("speed") = 80
	
	'METHODS
	function NinjaInitialize(this,arrParams)
		this("stealth") = arrParams(0)
		this("name") = arrParams(1)
	end function : klass "initialize"
	
	function NinjaSwingSword()
		NinjaSwingSword = "woosh woosh"
	end function : klass "swingSword"
'END CLASS

'START NEW CLASS
	dim DarkNinja : set DarkNinja = klass.extends(Ninja,"DarkNinja")
'END CLASS

'USE CLASSES
dim fighterJohn : set fighterJohn = klass.new(Fighter,array("john"))
dim ninjaBob : set ninjaBob = klass.new(Ninja,array(70,"bob"))
dim darkNinjaBill : set darkNinjaBill = klass.new(DarkNinja,array(20,"bill"))

response.write Fighter("type") & "<br />"
response.write ninjaBob("type") & " is a child of " & ninjaBob("parent")("type") & "<br />"
response.write darkNinjaBill("type") & " is a child of " & darkNinjaBill("parent")("type") & " who is a child of " & darkNinjaBill("parent")("parent")("type") & "<br />"
response.write "<br />"

response.write "i'm " & fighterJohn("name") & ", who are you? <br />"
response.write "i'm " & ninjaBob("name") & " and i'm a " & ninjaBob("type") & "<br />"
response.write "well i'm a true " & fighterJohn("type") & " and i hate ninjas! take this " & fighterJohn("swingSword")()  & "<br />"
response.write "I have a stealth rating of " & ninjaBob("stealth") & " and a speed rating of " & ninjaBob("speed") & "! you'll never hit me... now take this " & ninjaBob("swingSword")() & " " & ninjaBob("parent")("swingSword")() & "<br />"
response.write "You missed! as i have a speed rating of: " & fighterJohn("speed") & " ha ha ha <br />"
response.write "Wait! " & fighterJohn("name") & ", you are my father! I may be a " & ninjaBob("type") & ", but I am a descendant of " & ninjaBob("parent")("type") & "<br />"
response.write "Well I am your grandson " & fighterJohn("name") & "! I have a power rating of " & darkNinjaBill("power") & " but as my father " & ninjaBob("name") & " is here I can increase my power to " & darkNinjaBill("increasePower")(darkNinjaBill,200) & darkNinjaBill("power")
%>