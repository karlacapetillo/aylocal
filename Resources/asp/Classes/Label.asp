<% 
Function [$Label](ByVal sParameters, ByVal sText)
	Set [$Label]=[&New]("Label", sParameters)
	[$Label].Text=sText
End Function
Class Label
	'Properties
	Private sText, sTextMode, iWidth
	
	'Settings
	Private bRender
	
	'Objects
	Private oLabel, oLiteral, oParentCell, oContainer, oRelatives, oValue
	
	'Private Variables
	Private aElements(300)
	
	Public Property Get Object()
		Set Object = oLabel
	End Property


	Public Property Get Style()
		Set Style = oLabel.Style
	End Property
	Public Property Let Style(input)
		oLabel.Style = input
	End Property

	'Output objects
	Public Property Get Methods()
		Set Methods = oLabel.Methods
	End Property
	
	Public Property Get Properties()
		Set Properties = oLabel.Properties
	End Property
	Public Property Let Properties(input)
		oLabel.Properties = input
	End Property

	Private Sub Class_Initialize()
'		Set oRelatives = [$Relatives](Me)
		Set oLabel = [$Tag]("label")
'		oLabel.TagName="label"
		oLabel.HasClosingTag=TRUE
		Set oLiteral = new Literal
		oLabel.AddContent(oLiteral)
		Me.Render=TRUE
	End Sub
	
	Private Sub Class_Terminate()
		Set oLabel = nothing
		Set oLiteral = nothing
		Set oRelatives = nothing
	End Sub

	'Output Properties 
	Public Property Get CssClass()
		CssClass = oLabel.CssClass
	End Property
	Public Property Let CssClass(input)
		oLabel.CssClass = input
	End Property


	Public Property Get Width()
		Width = iWidth
	End Property
	Public Property Let Width(input)
		iWidth = input
	End Property


	Public Property Get TextMode()
		TextMode = sTextMode
	End Property
	Public Property Let TextMode(input)
		sTextMode = input
	End Property


	Public Property Get Id()
		Id = oLabel.Id
	End Property
	Public Property Let Id(input)
		oLabel.Id = input
	End Property


	Public Property Get Value()
		Set Value = oLiteral.Value
	End Property

	Public Property Get Text()
		Text=oLiteral.Text
	End Property
	Public Property Let Text(input)
		oLiteral.Text=input
	End Property


	Public Property Get Visible()
		Visible = oLabel.Visible
	End Property
	Public Property Let Visible(input)
		oLabel.Visible = input
	End Property

	Public Property Get Render()
		Render = oLabel.Render
	End Property
	Public Property Let Render(input)
		oLabel.Render = input
	End Property


	Public Property Get Relatives()
		Set Relatives = oRelatives
	End Property
	
	Public Property Get Container()
		Set Container = oRelatives.Parent
	End Property
	Public Property Let Container(input)
		oRelatives.Parent = input
	End Property

	Public Property Get ParentCell()
		ParentCell = oLabel.ParentCell
	End Property
	Public Property Let ParentCell(input)
		oLabel.ParentCell = input
	End Property

	'Public methods
	Public Function GetCode()
		IF TypeName(oLiteral.Value)="DataValue" THEN oLiteral.Value.FixedLength=50 END IF
		GetCode=oLabel.GetCode()
	End Function

	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub
End Class 
 %>