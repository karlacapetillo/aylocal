<%
Class DataTable
	'Properties
	Private sFieldNull, sDBTableName, sHeaderTemplate
	
	'Settings
	Private bShowRowNumbers, bDataBound

	'Private 
	
	'Objects
	Private oTable, oDataSource, oContainer, oRelatives, dDataFields
	
	'Private Properties
	Private sTableCode
	
	Private Sub Class_Initialize()
		Set oRelatives = [$Relatives](Me)
		Set oTable = new Table
		Set oDataSource = new RecordSet
		Set dDataFields=server.CreateObject("Scripting.Dictionary")
		bShowRowNumbers=TRUE
		sFieldNull="&nbsp;"
	End Sub
    
	Private Sub Class_Terminate()
		Set oTable = nothing
		Set oContainer = nothing
	End Sub
	
	'Interfaces
	Public Property Get Style()
		Set Style = oTable.Style
	End Property

	Public Property Get Properties()
		Set Properties = oTable.Properties
	End Property

	Public Property Get Relatives()
		Set Relatives = oRelatives
	End Property
	
	Public Property Get ProgressBar()
		Set ProgressBar = oDataSource.ProgressBar
	End Property

	Public Property Get DataSource()
		Set DataSource = oDataSource
	End Property
	
	Public Property Get Fields() 'Recupera todos los campos una vez que se hizo DataBind(), si se utiliza antes marca error
		Set Fields = oDataSource.DataFields
	End Property

	Public Property Get Field(sFieldName) 'Recupera un campo en espec�fico una vez que se hizo DataBind(), si se utiliza antes marca error. Tambi�n puede acceder al campo por medio del m�todo Fields.Field("FieldName")
		Set Field = oDataSource.Fields.Field(sFieldName)
	End Property

	Public Property Get Relatives()
		Set Relatives = oRelatives
	End Property
	
	Public Property Get Container()
		Set Container = oContainer
	End Property
	Public Property Let Container(input)
		Set oContainer = input
	End Property

	'Properties
	Public Property Get HeaderTemplate()
		HeaderTemplate = sHeaderTemplate
	End Property
	Public Property Let HeaderTemplate(input)
		sHeaderTemplate = input
	End Property

	Public Property Get ShowRowNumbers()
		ShowRowNumbers = bShowRowNumbers
	End Property
 	Public Property Let ShowRowNumbers(input)
		bShowRowNumbers = input
	End Property

	Public Property Get SupportsPaging()
		SupportsPaging = bSupportsPaging
	End Property
	Public Property Let SupportsPaging(input)
		bSupportsPaging = input
	End Property

	Public Property Get Mode()
		Mode = oTable.Mode
	End Property
	Public Property Let Mode(input)
		oTable.Mode = input
	End Property

	Public Property Get Id()
		Id = oTable.Id
	End Property
	Public Property Let Id(input)
		oTable.Id = input
	End Property

	Public Property Get CssClass()
		CssClass = oTable.CssClass
	End Property
	Public Property Let CssClass(input)
		oTable.CssClass = input
	End Property

	Public Property Get Render()
		Render = oTable.Render
	End Property
	Public Property Let Render(input)
		oTable.Render = input
	End Property

	Public Property Get DBTableName()
		DBTableName = sDBTableName
	End Property
	Public Property Let DBTableName(input)
		sDBTableName = input
		oTable.Properties.SetProperty "db_table_name", Me.DBTableName
	End Property

	Public Property Get FieldNull()
		FieldNull = sFieldNull
	End Property
	Public Property Let FieldNull(input)
		sFieldNull = input
	End Property

	Public Property Get FieldBlank()
		FieldBlank = oTable.FieldBlank
	End Property
	Public Property Let FieldBlank(input)
		oTable.FieldBlank = input
	End Property

	Public Property Get IsMainTable()
		IsMainTable = oTable.IsMainTable
	End Property
	Public Property Let IsMainTable(input)
		oTable.IsMainTable = input
	End Property

	'Methods
	Public Sub AddRow(ByRef oRow)
		oTable.AddRow(oRow)
	End Sub

	Public Sub DataBind()
		IF Me.ProgressBar.ProgressBarControlId<>"" THEN 
			Me.ProgressBar.ClassName="Fetching"
			Me.ProgressBar.ClearProgress()
		END IF
		IF oDataSource.SQLQueryString="" AND Me.DBTableName<>"" THEN 
			DIM rsMetadata, rsInlineChildrenDataSource, oInlineChildrenDatasource, oListChildrenDatasource, rsListChildrenDataSource
			
			oDataSource.AutoCreate Me.DBTableName, "Grid", Me.Mode
			
			DIM oFields: Set oFields=oDataSource.Fields
			
			IF NOT bShowRowNumbers THEN 
				oFields("SQLRowNumber").Render=FALSE
			ELSE
				oFields("SQLRowNumber").HeaderText="#"
			END IF
			bDataBound=TRUE
		END IF
	End Sub
	
	Public Sub BuildTable()
		DIM iFieldIndex, oField, oRecord, oRow, oCell
		DIM oFields: Set oFields=oDataSource.Fields
		DIM rsDataSource: Set rsDataSource=oFields.DataSource
		
		'Table Headers
		Set oRow = new DataRow
		oRow.DataFields = oFields
		oRow.IsHeader=TRUE
		FOR EACH oField IN oFields.GetItems()
			IF NOT oField IS NOTHING THEN
				IF oField.Render THEN
					oField.FieldNull=Me.FieldNull
'					IF oDataSource.FilterMembers.Exists(oField.FieldName) THEN oField.Visible=FALSE
					Set oCell = new Cell
					oCell.AddContent(oField.HeaderText)
					oCell.Visible=oField.Visible
					oRow.AddCell(oCell)
				END IF
			END IF
		NEXT
		oTable.AddRow(oRow)
		
'		response.write EVAL("""Usuario: ""& oFields(""IdUsuario"").Value")
'		response.end
		'Data Rows
		DIM sNewHeader, sPastHeader
		DO UNTIL rsDataSource.EOF
			Set oRow = new DataRow
			oRow.DataFields = oFields
			'Row Headers
			IF sHeaderTemplate<>"" THEN
				sNewHeader=EvaluateRowTemplate(sHeaderTemplate, oFields.All)
				IF sNewHeader<>sPastHeader THEN 
					Dim oNewHeaderRow: Set oNewHeaderRow=new Row
					oNewHeaderRow.CssClass="HeaderRow"
					Dim oHead: Set oHead=new Cell
					oHead.CellType="TH"
					oHead.AddContent(sNewHeader)
					oHead.ColSpan=oFields.FieldCount()
					oNewHeaderRow.AddCell(oHead)
					oTable.AddRow(oNewHeaderRow)
					sPastHeader=sNewHeader
				END IF
			END IF 
			FOR EACH oField IN oFields.GetItems()
				IF NOT oField IS NOTHING THEN
					IF oField.Render THEN
						IF oField.AutoRowSpan AND TRIM(oField.PrevValue)=TRIM(oField.Value) AND UCASE(TypeName(oField.Container))="CELL" THEN
							oField.Container.RowSpan=oField.Container.RowSpan+1
						ELSE
							Set oCell = new Cell
'							oCell.AddContent(oField.PrevValue &" ("&TypeName(oField.Container)&")  / ")
							oCell.AddContent(oField)
'							oCell.AddContent(" ("&oField.DataType&")")
							oCell.Visible=oField.Visible
							oRow.AddCell(oCell)
						END IF
					END IF
				END IF
			NEXT
			IF oFields.ExistsItem("SQLRowNumber") THEN IF oFields("SQLRowNumber").Render THEN oFields("SQLRowNumber").Container.IsHeader=TRUE
			oTable.AddRow(oRow)
'				response.write oRow.Relatives.GetTopParent().TagName&", "
			rsDataSource.MoveNext
		LOOP
		IF Me.IsMainTable AND NOT ISNULL(oDataSource.Metadata.InlineInsert) THEN
			Me.InlineInsert=oDataSource.Metadata.InlineInsert
		END IF
	End Sub
	
	Public Function GetCode()
		IF sTableCode="" THEN
			IF NOT bDataBound THEN Me.DataBind()
			Me.BuildTable()
			sTableCode=oTable.GetCode()
		END IF
		GetCode=sTableCode
	End Function
	
	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub
	
	Public Sub ShowStatistics()
		response.write "<center>"
		response.write "********* Statistics for "&Me.Id&" ************* " &"<br>"
'		response.write "Open Connection: "&ROUND(oDataSource.TimeOpenConn, 3) &"<br>"
'		response.write "Querying: "&ROUND(oDataSource.TimeQuery, 3) &"<br>"
'		response.write "Fetching: "&ROUND(tElapsedFetch, 3) &"<br>"
'		response.write "Formating: "& ROUND(tStartDisplay-tEndFetch, 3) &"<br>"
'		response.write "Displaying: "&ROUND(oDisplayTime.TotalTime, 3) &"<br>"
'		response.write "Total: "&ROUND(oDisplayTime.TotalTime, 3)&"<br>"
		response.write "**************************************** " &"<br><br>"
		response.write "</center>"
	End Sub
End Class
%>