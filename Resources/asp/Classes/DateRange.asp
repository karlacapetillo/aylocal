<% 
Class DateRange
	'Properties
'	Private iSteps, iValue
	
	'Objects 
	Private oDateBox, oDateBox2
	
	'Private variables
	Private sDateBoxSeparator
	
	Public Property Get Object()
		Set Object = oDateBox.Object
	End Property

	Private Sub Class_Initialize()
		sDateBoxSeparator=" al "
		Set oDateBox = new DateBox
		oDateBox.Object.Properties.SetProperty "valuePrefix", "'>='"
		Set oDateBox2 = new DateBox
		oDateBox2.Object.Properties.SetProperty "valuePrefix", "'<dateadd(day,1,'"
		oDateBox2.Object.Properties.SetProperty "valueSufix", "')'"

	End Sub

	'Output objects
	Public Property Get Style()
		Set Style = oDateBox.Style
	End Property
	
	Public Property Get Methods()
		Set Methods = oDateBox.Methods
	End Property

	'Output Interfaces 
	Public Property Get Name()
		Name = oDateBox.Name
	End Property

	Public Property Get Id()
		Id = oDateBox.Id
	End Property
	
	Public Property Get CssClass()
		CssClass = oDateBox.CssClass
	End Property
	
	'Input Interfaces
	Public Property Let Name(input)
		oDateBox.Name = input
		oDateBox2.Name = input
	End Property
	
	Public Property Let Id(input)
		oDateBox.Id = input
	End Property
	
	Public Property Let CssClass(input)
		oDateBox.CssClass = input
	End Property
	
	'Output Properties 
	Public Property Get Value()
		Value = oDateBox.Value
	End Property
	Public Property Let Value(input)
		oDateBox.Value = input
		oDateBox2.Value = input
	End Property

	Public Property Let MinValue(input)
		oDateBox.Value=input
	End Property
	Public Property Get MinValue()
		MinValue = dMinValue
	End Property

	Public Property Let MaxValue(input)
'		response.write "Me.MaxValue: "&Me.MaxValue&"<br>"
		oDateBox2.Value=input
	End Property
	Public Property Get MaxValue()
		MaxValue = dMaxValue
	End Property

	'Public methods
	Public Function GetCode()
		GetCode=oDateBox.GetCode() & sDateBoxSeparator & oDateBox2.GetCode()
	End Function

	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub
End Class 
 %>