<% 
'Original Source from http://www.daniweb.com/forums/printthread19997.html
Class CascadedDropDown
	'Properties
	Private sName
	
	'Objects 
	Private oRelatives, oDropDownCollection, cDropDownCollection, oValue
	
	'Private variables
	Private returnString, bNameSet
	
	Public Property Get Object()
		Set Object = Me
	End Property

	Public Property Get Visible()
		Visible = cDropDownCollection.Visible
	End Property
	Public Property Let Visible(input)
		cDropDownCollection.Visible = input
	End Property

	Private Sub Class_Initialize()
'		Set oRelatives = [$Relatives](Me)
		Set oDropDownCollection = new Div
		Set cDropDownCollection=oDropDownCollection.Content
		cDropDownCollection.Separator="<br>"
		cDropDownCollection.Visible = TRUE
	End Sub
	Private Sub Class_Terminate()
		Set oRelatives = nothing
		Set cDropDownCollection = nothing
	End Sub
	
'	Public Property Get Control()
'		Set Control = oDropDownList
'		cDropDownCollection
'	End Property
	
	'Interfaces
	Public Property Get Style()
		Set Style = cDropDownCollection.Style
	End Property
	
	Public Property Get Collection()
		Set Collection = cDropDownCollection
	End Property
	
	Public Default Property Get NamedItem(ByVal sItemName)
		Set NamedItem = cDropDownCollection(sItemName)
	End Property

	Public Property Get Text()
		Text = cDropDownCollection.LastItem.Text
	End Property
	
	Public Sub CreateValueObject()
		IF oValue IS NOTHING THEN
			Set oValue = new VariantValue
			'oContent.AddItem oValue
		END IF
	End Sub
	
	Public Property Get Value()
		Value = cDropDownCollection.LastItem.Value
	End Property
	Public Property Let Value(input)
		IF IsNullOrEmpty(input) THEN Exit Property
		IF IsObject(Input) THEN
			Set oValue = input
		ELSE 
			CreateValueObject()
			oValue.Value = input
		END IF

		DIM i, oDropDownList
		oValue.ArrayValueSeparator="@:@"
		i=cDropDownCollection.Count
		FOR EACH oDropDownList IN cDropDownCollection.GetItems()
			i=i-1
			oDropDownList.Properties.ExtendedProperties.Add "CDDIndex", i
			oDropDownList.Value=oValue
		NEXT
	End Property
	
'	Public Property Let Value(input)
'		IF IsNullOrEmpty(input) THEN Exit Property
'		DIM i, aDropDownLists, oDropDownList
'		DIM sParentTables: sParentTables=input
'		DIM iItemCount: iItemCount=cDropDownCollection.ItemsCount()
'		
'		IF NOT INSTR(input, "@/@")<>0 THEN 
'			Debugger Me, "Error en el dato recibido, el dato que se recibi� fue "&input
'			response.end
'			input=input&"@/@"&input
'			sParentTables=sParentTables&"@/@"&sParentTables
''			Err.Raise 13 'type mismatch
'		END IF
'		DIM aParentTables: aParentTables=SPLIT(sParentTables, "@/@")
'		
'		i=0
'		FOR EACH oDropDownList IN cDropDownCollection.GetItems()
'			'oDropDownList.Properties.ExtendedProperties.Add "ArrayIndex", i
'			oDropDownList.Value=aParentTables(iItemCount-1-i)
'			i=i+1
'		NEXT
'	End Property

	Public Property Let InfoString(input)
		DIM i, oDropDownList
		DIM sParentTables: sParentTables=input
		DIM aParentTables: aParentTables=SPLIT(sParentTables, "@/@")
		
		FOR i=UBOUND(aParentTables) TO 0 STEP -1
			Set oDropDownList = new AjaxDropDownList
			oDropDownList.InfoString=aParentTables(i)
			IF NOT IsEmpty(cDropDownCollection.LastItem) THEN 
				IF NOT cDropDownCollection.LastItem IS nothing THEN 
					oDropDownList.ParentControlId = cDropDownCollection.LastItem.Name
	'				oDropDownList.Filters = "oParent=getParent('TD', this).all('"&oDropDownList.Relatives.PrevSibling.Name&"'); ' AND "&oDropDownList.Relatives.PrevSibling.Name&"='+(getVal(oParent) || 'NULL');"
					oDropDownList.Filters = "' AND "&cDropDownCollection.LastItem.Name&"='+('\''+["&cDropDownCollection.LastItem.Name&"]+'\'' || 'NULL');"
				END IF
			END IF
			cDropDownCollection.AddNamedItem oDropDownList, oDropDownList.Name
			IF i<>0 THEN
				oDropDownList.Properties.SetProperty "isSubmitable", "false"
			END IF
		NEXT
	End Property

	'Interfaces
'	Public Property Let CssClass(input)
'		cDropDownCollection.CssClass=input
'	End Property

	'Input Properties
	Public Property Get Name()
		Name = sName
	End Property
	Public Property Let Name(input)
		sName = input
	End Property

	Public Sub SetName()
		IF cDropDownCollection.ItemsCount>0 THEN
			cDropDownCollection.LastItem.Name=sName
			bNameSet = TRUE
		END IF
	End Sub
	
	Public Property Get Separator()
		Separator = sSeparator
	End Property
	Public Property Let Separator(input)
		sSeparator = input
	End Property

	Public Property Get IsReadonly()
		IsReadonly = cDropDownCollection.IsReadonly
	End Property
	Public Property Let IsReadonly(input)
		cDropDownCollection.IsReadonly = input
	End Property

	Public Property Get CssClass()
		CssClass = cDropDownCollection.CssClass
	End Property
	Public Property Let CssClass(input)
		cDropDownCollection.CssClass = input
	End Property

	'Public methods
	Public Function GetCode()
		IF (NOT(bNameSet)) THEN SetName()
		GetCode=oDropDownCollection.GetCode()
	End Function
	
	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub
	
End Class 
 %>