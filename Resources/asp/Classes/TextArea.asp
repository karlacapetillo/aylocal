<% 
Class TextArea
	'Member Variables
	Private sName, sId, sCssClass, iSize, iMaxLength, iCols, iRows

	'Objects
	Private oTextArea, oLabel, oValue
	
	Public Property Get Render()
		Render = oTextArea.Render
	End Property
	Public Property Let Render(input)
		oTextArea.Render = input
	End Property

	Public Property Get Visible()
		Visible = oTextArea.Visible
	End Property
	Public Property Let Visible(input)
		oTextArea.Visible = input
	End Property

	Public Property Get Object()
		IF Me.IsReadonly THEN
			Set Object = oLabel
		ELSE
			Set Object = oTextArea
		END IF
	End Property

	Private Sub Class_Terminate()
		Set oValue = nothing
		Set oLabel = nothing
		'Set oRelatives = nothing
	End Sub
	Private Sub Class_Initialize()
		Set oTextArea = new Tag
		oTextArea.TagName="textarea"
		oTextArea.HasClosingTag=TRUE
		oTextArea.CssClass="texto"
		Set oValue = new VariantValue
		Set oLabel = new Label
		Me.Cols=25
		Me.Rows=4
	End Sub
	
	'Interfaces
	Public Property Get Methods()
		Set Methods = oTextArea.Methods
	End Property
	
	'Output objects
	Public Property Get Style()
		Set Style = oTextArea.Style
	End Property
	
	Public Property Get Properties()
		Set Properties = oTextArea.Properties
	End Property

	'Output Properties 
	Public Property Get Cols()
		Cols = iCols
	End Property
	Public Property Let Cols(input)
		iCols = input
		oTextArea.Properties.SetProperty "cols", iCols
	End Property

	Public Property Get Rows()
		Rows = iRows
	End Property
	Public Property Let Rows(input)
		iRows = input
		oTextArea.Properties.SetProperty "rows", iRows
	End Property

	Public Property Get OtherProperties()
		OtherProperties = oTextArea.OtherProperties
	End Property

	Public Property Get Name()
		Name = sName
	End Property
	Public Property Let Name(input)
		sName = input
		oLabel.Id = sName
		oTextArea.Properties.SetProperty "name", sName
	End Property
	
	Public Property Get Id()
		Id = sId
	End Property
	Public Property Let Id(input)
		sId = input
		oTextArea.Id=sId
	End Property
	
	Public Property Get CssClass()
		CssClass = sCssClass
	End Property
	Public Property Let CssClass(input)
		sCssClass = input
		oTextArea.CssClass=sCssClass
	End Property
	
	Public Property Get Value()
		Set Value = oValue
	End Property
	Public Property Let Value(input)
		IF IsObject(Input) THEN
			Set oValue = input
		ELSE 
			oValue.Value = input
		END IF
		oLabel.Text = oValue
		oTextArea.AddContent(oValue)
	End Property

	Public Property Get Text()
		Text = oValue.Text
	End Property
	Public Property Let Text(input)
		IF IsObject(Input) THEN
			Set oValue = input
		ELSE 
			oValue.Text = input
		END IF
	End Property

	Public Property Get Size()
		Size = iSize
	End Property
	Public Property Let Size(input)
		iSize = input
		IF UCASE(TypeName(oTextArea))="TAG" THEN oTextArea.Properties.SetProperty "size", iSize
	End Property
	
	Public Property Get MaxLength()
		MaxLength = iMaxLength
	End Property
	Public Property Let MaxLength(input)
		iMaxLength = input
		IF UCASE(TypeName(oTextArea))="TAG" THEN oTextArea.Properties.SetProperty "MaxLength", iMaxLength
	End Property
	
	'Input Properties 
	Public Property Get IsReadonly()
		IsReadonly = oTextArea.IsReadonly
	End Property
	Public Property Let IsReadonly(input)
		oTextArea.IsReadonly = CBOOL(input)
		IF input THEN 
			oTextArea.Properties.SetProperty "isSubmitable", "false"
			oTextArea.Properties.SetProperty "tabIndex", "-1"
			oTextArea.Properties.SetProperty "readonly", null
		ELSE
			oTextArea.Properties.RemoveProperty("isSubmitable")
			oTextArea.Properties.RemoveProperty("tabIndex")
			oTextArea.Properties.RemoveProperty("readonly")
		END IF
	End Property
	
	'Public methods
	Public Function GetCode()
		IF NOT Me.Render THEN EXIT Function END IF
		'Debugger Me, oValue.Format&" - "&oValue.Text
		IF Me.IsReadonly THEN
'			DIM oDiv: Set oDiv = new Div
'			oDiv.Scroll=TRUE
''			oDiv.Width=200
''			oDiv.Height=50
			GetCode=oLabel.GetCode()
		ELSE
			IF NOT(IsNullOrEmpty(oValue.Format)) THEN oTextArea.Properties.SetProperty "formato", dDefaultCssClass(UCASE(oValue.Format))
			IF TypeName(oValue)="NumericValue" THEN IF NOT(IsNullOrEmpty(oValue.DecimalPositions)) THEN oTextArea.Properties.SetProperty "decimalPositions", oValue.DecimalPositions
			GetCode=oTextArea.GetCode()
		END IF
	End Function
	
	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub
End Class 
 %>