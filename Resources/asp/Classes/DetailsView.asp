<%
Class DetailsView
	'Properties
	Private sDBTableName
	
	'Settings
	Private bSupportsPaging, bSupportsInsert, bSupportsUpdate, bSupportsDelete

	'Private 
	
	'Objects
	Private oDetailsView, oTable, oDataSource, oContainer, oRelatives, oProgress, oDisplayTime
	
	'Private Properties
	Private sTableCode
	
	Public Property Get Visible()
		Visible = oTable.Visible
	End Property
	Public Property Let Visible(input)
		oTable.Visible = input
	End Property

	Private Sub Class_Initialize()
		Set oDisplayTime = new StopWatch
		Set oRelatives = [$Relatives](Me)
		Set oDetailsView = new Div
		oDetailsView.CSSClass="DetailsView"
		Set oTable = new Table
		oDetailsView.AddContent(oTable)
		Set oDataSource = [$DataSource]
		oDataSource.Table=Me
		oDataSource.PageSize=1
		oDataSource.ShowRowNumber=FALSE
	End Sub
    
	Private Sub Class_Terminate()
		Set oDisplayTime = nothing
		Set oTable = nothing
		Set oContainer = nothing
	End Sub
	
	'Interfaces
	Public Property Get Style()
		Set Style = oTable.Style
	End Property

	Public Property Get Properties()
		Set Properties = oTable.Properties
	End Property

	Public Property Get ProgressBar()
		Set ProgressBar = oTable.ProgressBar
	End Property

	Public Property Get DataSource()
		Set DataSource = oDataSource
	End Property
	
	Public Property Get Fields() 'Recupera todos los campos una vez que se hizo DataBind(), si se utiliza antes marca error
		Set Fields = oDataSource.Fields
	End Property

	Public Property Get Field(sFieldName) 'Recupera un campo en espec�fico una vez que se hizo DataBind(), si se utiliza antes marca error. Tambi�n puede acceder al campo por medio del m�todo Fields.Field("FieldName")
		Set Field = oDataSource.Fields.Field(sFieldName)
	End Property

	Public Property Get Relatives()
		Set Relatives = oRelatives
	End Property
	
	Public Property Get Container()
		Set Container = oContainer
	End Property
	Public Property Let Container(input)
		Set oContainer = input
	End Property

	'Properties
	Public Property Get TableIdentifier()
		TableIdentifier = oTable.TableIdentifier
	End Property
	Public Property Let TableIdentifier(input)
		oTable.TableIdentifier = input
	End Property

	Public Property Get ShowRowNumber()
		ShowRowNumber = oDataSource.ShowRowNumber
	End Property
	Public Property Let ShowRowNumber(input)
		oDataSource.ShowRowNumber = input
	End Property

	Public Property Get PageSize()
		PageSize = oDataSource.PageSize
	End Property

	Public Property Get ProgressBarControlId()
		ProgressBarControlId = oTable.ProgressBarControlId
	End Property
	Public Property Let ProgressBarControlId(input)
		oTable.ProgressBarControlId = input
	End Property

	Public Property Get SupportsPaging()
		SupportsPaging = bSupportsPaging
	End Property
	Public Property Let SupportsPaging(input)
		bSupportsPaging = input
	End Property

	Public Property Get SupportsInsert()
		SupportsInsert = bSupportsInsert
	End Property
	Public Property Let SupportsInsert(input)
		bSupportsInsert = input
	End Property

	Public Property Get SupportsUpdate()
		SupportsUpdate = bSupportsUpdate
	End Property
	Public Property Let SupportsUpdate(input)
		bSupportsUpdate = input
	End Property

	Public Property Get SupportsDelete()
		SupportsDelete = bSupportsDelete
	End Property
	Public Property Let SupportsDelete(input)
		bSupportsDelete = input
	End Property

	Public Property Get DataBound()
		DataBound = oDataSource.DataBound
	End Property

	Public Property Get Mode()
		Mode = oDataSource.Metadata.Mode
	End Property
	Public Property Let Mode(input)
		oDataSource.Metadata.Mode = input
	End Property

	Public Property Get IsReadonly()
		IsReadonly = (UCASE(Me.Mode)="READONLY")
	End Property

	Public Property Get Id()
		Id = oTable.Id
	End Property
	Public Property Let Id(input)
		oTable.Id = input
	End Property

	Public Property Get CssClass()
		CssClass = oTable.CssClass
	End Property
	Public Property Let CssClass(input)
		oTable.CssClass = input
	End Property

	Public Property Get Render()
		Render = oTable.Render
	End Property
	Public Property Let Render(input)
		oTable.Render = input
	End Property

	Public Property Get DBTableName()
		DBTableName = sDBTableName
	End Property
	Public Property Let DBTableName(input)
		sDBTableName = input
		oTable.Properties.SetProperty "db_table_name", Me.DBTableName
	End Property

	Public Property Get FieldNull()
		FieldNull = oDataSource.FieldNull
	End Property
	Public Property Let FieldNull(input)
		oDataSource.FieldNull = input
	End Property

	Public Property Get FieldBlank()
		FieldBlank = oTable.FieldBlank
	End Property
	Public Property Let FieldBlank(input)
		oTable.FieldBlank = input
	End Property

	Public Property Get IsMainTable()
		IsMainTable = oTable.IsMainTable
	End Property
	Public Property Let IsMainTable(input)
		oTable.IsMainTable = input
	End Property

	'Methods
	Public Sub AddRow(ByRef oRow)
		oTable.AddRow(oRow)
	End Sub

	Public Sub DataBind()
		IF Me.ProgressBar.ProgressBarControlId<>"" THEN 
			Me.ProgressBar.ClassName="Fetching"
			Me.ProgressBar.ClearProgress()
		END IF
		IF oDataSource.CommandText="" AND Me.DBTableName<>"" THEN
			oDataSource.AutoCreate Me.DBTableName, TypeName(Me), Me.Mode
			DIM oFields: Set oFields=oDataSource.Fields
			IF NOT IsNullOrEmpty(oDataSource.Metadata.SupportsUpdate) THEN Me.SupportsUpdate=oDataSource.Metadata.SupportsUpdate
		END IF
		IF Me.ProgressBar.ProgressBarControlId<>"" THEN 
			Me.ProgressBar.SetComplete()
			Me.ProgressBar.DataSource=oDataSource.RecordSet
		END IF
	End Sub
	
	Public Sub BuildTable()
		IF NOT Me.DataBound THEN Me.DataBind()
		oDisplayTime.StartTimer()

		DIM oField, oRow, oCell
		DIM oFields: Set oFields=oDataSource.Fields
		DIM rsDataSource: Set rsDataSource=oFields.DataSource
		
		'Table Headers
		oFields.IdentityField.HeaderText=""
		oFields.IdentityField.Visible=FALSE
		IF NOT oFields.IdentityField IS NOTHING THEN 
			IF NOT(Me.IsMainTable) THEN
				oFields.IdentityField.Control.Properties.SetProperty "checkParent", "true"
				oFields.IdentityField.Control.Properties.SetProperty "foreignKey", TRIM(request.querystring("ParentIdentifier"))
				oFields.IdentityField.Control.Properties.SetProperty "otherFields", "oIdentifier=(getIdentifier(this) || document.getElementById('identifier')); '"&TRIM(request.querystring("ParentIdentifier"))&"='+getVal(oIdentifier)"
			END IF
		END IF
		FOR EACH oField IN oFields.GetItems()
			IF oField.Render THEN
				Set oRow = new Row
				oRow.CssClass="DataRow"
				Set oCell = new Cell
				oCell.IsHeader=TRUE
				oCell.AddContent(oField.HeaderText)
				oCell.Visible=oField.Visible
				oRow.AddCell(oCell)
				Set oCell = new Cell
				oCell.AddContent(oField)
				oCell.Visible=oField.Visible
				oRow.Visible=oField.Visible
				oRow.AddCell(oCell)
				oTable.AddRow(oRow)
			END IF
		NEXT
		Me.ProgressBar.ShowProgress()
'		IF Me.SupportsUpdate THEN
			response.write "<script language=""JavaScript"">try { top.frames('buttons').btnSave.enabled=true } catch(e) {}</script>"
			DIM oSubmit: Set oSubmit = new Button
			oSubmit.Id="submit_button"
			oSubmit.Value="Guardar Datos"
			Set oRow=New Row
			Set oCell=New Cell
			oCell.AddContent(oSubmit)
			oRow.AddCell(oCell)
			Me.AddRow(oRow)
			'oRow.Visible=FALSE
'		END IF
		IF IsObject(oProgress) THEN oProgress.SetComplete()
		oDisplayTime.StopTimer()
	End Sub
	
	Public Function GetCode()
		IF sTableCode="" THEN
			Me.BuildTable()
			sTableCode=oDetailsView.GetCode()
		END IF
		GetCode=sTableCode
	End Function
	
	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub
	
	Public Sub ShowStatistics()
		response.write "<center>"
		response.write "********* <strong>Statistics for "&Me.DBTableName&" (Id "&Me.Id&")</strong> ************* " &"<br>"
		response.write "Open Connection: "&ROUND(oDataSource.TimeOpenConn, 3) &"<br>"
		response.write "Retrieving metadata: "&ROUND(oDataSource.TimeMetadata, 3) &"<br>"
		response.write "Querying: "&ROUND(oDataSource.TimeQuery, 3) &"<br>"
		response.write "Fetching: "&ROUND(oDataSource.TimeFetching, 3) &"<br>"
'		response.write "Formating: "& ROUND(tStartDisplay-tEndFetch, 3) &"<br>"
		response.write "Displaying: "&ROUND(oDisplayTime.TotalTime, 3) &"<br>"
		response.write "<strong>Total: "&ROUND((oDataSource.TimeOpenConn+oDataSource.TimeMetadata+oDataSource.TimeQuery+oDataSource.TimeFetching+oDisplayTime.TotalTime), 3)&"</strong><br>"
		response.write "**************************************** " &"<br><br>"
		response.write "</center>"
	End Sub
End Class
%>