<% 
Class TableMetadata
	Private sMetadataString, bInlineInsert, dMetadata, bFromCache
	Private oMetadataTime
	Private sTableName, sViewMode, sMode
	
	
	Private Sub Class_Initialize()
		bFromCache=TRUE
		Set oMetadataTime = new StopWatch
	End Sub
	Private Sub Class_Terminate()
		Set oMetadataTime = nothing
	End Sub

	'Interfaces

	'Properties
	Public Property Get TableName()
		TableName = sTableName
	End Property
	Public Property Let TableName(input)
		sTableName = input
	End Property

	Public Property Get ViewMode()
		ViewMode = sViewMode
	End Property
	Public Property Let ViewMode(input)
		sViewMode = input
	End Property

	Public Property Get Mode()
		Mode = sMode
	End Property
	Public Property Let Mode(input)
		sMode = input
	End Property

	Public Property Get Metadata()
		Set Metadata = dMetadata
	End Property

	Public Property Get SupportsInsert()
		SupportsInsert = dMetadata("SupportsInsert")
	End Property

	Public Property Get SupportsUpdate()
		SupportsUpdate = dMetadata("SupportsUpdate")
	End Property

	Public Property Get SupportsDelete()
		SupportsDelete = dMetadata("SupportsDelete")
	End Property

	Public Property Get SupportsDetails()
		SupportsDetails = dMetadata("SupportsDetails")
	End Property

	Public Property Get SupportsInlineInsert()
		SupportsInlineInsert = dMetadata("InlineInsert")
	End Property

	Public Property Get HeaderTemplate()
		HeaderTemplate = CString(dMetadata("HeaderTemplate"))
	End Property

	Public Property Get FooterTemplate()
		FooterTemplate = CString(dMetadata("FooterTemplate"))
	End Property

	Public Property Get RecordsPerPage()
		RecordsPerPage = NullIfEmptyOrNullText(dMetadata("RecordsPerPage"))
	End Property

	Public Property Get TableFilter()
		TableFilter = dMetadata("TableFilter")
	End Property

	Public Property Get AppTableFilter()
		AppTableFilter = dMetadata("AppTableFilter")
	End Property

	Public Property Get AllColumns()
		AllColumns = HTMLDecode(dMetadata("AllColumns"))
	End Property

	Public Property Get TableParameters()
		TableParameters = dMetadata("TableParameters")
	End Property

	Public Property Get ColumnsMetadataString()
		IF dMetadata("MetadataColumns")="" THEN 
			Debugger Me, "No se pudo recuperar información para la tabla"
			[&Stop] Me
		END IF
'		Debugger Me, dMetadata("MetadataColumns")
		ColumnsMetadataString = [&CString](dMetadata("MetadataColumns")).RemoveEntities()
	End Property

	Public Property Get MetadataTime()
		MetadataTime = oMetadataTime.TotalTime
	End Property

	Public Property Get OnSubmit()
		OnSubmit = dMetadata("OnSubmit")
	End Property

	'Methods
	Public Sub GetMetadataFromDB(ByVal sDBTableName, ByVal sTableViewMode, ByVal sRenderMode)
		Me.TableName=sDBTableName
		Me.ViewMode=sTableViewMode
		Me.Mode=sRenderMode

'		ON ERROR  RESUME NEXT
'		DIM oDictionary: Set oDictionary=[&New]("VariantValue", "@Format ='Moneda', @Value=2")
'		Debugger Me, TypeName(oDictionary)
'		response.end
		
		Set dMetadata = server.CreateObject("Scripting.Dictionary")
		DIM i, sFieldName, oCn
		DIM rsMetaDataSource: Set rsMetaDataSource = Server.CreateObject("ADODB.RecordSet")
		Set oCn = server.CreateObject("ADODB.Connection")
		oCn.open application("StrCnn")

		DIM sSQLQuery: sSQLQuery="dbo.__GetTableMetadata '"&sDBTableName&"', '"&sTableViewMode&"', '"&sRenderMode&"', '"&request.querystring("TablePath")&"'"
		'Debugger Me, sSQLQuery&"<br>": [&Stop] Me
		oMetadataTime.StartTimer()
		Set Session(sSQLQuery) = NOTHING 'Deshabilita uso de caching
		IF SESSION("IdUsuario")=1 THEN 
			'Debugger Me, sSQLQuery: [&Stop] Me
'			Debugger Me, TypeName(Session(sSQLQuery))': RESPONSE.END
		END IF
		IF UCASE(TypeName(Session(sSQLQuery)))=UCASE("Dictionary") AND bFromCache THEN
			Set dMetadata = session(sSQLQuery)
			oMetadataTime.StopTimer()
		ELSE 
'			response.write sSQLQuery&"<br>": response.end
			Set rsMetaDataSource=oCn.execute(sSQLQuery)
			IF NOT(rsMetaDataSource.EOF AND rsMetaDataSource.BOF) THEN 
				StoreSessionVariable sSQLQuery, dMetadata
			END IF
			oMetadataTime.StopTimer()
			IF Err.Number<>0 THEN
				RESPONSE.WRITE "<strong class=""warning"">TableMetadata: </strong>Error al recuperar metadata "&sSQLQuery
				RESPONSE.END
				Err.Clear
			END IF
			IF rsMetaDataSource.BOF AND rsMetaDataSource.EOF THEN 
				RESPONSE.WRITE "<b>Cannot automatically create metadata for table "&sDBTableName&".</b><br>"
				IF SESSION("IdUsuario")=1 THEN Debugger Me, sSQLQuery
				RESPONSE.END
				Exit Sub
			END IF
			DIM oColumn
			FOR EACH oColumn IN rsMetaDataSource.fields
				sFieldName=getDisplayName(oColumn.name)
				SELECT CASE oColumn.Type
				CASE 204 'sql_variant
					dMetadata(sFieldName)=CString(oColumn).Replace(vbcr, vbcrlf).Replace(vbcrlf, "").HTMLDecode()
				CASE ELSE
					dMetadata(sFieldName)=oColumn
				END SELECT
				'Debugger Me, "("&TypeName(dMetadata(sFieldName))&" ("&oColumn.Type&"))" : response.write "<strong>"&sFieldName&"</strong>: ": response.write oColumn.Value: response.write " vs ": response.write dMetadata(sFieldName): response.write "<br>"
	'			dMetadata(sFieldName)=getMetadata(sMetadataString, sDataField, sFieldName)
			NEXT
'			RESPONSE.END
		    rsMetaDataSource.close
			Set rsMetaDataSource = nothing
		END IF
		oCn.Close
		set oCn=nothing
		ON ERROR  GOTO 0
	End Sub

'	Public Sub AddChildren(oItem)
'	End sub
	
End Class
 %>