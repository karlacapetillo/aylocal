<% 
Class NumericValue
	'Properties
	Private fValue, sFormat, iDecimalPositions
	
	Private Sub Class_Initialize()
		iDecimalPositions=NULL
	End Sub
	Private Sub Class_Terminate()
	End Sub
	
	Public Default Property Get Text()
		Text = FormatValue(fValue, sFormat, iDecimalPositions)
	End Property
	Public Property Let Text(input)
		fValue = input
	End Property

	Public Property Get Value()
		Value = fValue
	End Property
	Public Property Let Value(input)
		fValue = input
	End Property

	Public Property Get Format()
		Format = sFormat
	End Property
	Public Property Let Format(input)
		sFormat = input
	End Property

	Public Property Get DecimalPositions()
		DecimalPositions = iDecimalPositions
	End Property
	Public Property Let DecimalPositions(input)
		iDecimalPositions = CINT(input)
	End Property
End Class
 %>