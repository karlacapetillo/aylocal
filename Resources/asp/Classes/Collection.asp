<% 
Function [$Collection](ByRef oElements)
	DIM oCollection:	Set oCollection=[&New]("Collection", "")
	IF NOT IsEmpty(oElements) THEN oCollection.AddItem oElements
	Set [$Collection]=oCollection
End Function
Class Collection
	'Properties
	Private sCssClass, sSeparator', sOtherProperties
	
	'Settings
	Private bRender, bIsReadonly, bVisible
	
	'Objects
	Private oObject, oStyle, oContainer, oRelatives, oParentCell, oProperties, oMethods, dNamedItems
	
	'Private Variables
	Private iElementIndex, aElements, aValidDataTypes, sReturnString, sKey, i
	
	Public Property Get Object()
		Set Object = Me
	End Property

	Private Sub Class_Initialize()
'		Set oRelatives = [$Relatives](Me)
		Set aElements = new ArrayList
'		oRelatives.Content = aElements
		Set dNamedItems = nothing
		Me.Render = TRUE
'		Set oStyle = nothing
'		Set oProperties = new PropertiesCollection
		bVisible=FALSE
	End Sub
	Private Sub Class_Terminate()
		Set aElements = nothing
		Set oRelatives = nothing
		Set dNamedItems = nothing
		Set oStyle = nothing
		Set oProperties = nothing
		Set oContainer = nothing
	End Sub
	
	'Output objects
	Public Property Get Relatives()
'		Debugger Me, TypeName(oRelatives.Parent)
		Set Relatives = oRelatives
	End Property

	Public Property Get Style()
		IF IsEmpty(oStyle) THEN Set oStyle = new Styles
		Set Style = oStyle
	End Property

	Public Property Get Properties()
		IF IsEmpty(Properties) THEN Set Properties = new PropertiesCollection
		Set Properties = oProperties
	End Property

	Public Sub InitializeDictionary
		IF dNamedItems IS NOTHING THEN Set dNamedItems = new Dictionary
	End Sub
	
	Public Property Get NamedItems()
		InitializeDictionary()
		Set NamedItems = dNamedItems
	End Property

	Public Default Function Item(ByVal vItem)
		InitializeDictionary()
		IF IsNumeric(vItem) THEN
			Set Item = aElements(vItem)
		ELSE
			Set Item = Me.NamedItem(vItem)
		END IF
	End Function

	Public Function NamedItem(ByVal sItemName)
		InitializeDictionary()
		IF NOT Me.NamedItems.Exists(sItemName) THEN
			Debugger Me, "<strong>"&sItemName&" is not available. Valid names are: </strong><br>"&Me.ValidNames
			response.end
			Exit Function
		ELSE
			Set NamedItem = dNamedItems(sItemName)
		END IF
	End Function

	Public Function LastItem()
		IF NOT dNamedItems IS NOTHING THEN
			Assign LastItem, dNamedItems.Item(dNamedItems.Count-1)
		ELSE
			LastItem = Empty
		END IF
	End Function

	Public Function FirstItem()
		Assign FirstItem, dNamedItems.Item(0)
	End Function

	Public Property Get ValidNames()
		ValidNames=dNamedItems.ValidNames
	End Property

	'Output Properties 
'	Public Property Get ClassType()
'		ClassType = sClassType
'	End Property
	
	Public Property Get ValidDataTypes()
		ValidDataTypes = aValidDataTypes
	End Property

	Public Property Get IsReadonly()
		IsReadonly = bIsReadonly
	End Property
	Public Property Let IsReadonly(input)
		bIsReadonly = input
		Me.ApplyProperty "IsReadonly", input
		Me.ApplyProperty "isSubmitable", "false"
	End Property

	Public Property Get Id()
		Err.Raise 1, "ASP 101", "Property not available"
	End Property
	
	Public Property Get CssClass()
		Err.Raise 1, "ASP 101", "Property not available"
	End Property
	
	Public Property Get Render()
		Render = bRender
	End Property
	
	'Input Properties 
	Public Property Let Render(input)
		bRender = input
		Me.ApplyProperty "Render", input
	End Property
	
	Public Property Get Visible()
		Visible = bVisible
	End Property
	Public Property Let Visible(input)
		bVisible = input
		Me.ApplyProperty "Visible", input
	End Property
	
	Public Property Let ParentCell(input)
		Set oParentCell = input
'		Me.ApplyProperty "ParentCell", input
	End Property

'	Public Property Let ClassType(input)
'		sClassType = input
'		EVAL("Set oObject = new "&input)
'	End Property
	
	Public Property Let Id(input)
		sId = input
		Me.ApplyProperty "id", input
	End Property
	
	Public Property Let CssClass(input)
		sCssClass = input
		Me.ApplyProperty "CssClass", input
	End Property
	
	Public Property Get Separator()
		Separator = sSeparator
	End Property
	Public Property Let Separator(input)
		sSeparator = input
	End Property


	Public Property Get ItemsCount()
		ItemsCount=aElements.Count
	End Property

	Public Property Get Count()
		Count=aElements.Count
	End Property

	'Public methods
	Public Function List()
		Set List=aElements
	End Function
	
	Public Function GetItems()
		GetItems = aElements.ToArray()
	End Function
	
	Public Function ToArray()
		ToArray = aElements.ToArray()
	End Function
	
	Public Function GetItemByPosition(sPosition)
		GetItemByPosition=aElements(sPosition)
	End Function
	
	Public Property Get Content()
		Set Content = aElements
	End Property

	Public Sub SetProperties(ByRef oElement)
		SELECT CASE TypeName(oElement)
		CASE "DataValue", "VariantValue", "ArrayList"
		CASE ELSE
			IF oElement.Visible THEN bVisible=TRUE
			IF Err.Number<>0 THEN
				response.write "<strong>Collection: </strong>"&TypeName(oElement)&" doesn't support visible property<br>"
				response.end
				Err.Clear
			END IF
			'ON ERROR GOTO 0
			
			'ON ERROR RESUME NEXT
			'oElement.Container = Me
			IF Err.Number<>0 THEN
				response.write "<strong>Collection: </strong>"&TypeName(oElement)&" doesn't support container property<br>"
				response.end
			END IF
			ON ERROR  GOTO 0
			TryPropertySet oElement, "IsReadonly", Me.IsReadonly, FALSE 'oElement.IsReadonly = Me.IsReadonly
		END SELECT
	End Sub

	Public Sub AddContent(ByRef oElement)
		Me.AddItem oElement
	End Sub

	Public Sub Add(ByRef oElement)
		Me.AddItem oElement
	End Sub

	Public Sub AddItem(ByRef oElement)
		IF IsObject(oElement) THEN
			IF oElement IS NOTHING THEN Exit Sub
			SetProperties oElement
		END IF
		aElements.Add oElement	'oRelatives.AddChildren oElement
	End Sub
	
	Public Sub AddItemAt(ByRef oElement, ByVal iPosition)
		aElements.AddAt oElement, iPosition
	End Sub
	
	Public Sub AddNamedItem(ByRef oElement, ByVal sItemName)
		InitializeDictionary()
		Me.AddItem(oElement)
		IF dNamedItems.Exists(sItemName) THEN 
			IF NOT dNamedItems(sItemName) IS nothing THEN
				Debugger Me, "WARNING: "&sItemName&" already exists, no duplicate entries are allowed!"
'				Debugger Me, TypeName(Me.Relatives.Parent)
				response.end
			END IF
		END IF
		IF oElement.Visible THEN bVisible=TRUE
		TryPropertySet oElement, "IsReadonly", Me.IsReadonly, FALSE 'oElement.IsReadonly = Me.IsReadonly
		dNamedItems.Add sItemName, oElement
	End Sub
	
'	Public Sub CollectByName(ByRef oElement)
'		sObjectType = oElement.ObjectType
'		IF NOT dObjectTypes.exists(oElement.ObjectType) THEN
'			Set dObjectTypes(UCASE(sObjectType)) = new Collection
'		END IF
'		dObjectTypes(UCASE(sObjectType)).AddItem(oElement)
'	End Sub
	
	Public Sub ClearContent()
		IF Me.ItemsCount>0 THEN
			aElements.Clear()
			Set dNamedItems = new Dictionary
		END IF
	End Sub

	Public Sub ApplyProperty(sPropertyName, ByRef vValue)
		DIM vElement
		FOR EACH vElement IN aElements.ToArray()
			IF isObject(vValue) THEN
'				'ON ERROR RESUME NEXT
				TryPropertySet vElement, sPropertyName, vValue, FALSE
				IF err.Number<>0 THEN
					response.write "No se puede asignar la propiedad "&sPropertyName&" a un objeto de tipo "&TypeName(vElement)
					response.end
				END IF
			ELSEIF NOT(IsNullOrEmpty(vElement)) THEN
				IF vValue = "" THEN vValue = """"""
				TryPropertySet vElement, sPropertyName, vValue, FALSE
			END IF
		NEXT
	End Sub
	
	Public Sub RemoveProperty(ByVal sPropertyName)
		DIM vElement
		FOR EACH vElement IN aElements.ToArray()
			TryPropertyRemove vElement, sPropertyName, FALSE
		NEXT
	End Sub

	Public Sub SetProperty(sPropertyName, ByRef vValue)
		DIM vElement
		FOR EACH vElement IN aElements.ToArray()
			vElement.Properties.SetProperty sPropertyName, vValue
		NEXT
	End Sub
	
	Public Function ExistsItem(ByVal sItemName)
		ExistsItem=Me.NamedItems.Exists(sItemName)
	End Function

	Public Function GetCode()
		IF NOT Me.Render THEN 
			GetCode = ""
			EXIT Function
		END IF
		sReturnString = ""
		DIM iElementIndex

		DIM vElement
		FOR EACH vElement IN aElements.ToArray()
			IF NOT IsObject(vElement) THEN
				sReturnString=sReturnString& vElement & sSeparator
			ELSEIF NOT vElement IS NOTHING THEN
				SELECT CASE TypeName(vElement)
				CASE "DataValue", "VariantValue", "ArrayList"
					sReturnString=sReturnString& vElement & sSeparator
				CASE ELSE
'					Debugger Me, TypeName(vElement)&" --> "& TypeName(vElement.Relatives.Parent.Relatives.Parent)
					sReturnString=sReturnString& vElement.GetCode() & sSeparator
				END SELECT
			END IF
		NEXT
		GetCode = sReturnString
	End Function

	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub
End Class 
 %>