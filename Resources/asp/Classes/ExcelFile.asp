<% 
Class ExcelFile
Private Xcl

Sub StartExcel()
response.write "si entra"
response.end
    Set Xcl = CreateObject("Excel.Application")
    Xcl.Visible = true
    Set newBook = Xcl.Workbooks.Add
'Firstly, we create our MS Excel object Xcl - this will be the link between our code and Excel. The rest of our code will reside within a VBScript subroutine called StartExcel. We then use the Server.CreateObject function to create an a link to Excel and assign that to our object Xcl, then we set the visibility of the object to true, then we add a Workbook to our object. 
    newBook.Worksheets.Add 

'A standard Excel Workbook has 3 worksheets, for each additional one that you require, you will need to call the Add function of the Worksheet once for each worksheet more than 3 you require - in this case we only require 1. 
    newBook.Worksheets(1).Activate
    newBook.Worksheets(1).Columns("A").columnwidth=50
    newBook.Worksheets(1).Columns("A").WrapText = True
    newBook.Worksheets(1).Columns("B").columnwidth=50
    newBook.Worksheets(1).Columns("B").WrapText = True
    newBook.Worksheets(1).Range("A1:B1000").NumberFormat = "0"
    newBook.Worksheets(1).Range("A1:B1000").HorizontalAlignment = -4131

'We will Active the first worksheet in the book so that we it becomes the 'active' worksheet. We set the width of the first two columns to 50 characters and also enable wordwrapping on these columns. We set the format mask for numeric data to a specified set and the set the cell alignment to left for the range of cells from A1 to B1000. The reason we use the actual values - such as -4131 and not the Excel Constant of xlLeft - is that as we are interacting via ActiveX we don't have visibility of these constants, a full list of the real value of the constants is also available. 
    newBook.Worksheets(1).Cells(1,1).Interior.ColorIndex="15"
    newBook.Worksheets(1).Cells(1,1).value="First Column, First Cell"
    newBook.Worksheets(1).Cells(2,1).value="First Column, Second Cell"
    newBook.Worksheets(1).Cells(1,2).value="Second Column, First Cell"
    newBook.Worksheets(1).Cells(2,2).value="Second Column, Second Cell"
    newBook.Worksheets(1).Name="My First WorkSheet"

'Set the background colour of the first cell to the colour represented by the value 15 - actually gray, then we shall place some values on to the worksheet to represent the data you may wish to add. The last thing we do in this section is to name the current worksheet as My First Worksheet. 
    fname=Xcl.GetSaveAsFilename("Testing Excel Extraction.xls")
    if fname = "False" then
        fname="Testing Excel Extraction.xls"
    end if
    newBook.SaveAs fname
    Set Xcl = nothing
'    Location.Href="http://www.greggriffiths.org/index.html"
end Sub

'Finally, we call the Save As dialog passing in the suggested file name of Testing Excel Extraction, if the user selects another filename thats fine too, but if they cancel the dialog then we catch that that has happened and use our preferred name. We then destroy our object by setting its value to nothing, then we redirect the use to another page - in this case http://www.greggriffiths.org/index.html. To get the code to run, we set the function to be run once the HTML page has loaded by using the onLoad handler. Click here to see this code running.
'In some cases we may need to pass Excel function calls such as SUM in this way, this can be done by simply passing the full string as we would type into Excel, or the VBA code to achieve the same results, as shown in the following code : 

'Dim Xcl
'Sub StartExcel()
'    Set Xcl = CreateObject("Excel.Application")
'    Xcl.Visible = true
'    Set newBook = Xcl.Workbooks.Add
'    newBook.Worksheets(1).Activate
'    newBook.Worksheets(1).Range("A1:B1000").NumberFormat = "0"
'    newBook.Worksheets(1).Cells(1,1).value="Value1"
'    newBook.Worksheets(1).Cells(2,1).value="Value2"
'    newBook.Worksheets(1).Cells(1,2).value="20"
'    newBook.Worksheets(1).Cells(2,2).value="10"
'    vnewBook.Worksheets(1).Cells(3,1).value="Total"
'    newBook.Worksheets(1).Cells(3,2).value="=Sum(B1:B2)"
'    newBook.Worksheets(1).Cells(4,1).value="Total Times 2"
'    newBook.Worksheets(1).Cells(4,2).value=CInt(newBook.Worksheets(1).Cells(3,2).value * 2)
'    newBook.Worksheets(1).Name="My First WorkSheet"
'end Sub 
End Class
 %>