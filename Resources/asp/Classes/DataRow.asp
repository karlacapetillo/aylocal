<% 
Class DataRow
	'Member Variables
	Private returnString
    
	'Objects
	Private oRow, oContainer, oCells, iRecordSetIndex, oDataSource, bSupportsEdit, bSupportsDelete', bSupportsInsert, oDataFields
	
	Public Property Get Object()
		Set Object = oRow
	End Property

	Private Sub Class_Initialize()
		Set oRow = new Row
		oRow.CssClass="DataRow"
	End Sub
    
	Private Sub Class_Terminate()
		Set oRow = nothing
	End Sub

	'Interfaces
	Public Property Get Relatives()
		Set Relatives = oRelatives
	End Property
	
	Public Property Get Container()
		Set Container = oRow.Container
	End Property
	Public Property Let Container(input)
		oRow.Container = input
	End Property

	Public Property Get Style()
		Set Style = oRow.Style
	End Property
	
'	Public Property Get SupportsInsert()
'		SupportsInsert = bSupportsInsert
'	End Property
'	Public Property Let SupportsInsert(input)
'		bSupportsInsert = input
'	End Property

	Public Default Function GetItemByPosition(sPosition)
		Set GetItemByPosition=oRow(sPosition)
	End Function
	
	Public Property Get NamedCell(sCellName)
		Set NamedCell = oRow.NamedCell(sCellName)
	End Property

	Public Property Get GroupName()
		GroupName = oRow.GroupName
	End Property
	Public Property Let GroupName(input)
		oRow.GroupName = input
	End Property

	Public Property Get FloatingHeader()
		FloatingHeader = oRow.FloatingHeader
	End Property
	Public Property Let FloatingHeader(input)
		oRow.FloatingHeader = input
	End Property

	Public Property Get IsReadonly()
		IsReadonly = oRow.IsReadonly
	End Property
	Public Property Let IsReadonly(input)
		oRow.IsReadonly = input
	End Property
	
    Public Property Get CellCount()
		CellCount = oRow.CellCount
    End Property
       
    Public Property Let Id(input)
		oRow.Id=input
    End Property
       
    Public Property Get CssClass()
		CssClass=oRow.CssClass
    End Property
    Public Property Let CssClass(input)
		oRow.CssClass=input
    End Property

	Public Property Get Cells()
		Cells=oRow.Cells
	End Property

	Public Property Get IsHeader()
		IsHeader=oRow.IsHeader
	End Property
	Public Property Let IsHeader(input)
		oRow.IsHeader=input
	End Property

	Public Property Get IsFooter()
		IsFooter = oRow.IsFooter
	End Property
	Public Property Let IsFooter(input)
		oRow.IsFooter = input
	End Property

	Public Property Get Render()
		Render=oRow.Render
	End Property
	Public Property Let Render(input)
		oRow.Render=input
	End Property
	
	Public Property Get Visible()
		Visible=oRow.Visible
	End Property
	Public Property Let Visible(input)
		oRow.Visible=input
	End Property
	
	'Properties
	Public Property Get RecordSetIndex()
		RecordSetIndex = iRecordSetIndex
	End Property
	Public Property Let RecordSetIndex(input)
		iRecordSetIndex = input
	End Property

	Public Property Get DataSource()
		Set DataSource = oDataSource
	End Property
	Public Property Let DataSource(input)
		Set oDataSource = input
	End Property

'	Public Property Get DataFields()
'		Set DataFields = oDataFields
'	End Property
'	Public Property Let DataFields(input)
'		[&CheckValidObjectType] input, "DataFieldCollection"
'		Set oDataFields = input
'		IF NOT(oDataSource.BOF AND oDataSource.EOF) THEN rsCurrentRecordSet=oDataSource.Bookmark
'	End Property

	'Public methods
	Public Sub AddCell(ByRef oCell)
		oRow.AddCell oCell
	End Sub

	Public Sub AddNamedCell(ByRef oCell, ByVal sCellName)
		oRow.AddNamedCell oCell, sCellName
	End Sub

	Public Sub AddCellAt(ByRef oCell, ByVal iPosition)
		oRow.AddCellAt oCell, iPosition
	End Sub

	Public Function GetCode() 'Function Generate()
		IF NOT Me.Render THEN 
			EXIT Function
		END IF
		oDataSource.Fields.CommandsField.Command("Insert").Render=(oRow.IsHeader)
'		oDataSource.Fields.CommandsField.Command("Details").Render=NOT(oRow.IsHeader)
'		oDataSource.Fields.CommandsField.Command("Delete").Render=NOT(oRow.IsHeader)
		oDataSource.Index=iRecordSetIndex
'		Debugger Me, oDataSource.Index
		GetCode = oRow.GetCode()
	End Function
	
	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub
End Class 
%>