<% 
Function [$Queue](ByRef aObjects)
	DIM oQueue:	Set oQueue = CreateObject("System.Collections.Queue")
	DIM oElement
	IF NOT IsEmpty(aObjects) THEN
		FOR EACH oElement IN aObjects
			oQueue.Enqueue oElement
		NEXT
	END IF
	Set [$Queue] = oQueue
End Function 
Function [$ArrayList](ByRef aObjects)
'	DIM oQueue:	Set oQueue=[$Queue](aObjects)
	DIM oArrayList:	Set oArrayList = New ArrayList
	IF TypeName(aObjects)<>"Empty" THEN oArrayList.Add aObjects
	Set [$ArrayList]=oArrayList
End Function
Class ArrayList
	Private oElements, sMode, iCurrentIndex, sJoin
	
	Private Sub Class_Initialize()
		Set oElements = CreateObject("System.Collections.ArrayList")
	End Sub
	Private Sub Class_Terminate()
		Set oElements = nothing
	End Sub

	Public Property Get Count()
		Count=oElements.Count
	End Property

	Public Property Get LastItem()
		IF Me.Count>0 THEN
			Assign LastItem, oElements(Me.Count-1)
		ELSE
			Assign LastItem, Empty
		END IF
	End Property

	Public Property Get FirstItem()
		Assign Item, oElements(0)
	End Property

	Public Default Property Get Item(sPosition)
		ON ERROR	RESUME NEXT
		IF ISNUMERIC(sPosition) THEN
			Assign Item, oElements(sPosition)
		ELSE
			DIM oItem
			SELECT CASE UCASE(sPosition)
			CASE UCASE("First")
				Assign Item, oElements(0)
			CASE UCASE("Last")
				IF Me.Length>0 THEN
					Assign Item, oElements(Me.Length-1)
				ELSE
					Assign Item, Empty
				END IF
			CASE ELSE
				Assign Item, nothing
			END SELECT
		END IF
		[&Catch] TRUE, TypeName(Me)&".Item", "Error para "&TypeName(oElement) &""
		ON ERROR	GOTO 0
	End Property

	Public Function ToArray()
		ToArray = oElements.ToArray
	End Function

	Public Function Join(sSeparator)
		IF IsEmpty(sJoin) THEN
			DIM sJoin, oElement
			FOR EACH oElement IN oArray.ToArray()
				IF IsObject(oElement) THEN 
					sJoin=sJoin&"["&TypeName(oElement)&" Object]<br>"
				ELSE
					sJoin=sJoin&oElement&"<br>"
				END IF
			NEXT
			sJoin=sJoin&sSeparator
		END IF
		Join = sJoin
	End Function

	Public Function AddAt(ByRef oElement, ByVal iPosition)
		oElements.Insert iPosition, oElement
	End Function
	
	Public Sub AddItem(ByRef oElement)
		Me.Add oElement
	End Sub
	
	Public Sub Add(ByRef oElement)
'		SELECT CASE TypeName(oElement)
'		CASE "Variant()"
'			DIM aElement
'			FOR EACH aElement IN oElement
'				oElements.Add	aElement
'			NEXT
'		CASE ELSE
			oElements.Add	oElement
'		END SELECT
	End Sub

	Public Sub AppendList(ByRef aAppendList, ByVal iStartIndex)
		IF IsEmpty(aAppendList) THEN Exit Sub
		DIM iOriginalLength:	iOriginalLength=Me.Length
		'response.write "Length: "&Me.Length&" --> "
		Allocate(Me.Length+UBOUND(aAppendList)-iStartIndex)
		'response.write "Length: "&Me.Length&"<br>"
		
		DIM iAppendIndex
		FOR iAppendIndex=iStartIndex TO UBOUND(aAppendList)
'			Debugger Me, aAppendList(iAppendIndex)&vbcrlf&vbcrlf
			Assign oElements(iOriginalLength+iAppendIndex-iStartIndex), aAppendList(iAppendIndex)
			IF IsObject(aAppendList(iAppendIndex)) THEN 
'				IF TypeName(aAppendList(iAppendIndex))="Cell" THEN oElements(iOriginalLength+iAppendIndex-iStartIndex).Relatives.Index=iAppendIndex
'				TryPropertySet oElements(iOriginalLength+iAppendIndex-iStartIndex), "Relatives.Index", iAppendIndex, TRUE
			END IF
		NEXT
		sJoin=Empty
		'response.write "<strong>"&Me.Length&"</strong><br>"
	End Sub
	
	Public Sub Clear()
		oElements.Clear()
	End sub

	Public Sub ForEach(sOperations)
		IF sOperations<>"" THEN
			DIM oElement
			FOR EACH oElement in oElements
				SELECT CASE sOperations
				CASE "[&echo]"
					[&echo] oElement
					[&echo] "<br>"
				CASE ELSE
					WITH oElement
						EXECUTE("."&sOperations)
					END WITH
				END SELECT
			NEXT
		END IF
	End Sub
End Class
 %>