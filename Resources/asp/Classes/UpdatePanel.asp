<% 
Class UpdatePanel
	'Properties
	Private sId, sCssClass, fHeight, fWidth, sHeaderText
	
	'Settings
	Private sTabStripPlacement, bRender, bVisible
	
	'Objects 
	Private oDiv, oContainer
	
	'Private variables
	
	Private Sub Class_Initialize()
		Set oDiv = new Div
		Set oContainer = nothing
		Me.Visible=FALSE
	End Sub

	Private Sub Class_Terminate()
		Set oDiv = nothing
		Set oContainer = nothing
	End Sub

	'Output objects
	Public Property Get Methods()
		Set Methods = oDiv.Methods
	End Property
	
	Public Property Get Style()
		Set Style = oDiv.Style
	End Property
	
	'Output Interfaces
	Public Property Get ParentContainer()
		Set ParentContainer = oContainer
	End Property
	
	'Input Interfaces
	Public Property Let ParentContainer(input)
		Set oContainer = input
	End Property

	'Input Settings
	Public Property Get Render()
		Render = bRender
	End Property

	Public Property Get Visible()
		Visible = bVisible
	End Property

	'Output Settings
	Public Property Let Render(input)
		bRender = input
	End Property

	Public Property Let Visible(input)
		bVisible = input
		oDiv.Visible=input
	End Property

	'Output Properties 
	Public Property Get HeaderText()
		HeaderText = sHeaderText
	End Property

	Public Property Get Width()
		Width = sWidth
	End Property

	Public Property Get Height()
		Height = sHeight
	End Property

	Public Property Get TabStripPlacement()
		TabStripPlacement = sTabStripPlacement
	End Property
	
	Public Property Get Id()
		Id = sId
	End Property
	
	Public Property Get CssClass()
		CssClass = sCssClass
	End Property
	
	'Input Properties 
	Public Property Let HeaderText(input)
		sHeaderText = input
	End Property

	Public Property Let Width(input)
		fWidth = input
		oDiv.Properties.SetProperty "width", input
	End Property

	Public Property Let Height(input)
		fHeight = input
		oDiv.Properties.SetProperty "height", input
	End Property

	Public Property Let TabStripPlacement(input)
		SELECT CASE UCASE(sTabStripPlacement)
		CASE UCASE("Top"), UCASE("Bottom")
			sTabStripPlacement = input
		CASE ELSE
			Err.Raise 1, "ASP 101", input&" is not a TabStripPlacement parameter"
			response.end
		END SELECT
	End Property
	
	Public Property Let Id(input)
		sId = input
		oDiv.Id = sId
	End Property
	
	Public Property Let CssClass(input)
		sCssClass = input
		oDiv.CssClass = sCssClass
	End Property
	
	'Public methods
	Public Sub AddContent(ByRef oElement)
		oDiv.AddContent(oElement)
	End Sub

	Public Function GetCode()
		GetCode=oDiv.GetCode()
	End Function

	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub
End Class 
 %>