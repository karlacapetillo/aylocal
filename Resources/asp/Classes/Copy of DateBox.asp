<% 
Class DateBox
	'Properties
	Private sName, sText, sMode, sCssClass, iSize, iMaxLength
	
	'Settings
	Private bIsReadonly
	
	'Objects
	Private oTextBox, oHolder, oCalendarImage
	
	'Private variables
	Private sReturnString
	
	Public Property Get Object()
		Set Object = oTextBox.Object
	End Property

	Public Property Get Style()
		Set Style = oTextBox.Style
	End Property
	
	Private Sub Class_Initialize()
		iSize=8
		iMaxLength=10
	End Sub
	
	'Output objects
	'Output objects
	Public Property Get InnerHolder()
		Set InnerHolder = oHolder
	End Property

	Public Property Get InnerInput()
		Set InnerInput = oTextBox
	End Property

	'Output properties
	Public Property Get Name()
		Name = sName
	End Property
	Public Property Let Name(input)
		sName = input
		oTextBox.Name=sName
	End Property

	Public Property Get Value()
		Value = Me.Text
	End Property
	Public Property Let Value(input)
		Me.Text = input
	End Property

	Public Property Get Text()
		Text = sText
	End Property
	Public Property Let Text(input)
		sText = input
		IF ISDATE(sText) THEN
			IF CDATE(sText)=CDATE("1/1/1900") THEN
				oTextBox.Text=""
			END IF
		ELSE
			sText=""
		END IF
		oTextBox.Text=sText
	End Property

	Public Property Get CssClass()
		CssClass = sCssClass
	End Property
	Public Property Let CssClass(input)
		sCssClass = input
		oTextBox.CssClass=sCssClass
	End Property

	Public Property Get IsReadonly()
		IsReadonly = bIsReadonly
	End Property
	Public Property Let IsReadonly(input)
		bIsReadonly = input
		Me.Mode="READONLY"
	End Property
	
	Public Property Get Mode()
		Mode = sMode
	End Property
	Public Property Let Mode(input)
		sMode = input
		IF TRIM(UCASE(input))="READONLY" THEN
			Set oTextBox = new Label
			oTextBox.CssClass=sCssClass
			oTextBox.Text=sText
		ELSE
			'Input
			Set oTextBox = new TextBox
			sCssClass="fecha"
			oTextBox.Value=sText
			oTextBox.CssClass=sCssClass
			oTextBox.Size=iSize
			oTextBox.MaxLength=iMaxLength
			oTextBox.Methods.OnBlur="fnc_fechas_onblur(this);"
			oTextBox.Methods.OnFocus="this.fireEvent('onmouseover');"
			oTextBox.Methods.OnPropertyChange="if (event.propertyName!='value' || event.propertyName=='value' && event.srcElement!=this) fnc_fechas_onpropertychange(this, event.propertyName);"
			oTextBox.Properties.SetProperty "turnOn", "true"
			SELECT CASE UCASE(sMode)
				CASE UCASE("insert")
					oTextBox.Value=""
			END SELECT
			'Calendar image
			Set oCalendarImage = new Image
			oCalendarImage.Id="cal_fecha"
			oCalendarImage.ImageURL = sCalendarImagePath
			oCalendarImage.Height=15
			oCalendarImage.Width=16
			oCalendarImage.AlternateText="Mostrar calendario"
			oCalendarImage.Methods.OnClick="displayCalendar(document.all[this.sourceIndex-1],'dd-mm-yyyy',this)" 
			oCalendarImage.Style.Cursor="hand"
		END IF
	End Property

	'Input properties


	Public Sub SetProperty(ByVal sKey, ByVal input)
		oTextBox.Properties.SetProperty sKey, input
	End Sub

	Public Sub RemoveProperty(ByVal sKey)
		oTextBox.Properties.RemoveProperty sKey
	End Sub

	Public Function GetCode()
		sReturnString=""
		sReturnString=sReturnString&oTextBox.GetCode()
		sReturnString=sReturnString&oCalendarImage.GetCode()
		GetCode=sReturnString
	End Function
	
	Public Sub WriteCode()
		response.write Me.GetCode
	End Sub 
End Class
%>
