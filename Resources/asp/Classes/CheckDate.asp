<% 
Class CheckDate
	'Properties
	Private sName, sValue, sMode, sCssClass, iSize, iMaxLength
	
	'Settings
	Private bHasCheckBox, bHasTextBox
	
	'Objects
	Private oCheckBox, oInput, oHolder, oReadOnlyImage, oCalendarImage, oProperties, oContainer, oValue
	
	'Private variables
	Private sReturnString
	
	Public Property Get Visible()
		Visible = oContainer.Visible
	End Property
	Public Property Let Visible(input)
		oContainer.Visible = input
	End Property

	Public Property Get Object()
		Set Object = Me
	End Property

	Private Sub Class_Terminate()
		Set oValue = nothing
		Set oContainer = nothing
		Set oCheckbox = nothing
	End Sub
	Private Sub Class_Initialize()
		Set oValue = nothing
		Set oContainer = new Div
		Set oCheckbox = [$CheckBox]("", "")
		oCheckbox.DefaultValue="GETDATE()"
		oCheckbox.Id="chk_fecha"
		oCheckbox.Properties.SetProperty "highlightAlways", "true"
		oCheckbox.ShowLabel=FALSE
		oCheckbox.Methods.OnFocus="this.fireEvent('onmouseover');"
		oCheckbox.Methods.OnClick="obj_txt=document.all[this.sourceIndex-1]; esVacio(this.value)?this.checked=false:null; /*alert(obj_txt.value+', '+this.value); */obj_txt.value=this.checked?this.value:'';"
		Set oInput = new HiddenField'	new DateBox	'
'		oInput.Methods.OnBlur="fnc_fechas_onblur(this);"
'		oInput.Methods.OnFocus="this.fireEvent('onmouseover');"
		oInput.Methods.OnPropertyChange="if (event.propertyName!='value' || event.propertyName=='value' && event.srcElement!=this) fnc_fechas_onpropertychange(this, event.propertyName);"
		oContainer.AddContent(oInput)
		oContainer.AddContent(oCheckbox)
	End Sub
	
	'Output objects
	Public Property Get Style()
		Set Style = oInput.Style
	End Property
	
	Public Property Get Properties()
		Set Properties = oInput.Properties
	End Property

	Public Property Get InnerInput()
		Set InnerInput = oInput
	End Property

	Public Property Get CheckBox()
		Set CheckBox = oCheckBox
	End Property
	
	'Properties
	Public Property Get Relatives()
		Set Relatives = oRelatives
	End Property
	
	Public Property Get Container()
		Set Container = oRelatives.Parent
	End Property
	Public Property Let Container(input)
		oRelatives.Parent = input
	End Property

	Public Property Get Name()
		Name = sName
	End Property
	Public Property Let Name(input)
		sName = input
		oInput.Name=sName
	End Property


	Public Sub CreateValueObject()
		IF oValue IS NOTHING THEN
			Set oValue = new VariantValue
			'oContent.AddItem oValue
		END IF
	End Sub
	
	Public Property Get Value()
		Set Value = oValue
	End Property
	Public Property Let Value(input)
		IF IsObject(Input) THEN
			Set oValue = input
		ELSE 
			CreateValueObject()
			oValue.Value = input
		END IF
		IF IsNullOrEmpty(oValue.Format) THEN oValue.Format="Date"
		oCheckBox.Value = [$StringValue]
		oInput.Value = oValue
	End Property

	Public Property Get Text()
		Text = oValue.Text
	End Property
	Public Property Let Text(input)
		Me.Value = input
	End Property

	Public Property Get CssClass()
		CssClass = oContainer.CssClass
	End Property
	Public Property Let CssClass(input)
		oContainer.CssClass = input
	End Property


	Public Property Get IsReadonly()
		IsReadonly = oCheckbox.IsReadonly
	End Property
	Public Property Let IsReadonly(input)
		oCheckbox.IsReadonly=input
	End Property


	Public Function GetCode()
		sReturnString=""
		oCheckBox.Value=oInput.Value
'		IF oInput.Value<>"" THEN
'			oCheckBox.Checked=TRUE
'		ELSE
'			oCheckBox.Checked=FALSE
'		END IF
		sReturnString=sReturnString&oContainer.GetCode()
		GetCode=sReturnString
	End Function
	
	Public Sub WriteCode()
		response.write Me.GetCode
	End Sub 
End Class
%>
