<% 
Function [$RadioButton](ByVal sParameters, ByVal sDefaultValue)
	Set [$RadioButton]=[&New]("RadioButton", sParameters)
	[$RadioButton].DefaultValue=sDefaultValue
End Function
Class RadioButton
	'Properties
	Private sName, sValue, sText, bChecked, bIsReadonly, sMaxLimit, sMinLimit, vDefaultValue
	
	'Settings
	Private bShowLabel, bCheckIfHasValue
	
	'Objects
	Private oRadioButton, oLabel, oRadioButtonImage, oContent, oRelatives, oValue
	
	Public Property Get Object()
		Set Object = oContent
	End Property

	Private Sub Class_Terminate()
		Set oRelatives = nothing
		Set oContent = nothing
		Set oValue = nothing
		Set oRadioButtonImage = nothing
		Set oRadioButton = nothing
		Set oLabel = nothing
	End Sub

	Private Sub Class_Initialize()
'		Set oRelatives = [$Relatives](Me)
		Set oContent = new Collection
		oContent.Separator="&nbsp;"
		Set oValue = nothing
		Set oRadioButtonImage = new Image
		oRadioButtonImage.ImageURL = sCalendarReadonlyImagePath
		oRadioButtonImage.Height=12
		oRadioButtonImage.Width=12
		oRadioButtonImage.HasBorder=TRUE
		oContent.AddNamedItem oRadioButtonImage, "Image"
		Set oRadioButton = new Tag
		oRadioButton.TagName="Input"
		oRadioButton.Properties.SetProperty "type", "radio"
		oRadioButton.HasClosingTag=FALSE
		oContent.AddNamedItem oRadioButton, "RadioButton"
		Set oLabel = nothing
		Me.IsReadonly=FALSE
		bCheckIfHasValue=TRUE
	End Sub
	
	'Interfaces
	Public Property Get Methods()
		Set Methods = oRadioButton.Methods
	End Property
	
	'Output objects
	Public Property Get Style()
		Set Style = oRadioButton.Style
	End Property
	
	Public Property Get Properties()
		Set Properties = oRadioButton.Properties
	End Property

	'Properties 
	Public Property Get Relatives()
		Set Relatives = oRelatives
	End Property
	
	Public Property Get Container()
		Set Container = oRelatives.Parent
	End Property
	Public Property Let Container(input)
		oRelatives.Parent = input
	End Property

	Public Property Get CheckIfHasValue()
		CheckIfHasValue = bCheckIfHasValue
	End Property
	Public Property Let CheckIfHasValue(input)
		bCheckIfHasValue = input
	End Property

	Public Property Get Render()
		Render = oRadioButton.Render
	End Property
	Public Property Let Render(input)
		oRadioButton.Render = input
	End Property

	Public Property Get Visible()
		Visible = oRadioButton.Visible
	End Property
	Public Property Let Visible(input)
		oRadioButton.Visible = input
	End Property

	Public Property Get Name()
		Name = sName
	End Property
	Public Property Let Name(input)
		sName = input
		oRadioButton.Properties.SetProperty "name", sName
	End Property
	
	Public Property Get Id()
		Id = oRadioButton.Id
	End Property
	Public Property Let Id(input)
		oRadioButton.Id=input
	End Property
	
	Public Property Get CssClass()
		CssClass = oRadioButton.CssClass
	End Property
	Public Property Let CssClass(input)
		oRadioButton.CssClass = input
	End Property
	
	Public Sub CreateValueObject()
		IF oValue IS NOTHING THEN
			Set oValue = new VariantValue
			'oContent.AddItem oValue
		END IF
	End Sub
	
	Public Property Get DefaultValue()
		DefaultValue = vDefaultValue
	End Property
	Public Property Let DefaultValue(input)
		vDefaultValue = input
	End Property

	Public Property Get Value()
		IF oValue IS NOTHING THEN
			Value = Empty
		ELSE
			Value = oValue.Value
		END IF
	End Property
	Public Property Let Value(input)
		IF IsObject(Input) THEN
			Set oValue = input
			'oContent.AddItem oValue
		ELSE 
			CreateValueObject()
			oValue.Value = input
		END IF
	End Property

	Public Property Get Text()
		Text = oValue.Text
	End Property
	Public Property Let Text(input)
		Me.ShowLabel=TRUE
		oValue.Text = input
	End Property

	Public Property Get Checked()
		Checked = bChecked
	End Property
	Public Property Let Checked(input)
		bChecked = CBOOL(input)
	End Property

	Public Property Get ShowLabel()
		ShowLabel = bShowLabel
	End Property
	Public Property Let ShowLabel(input)
		bShowLabel = CBOOL(input)
	End Property

	Public Property Get IsReadonly()
		IsReadonly = bIsReadonly
	End Property
	Public Property Let IsReadonly(input)
		bIsReadonly=input
	End Property
	
	'Public methods
	Public Function GetCode()
		IF NOT Me.Render THEN EXIT Function END IF
		CreateValueObject()
		IF NOT IsNullOrEmpty(oValue.Text) THEN 
			oRadioButton.Properties.SetProperty "value", oValue.Text
			IF bCheckIfHasValue THEN bChecked=TRUE
		ELSE
			'Debugger Me, TypeName(oValue.Text)&"("&Me.Name&", "&Me.Id&"): "&oValue.Text
			IF IsEmpty(vDefaultValue) THEN 
				Debugger Me, "DefaultProperty must be set first ("&Me.Name&", "&Me.Id&")": [&Stop] Me
			END IF
			oRadioButton.Properties.SetProperty "value", vDefaultValue
			IF bCheckIfHasValue THEN bChecked=FALSE
		END IF
		IF bChecked THEN 
			oRadioButton.Properties.SetProperty "checked", null
		ELSE
			oRadioButton.Properties.RemoveProperty("checked")
		END IF
		oRadioButtonImage.Render=bIsReadonly
		oRadioButtonImage.Visible=bChecked
		oRadioButton.Visible= NOT(bIsReadonly)
		IF oLabel IS NOTHING THEN
			IF bShowLabel THEN
				CreateValueObject()
				Set oLabel=new Label
				oLabel.Text = oValue
				oContent.AddNamedItem oLabel, "Label"
			END IF
		ELSE
			oLabel.Render=bChecked
		END IF
		GetCode = oContent.GetCode()
	End Function

	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub
End Class 
 %>