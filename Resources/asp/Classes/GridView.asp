<%
Class GridView
	'Properties
	Private sDBTableName, sHeaderTemplate, sFooterTemplate
	
	'Settings
	Private bSupportsPaging, bSupportsInlineInsert, bSupportsInsert, bSupportsDetails, bSupportsUpdate, bSupportsDelete, bFloatingHeader

	'Private 
	
	'Objects
	Private oGridView, oTable, oDataSource, oContainer, oRelatives, oDisplayTime, oFormatTime
	
	'Private Properties
	Private sTableCode
	
	Public Property Get Visible()
		Visible = oGridView.Visible
	End Property
	Public Property Let Visible(input)
		oGridView.Visible = input
	End Property

	Private Sub Class_Initialize()
		Set oRelatives = [$Relatives](Me)
		Set oGridView = new Div
		oGridView.CSSClass="GridView"
		Set oTable = new Table
		oGridView.AddContent(oTable)
		Set oDataSource = [$DataSource]
		oDataSource.Table=Me
		bSupportsInlineInsert=NULL
	End Sub
    
	Private Sub Class_Terminate()
		Set oFormatTime = nothing
		Set oDisplayTime = nothing
		Set oTable = nothing
		Set oContainer = nothing
	End Sub
	
	'Interfaces
	Public Property Get Style()
		Set Style = oTable.Style
	End Property

	Public Property Get Properties()
		Set Properties = oTable.Properties
	End Property

	Public Property Get ProgressBar()
		Set ProgressBar = oTable.ProgressBar
	End Property

	Public Property Get DataSource()
		Set DataSource = oDataSource
	End Property
	
	Public Property Get Fields() 'Recupera todos los campos una vez que se hizo DataBind(), si se utiliza antes marca error
		Set Fields = oDataSource.Fields
	End Property

	Public Property Get Field(sFieldName) 'Recupera un campo en espec�fico una vez que se hizo DataBind(), si se utiliza antes marca error. Tambi�n puede acceder al campo por medio del m�todo Fields.Field("FieldName")
		Set Field = oDataSource.Fields.Field(sFieldName)
	End Property

	Public Property Get Relatives()
		Set Relatives = oRelatives
	End Property
	
	Public Property Get Container()
		Set Container = oContainer
	End Property
	Public Property Let Container(input)
		Set oContainer = input
	End Property

	'Properties
	Public Property Get TableIdentifier()
		TableIdentifier = oTable.TableIdentifier
	End Property
	Public Property Let TableIdentifier(input)
		oTable.TableIdentifier = input
	End Property

	Public Property Get ShowRowNumber()
		ShowRowNumber = oDataSource.ShowRowNumber
	End Property
	Public Property Let ShowRowNumber(input)
		oDataSource.ShowRowNumber = input
	End Property

	Public Property Get PageSize()
		PageSize = oDataSource.PageSize
	End Property
	Public Property Let PageSize(input)
		oDataSource.PageSize = input
	End Property

	Public Property Get HeaderTemplate()
		HeaderTemplate = sHeaderTemplate
	End Property
	Public Property Let HeaderTemplate(input)
		sHeaderTemplate = input
	End Property

	Public Property Get FooterTemplate()
		FooterTemplate = sFooterTemplate
	End Property
	Public Property Let FooterTemplate(input)
		sFooterTemplate = input
	End Property

	Public Property Get SupportsPaging()
		SupportsPaging = bSupportsPaging
	End Property
	Public Property Let SupportsPaging(input)
		bSupportsPaging = input
	End Property

	Public Property Get SupportsInlineInsert()
		SupportsInlineInsert = (bSupportsInsert AND (bSupportsInlineInsert=TRUE AND NOT(IsNullOrEmpty(bSupportsInlineInsert))))
	End Property
	Public Property Let SupportsInlineInsert(input)
		bSupportsInlineInsert = CBoolean(input)
	End Property

	Public Property Get SupportsInsert()
		SupportsInsert = bSupportsInsert
	End Property
	Public Property Let SupportsInsert(input)
		bSupportsInsert = CBoolean(input)
	End Property

	Public Property Get SupportsDetails()
		SupportsDetails = bSupportsDetails
	End Property
	Public Property Let SupportsDetails(input)
		bSupportsDetails = input
	End Property

	Public Property Get SupportsUpdate()
		SupportsUpdate = bSupportsUpdate
	End Property
	Public Property Let SupportsUpdate(input)
		bSupportsUpdate = input
	End Property

	Public Property Get SupportsDelete()
		SupportsDelete = bSupportsDelete
	End Property
	Public Property Let SupportsDelete(input)
		bSupportsDelete = input
	End Property

	Public Property Get DataBound()
		DataBound = oDataSource.DataBound
	End Property

	Public Property Get Mode()
		Mode = oDataSource.Metadata.Mode
	End Property
	Public Property Let Mode(input)
		oDataSource.Metadata.Mode = input
	End Property

	Public Property Get IsReadonly()
		IsReadonly = (UCASE(Me.Mode)="READONLY")
	End Property

	Public Property Get Id()
		Id = oTable.Id
	End Property
	Public Property Let Id(input)
		oTable.Id = input
	End Property

	Public Property Get CssClass()
		CssClass = oTable.CssClass
	End Property
	Public Property Let CssClass(input)
		oTable.CssClass = input
	End Property

	Public Property Get Render()
		Render = oTable.Render
	End Property
	Public Property Let Render(input)
		oTable.Render = input
	End Property

	Public Property Get DBTableName()
		DBTableName = sDBTableName
	End Property
	Public Property Let DBTableName(input)
		sDBTableName = input
		oTable.Properties.SetProperty "db_table_name", Me.DBTableName
		oTable.CssClass=Me.DBTableName
	End Property

	Public Property Get FieldNull()
		FieldNull = oDataSource.FieldNull
	End Property
	Public Property Let FieldNull(input)
		oDataSource.FieldNull = input
	End Property

	Public Property Get FieldBlank()
		FieldBlank = oTable.FieldBlank
	End Property
	Public Property Let FieldBlank(input)
		oTable.FieldBlank = input
	End Property

	Public Property Get FloatingHeader()
		FloatingHeader = bFloatingHeader
	End Property
	Public Property Let FloatingHeader(input)
		bFloatingHeader = input
	End Property

	Public Property Get IsMainTable()
		IsMainTable = oTable.IsMainTable
	End Property
	Public Property Let IsMainTable(input)
		IF NOT(CBOOL(input)) THEN 
			Me.SupportsInlineInsert=TRUE
		END IF
		oTable.IsMainTable = CBOOL(input)
		
	End Property

	'Methods
	Public Sub AddRow(ByRef oRow)
		oTable.AddRow(oRow)
	End Sub

	Public Sub DataBind()
		IF Me.ProgressBar.ProgressBarControlId<>"" THEN 
			Me.ProgressBar.ClassName="Fetching"
			Me.ProgressBar.ClearProgress()
		END IF
		
		DIM oFetching: Set oFetching = [$StopWatch]
		oFetching.StartTimer()
		'Debugger Me, "Iniciando recuperacion de informaci�n...": response.flush
		oDataSource.AutoCreate Me.DBTableName, TypeName(Me), Me.Mode
		IF NOT IsNullOrEmpty(oDataSource.Metadata.HeaderTemplate) THEN Me.HeaderTemplate=oDataSource.Metadata.HeaderTemplate
		IF NOT IsNullOrEmpty(oDataSource.Metadata.FooterTemplate) THEN Me.FooterTemplate=oDataSource.Metadata.FooterTemplate
'		IF SESSION("IdUsuario")=1 THEN Debugger Me, Me.SupportsInlineInsert
		IF Try(IsNull(Me.SupportsInlineInsert), TRUE, Me.SupportsInlineInsert) AND NOT(IsNullOrEmpty(oDataSource.Metadata.SupportsInlineInsert)) THEN Me.SupportsInlineInsert=oDataSource.Metadata.SupportsInlineInsert
'		IF SESSION("IdUsuario")=1 THEN 
'			Debugger Me, Me.SupportsInlineInsert
'			response.end 
'		END IF
		IF NOT IsNullOrEmpty(oDataSource.Metadata.SupportsInsert) THEN Me.SupportsInsert=oDataSource.Metadata.SupportsInsert
		IF NOT IsNullOrEmpty(oDataSource.Metadata.SupportsDetails) THEN Me.SupportsDetails=oDataSource.Metadata.SupportsDetails
		IF NOT IsNullOrEmpty(oDataSource.Metadata.SupportsUpdate) THEN Me.SupportsUpdate=oDataSource.Metadata.SupportsUpdate
		IF NOT IsNullOrEmpty(oDataSource.Metadata.SupportsDelete) THEN Me.SupportsDelete=oDataSource.Metadata.SupportsDelete

		IF Me.ProgressBar.ProgressBarControlId<>"" THEN 
			Me.ProgressBar.SetComplete()
			Me.ProgressBar.DataSource=oDataSource
		END IF
		oFetching.StopTimer()
		'Debugger Me, "Iniciando recuperacion de informaci�n...<strong>("&ROUND(oFetching.TotalTime, 3)&")</strong><br>	": response.flush
'			response.write Me.HeaderTemplate&"<br>"
	End Sub
	
	Public Sub BuildTable()
		Set oFormatTime=[$StopWatch]
		IF NOT Me.DataBound THEN Me.DataBind()
		oFormatTime.StartTimer()
		DIM oField, oRow, oCell
		DIM oFields: Set oFields=oDataSource.Fields
'		DIM rsDataSource: Set rsDataSource=oFields.DataSource.RecordSet
		
		'Debugger Me, "Iniciando formateo..."&ROUND(oFormatTime.Lap(), 3)&"<strong>("&ROUND(oFormatTime.ElapsedTime, 3)&")</strong>": response.flush
		'Table Headers
		Set oRow = new Row
		oRow.IsHeader = TRUE
		oRow.FloatingHeader=(Me.IsMainTable OR Me.FloatingHeader)
		IF NOT oFields.IdentityField IS NOTHING THEN 
			oFields.IdentityField.HeaderText=""
			IF NOT(Me.IsMainTable) THEN
				oFields.IdentityField.Control.Properties.SetProperty "checkParent", "true"
				oFields.IdentityField.Control.Properties.SetProperty "otherFields", "oIdentifier=(getIdentifier(this) || document.getElementById('identifier')); '"&TRIM(request.querystring("ParentIdentifier"))&"='+getVal(oIdentifier)"
			END IF
		END IF
		'Debugger Me, oFields.ValidFieldNames
		FOR EACH oField IN oFields.ToArray()
			IF oField.Render THEN
			'Debugger Me, "<strong>"&oField.FieldName&" ("&oField.DataType&")</strong>: "&oField.MetadataString&""
'				response.write oField.FieldName&": "&oField.TotalRowFormula&"<br>"
'				IF oField.GroupByColumn THEN RESPONSE.WRITE oField.HeaderText&"<br>"
'					IF oDataSource.FilterMembers.Exists(oField.FieldName) THEN oField.Visible=FALSE
				Set oCell = new Cell
				oCell.AddContent(oField.HeaderText)
				oCell.Visible=oField.Visible
				oRow.AddNamedCell oCell, oField.FieldName
			END IF
		NEXT

		DIM oInsertCommand: Set oInsertCommand = new ImageButton
		oInsertCommand.Width=30
		oInsertCommand.Height=30
		oInsertCommand.Id="CommandInsert"
		oInsertCommand.ImageURL=sImageLibraryPath & "btn_Insert.gif"
		IF Me.SupportsInlineInsert THEN
			oInsertCommand.CommandText="if (document.getElementById('"&Me.DBTableName&"_"&oTable.TableIdentifier&"_data_row')) { nr=addRow(document.getElementById('"&Me.DBTableName&"_"&oTable.TableIdentifier&"_data_row'), 0); fix_rowId( nr ); goToFirstVisibleObject(nr); repaintGridView(nr);/*addClass('Parent')*/} else {OpenLink('../DynamicData/Insert.asp?requestTable="&Me.DBTableName&"', false, false)}"
		ELSE
			oInsertCommand.CommandText="OpenLink('../DynamicData/Insert.asp?requestTable="&Me.DBTableName&"', false, false)"
		END IF
		oDataSource.Fields.CommandsField.Commands.AddNamedItem oInsertCommand, "Insert"
		
		IF Me.SupportsInlineInsert OR Me.SupportsInsert THEN
			IF NOT oFields.IdentityField IS NOTHING THEN oRow.NamedCell(oFields.IdentityField.FieldName).AddContent(oInsertCommand)
		END IF
		'Edit & Delete Command Buttons
'		IF Me.IsMainTable AND bSupportsDetails THEN
			IF NOT oFields.IdentityField IS NOTHING THEN 
				DIM oCommandUpdate: Set oCommandUpdate = new ImageButton
				oCommandUpdate.Id="CommandUpdate"
				oCommandUpdate.Width=25
				oCommandUpdate.Height=25
				oCommandUpdate.ImageURL=sImageLibraryPath & "btn_Edit.gif"
				oCommandUpdate.AlternateText="Editar registro"
				oCommandUpdate.CommandText="oIdentifier=getParent('TR', this).all('identifier'); oIdentifier=oIdentifier.length?oIdentifier(0):oIdentifier; OpenLink('../DynamicData/Update.asp?requestTable="&oFields.IdentityField.TableName&"&'+getDBColumnName(oIdentifier)+'='+getDBColumnValue(oIdentifier), false, false)"
				oCommandUpdate.Render=FALSE
				oDataSource.Fields.CommandsField.Commands.AddNamedItem oCommandUpdate, "Details"
			END IF
'		END IF

		IF bSupportsDelete THEN
			IF NOT oFields.IdentityField IS NOTHING THEN 
				DIM oCommandDelete: Set oCommandDelete = new ImageButton
				oCommandDelete.Id="CommandDelete"
				oCommandDelete.Width=25
				oCommandDelete.Height=25
				oCommandDelete.ImageURL=sImageLibraryPath & "btn_Delete.gif"
				oCommandDelete.AlternateText="Eliminar registro"
				oCommandDelete.CommandText="oIdentifier=getParent('TR', this).all('identifier'); oIdentifier=oIdentifier.length?oIdentifier(0):oIdentifier; var oGridView=$(oIdentifier).closest('.GridView'); deleteData(getDBTableName(oIdentifier), getDBColumnName(oIdentifier), getDBColumnValue(oIdentifier), getParent('TR', oIdentifier)); repaintGridView(oGridView);"
				oCommandDelete.Render=FALSE
				oDataSource.Fields.CommandsField.Commands.AddNamedItem oCommandDelete, "Delete"
			END IF
		END IF

		IF NOT(oDataSource.PageSelector IS NOTHING) THEN
			DIM oControlsRow: Set oControlsRow = new Row
			oControlsRow.IsHeader=TRUE
			Set oCell = new Cell
			oCell.AddContent(oDataSource.PageSelector)
			oCell.VAlign="middle"
			oCell.Align="left"
			oCell.ColSpan=oRow.CellCount
			oControlsRow.AddCell(oCell)
			oControlsRow.CssClass="ControlsRow"
			oTable.AddRow(oControlsRow)
		END IF
		IF (bSupportsDetails OR bSupportsDelete) THEN
			IF Me.IsMainTable AND bSupportsDetails THEN
				oDataSource.Fields.CommandsField.Command("Details").Render=TRUE
			END IF
			IF bSupportsDelete THEN
				oDataSource.Fields.CommandsField.Command("Delete").Render=bSupportsDelete
			END IF
		END IF
		oTable.AddRow(oRow)

		'Data Rows
		IF NOT(Me.SupportsInlineInsert) AND (oDataSource.BOF AND oDataSource.EOF) THEN 
			Exit Sub
		END IF
		DIM sNewHeader, sPastHeader, sNewFooter, sPastFooter
		DIM sFooterFields, sHeaderFields
'		response.end
'		sFooterFields=sFooterTemplate
		DIM sTranslatedHeaderTemplate
		IF TRIM(sHeaderTemplate)<>"" THEN 
			sTranslatedHeaderTemplate=TranslateTemplate(sHeaderTemplate)
			sHeaderFields=TranslateTemplate([&CString](sHeaderTemplate).Extract("\[(?!\$)(\w*)\]*").Join("&""<-->""&"))
		END IF
		DIM sTranslatedFooterTemplate
		IF TRIM(sFooterTemplate)<>"" THEN 
			sTranslatedFooterTemplate=TranslateTemplate(sFooterTemplate)
			sFooterFields=TranslateTemplate([&CString](sFooterTemplate).Extract("\[(?!\$)(\w*)\]*").Join("&""<-->""&"))
		END IF
		DO
'			response.write oFields("SQLRowNumber")&".- "&oFormatTime.Lap()&"<br>": response.flush
			Me.ProgressBar.ShowProgress()
			Set oRow = new DataRow
			oRow.DataSource = oDataSource
'			Debugger Me, oDataSource.Index
			oRow.RecordSetIndex = oDataSource.Index
			oRow.Id=""&Me.DBTableName&"_"&oTable.TableIdentifier&"_data_row"
'			IF oFields("SQLRowNumber") MOD 2 =0 THEN oRow.CssClass="AlternateRow"
'			RESPONSE.WRITE "Class: "&oRow.CssClass&"<br>"
			DIM bShowTotalRow: bShowTotalRow = FALSE
			DIM oTotalRow: Set oTotalRow = NOTHING
			DIM oTotalCell: Set oTotalCell = NOTHING
			DIM oTotalLabel
			DIM iColspanCellCount: iColspanCellCount=0
			FOR EACH oField IN oFields.GetItems()
''				IF oField.Render THEN
'					IF (UCASE(TypeName(oField))=UCASE("DataField")) THEN
						'Debugger Me, oField.FieldName&": "&TypeName(oField.Control)
						DIM vCurrentValue: vCurrentValue=oField.DBValue
						DIM vPastValue: vPastValue=oField.PrevValue
						DIM vNextValue: vNextValue=oField.NextValue
						IF oField.AutoRowSpan AND TRIM(oField.AutoRowSpanReferenceDataField)<>"" THEN
							vCurrentValue=oFields(oField.AutoRowSpanReferenceDataField).DBValue
							vPastValue=oFields(oField.AutoRowSpanReferenceDataField).PrevValue
							vNextValue=oFields(oField.AutoRowSpanReferenceDataField).NextValue
						END IF
						IF oField.AutoRowSpan AND NOT(IsEmpty(vPastValue)) AND TRIM(vPastValue)=TRIM(vCurrentValue) AND UCASE(TypeName(oField.Container))="CELL" THEN
							oField.Container.RowSpan=oField.Container.RowSpan+1
						ELSEIF oField.Render THEN
							Set oCell = new Cell
							oRow.AddCell(oCell)
							oCell.AddContent(oField)
							oCell.Visible=oField.Visible
						END IF

						IF oField.Render AND oField.Visible THEN 
							IF NOT oTotalCell IS NOTHING THEN
								oTotalCell.ColSpan=oTotalCell.ColSpan+1
							ELSE
								iColspanCellCount=iColspanCellCount+1
							END IF
						END IF

						IF oField.GroupByColumn THEN oRow.GroupName=AppendIfNotEmpty(oRow.GroupName, "_", "before") & ReplaceMatch(oField.DBValue, "[\s\+\-\\\/\*]", "")
						IF oField.GroupByColumn AND (IsEmpty(vNextValue) OR NOT(TRIM(vNextValue)=TRIM(vCurrentValue))) THEN
							IF oTotalRow IS NOTHING THEN 
								Set oTotalRow = new Row: oTotalRow.CssClass="TotalRow"
								Set oTotalCell = new Cell
								oTotalCell.Colspan=iColspanCellCount
								oTotalRow.AddCell(oTotalCell)
							END IF
						END IF
						
						IF NOT oTotalRow IS NOTHING THEN
							IF oField.TranslatedTotalRowFormula<>"" THEN
'								Debugger Me, oFields("GrupoCuenta")&" - "&(oFields("GrupoCuenta")="GASTOS FINANCIEROS")
								bShowTotalRow=TRUE
								IF oTotalCell.Colspan>1 THEN 
									oTotalCell.Colspan=oTotalCell.Colspan-1
									oTotalCell.AddContent("&nbsp;")
								END IF
'								Debugger Me, oField.TranslatedTotalRowFormula
								DIM oTotalRowFormula: Assign oTotalRowFormula, EvaluateFieldsTemplate(oField.TranslatedTotalRowFormula, oFields)
								DIM sTotalRowFormula
								IF IsObject(oTotalRowFormula) THEN
'									RESPONSE.WRITE TypeName(oTotalRowFormula)&": <br>"&oField.TotalRowFormula&"<br>"&oTotalRowFormula.GetCode()
									IF TypeName(oTotalRowFormula)="Cell" THEN
										Set oTotalCell = oTotalRowFormula
										oTotalRow.AddCell(oTotalCell)
									ELSE
										 Err.Raise 13 'type mismatch
									END IF
								ELSE
									Set oTotalCell = new Cell
									oTotalRow.AddCell(oTotalCell)
									sTotalRowFormula=oTotalRowFormula
									Set oTotalLabel = new Formula
									oTotalLabel.Id="subtotal_"&oField.FieldName&""
									oTotalLabel.Formula=sTotalRowFormula
									oTotalLabel.Format=oField.Format
									IF UCASE(TypeName(oField.Control))="FORMULA" THEN oTotalLabel.DecimalPositions=oField.Control.DecimalPositions
									oTotalCell.Align="right"
									oTotalCell.AddContent(oTotalLabel)
								END IF
							END IF
						END IF
'					END IF
''				END IF
			NEXT
			oFields.IdentityField.Container.AddContent(oDataSource.Fields.CommandsField)

			'Row Headers
			IF TRIM(sTranslatedHeaderTemplate)<>"" THEN
				sNewHeader=EvaluateFieldsTemplate(sTranslatedHeaderTemplate, oFields)
				IF sNewHeader<>sPastHeader THEN 
					Dim oNewHeaderRow: Set oNewHeaderRow = new Row
					oNewHeaderRow.CssClass="HeaderRow"
					Dim oHead: Set oHead=new Cell
					oHead.CellType="TH"
					oHead.AddContent(sNewHeader)
					oHead.ColSpan=oRow.CellCount()
					oNewHeaderRow.AddCell(oHead)
					oTable.AddRow(oNewHeaderRow)
					sPastHeader=sNewHeader
				END IF
			END IF 

			oTable.AddRow(oRow)
''			Debugger Me, TypeName(oFields("SQLRowNumber").Relatives.Parent)
			IF oFields.ExistsItem("SQLRowNumber") THEN IF oFields("SQLRowNumber").Render THEN oFields("SQLRowNumber").Container.IsHeader=TRUE
'			Debugger Me, "("&oDataSource.Index&", "&oDataSource.BOF&" - "&oDataSource.EOF&")"
			IF NOT(oDataSource.BOF AND oDataSource.EOF) THEN 
'				'Row Footers
				IF sFooterFields<>"" AND IsEmpty(sPastFooter) THEN
					sPastFooter=EvaluateFieldsTemplate(sFooterFields, oFields)
					'Debugger Me, "<strong>Past: </strong>"&sPastFooter
				END IF

				oDataSource.MoveNext

				IF NOT(oDataSource.EOF) AND sFooterFields<>"" THEN
					sNewFooter=EvaluateFieldsTemplate(sFooterFields, oFields)
					'Debugger Me, "<strong>New: </strong>"&sNewFooter
				END IF
			END IF
			IF oDataSource.EOF OR sNewFooter<>sPastFooter THEN
				oDataSource.MovePrevious
				Dim oFooterRow: Assign oFooterRow, EVAL(sTranslatedFooterTemplate)
				
				IF IsObject(oFooterRow) THEN
					
				ELSE
					Set oFooterRow = _
					[$Row]("@CssClass='FooterRow'", Array _
						(_
						[$Cell]("@ColSpan="&oRow.CellCount()&", @CellType='TH'", sPastFooter)_
						)_
					)
				END IF
				sPastFooter=sNewFooter
				oTable.AddRow(oFooterRow)
				oDataSource.MoveNext
			END IF
			IF NOT(oTotalRow IS NOTHING) THEN 
				IF bShowTotalRow THEN oTable.AddRow(oTotalRow)
'				IF IsNullOrEmpty(oRow.GroupName) THEN oTable.AddGroup()
			END IF
		LOOP UNTIL oDataSource.EOF

		IF Me.IsMainTable AND (Me.SupportsUpdate OR Me.SupportsInlineInsert) THEN
			response.write "<script language=""JavaScript"">try { top.frames('buttons').btnSave.enabled=true } catch(e) {}</script>"
			DIM oSubmit: Set oSubmit = new Button
			DIM sOnSubmit:	sOnSubmit=oDataSource.Metadata.OnSubmit
			IF NOT(IsNullOrEmpty(sOnSubmit)) THEN oSubmit.Properties.SetProperty "toDoOnSubmit", sOnSubmit
			oSubmit.Id="submit_button"
			oSubmit.Value="Guardar Datos"
'				oSubmit.Properties.SetProperty "toDoOnSubmit", "try {element.document.location.reload()} catch(e2) {alert(e2.description)}"
			Set oRow=New Row
			Set oCell=New Cell
			oCell.AddContent(oSubmit)
			oRow.AddCell(oCell)
			Me.AddRow(oRow)
			'oRow.Visible=FALSE
		END IF
		IF IsObject(Me.ProgressBar) THEN Me.ProgressBar.SetComplete()
		oFormatTime.StopTimer()
'		Debugger Me, "<strong>Total:</strong>"&oFormatTime.TotalTime: response.flush
'		[&Stop] Me
	End Sub
	
	Public Function GetCode()
		Set oDisplayTime = new StopWatch
		oDisplayTime.StartTimer()
		IF sTableCode="" THEN
			Me.BuildTable()
			sTableCode=oGridView.GetCode()
		END IF
		oDisplayTime.StopTimer()
		GetCode=sTableCode
	End Function
	
	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub
	
	Public Sub ShowStatistics()
		response.write "<center>"
		response.write "********* <strong>Statistics for "&Me.DBTableName&" (Id "&Me.Id&")</strong> ************* " &"<br>"
		response.write "Open Connection: "&ROUND(oDataSource.TimeOpenConn, 3) &"<br>"
		response.write "Retrieving metadata: "&ROUND(oDataSource.TimeMetadata, 3) &"<br>"
		response.write "Querying: "&ROUND(oDataSource.TimeQuery, 3) &"<br>"
		response.write "Fetching: "&ROUND(oDataSource.TimeFetching, 3) &"<br>"
		response.write "Formating: "& ROUND(oFormatTime.TotalTime, 3) &"<br>"
		response.write "Displaying: "&ROUND(oDisplayTime.TotalTime, 3) &"<br>"
		response.write "<strong>Total: "&ROUND((oDataSource.TimeOpenConn+oDataSource.TimeMetadata+oDataSource.TimeQuery+oDataSource.TimeFetching+oFormatTime.TotalTime+oDisplayTime.TotalTime), 3)&"</strong><br>"
		response.write "**************************************** " &"<br><br>"
		response.write "</center>"
	End Sub
End Class
%>