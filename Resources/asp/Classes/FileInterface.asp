<%
Class FileInterface
	'Properties
	Private sDBTableName, sFileName
	
	'Objects
	'Private oDataSource
	
	'Private Properties
	Private sFields, bOpened, sTableCode
    Private tStartOpen, tEndOpen, tElapsedOpen
	Private tStartDisplay, tEndDisplay, tElapsedDisplay
	
	Private Sub Class_Initialize()
		bOpened=FALSE
	End Sub
    
	Private Sub Terminate()
	End Sub
	
	'Properties
	Public Property Get DBTableName()
		DBTableName = sDBTableName
	End Property
	Public Property Let DBTableName(input)
		sDBTableName = input
	End Property


	Public Property Get FileName()
		FileName = sFileName
	End Property
	Public Property Let FileName(input)
		sFileName = input
	End Property


    Public Property Get TimeOpen()
		TimeFech = tElapsedOpen
    End Property

	Public Sub ShowStatistics()
		response.write "********* Statistics for "&Me.DBTableName&" ************* " &"<br>"
		response.write "Opening: "&ROUND(tElapsedOpen, 3) &"<br>"
		response.write "Displaying: "&ROUND(tElapsedDisplay, 3) &"<br>"
		response.write "Total: "&ROUND(tElapsedOpen+tElapsedDisplay, 3)&"<br>"
		response.write "********************************** " &"<br><br>"
	End Sub

	Private Function OpenFile()
		IF NOT bOpened THEN 
	    	tStartOpen=timer
			sTableCode="<iframe FRAMEBORDER=0 SCROLLING=NO src=""../Generic_Code/InterpretadorDocumentos.asp?RequestTable="&sDBTableName&"&FileName="& sFileName &"&sId="&RandomNumber(1000)&""" style=""height:100%; width:100%"" ></iframe>"'oFile.GetCode()
	    	tEndOpen=timer
			tElapsedOpen=tEndOpen-tStartOpen
			bOpened=TRUE
		END IF
		OpenFile=sTableCode
	End Function
	
	Public Function GetCode()
		GetCode=OpenFile()
	End Function
	
	Public Sub WriteCode()
		DIM sWriteCode
		sWriteCode=Me.GetCode()
    	tStartDisplay=timer
		response.write sWriteCode
		tEndDisplay=timer
		tElapsedDisplay=tEndDisplay-tStartDisplay
	End Sub
End Class
%>