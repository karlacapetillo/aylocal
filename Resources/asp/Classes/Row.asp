<% 
Function [$Row](ByVal sParameters, ByRef aCells)
	DIM oRow:	Set oRow=[&New]("Row", sParameters)
	SELECT CASE TypeName(aCells)
	CASE "Cell"
		oRow.AddCell(aCells)
	CASE ELSE
		oRow.AddCellArray(aCells)
	END SELECT
	Set [$Row]=oRow
End Function
Class Row
	'Member Variables
	Private sObjectCode, sId, bIsHeader, bIsFooter
	Private iRowPosition, sGroupName
	Private iRowNumber
	Private returnString
	Private bVisible, bRender
    
	'Objects
	Private oRow, oContainer, oCells, oRelatives, oCurrentRecordSet

	Public Property Get Object()
		Set Object = Me
	End Property
	
	Private Sub Class_Initialize()
'		Set oRelatives = [$Relatives](Me)
'		oRelatives.Content = new Tag
		Set oRow = new Tag	'Set oRow = oRelatives.Content
		Set oCells = oRow.Content
		oRow.TagName="TR"
		iRowPosition=NULL
		iRowNumber = NULL
		Me.Visible=TRUE
		Me.Render=TRUE
	End Sub
    
	Private Sub Class_Terminate()
		Set oCells = nothing
		Set oRow = nothing
		Set oContainer = nothing
	End Sub

	'Interfaces
	Public Property Get Relatives()
		Set Relatives = oRelatives
	End Property
	
	Public Property Get Container()
		Set Container = oRelatives.Parent
	End Property
	Public Property Let Container(input)
		oRelatives.Parent = input
	End Property


	Public Property Get PrevRow()
		Set PrevRow = oRelatives.PrevSibling
	End Property
	Public Property Let PrevRow(input)
		oRelatives.PrevSibling = input
	End Property


	Public Property Get NextRow()
		Set NextRow = oRelatives.NextSibling
	End Property
	Public Property Let NextRow(input)
		oRelatives.NextSibling = input
	End Property


	'Properties
	Public Property Get GroupName()
		GroupName = sGroupName
	End Property
	Public Property Let GroupName(input)
		sGroupName = input
	End Property

	Public Property Get Style()
		Set Style = oRow.Style
	End Property
	
	Public Property Get RowNumber()
		RowNumber = iRowNumber
	End Property
	Public Property Let RowNumber(input)
		iRowNumber = input
	End Property

	Public Property Get IsReadonly()
		IsReadonly = oCells.IsReadonly
	End Property
	Public Property Let IsReadonly(input)
		oCells.IsReadonly = input
	End Property
	
    Public Property Get CellCount()
		CellCount = oCells.ItemsCount
    End Property
       
    Public Property Let Id(input)
		sId = input
		oRow.Id=sId
    End Property
       
    Public Property Get CssClass()
		CssClass=oRow.CssClass
    End Property
    Public Property Let CssClass(input)
		oRow.CssClass=input
    End Property

	Public Property Let Position(var_iRowPosition)
		iRowPosition=var_iRowPosition
	End Property
	Public Property Get Position()
		Position=iRowPosition
	End Property


	Public Property Get IsHeader()
		IsHeader=bIsHeader
	End Property
	Public Property Let IsHeader(var_bIsHeader)
		bIsHeader=var_bIsHeader
		IF bIsHeader THEN 
			Me.Id="header"
			oCells.ApplyProperty "CellType", "TH"
		ELSE
			IF Me.Id="header" THEN Me.Id=""
			oCells.ApplyProperty "CellType", "TD"
		END IF
	End Property


	Public Property Get FloatingHeader()
		FloatingHeader = (Me.Id="header") 'bFloatingHeader
	End Property
	Public Property Let FloatingHeader(input)
		IF CBOOL(input) THEN
			Me.Id="header"
		ELSE
			Me.Id=""
		END IF
	End Property


	Public Property Get IsFooter()
		IsFooter = bIsFooter
	End Property
	Public Property Let IsFooter(input)
		bIsFooter = input
		IF bIsFooter THEN 
			oCells.ApplyProperty "CellType", "TH"
		ELSE
			oCells.ApplyProperty "CellType", "TD"
		END IF
	End Property


	Public Property Get Render()
		Render=oRow.Render
	End Property
	Public Property Let Render(var_bRender)
		bRender=var_bRender
		oRow.Render=bRender
	End Property
	
	Public Property Get Visible()
		Visible=oRow.Visible
	End Property
	Public Property Let Visible(input)
		oRow.Visible=input
	End Property
	
	'Public methods
	Public Default Function GetItemByPosition(sPosition)
		Set GetItemByPosition=oCells.GetItemByPosition(sPosition)
	End Function
	
	Public Property Get NamedCell(sCellName)
		Set NamedCell = oCells.NamedItem(sCellName)
	End Property

	Public Sub SetCellType(ByRef oCell)
		[&CheckValidObjectType] oCell, "Cell"
		IF NOT(IsNullOrEmpty(oCell.CellType)) THEN Exit Sub
		IF bIsHeader OR bIsFooter THEN 
			oCell.CellType="TH"
		ELSE
			oCell.CellType="TD"
		END IF
	End Sub
	
	Public Property Get Cells()
		Set Cells = oRelatives.Content
	End Property

	Public Sub AddCellArray(ByRef input)
		DIM oCell
		FOR EACH oCell IN input
			Me.AddCell oCell
		NEXT
	End Sub
	
	Public Sub AddCell(ByRef oCell)
'		SetCellType(oCell)
''		IF Me.CellCount MOD 2 = 1 THEN oCell.CssClass="AlternateCell"
''		Debugger Me, "Agregando Celda<br>"
''		oRelatives.AddChildren oCell
		oRow.AddContent oCell
	End Sub

	Public Sub AddNamedCell(ByRef oCell, ByVal sCellName)
		SetCellType(oCell)
		oCells.AddNamedItem oCell, sCellName
	End Sub

	Public Sub AddCellAt(ByRef oCell, ByVal iPosition)
		SetCellType(oCell)
		oCells.AddItemAt oCell, iPosition
	End Sub

	Public Function GetCode() 'Function Generate()
		IF NOT Me.Render THEN 
			EXIT Function
		END IF
		IF sObjectCode="" THEN
			sObjectCode=oRow.GetCode()
		END IF
		GetCode = sObjectCode
	End Function
	
	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub
End Class 
%>