<% 
Class Password
	'Properties
	Private iSteps, iValue, bVisible
	
	'Objects 
	Private oTextBox, oTextBox2, oContainer, oRelatives
	
	Public Property Get Visible()
		Visible = bVisible
	End Property
	Public Property Let Visible(input)
		bVisible = input
	End Property

	Public Property Get Object()
		Set Object = oTextBox.Object
	End Property

	Private Sub Class_Initialize()
		Set oRelatives = [$Relatives](Me)
		Set oTextBox = new TextBox
		oTextBox.TextMode="password"
		oTextBox.CssClass="text"
		oTextBox.Methods.OnBlur="passwordComparison(this, nextElement(this, 1))"
		Set oTextBox2 = new TextBox
		oTextBox2.TextMode="password"
		oTextBox2.CssClass="text"
		oTextBox2.Object.Properties.SetProperty "isSubmitable", "false"
		oTextBox2.Methods.OnBlur="passwordComparison(nextElement(this, -1), this)"
	End Sub

	'Output objects
	Public Property Get Style()
		Set Style = oTextBox.Style
	End Property
	
	Public Property Get Methods()
		Set Methods = oTextBox.Methods
	End Property

	'Output Interfaces 
	Public Property Get Name()
		Name = oTextBox.Name
	End Property

	Public Property Get Relatives()
		Set Relatives = oRelatives
	End Property
	
	Public Property Get Container()
		Set Container = oRelatives.Parent
	End Property
	Public Property Let Container(input)
		oRelatives.Parent = input
	End Property

	Public Property Get ParentCell()
		ParentCell = oTextBox.ParentCell
	End Property

	Public Property Get Id()
		Id = oTextBox.Id
	End Property
	
	Public Property Get ImageURL()
'		ImageURL = oTextBox.ImageURL
	End Property
	
	Public Property Get CssClass()
		CssClass = oTextBox.CssClass
	End Property
	
	'Input Interfaces
	Public Property Let Name(input)
		oTextBox.Name = input
		oTextBox2.Name = input
	End Property

	Public Property Let ParentCell(input)
		oTextBox.ParentCell = input
	End Property

	Public Property Let HasBorder(input)
		oTextBox.ApplyProperty "HasBorder", input
	End Property

	Public Property Let Width(input)
		oTextBox.ApplyProperty "Width", input
	End Property

	Public Property Let Height(input)
		oTextBox.ApplyProperty "Height", input
	End Property

	Public Property Let AlternateText(input)
		oTextBox.ApplyProperty "AlternateText", input
	End Property
	
	Public Property Let Id(input)
		oTextBox.Id = input
	End Property
	
	Public Property Let ImageURL(input)
		oTextBox.ApplyProperty "ImageURL", input
	End Property

	Public Property Let CssClass(input)
		oTextBox.CssClass = input
	End Property
	
	'Output Properties 
	Public Property Get Text()
		Text = Me.Value
	End Property
	Public Property Let Text(input)
		Me.Value = input
	End Property

	Public Property Get Value()
		Value = oTextBox.Value
	End Property

	Public Property Get CommandText()
		CommandText = sCommandText
	End Property

	Public Property Get Steps()
		Steps = iSteps
	End Property
	
	'Input Properties 
	Public Property Let Value(input)
		oTextBox.Value = input
		oTextBox2.Value = input
	End Property

	Public Property Let MinValue(input)
'		response.write "Me.MinValue: "&Me.MinValue&"<br>"
		oTextBox.MinValue=input
	End Property

	Public Property Let MaxValue(input)
'		response.write "Me.MaxValue: "&Me.MaxValue&"<br>"
		oTextBox.MaxValue=input
	End Property

	'Public methods
	Public Function GetCode()
		GetCode=oTextBox.GetCode()&"<br>Confirme:<br>"&oTextBox2.GetCode()
	End Function

	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub
End Class 
 %>