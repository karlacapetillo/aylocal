<% 
'Original Source from http://www.daniweb.com/forums/printthread19997.html
Class DropDownList
	'Properties
	Private sCatalog, bMultiple, sName, sValue, sText
	
	'Settings
'	Private 
	
	'Objects
	Private oRelatives, oSelectedOption, oDataValue, oValue, oHiddenField, oLabel
	
	'Member Variables
	Private bIsData
	Private sHTMLCode
	Private sSelectItem
	Private iCounter
	
	Private returnString
	
	'Objects 	
	Private oDropDownList, oReadonlyTag, oOption, sKeyDown
	
	Public Property Get Object()
		Set Object = oDropDownList
	End Property

	Public Property Get Tag()
		Set Tag = oDropDownList
	End Property

	Private Sub Class_Terminate()
		Set oHiddenField = nothing
		Set oLabel = nothing
		Set oSelectedOption = nothing
		Set oDataValue = nothing
		Set oValue = nothing
	End Sub
	
	Private Sub Class_Initialize()
		Set oHiddenField = nothing
		Set oLabel = nothing
		Set oDataValue = nothing
		Set oValue = nothing
		bIsData = False
		sName = ""
'		sSelectItem = ""
		Set oDropDownList = [$Tag]("select")
'		Set oRelatives = oDropDownList.Relatives
		oDropDownList.HasClosingTag=TRUE
		bMultiple=FALSE
		Set oSelectedOption = nothing
		'Me.Properties.ExtendedProperties.CheckValidName=FALSE
	End Sub

	'Interfaces
	Public Property Get Relatives()
		Set Relatives = oRelatives
	End Property
	
	Public Property Get Container()
		Set Container = oRelatives.Parent
	End Property
	Public Property Let Container(input)
		oRelatives.Parent = input
	End Property

	'Output objects
	Public Property Get Properties()
		Set Properties = oDropDownList.Properties
	End Property

	Public Property Get Methods()
		Set Methods = oDropDownList.Methods
	End Property
	
	Public Property Get Style()
		Set Style = oDropDownList.Style
	End Property
	
	'Output properties
	Public Property Get Visible()
		Visible = oDropDownList.Visible
	End Property
	Public Property Let Visible(input)
		oDropDownList.Visible = input
	End Property

	Public Property Get Multiple()
		Multiple = bMultiple
	End Property

	Public Property Get Catalog()
		Catalog = sCatalog
	End Property

	Public Property Get CssClass()
		CssClass = oDropDownList.CssClass
	End Property
	
	Public Sub GetValue()
		IF NOT oDataValue IS NOTHING THEN
			IF oValue IS NOTHING THEN
				Set oValue = new VariantValue
			END IF
			IF INSTR(oDataValue.Value, "@:@")>0 THEN
				sValue=oDataValue.Value
				IF INSTR(oDataValue.Value, "@/@")>0 THEN sValue=SPLIT(oDataValue.Value, "@/@")(Me.Properties.ExtendedProperties.NamedItem("CDDIndex"))	'CUANDO VIENE DESDE CASCADED DROP DROWN
				DIM aValues: aValues=SPLIT(sValue, "@:@")
				'Debugger Me, sValue&" Array: "&ubound(aValues)
				oValue.Value=aValues(0)
				oValue.Text=aValues(1)
			ELSE
				oValue.Value=oDataValue.Value
				oValue.Text=oDataValue.Text
			END IF
		END IF
	End Sub
	
	Public Sub CreateValueObject()
		IF oDataValue IS NOTHING THEN
			Set oDataValue = new VariantValue
		END IF
	End Sub
	
	Public Property Get Value()
		CreateValueObject()
		GetValue()
		SELECT CASE TypeName(oValue)
		CASE "DataValue"
			Set Value = oValue
		CASE ELSE
			Value = oValue.Value
		END SELECT
	End Property
	Public Property Let Value(input)
		IF IsNullOrEmpty(input) THEN Exit Property
		IF IsObject(Input) THEN
			Set oDataValue = input
		ELSE 
			CreateValueObject()
			oDataValue.Value = input
		END IF

		

'		sValue = TRIM(CSTR(input))
'		DIM bExistsItem: bExistsItem=oDropDownList.ExistsItem(sValue)
		
'		IF TRIM(sValue)="" THEN sValue="BLANK"
'		IF Me.OptionsCount>0 AND NOT(bExistsItem) AND sValue<>"BLANK" THEN
'			Me.NewOption sValue, sValue
'			RESPONSE.WRITE "<script language=""JavaScript"">alert('WARNING: Default value for DropDownList "&sName&" ("""&sValue&""") was not found. It was registered to avoid data loss')</script>"
'		END IF
'		IF sValue="BLANK" THEN 
'			oDropDownList.Properties.RemoveProperty "defaultValue"
'		ELSE
'			oDropDownList.Properties.SetProperty "defaultValue", sValue
'		END IF
'		IF Me.OptionsCount>0 AND bExistsItem THEN 
'			Me.SelectedOption=oDropDownList(sValue)
'			oDropDownList(sValue).Properties.SetProperty "selected", null
'			sText=oDropDownList(sValue).Text
'		ELSE
'			Me.SelectedOption=NOTHING
'		END IF
	End Property
	
	Public Property Get Text()
		GetValue()
		Text = oValue.Text
	End Property
	Public Property Let Text(input)
		oValue.Text = input
	End Property

	Public Property Get SelectItem()
		SelectItem = sSelectItem
	End Property
	
	'Properties interfaces
	Public Property Get IsReadonly()
		IsReadonly = oDropDownList.IsReadonly
	End Property
 	Public Property Let IsReadonly(input)
		oDropDownList.IsReadonly = input
	End Property

	'Input Properties
	Public Property Let Multiple(input)
		bMultiple = input
	End Property

	Public Property Let Catalog(input)
		sCatalog = input
		Me.Properties.SetProperty "catalogo", input
	End Property

	Public Property Let CssClass(input)
		oDropDownList.CssClass = input
	End Property
	
	Public Property Get Name()
		Name = sName
	End Property
	Public Property Let Name(input)
		sName = input
		oDropDownList.Properties.SetProperty "name", sName
	End Property
	
	Public Property Let Id(input)
		oDropDownList.Id=input
	End Property
	
	Public Property Let SelectItem(input)
		sSelectItem = input
	End Property
	
	'Public methods
	Public Sub FillCombo(ByRef rs)
		'Takes reference to  recordset
		'and puts data into a 2 dimensional
		'array
		If NOT(rs.EOF and rs.BOF) Then
			bIsData = False
		Else
			Dim aOptionElements
			bIsData = True
			aOptionElements = rs.GetRows
			FillComboWithArray(aOptionElements)
		End If
	End Sub
	
	Public Property Get OptionsCount()
		OptionsCount = oDropDownList.ItemsCount
	End Property

	Public Sub FillComboWithArray(ByRef elements)
		bIsData = True
		DIM iOptionIndex
		For iOptionIndex = 0 to Ubound(elements,2)
			DIM sThisValue: sThisValue=RTRIM(elements(0,iOptionIndex))
			DIM sThisText: sThisText=TRIM(elements(1,iOptionIndex))
			Me.NewOption sThisValue, sThisText
		Next
	End Sub
	
	Public Function NewOption(sOptValue, sOptThisText)
		Set oOption = New ListItem
		oOption.Text=sOptThisText
		oOption.Value=RTRIM(sOptValue)
		IF sOptValue=sValue THEN oOption.Selected=TRUE
		
		oDropDownList.AddNamedItem oOption, CSTR(sOptValue)
		Set NewOption=oOption
	End Function
	
'	Public Sub ResetProperties()
'		oDropDownList.Properties.RemoveProperty "defaultValue"
'	End Sub
	
	Public Property Get SelectedOption()
		Set SelectedOption = oSelectedOption
	End Property
	Public Property Let SelectedOption(input)
		IF NOT(oSelectedOption IS NOTHING) THEN oSelectedOption.Properties.RemoveProperty "selected"
		Set oSelectedOption=input
		IF NOT(oSelectedOption IS NOTHING) THEN oSelectedOption.Properties.SetProperty "selected", null
	End Property

	Public Sub RemoveItems()
		oDropDownList.ClearContent()
	End Sub

	Public Sub ClearSelections()
		oDropDownList.All.RemoveProperty "selected"
	End Sub
	
	Public Function GetCode()
		GetValue()
		'Debugger Me, Me.Properties.ExtendedProperties.NamedItem("ArrayIndex")
		IF Me.IsReadonly THEN
			IF oHiddenField IS NOTHING THEN 
				Set oHiddenField = new HiddenField
				oHiddenField.Id=Me.Name
			END IF
			IF oLabel IS NOTHING THEN 
				Set oLabel = new Label
			END IF
			oHiddenField.Value=Me.Value
			oLabel.Text=Me.Text
			GetCode=oHiddenField.GetCode()&" "&oLabel.GetCode()
		ELSE
			IF oDropDownList.ExistsItem(Me.Value) THEN oDropDownList(Me.Value).Properties.SetProperty "selected", null
			GetCode=oDropDownList.GetCode()
		END IF
	End Function
	
	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub
	
End Class 
 %>