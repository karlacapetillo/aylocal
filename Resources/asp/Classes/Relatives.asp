<% 
Function [$Relatives](ByRef oSource)
'	response.write "<strong>"&TypeName(oSource)&"</strong> -> "
	DIM oRelatives:	Set oRelatives = New Relatives
	oRelatives.Caller=oSource
''	[&BR]
	Set [$Relatives]=oRelatives	'Set [$Relatives]=nothing
End Function
Class Relatives
	Private iIndex, sType
	Private oCaller, oParent, oChildren, oContent
	
	Private Sub Class_Initialize()
		Set oCaller = nothing
		Set oParent = nothing
		Set oChildren = nothing
		Set oContent = nothing
	End Sub
	Private Sub Class_Terminate()
		Set oCaller = nothing
		Set oParent = nothing
		Set oChildren = nothing
		Set oContent = nothing
	End Sub

	'Interfaces
	Public Property Get Level()
		IF NOT oCaller.Relatives.Parent IS NOTHING THEN
'			Debugger Me, TypeName()
			Level = oCaller.Relatives.Parent.Relatives.Level+1
		ELSE
			Level = 0
		END IF
	End Property

	Public Sub GetTree()
		DIM iLevel:	iLevel=Me.Level
		response.write replicate("|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", iLevel)
		response.write "| "&TypeName(oCaller)
		IF NOT oCaller.Relatives.Parent IS NOTHING THEN response.write " ("&TypeName(oCaller.Relatives.Parent.Relatives.Parent)&")"
		SELECT CASE TypeName(oCaller)
		CASE"Tag"
		response.write " ("&oCaller.TagName&")"
		CASE ELSE
		END SELECT
		response.write "<br>"
		DIM vElement
		FOR EACH vElement IN Me.Content.GetItems()
			IF IsObject(vElement) THEN
				SELECT CASE TypeName(vElement)
				CASE "Collection"
					DIM vChild
					FOR EACH vChild IN vElement.GetItems()
						IF IsObject(vChild) THEN
							vChild.Relatives.GetTree()
						ELSE
			'				response.write "Object"
						END IF
					NEXT
				CASE ELSE
					vElement.Relatives.GetTree()
				END SELECT
			ELSE
'				response.write "Object"
			END IF
		NEXT
	End Sub

	Public Property Get Caller()
		Set Caller = oCaller
	End Property
	Public Property Let Caller(input)
		Set oCaller = input
	End Property

	Public Property Get Index()
		Index = iIndex
	End Property
	Public Property Let Index(input)
		iIndex = input
	End Property

	Public Property Get Parent()
		Set Parent = oParent
	End Property
	Public Property Let Parent(input)
		Set oParent = input
'		Debugger Me, "<strong>Relatives: "&TypeName(oParent.Relatives.Children)&", Parent: "&TypeName(oParent)&", Child:"&TypeName(oCaller)&"</strong>"
		ON ERROR	RESUME NEXT
		[&Catch] TRUE, TypeName(Me)&".Parent", "<strong>Relatives: "&TypeName(oParent.Relatives.Children)&", Parent: "&TypeName(oParent)&", Child:"&TypeName(oCaller)&"</strong>"
		ON ERROR	GOTO 0
	End Property
	
	Public Property Get [Type]()
		[Type] = sType
	End Property
	Public Property Let [Type](input)
		sType = input
	End Property

'	Public Property Get Content()
'		Set Content = oContent
'	End Property
'	Public Property Let Content(input)
'		Set oContent = input
'	End Property

'	Public Property Get Children()
'		Set Children = oContent
'	End Property
	
	Public Property Get PrevSibling()
		Set PrevSibling = Me.Parent.Relatives.Children(iIndex-1)
	End Property

	Public Property Get NextSibling()
		Set NextSibling = Me.Parent.Relatives.Children(iIndex+1)
	End Property


	'Methods
	Public Function GetParentOfType(sParentType)
		DIM oDirectParent
		Set oDirectParent=oCaller.Container
		DO UNTIL oDirectParent IS NOTHING OR UCASE(TypeName(oDirectParent))=UCASE(sParentType)
			Set oDirectParent=oDirectParent.Container
		LOOP
		Set GetParentOfType=oDirectParent
	End Function
	
	Public Function GetTopParent()
		DIM oDirectParent
		Set oDirectParent=oCaller.Container
		DO UNTIL oDirectParent.Container IS NOTHING
			Set oDirectParent=oDirectParent.Container
		LOOP
		Set GetTopParent=oDirectParent
	End Function
End Class
 %>