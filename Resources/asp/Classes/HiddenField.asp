<% 
Class HiddenField
	'Properties
	Private sName, sMode, oValue

	'Settings
	
	'Objects
	Private oHiddenField, oRelatives
	
	'Private variables
	
	Public Property Get Object()
		Set Object = oHiddenField
	End Property

	Private Sub Class_Terminate()
		Set oValue = nothing
		Set oHiddenField = nothing
		Set oRelatives = nothing
	End Sub
	Private Sub Class_Initialize()
'		Set oRelatives = [$Relatives](Me)
		Set oHiddenField=new Tag
		oHiddenField.TagName="input"
		oHiddenField.HasClosingTag=FALSE
		oHiddenField.Properties.SetProperty "type", "hidden"
		Set oValue = nothing
	End Sub
	
	'Output objects
	Public Property Get Properties()
		Set Properties = oHiddenField.Properties
	End Property

	Public Property Get Style()
		Set Style = oHiddenField.Style
	End Property
	
	Public Property Get Methods()
		Set Methods = oHiddenField.Methods
	End Property

	'Properties

	'Output Properties 
	Public Property Get Relatives()
		Set Relatives = oRelatives
	End Property
	
	Public Property Get Container()
		Set Container = oRelatives.Parent
	End Property
	Public Property Let Container(input)
		oRelatives.Parent = input
	End Property

	Public Property Get Visible()
		Visible = FALSE
	End Property

	Public Property Get Name()
		Name = sName
	End Property
	Public Property Let Name(input)
		sName = input
		oHiddenField.Properties.SetProperty "name", sName
	End Property

	Public Property Get Id()
		Id = oHiddenField.Id
	End Property
	Public Property Let Id(input)
		oHiddenField.Id = input
	End Property

	Public Sub CreateValueObject()
		IF oValue IS NOTHING THEN
			Set oValue = new VariantValue
			'oContent.AddItem oValue
		END IF
	End Sub
	
	Public Property Get Value()
		Set Value = oValue
	End Property
	Public Property Let Value(input)
		IF IsObject(Input) THEN
			Set oValue = input
		ELSE 
			CreateValueObject()
			oValue.Value = input
		END IF
	End Property

	Public Property Get Text()
		Text = oValue.Text
	End Property
	Public Property Let Text(input)
		IF IsObject(Input) THEN
			Set oValue = input
		ELSE 
			CreateValueObject()
			oValue.Text = input
		END IF
	End Property

	Public Property Get CssClass()
		CssClass = oHiddenField.CssClass
	End Property
	Public Property Let CssClass(input)
		oHiddenField.CssClass = input
	End Property

	'Input Properties 
	'Public methods
	Public Function GetCode()
		CreateValueObject()
		oHiddenField.Properties.SetProperty "value", oValue.Value
		GetCode=oHiddenField.GetCode()
	End Function

	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub
End Class 
 %>