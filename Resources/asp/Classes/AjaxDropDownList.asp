<% 
'Original Source from http://www.daniweb.com/forums/printthread19997.html
Class AjaxDropDownList
	'Properties
	Private sTextValue, sDataValue, bOptionChoose, bOptionAll, bTurnOn, sPromptText, sLoadingText, sParentControlId, sFilters, sParameters 
	'Settings
	Private sMode, bAllowInsert
	'Objects
	Private oRelatives, oValue
	
	'Member Variables
	Private aOptionElements
	Private sHTMLCode
	Private iRowIndex
	Private iRowCount
	Private sSelectItem
	Private iCounter
	
	Private returnString
	
	'Objects 	
	Private oDropDownList, oOption, dOptions, cParameters
	
	'Private variables
	Private bBuilt, sThisValue
	
	Public Property Get Object()
		Set Object = oDropDownList
	End Property
	
	Private Sub Class_Terminate()
		Set oDropDownList = nothing
		Set oValue = nothing
		Set cParameters = nothing
	End Sub

	Private Sub Class_Initialize()
'		Set oRelatives = [$Relatives](Me)
		Set oDropDownList = new DropDownList
		Set oValue = nothing
		'Me.Value=NULL
		oDropDownList.CssClass="catalogo"
		Set cParameters=new PropertiesCollection
		cParameters.PropertiesType="SQLParameters"
		Me.TurnOn=TRUE
		Me.AllowInsert=TRUE
	End Sub
	
	'Output objects
	Public Property Get Style()
		Set Style = oDropDownList.Style
	End Property
	
	Public Property Get Control()
		Set Control = oDropDownList
	End Property
	
	Public Property Get ParametersCollection()
		Set ParametersCollection = cParameters
	End Property

	Public Property Get Properties()
		Set Properties = oDropDownList.Properties
	End Property

	Public Property Get Relatives()
		Set Relatives = oRelatives
	End Property
	
	Public Property Get Container()
		Set Container = oRelatives.Parent
	End Property
	Public Property Let Container(input)
		oRelatives.Parent = input
	End Property

	'Interfaces
	Public Property Get Methods()
		Set Methods = oDropDownList.Methods
	End Property
	
	'Properties
	Public Property Get Visible()
		Visible = oDropDownList.Visible
	End Property
	Public Property Let Visible(input)
		oDropDownList.Visible = input
	End Property
	
	
	Public Property Get AllowInsert()
		AllowInsert = bAllowInsert
	End Property
	Public Property Let AllowInsert(input)
		bAllowInsert = input
		SetCommandAllow()
	End Property

	Public Sub SetCommandAllow()
		DIM sKeyDown
		IF bAllowInsert AND Me.Catalog<>"" THEN
			sKeyDown="modalOperations(this)"
		ELSE
			sKeyDown=""
		END IF
		oDropDownList.Methods.OnKeyDown=sKeyDown
	End Sub
	
	Public Property Get Name()
		Name = oDropDownList.Name
	End Property
	Public Property Let Name(input)
		oDropDownList.Name = input
	End Property

	Public Property Get Catalog()
		Catalog = oDropDownList.Catalog
	End Property
	Public Property Let Catalog(input)
		oDropDownList.Catalog=input
		SetCommandAllow()
	End Property

	Public Property Get CssClass()
		CssClass = oDropDownList.CssClass
	End Property
'	Public Property Let CssClass(input)
'		oDropDownList.CssClass=input
'	End Property

	Public Property Get Text()
		Text = oDropDownList.Text
	End Property

	Public Sub CreateValueObject()
		IF oValue IS NOTHING THEN
			Set oValue = new VariantValue
		END IF
	End Sub
	
	Public Property Get Value()
		Value = oDropDownList.Value
	End Property
	Public Property Let Value(input)
		IF IsNullOrEmpty(input) THEN Exit Property
		IF IsObject(Input) THEN
			Set oValue = input
		ELSE 
			CreateValueObject()
			oValue.Value = input
		END IF

'		oDropDownList.RemoveItems()
'		IF IsNullOrEmpty(input) THEN input="NULL@:@Sin Seleccionar..."
'		IF NOT INSTR(input, "@:@")<>0 THEN 
'			input=input&"@:@"&input
''			Err.Raise 13 'type mismatch
'		END IF
'		DIM aInfo: aInfo=split(input, "@:@")
'		DIM sValue: sValue=aInfo(0)
'		DIM sText: sText=aInfo(1)
'		oDropDownList.NewOption sValue, sText
		oDropDownList.Value=oValue
	End Property
	
	Public Property Get SelectItem()
		SelectItem = oDropDownList.SelectItem
	End Property
	Public Property Let SelectItem(input)
		oDropDownList.SelectItem = input
	End Property
	
	Public Property Let Id(input)
		oDropDownList.Id = input
	End Property
	
	'Output properties
	Public Property Get IsReadonly()
		IsReadonly = oDropDownList.IsReadonly
	End Property
	Public Property Let IsReadonly(input)
		oDropDownList.IsReadonly = input
	End Property

	Public Property Get Parameters()
		Parameters = sParameters
	End Property
	Public Property Let Parameters(input)
		sParameters = input
		oDropDownList.Properties.SetProperty "parameters", input
	End Property


	Public Property Get Filters()
		Filters = sFilters
	End Property
	Public Property Let Filters(input)
		sFilters = input
		oDropDownList.Properties.SetProperty "filtros", input
	End Property


	Public Property Get PromptText()
		PromptText = sPromptText
	End Property
	Public Property Let PromptText(input)
		sPromptText = input
	End Property


	Public Property Get LoadingText()
		LoadingText = sLoadingText
	End Property
	Public Property Let LoadingText(input)
		sLoadingText = input
	End Property


	Public Property Get ParentControlId()
		ParentControlId = sParentControlId
	End Property
	Public Property Let ParentControlId(input)
		sParentControlId = input
		IF input<>"" THEN 
			oDropDownList.Properties.SetProperty "dependencias", "["&input&"]"
		END IF
	End Property


	Public Property Get TextValue()
		TextValue = sTextValue
	End Property
	Public Property Let TextValue(input)
		sTextValue=input
		oDropDownList.Properties.SetProperty "textValue", RTRIM(input)
	End Property


	Public Property Get DataValue()
		DataValue = sDataValue
	End Property
	Public Property Let DataValue(input)
		sDataValue=input
		oDropDownList.Properties.SetProperty "dataValue", RTRIM(input)
	End Property


	Public Property Get OptionChoose()
		OptionChoose = bOptionChoose
	End Property
	Public Property Let OptionChoose(input)
		bOptionChoose=input
		oDropDownList.Properties.SetProperty "opt_choose", dMainDictionary("javascript_"&dMainDictionary(LCASE(CSTR(CBOOL(input)))))
	End Property


	Public Property Get OptionAll()
		OptionAll = bOptionAll
	End Property
	Public Property Let OptionAll(input)
		bOptionAll = input
		oDropDownList.Properties.SetProperty "opt_all", dMainDictionary("javascript_"&dMainDictionary(LCASE(CSTR(CBOOL(input)))))
	End Property


	Public Property Get TurnOn()
		TurnOn = bTurnOn
	End Property
	Public Property Let TurnOn(input)
		bTurnOn = input
		oDropDownList.Properties.SetProperty "turnOn", dMainDictionary("javascript_"&dMainDictionary(LCASE(CSTR(CBOOL(input)))))
	End Property


	Public Property Let InfoString(input)
		DIM aInfo: aInfo=split(input, "-")
		Me.Catalog=aInfo(0)
		Me.DataValue=aInfo(1)
		Me.Name=aInfo(2)
	End Property

	'Input Properties

	'Public methods
	Public Sub ResetProperties()
		oDropDownList.ResetProperties()
	End Sub

	Public Sub ClearItems()
		oDropDownList.ClearItems()
	End Sub

	Public Sub ClearSelections()
		oDropDownList.ClearSelections()
	End Sub
	
	Public Sub FillCombo(ByRef rs)
		oDropDownList.FillCombo(rs)
	End Sub
	
	Public Sub FillComboWithArray(ByRef elements)
		oDropDownList.FillComboWithArray(elements)
	End Sub
	
	Public Function NewOption(ByVal sValue, ByVal sText)
		Set NewOption = oDropDownList.NewOption(sValue, sText)
	End Function
	
'	Public Sub Build()
'		oDropDownList.Build()
'	End Sub
	
	Public Function GetCode()
		Me.Parameters=Me.ParametersCollection.GetCode()
		DIM sCurrentValue:	sCurrentValue=NullIfEmptyOrNullText(oDropDownList.Value)
		IF NOT(oDropDownList.Tag.Content.NamedItems.Exists(sCurrentValue)) THEN
			oDropDownList.RemoveItems()
			DIM sText
			IF IsNull(sCurrentValue) THEN 
				sCurrentValue=NullToText(oDropDownList.Value)
				sText="Ver Opciones..."
			ELSE
				sText=oDropDownList.Text
			END IF
			oDropDownList.NewOption sCurrentValue, sText
		END IF
		GetCode=oDropDownList.GetCode()
	End Function
	
	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub
End Class 
 %>