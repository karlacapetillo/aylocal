<% 
Function [$Dictionary](ByRef aObjects)
	DIM oDictionary:	Set oDictionary=[&New]("Dictionary", "")
	IF IsEmpty(aObjects) THEN
		
	ELSEIF IsArray(aObjects) THEN 
		oDictionary.AddAt aObjects, 0
	ELSEIF IsObject(aObjects) THEN
		DIM oElement
		FOR EACH oElement IN aObjects
			oDictionary.Add oElement
		NEXT
	END IF
	Set [$Dictionary]=oDictionary
End Function
Class Dictionary
	Private oDictionary, bCheckValidName
	
	Private Sub Class_Terminate()
		Set oDictionary = nothing
	End Sub
	Private Sub Class_Initialize()
		Set oDictionary = server.CreateObject("Scripting.Dictionary")
	End Sub
	
	Public Property Get Keys()
		Set Keys = [$ArrayList](oDictionary.Keys)
	End Property

	Public Property Get Items()
		Items = oDictionary.Items
	End Property

	Public Property Get Count()
		Count = oDictionary.Count
	End Property

	Public Property Get Item(sPosition)
		DIM oItems:	oItems=oDictionary.Items
		IF IsObject(oItems(sPosition)) THEN
			Set Item=oItems(sPosition)
		ELSE
			Item=oItems(sPosition)
		END IF
	End Property
	
	Public Property Get CheckValidName()
		CheckValidName = bCheckValidName
	End Property
	Public Property Let CheckValidName(input)
		bCheckValidName = input
	End Property

	Public Function Exists(sElementName)
		DIM vResult
		vResult=oDictionary.Exists(sElementName)
		Assign Exists, vResult
	End Function
	
	Public Default Property Get NamedItem(ByVal sItemName)
		IF bCheckValidName AND NOT oDictionary.Exists(sItemName) THEN
			response.write "<strong>"&sItemName&" is not available. Valid names are: </strong><br>"&Me.ValidNames
			response.end
			Exit Property
		ELSE
			IF IsObject(oDictionary(sItemName)) THEN
				Set NamedItem = oDictionary(sItemName)
			ELSE
				NamedItem = oDictionary(sItemName)
			END IF
		END IF
	End Property

	Public Property Get ValidNames()
		DIM sKeyName, sValidNames
		sValidNames=""
		FOR EACH sKeyName IN oDictionary
			sValidNames=sValidNames&" ["&sKeyName&"]"
		NEXT
		ValidNames = TRIM(sValidNames)
	End Property

	Public Function Add(ByVal sItemName, ByRef oElement)
		IF IsObject(oElement) THEN 
			Set oDictionary(sItemName) = oElement
			ON ERROR  RESUME NEXT:
			Set Add=oDictionary(sItemName): [&Catch] FALSE, TypeName(Me)&".Add", TypeName(oDictionary(sItemName))
			ON ERROR  GOTO 0
		ELSE
			oDictionary(sItemName) = oElement
			ON ERROR  RESUME NEXT:
			Add=oDictionary(sItemName): [&Catch] FALSE, TypeName(Me)&".Add", TypeName(oDictionary(sItemName))
			ON ERROR  GOTO 0
		END IF
	End Function

	Public Sub Clear()
		oDictionary.RemoveAll()
	End Sub
End Class
 %>