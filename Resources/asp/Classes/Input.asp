<% 
Class Input
	'Member Variables
	Private sName, sId, sCssClass, sValue, sText, iSize, iMaxLength, bIsReadonly, sMaxValue, sMinValue, sInputType

	'Objects
	Private oTag
	
	Public Property Get Object()
		Set Object = oTag
	End Property
	
	Private Sub Class_Initialize()
		Me.InputType = "text"
		Set oTag = nothing
	End Sub
	
	'Interfaces
	Public Property Get Methods()
		Set Methods = oTag.Methods
	End Property
	
	Public Property Get Properties()
		Set Properties = oTag.Properties
	End Property

	Public Property Get Style()
		Set Style = oTag.Style
	End Property
	
	'Output Properties 
	Public Property Get InputType()
		InputType = sInputType
	End Property
	Public Property Let InputType(input)
		sInputType = input
		IF Me.IsReadonly AND UCASE(sInputType)=UCASE("Checkbox") THEN
			Set oTag = new Image
			oTag.ImageURL = sCalendarReadonlyImagePath
			oTag.Height=10
			oTag.Width=10
			oTag.HasBorder=TRUE
		ELSE
			Set oTag = new Tag
			SELECT CASE UCASE(sInputType)
			CASE UCASE("Text"), UCASE("Password"), UCASE("Checkbox"), UCASE("Hidden")
				oTag.TagName="Input"
				oTag.Properties.SetProperty "type", sInputType
				oTag.HasClosingTag=FALSE
			CASE UCASE("TextArea")
				Set oTag = new TextArea
	'		CASE ELSE
			END SELECT
		Me.Id=sId
		Me.CssClass=sCssClass
		END IF
	End Property

	Public Property Get Visible()
		Visible = oTag.Visible
	End Property
	Public Property Let Visible(input)
		oTag.Visible = input
	End Property


	Public Property Get MinValue()
		MinValue = sMinValue
	End Property
	Public Property Let MinValue(input)
		sMinValue = input
		oTag.Properties.SetProperty "MinValue", sMinValue
	End Property


	Public Property Get MaxValue()
		MaxValue = sMaxValue
	End Property
	Public Property Let MaxValue(input)
		sMaxValue = input
		oTag.Properties.SetProperty "MaxValue", sMaxValue
	End Property


	Public Property Get Name()
		Name = oTag.Properties.GetProperty("name")
	End Property
	Public Property Let Name(input)
		oTag.Properties.SetProperty "name", input
	End Property


	Public Property Get Id()
		Id = sId
	End Property
	Public Property Let Id(input)
		sId = input
		IF NOT oTag IS NOTHING THEN oTag.Id=sId
	End Property


	Public Property Get CssClass()
		CssClass = sCssClass
	End Property
	Public Property Let CssClass(input)
		sCssClass = input
		IF NOT oTag IS NOTHING THEN oTag.CssClass=sCssClass
	End Property


	Public Property Get Text()
		SELECT CASE UCASE(sInputType)
		CASE UCASE("Checkbox")
			Text = sText
		CASE ELSE
			Text = Me.Value
		END SELECT
	End Property
	Public Property Let Text(input)
		SELECT CASE UCASE(sInputType)
		CASE UCASE("Checkbox")
			sText = input
		CASE ELSE
			Me.Value = input
		END SELECT
	End Property

	Public Property Get Value()
		Value = sValue
	End Property
	Public Property Let Value(input)
		sValue = input
		response.write sInputType&": "&input&"<br>"
		IF sValue="" THEN 
			sValue=empty
		END IF
		SELECT CASE UCASE(TypeName(oTag))
		CASE "LABEL"
			IF UCASE(sInputType)="PASSWORD" THEN
				oTag.Text="**********"
			ELSE
				oTag.Text=sValue
			END IF
		CASE "IMAGE"
			oTag.Properties.SetProperty "alt", sValue
		CASE ELSE
			oTag.Properties.SetProperty "value", sValue
		END SELECT
	End Property


	Public Property Get Size()
		Size = iSize
	End Property
	Public Property Let Size(input)
		iSize = input
		oTag.Properties.SetProperty "size", iSize
	End Property


	Public Property Get MaxLength()
		MaxLength = iMaxLength
	End Property
	Public Property Let MaxLength(input)
		iMaxLength = input
		oTag.Properties.SetProperty "MaxLength", iMaxLength
	End Property


	Public Property Get IsReadonly()
		IsReadonly = bIsReadonly
	End Property
	Public Property Let IsReadonly(input)
		bIsReadonly = CBOOL(input)
	End Property


	'Public methods
	Public Function GetCode()
		GetCode= oTag.GetCode()
	End Function
	
	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub
End Class
%>