<% 
Class RangeSelector
	'Properties
	Private iSteps, iValue
	
	'Objects 
	Private oImageCollection, oTextBox, oContainer, oRelatives
	
	Public Property Get Object()
		Set Object = oTextBox.Object
	End Property

	Private Sub Class_Initialize()
		Set oRelatives = [$Relatives](Me)
		Set oTextBox = new HiddenField
		oTextBox.CssClass="numero"
		oTextBox.Methods.OnPropertyChange="if (event.propertyName=='value' && document.activeElement!=this) {this.parentNode.getElementsByTagName('IMG')(0).fireEvent('onmouseout')}"
		Set oImageCollection = new Collection
	End Sub

	'Output objects
	Public Property Get Style()
		Set Style = oImageCollection.Style
	End Property
	
	Public Property Get Properties()
		Set Properties = oTextBox.Properties
	End Property

	Public Property Get Methods()
		Set Methods = oImageCollection.Methods
	End Property

	'Output Interfaces 
	Public Property Get Name()
		Name = oTextBox.Name
	End Property

	Public Property Get Relatives()
		Set Relatives = oRelatives
	End Property
	
	Public Property Get Container()
		Set Container = oRelatives.Parent
	End Property
	Public Property Let Container(input)
		oRelatives.Parent = input
	End Property

	Public Property Get ParentCell()
		ParentCell = oImageCollection.ParentCell
	End Property

	Public Property Get Id()
		Id = oImageCollection.Id
	End Property
	
	Public Property Get ImageURL()
'		ImageURL = oImageCollection.ImageURL
	End Property
	
	Public Property Get CssClass()
		CssClass = oImageCollection.CssClass
	End Property
	
	'Input Interfaces
	Public Property Let Name(input)
		oTextBox.Name = input
	End Property

	Public Property Let ParentCell(input)
		oImageCollection.ParentCell = input
	End Property

	Public Property Let HasBorder(input)
		oImageCollection.ApplyProperty "HasBorder", input
	End Property

	Public Property Let Width(input)
		oImageCollection.ApplyProperty "Width", input
	End Property

	Public Property Let Height(input)
		oImageCollection.ApplyProperty "Height", input
	End Property

	Public Property Let AlternateText(input)
		oImageCollection.ApplyProperty "AlternateText", input
	End Property
	
	Public Property Let Id(input)
		oImageCollection.Id = input
	End Property
	
	Public Property Let ImageURL(input)
		oImageCollection.ApplyProperty "ImageURL", input
	End Property

	Public Property Let CssClass(input)
		oImageCollection.CssClass = input
	End Property
	
	'Output Properties 
	Public Property Get Value()
		Value = oTextBox.Value
	End Property

	Public Property Get MinValue()
		MinValue = oTextBox.MinValue
	End Property

	Public Property Get MaxValue()
		MaxValue = oTextBox.MaxValue
	End Property

	Public Property Get CommandText()
		CommandText = sCommandText
	End Property

	Public Property Get Steps()
		Steps = iSteps
	End Property
	
	'Input Properties 
	Public Property Let Value(input)
		oTextBox.Value = input
		DIM oItem, i, iCompareValue
		IF input="" THEN 
			iCompareValue=0
		ELSE
			iCompareValue=input
		END IF
		FOR EACH oItem IN oImageCollection.GetItems
			i=i+1
			IF i<=CINT(iCompareValue) THEN
				oItem.Disabled=FALSE
			ELSE
				oItem.Disabled=TRUE
			END IF
		NEXT
	End Property

	Public Property Let MinValue(input)
'		response.write "Me.MinValue: "&Me.MinValue&"<br>"
		oTextBox.MinValue=input
		Me.CreateCollection()
	End Property

	Public Property Let MaxValue(input)
'		response.write "Me.MaxValue: "&Me.MaxValue&"<br>"
		oTextBox.MaxValue=input
		Me.CreateCollection()
	End Property

	Public Property Let CommandText(input)
		sCommandText = input
'		oImageCollection.Methods.OnClick = sCommandText
	End Property

	Public Property Let Steps(input)
		iSteps = input
	End Property
	
	'Public methods
	Public Sub CreateCollection()
'		response.write Me.MinValue&" vs. "&Me.MaxValue
		IF Me.MinValue="" OR Me.MaxValue="" THEN EXIT Sub
		DIM i, oImageButton
		FOR i=Me.MinValue TO Me.MaxValue
			Set oImageButton = new ImageButton
			oImageButton.Width=22
			oImageButton.Height=20
			oImageButton.ImageURL=sImageLibraryPath & "FilledStar.png"
			oImageButton.Methods.OnMouseOver="cllImg=this.parentNode.getElementsByTagName('IMG'); for (i=0; i<cllImg.length; ++i) {cllImg(i).enabled=(i<="&i-1&")}"
			oImageButton.Methods.OnMouseOut="cllImg=this.parentNode.getElementsByTagName('IMG'); for (i=0; i<cllImg.length; ++i) {cllImg(i).enabled=(i<getVal(this.parentNode.getElementsByTagName('INPUT')(0)))}"
			oImageButton.CommandText="this.parentNode.getElementsByTagName('INPUT')(0).value="&i
			oImageCollection.AddContent(oImageButton)
		NEXT
		oImageCollection.Properties.SetProperty "standByOpacity", 90
	End Sub
	
	Public Function GetCode()
		GetCode=oTextBox.GetCode()&" "&oImageCollection.GetCode()
	End Function

	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub
End Class 
 %>