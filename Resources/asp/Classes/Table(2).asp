<%
Class Table
	'Properties
	Private sId, sMode, bSupportsPaging, nTableIdentifier
	
	'Settings
	Private sFieldBlank
	
	'Objects
	Private oTable, oHeaders, oTableBodies, oTableBody, oFooters, oDisplayTime, dRowGroups, oHeadersCollection, oRelatives, oProgress, oRows
	
	'Private Properties
	Private sTableCode
	
	Private Sub Class_Terminate()
		Set oRelatives = nothing
		Set oRows = nothing
		Set oDisplayTime = nothing
		Set oTable = nothing
		Set oHeaders = nothing
		Set oTableBodies = nothing
		Set oTableBody = nothing
		Set oFooters = nothing 
		Set dRowGroups = nothing
		Set oProgress = nothing
	End Sub
	
	Private Sub Class_Initialize()
		Me.TableIdentifier = RandomNumber(1000)
		Set oProgress = new Progress
'		oProgress.ProgressBarControlId=""
		Set oDisplayTime = new StopWatch
		Set oRelatives = new Family
		oRelatives.InitFamily(Me)
'		Set oRows = oRelatives.Children
		Set oTable = new Tag
		oTable.TagName="table"
		Set oHeaders = new Tag
		oHeaders.TagName="THEAD"
		Set oFooters = new Tag
		oFooters.TagName="TFOOT"
		Set oTableBodies = new Collection
		Set dRowGroups=server.CreateObject("Scripting.Dictionary")
		Set dRowGroups("_HEADERS_") = new Collection
		oHeaders.AddContent(dRowGroups("_HEADERS_"))
		Set dRowGroups("_FOOTERS_") = new Collection
		oFooters.AddContent(dRowGroups("_FOOTERS_"))
	End Sub
    
	'Interfaces
	Public Property Get Relatives()
		Set Relatives = oRelatives
	End Property
	
	Public Property Get Style()
		Set Style = oTable.Style
	End Property

	Public Property Get Properties()
		Set Properties = oTable.Properties
	End Property

	Public Property Get ProgressBar()
		Set ProgressBar = oProgress
	End Property

	Public Property Get Relatives()
		Set Relatives = oRelatives
	End Property
	
	Public Property Get Container()
		Set Container = Me.Relatives.Parent
	End Property
	Public Property Let Container(input)
		Set oContainer = input
	End Property

	'Properties
	Public Property Get TableIdentifier()
		TableIdentifier = nTableIdentifier
	End Property
	Public Property Let TableIdentifier(input)
		nTableIdentifier = input
	End Property

	Public Property Get IsMainTable()
		IsMainTable = NOT(Me.Container IS nothing)
	End Property

	Public Property Get Mode()
		Mode = sMode
	End Property
	Public Property Let Mode(input)
		sMode = input
	End Property


	Public Property Get Id()
		Id = oTable.Id
	End Property
	Public Property Let Id(input)
		oTable.Id = input
	End Property


	Public Property Get CssClass()
		CssClass = oTable.CssClass
	End Property
	Public Property Let CssClass(input)
		oTable.CssClass = input
	End Property


	Public Property Get Render()
		Render = oTable.Render
	End Property
	Public Property Let Render(input)
		oTable.Render = input
	End Property


	Public Property Get FieldBlank()
		FieldBlank = sFieldBlank
	End Property
	Public Property Let FieldBlank(input)
		sFieldBlank = input
	End Property


	'Methods
	Public Sub AddRow(ByRef oRow)
		IF oRow.IsHeader THEN
			dRowGroups("_HEADERS_").AddItem(oRow)
		END IF
		IF oRow.IsFooter THEN
			dRowGroups("_FOOTERS_").AddItem(oRow)
		END IF
		IF NOT(oRow.IsHeader OR oRow.IsFooter) THEN
			DIM sRowGroupName
			sRowGroupName=oRow.Object.GroupName
			IF sRowGroupName<>"" THEN
				IF NOT dRowGroups.exists(sRowGroupName) THEN
					Set dRowGroups(UCASE(sRowGroupName)) = new Collection
					oTableBody.AddContent(dRowGroups(UCASE(sRowGroupName)))
				END IF
				dRowGroups(UCASE(sRowGroupName)).AddItem(oRow)
			ELSE
				Set oTableBody = new Tag
				oTableBody.TagName="TBODY"
				oTableBody.AddItem oTableBody, "_BODY_"
				oTableBodies.AddNamedItem oRow, "_BODY_"
				oTableBodies("_BODY_").AddItem(oTableBody)
			END IF
		END IF
	End Sub

	Public Function GetCode()
		oTable.Id="dataTable"
		IF sTableCode="" THEN
			IF Me.ProgressBar.ProgressBarControlId<>"" THEN 
				Me.ProgressBar.Parts=10
				Me.ProgressBar.ClassName="Loading"
				Me.ProgressBar.ClearProgress()
				Me.ProgressBar.TotalRecords=iMaxRowPosition
			END IF
			oTable.AddContent(oHeaders)
			oTable.AddContent(oTableBody)
			oTable.AddContent(oFooters)
			sTableCode=oTable.GetCode()
		END IF
		GetCode=sTableCode
	End Function
	
	Public Sub WriteCode()
    	oDisplayTime.StartTimer()
		response.write Me.GetCode()
		oDisplayTime.StopTimer()
	End Sub
	
	Public Sub ShowStatistics()
		response.write "<center>"
		response.write "********* Statistics for table "&Me.Id&" ************* " &"<br>"
'		response.write "Open Connection: "&ROUND(oDataSource.TimeOpenConn, 3) &"<br>"
'		response.write "Querying: "&ROUND(oDataSource.TimeQuery, 3) &"<br>"
'		response.write "Fetching: "&ROUND(tElapsedFetch, 3) &"<br>"
'		response.write "Formating: "& ROUND(tStartDisplay-tEndFetch, 3) &"<br>"
		response.write "Displaying: "&ROUND(oDisplayTime.TotalTime, 3) &"<br>"
		response.write "Total: "&ROUND(oDisplayTime.TotalTime, 3)&"<br>"
		response.write "**************************************** " &"<br><br>"
		response.write "</center>"
	End Sub
End Class
%>