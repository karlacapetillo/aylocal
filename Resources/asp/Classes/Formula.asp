<% 
Function [$Formula](ByVal sParameters, ByRef sFormula)
	DIM oFormula:	Set oFormula=[&New]("Formula", sParameters)
	oFormula.Formula=sFormula
	Set [$Formula]=oFormula
End Function
Class Formula
	'Properties
	Private sFormat
	
	'Objects
	Private oLabel, oRelatives, dFormats
	
	'Required properties
	Public Property Get Relatives()
		Set Relatives = oRelatives
	End Property
	
	Public Property Get Container()
		Set Container = oRelatives.Parent
	End Property
	Public Property Let Container(input)
		oRelatives.Parent = input
	End Property

	Public Property Get Visible()
		Visible = oLabel.Visible
	End Property
	Public Property Let Visible(input)
		oLabel.Visible = input
	End Property

	Public Property Get Object()
		Set Object = oLabel
	End Property

	Private Sub Class_Terminate()
	End Sub
	Private Sub Class_Initialize()
		Set oRelatives = [$Relatives](Me)
		Set oLabel = new label
		oLabel.CssClass="formula"
		oLabel.Text="- -"
		Set dFormats = server.CreateObject("Scripting.Dictionary")
		dFormats("MONEDA")	= "moneda"
		dFormats("MONEY")	= "moneda"
		dFormats("PORCENTAJE")	= "porcentaje"
		dFormats("PERCENT")	= "porcentaje"
	End Sub

	'Interfaces
	Public Property Get Style()
		Set Style = oLabel.Style
	End Property

	Public Property Get Id()
		Id = oLabel.Id
	End Property
	Public Property Let Id(input)
		oLabel.Id = input
	End Property

	Public Property Get TurnOn()
		TurnOn = TRIM(oLabel.Properties.GetProperty("turnOn"))
	End Property
	Public Property Let TurnOn(input)
		oLabel.Properties.SetProperty "turnOn", CBoolean(input).Text
	End Property

	Public Property Get Value()
		Value = oLabel.Value
	End Property

	Public Property Get Text()
		Text = oLabel.Text
	End Property
	Public Property Let Text(input)
		oLabel.Text = input
		IF TypeName(input)="DataValue" THEN input.DecimalPositions=Me.DecimalPositions
	End Property

	Public Property Get Formula()
		Formula = TRIM(oLabel.Properties.GetProperty("formula"))
	End Property
	Public Property Let Formula(input)
		oLabel.Properties.SetProperty "formula", input
	End Property

	Public Property Get Format()
		Format = sFormat
	End Property
	Public Property Let Format(input)
		sFormat = TRIM(input)
		IF Session("debugging") THEN IF NOT dFormats.Exists(UCASE(sFormat)) THEN response.write "<strong class=""warning"">WARNING!! ("&TypeName(Me)&")</strong> "&sFormat&" format is not defined<br>"
		oLabel.Properties.SetProperty "formato", dFormats(UCASE(sFormat)) 'dFormats(sFormat)
	End Property
	
	Public Property Get DecimalPositions()
		DecimalPositions=oLabel.Properties.GetProperty("decimalPositions")
	End Property
	Public Property Let DecimalPositions(input)
		oLabel.Properties.SetProperty "decimalPositions", input
		IF TypeName(oLabel.Value)="DataValue" THEN 
			oLabel.Value.DecimalPositions=Me.DecimalPositions
		END IF
	End Property

	'Methods
	Public Function GetCode()
		DIM sFormula: sFormula=Me.Formula
		IF IsNullOrEmpty(sFormula) THEN 
			response.write "Formula property hasn't been set yet."
			response.end
		END IF
		GetCode=oLabel.GetCode()
	End Function
	
	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub
End Class
 %>