<% 
Class MoneyRange
	'Properties
'	Private iSteps, iValue
	
	'Objects 
	Private oMoneyBox, oMoneyBox2, oDiv
	
	'Private variables
	Private sDateBoxSeparator
	
	Public Property Get Object()
		Set Object = oMoneyBox.Object
	End Property

	Public Property Get Visible()
		Visible = oDiv.Visible
	End Property
	Public Property Let Visible(input)
		oDiv.Visible = input
	End Property

	Private Sub Class_Initialize()
		Set oDiv = new Div
		sDateBoxSeparator=" a "
		Set oMoneyBox = new TextBox
		oMoneyBox.Size="14"
		oMoneyBox.MaxLength="14"
		oMoneyBox.CssClass="moneda"
		oMoneyBox.Object.Properties.SetProperty "valuePrefix", "'>='"
		Set oMoneyBox2 = new TextBox
		oMoneyBox2.Size="14"
		oMoneyBox2.MaxLength="14"
		oMoneyBox2.CssClass="moneda"
		oMoneyBox2.Object.Properties.SetProperty "valuePrefix", "'<='"
		oDiv.AddContent(oMoneyBox)
		oDiv.AddContent(sDateBoxSeparator)
		oDiv.AddContent(oMoneyBox2)
	End Sub

	'Output objects
	Public Property Get Style()
		Set Style = oMoneyBox.Style
	End Property
	
	Public Property Get Methods()
		Set Methods = oMoneyBox.Methods
	End Property

	'Output Interfaces 
	Public Property Get Name()
		Name = oMoneyBox.Name
	End Property

	Public Property Get Id()
		Id = oMoneyBox.Id
	End Property
	
	Public Property Get CssClass()
		CssClass = oMoneyBox.CssClass
	End Property
	
	'Input Interfaces
	Public Property Let Name(input)
		oMoneyBox.Name = input
		oMoneyBox2.Name = input
	End Property
	
	Public Property Let Id(input)
		oMoneyBox.Id = input
	End Property
	
	Public Property Let CssClass(input)
		oMoneyBox.CssClass = input
	End Property
	
	'Output Properties 
	Public Property Get Value()
		Value = oMoneyBox.Value
	End Property
	Public Property Let Value(input)
		oMoneyBox.Value = input
		oMoneyBox2.Value = input
	End Property

	Public Property Let MinValue(input)
		oMoneyBox.Value=input
	End Property
	Public Property Get MinValue()
		MinValue = dMinValue
	End Property

	Public Property Let MaxValue(input)
'		response.write "Me.MaxValue: "&Me.MaxValue&"<br>"
		oMoneyBox2.Value=input
	End Property
	Public Property Get MaxValue()
		MaxValue = dMaxValue
	End Property

	'Public methods
	Public Function GetCode()
		GetCode=oDiv.GetCode()
	End Function

	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub
End Class 
 %>