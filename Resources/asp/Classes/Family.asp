<% 
Function [$Family](ByRef oSource)
	DIM oFamily:	Set oFamily=[&New]("Family", "")
	oFamily.InitFamily(oSource)
	Set [$Family]=oFamily
End Function
Class Family
	Private oCaller, oParent, oPrevSibling, oNextSibling, oChildren
	Private oLastChild
	
	Private Sub Class_Initialize()
		Set oParent = nothing
		Set oLastChild = nothing
		Set oPrevSibling = nothing
		Set oNextSibling = nothing
	End Sub
	Private Sub Class_Terminate()
		Set oCaller = nothing
		Set oParent = nothing
		Set oPrevSibling = nothing
		Set oNextSibling = nothing
		Set oChildren = nothing
		Set oLastChild = nothing
	End Sub

	'Interfaces
	Public Sub InitFamily(ByRef oSource)
		Set oCaller = oSource
		IF UCASE(TypeName(oSource))=UCASE("Collection") THEN
			Set oChildren = nothing
		ELSE
			Set oChildren = new Collection
			oChildren.Container=oCaller
		END IF
	End Sub
	Public Property Get Caller()
		Set Caller = oCaller
	End Property

	Public Property Get Parent()
		Set Parent = oParent
	End Property
	Public Property Let Parent(input)
		Set oParent = input
	End Property


	Public Property Get PrevSibling()
		Set PrevSibling = oPrevSibling
	End Property
	Public Property Let PrevSibling(input)
		Set oPrevSibling = input
	End Property


	Public Property Get NextSibling()
		Set NextSibling = oNextSibling
	End Property
	Public Property Let NextSibling(input)
		Set oNextSibling = input
	End Property


	Public Property Get Children()
		Set Children = oChildren
	End Property
	Public Property Let Children(input)
		Set oChildren = input
	End Property

	'Methods
	Public Sub SetRelatives(ByRef oItem)
		oItem.Container=oCaller
		oItem.Relatives.PrevSibling=oLastChild
		IF Err.Number<>0 THEN response.write "Error in Family for "&TypeName(oItem)&": "&Err.Description: response.end
		IF NOT oLastChild IS nothing THEN
			oLastChild.Relatives.NextSibling=oItem
		END IF
		Set oLastChild=oItem
	End Sub

	Public Sub AddChildren(ByRef oItem)
		Me.SetRelatives(oItem)
		oChildren.AddItem(oItem)
	End sub
	
	Public Sub AddNamedChildren(ByRef oItem, ByVal sChildrenName)
		Me.SetRelatives(oItem)
		oChildren.AddNamedItem oItem, sChildrenName
	End sub
	
	Public Function GetParentOfType(sParentType)
		DIM oDirectParent
		Set oDirectParent=oCaller.Container
		DO UNTIL oDirectParent IS NOTHING OR UCASE(TypeName(oDirectParent))=UCASE(sParentType)
			Set oDirectParent=oDirectParent.Container
		LOOP
		Set GetParentOfType=oDirectParent
	End Function
	
	Public Function GetTopParent()
		DIM oDirectParent
		Set oDirectParent=oCaller.Container
		DO UNTIL oDirectParent.Container IS NOTHING
			Set oDirectParent=oDirectParent.Container
		LOOP
		Set GetTopParent=oDirectParent
	End Function
End Class
 %>