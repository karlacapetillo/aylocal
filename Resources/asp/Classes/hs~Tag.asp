<% 
Class Tag
	'Properties
	Private sTagName, sId, sCssClass', sOtherProperties
	
	'Settings
	Private bHasClosingTag, bRender, bVisible, sMode
	
	'Objects
	Private oStyle, oParentCell, oProperties, oMethods, oContainer, oRelatives, oContent
	
	'Private Variables
	Private iElementIndex, sReturnString, sKey, i
	
	Private Sub Class_Terminate()
		Set oRelatives = nothing
		Set oStyle = nothing
		Set oMethods = nothing
		Set oProperties = nothing
		Set oParentCell = nothing
		Set oContent = nothing
	End Sub
	
	Private Sub Class_Initialize()
		Set oRelatives = new Family
		oRelatives.InitFamily(Me)
		Set oContent = oRelatives.Children
		Set oStyle = new Styles
		Set oMethods = new MethodsCollection
		Set oProperties = new PropertiesCollection
		Set oParentCell = nothing
		Me.Render=TRUE
		Me.HasClosingTag=TRUE
	End Sub
	
	'Output objects
	Public Property Get Style()
		Set Style = oStyle
	End Property
	
	Public Property Get Methods()
		Set Methods = oMethods
	End Property
	
	Public Property Get Properties()
		Set Properties = oProperties
	End Property
	
	Public Property Get Attributes()
		Set Attributes = oProperties
	End Property
	
	Public Property Get Container()
		Set Container = oRelatives.Parent
	End Property
	Public Property Let Container(input)
		oRelatives.Parent = input
	End Property

	Public Property Get Relatives()
		Set Relatives = oRelatives
	End Property

	Public Property Get ParentCell()
		Set ParentCell = oParentCell
	End Property

	Public Property Get @Property()
		@Property = @Type@Property
	End Property
	Public Property Let @Property(input)
		@Type@Property = input
	End Property


	'Output Properties 
	Public Default Property Get NamedItem(ByVal sItemName)
		Set NamedItem=oContent.NamedItem(sItemName)
	End Property

	Public Property Get Render()
		Render = bRender
	End Property
	Public Property Let Render(input)
		bRender = input
	End Property

	Public Property Get Visible()
		Visible = bVisible
	End Property
	Public Property Let Visible(input)
		bVisible = input
		IF Me.Visible THEN 
			Me.Style.Display=""
		ELSE
			Me.Style.Display="none"
		END IF
	End Property

	Public Property Get TagName()
		TagName = sTagName
	End Property
	Public Property Let TagName(input)
		sTagName = input
		SELECT CASE UCASE(sTagName)
		CASE UCASE("input")
			Me.HasClosingTag=FALSE
		CASE ELSE
			Me.HasClosingTag=TRUE
		END SELECT
	End Property
	
	Public Property Get Id()
		Id = sId
	End Property
	Public Property Let Id(input)
		sId = input
		Me.Properties.SetProperty "id", input
	End Property
	
	Public Property Get CssClass()
		CssClass = oProperties.GetProperty("class")
	End Property
	Public Property Let CssClass(input)
		oProperties.SetProperty "class", input
	End Property
	
	Public Property Get Mode()
		Mode = sMode
	End Property
 	Public Property Let Mode(input)
		sMode = input
	End Property

	Public Property Get ItemsCount()
		ItemsCount = oContent.ItemsCount
	End Property

	'Input Properties 
	Public Property Let ParentCell(input)
		Set oParentCell = input
	End Property

	Public Property Let HasClosingTag(input)
		bHasClosingTag = input
	End Property
	
	'Public methods
	Public Sub SetStyle(ByRef oNewStyle)
		Set oStyle = oNewStyle
	End Sub

	Public Sub AddContent(ByRef oElement)
		oContent.AddItem(oElement)
	End Sub

	Public Sub AddNamedItem(ByRef oElement, ByVal sItemName)
		oContent.AddNamedItem oElement, sItemName
	End Sub
	
	Public Function ExistsItem(ByVal sItemName)
		ExistsItem=oContent.ExistsItem(sItemName)
	End Function

	Public Sub ClearContent()
		oContent.ClearContent()
	End Sub

	Public Function GetItems()
		GetItems=oContent.GetItems()
	End Function
	
	Public Function GetCode()
		IF NOT Me.Render THEN 
			GetCode=""
			EXIT Function
		END IF
		sReturnString=""
		IF NOT(TRIM(sTagName)<>"") THEN
			Err.Raise 1, "ASP 101", "TagName"&sErrPropertyNotSet
		END IF
		sReturnString=sReturnString& "<"&sTagName
		sReturnString=sReturnString& Me.Properties.GetCode()
		sReturnString=sReturnString& Me.Methods.GetCode()
		sReturnString=sReturnString& Me.Style.GetCode()
		sReturnString=sReturnString& ">" & vbCrLf
		sReturnString=sReturnString& oContent.GetCode()
		IF bHasClosingTag THEN sReturnString=sReturnString& "</"&sTagName&">" & vbCrLf
		GetCode=sReturnString
	End Function

	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub
End Class 
 %>