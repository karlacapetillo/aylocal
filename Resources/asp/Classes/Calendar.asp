<%
Class Calendar
	'Properties
	Private dStartDate, sTodayBorderColor, sTextColumn, sReferenceDateColumn, sLinkString, sLinkColumn, sSQLString, sTipoLista, sDBTableName, oRecordSet
	
	'Settings
	
	'Objects
	Private oInput
	
	'Private properties
	Private aWeekDays, aMonths, fDayWidth
	
	Public Property Get Object()
		Set Object = Me
	End Property

	Private Sub Class_Initialize()
		dStartDate=request.querystring("dStartDate")
		IF dStartDate="" THEN
			dStartDate=DATE
		END IF
		sTextColumn="Texto"
		sTodayBorderColor="red"
		aWeekDays=Array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "S�bado")
		aMonths=Array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre")
		fDayWidth="150"
		sLinkString=""
		sLinkColumn=""
		sTipoLista="ol"
		Set oRecordSet=new RecordSet
	End Sub

	Public Property Get TextColumn()
		TextColumn = sTextColumn
	End Property
	Public Property Let TextColumn(input)
		sTextColumn = input
	End Property

	Public Property Get DBTableName()
		DBTableName = sDBTableName
	End Property
	Public Property Let DBTableName(input)
		sDBTableName = input
	End Property

    Public Property Get DataSource()
		Set DataSource=oRecordSet
    End Property

	Public Function GetCode()
		DIM iDayMonth, dDateMonth, bDateEnabled, sTodayStyle, dFirstDayOfMonth, iFirstWeekDayOfMonth, sCurrentURL, sGoBackURL, sGoForwardURL, i, y, wd, s, oImgLArrow, oImgRArrow, iLastWeekDayOfMonth, dFirstDayOfNextMonth
		dFirstDayOfMonth=CDATE("1/"&MONTH(dStartDate)&"/"&YEAR(dStartDate))
		iFirstWeekDayOfMonth=DATEPART("w", dFirstDayOfMonth, 1, 2) 
		dFirstDayOfNextMonth=CDATE("1/"&MONTH(dStartDate)+1&"/"&YEAR(dStartDate))
		iLastWeekDayOfMonth=DATEPART("w", dFirstDayOfNextMonth, 1, 2)+7
		oRecordSet.SQLQueryString="SELECT * FROM (SELECT Fecha, Texto FROM "&Me.DBTableName&" WHERE "&Session("IdUsuario")&" IN (-1, 1,IdPromotor) ) AS SQLQuery WHERE Fecha>='"&dFirstDayOfMonth-iFirstWeekDayOfMonth+1&"' AND Fecha<'"&dFirstDayOfNextMonth-iLastWeekDayOfMonth&"'"
		oRecordSet.DataBind()
		oRecordSet.RecordSet.Filter="Fecha>='"&dFirstDayOfMonth&"'"
		sCurrentURL=Request.serverVariables("PATH_INFO")&"?"&Request.ServerVariables("QUERY_STRING") 
		sGoBackURL=updateURLString(sCurrentURL, "dStartDate", "1/"&MONTH(dFirstDayOfMonth-1)&"/"&YEAR(dFirstDayOfMonth-1))
		sGoForwardURL=updateURLString(sCurrentURL, "dStartDate", "1/"&MONTH(dFirstDayOfMonth+31)&"/"&YEAR(dFirstDayOfMonth+31))
		Set oImgLArrow=new ImageButton
		oImgLArrow.ImageURL=sImageLibraryPath & "btn_LArrow.png"
		oImgLArrow.Width=20
		oImgLArrow.Height=20
		oImgLArrow.AlternateText="VER MES ANTERIOR"
		oImgLArrow.CommandText="OpenLink(encodeURI('"&REPLACE(REPLACE(sGoBackURL, "'", "\'"), "\\'", "\'")&"'), false, true)"
		Set oImgRArrow=new ImageButton
		oImgRArrow.ImageURL=sImageLibraryPath & "btn_RArrow.png"
		oImgRArrow.Width=20
		oImgRArrow.Height=20
		oImgRArrow.AlternateText="VER MES SIGUIENTE"
		oImgRArrow.CommandText="OpenLink(encodeURI('"&REPLACE(REPLACE(sGoForwardURL, "'", "\'"), "\\'", "\'")&"'), false, true)"
		%>
		<TABLE width='100%' class='Calendario' cellpadding=0 cellspacing=0 border=1>
		<TR>
		<TH CLASS="MonthHeader" align="center" colspan="7"><% oImgLArrow.WriteCode() %>&nbsp;<select name="mes" onChange="location.replace(updateURLString(location.href, 'dStartDate', '1/'+this.value+'/<%= YEAR(dFirstDayOfMonth-1) %>'))">
	<% FOR i=1 TO 12 %>
	<option value="<%= i %>" <% IF MONTH(dStartDate)=i THEN %>selected<% END IF %>><%= aMonths(i-1) %></option>
	<% NEXT %>
	</select>&nbsp;<select name="anio" onChange="location.replace(updateURLString(location.href, 'dStartDate', '1/<%= MONTH(dStartDate) %>/'+this.value))">
	<% FOR y=YEAR(dStartDate)-5 TO YEAR(dStartDate)+5 %>
	<option value="<%= y %>" <% IF YEAR(dStartDate)=y THEN %>selected<% END IF %>><%= y %></option>
	<% NEXT %>
	</select>&nbsp;<% oImgRArrow.WriteCode() %>&nbsp;&nbsp;&nbsp;<b style="cursor:'hand';" onClick="location.replace(updateURLString(location.href, 'dStartDate', '<%= date %>'))">HOY</b> </TH>
		</TR>
		<TR><%
		FOR wd=0 TO 6 %>
			<TH CLASS="MonthHeader" align="center"> <%= LEFT(aWeekDays(wd), 3) %> </TH><%
		NEXT %>
		</TR><%
	  	FOR s=1 TO 6 %>
			<TR><%
			FOR wd=1 TO 7
				dDateMonth=dFirstDayOfMonth+(7*(s-1))+(wd-iFirstWeekDayOfMonth)
				bDateEnabled=(MONTH(dFirstDayOfMonth)=MONTH(dDateMonth))
				IF wd=1 AND NOT(MONTH(dDateMonth)=MONTH(dStartDate) OR MONTH(dDateMonth+6)=MONTH(dStartDate)) THEN
					EXIT FOR
				END IF
	'			IF MONTH(dDateMonth)=MONTH(dFirstDayOfMonth) THEN 
				iDayMonth=DAY(dDateMonth)
					IF dDateMonth=DATE THEN 
						sTodayStyle="border-color:'"&sTodayBorderColor&"'; border-style:'solid'; border-width:'3px';"
					ELSE
						sTodayStyle=""
					END IF
	'			END IF %>
				<TD valign="top" style="<%= sTodayStyle %><% IF NOT(bDateEnabled) THEN %>background:'silver'; color:'gray';<% END IF %>"><TABLE class="MonthDay" dia=<%= dDateMonth %> width="100%">
	<TR><TH style="<% IF MONTH(dFirstDayOfMonth)<>MONTH(dDateMonth) THEN %>background:'gray';<% END IF %>" CLASS="MonthDayHeader" align="left" width="14.286%"><%= iDayMonth %>
				<%= getData(oRecordSet, dDateMonth, sLinkString, sLinkColumn, bDateEnabled) %>
				</TH></TR>
				</TABLE></TD><%
			NEXT %>
			</TR><%
			IF wd=1 THEN
				EXIT FOR
			END IF
		NEXT %>
	</TABLE><%
	End Function

	Public FUNCTION getData(ByVal oTempRecordSet, currDay, sLinkString, sLinkColumn, bDateEnabled)
		DIM sSQL, rsRecordSet, count
		Set oTempRecordSet= new RecordSet
		oTempRecordSet.SQLQueryString="SELECT * FROM (SELECT Fecha, Texto FROM "&Me.DBTableName&" WHERE "&Session("IdUsuario")&" IN (-1, 1,IdPromotor)) AS SQLQuery WHERE Fecha>='"&currDay&"' AND Fecha<'"&CDATE(currDay)+1&"'"
		oTempRecordSet.DataBind()
		Set rsRecordSet=oTempRecordSet.RecordSet
		IF sReferenceDateColumn="" THEN sReferenceDateColumn="Fecha" END IF
'		rsRecordSet.Filter="Fecha>=#"&currDay&"# AND Fecha<#"&CDATE(currDay)+1&"#"
'		oTempRecordSet.SQLQueryString="SELECT Texto="&sTextColumn&", Fecha="&sReferenceDateColumn&" FROM ("&sSQLString&") AS SQLQuery WHERE "
'		oTempRecordSet.DataBind()
'		Set rsRecordSet = oTempRecordSet.RecordSet
'		rsRecordSet.open sSQLString, cn, 3, 1
		IF NOT(rsRecordSet.BOF AND rsRecordSet.EOF) THEN
			count=0 %>
	<%		DO UNTIL rsRecordSet.EOF 
				count=count+1 %>
			<TR><TD align="left" width="<%= fDayWidth %>" <% IF NOT(bDateEnabled) THEN %> style="color:'gray';"<% ELSE %><% IF sLinkString<>"" THEN %>onMouseOver="this.style.borderColor='orange';" onMouseOut="this.style.borderColor='white';" style="border:'solid 2px white'; cursor:'hand'; " onClick="location.href='<%= sLinkString %>'"<% END IF %><% END IF %> nowrap><% IF sTipoLista="ol" THEN %><b><%= count %>. </b><% ELSE %><li></li><% END IF %><%= TRIM(rsRecordSet("Texto")) %></TD></TR>
	<%		rsRecordSet.MoveNext
			LOOP
		END IF %>
			<TR><TD align="center" width="<%= fDayWidth %>" <% IF NOT(bDateEnabled) THEN %> style="color:'gray';"<% ELSE %><% IF sLinkString<>"" THEN %>onMouseOver="this.style.background='yellow'" onMouseOut="this.style.background=''" style="cursor:'hand'" onClick="location.href='<%= REPLACE(sLinkString, "'", "\'") %>0&fecha_calendario=<%= currDay %>' "<% END IF %><% END IF %>><br><br><br><br></TD></TR>
	<%
	END FUNCTION

	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub
End Class
%>