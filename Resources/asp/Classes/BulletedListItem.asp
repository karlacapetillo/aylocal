<% 
Class BulletedListItem
	'Properties
	Private sId, sText, sDisplayMode, sCommandText, sCssClass, aData

	'Settings
	
	'Objects
	Private oTag, oRelatives

	'Private variables
	Private sReturnString
	
	Public Property Get Visible()
		Visible = oTag.Visible
	End Property
	Public Property Let Visible(input)
		oTag.Visible = input
	End Property

	Private Sub Class_Initialize()
		Set oRelatives = [$Relatives](Me)
		Set oTag=new Tag
		oTag.TagName="LI"
		oTag.HasClosingTag=TRUE
	End Sub
	
	'Interfaces
	Public Property Get Relatives()
		Set Relatives = oRelatives
	End Property
	
	Public Property Get Container()
		Set Container = oRelatives.Parent
	End Property
	Public Property Let Container(input)
		oRelatives.Parent = input
	End Property

	'Output objects
	Public Property Get Style()
		Set Style = oTag.Style
	End Property
	
	Public Property Get Properties()
		Set Properties = oTag.Properties
	End Property

	'Output Properties 
	Public Property Get Data()
		Data = aData
	End Property

	Public Property Get CssClass()
		CssClass = oTag.CssClass
	End Property

	Public Property Get CommandText()
		CommandText = sCommandText
	End Property

	Public Property Get DisplayMode()
		DisplayMode = sDisplayMode
	End Property

	Public Property Get Id()
		Id = sId
	End Property
	
	Public Property Get Text()
		Text = sText
	End Property

	'Input Properties 
	Public Property Let Data(input)
		aData = input
	End Property

	Public Property Let CssClass(input)
		oTag.CssClass=input
	End Property

	Public Property Let CommandText(input)
		sCommandText = input
		IF sCommandText<>"" THEN
			Me.DisplayMode="HyperLink"
			oTag.Methods.OnClick=sCommandText
		ELSE
			oTag.Methods.OnClick="alert('Funcionalidad no implementada a�n. Consulte con el desarrollador.')"
			Me.DisplayMode="Text"
		END IF
	End Property

	Public Property Let DisplayMode(input)
		sDisplayMode = input
		SELECT CASE UCASE(sDisplayMode)
		CASE UCASE("Text")
		CASE UCASE("HyperLink")
			Me.Style.TextDecoration="underline"
			Me.Style.Cursor="hand"
		CASE UCASE("LinkButton")
		CASE ELSE
			Err.Raise 1, "ASP 101", "DisplayMode "&sDisplayMode&" not supported. Please try any of the following: Text, HyperLink, LinkButton"
		END SELECT
	End Property

	Public Property Let Id(input)
		sId = input
		oTag.Id=sId
	End Property
	
	Public Property Let Text(input)
		sText = input
		oTag.ClearContent()
		oTag.AddContent(sText)
	End Property
	
	'Public methods
	Public Function GetProperty(ByVal sKey)
		oTag.GetProperty sKey
	End Function

	Public Function GetCode()
		GetCode = oTag.GetCode()
	End Function

	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub
End Class 
 %>