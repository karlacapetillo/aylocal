<%
Class ReadonlyTable
	'Properties
	Private sDBTableName
	
	'Objects
	Private oDataSource
	
	'Private Properties
	Private sHeaders, bFetched, sTableCode
    Private tStartFetch, tEndFetch, tElapsedFetch
	Private tStartDisplay, tEndDisplay, tElapsedDisplay
	
	Private Sub Class_Initialize()
		Set oDataSource = new RecordSet
		bFetched=FALSE
	End Sub
    
	Private Sub Terminate()
		Set oDataSource = nothing
	End Sub
	
	Public Property Get DataSource()
		Set DataSource = oDataSource
	End Property
	
	Public Property Get DBTableName()
		DBTableName = sDBTableName
	End Property
	Public Property Let DBTableName(input)
		sDBTableName = input
	End Property

    Public Property Get TimeFetch()
		TimeFech = tElapsedFetch
    End Property

	Public Sub ShowStatistics()
		response.write "********* Statistics for "&Me.DBTableName&" ************* " &"<br>"
		response.write "Open Connection: "&ROUND(oDataSource.TimeOpenConn, 3) &"<br>"
		response.write "Querying: "&ROUND(oDataSource.TimeQuery, 3) &"<br>"
		response.write "Fetching: "&ROUND(tElapsedFetch, 3) &"<br>"
'		response.write "Formating: "& ROUND(tStartDisplay-tEndFetch, 3) &"<br>"
		response.write "Displaying: "&ROUND(tElapsedDisplay, 3) &"<br>"
		response.write "Total: "&ROUND(oDataSource.TimeOpenConn+oDataSource.TimeQuery+tElapsedFetch+tElapsedDisplay, 3)&"<br>"
		response.write "********************************** " &"<br><br>"
	End Sub

	Private Function FetchData()
		IF NOT bFetched THEN 
	    	tStartFetch=timer
			DIM counter, rsDataSource
			Set rsDataSource=oDataSource.RecordSet
		    FOR counter=0 to rsDataSource.Fields.Count-1
				sHeaders=sHeaders&"<th>"&rsDataSource(counter).name&"</th>"
			NEXT
			IF TRIM(sHeaders)<>"" THEN sHeaders="<tr>"&sHeaders&"</tr>"
			sTableCode="<table id=""dataTable""><thead><tr>"&sHeaders&"</tr></thead><tbody><tr><td>"&rsDataSource.GetString(,,"</td><td>","</td></tr><tr><td>","&nbsp;")&"</td></tr></tbody></table>"
	    	tEndFetch=timer
			tElapsedFetch=tEndFetch-tStartFetch
			bFetched=TRUE
		END IF
		FetchData=sTableCode
	End Function
	
	Public Function GetCode()
		oDataSource.AutoCreate Me.DBTableName, "Grid", "Readonly"
		oDataSource.DataBind()
		GetCode=FetchData()
	End Function
	
	Public Sub WriteCode()
		DIM sWriteCode
		sWriteCode=Me.GetCode()
    	tStartDisplay=timer
		response.write sTableCode
		tEndDisplay=timer
		tElapsedDisplay=tEndDisplay-tStartDisplay
	End Sub
End Class
%>