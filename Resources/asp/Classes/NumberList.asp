<% 
Class NumberList
	'Properties
	Private nMinValue, nMaxValue
	
	'Objects 
	Private oDropDownList, oOption, dOptions, oRelatives
	
	'Private variables
	Private bBuilt, aOptionElements
	
	Public Property Get DataType()
		DataType = "AjaxDropDownList"
	End Property

	Public Sub Class_Terminate()
	End Sub

	Private Sub Class_Initialize()
		Set oRelatives = [$Relatives](Me)
		Set oDropDownList = new AjaxDropDownList
		oDropDownList.Catalog="__NumberList"
		oDropDownList.TurnOn=TRUE
	End Sub
	
	'Properties
	Public Property Get IsReadonly()
		IsReadonly = oDropDownList.IsReadonly
	End Property
	Public Property Let IsReadonly(input)
		oDropDownList.IsReadonly = input
	End Property

	'Interfaces
	Public Property Get Relatives()
		Set Relatives = oRelatives
	End Property
	
	Public Property Get Container()
		Set Container = oRelatives.Parent
	End Property
	Public Property Let Container(input)
		oRelatives.Parent = input
	End Property

	'Output objects
	Public Property Get Style()
		Set Style = oDropDownList.Style
	End Property
	
	Public Property Get Properties()
		Set Properties = oDropDownList.Properties
	End Property

	Public Property Get Control()
		Set Control = oDropDownList
	End Property

	'Properties
	Public Property Get Visible()
		Visible = oDropDownList.Visible
	End Property
	Public Property Let Visible(input)
		oDropDownList.Visible = input
	End Property

	Public Property Get MinValue()
		MinValue = nMinValue
	End Property
	Public Property Let MinValue(input)
		nMinValue = input
		oDropDownList.ParametersCollection.SetProperty "StartNumber", input
	End Property

	Public Property Get MaxValue()
		MaxValue = nMaxValue
	End Property
	Public Property Let MaxValue(input)
		nMaxValue = input
		oDropDownList.ParametersCollection.SetProperty "EndNumber", input
	End Property

	Public Property Get Methods()
		Set Methods = oDropDownList.Methods
	End Property
	
	Public Property Get Name()
		Name = oDropDownList.Name
	End Property
	Public Property Let Name(input)
		oDropDownList.Name = input
	End Property

	Public Property Get Id()
		Id = oDropDownList.Id
	End Property
	Public Property Let Id(input)
		oDropDownList.Id = input
	End Property

	Public Property Get CssClass()
		CssClass = oDropDownList.CssClass
	End Property
	Public Property Let CssClass(input)
		oDropDownList.CssClass = input
	End Property

	Public Property Get Value()
		Value = oDropDownList.Value
	End Property
	Public Property Let Value(input)
		oDropDownList.Value = input
	End Property

	'Public methods
	Public Sub ClearSelections()
		oDropDownList.ClearSelections()
	End Sub
	
	Public Function GetCode()
		GetCode=oDropDownList.GetCode()
	End Function
	
	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub
End Class 
 %>