<% 
Function [$DataField](ByRef oDataSource, ByRef rsField)
	Set [$DataField] = new DataField
	[$DataField].Field = rsField
	[$DataField].DataSource = oDataSource
End Function
Class DataField
	'Properties
	Private sFieldName, sFieldNull, sTableName, sDataType, sFormat, sMetadataString, iTextObjectIfSizeGreaterThan, sMode, sTotalRowFormulaTranslated, sRowLevelParameters
	'Settings
	Private bRender
	'Objects
	Private dMetadata, oRelatives, oDataSource, oControlSelector, oMetadataField, oDataValue, oControls
	
	Private Sub Class_Initialize()
		Set oRelatives = [$Relatives](Me)
		Set oControls = nothing
		Set oControlSelector = nothing
		Set oMetadataField = nothing
		Set oDataValue = [$DataValue](Me)
		bRender=TRUE
		iTextObjectIfSizeGreaterThan=70
	End Sub
	Private Sub Class_Terminate()
		Set dMetadata = nothing
		Set oRelatives = nothing
		Set oControls = nothing
		Set oControlSelector = nothing
		Set oMetadataField = nothing
		Set oDataValue = nothing
	End Sub

	'Interfaces
	Public Property Get Metadata()
		Set Metadata = dMetadata
	End Property
	
	Public Property Get Control()
		IF oControls IS NOTHING THEN
			'Debugger Me, Me.FieldName
			Set Control = NOTHING
		ELSE
			Set Control = oControls("#MainControl#")
		END IF
	End Property

	Public Property Get Controls()
		Set Controls = oControls
	End Property

	Public Property Get Field()
		Set Field = oDataValue.Field
	End Property
	Public Property Let Field(input)
		oDataValue.Field = input
		sFieldName=oDataValue.Field.Name
	End Property

	Public Property Get FieldName()
		FieldName = oDataValue.Field.Name
	End Property

	Public Property Get DataSource()
		Set DataSource = oDataValue.DataSource
	End Property
	Public Property Let DataSource(input)
		oDataValue.DataSource = input
		Set oControlSelector = [$ControlSelector](Me)
	End Property

	Public Property Get MetadataField()
		Set MetadataField = oMetadataField
	End Property
	Public Property Let MetadataField(input)
		Set oMetadataField = input
	End Property

	'Properties
	Public Property Get Container()
		Set Container = oRelatives.Parent
	End Property
	Public Property Let Container(input)
		oRelatives.Parent = input
	End Property

	Public Property Get Render()
		SELECT CASE UCASE(Me.Mode)
		CASE "ACCESIBLE", "TAB"
			Render = False
		CASE ELSE
			Render = bRender
		END SELECT
	End Property
	Public Property Let Render(input)
		bRender = input
	End Property

	Public Property Get FieldNull()
		FieldNull = sFieldNull
	End Property
	Public Property Let FieldNull(input)
		sFieldNull = input
	End Property

	Public Default Property Get Text()
'		DIM sValue, sDataType
'		sValue = oDataValue.Value
'		sDataType = Me.DataType
'		IF [&CString](UCASE(TypeName(Me.Control))).ExistsIn(UCASE("CascadedDropDown, AjaxDropDownList, DropDownList")) THEN
'			Me.Control.Value=Me.Value
'			Text=Me.Control.Text
'		ELSE
			Text = oDataValue.Text
'		END IF
	End Property

	Public Property Get Value()
		'IF Session("IdUsuario")=1 THEN Debugger Me, Me.FieldName&" ("&TypeName(Me.Control)&"): "&Me.Control.Value
		IF Me.Control IS NOTHING THEN
			Value=Me.DBValue
		ELSE
			Value=Me.Control.Value
		END IF
	End Property

	Public Property Get DBValue()
		DBValue=oDataValue
	End Property

	Public Property Get PrevValue()
		PrevValue=oDataValue.PrevValue
	End Property

	Public Property Get NextValue()
		NextValue=oDataValue.NextValue
	End Property

	Public Property Get Visible()
		IF NOT(Me.Control IS nothing) THEN
'			Debugger Me, "<strong>"&Me.FieldName&"</strong>: "&TypeName(Me.Control)&" ("&Me.Control.Visible&")<br>"
			ON ERROR  RESUME NEXT
			Visible = Me.Control.Visible
			IF Err.Number<>0 THEN 
				Debugger Me, TypeName(Me.Control)&" doesn't support visible property"
				Err.Clear
				response.end
			END IF
			ON ERROR  GOTO 0
		ELSE
			Visible = FALSE
		END IF
	End Property
	Public Property Let Visible(input)
		ON ERROR  RESUME NEXT
		IF NOT([&CString](UCASE(TypeName(Me.Control))).ExistsIn(UCASE("Nothing, Label, HyperLink, CollapsedTable, GridView, TabPanel"))) THEN Me.Control.Visible = input
		IF Err.Number<>0 THEN
			Debugger Me, "("&sFieldName&") Error en control de tipo "&TypeName(Me.Control)&" para la propiedad ""Visible"": "&Err.Description
			Err.Clear
		END IF
		ON ERROR  GOTO 0
	End Property

'	Public Property Get Data()
'		Set Data = oDataSource
'	End Property
'	Public Property Let Data(ByRef input)
'		Set oDataSource = input
'	End Property

	Public Property Get TextObjectIfSizeGreaterThan()
		TextObjectIfSizeGreaterThan = iTextObjectIfSizeGreaterThan
	End Property
	Public Property Let TextObjectIfSizeGreaterThan(input)
		iTextObjectIfSizeGreaterThan = input
	End Property

	Public Property Get AppDefaultValue()
		AppDefaultValue = EVAL(REPLACE(dMetadata("AppDefaultValue"), """", """"""))
	End Property

	Public Property Get Formula()
		Formula = dMetadata("Formula")
	End Property

	Public Property Get TableName()
		TableName = dMetadata("TableName")
	End Property

	Public Property Get HeaderText()
		HeaderText=Me.DisplayName
	End Property
	Public Property Let HeaderText(input)
		Me.DisplayName = input
	End Property

	Public Property Get IsReadonly()
		IsReadonly = (UCASE(Me.ControlMode)="READONLY")
	End Property
	Public Property Let IsReadonly(input)
		IF CBOOL(input) THEN
			Me.ControlMode="Readonly"
		ELSE
'			response.write "WARNING (DataField): Property IsReadonly=FALSE doesn't make any changes.<br>"
		END IF
	End Property

	Public Property Get Mode()
		Mode = dMetadata("Mode")
	End Property
'	Public Property Let Mode(input)
'		IF NOT(IsNullOrEmpty(input)) THEN 
'			sMode=input
'			oControlSelector.Mode=sMode
'		END IF
'	End Property

	Public Property Get ControlMode()
		ControlMode = dMetadata("Mode")
	End Property
	Public Property Let ControlMode(input)
		IF NOT(IsNullOrEmpty(input)) THEN dMetadata("Mode") = input
	End Property

	Public Property Get MaxLength()
		MaxLength = ZeroIfInvalid(dMetadata("MaxLength"))
	End Property

	Public Property Get IsNullable()
		IsNullable = CBOOL(dMainDictionary(UCASE(CSTR(dMetadata("IsNullable")))))
	End Property

	Public Property Get Format()
		IF NOT(IsNullOrEmpty(dMetadata("Format"))) THEN
			Format = dMetadata("Format")
		ELSE
			SELECT CASE UCASE(Me.DataType)
			CASE "SMALLMONEY"
				Format = "Money"
			CASE "PERCENT", "MONEY"
				Format = Me.DataType
			CASE "FORMULA"
				Format = dMetadata("DataType")
			CASE ELSE
				Format = ""
			END SELECT
		END IF
	End Property
	Public Property Let Format(input)
		dMetadata("Format") = input
	End Property

	Public Property Get DataType()
		'Debugger Me, "<strong>"&Me.FieldName&"</strong> "&dMetadata("DataType")
		IF IsEmpty(sDataType) THEN
			'Debugger Me, sFieldName&": "&dMetadata("Mode")&" vs "&Me.Mode&" "&dMetadata("DataType")
			SELECT CASE UCASE(dMetadata("Mode"))
			CASE "TAB" 
				sDataType="TabPanel"
			CASE "HIDDEN"
				sDataType = "HiddenField"
			CASE ELSE
				IF Me.IsIdentity THEN
					sDataType="identity"
				ELSEIF NOT IsNullOrEmpty(dMetadata("ControlType")) THEN
					sDataType=dMetadata("ControlType")
				ELSEIF NOT IsNullOrEmpty(Me.Formula) THEN
					sDataType="formula"
				ELSEIF UCASE(dMetadata("DataType"))=UCASE("timestamp") OR UCASE(Me.Mode)="HIDDEN" THEN
					sDataType = "HiddenField"
				ELSEIF NOT IsNullOrEmpty(Me.ParentTables) THEN
					IF INSTR(Me.ParentTables, "@/@")>0 THEN
						sDataType = "CascadedDropDown"
					ELSE
						sDataType = "AjaxDropDownList"
					END IF
				ELSE
					sDataType = dMetadata("DataType")
				END IF
				IF CSNG(Me.MaxLength)>=CSNG(iTextObjectIfSizeGreaterThan) THEN 
					SELECT CASE UCASE(sDataType)
					CASE UCASE("char"), UCASE("nchar")
						sDataType="text"
					END SELECT
				END IF
			END SELECT
		END IF
		DataType=sDataType
	End Property

	Public Property Get IsIdentity()
		IsIdentity = CBOOL(dMainDictionary(UCASE(CSTR(dMetadata("IsIdentity")))))
	End Property

	Public Property Get DisplayName()
		IF NOT(IsNullOrEmpty(dMetadata("DisplayName"))) OR Me.IsIdentity THEN
			DisplayName = dMetadata("DisplayName")
		ELSEIF NOT IsNullOrEmpty(Me.ParentTables) AND INSTR(UCASE(">"&sFieldName), ">ID")>0 THEN
			DisplayName = ToTitleFromPascal(MID(sFieldName, 3))
		ELSE
			DisplayName = ToTitleFromPascal(sFieldName)
		END IF
	End Property
	Public Property Let DisplayName(input)
		dMetadata("DisplayName") = input
	End Property


	Public Property Get ParentTables()
		ParentTables = dMetadata("ParentTables")
	End Property

	Public Property Get GroupByColumn()
		GroupByColumn = dMetadata("GroupByColumn")
	End Property

	Public Property Get TranslatedTotalRowFormula()
		IF TRIM(Me.TotalRowFormula)<>"" AND TRIM(sTotalRowFormulaTranslated)="" THEN 
			sTotalRowFormulaTranslated=TranslateTemplate(Me.TotalRowFormula) 
'			Debugger Me, "<strong>Traduciendo formula de "&Me.FieldName&": </strong>"&sTotalRowFormulaTranslated
		END IF
		TranslatedTotalRowFormula=sTotalRowFormulaTranslated
	End Property

	Public Property Get TotalRowFormula()
		TotalRowFormula = dMetadata("TotalRowFormula")
	End Property

	Public Property Get AllowNegatives()
		AllowNegatives = dMetadata("AllowNegatives")
	End Property

	Public Property Get MinRange()
		MinRange = dMetadata("MinRange")
	End Property

	Public Property Get MaxRange()
		MaxRange = dMetadata("MaxRange")
	End Property

	Public Property Get AutoRowSpan()
		AutoRowSpan = CBOOL(EmptyIfNull(dMetadata("AutoRowSpan")))
	End Property
	Public Property Let AutoRowSpan(inpput)
		dMetadata("AutoRowSpan")=CBOOL(input)
	End Property

	Public Property Get AutoRowSpanReferenceDataField()
		AutoRowSpanReferenceDataField = dMetadata("AutoRowSpanReferenceDataField")
	End Property
	Public Property Let AutoRowSpanReferenceDataField(input)
		dMetadata("AutoRowSpanReferenceDataField") = input
	End Property

	'Methods
	Public Sub GetDBMetadata(sTableName)
		Dim oRecordSet, rsDataSource, i
		Set oRecordSet = new RecordSet
		oRecordSet.SQLQueryString="SELECT * FROM Metadata WHERE TableName='"&sTableName&"' AND ColumnName='"&sFieldName&"'"
		oRecordSet.DataBind()
		IF oRecordSet.RecordSet.BOF AND oRecordSet.RecordSet.EOF THEN 
			IF session("Debugging") THEN RESPONSE.WRITE "<strong>No hay informacion disponible para "&sFieldName&"</strong><br>"
			Exit Sub
		END IF
		DIM sColumnName
		
		DIM oColumn, sTempMetadataString
		Set rsDataSource=oRecordSet.RecordSet
		
'		oRecordSet.ShowProperties()
		response.write "&nbsp;&nbsp;&nbsp;<b>Metadata de la base de datos para "&sFieldName&": </b><br>"
'		FOR i=0 TO oRecordSet.RecordSet.fields.count-1
		FOR EACH oColumn IN rsDataSource.Fields
			IF TRIM(oColumn.Name)<>"ColumnName" THEN
				sColumnName=oColumn.name
				sTempMetadataString=sTempMetadataString&sColumnName&":"&RTRIM(oColumn.Value)&";"
			END IF
		NEXT
		Me.MetadataString=sTempMetadataString
	End Sub
	
	Public Property Get MetadataString()
		MetadataString = sMetadataString
	End Property
	Public Property Let MetadataString(input)
		sMetadataString = HTMLDecode(input)
		Set dMetadata = server.CreateObject("Scripting.Dictionary")
		'Debugger Me, "<strong>MetadataString para "&sFieldName&":</strong> "&sMetadataString&"<br>"
		IF TRIM(sMetadataString)<>"" THEN
'			response.write getMetadata(sMetadataString, sFieldName, "TotalRowFormula")&"<br>"
			dMetadata("TableName")=getMetadata(sMetadataString, sFieldName, "Table_Name")
			dMetadata("AppDefaultValue")=getMetadata(sMetadataString, sFieldName, "AppDefaultValue")
			dMetadata("Mode")=getMetadata(sMetadataString, sFieldName, "Mode")
			dMetadata("MaxLength")=getMetadata(sMetadataString, sFieldName, "MaxLength")
			dMetadata("IsNullable")=getMetadata(sMetadataString, sFieldName, "IsNullable")
			dMetadata("DataType")=getMetadata(sMetadataString, sFieldName, "DataType")
			dMetadata("ControlType")=getMetadata(sMetadataString, sFieldName, "ControlType")
			dMetadata("Format")=getMetadata(sMetadataString, sFieldName, "Format")
			dMetadata("Controls")=getMetadata(sMetadataString, sFieldName, "Controls")
			dMetadata("ControlParameters")=getMetadata(sMetadataString, sFieldName, "ControlParameters")
			dMetadata("RowLevelParameters")=getMetadata(sMetadataString, sFieldName, "RowLevelParameters")
			dMetadata("Formula")=getMetadata(sMetadataString, sFieldName, "Formula")
			dMetadata("IsIdentity")=getMetadata(sMetadataString, sFieldName, "IsIdentity")
			dMetadata("DisplayName")=getMetadata(sMetadataString, sFieldName, "DisplayName")
			dMetadata("ParentTables")=getMetadata(sMetadataString, sFieldName, "ParentTables")
			dMetadata("GroupByColumn")=getMetadata(sMetadataString, sFieldName, "GroupByColumn")
			dMetadata("TotalRowFormula")=getMetadata(sMetadataString, sFieldName, "TotalRowFormula")
			dMetadata("DefaultValue")=getMetadata(sMetadataString, sFieldName, "DefaultValue")
			dMetadata("AllowNegatives")=getMetadata(sMetadataString, sFieldName, "AllowNegatives")
			dMetadata("MinRange")=getMetadata(sMetadataString, sFieldName, "MinRange")
			dMetadata("MaxRange")=getMetadata(sMetadataString, sFieldName, "MaxRange")
			dMetadata("AutoRowSpan")=getMetadata(sMetadataString, sFieldName, "AutoRowSpan")
			dMetadata("AutoRowSpanReferenceDataField")=getMetadata(sMetadataString, sFieldName, "AutoRowSpanReferenceDataField")
			dMetadata("TableCommonIdentifier")=getMetadata(sMetadataString, sFieldName, "TableCommonIdentifier")
'			dMetadata("")=getMetadata(sMetadataString, sFieldName, "")
		ELSEIF UCASE(sFieldName)=UCASE("SQLRowNumber") THEN
			dMetadata("DataType")="SQLRowNumber"
		ELSE
'			response.write "No metadataString was set for "&sFieldName&"<br>"
			Exit Property
		END IF
		'Debugger Me, "<strong>"&sFieldName&"</strong> "&dMetadata("Mode")
		oControlSelector.MaxLength=Me.MaxLength
		oControlSelector.DataType=Me.DataType
		Set oControls = new Collection
		oControls.Separator=" "
		oControls.AddNamedItem oControlSelector.Control, "#MainControl#"
		
		IF dMetadata("Controls")<>"" THEN
			EXECUTE("WITH oControls "&[&CString](dMetadata("Controls")).Remove(vbcrlf).Replace("\\""", """""")&" END WITH")
		END IF
		TryPropertySet Me.Control, "Format", Me.Format, FALSE
		
''		response.write "DataField: "&sFieldName&" ("&dMetadata("ControlParameters")&")<br>"
''		response.write "DataField: "&sFieldName&" ("&Me.Mode&"): "&TypeName(Me.Control)&" - "&Me.IsReadonly&"<br>"
		TryPropertySet Me.Control, "IsReadonly", Me.IsReadonly, FALSE
''		response.write "<br>"
		
		oDataValue.Format=Me.Format
		IF NOT Me.Control IS NOTHING THEN
			SELECT CASE UCASE(Me.DataType)
			CASE UCASE("Identity")
				Me.Control.CheckIfHasValue=FALSE
				Me.Control.Value=oDataValue
				Me.Control.Name=sFieldName
'				Me.Control.ShowLabel=TRUE
				oDataValue.TreatNullsAs "[new]"
			CASE ELSE
				'Defining Values
				ON ERROR  RESUME NEXT
				SELECT CASE UCASE(TypeName(Me.Control))
				CASE UCASE("Label"), UCASE("HyperLink"), UCASE("CollapsedTable"), UCASE("GridView"), UCASE("Formula"), UCASE("TabPanel")
					Me.Control.Id=sFieldName
				CASE ELSE
					Me.Control.Name=sFieldName
				END SELECT
				IF Err.Number<>0 THEN
					Debugger Me, "Error en control de tipo "&TypeName(Me.Control)&": "&Err.Description
					Err.Clear
				END IF
				ON ERROR  GOTO 0
				SELECT CASE UCASE(TypeName(Me.Control))
				CASE UCASE("AjaxDropDownList"), UCASE("CascadedDropDown")
					Me.Control.Name=sFieldName
					Me.Control.InfoString=Me.ParentTables
					Me.Control.Value=oDataValue
				CASE UCASE("DropDownList")
					Me.Control.Name=sFieldName
					Me.Control.Value=Try(NOT(UCASE(Me.DataType)="BIT"), Me.DBValue, dMainDictionary(LCASE(Me.DBValue)))
				CASE UCASE("List"), UCASE("insertButton"), UCASE("updateButton"), UCASE("Collection")
				CASE UCASE("CheckBox")
					IF NOT ISNULL(dMetadata("DefaultValue")) THEN Me.Control.DefaultValue=EvaluateFieldsTemplate(TranslateTemplate(dMetadata("DefaultValue")), oDataValue.DataSource.Fields)
					Me.Control.Name=sFieldName
					Me.Control.Value=oDataValue
				CASE UCASE("TextBox")
					Me.Control.Name=sFieldName
					Me.Control.Value=oDataValue
					SELECT CASE UCASE(Me.DataType)
					CASE UCASE("char")
						Me.Control.Size = Me.MaxLength
						Me.Control.MaxLength = Me.MaxLength
					CASE "PERCENT", "MONEY", "SMALLMONEY"
						oDataValue.DecimalPositions=2
					END SELECT
				CASE UCASE("Formula")
					IF NOT IsNullOrEmpty(Me.Formula) THEN 
						DIM Matches: Set Matches = getMatch(Me.Formula, "\[{0,1}dbo\]{0,1}\.\[{0,1}((?:\w|\s|_|:)+)\]{0,1}")
						DIM sFormula: sFormula=Me.Formula
'						FOR EACH Match IN Matches
'							sFormula=REPLACE(sFormula, Match.Value, REPLACE(REPLACE(Match.Value, "[", ""), "]", "") )
'						NEXT
						Me.Control.Formula=sFormula
					END IF
					Me.Control.Id=sFieldName
					Me.Control.Text=oDataValue
				CASE UCASE("Label"), UCASE("HyperLink")
					Me.Control.Id=sFieldName
					Me.Control.Text=oDataValue
				CASE "COLLAPSEDTABLE"
Me.Control.CurrentLocation="["&Me.TableName&"]("&Me.DataSource.Metadata.ViewMode&":"&Me.DataSource.Metadata.Mode&")"
					Me.Control.DBTableName=sFieldName
					Me.Control.URLParameters="&IsMainTable=FALSE"
					IF IsNullOrEmpty(dMetadata("TableCommonIdentifier")) THEN 
						Debugger Me, "No se pudo recuperar la columna identificadora de la tabla relacionada <strong>"&sFieldName&"</strong>"
						[&Stop] Me
					END IF
					Me.Control.DBTableIdColumn=dMetadata("TableCommonIdentifier") 'SPLIT(Me.Text, ":=")(0)
'					Me.Control.DBTableIdColumn=SPLIT(Me.Text, ":=")(1)
				CASE "GRIDVIEW"
					Me.Control.DBTableName=sFieldName
					Me.Control.SupportsUpdate=FALSE
					Me.Control.SupportsDelete=FALSE
					Me.Control.SupportsInsert=FALSE
					Me.Control.FieldNull="&nbsp;"
					Me.Control.ControlMode="update"
					Me.Control.PageSize=20
				CASE "MONEYRANGE", "TABPANEL"
				CASE ELSE
'					ON ERROR  RESUME NEXT
					Me.Control.Value = oDataValue
'					TryPropertySet Me.Control, "Value/Text", oDataValue, TRUE
					IF Err.Number<>0 THEN 
						RESPONSE.WRITE "<strong class=""warning"">DataField: </strong>La propiedad Text no est� definida o no acepta este valor "&TypeName(Me.Control)&"<br>"
						Err.Clear
					END IF
					ON ERROR  GOTO 0
				END SELECT
			END SELECT
		END IF
		sRowLevelParameters=TranslateTemplate(dMetadata("RowLevelParameters"))
		IF NOT(IsNullOrEmpty(dMetadata("ControlParameters"))) THEN
			'response.write sMetadataString&"<br><br>"
			'Debugger Me, dMetadata("ControlParameters")&"<br><br>"
			DIM aControlParameters: Set aControlParameters=getParameters(dMetadata("ControlParameters"))
			DIM ixControlParameter, Match
			FOR EACH Match IN aControlParameters
				'IF SESSION("IdUsuario")=1 THEN 
				'	Debugger Me, TranslateTemplate(Match.SubMatches(1))
				'END IF
				DIM sParameterName: sParameterName=Match.SubMatches(0)
				DIM sParameterValue: sParameterValue=Evaluate(Match.SubMatches(1)) 'EvaluateFieldsTemplate(TranslateTemplate(Match.SubMatches(1)), oDataValue.DataSource.Fields)	'
				
				'EXECUTE("DIM sParameterValue: sParameterValue="&Match.SubMatches(1))
				'IF SESSION("IdUsuario")=1 THEN Debugger Me, sFieldName&": "&Me.Control.IsReadonly
				'Debugger Me, sParameterName&"="&sParameterValue
				TryPropertySet Me.Control, sParameterName, sParameterValue, TRUE
				
'				response.write sFieldName&": "&Match.Value&" --> ("&Match.Submatches.count&")<br>"
'				DIM SubMatch
'				FOR EACH SubMatch IN getMatch(Match.Value, "^\w+(?=\=)")
''					EXECUTE("Me.Control."&SubMatch.Value&"=REPLACE("">""&Match.Value, "">""&SubMatch.Value&""="", """"))")
'					TryPropertySet Me.Control, SubMatch.Value, REPLACE(">"&Match.Value, ">"&SubMatch.Value&"=", ""), FALSE
'				NEXT
			NEXT
		END IF
		IF Session("debugging") THEN
			response.write "<strong>Metadata para "&sFieldName&":</strong> <br>"
			DIM sKeyName
			FOR EACH sKeyName IN Me.Metadata.keys
				response.write "&nbsp;&nbsp;&nbsp;"&sKeyName&": "&Me.Metadata.Item(sKeyName)&"<br>"
			NEXT
			response.write "<br><br>"
		END IF
		'Me.Controls.AddItem [$Label]("", "Probando")
	End Property
	
	Public Function GetCode()
		IF NOT(Me.Render) THEN GetCode="" : Exit Function END IF
		DIM Match, sReturnCode
		IF NOT Me.Control IS NOTHING THEN
			SELECT CASE UCASE(Me.DataType)
			CASE UCASE("Identity")
			CASE ELSE
'				'Defining Values
				SELECT CASE UCASE(TypeName(Me.Control))
				CASE UCASE("AjaxDropDownList"), UCASE("CascadedDropDown")
				CASE UCASE("DropDownList")
				CASE UCASE("List"), UCASE("insertButton"), UCASE("updateButton"), UCASE("Collection")
				CASE UCASE("CheckBox")
				CASE UCASE("TextBox")
				CASE UCASE("Formula")
				CASE UCASE("Label"), UCASE("HyperLink")
				CASE "COLLAPSEDTABLE"
				CASE "MONEYRANGE"
				CASE ELSE
				END SELECT
			END SELECT
'			'Defining Row Specific properties
			DIM sFieldMetadata
'			IF sFieldName="FechaAutorizacion" AND NOT(IsNullOrEmpty(Me.Value)) THEN
'				Debugger Me, Me.DataSource.Fields.CommandsField.Command("Delete").Render
'				Me.DataSource.Fields.CommandsField.Command("Delete").Render=FALSE
'			END IF
			sFieldMetadata=sRowLevelParameters
			'IF SESSION("IdUsuario")=1 THEN Debugger Me, sRowLevelParameters&": "&oDataValue.DataSource.Fields.Field("IdUsuarioSolicitante")&": "&SESSION("IdUsuario")
			REDIM PRESERVE aOldPropertiesStack(2, 0)
			'IF NOT Me.MetadataField IS nothing THEN IF SESSION("IdUsuario")=1 THEN Debugger Me, oMetadataField.Value()
			IF NOT Me.MetadataField IS nothing THEN sFieldMetadata=sFieldMetadata&oMetadataField.Value()
			IF NOT(IsNullOrEmpty(sFieldMetadata)) THEN
				'Debugger Me, "<strong>"&Me.FieldName&":</strong> "&sFieldMetadata&" ("&TypeName(oMetadataField)&")"
				DIM oParameters: Set oParameters = new Parameters
				oParameters.Parameters=sFieldMetadata
				DIM oParameter, iOPS: iOPS=0
				FOR EACH oParameter IN oParameters.All.Items
					iOPS=iOPS+1
					DIM sParameterName: sParameterName=oParameter.Name
					'IF SESSION("IdUsuario")=1 THEN Debugger Me, "<strong>"&sParameterName&":</strong> "&replace(oParameter.Value, "'", "")&"<br>"
					DIM oFields: Set oFields=oDataValue.DataSource.Fields
'					response.write "<strong>"&oFields("IdUsuario").Control.NamedItem("IdUsuario").Value&"</strong>"
'					Debugger Me, EvaluateFieldsTemplate("oFields(""IdUsuario"")", oDataValue.DataSource.Fields)
					REDIM PRESERVE aOldPropertiesStack(2, iOPS)
					aOldPropertiesStack(1, iOPS)=sParameterName
					TryPropertyGet Me, sParameterName, aOldPropertiesStack(2, iOPS), TRUE
					TryPropertySet Me, sParameterName, EvaluateFieldsTemplate(oParameter.Value, oDataValue.DataSource.Fields), TRUE
				NEXT
				Set oParameters = nothing
			END IF

'			'Generating code
			sReturnCode=Me.Controls.GetCode()

			'Reset properties to their previous values
			FOR iOPS=1 TO UBOUND(aOldPropertiesStack, 2)
				TryPropertySet Me, aOldPropertiesStack(1, iOPS), aOldPropertiesStack(2, iOPS), TRUE
			NEXT
		ELSE
			sReturnCode=Me.Text
		END IF
		GetCode=sReturnCode'&"<strong>"&TypeName(Me.Control)&" ("&oDataValue.Format&"): "&dMetadata("DataType")&"</strong>"
	End Function
	
	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub
End Class
 %>