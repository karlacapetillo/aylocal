<% 
Class MethodsCollection
	'Properties
	Private sMethodsString
	Private sOnFocus, sOnBlur, sOnMouseOver, sOnMouseOut, sOnClick, sOnPropertyChange, sOnChange, sOnLoad, sOnKeyDown, sOnContextMenu
	
	'Settings
	
	'Objects
	Private oProperties
	
	'Private Variables
	
	Private Sub Class_Initialize()
		Set oProperties = new PropertiesCollection
	End Sub
	
	'Output Properties
	Public Property Get MethodsString()
		MethodsString = sMethodsString
	End Property
	
	'Public methods
	Public Property Get OnContextMenu()
		OnContextMenu = sOnContextMenu
	End Property
	Public Property Let OnContextMenu(input)
		sOnContextMenu = input
		oProperties.SetProperty "oncontextmenu", input
	End Property

	Public Property Get OnLoad()
		OnLoad = sOnLoad
	End Property
	Public Property Let OnLoad(input)
		sOnLoad = input
		oProperties.SetProperty "onload", input
	End Property

	Public Property Get OnKeyDown()
		OnKeyDown = sOnKeyDown
	End Property
	Public Property Let OnKeyDown(input)
		sOnKeyDown = input
		oProperties.SetProperty "onkeydown", input
	End Property

	Public Property Get OnChange()
		OnChange = sOnChange
	End Property
	Public Property Let OnChange(input)
		sOnChange = input
		oProperties.SetProperty "onchange", input
	End Property

	Public Property Get OnPropertyChange()
		OnPropertyChange = sOnPropertyChange
	End Property
	Public Property Let OnPropertyChange(input)
		sOnPropertyChange = input
		oProperties.SetProperty "onpropertychange", input
	End Property

	Public Property Get OnClick()
		OnClick = sOnClick
	End Property
	Public Property Let OnClick(input)
		sOnClick = input
		oProperties.SetProperty "onclick", input
	End Property

	Public Property Get OnFocus()
		OnFocus = sOnFocus
	End Property
	Public Property Let OnFocus(input)
		sOnFocus = input
		oProperties.SetProperty "onfocus", input
	End Property

	Public Property Get OnBlur()
		OnBlur = sOnBlur
	End Property
	Public Property Let OnBlur(input)
		sOnBlur = input
		oProperties.SetProperty "onblur", input
	End Property

	Public Property Get OnMouseOver()
		OnMouseOver = sOnMouseOver
	End Property
	Public Property Let OnMouseOver(input)
		sOnMouseOver = input
		oProperties.SetProperty "onmouseover", input
	End Property
	
	Public Property Get OnMouseOut()
		OnMouseOut = sOnMouseOut
	End Property
	Public Property Let OnMouseOut(input)
		sOnMouseOut = input
		oProperties.SetProperty "onmouseout", input
	End Property

	'Public methods
	Public Function GetCode()
		GetCode=oProperties.GetCode()
	End Function

	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub
End Class 
 %>