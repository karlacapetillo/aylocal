<% 
Class Div
	'Properties
	Private sId
	
	'Settings
	Private bIsReadonly
	
	'Objects
	Private oDiv, oProperties, oContainer, oRelatives
	
	'Private Variables
	
	Public Property Get Object()
		Set Object = oDiv
	End Property

	Public Property Get IsReadonly()
		IsReadonly = bIsReadonly
	End Property
	Public Property Let IsReadonly(input)
		bIsReadonly = input
	End Property

	Private Sub Class_Initialize()
		Set oRelatives = [$Relatives](Me)
		Set oDiv = new Tag 
		oDiv.TagName="div"
		Set oProperties = new PropertiesCollection
	End Sub
	
	'Output objects
	Public Property Get Properties()
		Set Properties = oDiv.Properties
	End Property

	Public Property Get Methods()
		Set Methods = oDiv.Methods
	End Property
	
	Public Property Get Style()
		Set Style = oDiv.Style
	End Property
	
	Public Property Get Relatives()
		Set Relatives = oRelatives
	End Property
	
	Public Property Get Container()
		Set Container = oRelatives.Parent
	End Property
	Public Property Let Container(input)
		oRelatives.Parent = input
	End Property

	Public Property Get Content()
		Set Content = oDiv.Content
	End Property

	Public Property Get ParentCell()
		Set ParentCell = oDiv.ParentCell
	End Property
	
	'Output Properties
	Public Property Get Text()
		Text = oDiv.Text
	End Property
	Public Property Let Text(input)
		oDiv.Text=input
	End Property

	Public Property Get Width()
		Width = oDiv.Style.Width
	End Property
	Public Property Let Width(input)
		oDiv.Style.Width = input
	End Property
	
	Public Property Get Height()
		Height = oDiv.Style.Height
	End Property
	Public Property Let Height(input)
		oDiv.Style.Height = input
	End Property
	
	Public Property Get Scroll()
		IF UCASE(oDiv.Style.OverFlow)="AUTO" or UCASE(oDiv.Style.OverFlow)="SCROLL" THEN
			Scroll = True
		ELSE
			Scroll = False
		END IF
	End Property
	Public Property Let Scroll(input)
		IF input THEN
			oDiv.Style.OverFlow = "auto"
		ELSE
			oDiv.Style.OverFlow = "hidden"
		END IF
	End Property
	
	Public Property Get Visible()
		Visible = oDiv.Visible
	End Property

	Public Property Get Id()
		Id = sId
	End Property
	
	Public Property Get CssClass()
		CssClass = oDiv.CssClass
	End Property
	
	'Input Properties 
	Public Property Let ParentCell(input)
		oDiv.ParentCell = input
	End Property

	Public Property Let Visible(input)
		oDiv.Visible = input
	End Property

	Public Property Let Id(input)
		sId = input
		Me.Properties.SetProperty "id", input
	End Property
	
	Public Property Let HasClosingTag(input)
		bHasClosingTag = input
	End Property
	
	Public Property Let CssClass(input)
		oDiv.CssClass=input
	End Property
	
'	Public Property Let OtherProperties(input)
'		sOtherProperties = input
'	End Property
	
	'Public methods
	Public Sub SetStyle(ByRef oNewStyle)
		Set oStyle = oNewStyle
	End Sub

	Public Sub AddContent(ByRef oElement)
		oDiv.AddContent oElement
	End Sub

	Public Sub ClearContent()
		oDiv.ClearContent()
	End Sub

	Public Function GetCode()
		GetCode=oDiv.GetCode()
	End Function

	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub
End Class 
 %>