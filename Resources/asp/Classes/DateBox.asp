<% 
Class DateBox
	'Properties
	Private sName, sMode, sCssClass, iSize, iMaxLength
	
	'Settings
	Private bIsReadonly
	
	'Objects
	Private oTextBox, oHolder, oCalendarImage, oCollection, oRelatives, oValue
	
	'Private variables
	Private sReturnString
	
	Public Property Get Visible()
		Visible = oCollection.Visible
	End Property
	Public Property Let Visible(input)
		oCollection.Visible = input
	End Property

	Public Property Get Object()
		Set Object = oTextBox.Object
	End Property

	Public Property Get Style()
		Set Style = oTextBox.Style
	End Property
	
	Private Sub Class_Terminate()
		Set oCollection = nothing
		Set oCalendarImage = nothing
		Set oValue = nothing
	End Sub
	Private Sub Class_Initialize()
		Set oValue = NOTHING
'		Set oRelatives = [$Relatives](Me)
		Set oCollection = new Collection
		sCssClass="fecha"
		iSize=8
		iMaxLength=10
		'TextBox
		Set oTextBox = new TextBox
		oTextBox.CssClass=sCssClass
		oTextBox.Size=iSize
		oTextBox.MaxLength=iMaxLength
		oTextBox.Properties.SetProperty "turnOn", "true"
		oCollection.AddItem(oTextBox)
		'Calendar image
		Set oCalendarImage = new Image
		oCalendarImage.Id="cal_fecha"
		oCalendarImage.ImageURL = sCalendarImagePath
		oCalendarImage.Height=15
		oCalendarImage.Width=16
		oCalendarImage.AlternateText="Mostrar calendario"
		oCalendarImage.Methods.OnClick="displayCalendar(document.all[this.sourceIndex-1],'dd-mm-yyyy',this)" 
		oCalendarImage.Style.Cursor="hand"
		oCollection.AddItem(oCalendarImage)
	End Sub
	
	'Output objects
	Public Property Get CalendarObject()
		Set CalendarObject = oCalendarImage
	End Property

	Public Property Get Properties()
		Set Properties = oTextBox.Properties
	End Property

	Public Property Get Methods()
		Set Methods = oTextBox.Methods
	End Property

	Public Property Get InnerHolder()
		Set InnerHolder = oHolder
	End Property

	Public Property Get InnerInput()
		Set InnerInput = oTextBox
	End Property

	'Properties
	Public Property Get Name()
		Name = sName
	End Property
	Public Property Let Name(input)
		sName = input
		oTextBox.Name=sName
	End Property

	Public Sub CreateValueObject()
		IF oValue IS NOTHING THEN
			Set oValue = new StringValue
		END IF
	End Sub
	
	Public Property Get Text()
		DIM sText:	sText = oValue.Text
		IF ISDATE(sText) THEN
			IF CDATE(sText)=CDATE("1/1/1900") THEN
				sText=""
			END IF
		ELSE
			sText=""
		END IF
		IF sText<>"" THEN oTextBox.Text=FORMATDATETIME(sText, 2)
		Text = sText
	End Property
	Public Property Let Text(input)
		CreateValueObject()
		Me.Value=input
	End Property

	Public Property Get Value()
		Value = oValue
	End Property
	Public Property Let Value(input)
		IF IsObject(Input) THEN
			Set oValue = input
		ELSE 
			CreateValueObject()
			oValue.Value = input
		END IF
		IF IsNullOrEmpty(oValue.Format) THEN oValue.Format="Date"
		oTextBox.Value = oValue
	End Property

	Public Property Get CssClass()
		CssClass = sCssClass
	End Property
	Public Property Let CssClass(input)
		sCssClass = input
		oTextBox.CssClass=sCssClass
	End Property

	Public Property Get IsReadonly()
		IsReadonly = bIsReadonly
	End Property
	Public Property Let IsReadonly(input)
		bIsReadonly=CBOOL(input)
		oTextBox.IsReadonly=bIsReadonly
		oCalendarImage.Render=(NOT(bIsReadonly))
	End Property
	
	'Input properties
	Public Function GetCode()
		GetCode=oCollection.GetCode()
	End Function
	
	Public Sub WriteCode()
		response.write Me.GetCode
	End Sub 
End Class
%>
