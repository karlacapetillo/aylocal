<% 
Class CollapsedTable
	'Properties
	Private sDBTableName, sDBTableIdColumn, sParentTable, sCurrentLocation, bURLSet, sURLParameters, sTableMode
	
	'Settings
	
	'Objects
	Private oCollapsedTable, oURLLoader, oContainer, oRelatives, oLabel
	
	'Private Variables
'	Private 
	
	Public Property Get Visible()
		Visible = oCollapsedTable.Visible
	End Property
	Public Property Let Visible(input)
		oCollapsedTable.Visible = input
	End Property

	Public Property Get Object()
		Set Object = oCollapsedTable
	End Property

	Private Sub Class_Initialize()
		sTableMode="List"
		Set oRelatives = [$Relatives](Me)
		Set oURLLoader = new URLLoader
		oURLLoader.AddContent("<b>Cargando...</b>")
		Set oCollapsedTable = new Expander
		oCollapsedTable.TargetObject=oURLLoader
		Set oLabel = nothing
	End Sub
	Private Sub Class_Terminate()
		Set oRelatives = nothing
		Set oURLLoader = nothing
		Set oCollapsedTable = nothing
		Set oLabel = nothing
	End Sub
	
	'Output objects
	Public Property Get Style()
		Set Style = oCollapsedTable.Style
	End Property
	
	Public Property Get Properties()
		Set Properties = oCollapsedTable.Properties
	End Property

	Public Property Get Methods()
		Set Methods = oCollapsedTable.Methods
	End Property

	'Output Properties 
	Public Property Get Label()
		IF oLabel IS NOTHING THEN Set oLabel = [$Label]("", Empty)
		Set Label = oLabel
	End Property

	Public Property Get Id()
		Id = oCollapsedTable.Id
	End Property
	Public Property Let Id(input)
		oCollapsedTable.Id = input
	End Property


	Public Property Get CssClass()
		CssClass = oCollapsedTable.CssClass
	End Property
	Public Property Let CssClass(input)
		oCollapsedTable.CssClass=input
	End Property


	Public Property Get IsReadonly()
		IsReadonly = oCollapsedTable.IsReadonly
	End Property
	Public Property Let IsReadonly(input)
		oCollapsedTable.IsReadonly = input
	End Property


	Public Property Get Relatives()
		Set Relatives = oRelatives
	End Property
	
	Public Property Get Container()
		Set Container = oRelatives.Parent
	End Property
	Public Property Let Container(input)
		oRelatives.Parent = input
	End Property

	Public Property Get DBTableName()
		DBTableName = sDBTableName
	End Property
	Public Property Let DBTableName(input)
		sDBTableName = input
	End Property

	Public Property Get DBTableIdColumn()
		DBTableIdColumn = sDBTableIdColumn
	End Property
	Public Property Let DBTableIdColumn(input)
		sDBTableIdColumn = input
	End Property

	Public Property Get URLParameters()
		URLParameters = sURLParameters
	End Property
	Public Property Let URLParameters(input)
		sURLParameters = input
	End Property

	Public Property Get ParentTable()
		ParentTable = sParentTable
	End Property
	Public Property Let ParentTable(input)
		sParentTable = input
	End Property

	Public Property Get TableMode()
		TableMode = sTableMode
	End Property
	Public Property Let TableMode(input)
		sTableMode = input
	End Property
	
	Public Property Get CurrentLocation()
		CurrentLocation = sCurrentLocation
	End Property
	Public Property Let CurrentLocation(input)
		sCurrentLocation = input
	End Property

	Public Function GetFileNameByMode(sMode)
		DIM sReturnValue
		SELECT CASE UCASE(sMode)
		CASE "FILTER"
			sReturnValue="Filters"
'		CASE "UPDATE", "INSERT"
'			sReturnValue=sMode
		CASE ELSE
			sReturnValue="List"
		END SELECT
		GetFileNameByMode = sReturnValue&".asp"
	End Function
	
	Public Sub SetURL
		oURLLoader.ContentURL="oIdentifier=getIdentifier(this); '"&GetFileNameByMode(sTableMode)&"?requestTable="&sDBTableName&""&sURLParameters&"&PageSize=NULL&Mode="&sTableMode&"&TablePath="&request.queryString("TablePath")&""&sCurrentLocation&"&ParentIdentifier="&sDBTableIdColumn&"&"&sDBTableIdColumn&"='+(oIdentifier.value=='[new]'?'NULL':oIdentifier.value)"
		bURLSet=TRUE
	End Sub
	
	'Public methods
	Public Function GetCode()
		IF NOT(bURLSet) THEN SetURL()
		IF oLabel IS NOTHING THEN
			GetCode=oCollapsedTable.GetCode()
		ELSE
			GetCode=oCollapsedTable.GetCode()&" "&oLabel.GetCode
		END IF
	End Function
	
	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub
End Class 
 %>