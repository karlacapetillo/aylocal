<% 
Function [$Dictionary](ByRef aObjects)
	DIM oDictionary:	Set oDictionary=[&New]("Dictionary", "")
	IF IsArray(aObjects) THEN 
		oDictionary.AddAt aObjects, 0
	ELSEIF IsObject(aObjects) THEN
		DIM oElement
		FOR EACH oElement IN aObjects
			oDictionary.Add oElement
		NEXT
	END IF
	Set [$Dictionary]=oDictionary
End Function
Class Dictionary
	Private oDictionary
	Private Sub Class_Initialize()
		Set oDictionary = CreateObject("System.Collections.SortedList")
	End Sub
	Private Sub Class_Terminate()
		Set oDictionary = nothing
	End Sub
	
	Public Property Get Items()
		Items = oDictionary.Items
	End Property

	Public Property Get Keys()
		Keys = oDictionary.Keys
	End Property

	Public Property Get Count()
		Count = oDictionary.Count
	End Property

	Public Function ToArray()
		DIM oResult:	oResult=Array
		Debugger Me, TypeName(oDictionary.Values)&": "&oDictionary.Count
		IF oDictionary.Count>0 THEN
			oDictionary.Values.CopyTo oResult, 0
			Debugger Me, TypeName(oResult)
			[&Stop] Me
			FOR EACH oResult IN oDictionary.Values
			NEXT
		END IF
		ToArray=oResult
	End Function

	Public Property Get Item(sPosition)
		Assign Item, oDictionary.GetByIndex(sPosition)
	End Property
	
	Public Function Exists(sElementName)
		DIM vResult
		vResult=oDictionary.ContainsKey(sElementName)
		Exists=vResult
	End Function
	
	Public Default Property Get NamedItem(ByVal sItemName)
		IF NOT oDictionary.ContainsKey(sItemName) THEN
			response.write "<strong>"&sItemName&" is not available. Valid names are: </strong><br>"&Me.ValidNames
			response.end
			Exit Property
		ELSE
			IF IsObject(oDictionary(sItemName)) THEN
				Set NamedItem = oDictionary(sItemName)
			ELSE
				NamedItem = oDictionary(sItemName)
			END IF
		END IF
	End Property

	Public Property Get ValidNames()
		DIM sValidNames
		sValidNames=""
		DIM i
		FOR i=0 TO oDictionary.Count-1
			sValidNames=sValidNames&" ["&oDictionary.getKey(i)&"]"
		NEXT
		ValidNames = TRIM(sValidNames)
	End Property

	Public Function Add(ByVal sItemName, ByRef oElement)
		
		IF IsObject(oElement) THEN 
			Set oDictionary(sItemName) = oElement
			ON ERROR  RESUME NEXT:
			Set Add=oDictionary(sItemName): [&Catch] FALSE, TypeName(Me)&".Add", TypeName(oDictionary(sItemName))
			ON ERROR  GOTO 0
		ELSE
			oDictionary(sItemName) = oElement
			ON ERROR  RESUME NEXT:
			Add=oDictionary(sItemName): [&Catch] FALSE, TypeName(Me)&".Add", TypeName(oDictionary(sItemName))
			ON ERROR  GOTO 0
		END IF
	End Function
End Class
 %>