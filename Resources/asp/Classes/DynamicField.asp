<%
'TABLE_CATALOG, TABLE_SCHEMA, TABLE_NAME, COLUMN_NAME, ORDINAL_POSITION, COLUMN_DEFAULT, IS_NULLABLE, DATA_TYPE, CHARACTER_MAXIMUM_LENGTH, CHARACTER_OCTET_LENGTH, NUMERIC_PRECISION, NUMERIC_PRECISION_RADIX, NUMERIC_SCALE, DATETIME_PRECISION, CHARACTER_SET_CATALOG, CHARACTER_SET_SCHEMA, CHARACTER_SET_NAME, COLLATION_CATALOG, COLLATION_SCHEMA, COLLATION_NAME, DOMAIN_CATALOG, DOMAIN_SCHEMA, DOMAIN_NAME

Class DynamicField
	Private bDebug
	Private sName, sTableName, sDataField, bIsIdentity, bIsNullable, sDataType, iMaxLength, sDefaultValue, sValue, oSibling, sAutoRowSpanReferenceDataField, bGroupByColumn, sTotalRowFormula, bAutoRowSpanEnabled, sNullText, sFormula, iDataFieldCollectionIndex, iTextObjectIfSizeGreaterThan, vMinValue, vMaxValue, sDBDefaultValue, sMetadataString, aParentTables, sAppDefaultValue, oContainer, oRelatives', oParentCell
	Private bAllowNegatives
	Private bScaffoldColumn, bRender, bVisible
	
	Private sMode, sCurrentMode, sProperties
	Private sCssClass
	Private iPosition
	
	'Objects
	Private oControl, oControlCollection, oReadonlyControl, oHeaderTag, oOptions, oStyle, aChildDataFields(2048)
	
	'Private Variables
	Private sReturnString, oDataSource, rsRecordSet, oDataField, oPrevDataField, i, iRow, aData(3, 1024), iMaxRecord, dP, iCurrentRecord, oCurrentParentCell, iRowSpan, bAutoSpanDone, sParentTables, cDataFields, tDataField, sTempDataType, sParentTableName, sParentColumnName
	
	Private Sub Class_Initialize()
		Set oRelatives = [$Relatives](Me)
		Me.Mode=NULL
		iTextObjectIfSizeGreaterThan=70
		iMaxLength="14"
		Set oStyle = new Styles
		Set dP=server.CreateObject("Scripting.Dictionary")
		dP("Value")=0
		dP("ParentCell")=1
		dP("Mode")=2
		iCurrentRecord=0 '0 Is for header
		iPosition=-1
		iMaxRecord=0
		Me.ScaffoldColumn=TRUE
		iDataFieldCollectionIndex=0
		Set oReadonlyControl = new Tag
		oReadonlyControl.TagName="label"
		oReadonlyControl.HasClosingTag=TRUE
		Set oHeaderTag = new Tag
		oHeaderTag.TagName="label"
		oHeaderTag.HasClosingTag=TRUE
		Me.MinValue=""
		Me.MaxValue=""
		bAllowNegatives=FALSE
		Me.Render=TRUE
		Me.Visible=TRUE
		bAutoRowSpanEnabled=FALSE
		bGroupByColumn=FALSE
		sTotalRowFormula=""
		Me.AppDefaultValue=NULL
	End Sub
	
	Public Function GetDBMetadata()
		IF Me.DataField="SQLRowNumber" THEN EXIT FUNCTION
		DIM tmp_metadataString, tmp_MaxLength, tmp_IsNullable, tmp_ParentTables, tmp_ParentTableName, tmp_UIHint, tmp_DataType, tmp_IsIdentity, tmp_DisplayName, tmp_ParentColumnName, tmp_DefaultValue, tmp_AllowNegatives, tmp_MinRange, tmp_MaxRange, tmp_Mode, tmp_AutoRowSpan, tmp_AppDefaultValue, tmp_GroupByColumn, tmp_TotalRowFormula ', tmp_ParentDisplayColumn
'response.write "Metadata para "&sDataField&": "&sMetadataString&"<br>"
		tmp_metadataString=Me.MetadataString
		IF TRIM(tmp_metadataString)<>"" THEN
'		response.write "usando metadatastring para "&sDataField&":<br>"&tmp_metadataString&"<br><br>"
			tmp_AppDefaultValue=getMetadata(tmp_metadataString, sDataField, "AppDefaultValue")
			tmp_Mode=getMetadata(tmp_metadataString, sDataField, "Mode")
			tmp_MaxLength=getMetadata(tmp_metadataString, sDataField, "MaxLength")
			tmp_IsNullable=getMetadata(tmp_metadataString, sDataField, "IsNullable")
			tmp_UIHint=getMetadata(tmp_metadataString, sDataField, "UIHint")
			tmp_DataType=getMetadata(tmp_metadataString, sDataField, "DataType")
			tmp_IsIdentity=getMetadata(tmp_metadataString, sDataField, "IsIdentity")
			tmp_DisplayName=getMetadata(tmp_metadataString, sDataField, "DisplayName")
'			response.write "usando metadatastring para "&sDataField&":<br>"&tmp_DisplayName&" VS. "&getMetadata(tmp_metadataString, sDataField, "DisplayName")&" VER "&tmp_metadataString&"<br>"
			tmp_ParentTables=getMetadata(tmp_metadataString, sDataField, "ParentTables")
			tmp_ParentTableName=getMetadata(tmp_metadataString, sDataField, "ParentTableName")
			tmp_ParentColumnName=getMetadata(tmp_metadataString, sDataField, "ParentColumnName")
			tmp_GroupByColumn=getMetadata(tmp_metadataString, sDataField, "GroupByColumn")
			tmp_TotalRowFormula=getMetadata(tmp_metadataString, sDataField, "TotalRowFormula")
			'tmp_ParentDisplayColumn=getMetadata(tmp_metadataString, sDataField, "ParentDisplayColumn")
			tmp_DefaultValue=getMetadata(tmp_metadataString, sDataField, "DefaultValue")
'			response.write sDataField&": "&tmp_DefaultValue&"<br><br>"
			tmp_AllowNegatives=getMetadata(tmp_metadataString, sDataField, "AllowNegatives")
			tmp_MinRange=getMetadata(tmp_metadataString, sDataField, "MinRange")
			tmp_MaxRange=getMetadata(tmp_metadataString, sDataField, "MaxRange")
			tmp_AutoRowSpan=getMetadata(tmp_metadataString, sDataField, "AutoRowSpan")
		ELSE
			response.write "No se encontr� informaci�n para mostrar la columna: "&sDataField
			response.end
'			response.write "usando query para "&sDataField&"<br>"
			Set oDataSource=new RecordSet
			oDataSource.SQLQueryString="SELECT * FROM Metadata WHERE TableName='"&Me.TableName&"' AND ColumnName='"&Me.DataField&"'"
			oDataSource.DataBind()
			IF oDataSource.RecordSet.BOF AND oDataSource.RecordSet.EOF THEN 
				IF bDebug THEN RESPONSE.WRITE "<b>No hay informacion disponible para "&Me.DataField&"</b><br>"
				Me.DataType="char"
				GetDBMetadata=FALSE
				Exit Function
			END IF
			tmp_AppDefaultValue=rsRecordSet("AppDefaultValue")
			tmp_Mode=NULL
			rsRecordSet=oDataSource.RecordSet
			tmp_MaxLength=rsRecordSet("MaxLength")
			tmp_IsNullable=rsRecordSet("IsNullable")
			tmp_UIHint=rsRecordSet("UIHint")
			tmp_DataType=rsRecordSet("DataType")
			tmp_IsIdentity=rsRecordSet("IsIdentity")
			tmp_DisplayName=rsRecordSet("DisplayName")
			tmp_ParentTables=rsRecordSet("ParentTables")
			tmp_ParentTableName=rsRecordSet("ParentTableName")
			tmp_ParentColumnName=rsRecordSet("ParentColumnName")
			tmp_GroupByColumn=rsRecordSet("GroupByColumn")
			tmp_TotalRowFormula=rsRecordSet("TotalRowFormula")
			'tmp_ParentDisplayColumn=rsRecordSet("ParentDisplayColumn")
			tmp_DefaultValue=rsRecordSet("DefaultValue")
			tmp_AllowNegatives=rsRecordSet("AllowNegatives")
			tmp_MinRange=rsRecordSet("MinRange")
			tmp_MaxRange=rsRecordSet("MaxRange")
			tmp_AutoRowSpan=rsRecordSet("AutoRowSpan")
		END IF
'		RESPONSE.WRITE "tmp_MaxLength: "& isempty(tmp_MaxLength) &"<br>"
		iMaxLength=tmp_MaxLength
		IF NOT ISNULL(tmp_AppDefaultValue) THEN Me.AppDefaultValue=EVAL(tmp_AppDefaultValue)
		IF NOT ISNULL(tmp_AutoRowSpan) THEN Me.AutoRowSpanEnabled=CBOOL(tmp_AutoRowSpan)
		IF UCASE(Me.Mode)<>UCASE("Filter") AND NOT ISNULL(tmp_Mode) THEN Me.Mode=tmp_Mode
		IF NOT ISNULL(tmp_MinRange) THEN Me.MinValue=tmp_MinRange
		IF NOT ISNULL(tmp_MaxRange) THEN Me.MaxValue=tmp_MaxRange
		IF ISNULL(iMaxLength) THEN iMaxLength=1 END IF
		Me.IsNullable=CBOOL(dMainDictionary(UCASE(CSTR(tmp_IsNullable))))
		IF NOT ISNULL(tmp_GroupByColumn) THEN Me.GroupByColumn=CBOOL(tmp_GroupByColumn)
		IF NOT ISNULL(tmp_TotalRowFormula) THEN Me.TotalRowFormula=TRIM(tmp_TotalRowFormula)
		sParentTables=tmp_ParentTables
		sParentTableName=tmp_ParentTableName
		IF NOT ISNULL(tmp_UIHint) THEN
			Me.DataType=tmp_UIHint
		ELSE
			sTempDataType=tmp_DataType
			IF CBOOL(tmp_IsIdentity) THEN
				Me.IsIdentity=CBOOL(tmp_IsIdentity) 'IsIdentity must be set before DataType property for it might change DataType automatically
			ELSEIF NOT Me.Visible THEN
				Me.DataType="HiddenField"
			ELSEIF UCASE(sTempDataType)=UCASE("timestamp") THEN 
				Me.ScaffoldColumn=FALSE
				GetDBMetadata=FALSE
				EXIT Function
			ELSEIF ISNULL(sParentTableName) OR TRIM(sParentTableName)="" THEN
				Me.DataType=TRIM(sTempDataType)
			ELSE
				Dim i, oDropDownList, aInfo, oPrevious
				Me.DataType="Collection"
'				oControl.TextValue=RTRIM(sDisplayColumn)
				Set oPrevious = nothing
'				response.write sDataField&": "&sParentTables&"<br>"
				aParentTables=SPLIT(sParentTables, "@/@")
				oControl.Separator="<br>"
				FOR i=UBOUND(aParentTables) TO 0 STEP -1
					Set oDropDownList = new AjaxDropDownList
					aInfo=split(aParentTables(i), "-")
					oDropDownList.Catalog=aInfo(0)
					oDropDownList.DataValue=aInfo(1)
					oDropDownList.Name=aInfo(2)
					IF i<>0 THEN
						oDropDownList.Properties.SetProperty "isSubmitable", "false"
					END IF
					IF NOT oPrevious IS nothing THEN 
						oDropDownList.ParentControlId = oPrevious.Name
						oDropDownList.Filters = "oParent=getParent('TD', this).all('"&oPrevious.Name&"'); ' AND "&oPrevious.Name&"='+(getVal(oParent) || 'NULL');"
					END IF
					oControl.AddContent(oDropDownList)
'					response.write sDataField&" - "&aInfo(0)&", "&aInfo(1)&"<br>"
					Set oPrevious=oDropDownList
				NEXT
				GetDBMetadata=TRUE
'				Me.DisplayColumn=tmp_ParentDisplayColumn
'				Me.ParentColumnName=tmp_DisplayName
				IF NOT ISNULL(tmp_DisplayName) THEN
					Me.HeaderText=tmp_DisplayName
				ELSE
					Me.HeaderText=ToTitleFromPascal(sParentTableName)
				END IF
				Exit Function
			END IF
		END IF
'		response.write sDataField&": "&tmp_DisplayName&"<br>"
'		''@'ON ERROR RESUME NEXT
		Me.DBDefaultValue=tmp_DefaultValue
		IF ERR.Number<>0 THEN 
			RESPONSE.WRITE "Error en "&Me.DataField&": "&Me.DataType&" ("&TypeName(oControl)&"), "&Err.Description
			RESPONSE.END
		END IF
		Me.AllowNegatives=tmp_AllowNegatives

		IF NOT ISNULL(tmp_DisplayName) THEN
			Me.HeaderText=tmp_DisplayName
		ELSE
			Me.HeaderText=ToTitleFromPascal(sDataField)
		END IF
'		Dim Counter
'	    FOR counter=0 to oDataSource.RecordSet.Fields.Count-1 
'			response.write rsRecordSet(counter).name &",&nbsp;"
'		NEXT
'		response.write "<br>"
''		response.write oDataSource.RecordSet.Fields()&"<br>"
'		response.write oDataSource.RecordSet.GetString(, , "#c#", "#r#" & vbcrlf, "-null-")
'		response.end
		GetDBMetadata=TRUE
	End Function
	
	Public Property Get MetadataString()
		MetadataString = sMetadataString
	End Property
	Public Property Let MetadataString(input)
		sMetadataString = input
	End Property

	Public Property Get DBDefaultValue()
		DBDefaultValue = sDBDefaultValue
	End Property
	Public Property Let DBDefaultValue(input)
		sDBDefaultValue = input
		IF ISNULL(sDBDefaultValue) THEN 
			oControl.Object.Properties.RemoveProperty "DBDefaultValue"
		ELSE
			sDBDefaultValue=REPLACE(TRIM(sDBDefaultValue), "&#x0D", " ")
			oControl.Object.Properties.SetProperty "DBDefaultValue", TRIM(sDBDefaultValue)
		END IF
	End Property

	Public Property Get AppDefaultValue()
		AppDefaultValue = sAppDefaultValue
	End Property
	Public Property Let AppDefaultValue(input)
		sAppDefaultValue = input
	End Property

	Public Property Get AllowNegatives()
		AllowNegatives = bAllowNegatives
	End Property
	Public Property Let AllowNegatives(input)
		bAllowNegatives = input
		IF ISNULL(bAllowNegatives) THEN 
			oControl.Object.Properties.RemoveProperty "allowNegatives"
		ELSE
			oControl.Object.Properties.SetProperty "allowNegatives", dMainDictionary("javascript_"&dMainDictionary(LCASE(CSTR(CBOOL(bAllowNegatives)))))
		END IF
	End Property

	Public Property Get Control()
		Set Control = oControl
	End Property
	

	Public Property Get GroupByColumn()
		GroupByColumn = bGroupByColumn
	End Property
	Public Property Let GroupByColumn(input)
		bGroupByColumn = input
	End Property


	Public Property Get TotalRowFormula()
		TotalRowFormula = sTotalRowFormula
	End Property
	Public Property Let TotalRowFormula(input)
		sTotalRowFormula = input
	End Property


	Public Property Get MinValue()
		MinValue = vMinValue
	End Property

	Public Property Get MaxValue()
		MaxValue = vMaxValue
	End Property

	Public Property Get TextObjectIfSizeGreaterThan()
		TextObjectIfSizeGreaterThan = iTextObjectIfSizeGreaterThan
	End Property

	Public Property Get DataFieldCollectionIndex()
		DataFieldCollectionIndex = iDataFieldCollectionIndex
	End Property

	Public Property Get AutoRowSpanEnabled()
		AutoRowSpanEnabled = bAutoRowSpanEnabled
	End Property

	Public Property Get AutoRowSpanReferenceDataField()
		AutoRowSpanReferenceDataField = sAutoRowSpanReferenceDataField
	End Property

	Public Property Get NullText()
		NullText = sNullText
	End Property

	Public Property Get ScaffoldColumn()
		ScaffoldColumn = bScaffoldColumn
	End Property

	Public Property Get Position()
		Position = iPosition
	End Property

	Public Property Get CssClass()
		CssClass = sCssClass
	End Property
	
	Public Property Get Formula()
		Formula = sFormula
	End Property
	
	Public Property Get ChildrenCount()
		ChildrenCount = iChildrenCount
	End Property
	
	Public Property Get Style()
		Set Style = oStyle
	End Property
	
	Public Property Let MinValue(input)
		vMinValue = input
	End Property

	Public Property Let MaxValue(input)
		vMaxValue = input
	End Property

	Public Property Let TextObjectIfSizeGreaterThan(input)
		iTextObjectIfSizeGreaterThan = input
	End Property

	Public Property Let DataFieldCollectionIndex(input)
		iDataFieldCollectionIndex = input
	End Property

	Public Property Let NullText(input)
		sNullText = input
	End Property

	Public Property Let AutoRowSpanEnabled(input)
		bAutoRowSpanEnabled = input
	End Property

	Public Property Let AutoRowSpanReferenceDataField(input)
		IF bAutoSpanDone THEN 
			Err.Raise 1, "ASP 101", "AutoRowSpanReferenceDataField Property must be set before performing AutoRowSpan method"
			response.end
		END IF
		sAutoRowSpanReferenceDataField = input
	End Property

	Public Property Let ScaffoldColumn(input)
		bScaffoldColumn = input
		DoNotRender()
	End Property
	
	Public Sub DoNotRender()
		FOR i=iMaxRecord TO 0 STEP -1
			IF IsObject(aData(dP("ParentCell"), i)) THEN 
				aData(dP("ParentCell"), i).Render=Me.ScaffoldColumn
				aData(dP("ParentCell"), i).Visible=Me.Visible
			END IF
		NEXT
	End Sub
	
	Public Property Let Position(input)
		iPosition = input
	End Property

	Public Property Let CssClass(input)
		sCssClass=input
	End Property
	
	Public Property Let Formula(input)
		sFormula = input
	End Property

	Public Property Let TableName(ByVal input)
		sTableName=input
	End Property
	Public Property Get TableName()
		TableName=sTableName
	End Property
	
	Public Property Let Render(input)
		bRender=input
	End Property
	Public Property Get Render()
		Render=bRender
	End Property
	
	Public Property Let Visible(input)
		bVisible=input
		DoNotRender()
	End Property
	Public Property Get Visible()
		Visible=bVisible
	End Property
	
	Public Property Get Name()
		Name = sName
	End Property
	Public Property Let Name(ByVal input)
		sName = input
	End Property
	
	Public Property Get IsIdentity()
		IsIdentity = bIsIdentity
	End Property

	Public Property Let IsIdentity(ByVal input)
		bIsIdentity = input
		IF bIsIdentity THEN
			Me.DataType="identity"
		END IF
	End Property
	
	Public Property Get IsNullable()
		IsNullable = bIsNullable
	End Property
	Public Property Let IsNullable(ByVal input)
		bIsNullable = input
	End Property
	
	Public Property Get DataField()
		DataField = sDataField
	End Property
	Public Property Let DataField(ByVal input)
		sDataField = input
	End Property

	Public Property Get HeaderText()
		HeaderText = aData(dP("Value"), 0)
	End Property
	Public Property Let HeaderText(ByVal input)
		aData(dP("Value"), 0) = input
	End Property
	
	Public Property Get Value()
		Value=aData(dP("Value"), iCurrentRecord)
	End Property 

	Public Property Get ValueAt(iRecord)
		'Resolver para que no puedan poner en el 0
		ValueAt=aData(dP("Value"), iRecord)
	End Property 

	Public Property Let Value(input)
'		CurrentRecordError()
		aData(dP("Value"), iCurrentRecord)=input
	End Property
	
	Public Property Get DefaultValue()
		DefaultValue = sDefaultValue
	End Property
	Public Property Let DefaultValue(ByVal input)
		sDefaultValue = input
	End Property
	
	Private sFormatedValue
	Public Property Get FormatedValue()
		
	End Property
'	Public Property Let FormatedValue(ByVal input)
'		FormatedValue = input
'	End Property
	
	Public Property Get Mode()
		Mode = sMode
	End Property
	
	Public Property Let Mode(input)
		sMode = TRIM(input)
		sCurrentMode = TRIM(input)
		IF UCASE(sMode)=UCASE("hidden") THEN Me.Visible=FALSE
'		response.write sDataField&": "&sMode&"<br>"
	End Property
	
	Public Property Get Relatives()
		Set Relatives = oRelatives
	End Property
	
	Public Property Get Container()
		Set Container = oRelatives.Parent
	End Property
	Public Property Let Container(input)
		oRelatives.Parent = input
	End Property

	Public Property Get ParentCell()
		IF IsObject(aData(dP("ParentCell"), iCurrentRecord)) THEN
			Set ParentCell = aData(dP("ParentCell"), iCurrentRecord)
		ELSE
			Set ParentCell = nothing
		END IF
	End Property
	
	Public Sub SetParentCell(ByRef input)
		Me.ParentCell=input
	End Sub
	
	Public Property Let ParentCell(input)
		Set aData(dP("ParentCell"), iCurrentRecord) = input
		aData(dP("ParentCell"), iCurrentRecord).CurrentRecord = iCurrentRecord
		aData(dP("ParentCell"), iCurrentRecord).Render=Me.ScaffoldColumn
		aData(dP("ParentCell"), iCurrentRecord).Visible=Me.Visible
	End Property

	Public Function GetSiblingDataField(ByVal DataFieldName)
		Set GetSiblingDataField=aData(dP("ParentCell"), iCurrentRecord).ParentRow.DataFields.item(DataFieldName)
	End Function
	
	Public Function GetSiblingDataFieldByIndex(ByVal iIndex, ByVal bRelative)
		IF bRelative THEN
			iIndex=Me.GetDataFieldCollectionIndex()+iIndex
		END IF
		cDataFields=aData(dP("ParentCell"), iCurrentRecord).ParentRow.DataFields.items
		Set GetSiblingDataField=cDataFields(iIndex)
	End Function
	
	Public Function GetDataFieldCollectionIndex()
		FOR EACH tDataField IN aData(dP("ParentCell"), iCurrentRecord).ParentRow.DataFields.items
			IF Me=tDataField THEN EXIT FOR
			iDataFieldCollectionIndex=iDataFieldCollectionIndex+1
		NEXT
		GetDataFieldCollectionIndex=iDataFieldCollectionIndex
	End Function

	Public Sub SetMode(input)
		CurrentRecordError()
		IF input<>sMode THEN aData(dP("Mode"), iCurrentRecord)=input
	End Sub
	
	Public Function GetMode()
		CurrentRecordError()
		IF aData(dP("Mode"), iCurrentRecord)<>"" THEN 
			GetMode = aData(dP("Mode"), iCurrentRecord)
		ELSE
			GetMode = sMode
		END IF
	End Function
	
	Public Property Get DataType()
		DataType = sDataType
	End Property

	Public Property Let DataType(ByVal input)
		IF TRIM(Me.Mode)="" THEN
			Err.Raise 1, "ASP 101", "Dynamic Field's mode must be set first."
			response.end
		END IF
'		Me.Visible=TRUE
		IF NOT Me.Visible THEN
			sDataType = RTRIM(input)
			Set oControl = new HiddenField
		ELSEIF NOT UCASE(RTRIM(sDataType)) = UCASE(RTRIM(input)) THEN
			sDataType = RTRIM(input)
			'Changes type if necessary
			IF INSTR(sDataType, "UI:")<>0 THEN
				DIM sControl
				EXECUTE("Set oControl = new "&REPLACE(sDataType, "UI:", ""))
				sDataType = oControl.DataType
'				EXECUTE("Set oReadonlyControl = new "&REPLACE(sDataType, "UI:", ""))
			ELSE
				IF CSNG(iMaxLength)>=CSNG(iTextObjectIfSizeGreaterThan) THEN 
					SELECT CASE UCASE(sDataType)
					CASE UCASE("char"), UCASE("nchar")
						sDataType="text"
					END SELECT
				END IF
				SELECT CASE UCASE(sDataType)
				CASE UCASE("HiddenField")
					Set oControl = new HiddenField
					Me.Visible=FALSE
					Set oReadonlyControl = new HiddenField
				CASE UCASE("insertButton"), UCASE("updateButton")
					Set oControl = new ImageButton
					Set oReadonlyControl = new ImageButton
				CASE UCASE("Collection")
					Set oControl = new Collection
'					Me.Mode="readonly"
'					oControl.Mode="readonly"
				CASE UCASE("AjaxDropDownList")
					Set oControl = new AjaxDropDownList
					oControl.TurnOn=TRUE
				CASE UCASE("RangeSelector")
					Set oControl = new RangeSelector
					oControl.MinValue=Me.MinValue
					oControl.MaxValue=Me.MaxValue
'					oControl.AllowNegatives=bAllowNegatives
				CASE UCASE("FileUploader")
					Set oControl = new FileUploader
				CASE UCASE("CheckDate")
					Set oControl = new CheckDate
				CASE UCASE("identity")
					Set oControl = new Checkbox
					oControl.Id="identifier"
					Me.IsIdentity=TRUE
					Me.Visible=FALSE
				CASE UCASE("List")
					Set oControl = new BulletedList
				CASE UCASE("bit")
	'				IF NOT bIsNullable THEN
	'					Set oControl = new Checkbox
	'				ELSE
					Set oControl = new DropDownList
					DIM SelectOption(2,2)
					SelectOption(0,0)="NULL"
					SelectOption(1,0)="[Auto]"
					SelectOption(0,1)="1"
					SelectOption(1,1)="S�"
					SelectOption(0,2)="0"
					SelectOption(1,2)="No"
					oControl.FillComboWithArray SelectOption
	'				END IF
				CASE UCASE("password")
					Set oControl = new Password
				CASE UCASE("text"), UCASE("ntext"), UCASE("varchar"), UCASE("nvarchar")
					Set oControl = new TextBox
					oControl.TextMode="multiline"
				CASE UCASE("float")
					Set oControl = new TextBox
					oControl.Size=14
					oControl.MaxLength=14
				CASE UCASE("money"), UCASE("smallmoney")
					IF UCASE(Me.Mode)=UCASE("Filter") THEN
						Set oControl=new MoneyRange
						oControl.Object.Mode="Insert"
					ELSE
						Set oControl = new TextBox
						oControl.Size=14
						oControl.MaxLength=14
					END IF
				CASE UCASE("int")
					Set oControl = new TextBox
					oControl.Size=8
					oControl.MaxLength=8
				CASE UCASE("tinyint")
					Set oControl = new TextBox
					oControl.Size=3
					oControl.MaxLength=3
				CASE UCASE("datetime"), UCASE("date"), UCASE("smalldatetime"), UCASE("smalldate")
'					response.write Me.Mode
					IF UCASE(Me.Mode)=UCASE("Filter") THEN
						Set oControl=new DateRange
						oControl.Object.Mode="Insert"
					ELSE
						Set oControl=new DateBox
						oControl.Mode=sCurrentMode
					END IF
		'				oControl.HasCheckBox=TRUE
				CASE ELSE
					Set oControl = new TextBox
					oControl.Size=iMaxLength
					oControl.MaxLength=iMaxLength
				END SELECT
			END IF
			IF NOT ISOBJECT(oControl) THEN
				response.write oControl &" OBJECT IS NOT DEFINED FOR "&sDatatype
				response.end
			END IF
			IF oControl IS nothing THEN 
				response.write oControl &" OBJECT IS NOT DEFINED"
				response.end
			END IF
'			''@'ON ERROR RESUME NEXT 
			IF Me.IsIdentity OR ucase(sDataField)=UCASE("MontoTope") THEN
				oControl.Mode="update"
				Me.Mode="update"
			ELSE
				oControl.Object.Mode=Me.Mode
			END IF
			IF ERR.Number<>0 THEN 
				RESPONSE.WRITE "la propiedad no est� definida para el tipo de objeto "&TypeName(oControl)&"("&TypeName(oControl.Object)&")"
				response.end
			END IF
			SELECT CASE UCASE(sDataType)
			CASE UCASE("List"), UCASE("insertButton"), UCASE("updateButton"), UCASE("Collection")
			CASE ELSE
				oControl.Name=sDataField
			END SELECT
			IF sCssClass="" THEN sCssClass=dDefaultCssClass(UCASE(sDataType))
	
			IF sDataType<>"" AND UCASE(sDataType)<>UCASE("AjaxDropDownList") THEN oControl.CssClass=sCssClass 
			
	'		response.write sDataField&": "&sDataType&"<br>"
		END IF
	End Property
	
	Public Property Get MaxLength()
		MaxLength = iMaxLength
	End Property
	Public Property Let MaxLength(ByVal input)
		iMaxLength = input
	End Property
	
	Public Property Get CurrentRecord()
		CurrentRecord = iCurrentRecord
	End Property

	Public Property Let CurrentRecord(input)
		iCurrentRecord = input
		IF iCurrentRecord>iMaxRecord THEN iMaxRecord=iCurrentRecord
	End Property
	
	Public Sub CurrentRecordError()
		IF iCurrentRecord=0 THEN
			Err.Raise 1, "ASP 101", "CurrentRecord Property must be set first to a value greater than 0"
			response.end
		END IF
	End Sub
	
	'Public Methods
	Public Sub AutoRowSpan()
		iRowSpan=1
		IF bAutoSpanDone THEN Exit Sub
		FOR i=iMaxRecord TO 1 STEP -1
			IF not ISOBJECT(aData(dP("ParentCell"), i)) THEN 
				response.write "ParentCell is not an object for "&Me.DataField&" at record "&i&"<br>"
'				response.end
			ELSEIF aData(dP("ParentCell"), i) IS NOTHING THEN 
				response.write "ParentCell is not defined for "&Me.DataField&" at record "&i&"<br>"
'				response.end
			ELSE
				IF aData(dP("Value"), i)<>aData(dP("Value"), i-1) OR i=1 THEN 
					aData(dP("ParentCell"), i).RowSpan=iRowSpan
					iRowSpan=1
				ELSEIF sAutoRowSpanReferenceDataField<>"" THEN
					Set oSibling=GetSiblingDataField(sAutoRowSpanReferenceDataField)
					IF oSibling.ValueAt(i)<>oSibling.ValueAt(i-1) THEN
						aData(dP("ParentCell"), i).RowSpan=iRowSpan
						iRowSpan=1
					ELSE
						aData(dP("ParentCell"), i).Render=FALSE
						iRowSpan=iRowSpan+1
					END IF
				ELSE
					aData(dP("ParentCell"), i).Render=FALSE
					iRowSpan=iRowSpan+1
				END IF
			END IF
		NEXT
		bAutoSpanDone=TRUE
	End Sub

	Public Function GetCode()
		IF sMode="" THEN
			Err.Raise 1, "ASP 101", "Mode property for DataField "&sDataField&" is required (DataType: "&sDataType&")"
			RESPONSE.END
		END IF
		IF bIsNullable OR Me.DBDefaultValue<>"" THEN 
			oControl.Style.BackgroundColor="lightgoldenrodyellow"
		ELSE
			IF NOT Me.IsIdentity THEN
				oControl.Object.Properties.SetProperty "IsRequired", "true"
			END IF
			oControl.Object.Properties.SetProperty "notAllowedValue", "'NULL'"
		END IF
		sReturnString=""
		sValue=Me.Value
		IF sValue="" THEN sValue=NULL
		IF IsObject(Me.ParentCell) THEN
			IF NOT Me.ParentCell IS nothing THEN 
				IF NOT ISNULL(Me.ParentCell.CurrentRecord) THEN 
					iCurrentRecord=Me.ParentCell.CurrentRecord
				END IF
			END IF
		END IF
		IF iCurrentRecord=0 THEN
			oHeaderTag.AddContent(aData(dP("Value"), 0)) '&": "&sDataType
			GetCode = oHeaderTag.GetCode()
			EXIT Function
		END IF

		'Check for required properties
'		SELECT CASE UCASE(sDataType)
'		CASE UCASE("AjaxDropDownList")
'			IF ISNULL(sDisplayColumn) OR ISEMPTY(sDisplayColumn) THEN 
'				Err.Raise 1, "ASP 101", "DisplayColumn is not set for table "&sParentTableName&" and no default column could be found. (SQL: '"&oDataSource.SQLQueryString&":"&rsRecordSet("DisplayColumn")&"<br>"&")"
'				RESPONSE.END
'			END IF
'		END SELECT

		'Reset Some Properties for Controls 
		Dim i, oCollection, aValues, oDropDownList, aInfo
		SELECT CASE UCASE(TypeName(oControl))
		CASE UCASE("AjaxDropDownList"), UCASE("DropDownList")
			oControl.ClearSelections()
			oControl.ResetProperties()
		CASE UCASE("Collection")
			oCollection=oControl.GetItems()
			FOR i=UBOUND(oCollection) TO 1 STEP -1
				Set oDropDownList=oCollection(i)
				oDropDownList.ClearItems()
				oDropDownList.ResetProperties()
			NEXT
		END SELECT
		
		IF IsNull(sValue) AND NOT IsNull(Me.AppDefaultValue) THEN
			sValue=Me.AppDefaultValue
		END IF
		
		'Format value
		sCurrentMode=Me.GetMode
'		response.write sDataField&"("&sDataType&"): "&scurrentMode&"<br>"
		IF sCurrentMode="" THEN sCurrentMode=sMode
		IF NOT(IsNull(sValue)) THEN
			SELECT CASE UCASE(sDataType)
			CASE UCASE("identity")
			CASE UCASE("List")
				IF sValue<>"" THEN oControl.NewOption sValue
			CASE UCASE("bit")
				sValue=dMainDictionary(LCASE(CSTR(CBOOL(sValue))))
			CASE UCASE("text"), UCASE("ntext"), UCASE("varchar"), UCASE("nvarchar")
			CASE UCASE("float")
				sValue=CCUR(sValue)
			CASE UCASE("money"), UCASE("smallmoney")
				sValue=FORMATCURRENCY(sValue)
			CASE UCASE("int"), UCASE("RangeSelector")
				sValue=CINT(sValue)
			CASE UCASE("tinyint")
				sValue=CINT(sValue)
			CASE UCASE("datetime"), UCASE("date"), UCASE("smalldatetime"), UCASE("smalldate")
				sValue=FORMATDATETIME(CDATE(sValue),2)
			CASE UCASE("insertButton")
				oControl.ImageURL=sImageLibraryPath & sValue
				oControl.CommandText="OpenLink('../DynamicData/Insert.asp&requestTable="&Me.GetSiblingDataField("TableName").ValueAt(iCurrentRecord)&"', false, false)"
			CASE UCASE("updateButton")
				oControl.ImageURL= sImageLibraryPath & sValue 'Request.serverVariables("PATH_INFO") & sImageLibraryPath & sValue
				oControl.CommandText="OpenLink('../DynamicData/Insert.asp&requestTable="&Me.GetSiblingDataField("TableName").ValueAt(iCurrentRecord)&"', false, false)"
			CASE UCASE("AjaxDropDownList"), UCASE("DropDownList"), UCASE("Collection")
			CASE UCASE("nchar"), UCASE("char"), UCASE("HiddenField"), UCASE("password"), UCASE("FileUploader"), UCASE("CheckDate")
			CASE ELSE
				IF UCASE(sCurrentMode)<>UCASE("readonly") THEN
					GetCode="[DataType: "&sDataType&" not supported]"
					EXIT Function
				END IF
			END SELECT
		ELSEIF UCASE(sCurrentMode)<>UCASE("readonly") THEN
			SELECT CASE UCASE(TypeName(oControl))
			CASE UCASE("DropDownList"), UCASE("AjaxDropDownList"), UCASE("Collection")
				sValue="NULL"
			CASE ELSE
				IF UCASE(sCurrentMode)=UCASE("readonly") THEN
					sValue=sNullText
				ELSEIF (UCASE(sCurrentMode)=UCASE("filter") OR UCASE(sCurrentMode)=UCASE("insert") OR UCASE(sCurrentMode)=UCASE("update") ) AND UCASE(sDataType)=UCASE("Identity") THEN 
					sValue="[new]"
				ELSE
					sValue=""
				END IF
			END SELECT
		END IF
'		response.write sDataField&".value: "&sValue&"<br>"
		'Selects the right render mode
		SELECT CASE UCASE(sCurrentMode)
		CASE UCASE("readonly")
			SELECT CASE UCASE(sDataType)
			CASE UCASE("insertButton"), UCASE("updateButton")
				Set oReadonlyControl = oControl
			CASE ELSE
				IF TypeName(oReadonlyControl)="Tag" THEN
					IF UCASE(sDataType)=UCASE("Collection") THEN
						oCollection=oControl.GetItems()
						IF NOT(sValue="NULL" OR ISNULL(sValue)) THEN
							DIM temp_sValue
							IF INSTR(sValue, ":")>0 THEN
								temp_sValue=""
								aValues=SPLIT(sValue, "@/@")
								FOR i=0 TO UBOUND(aValues)
									aInfo=split(aValues(UBOUND(aValues)-i), ":")
									IF i>0 THEN temp_sValue=temp_sValue&" / "
									temp_sValue=temp_sValue&aInfo(1)
								NEXT
								sValue=temp_sValue
							END IF
						ELSE
							FOR i=UBOUND(oCollection) TO 0 STEP -1
								Set oDropDownList=oCollection(i)
								oDropDownList.NewOption "NULL", "Sin Seleccionar..."
							NEXT
						END IF
					ELSE
						Dim oHiddenControl
						Set oHiddenControl = new HiddenField
						oHiddenControl.Value=sValue
						oHiddenControl.Name=Me.DataField
						oReadonlyControl.AddContent(oHiddenControl)
					END IF
					oReadonlyControl.ClearContent()
					oReadonlyControl.AddContent(sValue)
				ELSE
					oReadonlyControl.Value=sValue
				END IF
			END SELECT
			sReturnString=sReturnString& oReadonlyControl.GetCode()
		CASE UCASE("hidden")
			Set oHiddenControl = new HiddenField
			oHiddenControl.Value=sValue
			oHiddenControl.Name=Me.DataField
			Me.Visible=FALSE
			sReturnString=sReturnString& oHiddenControl.GetCode()
		CASE UCASE("filter"), UCASE("insert"), UCASE("update")
			IF UCASE(sDataType)=UCASE("Collection") THEN
				oCollection=oControl.GetItems()
'				response.write sDataField&": "&sValue&"<br>"
				IF NOT(sValue="NULL" OR ISNULL(sValue)) THEN
					IF INSTR(sValue, ":")>0 THEN
						aValues=SPLIT(sValue, "@/@")
						FOR i=UBOUND(aValues) TO 0 STEP -1
							Set oDropDownList=oCollection(i+1)
							aInfo=split(aValues(UBOUND(aValues)-i), ":")
							oDropDownList.NewOption aInfo(0), aInfo(1)
						NEXT
					END IF
				ELSE
					FOR i=UBOUND(oCollection) TO 1 STEP -1
						Set oDropDownList=oCollection(i)
						oDropDownList.NewOption "NULL", "Sin Seleccionar..."
					NEXT
				END IF
			ELSEIF UCASE(sDataType)=UCASE("AjaxDropDownList") AND NOT(sValue="NULL") AND NOT(Me.Visible) THEN
				oControl.Control.NewOption sValue, sValue
			ELSEIF UCASE(sDataType)<>UCASE("List") AND UCASE(sDataType)<>UCASE("Collection") THEN 
				oControl.Value=REPLACE(sValue, vbcrlf, "")
			END IF
			sReturnString=sReturnString& oControl.GetCode()
		CASE ELSE
			Err.Raise 1, "ASP 101", "Mode "&sCurrentMode&" is not supported"
			response.end
		END SELECT
		GetCode=sReturnString
	End Function
	
	Public Sub Step(iSteps)
		iCurrentRecord=iCurrentRecord+iSteps
	End Sub
	
	Public Sub MoveFirst()
		iCurrentRecord=0
	End Sub
	
	Public Sub WriteCode()
'		Set oControl = new Tag
		IF NOT bRender THEN 
			'Generate="" 
			EXIT Sub'Function
		END IF
		response.write GetCode()
	End Sub
	
	Private Sub Class_Terminate()	 
	End Sub
End Class
%>