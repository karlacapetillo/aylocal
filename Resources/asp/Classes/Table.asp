<%
Function [$Table](ByVal sParameters, ByRef aRows)
	DIM oTable:	Set oTable=[&New]("Table", sParameters)
	SELECT CASE TypeName(aRows)
	CASE "ArrayList"
		oTable.AddRows(aRows.ToArray())
	CASE ELSE
		oTable.AddRows(aRows)
	END SELECT
	Set [$Table]=oTable
	SET oTable = NOTHING
End Function
Class Table
	'Properties
	Private sId, bSupportsPaging, nTableIdentifier
	
	'Settings
	Private sFieldBlank
	
	'Objects
	Private oTable, oColumns, oHeaders, oTableBody, oFooters, oDisplayTime, oHeadersCollection, oRelatives, oProgress, oRows, bIsMainTable
	
	'Private Properties
	Private sTableCode
	
	Public Property Get Visible()
		Visible = oTable.Visible
	End Property
	Public Property Let Visible(input)
		oTable.Visible = input
	End Property

	Private Sub Class_Terminate()
		Set oRelatives = nothing
		Set oRows = nothing
		Set oDisplayTime = nothing
		Set oTable = nothing
		Set oHeaders = nothing
		Set oTableBody = nothing
		Set oFooters = nothing 
		Set oProgress = nothing
	End Sub
	
	Private Sub Class_Initialize()
		Me.TableIdentifier = RandomNumber(1000)
		Set oProgress = new Progress
		Set oDisplayTime = new StopWatch
'		Set oRelatives = [$Relatives](Me)
		Set oTable = new Tag	'		Set oTable = oRelatives.Content
		'oRelatives.Content = oTable
		oTable.TagName="table"
		Set oHeaders = [$Tag]("THEAD")
		oTable.AddContent oHeaders
		Set oTableBody = new Collection
		oTable.AddContent oTableBody
		Set oFooters = nothing 
		bIsMainTable=TRUE
	End Sub
    
	'Interfaces
	Public Property Get Style()
		Set Style = oTable.Style
	End Property

	Public Property Get Properties()
		Set Properties = oTable.Properties
	End Property

	Public Property Get ProgressBar()
		Set ProgressBar = oProgress
	End Property

	Public Property Get Relatives()
		Set Relatives = oRelatives
	End Property
	
	Public Property Get Container()
		Set Container = Me.Relatives.Parent
	End Property
	Public Property Let Container(input)
		Me.Relatives.Parent = input
	End Property

	'Properties
	Public Property Get Border()
		Border = oTable.Properties.GetProperty("border")
	End Property
	Public Property Let Border(input)
		oTable.Properties.SetProperty "border", input
	End Property

	Public Property Get BodyGroupCount()
		BodyGroupCount = oTableBody.ItemsCount()
	End Property

	Public Property Get TableIdentifier()
		TableIdentifier = nTableIdentifier
	End Property
	Public Property Let TableIdentifier(input)
		nTableIdentifier = input
	End Property

	Public Property Get ProgressBarControlId()
		ProgressBarControlId = oProgress.ProgressBarControlId
	End Property
	Public Property Let ProgressBarControlId(input)
		oProgress.ProgressBarControlId = input
	End Property

	Public Property Get IsMainTable()
		IsMainTable = bIsMainTable
	End Property
	Public Property Let IsMainTable(input)
		bIsMainTable = input
	End Property

	Public Property Get IsReadonly()
		IsReadonly = oTable.IsReadonly
	End Property
	Public Property Let IsReadonly(input)
		oTable.IsReadonly = input
	End Property


	Public Property Get Id()
		Id = oTable.Id
	End Property
	Public Property Let Id(input)
		oTable.Id = input
	End Property


	Public Property Get CssClass()
		CssClass = oTable.CssClass
	End Property
	Public Property Let CssClass(input)
		oTable.CssClass = input
	End Property


	Public Property Get Render()
		Render = oTable.Render
	End Property
	Public Property Let Render(input)
		oTable.Render = input
	End Property


	Public Property Get FieldBlank()
		FieldBlank = sFieldBlank
	End Property
	Public Property Let FieldBlank(input)
		sFieldBlank = input
	End Property

	'Methods
	Public Sub AddGroup()
		Me.AddNamedGroup("")
	End Sub

	Public Sub AddNamedGroup(sRowGroupName)
		DIM oTBodyGroup: Set oTBodyGroup = new Tag
		oTBodyGroup.TagName="TBODY"
		IF NOT(IsNullOrEmpty(sRowGroupName)) THEN oTBodyGroup.Id="Group_"&sRowGroupName
		oTableBody.AddNamedItem oTBodyGroup, sRowGroupName
'		Set oTBodyGroup = nothing
	End Sub

	Public Sub AddRows(ByRef oRows)
		[&CheckValidObjectType] oRows, "Row, Variant()"
		IF TypeName(oRows)="Row" THEN oRows=Array(oRows)
		FOR EACH oRow IN oRows
			Me.AddRow oRow
		NEXT
	End Sub


	Public Sub AddRow(ByRef oRow)
		IF oRow.IsHeader THEN
			oHeaders.AddContent(oRow)
		END IF
		IF oRow.IsFooter THEN
			IF oFooters IS NOTHING THEN Set oFooters = [$Tag]("TFOOT")
			oFooters.AddContent(oRow)
		END IF
		IF NOT(oRow.IsHeader OR oRow.IsFooter) THEN
			DIM sRowGroupName
			sRowGroupName=oRow.Object.GroupName
			IF IsNullOrEmpty(sRowGroupName) AND NOT(oTableBody.ItemsCount>0) THEN Me.AddGroup()
			IF IsEmpty(sRowGroupName) AND oTableBody.ItemsCount>0 THEN
				oTableBody.LastItem.AddContent oRow
			ELSEIF oTableBody.ExistsItem(sRowGroupName) THEN
				oTableBody(sRowGroupName).AddContent oRow
			ELSE
				Me.AddNamedGroup(sRowGroupName)
				oTableBody(sRowGroupName).AddContent oRow
			END IF
		END IF
	End Sub

	Public Function GetCode()
		oTable.Id="dataTable"
		IF sTableCode="" THEN
			IF Me.ProgressBar.ProgressBarControlId<>"" THEN 
				Me.ProgressBar.Parts=10
				Me.ProgressBar.ClassName="Loading"
				Me.ProgressBar.ClearProgress()
			END IF
			sTableCode=oTable.GetCode()
		END IF
		GetCode=sTableCode
	End Function
	
	Public Sub WriteCode()
    	oDisplayTime.StartTimer()
		response.write Me.GetCode()
		oDisplayTime.StopTimer()
	End Sub
	
	Public Sub ShowStatistics()
		response.write "<center>"
		response.write "********* Statistics for table "&Me.Id&" ************* " &"<br>"
'		response.write "Open Connection: "&ROUND(oDataSource.TimeOpenConn, 3) &"<br>"
'		response.write "Querying: "&ROUND(oDataSource.TimeQuery, 3) &"<br>"
'		response.write "Fetching: "&ROUND(tElapsedFetch, 3) &"<br>"
'		response.write "Formating: "& ROUND(tStartDisplay-tEndFetch, 3) &"<br>"
		response.write "Displaying: "&ROUND(oDisplayTime.TotalTime, 3) &"<br>"
		response.write "Total: "&ROUND(oDisplayTime.TotalTime, 3)&"<br>"
		response.write "**************************************** " &"<br><br>"
		response.write "</center>"
	End Sub
End Class
%>