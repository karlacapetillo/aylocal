<% 
Function [$TextBox](ByVal sParameters)
	Set [$TextBox]=[&New]("TextBox", sParameters)
End Function
Class TextBox
	'Properties
	Private sTextMode, iSize, sMinValue, sMaxValue, iMaxLength, sName
	
	'Settings
	
	'Objects
	Private oTextBox, oParentCell, oContainer, oRelatives, oLabel, oValue
	
	'Private Variables
'	Private 
	
	Public Property Get Object()
		IF Me.IsReadonly THEN
			Set Object = oLabel
		ELSE
			Set Object = oTextBox
		END IF
	End Property

	Private Sub Class_Terminate()
		Set oValue = nothing
		Set oLabel = nothing
		Set oRelatives = nothing
	End Sub
	Private Sub Class_Initialize()
		Set oValue = new VariantValue
		Set oLabel = new Label
		Set oRelatives = [$Relatives](Me)
		Me.TextMode="SingleLine"
	End Sub
	
	'Output objects
	Public Property Get Style()
		Set Style = oTextBox.Style
	End Property
	
	Public Property Get Properties()
		Set Properties = oTextBox.Properties
	End Property

	Public Property Get Methods()
		Set Methods = oTextBox.Methods
	End Property

	'Output Properties 
	Public Property Get Render()
		Render = oTextBox.Render
	End Property
	Public Property Let Render(input)
		oTextBox.Render = input
	End Property

	Public Property Get Visible()
		Visible = oTextBox.Visible
	End Property
	Public Property Let Visible(input)
		oTextBox.Visible = input
	End Property


	Public Property Get MinValue()
		MinValue = sMinValue
	End Property
	Public Property Let MinValue(input)
		sMinValue = input
		oTextBox.Properties.SetProperty "MinValue", sMinValue
	End Property


	Public Property Get MaxValue()
		MaxValue = sMaxValue
	End Property
	Public Property Let MaxValue(input)
		sMaxValue = input
		oTextBox.Properties.SetProperty "MaxValue", sMaxValue
	End Property


	Public Property Get Height()
		Height = iHeight
	End Property
	Public Property Let Height(input)
		iHeight = input
	End Property


	Public Property Get Width()
		Width = iWidth
	End Property
	Public Property Let Width(input)
		iWidth = input
	End Property

	Public Property Get TextMode()
		TextMode = sTextMode
	End Property
	Public Property Let TextMode(input)
		sTextMode = input
		IF UCASE(sTextMode)=UCASE("SingleLine") THEN 
			Set oTextBox=new Tag
			oTextBox.TagName="Input"
			oTextBox.Properties.SetProperty "type", "text"
			oTextBox.HasClosingTag=FALSE
		ELSEIF UCASE(sTextMode)=UCASE("MultiLine") THEN 
			Set oTextBox = new TextArea
		ELSEIF UCASE(sTextMode)=UCASE("Password") THEN 
			Set oTextBox=new Tag
			oTextBox.TagName="Input"
			oTextBox.Properties.SetProperty "type", "password"
			oTextBox.HasClosingTag=FALSE
		END IF
		oLabel.Style = Me.Style
	End Property


	Public Property Get Name()
		Name = sName
	End Property
	Public Property Let Name(input)
		sName = input
		oLabel.Id = sName
		IF UCASE(TypeName(oTextBox))="TAG" THEN
			oTextBox.Properties.SetProperty "name", sName
		ELSE
			oTextBox.Name=sName
		END IF
	End Property


	Public Property Get Id()
		Id = oTextBox.Id
	End Property
	Public Property Let Id(input)
		oTextBox.Id = input
		oLabel.Id = input
	End Property


	Public Property Get CssClass()
		CssClass = oTextBox.CssClass
	End Property
	Public Property Let CssClass(input)
		oTextBox.CssClass=input
'		oValue.Format=input
		oLabel.CssClass = Me.CssClass
	End Property


	Public Property Get Value()
		Set Value = oValue
	End Property
	Public Property Let Value(input)
		'Debugger Me, input.Value&": "&input.Format
		IF IsObject(Input) THEN
			Set oValue = input
		ELSE 
			oValue.Value = input
		END IF
		oLabel.Text = oValue

	End Property

	Public Property Get Text()
		Text = oValue.Text
	End Property
	Public Property Let Text(input)
		IF IsObject(Input) THEN
			Set oValue = input
		ELSE 
			oValue.Text = input
		END IF
	End Property

	Public Property Get Size()
		Size = iSize
	End Property
	Public Property Let Size(input)
		iSize = input
		oTextBox.Properties.SetProperty "size", iSize
	End Property


	Public Property Get MaxLength()
		MaxLength = iMaxLength
	End Property
	Public Property Let MaxLength(input)
		iMaxLength = input
		oTextBox.Properties.SetProperty "MaxLength", iMaxLength
	End Property


	Public Property Get IsReadonly()
		IsReadonly = oTextBox.IsReadonly
	End Property
	Public Property Let IsReadonly(input)
		oTextBox.IsReadonly = input
	End Property


	Public Property Get Relatives()
		Set Relatives = oRelatives
	End Property
	
	Public Property Get Container()
		Set Container = oRelatives.Parent
	End Property
	Public Property Let Container(input)
		oRelatives.Parent = input
	End Property

	Public Property Get ParentCell()
		Set ParentCell = oParentCell
	End Property
	Public Property Let ParentCell(input)
		Set oParentCell = input
	End Property

	Public Property Get Format()
		Format = oValue.Format
	End Property
	Public Property Let Format(input)
		oValue.Format = input
		
	End Property

	'Public methods
	Public Function GetCode()
		IF NOT Me.Render THEN EXIT Function END IF
		'Debugger Me, oValue.Format&" - "&oValue.Text
		IF Me.IsReadonly THEN
			GetCode=oLabel.GetCode()
		ELSE
			oTextBox.Properties.SetProperty "value", oValue.Text
			IF NOT(IsNullOrEmpty(oValue.Format)) THEN oTextBox.Properties.SetProperty "formato", dDefaultCssClass(UCASE(oValue.Format))
			IF TypeName(oValue)="NumericValue" THEN IF NOT(IsNullOrEmpty(oValue.DecimalPositions)) THEN oTextBox.Properties.SetProperty "decimalPositions", oValue.DecimalPositions
			GetCode=oTextBox.GetCode()
		END IF
	End Function
	
	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub
End Class 
 %>