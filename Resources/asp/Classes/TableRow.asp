<% 
Class TableRow
	'Member Variables
	Private sId, bIsHeader, bIsFooter
	Private iElementIndex, iCellPosition, iRowPosition, iThisCellPosition, iMaxPosition, sRowGroup
	Private iRowNumber
	Private aElements(512)
	Private returnString
	Private oCell, oPrevRow, oNextRow, oTemp, oElement, oParentCell, oDataField, oRow
	Private bVisible, bRender
    
	'Objects
	Private dDataFields, dCellPosDictionary

	
	Private Sub Class_Initialize()
		Set oRow = new Tag
		oRow.TagName="TR"
		iMaxPosition=0
		iRowPosition=-1
		iCellPosition=-1
'		Me.Mode="readonly"
		Set dCellPosDictionary=server.CreateObject("Scripting.Dictionary")
		Set dDataFields=server.CreateObject("Scripting.Dictionary")
		Me.RowNumber=0
		Me.Visible=TRUE
		Me.Render=FALSE
'	    sName = ""
	End Sub

	Public Property Get RowGroup()
		RowGroup = sRowGroup
	End Property
	Public Property Let RowGroup(input)
		sRowGroup = input
	End Property

	Public Property Get Style()
		Set Style = oRow.Style
	End Property
	
	Public Property Get RowNumber()
		RowNumber = iRowNumber
	End Property

	Public Property Get Mode()
		Mode = oRow.Mode
	End Property
	Public Property Let Mode(input)
		oRow.Mode = input
	End Property
	
    Public Property Get CellCount()
		CellCount = aElements(0)
    End Property
       
	Public Property Let RowNumber(input)
		iRowNumber = input
	End Property

    Public Property Let Id(input)
		sId = input
		oRow.Id=sId
    End Property
       
    Public Property Get CssClass()
		CssClass=oRow.CssClass
    End Property
    Public Property Let CssClass(input)
		oRow.CssClass=input
    End Property

	Public Property Get DataFields()
		Set DataFields=dDataFields
	End Property
       
	Public Property Let DataFields(input)
		FOR EACH oDataField IN input.items
			Me.AddDataField(oDataField)
		NEXT
	End Property
    
	Private Sub SetParents(oField)
		IF IsObject(oField.ParentCell()) THEN
			Set oCell=oField.ParentCell()
			IF oCell IS nothing THEN
				Set oCell = new TableCell
				oCell.SetParentRow(Me)
				oField.SetParentCell(oCell)
				oCell.AddContent(oField)
			END IF
		ELSE
			Set oCell = new TableCell
			oCell.SetParentRow(Me)
			oField.SetParentCell(oCell)
			oCell.AddContent(oField)
		END IF
	End Sub
	
	Public Property Get Cells()
		Cells=aElements
	End Property
	
	Public Property Let Position(var_iRowPosition)
		iRowPosition=var_iRowPosition
	End Property
	Public Property Get Position()
		Position=iRowPosition
	End Property


	Public Property Get IsHeader()
		IsHeader=bIsHeader
	End Property
	Public Property Let IsHeader(var_bIsHeader)
		bIsHeader=var_bIsHeader
	End Property


	Public Property Get IsFooter()
		IsFooter = bIsFooter
	End Property
	Public Property Let IsFooter(input)
		bIsFooter = input
	End Property


	Public Property Get Render()
		Render=oRow.Render
	End Property
	Public Property Let Render(var_bRender)
		bRender=var_bRender
		oRow.Render=bRender
	End Property
	
	Public Property Get Visible()
		Visible=oRow.Visible
	End Property
	Public Property Let Visible(input)
		oRow.Visible=input
	End Property
	
'	Public Sub SetCellRelatives()
'		Dim i
'		FOR i=1 TO aElements(0)
'			aElements(i).SetRelatives()
'		NEXT
'	End Sub
	
	'RelatedObjects
	Public Property Get PrevRow()
		IF IsObject(oPrevRow) THEN
			Set PrevRow = oPrevRow
		ELSE
			Set PrevRow = nothing
		END IF
	End Property
	Public Sub SetPrevRow(ByRef input)
		IF IsObject(input) THEN
			Set oPrevRow = input
		ELSE
			Set oPrevRow = nothing
		END IF
	End Sub

	Public Property Get NextRow()
		Set NextRow = oNextRow
	End Property
	Public Sub SetNextRow(input)
		IF IsObject(input) THEN
			Set oNextRow = input
		ELSE
			Set oNextRow = nothing
		END IF
	End Sub

	'Public methods
	Public Function GetCellByPosition(ByVal iPos)
		IF dCellPosDictionary.exists(iPos) THEN
			Set GetCellByPosition = dCellPosDictionary(iPos)
		ELSE
			Set GetCellByPosition = nothing
		END IF
	End Function
	
	Public Sub AddCell(ByRef oCell)
		Me.Render=TRUE
		aElements(0)=aElements(0)+1
		oCell.SetParentRow(Me)
		AssignPosition(oCell)
		Set aElements(aElements(0))=oCell
	End Sub

	Public Sub AddDataField(ByRef oField)
		Me.Render=TRUE
		IF (oField.DataField="") THEN
			Err.Raise 1, "ASP 101", "DataField property must be set first"
			response.end
		ELSE
			oField.CurrentRecord=iRowNumber
			Set dDataFields(oField.DataField)=oField
			SetParents oField
		END IF
'		IF NOT IsObject(oField.ParentCell()) THEN
'			response.write "ParentCell no fue definido para "&oField.DataField&"<br>"
'		ELSEIF oField.ParentCell() IS nothing THEN
'			response.write "ParentCell no fue inicializado para "&oField.DataField&"<br>"
'		ELSE
'			response.write "ParentCell fue agregado correctamente para "&oField.DataField&"<br>"
'		END IF
		aElements(0)=aElements(0)+1
		AssignPosition(oField)
		Set aElements(aElements(0))=oField
	End Sub
	
	Private Sub AssignPosition(ByRef oCell)
		IF NOT IsObject(oCell) THEN EXIT SUB
		IF oCell IS nothing THEN EXIT SUB
		iThisCellPosition=oCell.Position
		IF iThisCellPosition=-1 THEN iThisCellPosition=iMaxPosition+1
		iCellPosition=iThisCellPosition


'		response.write "<br>iCellPosition: "&iCellPosition&", iMaxPosition: "&iMaxPosition&"<br>"
		DO 
			IF dCellPosDictionary.exists(iCellPosition) THEN
				Err.Raise 1, "ASP 101", "The position "&iCellPosition&" is already taken ("&TypeName(oCell)&") This Position: "&oCell.Position
				response.end
			ELSE
				IF iMaxPosition<iCellPosition THEN iMaxPosition=iCellPosition
				oCell.Position=iCellPosition
				Set dCellPosDictionary(iCellPosition)=oCell
'				IF dCellPosDictionary.Exists(iCellPosition-1) THEN
'					oCell.SetPrevSibling(dCellPosDictionary(iCellPosition-1))
'					oCell.PrevSibling.SetNextSibling(oCell)
'				ELSE
'					oCell.SetPrevSibling(nothing)
'				END IF
				EXIT DO
			END IF
			iCellPosition=iCellPosition-1
		LOOP UNTIL iCellPosition<=0
	End Sub

	Public Function GetCode() 'Function Generate()
		IF NOT Me.Render THEN 
			'Generate="" 
			EXIT Function
		END IF
		'returnString=""
		
'		response.write "<td style=""'background-color:yellow';"">"
'		IF IsObject(oPrevRow) THEN
'			IF NOT oPrevRow IS NOTHING THEN response.write " <-- "&oPrevRow.Position
'		END IF
'		response.write " ("&Me.Position&", "&sAlternateRowCssClass&", "&bIsHeader&") "
'		IF IsObject(oNextRow) THEN
'			IF NOT oNextRow IS NOTHING THEN response.write oNextRow.Position&" --> "
'		END IF
'		response.write "</td>"
'		response.write iRowNumber&"> "&CellCount()&"<br>"
		For iElementIndex = 1 to iMaxPosition
			IF dCellPosDictionary.exists(iElementIndex) THEN
				Set oElement=dCellPosDictionary(iElementIndex)
'				response.write iRowNumber&", "&iElementIndex&"> "&TypeName(oElement)&"<br>"
				IF TypeName(oElement)="DynamicField" THEN
					oElement.CurrentRecord=iRowNumber
'					response.write oElement.DataField &": "&oElement.Value&"<br>"
					IF oElement.ScaffoldColumn THEN 
						Set oParentCell=nothing
						IF isObject(oElement.ParentCell()) THEN
							Set oParentCell = oElement.ParentCell()
						END IF
						IF oParentCell IS nothing THEN 
							response.write "Parent Cell is not defined for "&oElement.DataField&" at row "&iRowNumber&"<br>"
'							response.end
						ELSE
						IF iRowNumber=0 THEN oParentCell.CellType="TH"
						oRow.AddContent(oParentCell)
						END IF
					END IF
				ELSE
					oRow.AddContent(oElement)
				END IF
			END IF
		Next
		GetCode = oRow.GetCode()
	End Function
	
	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub

	Private Sub Class_Terminate()
		Set dCellPosDictionary=nothing
		Set dDataFields=nothing
	End Sub
End Class 
%>