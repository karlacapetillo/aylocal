<% 
Class Button
	'Member Variables
	Private sName, sId, sCssClass, sValue

	'Objects
	Private oButton, oProperties, oContainer, oRelatives
	
	'Private variables
	Private sInputType
	
	Private Sub Class_Initialize()
		Set oRelatives = [$Relatives](Me)
		Set oButton = new Tag
		oButton.TagName="input"
		oButton.Properties.SetProperty "type", "button"
	End Sub

	'Interfaces
	Public Property Get Methods()
		Set Methods = oButton.Methods
	End Property
	
	'Output objects
	Public Property Get Style()
		Set Style = oButton.Style
	End Property
	
	Public Property Get Properties()
		Set Properties = oButton.Properties
	End Property
	Public Property Get Relatives()
		Set Relatives = oRelatives
	End Property
	
	Public Property Get Container()
		Set Container = oRelatives.Parent
	End Property
	Public Property Let Container(input)
		oRelatives.Parent = input
	End Property

	'Properties 
	Public Property Get Visible()
		Visible = oButton.Visible
	End Property
	Public Property Let Visible(input)
		oButton.Visible = input
	End Property

	Public Property Get Name()
		Name = sName
	End Property
	
	Public Property Get Id()
		Id = sId
	End Property
	
	Public Property Get CssClass()
		CssClass = sCssClass
	End Property
	
	Public Property Get Value()
		Value = sValue
	End Property
	
	'Input Properties 
	Public Property Let Name(input)
		sName = input
	End Property
	
	Public Property Let Id(input)
		sId = input
	End Property
	
	Public Property Let CssClass(input)
		sCssClass = input
	End Property
	
	Public Property Let Value(input)
		sValue = input
	End Property
	
	'Public methods
	Public Function GetCode()
		'loops through an array to 
		'create dynamic HTML
'		response.write "<input type="""&sInputType&""""
		IF TRIM(sId)<>"" THEN oButton.Id=sId
		IF TRIM(sCssClass)<>"" THEN oButton.CssClass=sCssClass
		IF TRIM(sName)<>"" THEN oButton.Properties.SetProperty "name", sName
		IF TRIM(sValue)<>"" THEN oButton.Properties.SetProperty "value", sValue
'		IF TRIM(sOtherProperties)<>"" THEN oButton.OtherProperties=oButton.OtherProperties & sOtherProperties
'		response.write ">" & vbCrLf
		GetCode= oButton.GetCode()
	End Function
	
	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub
End Class
%>