<% 
Class FileTranslator
	'Properties
	Private sFileName, sFilePath, sSourceFilesFolder
	
	'Settings
	
	'Objects
	Private oProperties, oDataSource
'	Private oInput, oLabel
	
	Public Property Get Object()
		Set Object = Me
	End Property

	Private Sub Class_Terminate()
		Set oProperties = nothing
		Set oDataSource = nothing
	End Sub
	
	Private Sub Class_Initialize()
		Set oDataSource = nothing
		Set oProperties = new PropertiesCollection
		Me.FileName=request.querystring("FileName")
'		sSourceFilesFolder="Formato SER-01-b PREFILTRO_archivos/" 'request.querystring("SourceFilesFolder")
		Me.FilePath="documentos\"
	End Sub

	Public Property Get DataSource()
		Set DataSource = oDataSource
	End Property
	Public Property Let DataSource(input)
		Set oDataSource = input
	End Property

	Public Property Get Properties()
		Set Properties = oProperties
	End Property

	'Properties
	Public Property Get FileName()
		FileName = sFileName
	End Property
	Public Property Let FileName(input)
		sFileName = input
		IF CString(sFileName).IsLike("Formato.*") THEN
			sSourceFilesFolder="Formato SER-01-b PREFILTRO_archivos/"
		ELSEIF CString(sFileName).IsLike("SER-.*") THEN
			sSourceFilesFolder="SER-01-a PERFIL DE PUESTO_archivos/"
		ELSEIF CString(sFileName).IsLike("Orden Compra.*") THEN
			sSourceFilesFolder="Orden Compra_archivos/"
		ELSE
			sSourceFilesFolder=""
		END IF
	End Property


	Public Property Get FilePath()
		FilePath = sFilePath
	End Property
	Public Property Let FilePath(input)
		sFilePath = input
	End Property


	Function readFile(byVal fileName)
		Const ParaLeer = 1
		Dim fso, f
		Set fso = CreateObject("Scripting.FileSystemObject")
		ON ERROR  RESUME NEXT
		Set f = fso.OpenTextFile(fileName, ParaLeer)
		IF Err.Number<>0 THEN
			response.write "Error al abrir archivo "&fileName&". Error:"&REPLACE(Err.Description,"'","\'")
			response.end
		END IF
		ON ERROR  GOTO 0
		readFile =  f.ReadAll
	End Function

	Public Function GetCode()
'		IF session("IdUsuario")<>1 THEN response.write "P�gina en mantenimiento... esto tomar� algunos minutos."
		DIM strDocumento, sFullPathName
		IF TRIM(sFileName)="" THEN %>
		<center style="color:'red'; font-weight:bolder;">NO SE HA PROPORCIONADO EL NOMBRE DEL ARCHIVO.</center><br><br>
		<% RESPONSE.END
		END IF
		IF NOT Me.DataSource IS NOTHING THEN 
			Set oFields=Me.DataSource.Fields
		ELSE
			Set oFields=nothing
		END IF
		sFullPathName=application("PhysicalRootFolder") & "\" & sFilePath & sFileName
		strDocumento = readFile(sFullPathName) 
'		strDocumento = interpretaContratos(strDocumento).Remove(vblf)
		strDocumento = CString(strDocumento).Replace("href=(stylesheet.css)", REPLACE("href=""/"&sFilePath&""&sSourceFilesFolder&"$1""", "\", "/"))
		strDocumento = TextDataBind(strDocumento, oFields) _
'			.Remove("<!--\[if (.*?)<!\[endif\]-->") 
		IF INSTR(strDocumento, "�")>0 THEN %>
			<center style="color:'red'; font-weight:bolder;">EL DOCUMENTO NO EST� INTERPRETADO CORRECTAMENTE.</center><br><br>
			<% IF session("IdGrupo")<>1 THEN %>
				<% '<center style="color:'red'; font-weight:bolder;">NO SE PUEDE CONTINUAR, FAVOR DE COMUNICARSE CON EL RESPONSABLE DEL SISTEMA</center><br><br> %>
				<% 'response.end %>
			<% ELSE %>
				<center style="color:'red'; font-weight:bolder;">ES POSIBLE QUE EL ARCHIVO FUENTE TENGA ERRORES ORTOGRAFICOS O GRAMATICALES Y SEA NECESARIO DESACTIVARLOS.</center><br><br>
			<% END IF 
		END IF 
		'strDocumento=replace(strDocumento, "window.location.replace(", "null;//window.location.replace(")
		'strDocumento=replace(strDocumento, "<span style='display:none'>", "")
		'strDocumento=replace(strDocumento, "�</span>", "�")
'		'strDocumento=replace(strDocumento, "<!--[if", "<!--")
'		'strDocumento=replace(strDocumento, "<![endif]-->", "-->")
		'strDocumento=replace(strDocumento, "<![if !vml]>", "")
		'strDocumento=replace(strDocumento, "<![if !supportLists]", "")
		'strDocumento=replace(strDocumento, "<![endif]>", "")
		
		'strDocumento=replace(strDocumento, "�", "�</label>")
		'strDocumento=replace(strDocumento, "�", "<label style=""color:'red'; font-weight:bolder;"">�")
'		strDocumento=replace(strDocumento, "<link rel=Stylesheet href=stylesheet.css>", "<link rel=Stylesheet href=""/"&sFilePath&""&sSourceFilesFolder&"stylesheet.css"">")
		'strDocumento=replace(strDocumento, "<frame src=""", "<frame src=""../Generic_Code/InterpretadorDocumentos.asp?SourceFilesFolder="&REPLACE(sFileName, ".htm", "")&"_archivos/&RequestTable="&request.querystring("RequestTable")&"&FileName=")
		'strDocumento=replace(strDocumento, "id=""shLink"" href=""", "id=""shLink"" href=""/"&application("RootFolder")&"/Generic_Code/InterpretadorDocumentos.asp?SourceFilesFolder="&REPLACE(sFileName, ".htm", "")&"_archivos/&RequestTable="&request.querystring("RequestTable")&"&FileName=/")
		'strDocumento=replace(strDocumento, ".gif ", ".gif"" ")
		'strDocumento=replace(strDocumento, "src=image", "src=""image")
		strDocumento = CString(strDocumento).Replace("src=""?(\w+?\.\w+\b)""?", REPLACE("src=""/"&sFilePath&""&sSourceFilesFolder&"$1""", "\", "/"))
		'strDocumento=replace(strDocumento, "src=""", "src=""/"&REPLACE(application("RootFolder")&"\"&sFilePath&""&sSourceFilesFolder, "\", "/")&"")
'		strDocumento=replace(strDocumento, "src=""image", "src=""/"&REPLACE(application("RootFolder")&"\"&sFilePath&""&sSourceFilesFolder, "\", "/")&"image")
		
		GetCode = strDocumento
	End Function

	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub

End Class %>