<% 
Function [$ControlSelector](ByRef oDataField)
	Set [$ControlSelector] = new ControlSelector
	[$ControlSelector].DataField = oDataField
End Function
Class ControlSelector
	Private oControl, oDataField, sDataType, sFormat, iMaxLength, sViewMode, sMode
	Private Sub Class_Initialize()
		Set oControl = nothing
		Set oDataField = nothing
		iMaxLength="14"
	End Sub
	Private Sub Class_Terminate()
		Set oControl = nothing
		Set oDataField = nothing
	End Sub
	
	Public Property Get Control()
		sTableName=oDataField.DataSource.Metadata.TableName
		sViewMode=oDataField.DataSource.Metadata.ViewMode
		sMode=oDataField.DataSource.Metadata.Mode
		IF oControl IS NOTHING AND NOT(IsNullOrEmpty(sDataType)) THEN
						'Changes type if necessary
			SELECT CASE UCASE(sDataType)
			CASE UCASE("SQLRowNumber")
				Set oControl = new Label
				oControl.Id="row_Id"
			CASE UCASE("TabPanel")
				'oDataField.DataSource.Fields.IdentityField.FieldName
				Set oControl = new TabPanel
oControl.Id="Panel_"&TypeName(oDataField.DataSource.Table)&"_"&oDataField.DataSource.Table.TableIdentifier&"_"&RandomNumber(1000)
				oControl.HeaderText=ToTitleFromPascal(oDataField.DisplayName)
				oControl.ContentURL="oIdentifier=document.all('identifier'); oIdentifier=oIdentifier.length?oIdentifier(0):oIdentifier; 'List.asp?requestTable="&oDataField.FieldName&"&Mode="&sMode&"&TablePath="&request.queryString("TablePath")&""&"["&sTableName&"]("&sViewMode&":"&sMode&")"&"&IsMainTable=FALSE&FloatingHeader=TRUE&ParentIdentifier="&oDataField.Metadata.Item("TableCommonIdentifier")&"&"&oDataField.Metadata.Item("TableCommonIdentifier")&"='+(oIdentifier.value=='[new]'?'NULL':oIdentifier.value)"
				oControl.AddContent("<b>Cargando...</b>")
			CASE "TABLE"
				Set oControl = new CollapsedTable
				oControl.TableMode=sMode
			CASE UCASE("text"), UCASE("ntext") , UCASE("xml")
				Set oControl = new TextArea
			CASE UCASE("varchar")
				Set oControl = new TextBox
			CASE UCASE("nvarchar")
				Set oControl = new TextArea
			CASE UCASE("char"), UCASE("nchar")
				Set oControl = new TextBox
			CASE UCASE("float"), UCASE("decimal"), UCASE("real")
				Set oControl = new TextBox
				oControl.Size=14
				oControl.MaxLength=14
			CASE UCASE("money"), UCASE("smallmoney")
				IF UCASE(sViewMode)="FILTER" THEN 
					Set oControl = new MoneyRange
				ELSE 
					Set oControl = new TextBox
					oControl.Size=14
					oControl.MaxLength=14
					sFormat="moneda"
				END IF
			CASE UCASE("int"), UCASE("bigint"), UCASE("numeric")
				Set oControl = new TextBox
				oControl.Size=8
				oControl.MaxLength=8
			CASE UCASE("tinyint")
				Set oControl = new TextBox
				oControl.Size=3
				oControl.MaxLength=3
			CASE UCASE("Button"), UCASE("insertButton"), UCASE("updateButton")
				Set oControl = new ImageButton
			CASE UCASE("identity")
				Set oControl = new Checkbox
				oControl.Id="identifier"
			CASE UCASE("List")
				Set oControl = new BulletedList
			CASE UCASE("bit")
'				IF NOT bIsNullable THEN
'					Set oControl = new Checkbox
'				ELSE
				Set oControl = new DropDownList
				DIM SelectOption(2,2)
				SelectOption(0,0)="NULL"
				SelectOption(1,0)="- -"
				SelectOption(0,1)="1"
				SelectOption(1,1)="S�"
				SelectOption(0,2)="0"
				SelectOption(1,2)="No"
				oControl.FillComboWithArray SelectOption
'				END IF
			CASE UCASE("datetime"), UCASE("date"), UCASE("smalldatetime"), UCASE("smalldate")
				Set oControl=new DateBox
			CASE ELSE
				EXECUTE("Set oControl = new "&REPLACE(sDataType, "UI:", ""))
				sDataType = TypeName(oControl)
			END SELECT

			IF NOT ISOBJECT(oControl) THEN
				response.write oControl &" OBJECT IS NOT DEFINED FOR "&sDatatype
				response.end
			END IF
			IF oControl IS nothing THEN 
				response.write oControl &" OBJECT IS NOT DEFINED"
				response.end
			END IF

			SELECT CASE UCASE(sMode)
			CASE "READONLY"
				'ON ERROR  RESUME NEXT
				TryPropertySet oControl, "IsReadonly", FALSE, FALSE
				IF ERR.Number<>0 THEN 
					RESPONSE.WRITE "La propiedad no est� definida para el tipo de objeto "&TypeName(oControl)&""
					response.end
				END IF
				'ON ERROR GOTO 0
			CASE ELSE
				'ON ERROR RESUME NEXT
				SELECT CASE UCASE(TypeName(oControl))
				CASE "HIDDENFIELD", "LABEL"
					TryPropertySet oControl, "IsReadonly", FALSE, FALSE
				END SELECT
				'ON ERROR GOTO 0
			END SELECT
			IF IsNullOrEmpty(sFormat) THEN sFormat=sDataType
			IF NOT(IsNullOrEmpty(sFormat)) THEN TryPropertySet oControl, "CssClass", dDefaultCssClass(UCASE(sFormat)), FALSE
		END IF
		Set Control = oControl
	End Property

	'Interfaces
	'Properties
	Public Property Get MaxLength()
		MaxLength = iMaxLength
	End Property
	Public Property Let MaxLength(input)
		iMaxLength = input
	End Property

	Public Property Get DataType()
		DataType = sDataType
	End Property
	Public Property Let DataType(input)
		sDataType = RTRIM(input)
	End Property

	Public Property Get Format()
		Format = sFormat
	End Property
	Public Property Let Format(input)
		sFormat = input
		IF NOT(IsNullOrEmpty(sFormat)) THEN 
			DIM bPropertySet: bPropertySet=TryPropertySet(oControl, "Format", sFormat, FALSE)
			IF NOT(bPropertySet) THEN
				TryPropertySet oControl, "CssClass", dDefaultCssClass(UCASE(sFormat)), FALSE
			END IF
			
		END IF
	End Property

	Public Property Get DataField()
		Set DataField = oDataField
	End Property
	Public Property Let DataField(input)
		Set oDataField = input
	End Property
End Class
 %>