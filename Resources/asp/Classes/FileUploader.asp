<% 
Class FileUploader
	'Properties
	Private iValue
	
	'Objects 
	Private oHiddenField, oFileImage, oAttachNew, oAttachDel, oContainer, oRelatives
	
	Public Property Get Object()
		Set Object = oHiddenField.Object
	End Property

	Private Sub Class_Initialize()
		Set oRelatives = [$Relatives](Me)
		Set oContainer=new Div
		oContainer.id="uploaderContainer"

		Set oHiddenField = new HiddenField
		oHiddenField.Id="__FileName"
		oHiddenField.Methods.OnPropertyChange="if (event.propertyName=='value' && document.activeElement!=this) this.fireEvent('onchange');"
		oHiddenField.Methods.OnChange="this.parentNode.all('__ImageHolder').src=this.value;"
		oContainer.AddContent(oHiddenField)
		Set oFileImage = new ImageButton
		oFileImage.Id="__ImageHolder"
		oFileImage.Height=70
'		oFileImage.Width=35
		oFileImage.ImageURL="../images/archivo_desconocido.gif"
		oContainer.AddContent(oFileImage)
		Set oAttachNew = new ImageButton
		oAttachNew.Height=17
		oAttachNew.Width=20
		oAttachNew.Id="__obj_New"
		oAttachNew.CssClass="uploader"
		oAttachNew.Properties.SetProperty "pathFile", application("PhysicalRootFolder")&"\FilesRepository"
		oAttachNew.Methods.onPropertyChange="adhiereValor();"
		oAttachNew.ImageURL="../images/attachment_new.gif"
		oContainer.AddContent(oAttachNew)
		Set oAttachDel = new ImageButton
		oAttachDel.Height=17
		oAttachDel.Width=20
		oAttachDel.Id="__obj_Del"
		oAttachDel.CssClass="uploader"
		oAttachDel.Properties.SetProperty "pathFile", application("PhysicalRootFolder")&"\FilesRepository"
		oAttachDel.Methods.onPropertyChange="adhiereValor();"
		oAttachDel.ImageURL="../images/attachment_delete.gif"
'		oAttachDel.CommandText="oIdentifier=getParent('TR', this).all('identifier');  oIdentifier=oIdentifier.length?oIdentifier(0):oIdentifier; deleteData(getDBTableName(oIdentifier), getDBColumnName(oIdentifier), getDBColumnValue(oIdentifier), getParent('TR', oIdentifier), undefined, true);"
	oContainer.AddContent(oAttachDel)
	End Sub

	'Output objects
'	Public Property Get Style()
'		Set Style = oHiddenField.Style
'	End Property
	
'	Public Property Get Methods()
'		Set Methods = oHiddenField.Methods
'	End Property

	'Output Interfaces 
	Public Property Get Name()
		Name = oHiddenField.Name
	End Property
	Public Property Let Name(input)
		oHiddenField.Name = input
	End Property

	Public Property Get Relatives()
		Set Relatives = oRelatives
	End Property
	
	Public Property Get Container()
		Set Container = oRelatives.Parent
	End Property
	Public Property Let Container(input)
		oRelatives.Parent = input
	End Property

	Public Property Get ParentCell()
		ParentCell = oHiddenField.ParentCell
	End Property
	Public Property Let ParentCell(input)
		oHiddenField.ParentCell = input
	End Property

	Public Property Get Id()
		Id = oHiddenField.Id
	End Property
	Public Property Let Id(input)
		oHiddenField.Id = input
	End Property
	
	Public Property Get CssClass()
		CssClass = oHiddenField.CssClass
	End Property
	Public Property Let CssClass(input)
		oHiddenField.CssClass = input
	End Property
	
	Public Property Get Value()
		Value = oHiddenField.Value
	End Property
	Public Property Let Value(input)
		IF TRIM(input)<>"" THEN
			oHiddenField.Value = input
			oFileImage.ImageUrl = input
		END IF
	End Property

	Public Property Let MinValue(input)
'		response.write "Me.MinValue: "&Me.MinValue&"<br>"
		oHiddenField.MinValue=input
	End Property

	Public Property Let MaxValue(input)
'		response.write "Me.MaxValue: "&Me.MaxValue&"<br>"
		oHiddenField.MaxValue=input
	End Property

	'Public methods
	Public Sub SetStyle(ByRef oStyle)
	End Sub

	Public Function GetCode()
		GetCode=oHiddenField.GetCode()&oFileImage.GetCode&oAttachNew.GetCode&oAttachDel.GetCode()
	End Function

	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub
End Class 
 %>