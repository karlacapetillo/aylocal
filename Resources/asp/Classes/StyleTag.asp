<% 
Class StyleTag
	'Properties
	Private sId
	
	'Settings
	
	'Objects
	Private oTag, oContainer, oRelatives
	
	'Private Variables
	
	Private Sub Class_Initialize()
		Set oRelatives = [$Relatives](Me)
		Set oTag = new Tag 
		oTag.TagName="Style"
	End Sub
	
	'Output objects
	Public Property Get Object()
		Set Object = oTag
	End Property

	Public Property Get Relatives()
		Set Relatives = oRelatives
	End Property
	
	Public Property Get Container()
		Set Container = oRelatives.Parent
	End Property
	Public Property Let Container(input)
		oRelatives.Parent = input
	End Property

	Public Property Get ParentCell()
		Set ParentCell = oTag.ParentCell
	End Property
	Public Property Let ParentCell(input)
		oTag.ParentCell = input
	End Property
	
	'Output Properties 
	Public Function GetCode()
		GetCode=oTag.GetCode()
	End Function

	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub
End Class 
 %>