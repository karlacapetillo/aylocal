<% 
Class DataFieldCollection
	'Properties
	Private iRecordCount
	'Private 
	'Settings
	'Objects
	Private dMetadataFields, oFields, oDataSource, oRelatives, oIdentityField, oCommandsField

	Private Sub Class_Initialize()
		Set oRelatives = [$Relatives](Me)
		Set oFields = new Collection
		Set dMetadataFields = server.CreateObject("Scripting.Dictionary")
		Set oIdentityField = nothing
		Set oCommandsField = nothing
	End Sub
	Private Sub Class_Terminate()
		Set oRelatives = nothing
		Set oFields = nothing
		Set dMetadataFields = nothing
		Set oIdentityField = nothing
		Set oCommandsField = nothing
	End Sub

	'Interfaces
	Public Property Get Relatives()
		Set Relatives = oRelatives
	End Property
	
	Public Property Get Container()
		Set Container = oRelatives.Parent
	End Property
	Public Property Let Container(input)
		oRelatives.Parent = input
	End Property

'	Public Property Get Fields()
'		Set Fields = oFields
'	End Property

	Public Property Get All()
		Set All = oFields.NamedItems
	End Property

	Public Default Property Get Field(sFieldName)
		Set Field = oFields(sFieldName)
	End Property

	Public Property Get IdentityField()
		Set IdentityField = oIdentityField
	End Property

	'Properties
	Public Property Get DataSource()
		Set DataSource = oDataSource
	End Property
	Public Property Let DataSource(input)
		Set oDataSource = input
		iRecordCount=oDataSource.RecordCount
		DIM sMetadataString:	sMetadataString=oDataSource.Metadata.ColumnsMetadataString
		'Debugger Me, sMetadataString&"<br><br><br>"
		DIM oField, rsField, rsRecordSet:	Set rsRecordSet=oDataSource.RecordSet
		'DIM tStart:	tStart=timer
		FOR EACH rsField IN rsRecordSet.Fields
			IF NOT( rsField.name="SQLRowCount" OR (rsField.name="SQLRowNumber" AND NOT(oDataSource.ShowRowNumber)) ) THEN
				Set oField = [$DataField](oDataSource, rsField)
'				Debugger Me, rsField.Name&": "&rsField.Type
				DIM sFieldName:	sFieldName=rsField.name 'getDisplayName(rsField.name)
				oField.FieldNull=oDataSource.FieldNull
				'oField.Data=rsRecordSet
'				oField.Mode=oDataSource.Metadata.Mode
				oField.MetadataString=getMetadataString(sMetadataString, sFieldName)
'					Debugger Me, "<strong>"&sFieldName&"</strong>: "&oField.MetadataString
				IF oDataSource.FilterMembers.Exists(TRIM(sFieldName)) THEN oField.Visible=FALSE
				IF request.querystring("ParentIdentifier")=TRIM(sFieldName) THEN 
					IF oField.IsIdentity THEN
						oField.Visible=FALSE
					ELSE
						oField.Render=FALSE 
					END IF
				END IF'IsSubmitable=FALSE
				Me.AddDataField(oField)
'				oField.GetDBMetadata(oField.TableName)
			END IF
		NEXT
		'tempArray(tempArray(0, 0), 1)=timer-tStart
		IF oFields.ExistsItem("SQLRowNumber") THEN oFields("SQLRowNumber").HeaderText="#"
	End Property
	
	Public Property Get CommandsField()
		IF oCommandsField IS NOTHING THEN
			Set oCommandsField = [$CommandsField](oDataSource)
		END IF
		Set CommandsField = oCommandsField
	End Property

	Public Property Get ValidFieldNames()
		ValidFieldNames=oFields.ValidNames
	End Property

	Public Property Get FieldCount()
		FieldCount = oFields.ItemsCount
	End Property

	'Methods
	Public Function GetItems()
		GetItems=oFields.GetItems()
	End Function
	
	Public Function ToArray()
		ToArray=oFields.ToArray()
	End Function
	
	Public Function ExistsItem(ByVal sItemName)
		ExistsItem=oFields.ExistsItem(sItemName)
	End Function

	Public Sub AddDataField(ByRef oField)
		[&CheckValidObjectType] oField, "DataField"
		IF oField.IsIdentity THEN Set oIdentityField=oField
		DIM Matches: Set Matches = getMatch(oField.FieldName, "<Metadata:(.*?)>")
		IF Matches.Count>0 THEN
			DIM sFieldName: sFieldName=Matches(0).Submatches(0)
			Set dMetadataFields(sFieldName) = oField
			IF oFields.ExistsItem(sFieldName) THEN oFields(sFieldName).MetadataField = dMetadataFields(sFieldName)
		ELSE
			IF dMetadataFields.Exists(oField.FieldName) THEN oField.MetadataField = dMetadataFields(oField.FieldName)
			oFields.AddNamedItem oField, oField.FieldName
		END IF
	End Sub
End Class
 %>