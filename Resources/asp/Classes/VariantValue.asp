<% 
Function [$VariantValue]()
	Set [$VariantValue] = new VariantValue
End Function
Class VariantValue
	'Properties
	Private vValue, sFormat
	'Objects
	Private oValue
	
	Private Sub Class_Initialize()
		Set oValue = nothing
	End Sub
	Private Sub Class_Terminate()
		Set oValue = nothing
	End Sub
	
	Public Property Get Object()
		Set Object = oValue
	End Property

	Public Property Get ValueType()
		ValueType = TypeName(oValue)
	End Property

	Public Default Property Get Text()
		IF oValue IS NOTHING  THEN
			Text = Null
		ELSE
			Text = oValue.Text
		END IF
	End Property
	Public Property Let Text(input)
		'Debugger Me, "************* Text ("&TypeName(input)&") "&input
		SetProperties()
		oValue.Text = input
	End Property

	Public Property Get Value()
		Value = vValue
	End Property
	Public Property Let Value(input)
		vValue = input
		SetProperties()
	End Property

	Public Property Get Format()
		Format = sFormat
	End Property
	Public Property Let Format(input)
		sFormat = input
		IF CString(UCASE(sFormat)).ExistsIn("MONEY, PERCENT") THEN
			IF oValue IS NOTHING THEN 
				Set oValue = new NumericValue
			END IF
			oValue.Format = sFormat
		END IF
	End Property

	Public Property Get DecimalPositions()
		IF TypeName(oValue)<>"NumericValue" THEN 
			DecimalPositions = NULL
		ELSE
			DecimalPositions = oValue.DecimalPositions
		END IF
	End Property
	Public Property Let DecimalPositions(input)
		IF oValue IS NOTHING THEN Set oValue = new NumericValue
		oValue.DecimalPositions=CINT(ZeroIfInvalid(input))
	End Property
	
	Private Sub SetProperties()
'		Debugger Me, "SetProperties:"&TypeName(oValue)&": "&vValue
		IF oValue IS NOTHING THEN
			IF IsNumeric(vValue) THEN
				Set oValue = new NumericValue
			ELSEIF IsDate(vValue) THEN
				Set oValue = new StringValue
				IF CDATE(vValue)=CDATE("1/1/1900") THEN
					vValue = ""
				END IF
			ELSE
				Set oValue = new StringValue
			END IF
		END IF
		oValue.Value = vValue
	End Sub

	Public Function GetCode() 'Function Generate()
		GetCode = Me.Text()
	End Function
	
	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub
End Class
 %>