<%
Class DynamicTable
	'Properties
	Private bBorder, oParentCell, nTableIdentifier, oIdentityDataField, sQueryFilter
	
	'Settings
	Private bInlineInsert, bMainTable
	
	'Objects
	Private oChildrenTable, oTabContainer, oTabPanel, oImgExpand, oContainer, oRelatives
	
	'Member Variables
	Private bIsData, bDebug
	Private sDBTableName, sCssClass, sDataKeyName
	Private sMinDate, sMaxDate, iDia, iTimeLineColCounter, iSemana, iDiasPeriodo, tmp_FechaMin, tmp_FechaMax, bGetChildrenTables
	Private sId
	Private sOrientation
	Private sName, sMode, sBehaveMode, sDisplayName, sTimeLineDataType, sProgressBarControlId
	Private iRowIndex, iRowCount, iColCount, iMaxRowPosition, iThisRowPosition, iRowPosition, sAlternateRowCssClass
	Private aRows(2048), aeData, aeDataRow
	Private returnString
	Private sConn, oCn, oDataSource, rsDataSource, oAutoDataSource, oRecordSet, oTableSettings, rsTableSettings, oListChildrenDatasource, oInlineChildrenDatasource, rsChildrenDatasource, rsListChildrenDataSource, rsInlineChildrenDataSource
	Private iDefaultPageSize, iPageSize, iPageIndex, iPageMaxReg, sPageId, iTotalPages
	Private oDictionary, oRowPosDictionary, oPrevRow


    Private tStartOpenConnection, tEndOpenConnection, tElapsedOpenConnection
    Private tStartQuery, tEndQuery, tElapsedQuery
    Private tStartFetch, tEndFetch, tElapsedFetch
	Private tStartDisplay, tEndDisplay, tElapsedDisplay
	Private cellcount, cellspersecond
    
	Private aPagedData, aData, aDataRow, tempArray, oDataField, oField
    Private howmany, counter, sDataString
    Private tempSTRfields, thefields

	Private sNewHeader, sPastHeader
	
    Private rowheader, rowfooter, colheader, colfooter
    Private sFieldNull, sFieldBlank, template

    Private alldata, rsinfo, column_name, iColCounter
    
    Private iThisRow, fldName, fldDisplayName, fldnumb, fldvalue, fldtemplate, fldformat, fldcontainer
    Private thisrow
    Private whatever
	
	Private bShowColNames, bHorizontalTotalEnabled, bVerticalTotalEnabled, sPeriodo, sNotVisibles, bDisableRowNumbers, sTableType, sFreezeUntilColumn, sRightAlignForColumns, sLeftAlignForColumns, sViewMode, sSpanRowsBy, sHeaderTemplate, sSpanRowsForColumns, sWrapForColumns
	
	Private bStopFreezing, TotalHorizontal, thisRowValReference, Rowspan, lastRowValReference, iShownColCounter, spanColumn, wrapColumn, rightAlignColumn, leftAlignColumn

	Private oRow, oCell, oHeaderRow, oControlsRow, oCommandsRow, oHead, oProgress, bDataRetrieved
	Private r, oTemp, j, sMD_DT, iRowNumber, aOriginalDataFields(1024), aDataFields(1024), dDataFields, cTimeLineDataFields, oSubmit, oImgLArrow, oImgRArrow, oDiv, oImg, sGoForwardURL, sGoBackURL, sCurrentURL, oMenus, oTable, sQueryColumns, sMetadataString
	
	Private Sub Class_Initialize()
		Set oRelatives = [$Relatives](Me)
		Me.IdentityDataField = nothing
		Me.TableIdentifier = RandomNumber(1000)
		Set oTable=new Tag
		oTable.TagName="table"
		oTable.Render=TRUE
		Me.ProgressBarControlId="top.frames('logo').document.all('progress_bar')"
		Me.Mode="readonly"
		Me.ViewMode="Grid"
		Me.AlternateRowCssClass="AlternateRow"
		Set oRowPosDictionary=server.CreateObject("Scripting.Dictionary")
		bDisableRowNumbers=FALSE
		Me.Border=TRUE
		bShowColNames=TRUE
		bHorizontalTotalEnabled=FALSE
		bVerticalTotalEnabled=FALSE
		sFieldNull="" '" -null- "
		sFieldBlank="&nbsp;"
		sOrientation="landscape"
		sConn=application("StrCnn")
		sPeriodo="SEMANA"
		Set oDictionary=server.CreateObject("Scripting.Dictionary")
		Set dDataFields=server.CreateObject("Scripting.Dictionary")
		Set cTimeLineDataFields = new DataFieldCollection
		Set oImgLArrow=new ImageButton
		oImgLArrow.ImageURL="\"&application("btn_LArrow_path")
		oImgLArrow.Width=20
		oImgLArrow.Height=20
		Set oImgRArrow=new ImageButton
		oImgRArrow.ImageURL="\"&application("btn_RArrow_path")
		oImgRArrow.Width=20
		oImgRArrow.Height=20
		
		Set oControlsRow = new TableRow
		oControlsRow.IsHeader=TRUE
		Me.AddRow(oControlsRow)
		
		Set oCommandsRow = new TableRow
		oCommandsRow.IsHeader=TRUE
		Me.AddRow(oCommandsRow)
		
		Set oTabContainer = nothing
		
		Set rsChildrenDatasource = nothing
		Set rsListChildrenDataSource = nothing
		Set rsInlineChildrenDataSource = nothing
		
		Set oHeaderRow = new TableRow
		oHeaderRow.IsHeader=TRUE
		Me.AddRow(oHeaderRow)
		IF request.querystring("pageIndex")<>"" THEN 
			iPageIndex=request.querystring("pageIndex") 
		ELSE
			iPageIndex=1 
		END IF
		iDefaultPageSize=15
		iPageSize=NULL
		iRowPosition=-1
		
		Set oDataSource=new RecordSet
'		iMaxRowPosition=0
'	    bIsData = False
'	    sName = "Id"
		IF INSTR("&"&TRIM(request.querystring)&"=", "&sid=")>0 THEN 
			Me.MainTable=FALSE
			Me.InlineInsert=TRUE
		ELSE
			Me.MainTable=TRUE
			Me.InlineInsert=FALSE
		END IF
	End Sub
    
	Public Property Get TabContainer()
		Set TabContainer = oTabContainer
	End Property

	Public Property Get Debug()
		Debug = bDebug
	End Property

	Public Property Get TableIdentifier()
		TableIdentifier = nTableIdentifier
	End Property

	Public Property Get QueryFilter()
		QueryFilter = oDataSource.QueryFilter
	End Property

	Public Property Get CssClass()
		CssClass = oTable.CssClass
	End Property

	Public Property Get Id()
		Id = oTable.Id
	End Property

	Public Property Get DataKeyName()
		DataKeyName = sDataKeyName
	End Property

	Public Property Get InlineInsert()
		InlineInsert = bInlineInsert
	End Property

	Public Property Get MainTable()
		MainTable = bMainTable
	End Property

	Public Property Get IdentityDataField()
		Set IdentityDataField = oIdentityDataField
	End Property

	Public Property Get Border()
		Border = bBorder
	End Property

	Public Property Get Relatives()
		Set Relatives = oRelatives
	End Property
	
	Public Property Get Container()
		Set Container = oRelatives.Parent
	End Property
	Public Property Let Container(input)
		oRelatives.Parent = input
	End Property

	Public Property Get ParentCell()
		Set ParentCell = oParentCell
	End Property

	Public Property Get GetChildrenTables()
		GetChildrenTables = bGetChildrenTables
	End Property

	Public Property Get PageIndex()
		PageIndex = iPageIndex
	End Property

	Public Property Get ProgressBarControlId()
		ProgressBarControlId = sProgressBarControlId
	End Property

	Public Property Get AlternateRowCssClass()
		AlternateRowCssClass = sAlternateRowCssClass
	End Property

	Public Property Get DataSource()
		Set DataSource=oDataSource
	End Property
	
	Public Function DataFields(sFieldName)
'		response.write "aDataFields: "&aDataFields(0)
'		response.end
		IF NOT dDataFields.exists(sFieldName) THEN 
			DIM sValidFieldNames, keyname
			IF bDebug THEN 
				sValidFieldNames=""
				FOR EACH keyname IN dDataFields.keys
					sValidFieldNames=sValidFieldNames&keyname&" "
				NEXT
				Err.Raise 1, "ASP 101", sFieldName & " DataField is not valid (Valid FieldNames are: "&REPLACE(RTRIM(sValidFieldNames), " ", ", ")&")."
			ELSE
				Err.Raise 1, "ASP 101", sFieldName & " DataField is not valid."
			END IF
			response.end
		END IF

		Set DataFields = dDataFields(sFieldName)
	End Function
    
	Public Property Get TimeLineDataFields()
		Set TimeLineDataFields = cTimeLineDataFields
	End Property
	
	Public Property Get Mode()
		Mode = sMode
	End Property
	Public Property Let Mode(input)
		CheckForDataRetrieved("Mode")			'Add Functionality
		sMode = input
	End Property
	
	Public Property Get BehaveMode()
		BehaveMode = sBehaveMode
	End Property
'	Public Property Let BehaveMode(input)
'		sBehaveMode = input
'	End Property
	
	Public Property Let Orientation(var_sOrientation)
		SELECT CASE UCASE(var_sOrientation)
		CASE "portrait", "landscape"
			sOrientation=var_sOrientation
		CASE ELSE 
			Err.Raise 1, "ASP 101", "Invalid table type"
			response.end
		END SELECT
	End Property

    Public Property Get TimeFetch()
		TimeFech = tElapsedFetch
    End Property
	
    Public Property Let DisablePaging(var_bDisablePaging)
		bDisablePaging = var_bDisablePaging
    End Property

    Public Property Let DisableRowNumbers(var_bDisableRowNumbers)
		bDisableRowNumbers = var_bDisableRowNumbers
    End Property

    Public Property Let PageId(var_iPageId)
		sPageId = var_iPageId
    End Property

    Public Property Let PageSize(var_iPageSize)
		iPageSize = var_iPageSize
    End Property

    Public Property Let PageMaxReg(var_iPageMaxReg)
		iPageMaxReg = var_iPageMaxReg
		GranTotalRegistros=iPageMaxReg
    End Property

    Public Property Let DBTableName(input)
		sDBTableName = TRIM(input)
'		IF UCASE(sDBTableName)=UCASE("MetadataTable") THEN
'			DIM oTempRecordSet
'			Set oTempRecordSet = new RecordSet
'			oTempRecordSet.SQLQueryString="EXEC UpdateMetadata"
'			oTempRecordSet.DataBind()
'		END IF
    End Property
    Public Property Get DBTableName()
		DBTableName=sDBTableName
    End Property
	
	Public Property Let TabContainer(input)
		Set oTabContainer = input
		Set oTabPanel = new TabPanel
		oTabPanel.Id="Panel_"&Me.DBTableName&"_"&Me.TableIdentifier&"_"&RandomNumber(1000)
		oTabPanel.HeaderText=ToTitleFromPascal(Me.DBTableName)
		oTabPanel.AddContent(Me)
		oTabContainer.AddTab(oTabPanel)
	End Property

	Public Property Let Debug(input)
		bDebug = input
	End Property

	Public Property Let TableIdentifier(input)
		nTableIdentifier = input
	End Property

	Public Property Let QueryFilter(input)
		oDataSource.QueryFilter = input
	End Property

    Public Property Let CssClass(var_sCssClass)
		oTable.CssClass=sCssClass
    End Property

    Public Property Let Id(input)
		oTable.Id = input
    End Property
       
	Public Property Let DataKeyName(input)
		sDataKeyName = input
	End Property

	Public Property Let InlineInsert(input)
		bInlineInsert = input
	End Property

	Public Property Let MainTable(input)
		bMainTable = input
		IF bMainTable THEN 
			Me.Id="dataTable"
			oControlsRow.id="header"
			oCommandsRow.id="header"
			oHeaderRow.id="header"
		ELSE
			oControlsRow.id=""
			oCommandsRow.id=""
			oHeaderRow.id=""
		END IF
	End Property

	Public Property Let IdentityDataField(input)
		Set oIdentityDataField = input
		IF NOT(Me.MainTable) AND NOT(oIdentityDataField IS nothing) THEN 
			oIdentityDataField.Control.Properties.SetProperty "checkParent", "false"
			oIdentityDataField.Control.Properties.SetProperty "otherFields", "oIdentifier=(getIdentifier(this) || document.getElementById('identifier')); '"&TRIM(request.querystring("ParentIdentifier"))&"='+getVal(oIdentifier)"
		END IF
	End Property

	Public Property Let Border(input)
		bBorder = input
		IF Me.Border THEN 
			oTable.Properties.SetProperty "border", "1"
		ELSE
			oTable.Properties.RemoveProperty("border")
		END IF
	End Property

	Public Property Let ParentCell(input)
		Set oParentCell = input
	End Property

	Public Property Let GetChildrenTables(input)
		bGetChildrenTables = input
	End Property

	Public Property Let PageIndex(input)
		CheckForDataRetrieved("PageIndex")
		iPageIndex = input
	End Property

	Public Property Let ProgressBarControlId(input)
		CheckForDataRetrieved("ProgressBarControlId")
		sProgressBarControlId = input
	End Property

	Public Property Let AlternateRowCssClass(input)
		sAlternateRowCssClass = input
	End Property

    Public Property Let Name(var_sName)
		sName = var_3sName
    End Property

    Public Property Let FieldNull(var_sFieldNull)
		sFieldNull = var_sFieldNull
    End Property


    Public Property Let FreezeUntilColumn(var_ssFreezeUntilColumn)
		ssFreezeUntilColumn = var_ssFreezeUntilColumn
    End Property

    Public Property Let RightAlignForColumns(var_sRightAlignForColumns)
		sRightAlignForColumns = var_sRightAlignForColumns
    End Property

    Public Property Let LeftAlignForColumns(var_sLeftAlignForColumns)
		sLeftAlignForColumns = var_sLeftAlignForColumns
    End Property

    Public Property Let WrapForColumns(var_sWrapForColumns)
		sWrapForColumns = var_sWrapForColumns
    End Property

    Public Property Let SpanRowsForColumns(var_sSpanRowsForColumns)
		sSpanRowsForColumns = var_sSpanRowsForColumns
    End Property

    Public Property Let SpanRowsBy(var_sSpanRowsBy)
		sSpanRowsBy = var_sSpanRowsBy
    End Property

    Public Property Let TimeLineDataType(input)
		sTimeLineDataType = input
    End Property

    Public Property Let HorizontalTotalEnabled(var_bHorizontalTotalEnabled)
		bVerticalTotalEnabled = var_bHorizontalTotalEnabled
    End Property

    Public Property Let VerticalTotalEnabled(var_bVerticalTotalEnabled)
		bVerticalTotalEnabled = var_bVerticalTotalEnabled
    End Property
	
    Public Property Let TableType(var_sTableType)
		sTableType = var_sTableType
    End Property

    Public Property Let Periodo(var_sPeriodo)
		sPeriodo = var_sPeriodo
    End Property

    Public Property Let NotVisibles(var_sNotVisibles)
		sNotVisibles = var_sNotVisibles
    End Property	
	
    Public Property Let MinDate(input)
		sMinDate = input
    End Property
	
    Public Property Let MaxDate(input)
		sMaxDate = input
    End Property
	
    Public Property Let ViewMode(var_sViewMode)
		sViewMode = var_sViewMode
    End Property
	
    Public Property Let HeaderRow(var_oHeaderRow)
		Set oHeaderRow = var_oHeaderRow
    End Property
    Public Property Get HeaderRow()
		Set HeaderRow = oHeaderRow
    End Property

    Public Property Let HeaderTemplate(var_sHeaderTemplate)
		sHeaderTemplate = var_sHeaderTemplate
    End Property
	
    Public Property Let TemplateDictionary(input)
		oDictionary = var_oDictionary
    End Property
	
'	REDIM timeLineVerticalTotals(0)
	
	'Private Methods
	Public Sub CheckForDataRetrieved(tmpProperty)
		IF bDataRetrieved THEN 
			response.write "WARNING: "&tmpProperty&" Property must be set before data is retrieved!!!"
			response.end
		END IF
	End Sub

	'Public methods
	Public Sub AddRow(ByRef oRow)
		aRows(0)=aRows(0)+1
		AssignPosition(oRow)
'		oRow.SetCellRelatives()
		Set aRows(aRows(0))=oRow
		bIsData=TRUE
	End Sub

	Public Sub AddDataField(ByRef oDF)
		aDataFields(0)=aDataFields(0)+1
		Set aDataFields(aDataFields(0))=oDF
		Set dDataFields(oDF.DataField)=oDF
	End Sub

	Private Sub AssignPosition(ByRef oRow)
		iThisRowPosition=oRow.Position
		IF iThisRowPosition=-1 THEN iThisRowPosition=iMaxRowPosition+1
		iRowPosition=iThisRowPosition
		DO 
			IF oRowPosDictionary.exists(iRowPosition) THEN
				Err.Raise 1, "ASP 101", "The position "&iRowPosition&" is already taken by another oRow"
				response.end
			ELSE
				IF iMaxRowPosition<iRowPosition THEN iMaxRowPosition=iRowPosition
				oRow.Position=iRowPosition
'				oRow.AddContent(" P: "&oRow.Position)
				Set oRowPosDictionary(iRowPosition)=oRow
				IF oRowPosDictionary.Exists(iRowPosition-1) THEN
					oRow.SetPrevRow(oRowPosDictionary(iRowPosition-1))
					Set oPrevRow=oRow.PrevRow
					oPrevRow.SetNextRow(oRow)
				ELSE
					oRow.SetPrevRow(nothing)
				END IF
				EXIT DO
			END IF
			iRowPosition=iRowPosition-1
		LOOP UNTIL iRowPosition<=0
	End Sub

	Public Sub ShowStatistics()
		response.write "********* Statistics for "&Me.DBTableName&" ************* " &"<br>"
		response.write "Open Connection: "&ROUND(oDataSource.TimeOpenConn, 3) &"<br>"
		response.write "Querying: "&ROUND(oDataSource.TimeQuery, 3) &"<br>"
		response.write "Fetching: "&ROUND(tElapsedFetch, 3) &"<br>"
		response.write "Formating: "& ROUND(tStartDisplay-tEndFetch, 3) &"<br>"
		response.write "Displaying: "&ROUND(tElapsedDisplay, 3) &"<br>"
		response.write "Total: "&ROUND(oDataSource.TimeOpenConn+oDataSource.TimeQuery+tElapsedFetch+(tStartDisplay-tEndFetch)+tElapsedDisplay, 3)&"<br>"
		response.write "********************************** " &"<br><br>"
	End Sub
	
	Public Function GetCode()
		DIM sReturnString
		IF sProgressBarControlId<>"" THEN 
			Set oProgress=new Progress
			oProgress.ProgressBarControlId=sProgressBarControlId
			oProgress.Parts=10
			oProgress.ClassName="Loading"
			oProgress.ClearProgress()
			oProgress.TotalRecords=iMaxRowPosition
		END IF
    	tStartDisplay=timer
		oTable.Properties.SetProperty "db_table_name", Me.DBTableName
		iRowCount=0
		If bIsData Then
			For iRowIndex = 1 to iMaxRowPosition
'				IF iRowIndex=20 THEN RESPONSE.END
				IF IsObject(oProgress) THEN oProgress.StepForward()
				IF oRowPosDictionary.exists(iRowIndex) THEN
					Set oRow=oRowPosDictionary(iRowIndex)
					iRowCount=iRowCount+1
					IF iRowCount MOD 2=0 AND TRIM(sAlternateRowCssClass)<>"" AND NOT oRow.IsHeader THEN
						oRow.CssClass=sAlternateRowCssClass
'					ELSE
'						oRow.CssClass=""
					END IF
					oTable.AddContent(oRow) 'Generate()
				END IF
'				sReturnString=sReturnString& aCells(iRowIndex)
			Next
		End If
		IF IsObject(oProgress) THEN 
			oProgress.ClassName="StandBy"
			oProgress.SetComplete()
		END IF
		tEndDisplay=timer
		tElapsedDisplay=tEndDisplay-tStartDisplay
		GetCode=oTable.GetCode()
	End Function
	
	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub
	
	Public Sub RetrieveData()
		bDataRetrieved=FALSE
		IF sProgressBarControlId<>"" THEN 
			Set oProgress=new Progress
			oProgress.ProgressBarControlId=sProgressBarControlId
			oProgress.ClassName="Fetching"
			oProgress.ClearProgress()
		END IF
		IF oDataSource.SQLQueryString="" AND Me.DBTableName<>"" THEN 
			sBehaveMode=Me.Mode
			SELECT CASE UCASE(sViewMode)
			CASE UCASE("Calendar")
				sBehaveMode="Readonly"
			CASE UCASE("Grid")
'				IF Me.MainTable THEN
'					sBehaveMode="Readonly"
'				ELSE
					sBehaveMode="Update"
'				END IF
			CASE UCASE("Details")
				SELECT CASE UCASE(Me.Mode)
				CASE UCASE("Filter")
'					sBehaveMode="Insert"
				CASE UCASE("Insert")
				CASE UCASE("Update")
				END SELECT
			END SELECT
			oDataSource.GetMetadataFromDB Me.DBTableName, sViewMode, Me.Mode
			IF ISNULL(iPageSize) THEN iPageSize=oDataSource.PageSize

'			Set oTableSettings = new RecordSet
'			oTableSettings.SQLQueryString="SELECT InlineInsert FROM MetadataTable WHERE TableName='"&Me.DBTableName&"'"
'			oTableSettings.DataBind()
			
'			Set rsTableSettings=oTableSettings.RecordSet
			IF Me.MainTable AND NOT ISNULL(oDataSource.Metadata.InlineInsert) THEN
				Me.InlineInsert=oDataSource.Metadata.InlineInsert
			END IF

			sQueryColumns=""
			sMetadataString=oDataSource.Metadata.ColumnsMetadataString

			Set oListChildrenDatasource = new RecordSet
			oListChildrenDatasource.SQLQueryString="SELECT COLUMN_NAME, TABLE_NAME, VIEW_MODE, DISPLAY_NAME=COALESCE(DisplayName, TABLE_NAME) FROM ForeignKeys FK INNER JOIN MetadataTable MT ON MT.TableName=FK.Table_Name WHERE (VIEW_MODE IS NULL OR VIEW_MODE='list') AND FK.PARENT_TABLE_NAME='"&Me.DBTableName&"' AND (ScaffoldTable IS NULL OR ScaffoldTable=1)"
			oListChildrenDatasource.DataBind()
			Set rsListChildrenDataSource=oListChildrenDatasource.RecordSet
			
			IF (UCASE(Me.Mode)<>UCASE("Filter")) THEN
				Set oInlineChildrenDatasource = new RecordSet
				oInlineChildrenDatasource.SQLQueryString="SELECT COLUMN_NAME, TABLE_NAME, VIEW_MODE, DISPLAY_NAME=COALESCE(DisplayName, TABLE_NAME) FROM ForeignKeys FK INNER JOIN MetadataTable MT ON MT.TableName=FK.Table_Name WHERE (VIEW_MODE='inline') AND FK.PARENT_TABLE_NAME='"&Me.DBTableName&"' AND (ScaffoldTable IS NULL OR ScaffoldTable=1)"
'response.write oInlineChildrenDatasource.SQLQueryString
				oInlineChildrenDatasource.DataBind()
				Set rsInlineChildrenDataSource=oInlineChildrenDatasource.RecordSet
'				response.write QueryFilter&": "&Me.QueryFilter &"<br>"
			END IF
			IF NOT(rsListChildrenDataSource.BOF AND rsListChildrenDataSource.EOF) AND NOT(UCASE(sViewMode)=UCASE("Details")) AND Me.MainTable THEN
				sQueryColumns=sQueryColumns&", '' [ChildrenTables]"
			END IF 
			IF ISNULL(iPageSize) THEN 
				IF Me.MainTable THEN
					iPageSize=iDefaultPageSize
				ELSE
					iPageSize=255
				END IF
			END IF
'			RESPONSE.WRITE oDataSource.SQLQueryString &"<br><br>"
'			response.end
		END IF 
		'oDataSource.EnablePaging=TRUE
		IF UCASE(sViewMode)=UCASE("Details") THEN 
			oDataSource.PageSize=1
		ELSE
			oDataSource.PageSize=20'iPageSize
			oDataSource.PageIndex=iPageIndex
		END IF
		IF (UCASE(Me.Mode)=UCASE("Filter")) THEN Me.QueryFilter=""
		oDataSource.AutoCreate Me.DBTableName, sViewMode, Me.Mode

		Set rsDataSource=oDataSource.RecordSet
	    tStartFetch=timer
			IF rsDataSource.EOF AND Me.Mode="readonly" THEN
		'        objDictionary.item("errorsource")="retrieveData"
		'        objDictionary.item("errordesc")="No records <b>" & objDictionary.item("sql") & "</b>" 
		'        objDictionary.item("errornum")=1
		        rsDataSource.close
		        set rsDataSource=nothing
		        set oDataSource=nothing
				response.write "NO HAY RESULTADOS PARA LA CONSULTA"
				IF IsObject(oProgress) THEN oProgress.SetComplete()
					response.end
				Exit Sub
		    END IF
'			RESPONSE.WRITE rsDataSource("IdEtapa")
			IF NOT(rsDataSource.BOF AND rsDataSource.EOF) THEN sDataString=rsDataSource.getstring(, , "#c#", "#r#" & vbcrlf, "NULL")
'			response.write sDataString&"<br>"
			sCurrentURL=Request.serverVariables("PATH_INFO")
			IF Request.ServerVariables("QUERY_STRING")<>"" THEN
				sCurrentURL=sCurrentURL&"?"&REPLACE(Request.ServerVariables("QUERY_STRING"), "'", "\'")
			END IF
'	    IF NOT(UCASE(Me.Mode)<>UCASE("insert")) THEN
			Set oCell = new TableCell
			sGoBackURL=updateURLString(sCurrentURL, "PageIndex", iPageIndex-1)
'			sGoBackURL=updateURLString(sGoBackURL, "History", History)
			sGoForwardURL=updateURLString(sCurrentURL, "PageIndex", iPageIndex+1)
'			sGoForwardURL=updateURLString(sGoForwardURL, "History", History) 
			IF NOT(UCASE(sViewMode)=UCASE("Details")) THEN
				IF Me.MainTable THEN
					Set oDiv = new Tag
					oDiv.TagName="div"
					oDiv.HasClosingTag=TRUE
					IF Me.MainTable THEN oDiv.Id="freeze"
					IF 1=1 OR iPageIndex-1>0 THEN
						oImgLArrow.CommandText="OpenLink(encodeURI('"&REPLACE(REPLACE(sGoBackURL, "'", "\'"), "\\'", "\'")&"'), false, true)"
					END IF
					iTotalPages=ROUND((oDataSource.TotalRecords/iPageSize)+.5, 0)
					IF 1=1 OR iPageIndex+1<=iTotalPages THEN 
						oImgRArrow.CommandText="OpenLink(encodeURI('"&REPLACE(REPLACE(sGoForwardURL, "'", "\'"), "\\'", "\'")&"'), false, true)"
					END IF
					DIM oPageSelector, iCurrentPage
'					REDIM aPageIndexes(2,0)
'					Set oPageSelector = new DropDownList
'					FOR iCurrentPage=1 TO iTotalPages
'						IF iCurrentPage>1 THEN REDIM PRESERVE aPageIndexes(2, iCurrentPage-1)
'						aPageIndexes(0, UBOUND(aPageIndexes, 2))=iCurrentPage
'						aPageIndexes(1, UBOUND(aPageIndexes, 2))=iCurrentPage
'					NEXT 
'					oPageSelector.FillComboWithArray(aPageIndexes)
					Set oPageSelector = new NumberList
					oPageSelector.Control.NewOption CSTR(iPageIndex), CSTR(iPageIndex)
					oPageSelector.Control.OptionChoose=FALSE
					oPageSelector.Control.Properties.SetProperty "isSubmitable", "false"
					oPageSelector.MinValue=1
					oPageSelector.MaxValue=iTotalPages
					oPageSelector.Methods.OnChange="OpenLink(encodeURI('"&REPLACE(REPLACE(updateURLString(sCurrentURL, "PageIndex", "'+this.value+'"), "'", "\'"), "\\'", "\'")&"'), false, true)"
					oPageSelector.Value=CSTR(iPageIndex)
					oDiv.AddContent(oImgLArrow)
					oDiv.AddContent("&nbsp;<b>P�gina: ")
					oDiv.AddContent(oPageSelector)
					oDiv.AddContent(" / "&iTotalPages&"</b>&nbsp;")
					oCell.AddContent(oDiv)
					oCell.VAlign="middle"
					oDiv.AddContent(oImgRArrow)
					oCell.ColSpan=rsDataSource.fields.count+3
					oCell.Align="left"
					oControlsRow.AddCell(oCell)
					oControlsRow.CssClass="ControlsRow"
				END IF
				
'				Set oCell = new TableCell
'				oCell.ColSpan=rsDataSource.fields.count+3
'				oCell.Align="left"
				
'				oCommandsRow.AddCell(oCell)
			ELSEIF NOT rsListChildrenDataSource IS nothing THEN
				IF NOT (rsListChildrenDataSource.BOF AND rsListChildrenDataSource.EOF) THEN
'					Set oMenus = new BulletedList
'	'				oMenus.DataSource.SQLQueryString="SELECT IdMetadataCategory, CategoryName FROM MetadataCategory ORDER BY CategoryName"
'					oMenus.Id="tabs"
'					oMenus.DisplayMode="hyperlink"
'					oMenus.Style.TextDecoration="none"
'					oMenus.DisplayTextFormat="""<a href=""""#""""><span>$&</span></a>"""
'					'oMenus.Properties.SetProperty "text", Me.DBTableName
'					oMenus.NewOption oMenus.NewItem(Me.DBTableName, ToTitleFromPascal(Me.DBTableName))
'					oMenus.FillWithRecordset(rsListChildrenDataSource)
'					Set oCell = new TableCell
'					oCell.ColSpan=2
'					oCell.Align="left"
'					oCell.CssClass="NoBorders"
'					oCell.AddContent(oMenus)
'					oTabsRow.AddCell(oCell)
				END IF
			END IF
'		END IF
	    howmany=rsDataSource.fields.count
		IF NOT UCASE(sViewMode)=UCASE("Details") THEN
			IF NOT bDisableRowNumbers THEN 
				Set oImg = new ImageButton
				oImg.Width=30
				oImg.Height=30
				oImg.Id="CommandInsert"
				oImg.ImageURL=sImageLibraryPath & "btn_Insert.gif"
				IF Me.InlineInsert THEN
					oImg.CommandText="if (document.getElementById('"&Me.DBTableName&"_"&Me.TableIdentifier&"_data_row')) { nr=addRow(document.getElementById('"&Me.DBTableName&"_"&Me.TableIdentifier&"_data_row'), 0); fix_rowId( nr ); goToFirstVisibleObject(nr)} else {OpenLink('../DynamicData/Insert.asp?requestTable="&Me.DBTableName&"', false, false)}"
				ELSE
					oImg.CommandText="OpenLink('../DynamicData/Insert.asp?requestTable="&Me.DBTableName&"', false, false)"
				END IF
				
				Set oCell=new TableCell
				IF Me.MainTable THEN oCell.id="freeze"
'				oCell.AddContent("&nbsp;")
				oCell.AddContent(oImg)
				oCell.CellType="TH"
				oHeaderRow.AddCell(oCell) 
			END IF 
			Set oCell=new TableCell
			oCell.AddContent("&nbsp;")
			oCell.CellType="TH"
			oHeaderRow.AddCell(oCell) 
		END IF
'		response.write "MetadatString: "&sMetadataString&"<br><br><br>"
	    FOR counter=0 to howmany-1 
			sDisplayName=getDisplayName(rsDataSource(counter).name)
			Set oDataField= new DynamicField
			oDataField.NullText=sFieldNull
			oDataField.DataField=sDisplayName 
			oDataField.TableName=Me.DBTableName
			
			oDataField.Mode=sBehaveMode
'			oDataField.Mode="readonly"
			IF UCASE(oDataField.DataField)=UCASE("ChildrenTables") THEN 
				IF Me.MainTable THEN
					oDataField.DataType="List"
					oDataField.Control.DisplayTextFormat="""Ver ""&ToTitleFromPascal(""$&"")"
					oDataField.Control.FillWithRecordSet(rsListChildrenDataSource)
					oDataField.Control.Properties.SetProperty "catalogo", Me.DBTableName
	'				oDataField.Control.DisplayMode="HyperLink"
					oDataField.Control.ItemsCommandText="OpenLink('../DynamicData/List.asp?requestTable='+this.getAttribute('commandArguments')+'&'+this.getAttribute('value')+'='+getIdentifier(this).value, false, false)"
					oDataField.HeaderText="M�s informaci�n"
				ELSE
					oDataField.ScaffoldColumn=FALSE
				END IF
			ELSE
				oDataField.HeaderText=ToTitleFromPascal(sDisplayName)
				oDataField.MetadataString=getMetadataString(sMetadataString, oDataField.DataField)
'				response.write oDataField.DataField&" "&oDataField.MetadataString&"<br><br>"
				oDataField.GetDBMetadata()
			END IF
			IF oDataField.IsIdentity THEN Me.IdentityDataField=oDatafield
			IF INSTR("&"&TRIM(request.querystring)&"=", "&"&TRIM(oDataField.DataField)&"=")>0 THEN 
				IF UCASE(TypeName(oDataField.Control))=UCASE("AjaxDropDownList") THEN
'					oDataField.Control.Properties.SetProperty "checkParent", "true"
'					oDataField.Control.Properties.SetProperty "otherFields", "getDBColumnName(getIdentifier(this))+'='+getVal(getIdentifier(this))"
					oDataField.Control.OptionChoose=FALSE
				END IF
				IF UCASE(request.querystring("ParentIdentifier"))=UCASE(oDataField.DataField) THEN
					oDataField.ScaffoldColumn=FALSE
				END IF
			END IF
			IF oDataField.DataField="SQLRowNumber" THEN
				oDataField.ScaffoldColumn=FALSE
			END IF
			Me.AddDataField(oDataField)
			SELECT CASE UCASE(sViewMode)
			CASE UCASE("Calendar")
				Set oCalendar = new Calendar
			CASE UCASE("Grid")
				oHeaderRow.AddDataField(oDataField)
				oDataField.ParentCell.Align="center"
			CASE UCASE("TimeLine")
				IF IsNaN(oDataField.DataField) THEN
					oHeaderRow.AddDataField(oDataField)
				ELSE
					oDataField.DataType=sTimeLineDataType
				END IF
			CASE UCASE("Details")
				Set oRow = new TableRow
				oRow.Id=""&Me.DBTableName&"_"&Me.TableIdentifier&"_data_row"
				Set oHead = new TableCell
				oHead.CellType="TH"
				oHead.AddContent(oDataField)
				oDataField.Position=1
				oRow.AddCell(oHead)

				oDataField.CurrentRecord=1
				Set oCell=new TableCell
				oCell.Mode=oRow.Mode
				oCell.Align="left"
				oCell.AddContent(oDataField)
				oDataField.Position=2
				oCell.SetParentRow(oRow)
				oRow.AddCell(oCell)
				Me.AddRow(oRow)
			CASE ELSE
				
			END SELECT
	    NEXT
		
		IF NOT rsInlineChildrenDataSource IS nothing THEN
			IF NOT(rsInlineChildrenDataSource.BOF AND rsInlineChildrenDataSource.EOF) THEN
				rsInlineChildrenDataSource.MoveFirst
				DO UNTIL rsInlineChildrenDataSource.EOF
					IF UCASE(sViewMode)<>UCASE("Details") THEN
						Set oCell = new TableCell
						oCell.AddContent(ToTitleFromPascal(rsInlineChildrenDataSource("DISPLAY_NAME")))
						oCell.CellType="TH"
						oHeaderRow.AddCell(oCell)
					END IF
					rsInlineChildrenDataSource.MoveNext
				LOOP
			END IF
		END IF

		IF UCASE(sViewMode)=UCASE("timeline") THEN
			IF sPeriodo="DIARIO" THEN
				iDiasPeriodo=1	'	strSemana="DATEPART(dd, Fecha)"
			ELSE
				iDiasPeriodo=7	'	strSemana="SemanaAvance"
			END IF
			IF sMinDate<>"" AND INSTR(sMinDate, "{")>0 THEN
				tmp_FechaMin=evaluateTemplate(sMinDate, oRow.DataFields, oRow.RowNumber)
			END IF
			IF tmp_FechaMin<>"" THEN
				sMinDate=tmp_FechaMin
			ELSEIF sMinDate="" THEN
				sMinDate="1/1/1900"
			END IF
			sMinDate=CDATE(sMinDate)

			IF sMaxDate<>"" AND INSTR(sMaxDate, "{")>0 THEN
				tmp_FechaMax=evaluateTemplate(sMaxDate, oRow.DataFields, oRow.RowNumber)
			END IF
			IF tmp_FechaMax<>"" THEN
				sMaxDate=tmp_FechaMax
			ELSEIF sMaxDate="" THEN
				sMaxDate="1/1/1900"
			END IF
			sMaxDate=CDATE(sMaxDate)

			IF CDATE(sMaxDate)=CDATE("1/1/1900") THEN
				sMaxDate=DATE()
			END IF

			IF sFreezeUntilColumn<>"" OR UCASE(sTableType)=UCASE("timeline") THEN
				bStopFreezing=FALSE
			ELSE
				bStopFreezing=TRUE
			END IF

			iDia=CDATE(sMinDate-DatePart("w",sMinDate,2,2)+1)
			iTimeLineColCounter=0
			DO WHILE iDia<=sMaxDate
				iTimeLineColCounter=iTimeLineColCounter+1
				IF sPeriodo="DIARIO" THEN
					iSemana=DatePart("d",iDia,2,2)
				ELSEIF sPeriodo="SEMANA" THEN
					iSemana=semanaReal(iDia)
				END IF
				
				IF iSemana<10 THEN
					iSemana="0" & CSTR(iSemana)
				END IF
				
				'REDIM PRESERVE timeLineVerticalTotals(iTimeLineColCounter) 
				IF dDataFields.exists("09"&iSemana) THEN
					Set oDataField= dDataFields("09"&iSemana)
				ELSE
					Set oDataField= new DynamicField
					oDataField.DataField="09"&iSemana
				END IF
				cTimeLineDataFields.AddDataField(oDataField)
				IF sTimeLineDataType<>"" THEN oDataField.DataType=sTimeLineDataType
				oDataField.NullText=sFieldNull
				oDataField.Mode=sBehaveMode
				oDataField.TableName=Me.DBTableName
				oHeaderRow.AddDataField(oDataField)
				IF sPeriodo="SEMANA" THEN 
					oDataField.HeaderText=iSemana&"<br><label>Del "& iDia &" <br>al "& iDia+iDiasPeriodo-1 &"</label>"
				ELSE  
					oDataField.HeaderText=iSemana&"<br>"&WeekdayName(DatePart("w",iDia,2,2), TRUE)
				END IF 
'				oCell.Style.StyleString="font-size:8pt; border-collapse:'collapse'; border-bottom-style:'solid'; border-bottom-color:'transparent'; border-bottom-width:'3'; border-top-style:'solid'; border-top-width:'3'; border-top-color:'transparent'; border-right-width:'3'; border-right-style:'solid'; border-left-width:'3'; border-left-style:'slashed'; border-right-style:'solid'; border-right-color:'transparent';" 
				iDia=iDia+iDiasPeriodo
			LOOP 
			IF bHorizontalTotalEnabled THEN 
				Set oCell=new TableCell
				oCell.CellType="TH"
				oCell.AddContent("Total")
				oHeaderRow.AddCell(oCell) 
			END IF
		END IF 
		IF (Me.InlineInsert OR UCASE(sViewMode)=UCASE("Details")) AND UBOUND(split(sDataString, "#r#" & vbcrlf))<=0 THEN
			DIM sNewRow
			sNewRow=""
			FOR i=0 TO rsDataSource.fields.count-1
				sNewRow=sNewRow&"NULL#c#"
			NEXT
			sDataString=sDataString&REPLACE((sNewRow&"#r#" & vbcrlf), "#c##r#", "#r#")
		END IF
        aData=split(sDataString, "#r#" & vbcrlf)
	    rsDataSource.close
	    set rsDataSource=nothing

		IF 1=0 AND NOT(oDataSource.EnablePaging) AND iPageSize>0 THEN
			REDIM tempArray(iPageSize)
			DIM a
			i=0
			FOR a=(iPageSize*(iPageIndex-1)) TO (iPageSize*iPageIndex)-1
'				RESPONSE.WRITE " a: "&a&" de "&UBOUND(aData)&"<br><br>"
				IF a>=UBOUND(aData) THEN 
					EXIT FOR 
				END IF
				tempArray(i)=aData(a)
				IF NOT INSTR(tempArray(i), "#c#")>0 THEN tempArray(i)=tempArray(i)&"#c#"
				i=i+1
			NEXT
			REDIM PRESERVE tempArray(i)
			iPageMaxReg=UBOUND(aData)
		ELSE
			tempArray=aData
		END IF
	    tEndFetch=timer
		tElapsedFetch=tEndFetch-tStartFetch
		aPagedData=tempArray
'		response.write "aPagedData: "&UBOUND(aPagedData)
		Dim i, aCells
		SELECT CASE UCASE(sViewMode)
		CASE UCASE("Details")
			aDataRow=SPLIT(aPagedData(0), "#c#")
'			response.write aPagedData(0)&"<br>"
			i=0
			FOR EACH aeDataRow in aDataRow
				i=i+1
				Set oDataField=aDataFields(i)
				oDataField.CurrentRecord=1
				IF aeDataRow="NULL" THEN aeDataRow=NULL
				IF UCASE(sBehaveMode)<>UCASE("Insert") AND UCASE(sBehaveMode)<>UCASE("Filter") THEN 
					IF INSTR("&"&TRIM(request.querystring)&"=", "&"&TRIM(oDataField.DataField)&"=")>0 THEN 
						oDataField.Value=request.querystring(TRIM(oDataField.DataField))
					ELSE
						oDataField.Value=RTRIM(aeDataRow)
					END IF
				END IF
				oDataField.NullText=sFieldNull
			NEXT
			IF NOT rsInlineChildrenDataSource IS nothing THEN
				IF NOT(rsInlineChildrenDataSource.BOF AND rsInlineChildrenDataSource.EOF) THEN
					rsInlineChildrenDataSource.MoveFirst
					DO UNTIL rsInlineChildrenDataSource.EOF
						IF 1=1 THEN
							Set oChildrenTable = new URLLoader
							oChildrenTable.ContentURL="oIdentifier=getIdentifier(this); 'List.asp?requestTable="&TRIM(rsInlineChildrenDataSource("TABLE_NAME"))&"&ParentIdentifier="&TRIM(rsInlineChildrenDataSource("COLUMN_NAME"))&"&"&TRIM(rsInlineChildrenDataSource("COLUMN_NAME"))&"='+(oIdentifier.value=='[new]'?'NULL':oIdentifier.value)"
							oChildrenTable.AddContent("<b>Cargando...</b>")
							Set oImgExpand = new Expander
							oImgExpand.TargetObject=oChildrenTable
							Set oRow = new TableRow
							Set oCell = new TableCell
							oCell.CellType="TH"
							oCell.AddContent(ToTitleFromPascal(rsInlineChildrenDataSource("TABLE_NAME")))
							oRow.AddCell(oCell)
							Set oCell = new TableCell
							oCell.AddContent(oImgExpand)
							oRow.AddCell(oCell)
							Me.AddRow(oRow)
						ELSE
							Set oChildrenTable = new DynamicTable
							oChildrenTable.Debug=TRUE
							oChildrenTable.MainTable=FALSE
							oChildrenTable.DBTableName=rsInlineChildrenDataSource("TABLE_NAME")
							IF TRIM(Me.IdentityDataField.Value)="" OR ISNULL(Me.IdentityDataField.Value) OR UCASE(TRIM(Me.IdentityDataField.Value))="[NEW]" THEN
								oChildrenTable.QueryFilter=TRIM(rsInlineChildrenDataSource("COLUMN_NAME"))&"=NULL" 'Esto no producir�a ning�n resultado puesto que la comparaci�n =NULL no devuelve filas
							ELSE
								oChildrenTable.QueryFilter=RTRIM(rsInlineChildrenDataSource("COLUMN_NAME"))&"="&Me.IdentityDataField.Value
							END IF
							oChildrenTable.InlineInsert=TRUE
							oChildrenTable.ViewMode="Grid"
							oChildrenTable.FieldNull="&nbsp;"
							oChildrenTable.Mode=sBehaveMode
							oChildrenTable.GetChildrenTables=TRUE
							oChildrenTable.RetrieveData() 
		'						oChildrenTable.DataFields("IdTipoTelefono").Control.Methods.OnClick="alert(getDBColumnName(getIdentifier(getIdentifier(this)))+' '+getVal(getIdentifier(getIdentifier(this))))"
							oChildrenTable.DataFields(TRIM(rsInlineChildrenDataSource("COLUMN_NAME"))).ScaffoldColumn=FALSE
							oChildrenTable.IdentityDataField.Control.Properties.SetProperty "checkParent", "true"
							oChildrenTable.IdentityDataField.Control.Properties.SetProperty "otherFields", "getDBColumnName(getIdentifier(this))+'='+getVal(getIdentifier(this))"
		'						checkParent="true" db_column_name="IdRelacionPuestos" value="<% IF rsDatos("IdRelacionPuestos")<>"" THEN %%<%= rsDatos("IdRelacionPuestos") %%<% ELSE %%0<% END IF %%" otherFields="'IdPuesto=<%= rsDatos("IdPuesto") %%'"
							Set oRow = new TableRow
							Set oCell = new TableCell
							oCell.CellType="TH"
							oCell.AddContent(ToTitleFromPascal(oChildrenTable.DBTableName))
							oRow.AddCell(oCell)
							Set oCell = new TableCell
							oCell.AddContent(oChildrenTable)
							oRow.AddCell(oCell)
							Me.AddRow(oRow)
						END IF
						rsInlineChildrenDataSource.MoveNext
					LOOP
				END IF
			END IF
			IF NOT(rsListChildrenDataSource.BOF AND rsListChildrenDataSource.EOF) THEN
				rsListChildrenDataSource.MoveFirst
				IF NOT Me.TabContainer IS nothing THEN
					DIM oTabPanel
					rsListChildrenDataSource.MoveFirst
					DO UNTIL rsListChildrenDataSource.EOF
						Set oTabPanel = new TabPanel
'						Set oChildrenTable = new DynamicTable
'						oChildrenTable.Debug=TRUE
'						oChildrenTable.MainTable=FALSE
'						oChildrenTable.DBTableName=rsListChildrenDataSource("TABLE_NAME")
'						
''						response.write Me.IdentityDataField.DataField&"<br>"
''						response.write Me.IdentityDataField.Value&"<br>"
''						response.write rsListChildrenDataSource("COLUMN_NAME")
'						IF TRIM(Me.IdentityDataField.Value)="" OR ISNULL(Me.IdentityDataField.Value) THEN
'							oChildrenTable.QueryFilter=rsListChildrenDataSource("COLUMN_NAME")&"=0"
'						ELSE
'							oChildrenTable.QueryFilter=rsListChildrenDataSource("COLUMN_NAME")&"="&Me.IdentityDataField.Value
'						END IF
'						oChildrenTable.InlineInsert=TRUE
'						oChildrenTable.ViewMode="Grid"
'						oChildrenTable.FieldNull="&nbsp;"
'						oChildrenTable.Mode=sBehaveMode
'						oChildrenTable.GetChildrenTables=TRUE
'						oChildrenTable.RetrieveData() 
'						oChildrenTable.DataFields(TRIM(rsListChildrenDataSource("COLUMN_NAME"))).ScaffoldColumn=FALSE
'						oChildrenTable.IdentityDataField.Control.Properties.SetProperty "checkParent", "true"
'						oChildrenTable.IdentityDataField.Control.Properties.SetProperty "otherFields", "oIdentifier=document.all('"&"Panel_"&Me.DBTableName&"_"&Me.TableIdentifier&"').all('identifier'); oIdentifier=oIdentifier.length?oIdentifier(0):oIdentifier; getDBColumnName(oIdentifier)+'='+getVal(oIdentifier)"
'						oTabPanel.Id="Panel_"&oChildrenTable.DBTableName&"_"&oChildrenTable.TableIdentifier
						oTabPanel.Id="Panel_"&TRIM(rsListChildrenDataSource("TABLE_NAME"))&"_"&Me.TableIdentifier&"_"&RandomNumber(1000)
						oTabPanel.HeaderText=ToTitleFromPascal(rsListChildrenDataSource("DISPLAY_NAME"))
'						oTabPanel.ContentURL="oIdentifier=getParent('TR', this).all('identifier'); oIdentifier=oIdentifier.length?oIdentifier(0):oIdentifier; 'List.asp?requestTable="&TRIM(rsListChildrenDataSource("TABLE_NAME"))&"&"&rsListChildrenDataSource("COLUMN_NAME")&"='+oIdentifier.value"
						oTabPanel.ContentURL="oIdentifier=document.all('identifier'); oIdentifier=oIdentifier.length?oIdentifier(0):oIdentifier; 'List.asp?requestTable="&TRIM(rsListChildrenDataSource("TABLE_NAME"))&"&ParentIdentifier="&TRIM(rsListChildrenDataSource("COLUMN_NAME"))&"&"&TRIM(rsListChildrenDataSource("COLUMN_NAME"))&"='+(oIdentifier.value=='[new]'?'NULL':oIdentifier.value)"
						oTabPanel.AddContent("<b>Cargando...</b>")
						Me.TabContainer.AddTab(oTabPanel)
						rsListChildrenDataSource.MoveNext
					LOOP
					Me.TabContainer.SetActiveTab(0)
				END IF
			END IF
			CASE UCASE("Calendar")
				Set oCalendar = new Calendar
				
			CASE UCASE("Grid"), UCASE("TimeLine")
			j=0
			'FOR aeData in aPagedData
			DIM sPreviousTotalTemplate
			FOR j=0 TO UBOUND(aPagedData)-1
				aeData=aPagedData(j)
				Set oRow=new TableRow
				oRow.Id=""&Me.DBTableName&"_"&Me.TableIdentifier&"_data_row"
				oRow.Mode=sBehaveMode
				oRow.RowNumber=j+1
				aDataRow=SPLIT(aeData, "#c#")
				i=0
				IF NOT bDisableRowNumbers THEN
					i=i+1
					Set oCell=new TableCell
					IF Me.MainTable THEN oCell.id="freeze"
					oCell.CellType="TH"
					oCell.AddContent("#<b id=""row_Id"">"&(iPageIndex-1)*iPagesize+j+1&"</b>")
					oRow.AddCell(oCell)
				END IF
				
				Set oCell=new TableCell
				IF Me.MainTable THEN
				Set oImg = new ImageButton
				oImg.Id="CommandUpdate"
				oImg.Width=25
				oImg.Height=25
				oImg.ImageURL=sImageLibraryPath & "btn_Edit.gif"
				oImg.AlternateText="Editar registro"
				oImg.CommandText="oIdentifier=getParent('TR', this).all('identifier'); oIdentifier=oIdentifier.length?oIdentifier(0):oIdentifier; OpenLink('../DynamicData/Update.asp?requestTable="&Me.DBTableName&"&'+getDBColumnName(oIdentifier)+'='+getDBColumnValue(oIdentifier), false, false)"
				oCell.AddContent(oImg)
				END IF

				Set oImg = new ImageButton
				oImg.Id="CommandDelete"
				oImg.Width=25
				oImg.Height=25
				oImg.ImageURL=sImageLibraryPath & "btn_Delete.gif"
				oImg.AlternateText="Eliminar registro"
				oImg.CommandText="oIdentifier=getParent('TR', this).all('identifier');  oIdentifier=oIdentifier.length?oIdentifier(0):oIdentifier; deleteData(getDBTableName(oIdentifier), getDBColumnName(oIdentifier), getDBColumnValue(oIdentifier), getParent('TR', oIdentifier));"
'tabla=getParent('TABLE', this); fila=getParent('TR', this); if (confirm('Seguro que desea eliminar esta linea?')) { /*alert(oIdentifier+', '+getDBTableName(oIdentifier)+', '+getDBColumnName(oIdentifier)+', '+getVal(oIdentifier)+', '+46); */ if (borraElemento(oIdentifier, getDBTableName(oIdentifier), getDBColumnName(oIdentifier), getVal(oIdentifier), 46)) { !(tabla.all('"&Me.DBTableName&"_"&Me.TableIdentifier&"_data_row').length && tabla.all('"&Me.DBTableName&"_"&Me.TableIdentifier&"_data_row').length>1)?addRow(fila):null; fix_rowId( removeRow(fila) ); } }"
				oCell.AddContent(oImg)
				oRow.AddCell(oCell)

				oRow.DataFields=oHeaderRow.DataFields
				DIM sTotalTemplate
				DIM oTotalRow, oTempRow
				Set oTotalRow=nothing
				Set oTempRow = new TableRow
				oTempRow.Style.BackgroundColor="orange"
'				response.write oRow.RowNumber&"--> <br>"
				FOR r=0 TO UBOUND(aDataRow)
'					response.write " "&aDataFields(r+1).DataField &": "&oRow.DataFields.exists(aDataFields(r+1).DataField)&"<br>"
					IF oRow.DataFields.exists(aDataFields(r+1).DataField) THEN 
						Set oDataField=oRow.DataFields.item(aDataFields(r+1).DataField)
						Dim oTotalCell, oTotalLabel
						Set oTotalCell = new TableCell
						aeDataRow=aDataRow(r)
						i=i+1
						IF aeDataRow="NULL" THEN aeDataRow=NULL
						IF INSTR("&"&TRIM(request.querystring)&"=", "&"&TRIM(oDataField.DataField)&"=")>0 THEN 
							oDataField.Value=request.querystring(TRIM(oDataField.DataField))
						ELSE
							oDataField.Value=RTRIM(aeDataRow)
						END IF
						oDataField.NullText=sFieldNull
						oTotalCell.Visible=oDataField.Visible
						IF oDataField.GroupByColumn AND oDataField.CurrentRecord>1 AND oDataField.Value<>oDataField.ValueAt(oDataField.CurrentRecord-1) THEN
							IF oTotalRow IS NOTHING THEN Set oTotalRow = oTempRow
'							oTotalCell.ColSpan=UBOUND(aDataRow)
'							Set oTotalLabel = new Label
'							oTotalLabel.Text="Total: "&oDataField.DataField&" --> "&oDataField.Value&" vs. "&oDataField.ValueAt(oDataField.CurrentRecord-1)
						END IF
						IF oDataField.TotalRowFormula<>"" THEN
							Set oTotalLabel = new Label
							oTotalLabel.Id="subtotal"
							oTotalLabel.CssClass="formula"
							oTotalLabel.Properties.SetProperty "formula", oDataField.TotalRowFormula
							oTotalLabel.Text="- -"
							oTotalCell.AddContent(oTotalLabel)
						END IF
						oTempRow.AddCell(oTotalCell)
'						response.write "	"&aDataFields(r+1).DataField&" value: "&RTRIM(aeDataRow)&" vs. "&oDataField.Value&"<br>"
					END IF
				NEXT
				IF NOT rsInlineChildrenDataSource IS nothing THEN
					IF NOT(rsInlineChildrenDataSource.BOF AND rsInlineChildrenDataSource.EOF) THEN
						rsInlineChildrenDataSource.MoveFirst
						DO UNTIL rsInlineChildrenDataSource.EOF
							Set oChildrenTable = new URLLoader
'							oChildrenTable.Id="Panel_"&TRIM(rsListChildrenDataSource("TABLE_NAME"))&"_"&Me.TableIdentifier&"_"&RandomNumber(1000)
'							oChildrenTable.HeaderText=ToTitleFromPascal(rsListChildrenDataSource("DISPLAY_NAME"))
'	'						oTabPanel.ContentURL="oIdentifier=getParent('TR', this).all('identifier'); oIdentifier=oIdentifier.length?oIdentifier(0):oIdentifier; 'List.asp?requestTable="&TRIM(rsListChildrenDataSource("TABLE_NAME"))&"&"&rsListChildrenDataSource("COLUMN_NAME")&"='+oIdentifier.value"
							oChildrenTable.ContentURL="oIdentifier=getIdentifier(this); 'List.asp?requestTable="&TRIM(rsInlineChildrenDataSource("TABLE_NAME"))&"&ParentIdentifier="&TRIM(rsInlineChildrenDataSource("COLUMN_NAME"))&"&"&TRIM(rsInlineChildrenDataSource("COLUMN_NAME"))&"='+(oIdentifier.value=='[new]'?'NULL':oIdentifier.value)"
							oChildrenTable.AddContent("<b>Cargando...</b>")
							
'							Set oChildrenTable = new DynamicTable
'							oChildrenTable.Debug=TRUE
'							oChildrenTable.MainTable=FALSE
'							oChildrenTable.DBTableName=rsInlineChildrenDataSource("TABLE_NAME")
'							IF TRIM(Me.IdentityDataField.Value)="" OR ISNULL(Me.IdentityDataField.Value) THEN
'								oChildrenTable.QueryFilter=rsInlineChildrenDataSource("COLUMN_NAME")&"=0"
'							ELSE
'								oChildrenTable.QueryFilter=rsInlineChildrenDataSource("COLUMN_NAME")&"="&Me.IdentityDataField.Value
'							END IF
'							oChildrenTable.InlineInsert=TRUE
'							oChildrenTable.ViewMode="Grid"
'							oChildrenTable.FieldNull="&nbsp;"
'							oChildrenTable.Mode=sBehaveMode
'							oChildrenTable.GetChildrenTables=FALSE
'							oChildrenTable.RetrieveData() 
'	'						oChildrenTable.DataFields("IdTipoTelefono").Control.Methods.OnClick="alert(getDBColumnName(getIdentifier(getIdentifier(this)))+' '+getVal(getIdentifier(getIdentifier(this))))"
'							oChildrenTable.DataFields(TRIM(rsInlineChildrenDataSource("COLUMN_NAME"))).ScaffoldColumn=FALSE
'							oChildrenTable.IdentityDataField.Control.Properties.SetProperty "checkParent", "true"
'							oChildrenTable.IdentityDataField.Control.Properties.SetProperty "otherFields", "getDBColumnName(getIdentifier(this))+'='+getVal(getIdentifier(this))"
'	'						checkParent="true" db_column_name="IdRelacionPuestos" value="<% IF rsDatos("IdRelacionPuestos")<>"" THEN %%<%= rsDatos("IdRelacionPuestos") %%<% ELSE %%0<% END IF %%" otherFields="'IdPuesto=<%= rsDatos("IdPuesto") %%'"
							Set oImgExpand=new ImageButton
							oImgExpand.ImageURL=sImageLibraryPath & "expand.jpg"
							oImgExpand.Width=15
							oImgExpand.Height=15
							oImgExpand.Methods.OnClick="document.all[this.sourceIndex+1].style.display='inline';"
							Set oCell = new TableCell
							oCell.AddContent(oImgExpand)
							oCell.AddContent(oChildrenTable)
							oRow.AddCell(oCell)
							rsInlineChildrenDataSource.MoveNext
						LOOP
					END IF
				END IF
				'response.write oRow.RowNumber&": "&oRow.CellCount()&"<br>"
				IF sHeaderTemplate<>"" THEN
					IF IsObject(oRow.DataFields) THEN
						sNewHeader=evaluateTemplate(sHeaderTemplate, oRow.DataFields, oRow.RowNumber) 'response.write oRow.Fields.item("Fraccionamiento").Value&"<br>"
					END IF
					IF sNewHeader<>sPastHeader THEN 
						Dim oNewHeaderRow
						Set oNewHeaderRow=new TableRow
						Set oHead=new TableCell
						oHead.CellType="TH"
						oHead.AddContent(sNewHeader)
						oHead.Align="left"
						oHead.Style.BackgroundColor="red"
						oHead.ColSpan=oHeaderRow.CellCount
						oNewHeaderRow.AddCell(oHead)
						Me.AddRow(oNewHeaderRow)
						sPastHeader=sNewHeader
					END IF
				END IF 
				IF ISOBJECT(oTotalRow) AND NOT oTotalRow IS nothing THEN Me.AddRow(oTotalRow)
				Me.AddRow(oRow)
			NEXT
		END SELECT
		
		FOR EACH oDataField IN dDataFields.items
			IF oDataField.AutoRowSpanEnabled THEN oDataField.AutoRowSpan() END IF
		NEXT
		
		SELECT CASE UCASE(Me.Mode)
		CASE UCASE("Filter"), UCASE("insert"), UCASE("update")
			IF Me.MainTable THEN
				response.write "<script language=""JavaScript"">try { top.frames('buttons').btnSave.enabled=true } catch(e) {}</script>"
				Set oSubmit = new Button
				oSubmit.Id="submit_button"
				oSubmit.Value="Guardar Datos"
'				oSubmit.Properties.SetProperty "toDoOnSubmit", "try {element.document.location.reload()} catch(e2) {alert(e2.description)}"
				Set oRow=New TableRow
				Set oCell=New TableCell
				oCell.AddContent(oSubmit)
				oRow.AddCell(oCell)
				Me.AddRow(oRow)
				oRow.Visible=FALSE
			END IF
		END SELECT
		IF IsObject(oProgress) THEN oProgress.SetComplete()
		bDataRetrieved=TRUE
	End Sub

	Public Sub BuildTable()
		Me.WriteCode()
		EXIT SUB
		IF bShowColNames=TRUE THEN 
'			IF oTimeLineHeaderRow.CellCount>0 THEN
'				oTimeLineHeaderRow.WriteAlternateCode()
'			END IF
'			IF oHeaderRow.CellCount>0 THEN
				'AssignPosition(oHeaderRow)
'				oHeaderRow.WriteCode()
'			END IF
'			IF bShowMonthName THEN
			Set oRow=new TableRow
			oRow.IsHeader=TRUE
			IF Me.MainTable THEN oRow.id="header"
			IF sFreezeUntilColumn<>"" OR UCASE(sTableType)=UCASE("timeline") THEN
				bStopFreezing=FALSE
			ELSE
				bStopFreezing=TRUE
			END IF
			FOR iColCounter=0 TO iColCount-1
				IF NOT(IsNaN(DataFieldsMetaData(iColCounter, 2))) THEN EXIT FOR END IF
		        IF DataFieldsMetaData(iColCounter, 0) THEN 
					Set oCell=new TableCell
					IF Me.MainTable AND NOT bStopFreezing THEN oCell.id="freeze"
					oCell.CellType="TH"
					oCell.AddContent("<b>"&DataFieldsMetaData(iColCounter, 2)&"</b>")
					oRow.AddCell(oCell)
					IF sFreezeUntilColumn<>"" AND INSTR(" "&LCASE(sFreezeUntilColumn)&",", " "&LCASE(TRIM(DataFieldsMetaData(iColCounter, 2))&","))>0 THEN
							bStopFreezing=TRUE
					END IF 
				END IF
		    NEXT

		'AssignPosition(oRow)
		Me.AddRow(oRow)
	END IF 
	IF isArray(aData) THEN
	    iRowCount=ubound(aData)
	ELSE
		iRowCount=0
	END IF

	IF Err.Number<>0 THEN
		response.write "Error en iRowCount"
	END IF

	IF UCASE(sViewMode)=UCASE("Details") THEN
		
	ELSEIF UCASE(sViewMode)=UCASE("Grid") THEN

'	iRowCount=0
    FOR iThisRow=0 TO iRowCount-1 	
		TotalHorizontal=0
		IF sSpanRowsBy="" THEN
			thisRowValReference=1
			Rowspan=1
		ELSE
			thisRowValReference=evalTemplate(sSpanRowsBy, iThisRow)
			Rowspan=calculateRowSpan(iThisRow, sSpanRowsBy)
		END IF 
		Set oRow=new TableRow
		IF NOT bDisableRowNumbers THEN 
			IF NOT(lastRowValReference=thisRowValReference) THEN
				iRowCount=iRowCount+1 
				Set oCell=new TableCell
				oCell.CellType="TH"
				oCell.AddContent("# "&iRowCount)
				IF Me.MainTable THEN oCell.id="freeze"
				oCell.Rowspan=Rowspan
				oRow.AddCell(oCell)
			ELSE
			END IF
		END IF  

		iShownColCounter=0
		IF sFreezeUntilColumn<>"" OR UCASE(sTableType)=UCASE("timeline") THEN
			bStopFreezing=FALSE
		ELSE
			bStopFreezing=TRUE
		END IF
		IF 1=0 THEN
	        FOR iColCounter=0 TO iColCount-1
	'            fldName=DataFieldsMetaData(iColCounter, 2)
	            fldDisplayName=DataFieldsMetaData(iColCounter, 2)
				IF NOT(IsNaN(fldDisplayName)) THEN EXIT FOR END IF
	
			   	Dim objField
				Set objField = New DynamicField
				objField.Value=getDataRowValue(iThisRow, iColCounter, aData)
				
				spanColumn=INSTR(" "&LCASE(sSpanRowsForColumns)&",", " "&LCASE(TRIM(DataFieldsMetaData(iColCounter, 2))&","))>0
				wrapColumn=INSTR(" "&LCASE(sWrapForColumns)&",", " "&LCASE(TRIM(DataFieldsMetaData(iColCounter, 2))&","))>0
				rightAlignColumn=INSTR(" "&LCASE(sRightAlignForColumns)&",", " "&LCASE(TRIM(DataFieldsMetaData(iColCounter, 2))&","))>0
	   			leftAlignColumn=INSTR(" "&LCASE(sLeftAlignForColumns)&",", " "&LCASE(TRIM(DataFieldsMetaData(iColCounter, 2))&","))>0
	
	
				IF lastRowValReference=thisRowValReference AND spanColumn THEN %>
	
	<%			ELSEIF DataFieldsMetaData(iColCounter, 0) THEN 
					iShownColCounter=iShownColCounter+1
		            IF trim(objField.Value)="" THEN
		                objField.Value=sFieldBlank
					ELSEIF objField.Value<>"" AND UCASE(TRIM(DataFieldsMetaData(iColCounter, 3)))="MONEY" THEN
						objField.Value=FORMATCURRENCY(objField.Value)
		            END IF
		
		            fldformat=oDictionary.item("fldFormat_" & fldDisplayName)
					IF fldformat="" THEN
			            fldformat=oDictionary.item("fldFormat_" & iShownColCounter)
					END IF
		            IF fldformat<>"" THEN
						objField.Value=evalTemplate(fldformat, iThisRow)
					END IF 
					
					fldtemplate=""
					IF oDictionary.exists("objTableCell_" & fldDisplayName) THEN
						set objTableCell=NOTHING
			            set objTableCell=oDictionary.item("objTableCell_" & fldDisplayName)
			            IF NOT objTableCell IS NOTHING THEN 
							objField.Name=DataFieldsMetaData(iColCounter, 2)
							objField.Style="color:'red'"
							objField.Mode="insert"
							objField.DataType="moneda"
							objTableCell.InnerHTML=objField.FormatedValue
							fldtemplate= objTableCell.HTMLCode
						END IF 
					END IF
					
	'				fldtemplate=oDictionary.item("fldTemplate_" & fldDisplayName)
	'				IF fldtemplate="" THEN
	'		            fldtemplate=oDictionary.item("fldTemplate_" & iShownColCounter)
	'				END IF
					IF fldtemplate<>"" THEN
		                response.write replace(fldtemplate, "{0}", objField.Value) '"datarow(oDictionary(""fieldsDictionary"")(""PrecioViv""))-123")
					ELSEIF NOT bStopFreezing THEN 
						Set oCell=new TableCell
						oCell.CellType="TH"
						oCell.AddContent(objField.Value&"<br>")
						IF Me.MainTable THEN oCell.Id="freeze"
						oCell.NoWrap=NOT(wrapColumn)
						IF spanColumn THEN oCell.RowSpan=Rowspan
						IF leftAlignColumn THEN 
							oCell.Align="left"
						ELSEIF rightAlignColumn OR UCASE(DataFieldsMetaData(iColCounter, 3))="MONEY" THEN 
							oCell.Align="right"
						END IF 
						oRow.AddCell(oCell)
						IF sFreezeUntilColumn<>"" AND INSTR(" "&LCASE(sFreezeUntilColumn)&",", " "&LCASE(TRIM(DataFieldsMetaData(iColCounter, 2))&","))>0 THEN
							bStopFreezing=TRUE
						END IF
					ELSE 
						Set oCell=new TableCell
						oCell.AddContent(objField.Value)
						IF spanColumn THEN oCell.SpanColumn=Rowspan
						IF UCASE(DataFieldsMetaData(iColCounter, 3))="MONEY" THEN oCell.Align="right"
						oCell.NoWrap=NOT(wrapColumn)
						oRow.AddCell(oCell)
					END IF
			      	cellcount=cellcount+1
				END IF
	        NEXT 
		END IF
		IF UCASE(sTableType)=UCASE("timeline") THEN 
			iDia=CDATE(sMinDate-DatePart("w",sMinDate,2,2)+1) 
			iTimeLineColCounter=0
			DO WHILE iDia<=sMaxDate
				iTimeLineColCounter=iTimeLineColCounter+1
				IF sPeriodo="DIARIO" THEN
					iSemana=DatePart("d",iDia,2,2)
				ELSEIF sPeriodo="SEMANA" THEN
					iSemana=semanaReal(iDia)
				END IF
				
				IF iSemana<10 THEN
					iSemana="0" & CSTR(iSemana)
				END IF
				fldvalue=evalTemplate("{"&RIGHT(CSTR(anioReal(iDia)),2)&CSTR(iSemana)&"}", iThisRow) 
				Set oCell=new TableCell
				IF fldvalue<>"" THEN 
					IF timeLineDataType="MONEY" THEN 
						timeLineVerticalTotals(iTimeLineColCounter)=CSNG(timeLineVerticalTotals(iTimeLineColCounter))+CSNG(fldvalue) 
						TotalHorizontal=TotalHorizontal+CSNG(fldvalue) 
						GranTotal=GranTotal+fldvalue 
						oCell.AddContent(FORMATCURRENCY(fldvalue))
					ELSE 
						oCell.AddContent(fldvalue)
					END IF 
				ELSE 
				oCell.AddContent("&nbsp;")
				END IF 
				IF timeLineDataType="MONEY" THEN oCell.Align="right"
				IF fldvalue<>"" AND timeLineDataType="MONEY" AND CSNG(fldvalue)<0 THEN oCell.Style="color:'red';"
				oRow.AddCell(oCell)
				iDia=iDia+iDiasPeriodo
			LOOP
			IF bHorizontalTotalEnabled THEN 
				Set oCell=new TableCell
				oCell.CellType="TH"
				IF timeLineDataType="MONEY" THEN 
					oCell.AddContent(FORMATCURRENCY(TotalHorizontal)) 
				ELSE 
					oCell.AddContent(TotalHorizontal)
				END IF
				oRow.AddCell(oCell)
			END IF
		END IF
		'AssignPosition(oRow)
		Me.AddRow(oRow)
		'        response.write rowfooter & vbcrlf
		IF sSpanRowsBy="" THEN
			lastRowValReference=""
		ELSE
			lastRowValReference=thisRowValReference
		END IF
    NEXT 

	IF bVerticalTotalEnabled THEN 
		Set oRow=new TableRow
		IF NOT bDisableRowNumbers THEN 
			Set oCell=new TableCell
			oCell.CellType="TH"
			IF Me.MainTable THEN oCell.id="freeze"
			oCell.AddContent("Total:")
			oRow.AddCell(oCell)
		END IF 
		IF sFreezeUntilColumn<>"" OR UCASE(sTableType)=UCASE("timeline") THEN
			bStopFreezing=FALSE
		ELSE
			bStopFreezing=TRUE
		END IF %>
<%		FOR iColCounter=0 TO iColCount-1 %>
<% 			IF NOT(IsNaN(DataFieldsMetaData(iColCounter, 2))) THEN EXIT FOR END IF %>
<%			IF DataFieldsMetaData(iColCounter, 0) THEN 
				Set oCell=new TableCell
				oCell.CellType="TH"
				IF Me.MainTable AND NOT bStopFreezing THEN oCell.id="freeze"
				oCell.AddContent("&nbsp;")
				oRow.AddCell(oCell) %>
<%				IF sFreezeUntilColumn<>"" AND INSTR(" "&LCASE(sFreezeUntilColumn)&",", " "&LCASE(TRIM(DataFieldsMetaData(iColCounter, 2))&","))>0 THEN
					bStopFreezing=TRUE
				END IF %>
		<% 	END IF %>
<% 		NEXT 
		IF UCASE(sTableType)=UCASE("timeline") THEN 
			iDia=CDATE(sMinDate-DatePart("w",sMinDate,2,2)+1)
			iTimeLineColCounter=0
			DO WHILE iDia<=sMaxDate
				iTimeLineColCounter=iTimeLineColCounter+1
				IF sPeriodo="DIARIO" THEN
					iSemana=DatePart("d",iDia,2,2)
				ELSEIF sPeriodo="SEMANA" THEN
					iSemana=semanaReal(iDia)
				END IF
				
				IF iSemana<10 THEN
					iSemana="0" & CSTR(iSemana)
				END IF
				fldvalue=timeLineVerticalTotals(iTimeLineColCounter)
				
				Set oCell=new TableCell
				oCell.CellType="TH"
				IF fldvalue<>"" THEN 
					IF timeLineDataType="MONEY" THEN 
						TotalHorizontal=TotalHorizontal+CSNG(fldvalue) 
						oCell.AddContent(FORMATCURRENCY(fldvalue))
					ELSE 
						oCell.AddContent(fldvalue)
					END IF 
				ELSE 
					oCell.AddContent("&nbsp;")
				END IF 
				IF Me.MainTable AND NOT bStopFreezing THEN oCell.id="freeze"
				IF timeLineDataType="MONEY" THEN 
					oCell.Align="right"
					IF CSNG(fldvalue)<0 THEN
						oCell.Style="color:'red';"
					END IF
				END IF
				oRow.AddCell(oCell) 
				iDia=iDia+iDiasPeriodo 
			LOOP %>
			<% IF bHorizontalTotalEnabled THEN 
				Set oCell=new TableCell
				oCell.CellType="TH"
				IF timeLineDataType="MONEY" THEN 
					oCell.AddContent(FORMATCURRENCY(TotalHorizontal))
				ELSE
					oCell.AddContent(TotalHorizontal)
				END IF 
				oRow.AddCell(oCell)
			END IF 
		END IF 
	'AssignPosition(oRow)
	Me.AddRow(oRow)	
	END IF 
END IF 
Me.WriteCode()
'	</table>
	End Sub
	Private Sub Terminate()
		Set oDictionary=nothing
		Set oHeaderRow = nothing
		Set oRowPosDictionary=nothing
	End Sub
End Class 
%>