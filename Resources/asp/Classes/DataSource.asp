<% 
Function [$DataSource]()
	DIM oDataSource:	Set oDataSource = new DataSource
'	oDataSource.DataField = oDataField
	Set [$DataSource] = oDataSource
End Function
Class DataSource
	Private sFieldNull, bShowRowNumber, bDataBound
	Private sConn, oConnection, sCommandText, sPagedCommandText, sConnectionString, rsRecordSet, aRecords, oMetadata, oFields
	Private bEnablePaging, iPageSize, iPageIndex, sRankORDERBY, rsAllOriginalRecords, sAllOriginalRecords, iTotalRecords, iRecordCount, sDBLanguage, sQueryFilter, dFilterMembers
	Private oPageSelector, dParameters, oProgressBar

	Private oTable
	Private oIndex

	'Timers
    Private oTimerOpenConection, oTimerQuery, oTimeFetching, oTimeMetadata
	
	Private Sub Class_Terminate()
		IF NOT oConnection IS NOTHING THEN oConnection.Close
		Set oConnection = nothing
		set rsAllOriginalRecords=nothing
		set rsRecordSet=nothing
		Set dFilterMembers = NOTHING
		Set oTimerOpenConection = nothing
		Set oTimerQuery = nothing
		Set oTimeFetching = nothing
		Set oTimeMetadata = nothing
		Set oPageSelector = nothing
		Set oProgressBar = nothing
	End Sub

	Private Sub Class_Initialize()
		Set oIndex = new NumericValue
		oIndex.Value = 0
		Set oConnection = nothing
		Set oTimerOpenConection = new StopWatch
		Set oTimerQuery = new StopWatch
		Set oTimeFetching = new StopWatch
		Set oTimeMetadata = new StopWatch
		Set oConnection = server.createobject("ADODB.Connection")
		set rsRecordSet=nothing
		Set oMetadata = new TableMetadata
		Set dFilterMembers = server.CreateObject("Scripting.Dictionary")
		Set dParameters = server.CreateObject("Scripting.Dictionary")
		sConn=application("StrCnn")
		OpenConnection()
		IF request.querystring("PageIndex")<>"" THEN
			iPageIndex = CINT(request.querystring("PageIndex"))
		ELSEIF request.form("PageIndex")<>"" THEN
			iPageIndex = CINT(request.form("PageIndex"))
		ELSE
			iPageIndex=1
		END IF
		sFieldNull="&nbsp;"
		bShowRowNumber=TRUE
		Set oPageSelector = nothing
		Set oProgressBar = nothing
	End Sub
	
	Public Property Get ProgressBar()
		IF oProgressBar IS NOTHING THEN Set oProgressBar = new Progress
		Set ProgressBar = oProgressBar
	End Property

	Public Property Get Table()
		Set Table = oTable
	End Property
	Public Property Let Table(input)
		Set oTable = input
	End Property

	Public Property Get QueryFilter()
		QueryFilter = sQueryFilter
	End Property
	Public Property Let QueryFilter(input)
		sQueryFilter = input
	End Property

	Public Property Get Parameters()
		Set Parameters = dParameters
	End Property

	Public Sub ShowProperties()
		response.write "Properties for recordset: <br>"
		DIM oColumn
		FOR EACH oColumn IN rsRecordSet.Properties
			response.write "<b>"&oColumn.Name&"</b>: "&oColumn.Value&"<br>"
		NEXT
	End Sub

	Public Sub GetMetadataFromDB(ByVal sDBTableName, ByVal sTableViewMode, ByVal sRenderMode)
		oMetadata.GetMetadataFromDB sDBTableName, sTableViewMode, sRenderMode
		IF UCASE(sRenderMode)="FILTER" OR UCASE(sRenderMode)="INSERT" THEN 
			Me.PageSize=0
		ELSEIF IsNullOrEmpty(Me.PageSize) THEN 
			Me.PageSize=oMetadata.RecordsPerPage
		END IF
		IF oMetadata.AppTableFilter THEN Me.QueryFilter=Me.QueryFilter&EVAL(REPLACE(oMetadata.AppTableFilter, "", """"))
	End Sub
	
	Public Sub AutoCreate(ByVal sDBTableName, ByVal sTableViewMode, ByVal sRenderMode)
		oTimeMetadata.StartTimer()
		Me.GetMetadataFromDB sDBTableName, sTableViewMode, sRenderMode
		DIM sParameter, sTableParameters: sTableParameters=oMetadata.TableParameters
		DIM bUseParameters
		IF NOT(IsNullOrEmpty(TRIM(sTableParameters))) THEN
			FOR EACH sParameter IN SPLIT(sTableParameters, ",")
				sParameter=replaceMatch(sParameter, "(\[|\])", "")
				DIM bParameterInQuerystring: bParameterInQuerystring=(INSTR("&@"&TRIM(request.querystring)&"=", "&"&TRIM(sParameter)&"=")>0)
				DIM sParameterValue: sParameterValue=request.querystring(TRIM(sParameter))
				sParameterValue=Try((NOT(IsNumeric(sParameterValue)) AND NOT(IsNullOrEmpty(sParameterValue)) ), QuoteName(sParameterValue), sParameterValue)
				bUseParameters=Try((NOT(bUseParameters) AND bParameterInQuerystring), TRUE, CBOOL(bUseParameters))
				dParameters(sParameter)=Try((bParameterInQuerystring AND NOT(IsNullOrEmpty(sParameterValue))), sParameterValue, "default")
			NEXT
		END IF
		DIM sColumn
		'Debugger Me, oMetadata.AllColumns: [&Stop] Me
		FOR EACH sColumn IN SPLIT(oMetadata.AllColumns, ",")
			sColumn=replaceMatch(sColumn, "(\[|\])", "")
			sParameter=sColumn
			IF INSTR("&"&TRIM(request.querystring)&"=", "&"&TRIM(sColumn)&"=")>0 THEN
				dFilterMembers(TRIM(sColumn))=request.querystring(TRIM(sColumn))
			ELSEIF INSTR("&"&TRIM(request.querystring)&"=", "&@"&TRIM(sColumn)&"=")>0 THEN
				sParameter="@"&TRIM(sColumn)
			END IF
			
			'IF session("IdUsuario")=1 THEN
			'	Parameters=getMatch(request.querystring, "&(?:@)(\[?("&sColumn&")\]?)=([^@]*)(?!(\s*@))")
			'END IF
			IF INSTR("&"&TRIM(request.querystring)&"=", "&"&TRIM(sParameter)&"=")>0 THEN 
				IF TRIM(request.querystring(TRIM(sParameter)))="[all]" THEN 
				ELSEIF TRIM(request.querystring(TRIM(sParameter)))="NULL" THEN 
					Me.QueryFilter=Me.QueryFilter&" AND "&TRIM(sColumn)&" IS "&request.querystring(TRIM(sParameter))&"" 
				ELSEIF TRIM(request.querystring(TRIM(sParameter)))<>"" THEN
					Me.QueryFilter=Me.QueryFilter&" AND "&TRIM(sColumn)&"='"&request.querystring(TRIM(sParameter))&"'"
					'Me.QueryFilter=Me.QueryFilter&" AND "&TRIM(sColumn)&" LIKE '%"&request.querystring(TRIM(sParameter))&"%'"
				END IF
			END IF
		NEXT
		
'		RESPONSE.WRITE "QueryFilter: "&Me.QueryFilter
'		RESPONSE.END
		oTimeMetadata.StopTimer()

		DIM sParameters: sParameters=REPLACE(JOIN(dParameters.items, ","), "'", "''")
		Me.CommandText="dbo.__GetTableData @TableName='"& sDBTableName &"', @Parameters="& Try(bUseParameters, QuoteName(sParameters), "NULL") &", @ViewMode='"&sTableViewMode&"', @Mode='"&sRenderMode&"', @QueryFilters='"&REPLACE(Me.QueryFilter, "'", "''")&"', @InitialRecord="& NullToText(NullIfEmptyOrNullText(Me.InitialRecord)) &", @PageSize="& NullToText(NullIfEmptyOrNullText(iPageSize)) &", @FullPath='"&request.querystring("TablePath")&"'"
		'IF SESSION("IdUsuario")=1 THEN Debugger Me, Me.CommandText&"<br>": [&Stop] Me
		Me.DataBind()
		IF IsObjectReady(Me.Metadata) THEN
			oTimeFetching.StartTimer()
			DIM oField, i, sFieldName
			Set oFields = new DataFieldCollection
			oFields.DataSource = Me
			oTimeFetching.StopTimer()
		END IF
	End Sub
	
	Public Function DataBind()
		IF sCommandText="" THEN 
			Err.Raise 1, "ASP 101", "SQL Query is not defined"
			response.end
		END IF
'		response.write sCommandText&"<br>"
		oTimerQuery.StartTimer()
		IF bEnablePaging THEN
			set rsRecordSet=oConnection.execute(sPagedCommandText)
		ELSE
'			set rsRecordSet=oConnection.execute(sCommandText)
			ON ERROR  RESUME NEXT
			Set rsRecordSet = Server.CreateObject("ADODB.RecordSet")
			rsRecordSet.CursorLocation 	= 3
			rsRecordSet.CursorType 		= 3
			rsRecordSet.open sCommandText, oConnection

			IF Err.Number<>0 THEN
				Debugger Me, Err.Description 
				Debugger Me, "SQL Query: "&sCommandText
				response.end
			END IF

			DIM sFieldName, rsField, bSQLRowCountExists: bSQLRowCountExists=FALSE
			FOR EACH rsField IN rsRecordSet.Fields
				IF rsField.name="SQLRowCount" THEN 
					bSQLRowCountExists=TRUE
					EXIT FOR
				END IF
			NEXT
			
			IF rsRecordSet.BOF AND rsRecordSet.EOF THEN
				iTotalRecords=0
				iRecordCount=0
			ELSEIF bSQLRowCountExists THEN
				iTotalRecords = rsRecordSet("SQLRowCount") 'rsRecordSet.RecordCount
				iRecordCount = rsRecordSet.RecordCount
			ELSE
				iTotalRecords = rsRecordSet.RecordCount
				iRecordCount = rsRecordSet.RecordCount
			END IF
			
'			iTotalRecords= rsRecordSet.QueryTotalRecord
			'response.write rsRecordSet.RecordCount
		END IF
		oTimerQuery.StopTimer()
		'Me.CreateHTMLPager()

		bDataBound=TRUE
		Set DataBind = rsRecordSet
	End Function
	
	Public Sub OpenConnection()
		oTimerOpenConection.StartTimer()
		oConnection.open sConn
		oConnection.CommandTimeout = 120 
		oTimerOpenConection.StopTimer()
	End Sub
	
	Public Property Get Fields()
		Set Fields = oFields
	End Property

	Public Property Get Metadata()
		Set Metadata = oMetadata
	End Property

	Public Property Get PageSelector()
		Set PageSelector = oPageSelector
	End Property

	Public Property Get DBLanguage()
		DBLanguage = sDBLanguage
	End Property
	Public Property Let DBLanguage(input)
		sDBLanguage = input
		Me.SetDBLanguage(sDBLanguage)
	End Property
	
	Public Property Get FieldNull()
		FieldNull = sFieldNull
	End Property
	Public Property Let FieldNull(input)
		sFieldNull = input
	End Property

   Public Property Get RecordSet()
		Set RecordSet = rsRecordSet
    End Property

	Public Property Get ShowRowNumber()
		ShowRowNumber = bShowRowNumber
	End Property
	Public Property Let ShowRowNumber(input)
		bShowRowNumber = input
	End Property

	Public Property Get DataBound()
		DataBound = bDataBound
	End Property

	Public Property Get FilterMembers()
		Set FilterMembers = dFilterMembers
	End Property

    Public Property Get TotalRecords()
		TotalRecords = iTotalRecords
    End Property

	Public Property Get RecordCount()
		RecordCount = iRecordCount
	End Property

    Public Property Get EnablePaging()
		EnablePaging = bEnablePaging
    End Property
    Public Property Let EnablePaging(input)
		bEnablePaging = input
    End Property

	Public Property Get InitialRecord()
		InitialRecord = iPageSize*(iPageIndex-1)
	End Property

    Public Property Get PageSize()
		PageSize = iPageSize
    End Property
    Public Property Let PageSize(input)
		iPageSize = input
    End Property

    Public Property Get PageIndex()
		PageIndex = iPageIndex
    End Property
    Public Property Let PageIndex(input)
		iPageIndex = input
    End Property

    Public Default Property Get CommandText()
		CommandText = sCommandText
    End Property
    Public Property Let CommandText(input)
		sCommandText = input
    End Property
	
    Public Property Get PagedCommandText()
		PagedCommandText = sPagedCommandText
    End Property

    Public Property Get ConnectionString()
		CommandText = sConnectionString
    End Property
    Public Property Let ConnectionString(input)
		sConnectionString = input
    End Property
	
    Public Property Get TimeOpenConn()
		TimeOpenConn = 	oTimerOpenConection.TotalTime()
    End Property
	
	Public Property Get TimeQuery()
		TimeQuery = oTimerQuery.TotalTime
    End Property
       
	Public Property Get TimeFetching()
		TimeFetching = oTimeFetching.TotalTime
	End Property

	Public Property Get TimeMetadata()
		TimeMetadata = oTimeMetadata.TotalTime
	End Property

	Public Property Get Index()
		Set Index = oIndex
	End Property
	Public Property Let Index(input)
		oIndex.Value = input
	End Property

	Public Sub MoveNext()
		oIndex.Value=oIndex.Value+1
'		IF NOT oDataSource.RecordSet.EOF THEN
'			oDataSource.RecordSet.MoveNext
'		END IF
	End Sub

	Public Sub MoveTo(input)
		oIndex.Value=input
	End Sub

	Public Sub MoveFirst()
		IF NOT(oDataSource.RecordSet.BOF AND oDataSource.RecordSet.EOF) THEN
			oIndex.Value=0
'			oDataSource.RecordSet.MoveFirst
		END IF
	End Sub

	Public Property Get BOF()
		BOF = (oIndex.Value-1<0)
	End Property

	Public Property Get EOF()
		EOF = oIndex.Value>iRecordCount-1
	End Property

	Public Sub MoveLast()
		oIndex.Value=UBOUND(aValues)-1
'		oDataSource.RecordSet.MoveLast
	End Sub

	Public Sub MovePrevious()
		oIndex.Value=oIndex.Value-1
'		oDataSource.RecordSet.MovePrevious
	End Sub

	Public Sub SetDBLanguage(ByVal sLanguage)
'		IF sLanguage<>sDBLanguage THEN
		oConnection.open sConn
		oConnection.Execute("sp_defaultlanguage @loginame = 'sa' ,@language = '"&sLanguage&"'")
		oConnection.Execute("SET LANGUAGE "&sLanguage)
'		response.write "Language set to "&sLanguage&"<br>"
		oConnection.close
'		END IF
	End Sub
	
	Public Sub CreateHTMLPager
		IF Me.PageSize>0 THEN
			Set oPageSelector = new Div
			oPageSelector.Id="freeze"
			DIM oImgLArrow: Set oImgLArrow=new ImageButton
			oImgLArrow.ImageURL="\"&application("btn_LArrow_path")
			oImgLArrow.Disabled=TRUE
			oImgLArrow.Width=20
			oImgLArrow.Height=20
			DIM oImgRArrow: Set oImgRArrow=new ImageButton
			oImgRArrow.ImageURL="\"&application("btn_RArrow_path")
			oImgRArrow.Disabled=TRUE
			oImgRArrow.Width=20
			oImgRArrow.Height=20
			
			DIM sCurrentURL: sCurrentURL=Request.serverVariables("PATH_INFO")
			IF Request.ServerVariables("QUERY_STRING")<>"" THEN
				sCurrentURL=sCurrentURL&"?"&REPLACE(Request.ServerVariables("QUERY_STRING"), "'", "\'")
			END IF
			DIM sGoBackURL: sGoBackURL=updateURLString(sCurrentURL, "PageIndex", Me.PageIndex-1)
			DIM sGoForwardURL: sGoForwardURL=updateURLString(sCurrentURL, "PageIndex", Me.PageIndex+1)
			IF Me.PageIndex-1>0 THEN
				oImgLArrow.CommandText="OpenLink(encodeURI('"&REPLACE(REPLACE(sGoBackURL, "'", "\'"), "\\'", "\'")&"'), false, true)"
				oImgLArrow.Disabled=FALSE
			END IF
			DIM iTotalPages: iTotalPages=ROUND((Me.TotalRecords/Me.PageSize)+.5, 0)
			IF Me.PageIndex+1<=iTotalPages THEN 
				oImgRArrow.CommandText="OpenLink(encodeURI('"&REPLACE(REPLACE(sGoForwardURL, "'", "\'"), "\\'", "\'")&"'), false, true)"
				oImgRArrow.Disabled=FALSE
			END IF
			DIM oDropDownSelector: Set oDropDownSelector = new NumberList
			oDropDownSelector.Control.NewOption CSTR(Me.PageIndex), CSTR(Me.PageIndex)
			oDropDownSelector.Control.OptionChoose=FALSE
			oDropDownSelector.Control.Properties.SetProperty "isSubmitable", "false"
			oDropDownSelector.MinValue=1
			oDropDownSelector.MaxValue=iTotalPages
			oDropDownSelector.Methods.OnChange="OpenLink(encodeURI('"&updateURLString(REPLACE(REPLACE(sCurrentURL, "'", "\'"), "\\'", "\'"), "PageIndex", "'+this.value+'")&"'), false, true)"
			oDropDownSelector.Value=CSTR(Me.PageIndex)&"@:@"&CSTR(Me.PageIndex)
			oPageSelector.AddContent(oImgLArrow)
			oPageSelector.AddContent("&nbsp;<b>P�gina: ")
			oPageSelector.AddContent(oDropDownSelector)
			oPageSelector.AddContent(" / "&iTotalPages&"</b>&nbsp;")
			oPageSelector.AddContent(oImgRArrow)
		END IF
	End Sub
End Class
 %>