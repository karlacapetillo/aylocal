<% 
Class BulletedList
	'Properties
	Private sDisplayMode, sItemsCommandText, sDisplayTextFormat
	
'	Private sName
	
	'Settings
	
	'Objects 	
	Private oTag, dOptionElements, aOptionElements, aOptions(100), oDataSource, rsRecordSet, oParentCell, oProperties, oContainer, oRelatives
	
	'Private variables
	Private bBuilt, sThisValue, sThisText, iItemsIndex, iItemsCount
	
	Public Property Get Visible()
		Visible = oTag.Visible
	End Property
	Public Property Let Visible(input)
		oTag.Visible = input
	End Property

	Public Property Get Object()
		Set Object = oTag
	End Property

	Private Sub Class_Initialize()
		Set oRelatives = [$Relatives](Me)
		iItemsCount=-1
		Set oTag = new Tag
		oTag.TagName="ul"
		oTag.HasClosingTag=TRUE
		sDisplayMode = "text"
'		Me.AllowInsert=TRUE
		Set dOptionElements=server.CreateObject("Scripting.Dictionary")
		Set oDataSource = new Recordset
		Set rsRecordSet = nothing
		Set oProperties = new PropertiesCollection
	End Sub
	
	'Output interfaces
	Public Property Get Id()
		Id = oTag.Id
	End Property
	
	'Input Interfaces
	Public Property Let Id(input)
		oTag.Id = input
	End Property
	
	'Output objects
	Public Property Get DataSource()
		Set DataSource=oDataSource
	End Property
	
	Public Property Get Style()
		Set Style = oTag.Style
	End Property
	
	Public Property Get Methods()
		Set Methods = oTag.Methods
	End Property
	
	Public Property Get Items()
		Items=aOptions
	End Property
	
	Public Property Get Properties()
		Set Properties = oProperties
	End Property

	'Output properties
	Public Property Get Relatives()
		Set Relatives = oRelatives
	End Property
	
	Public Property Get Container()
		Set Container = oRelatives.Parent
	End Property
	Public Property Let Container(input)
		oRelatives.Parent = input
	End Property

	Public Property Get ParentCell()
		Set ParentCell = oParentCell
	End Property

	Public Property Get DisplayTextFormat()
		DisplayTextFormat = sDisplayTextFormat
	End Property

	Public Property Get ItemsCommandText()
		ItemsCommandText = sItemsCommandText
	End Property

	Public Property Get DisplayMode()
		DisplayMode = sDisplayMode
	End Property

	Public Property Get CssClass()
		CssClass = oTag.CssClass
	End Property
	
	'Input Properties
	Public Property Let ParentCell(input)
		Set oParentCell = input
	End Property

	Public Property Let DisplayTextFormat(input)
		sDisplayTextFormat = input
	End Property

	Public Property Let ItemsCommandText(input)
		sItemsCommandText = input
		FOR iItemsIndex=1 TO aOptions(0)
			aOptions(iItemsIndex).CommandText=sItemsCommandText
		NEXT
	End Property

	Public Property Let DisplayMode(input)
		sDisplayMode = input
		FOR iItemsIndex=1 TO aOptions(0)
			aOptions(iItemsIndex).DisplayMode=sDisplayMode
		NEXT
	End Property

	Public Property Let CssClass(input)
		oTag.CssClass = input
	End Property
	
	'Public methods
	Public Function GetItem(vElementName)
		IF NOT IsNaN(vElementName) THEN
			Set GetItem = aOptions(vElementName)
		ELSE
			Set GetItem = dOptionElements(vElementName)
		END IF
	End Function

	Public Sub Fill()
		oDataSource.DataBind()
		FillWithRecordSet(oDataSource.RecordSet)
	End Sub
	
	Public Sub FillWithRecordSet(ByRef rs)
		If rs.EOF and rs.BOF Then
			iItemsCount=-1
		Else
			rs.MoveFirst
			aOptionElements = rs.GetRows
			iItemsCount = Ubound(aOptionElements,2)
			FillWithArray(aOptionElements)
		End If
	End Sub
	
	Public Sub FillWithArray(ByRef elements)
		aOptionElements = elements
		iItemsCount = Ubound(aOptionElements,2)
		Build()
	End Sub
	
	Public Sub NewOption(ByRef oItem) '(ByVal sValue, ByRef oItem)
'		Set dOptionElements(sValue)=oItem
		aOptions(0)=aOptions(0)+1
		Set aOptions(aOptions(0))=oItem
		oTag.AddContent(oItem)
	End Sub
	
	Public Function NewItem(sOptValue, sOptThisText)
		Dim oItem
		Set oItem = New BulletedListItem
		IF sDisplayTextFormat<>"" THEN
			oItem.Text=applyTemplate(TRIM(sOptThisText), "^(.*)$", sDisplayTextFormat)
		ELSE
			oItem.Text=sOptThisText
		END IF
		oItem.CommandText=sItemsCommandText
		oItem.DisplayMode=sDisplayMode
		oItem.Properties.SetProperty "commandArguments", RTRIM(sOptThisText)
		IF sOptValue<>"" THEN oItem.Properties.SetProperty "value", RTRIM(sOptValue)
		Set NewItem=oItem
	End Function
	
	Public Sub Build()
		For iItemsIndex = 0 to iItemsCount
			ReDim aData(0)
			Dim i, oItem
'			response.write aOptionElements(0,iItemsIndex)&", "&aOptionElements(1,iItemsIndex)&", "&aOptionElements(2,iItemsIndex)
			FOR i=0 TO UBOUND(aOptionElements, 1)
				ReDim Preserve aData(i)
				aData(i)=aOptionElements(i,iItemsIndex)
			NEXT
			sThisValue=RTRIM(aOptionElements(0,iItemsIndex))
			sThisText=TRIM(aOptionElements(1,iItemsIndex))
			Set oItem = Me.NewItem(sThisValue, sThisText)
			oItem.Data=aData
			Me.NewOption oItem
'			Me.NewOption sThisValue, NewItem(sThisValue, sThisText)
		Next
		bBuilt=TRUE
	End Sub
	
	Public Function GetCode()
		IF NOT bBuilt THEN Build()
		GetCode=oTag.GetCode()
	End Function
	
	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub
End Class 
 %>