<% 
Function [$StringValue]()
	Set [$StringValue] = new StringValue
End Function
Class StringValue
	'Properties
	Private sValue, sFormat, iWidth
	
	Private Sub Class_Initialize()
		iWidth=4000
	End Sub
	Private Sub Class_Terminate()
	End Sub
	
	Public Default Property Get Text()
		DIM sText:	sText=sValue
'		Debugger Me, sText
		IF LEN(TRIM(sText))>iWidth THEN sText=LEFT(TRIM(sText), iWidth)&"..."
'		Debugger Me, sText
		Text = FormatValue(sText, sFormat, NULL)
	End Property
	Public Property Let Text(input)
		sValue = input
	End Property

	Public Property Get Value()
		Value = sValue
	End Property
	Public Property Let Value(input)
		sValue = input
	End Property

	Public Property Get Format()
		Format = sFormat
	End Property
	Public Property Let Format(input)
		sFormat = input
	End Property

	Public Property Get Width()
		Width = iWidth
	End Property
	Public Property Let Width(input)
		iWidth = input
	End Property
End Class
 %>