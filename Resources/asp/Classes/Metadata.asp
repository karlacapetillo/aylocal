<% 
Class Metadata
	Private sMetadataString, bInlineInsert
	
	Private Sub Class_Initialize()
	End Sub
	Private Sub Class_Terminate()
	End Sub

	'Interfaces
	Public Sub InitMetadata(ByRef oSource)
	End Sub

	'Properties
	Public default Property Get MetadataString()
		MetadataString = sMetadataString
	End Property
	Public Property Let MetadataString(input)
		sMetadataString = input
	End Property

	Public Property Get InlineInsert()
		InlineInsert = bInlineInsert
	End Property
	Public Property Let InlineInsert(input)
		bInlineInsert = input
	End Property


	'Methods
	Public Sub AddChildren(oItem)
	End sub
	
End Class
 %>