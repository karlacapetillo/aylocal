<% 
Class RadioButtonList
	'Properties
	Private bAllowInsert, sCatalog, bMultiple, sName, sValue, sText
	
	'Settings
'	Private 
	
	'Objects
	Private oRelatives, oSelectedOption, oDataValue, oValue, oHiddenField, oLabel
	
	'Member Variables
	Private bIsData
	Private sHTMLCode
	Private sSelectItem
	Private iCounter
	
	Private returnString
	
	'Objects 	
	Private oRadioButtonList, oReadonlyTag, oOption, sKeyDown
	
	Public Property Get Object()
		Set Object = oRadioButtonList
	End Property

	Public Property Get Tag()
		Set Tag = oRadioButtonList
	End Property

	Private Sub Class_Terminate()
		Set oHiddenField = nothing
		Set oLabel = nothing
		Set oSelectedOption = nothing
		Set oDataValue = nothing
		Set oValue = nothing
	End Sub
	
	Private Sub Class_Initialize()
		Set oHiddenField = nothing
		Set oLabel = nothing
		Set oDataValue = nothing
		Set oValue = nothing
		bIsData = False
		sName = ""
'		sSelectItem = ""
		Set oRadioButtonList = [$Tag]("select")
'		Set oRelatives = oRadioButtonList.Relatives
		oRadioButtonList.HasClosingTag=TRUE
		Me.AllowInsert=TRUE
		bMultiple=FALSE
		Set oSelectedOption = nothing
		'Me.Properties.ExtendedProperties.CheckValidName=FALSE
	End Sub

	'Interfaces
	Public Property Get Relatives()
		Set Relatives = oRelatives
	End Property
	
	Public Property Get Container()
		Set Container = oRelatives.Parent
	End Property
	Public Property Let Container(input)
		oRelatives.Parent = input
	End Property

	'Output objects
	Public Property Get Properties()
		Set Properties = oRadioButtonList.Properties
	End Property

	Public Property Get Methods()
		Set Methods = oRadioButtonList.Methods
	End Property
	
	Public Property Get Style()
		Set Style = oRadioButtonList.Style
	End Property
	
	'Output properties
	Public Property Get Visible()
		Visible = oRadioButtonList.Visible
	End Property
	Public Property Let Visible(input)
		oRadioButtonList.Visible = input
	End Property

	Public Property Get Multiple()
		Multiple = bMultiple
	End Property

	Public Property Get AllowInsert()
		AllowInsert = bAllowInsert
	End Property

	Public Property Get Catalog()
		Catalog = sCatalog
	End Property

	Public Property Get CssClass()
		CssClass = oRadioButtonList.CssClass
	End Property
	
	Public Sub GetValue()
		IF NOT oDataValue IS NOTHING THEN
			IF oValue IS NOTHING THEN
				Set oValue = new VariantValue
			END IF
			IF INSTR(oDataValue.Value, "@:@")>0 THEN
				sValue=oDataValue.Value
				IF INSTR(oDataValue.Value, "@/@")>0 THEN sValue=SPLIT(oDataValue.Value, "@/@")(Me.Properties.ExtendedProperties.NamedItem("CDDIndex"))	'CUANDO VIENE DESDE CASCADED DROP DROWN
				DIM aValues: aValues=SPLIT(sValue, "@:@")
				'Debugger Me, sValue&" Array: "&ubound(aValues)
				oValue.Value=aValues(0)
				oValue.Text=aValues(1)
			ELSE
				oValue.Value=oDataValue.Value
				oValue.Text=oDataValue.Text
			END IF
		END IF
	End Sub
	
	Public Sub CreateValueObject()
		IF oDataValue IS NOTHING THEN
			Set oDataValue = new VariantValue
		END IF
	End Sub
	
	Public Property Get Value()
		CreateValueObject()
		GetValue()
		SELECT CASE TypeName(oValue)
		CASE "DataValue"
			Set Value = oValue
		CASE ELSE
			Value = oValue.Value
		END SELECT
	End Property
	Public Property Let Value(input)
		IF IsNullOrEmpty(input) THEN Exit Property
		IF IsObject(Input) THEN
			Set oDataValue = input
		ELSE 
			CreateValueObject()
			oDataValue.Value = input
		END IF

		

'		sValue = TRIM(CSTR(input))
'		DIM bExistsItem: bExistsItem=oRadioButtonList.ExistsItem(sValue)
		
'		IF TRIM(sValue)="" THEN sValue="BLANK"
'		IF Me.OptionsCount>0 AND NOT(bExistsItem) AND sValue<>"BLANK" THEN
'			Me.NewOption sValue, sValue
'			RESPONSE.WRITE "<script language=""JavaScript"">alert('WARNING: Default value for RadioButtonList "&sName&" ("""&sValue&""") was not found. It was registered to avoid data loss')</script>"
'		END IF
'		IF sValue="BLANK" THEN 
'			oRadioButtonList.Properties.RemoveProperty "defaultValue"
'		ELSE
'			oRadioButtonList.Properties.SetProperty "defaultValue", sValue
'		END IF
'		IF Me.OptionsCount>0 AND bExistsItem THEN 
'			Me.SelectedOption=oRadioButtonList(sValue)
'			oRadioButtonList(sValue).Properties.SetProperty "selected", null
'			sText=oRadioButtonList(sValue).Text
'		ELSE
'			Me.SelectedOption=NOTHING
'		END IF
	End Property
	
	Public Property Get Text()
		GetValue()
		Text = oValue.Text
	End Property
	Public Property Let Text(input)
		oValue.Text = input
	End Property

	Public Property Get SelectItem()
		SelectItem = sSelectItem
	End Property
	
	'Properties interfaces
	Public Property Get IsReadonly()
		IsReadonly = oRadioButtonList.IsReadonly
	End Property
 	Public Property Let IsReadonly(input)
		oRadioButtonList.IsReadonly = input
	End Property

	'Input Properties
	Public Property Let Multiple(input)
		bMultiple = input
	End Property

	Public Property Let AllowInsert(input)
		bAllowInsert = input
		SetCommandAllow()
	End Property

	Public Property Let Catalog(input)
		sCatalog = input
		Me.Properties.SetProperty "catalogo", input
		SetCommandAllow()
	End Property

	Public Sub SetCommandAllow()
		IF bAllowInsert AND sCatalog<>"" THEN
			sKeyDown="modalOperations(this)"
		ELSE
			sKeyDown=""
		END IF
		oRadioButtonList.Methods.OnKeyDown=sKeyDown
	End Sub
	
	Public Property Let CssClass(input)
		oRadioButtonList.CssClass = input
	End Property
	
	Public Property Get Name()
		Name = sName
	End Property
	Public Property Let Name(input)
		sName = input
		oRadioButtonList.Properties.SetProperty "name", sName
	End Property
	
	Public Property Let Id(input)
		oRadioButtonList.Id=input
	End Property
	
	Public Property Let SelectItem(input)
		sSelectItem = input
	End Property
	
	'Public methods
	Public Sub FillCombo(ByRef rs)
		'Takes reference to  recordset
		'and puts data into a 2 dimensional
		'array
		If NOT(rs.EOF and rs.BOF) Then
			bIsData = False
		Else
			Dim aOptionElements
			bIsData = True
			aOptionElements = rs.GetRows
			FillComboWithArray(aOptionElements)
		End If
	End Sub
	
	Public Property Get OptionsCount()
		OptionsCount = oRadioButtonList.ItemsCount
	End Property

	Public Sub FillComboWithArray(ByRef elements)
		bIsData = True
		DIM iOptionIndex
		For iOptionIndex = 0 to Ubound(elements,2)
			DIM sThisValue: sThisValue=RTRIM(elements(0,iOptionIndex))
			DIM sThisText: sThisText=TRIM(elements(1,iOptionIndex))
			Me.NewOption sThisValue, sThisText
		Next
	End Sub
	
	Public Function NewOption(sOptValue, sOptThisText)
		Set oOption = New ListItem
		oOption.Text=sOptThisText
		oOption.Value=RTRIM(sOptValue)
		IF sOptValue=sValue THEN oOption.Selected=TRUE
		
		oRadioButtonList.AddNamedItem oOption, CSTR(sOptValue)
		Set NewOption=oOption
	End Function
	
'	Public Sub ResetProperties()
'		oRadioButtonList.Properties.RemoveProperty "defaultValue"
'	End Sub
	
	Public Property Get SelectedOption()
		Set SelectedOption = oSelectedOption
	End Property
	Public Property Let SelectedOption(input)
		IF NOT(oSelectedOption IS NOTHING) THEN oSelectedOption.Properties.RemoveProperty "selected"
		Set oSelectedOption=input
		IF NOT(oSelectedOption IS NOTHING) THEN oSelectedOption.Properties.SetProperty "selected", null
	End Property

	Public Sub RemoveItems()
		oRadioButtonList.ClearContent()
	End Sub

	Public Sub ClearSelections()
		oRadioButtonList.All.RemoveProperty "selected"
	End Sub
	
	Public Function GetCode()
		GetValue()
		'Debugger Me, Me.Properties.ExtendedProperties.NamedItem("ArrayIndex")
		IF Me.IsReadonly THEN
			IF oHiddenField IS NOTHING THEN 
				Set oHiddenField = new HiddenField
				oHiddenField.Id=Me.Name
				oHiddenField.Value=oValue
			END IF
			IF oLabel IS NOTHING THEN 
				Set oLabel = new Label
				oLabel.Text=oValue
			END IF
			GetCode=oHiddenField.GetCode()&" "&oLabel.GetCode()
		ELSE
			GetCode=oRadioButtonList.GetCode()
		END IF
	End Function
	
	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub
	
End Class 
 %>