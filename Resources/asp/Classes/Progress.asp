<% 
Class Progress
	'Properties
	Private sProgressBarControlId, sClassName, iTotalRecords, iParts, fProgressBarWidth', sOtherProperties
	
	'Settings
	
	'Objects
	Private oDataSource
	
	'Private Variables
		Private iSteps, iCurrentProgress
	
	'Accessor methods
	Private Sub Class_Terminate()
		Set oDataSource = nothing
	End Sub
	
	Private Sub Class_Initialize()
		sProgressBarControlId=application("ProgressBarControlId")
		Set oDataSource = nothing
		iTotalRecords=NULL
		iParts=10
		fProgressBarWidth=100
	End Sub
	
	'Output objects
	
	'Output Properties 
	Public Property Get DataSource()
		Set DataSource = oDataSource
	End Property
	Public Property Let DataSource(input)
		Set oDataSource = input
	End Property
	
	
	Public Property Get ProgressBarControlId()
		ProgressBarControlId = sProgressBarControlId
	End Property
	Public Property Let ProgressBarControlId(input)
		sProgressBarControlId = input
		IF sClassName<>"" THEN Me.ClassName=sClassName
	End Property
	
	
	Public Property Get Parts()
		Parts = iParts
	End Property
	Public Property Let Parts(input)
		iParts = input
	End Property
	
	
	Public Property Get TotalRecords()
		IF TypeName(oDataSource)="DataSource" THEN
			DIM iRecordCount: iRecordCount=oDataSource.TotalRecords
			TotalRecords = Try(iRecordCount>0, iRecordCount, 1)
		ELSE
			TotalRecords = 1'iTotalRecords
		END IF
	End Property
	Public Property Let TotalRecords(input)
		iTotalRecords = input
	End Property
	
	
	Public Property Get ClassName()
		ClassName = sClassName
	End Property
	Public Property Let ClassName(input)
		sClassName = input
		IF sProgressBarControlId<>"" THEN 
			response.write "<script language=""javascript"">try { with ("&sProgressBarControlId&") { className='"&sClassName&"'; } } catch(e) {}</script>"&vbcrlf
			response.Flush
		END IF
	End Property
	
	'Input Properties 
	
	Public Property Get PartSize()
		IF Me.TotalRecords>1 THEN
			PartSize=Round((Me.TotalRecords/iParts)+.4)
		ELSE
			PartSize=1
		END IF
	End Property

	Public Sub StepForward
		iSteps = iSteps + 1
		iCurrentProgress = (iSteps / iTotalRecords)
		if (iSteps mod Me.PartSize = 0 OR iSteps=iTotalRecords) then
			Me.WriteCode()
		end if
	End Sub
	
	Public Sub ClearProgress()
		iCurrentProgress=0
		Me.WriteCode()
	End Sub

	Public Sub SetComplete()
		iCurrentProgress=1
		Me.WriteCode()
	End Sub

	Public Property Get CurrentPosition()
		IF TypeName(oDataSource)="DataSource" THEN
			CurrentPosition = oDataSource.Index
		ELSE
			CurrentPosition = 0
		END IF
	End Property
	
	Public Property Get CurrentProgress()
		CurrentProgress = Me.CurrentPosition/Me.TotalRecords
	End Property

	Public Sub ShowProgress()
		IF sProgressBarControlId<>"" THEN Me.WriteCode()
	End Sub

	Public Sub WriteCode()
		IF sProgressBarControlId="" THEN 
			Err.Raise 1, "ASP 101", "ProgressBarControlId property must be set first."
			response.end
		END IF
		IF (Me.CurrentPosition mod Me.PartSize)=0 OR Me.CurrentProgress=Me.TotalRecords THEN
'			response.write Me.PartSize&":"&Me.CurrentPosition&" "
			response.write "<script language=""javascript"">try { with ("&sProgressBarControlId&") { style.width=Math.ceil("&Me.CurrentProgress&" * "&fProgressBarWidth&"); } } catch(e) {}</script>"&vbcrlf
			response.Flush
		END IF
	End Sub
End Class 
 %>