<% 
Class CascadedDropDownList
	'Properties
	Private sTextValue, sDataValue, bOptionChoose, bOptionAll, bTurnOn, sPromptText, sLoadingText, sParentControlId, sFilters, sParameters 
	'Settings
	Private sMode
	'Objects
	Private oRelatives
	
	'Member Variables
	Private aOptionElements
	Private sHTMLCode
	Private iRowIndex
	Private iRowCount
	Private sSelectItem
	Private iCounter
	
	Private returnString
	
	'Objects 	
	Private oOption, dOptions, cParameters
	
	'Private variables
	Private bBuilt, sThisValue
	
	Public Property Get Object()
		Set Object = Me
	End Property
	
	Private Sub Class_Initialize()
		Set oRelatives = [$Relatives](Me)
		Set oContent = oRelatives.Children
		oContent.ValidDataTypes=Array("AjaxDropDownList")
	End Sub
	
	'Interfaces
	Public Property Get Style()
		Set Style = oContent.Style
	End Property
	
	Public Property Get Control()
		Set Control = Me
	End Property
	
	Public Property Get Relatives()
		Set Relatives = oRelatives
	End Property
	
	Public Property Get Container()
		Set Container = oRelatives.Parent
	End Property
	Public Property Let Container(input)
		oRelatives.Parent = input
	End Property

'	Public Property Get Methods()
'		Set Methods = oDropDownList.Methods
'	End Property
	
'	Public Property Get AllowInsert()
'		AllowInsert=oDropDownList.AllowInsert
'	End Property
'	Public Property Let AllowInsert(input)
'		oDropDownList.AllowInsert = input
'	End Property

'	Public Property Get CssClass()
'		CssClass = oDropDownList.CssClass
'	End Property
'	Public Property Let CssClass(input)
'		oDropDownList.CssClass=input
'	End Property

'	Public Property Get Value()
'		Value = oDropDownList.Value
'	End Property
'	Public Property Let Value(input)
'		IF TRIM(input)<>"" THEN oDropDownList.Value = input
'	End Property
	
'	Public Property Get SelectItem()
'		SelectItem = oDropDownList.SelectItem
'	End Property
'	Public Property Let SelectItem(input)
'		oDropDownList.SelectItem = input
'	End Property
	
'	Public Property Let Id(input)
'		oDropDownList.Id = input
'	End Property
	
	'Output properties
	Public Property Get IsReadonly()
		IsReadonly = oDropDownList.IsReadonly
	End Property
	Public Property Let IsReadonly(input)
		oDropDownList.IsReadonly=input
	End Property

	Public Property Get Parameters()
		Parameters = sParameters
	End Property

	Public Property Get Filters()
		Filters = sFilters
	End Property

	Public Property Get PromptText()
		PromptText = sPromptText
	End Property

	Public Property Get LoadingText()
		LoadingText = sLoadingText
	End Property

	Public Property Get ParentControlId()
		ParentControlId = sParentControlId
	End Property

	Public Property Get TextValue()
		TextValue = sTextValue
	End Property

	Public Property Get DataValue()
		DataValue = sDataValue
	End Property

	Public Property Get OptionChoose()
		OptionChoose = bOptionChoose
	End Property

	Public Property Get OptionAll()
		OptionAll = bOptionAll
	End Property

	Public Property Get TurnOn()
		TurnOn = bTurnOn
	End Property
	
	'Input Properties
	Public Property Let Filters(input)
		sFilters = input
		oDropDownList.Properties.SetProperty "filtros", input
	End Property

	Public Property Let Parameters(input)
		sParameters = input
		oDropDownList.Properties.SetProperty "parameters", input
	End Property

	Public Property Let PromptText(input)
		sPromptText = input
	End Property

	Public Property Let LoadingText(input)
		sLoadingText = input
	End Property

	Public Property Let ParentControlId(input)
		sParentControlId = input
		IF input<>"" THEN 
			oDropDownList.Properties.SetProperty "dependencias", "["&input&"]"
		END IF
	End Property

	Public Property Let TextValue(input)
		sTextValue=input
		oDropDownList.Properties.SetProperty "textValue", RTRIM(input)
	End Property

	Public Property Let DataValue(input)
		sDataValue=input
		oDropDownList.Properties.SetProperty "dataValue", RTRIM(input)
	End Property

	Public Property Let OptionChoose(input)
		bOptionChoose=input
		oDropDownList.Properties.SetProperty "opt_choose", dMainDictionary("javascript_"&dMainDictionary(LCASE(CSTR(CBOOL(input)))))
	End Property

	Public Property Let OptionAll(input)
		bOptionAll = input
		oDropDownList.Properties.SetProperty "opt_all", dMainDictionary("javascript_"&dMainDictionary(LCASE(CSTR(CBOOL(input)))))
	End Property

	Public Property Let TurnOn(input)
		bTurnOn = input
		oDropDownList.Properties.SetProperty "turnOn", dMainDictionary("javascript_"&dMainDictionary(LCASE(CSTR(CBOOL(input)))))
	End Property

	'Public methods
	Public Sub ResetProperties()
		oDropDownList.ResetProperties()
	End Sub

	Public Sub ClearItems()
		oDropDownList.ClearItems()
	End Sub

	Public Sub ClearSelections()
		oDropDownList.ClearSelections()
	End Sub
	
	Public Sub FillCombo(ByRef rs)
		oDropDownList.FillCombo(rs)
	End Sub
	
	Public Sub FillComboWithArray(ByRef elements)
		oDropDownList.FillComboWithArray(elements)
	End Sub
	
	Public Function NewOption(ByVal sValue, ByVal sText)
		Set NewOption = oDropDownList.NewOption(sValue, sText)
	End Function
	
'	Public Sub Build()
'		oDropDownList.Build()
'	End Sub
	
	Public Function GetCode()
		Me.Parameters=Me.ParametersCollection.GetCode()
		GetCode=oDropDownList.GetCode()
	End Function
	
	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub
	
	Public Sub Class_Terminate()
	End Sub
End Class 
 %>