<% 
Class ImageButton
	Private sCommandArguments, sCommandName, bDisabled, sCommandText
	
	'Settings
	Private bIsReadonly
	
	'Objects 
	Private oImage, oContainer, oRelatives
	
	Private Sub Class_Initialize()
		Set oRelatives = [$Relatives](Me)
		Set oImage = new Image
		Me.Style.Cursor="Hand"
		Me.Disabled=FALSE
		Me.CssClass="Button"
		Me.Methods.OnContextMenu="return false;"
	End Sub

	'Output objects
	Public Property Get Style()
		Set Style = oImage.Style
	End Property
	
	Public Property Get Methods()
		Set Methods = oImage.Methods
	End Property
	
	Public Property Get Properties()
		Set Properties = oImage.Properties
	End Property
	
	Public Property Get Object()
		Set Object = oImage.Object
	End Property
	
	'Output Interfaces 
	Public Property Get Relatives()
		Set Relatives = oRelatives
	End Property
	
	Public Property Get Container()
		Set Container = oRelatives.Parent
	End Property
	Public Property Let Container(input)
		oRelatives.Parent = input
	End Property

	Public Property Get ParentCell()
		ParentCell = oImage.ParentCell
	End Property

	Public Property Get Render()
		Render = oImage.Render
	End Property
	Public Property Let Render(input)
		oImage.Render = input
	End Property

	Public Property Get Visible()
		Visible = oImage.Visible
	End Property
	Public Property Let Visible(input)
		oImage.Visible = input
	End Property

	Public Property Get HasBorder()
		HasBorder = oImage.HasBorder
	End Property

	Public Property Get Width()
		Width = oImage.Width
	End Property

	Public Property Get Height()
		Height = oImage.Height
	End Property

	Public Property Get AlternateText()
		AlternateText = oImage.AlternateText
	End Property
	
	Public Property Get Id()
		Id = oImage.Id
	End Property
	
	Public Property Get ImageURL()
		ImageURL = oImage.ImageURL
	End Property
	
	Public Property Get CssClass()
		CssClass = oImage.CssClass
	End Property
	
	'Input Interfaces

	Public Property Let HasBorder(input)
		oImage.HasBorder = input
	End Property

	Public Property Let Width(input)
		oImage.Width = input
	End Property

	Public Property Let Height(input)
		oImage.Height = input
	End Property

	Public Property Let AlternateText(input)
		oImage.AlternateText = input
	End Property
	
	Public Property Let Id(input)
		oImage.Id = input
	End Property
	
	Public Property Let ImageURL(input)
		oImage.ImageURL = input
	End Property

	Public Property Let CssClass(input)
		oImage.CssClass = input
	End Property
	
	'Output Properties 
	Public Property Get CommandText()
		CommandText = sCommandText
	End Property

	Public Property Get Disabled()
		Disabled = bDisabled
	End Property
	
	'Input Properties 
	Public Property Let CommandText(input)
		sCommandText = input
		oImage.Methods.OnClick = sCommandText
	End Property

	Public Property Let Disabled(input)
		bDisabled = input
	End Property
	
	Public Property Get IsReadonly()
		IsReadonly = bIsReadonly
	End Property
	Public Property Let IsReadonly(input)
		bIsReadonly = input
	End Property

	'Public methods
	Public Function GetCode()
		IF NOT(TRIM(oImage.ImageURL)<>"") THEN
			Err.Raise 1, "ASP 101", "ImageURL"&sErrPropertyNotSet
		END IF
'		Me.Disabled=(bDisabled OR sCommandText="")
		oImage.Properties.SetProperty "enabled", dMainDictionary("javascript_"&dMainDictionary(LCASE(CSTR(CBOOL(NOT(Me.Disabled))))))
		GetCode=oImage.GetCode()
	End Function

	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub
End Class 
 %>