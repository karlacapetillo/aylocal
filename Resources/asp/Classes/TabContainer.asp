<% 
Class TabContainer
	'Properties
	Private sId, sCssClass, fHeight, fWidth
	
	'Settings
	Private sTabStripPlacement, bRender
	
	'Objects 
	Private oContentPanel, dTabs, oTabStrip, oTabList, oContainer, oRelatives
	
	'Private variables
	
	Private Sub Class_Initialize()
		Set oRelatives = [$Relatives](Me)
		Me.TabStripPlacement="top"
		Set dTabs = server.CreateObject("Scripting.Dictionary")
		Set oTabStrip = new Div
		oTabStrip.Id="tab_strip"
		oTabStrip.Style.Position="absolute"
		Set oTabList = new BulletedList
		oTabStrip.AddContent(oTabList)
		oTabList.Id="tabs"
		oTabList.DisplayMode="hyperlink"
		oTabList.Style.TextDecoration="none"
		oTabList.DisplayTextFormat="""<a href=""""#""""><span>$&</span></a>"""
		oTabList.Methods.OnContextMenu="return false;"
		Set oContentPanel = new Div
		oContentPanel.id="tab_contentpanel"
		oContentPanel.CssClass="PanelContainer"
		oContentPanel.Object.Properties.SetProperty "offset_top", "parseInt(document.all('tab_strip').offsetTop)+parseInt(document.all('tab_strip').clientHeight)"
		oContentPanel.Object.Properties.SetProperty "enableFloating", "false"
'		oContentPanel.Style.PosTop="15"
	End Sub

	Private Sub Class_Terminate()
		Set dTabs = nothing
		Set oContentPanel = nothing
	End Sub
	
	'Output objects
	Public Property Get Properties()
		Set Properties = oContentPanel.Properties
	End Property

	Public Property Get Tabs()
		Set Tabs = dTabs
	End Property

	Public Property Get Style()
		Set Style = oContentPanel.Style
	End Property
	
	'Output Interfaces
	Public Property Get Relatives()
		Set Relatives = oRelatives
	End Property
	
	Public Property Get Container()
		Set Container = oRelatives.Parent
	End Property
	Public Property Let Container(input)
		oRelatives.Parent = input
	End Property

	Public Property Get ParentCell()
		Set ParentCell = oContentPanel.ParentCell
	End Property
	
	'Input Interfaces
	Public Property Let ParentCell(input)
		Set oContentPanel.ParentCell = input
	End Property

	'Input Settings
	Public Property Get Render()
		Render = bRender
	End Property

	'Output Settings
	Public Property Let Render(input)
		bRender = input
	End Property

	'Output Properties 
	Public Property Get Width()
		Width = sWidth
	End Property

	Public Property Get Height()
		Height = sHeight
	End Property

	Public Property Get TabStripPlacement()
		TabStripPlacement = sTabStripPlacement
	End Property
	
	Public Property Get Id()
		Id = sId
	End Property
	
	Public Property Get CssClass()
		CssClass = sCssClass
	End Property
	
	'Input Properties 
	Public Property Let Width(input)
		fWidth = input
		oContentPanel.Properties.SetProperty "width", input
	End Property

	Public Property Let Height(input)
		fHeight = input
		oContentPanel.Properties.SetProperty "height", input
	End Property

	Public Property Let TabStripPlacement(input)
		SELECT CASE UCASE(input)
		CASE UCASE("Top"), UCASE("Bottom")
			sTabStripPlacement = input
		CASE ELSE
			Err.Raise 1, "ASP 101", input&" is not a TabStripPlacement parameter"
			response.end
		END SELECT
	End Property
	
	Public Property Let Id(input)
		sId = input
		oContentPanel.Id = sId
	End Property
	
	Public Property Let CssClass(input)
		sCssClass = input
		oContentPanel.CssClass = sCssClass
	End Property
	
	'Public methods
	Public Sub AddTab(ByRef oElement)
		IF UCASE(TypeName(oElement))<>UCASE("TabPanel") THEN 
			Err.Raise 1, "ASP 101", "Only TabPanel objects are allowed."
			response.end
		END IF
		IF TRIM(oElement.Id)="" THEN 
			Err.Raise 1, "ASP 101", "TabPanel Id Property must be set before adding it to collection"
			response.end
		END IF
		IF dTabs.exists(oElement.Id) THEN
			Err.Raise 1, "ASP 101", "A tab panel with id """&oElement.Id&""" already exists"
			response.end
		END IF

		DIM oTab, oDiv
		Set dTabs(oElement.Id) = oElement
		Set oTab = oTabList.NewItem(oElement.Id, oElement.HeaderText)
		oTab.CssClass="tab"&Try(NOT(IsNullOrEmpty(oElement.CssClass)), " "&oElement.CssClass, "")
		oElement.CssClass="TabPanel"
		oTab.CommandText="null"
		oTabList.NewOption oTab
		oContentPanel.AddContent(oElement)
	End Sub
	
	Public Function SetActiveTab(sTab)
		DIM oItem, oDictionary
		oDictionary=dTabs.Items
		FOR EACH oItem IN oDictionary
			oItem.Visible=FALSE
		NEXT
		IF dTabs.Exists(sTab) THEN
			dTabs(sTab).Visible=TRUE
		ELSE
			oDictionary(sTab).Visible=TRUE
		END IF
	End Function
	
	Public Function GetCode()
		DIM sCode, oTabPanel
		sCode=""
		IF UCASE(sTabStripPlacement)=UCASE("Top") THEN sCode=sCode & oTabStrip.GetCode()
		sCode=sCode & oContentPanel.GetCode
'		FOR EACH oTabPanel IN dTabs.items
'			sCode=sCode & oTabPanel.GetCode()
'		NEXT
		IF UCASE(sTabStripPlacement)=UCASE("Bottom") THEN sCode=sCode & oTabStrip.GetCode()
		sCode=sCode & "<script language=""JavaScript"">document.all('"&oContentPanel.Id&"').style.top=document.all('"&oTabStrip.Id&"').clientHeight</script>"
		GetCode=sCode
	End Function

	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub
End Class 
 %>