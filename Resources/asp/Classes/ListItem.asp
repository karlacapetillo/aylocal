<% 
Class ListItem
	'Properties
	Private bSelected, bEnabled, sText, sValue, sId', sOtherProperties
	
	'Settings
	Private bRender, bIsReadonly
	
	'Objects
	Private oOption, oContainer, oRelatives
	'Private Variables
	
	Private Sub Class_Initialize()
		Set oRelatives = [$Relatives](Me)
		Set oOption = New Tag
		oOption.TagName="option"
		oOption.HasClosingTag=TRUE
		Me.Render=TRUE
	End Sub
	
	'Output objects
	Public Property Get Style()
		Set Style = oOption.Style
	End Property
	
	Public Property Get Methods()
		Set Methods = oOption.Methods
	End Property
	
	Public Property Get Attributes()
		Set Attributes = oOption.Properties
	End Property
	
	Public Property Get Properties()
		Set Properties = oOption.Properties
	End Property
	
	Public Property Get Relatives()
		Set Relatives = oRelatives
	End Property
	
	Public Property Get Container()
		Set Container = oRelatives.Parent
	End Property
	Public Property Let Container(input)
		oRelatives.Parent = input
	End Property

	Public Property Get ParentCell()
		Set ParentCell = oOption.ParentCell
	End Property
	
'	Public Property Let ParentCell(input)
'		Set oParentCell = input
'	End Property

	'Output Properties 
	Public Property Get Visible()
		Visible = oOption.Visible
	End Property
	Public Property Let Visible(input)
		oOption.Visible = input
	End Property

	Public Property Get Render()
		Render = bRender
	End Property
	Public Property Let Render(input)
		bRender = input
	End Property

	Public Property Get Text()
		Text = sText
	End Property
	Public Property Let Text(input)
		sText = input
		oOption.ClearContent()
		oOption.AddContent(sText)
	End Property

	Public Property Get Value()
		Value = sValue
	End Property
	Public Property Let Value(input)
		sValue = input
		IF sValue<>"" THEN oOption.Properties.SetProperty "value", input
	End Property

	Public Property Get Selected()
		Selected = bSelected
	End Property
	Public Property Let Selected(input)
		bSelected = input
		IF CBOOL(bSelected) THEN
			oOption.Properties.SetProperty "selected", NULL
		ELSE
			oOption.Properties.RemoveProperty("selected")
		END IF
	End Property

	Public Property Get Id()
		Id = sId
	End Property
	Public Property Let Id(input)
		sId = input
		oOption.Properties.SetProperty "id", input
	End Property

	Public Property Get CssClass()
		CssClass = oOption.Properties.GetProperty("class")
	End Property
	Public Property Let CssClass(input)
		oOption.Properties.SetProperty "class", input
	End Property
	
	Public Property Get IsReadonly()
		IsReadonly = bIsReadonly
	End Property
 	Public Property Let IsReadonly(input)
		bIsReadonly = input
	End Property

	'Public methods
	Public Function GetCode()
		IF NOT Me.Render THEN 
			GetCode=""
			EXIT Function
		END IF
		GetCode=oOption.GetCode()
	End Function

	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub
End Class 
 %>