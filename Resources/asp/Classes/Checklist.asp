<% 
Class CheckList
	'Properties
	Private iItemsCount,aOptionElements,sName
	
'	Private sName
	
	'Settings
	
	'Objects 	
	Private oCheckBoxCollection,oDataSource, rsRecordSet, oParentCell, oDiv, oContainer, oRelatives
	
	'Private variables
	Private bBuilt
	
	Public Property Get Object()
		Set Object = oCheckBoxCollection
	End Property

	Private Sub Class_Initialize()
		Set oRelatives = [$Relatives](Me)
		Set oDiv = new Div
		Set oDataSource = new Recordset
		Set rsRecordSet = nothing
		Set oCheckBoxCollection = new Collection
		oDiv.AddContent(oCheckBoxCollection)
		oCheckBoxCollection.separator = "<br />"
	End Sub
	
	'Output interfaces
	Public Property Get ItemsCount()
		ItemsCount = iItemsCount
	End Property
	
	Public Property Get Id()
		Id = oDiv.Id
	End Property
	Public Property Let Id(input)
		oDiv.Id = input
	End Property
	
	Public Property Get Width()
		Width = oDiv.Width
	End Property
	Public Property Let Width(input)
		oDiv.Width = input
	End Property
	
	Public Property Get Height()
		Height = oDiv.Height
	End Property
	Public Property Let Height(input)
		oDiv.Height = input
	End Property
	
	Public Property Get Name()
		Name = sName
	End Property
	Public Property Let Name(input)
		oCheckBoxCollection.ApplyProperty "name" , input
		sName = input
	End Property
	
	Public Property Get DataSource()
		Set DataSource=oDataSource
	End Property
	
	Public Property Get Style()
		Set Style = oDiv.Style
	End Property
	
	Public Property Get Items()
		Items=oCheckboxCollection.getItems()
	End Property

	Public Property Get Relatives()
		Set Relatives = oRelatives
	End Property
	
	Public Property Get Container()
		Set Container = oRelatives.Parent
	End Property
	Public Property Let Container(input)
		oRelatives.Parent = input
	End Property

	Public Property Get ParentCell()
		Set ParentCell = oParentCell
	End Property
	Public Property Let ParentCell(input)
		Set oParentCell = input
	End Property
	
	Public Property Get CssClass()
		CssClass = oDiv.CssClass
	End Property
	Public Property Let CssClass(input)
		oDiv.CssClass = input
	End Property

	'Public methods
	Public Property Get IsReadonly()
		IsReadonly = oDiv.IsReadonly
	End Property
 	Public Property Let IsReadonly(input)
		oDiv.IsReadonly = input
	End Property

	Public Sub Fill()
		oDataSource.DataBind()
		FillWithRecordSet(oDataSource.RecordSet)
	End Sub
	
	Public Sub FillWithRecordSet(ByRef rs)
		If rs.EOF and rs.BOF Then
			iItemsCount=-1
		Else
			rs.MoveFirst
			aOptionElements = rs.GetRows
			iItemsCount = Ubound(aOptionElements,2)
			FillWithArray(aOptionElements)
		End If
	End Sub
	
	Public Sub FillWithArray(ByRef elements)
		aOptionElements = elements
		iItemsCount = Ubound(aOptionElements,2)
		Build()
	End Sub
	
	Public Sub Build()
		Dim iItemsIndex,sThisValue,sThisText
		For iItemsIndex = 0 to iItemsCount
			Dim oItem
			sThisValue=RTRIM(aOptionElements(0,iItemsIndex))
			sThisText=TRIM(aOptionElements(1,iItemsIndex))
			Set oItem = New Checkbox
			oItem.Value=sThisValue
			oItem.Text=sThisText
			oCheckBoxCollection.AddContent(oItem)
		Next
		bBuilt=TRUE
	End Sub
	
	Public Sub AddContent(oItem)
		oCheckBoxCollection.AddContent(oItem)
	End Sub
	
	Public Function GetCode()
		IF NOT bBuilt THEN Build()
		GetCode=oDiv.GetCode()
	End Function
	
	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub
End Class 
 %>