<% 
Class TabPanel
	'Properties
	Private sId, sCssClass, fHeight, fWidth, sHeaderText, sContentURL
	
	'Settings
	Private bRender, bVisible
	
	'Objects 
	Private oDiv, oRelatives
	
	'Private variables
	
	Private Sub Class_Initialize()
		Set oRelatives = [$Relatives](Me)
		Set oDiv = new Div
		Me.Visible=FALSE
'		oDiv.Methods.OnPropertyChange="loadTabContent(this)"
	End Sub

	Private Sub Class_Terminate()
		Set oDiv = nothing
	End Sub

	'Interfaces
	Public Property Get Relatives()
		Set Relatives = oRelatives
	End Property
	
	Public Property Get Container()
		Set Container = oRelatives.Parent
	End Property
	Public Property Let Container(input)
		oRelatives.Parent = input
	End Property

	'Output objects
	Public Property Get Methods()
		Set Methods = oDiv.Methods
	End Property
	
	Public Property Get Style()
		Set Style = oDiv.Style
	End Property
	
	'Output Interfaces
	Public Property Get ParentContainer()
		Set ParentContainer = oContainer
	End Property
	
	'Input Interfaces
	Public Property Let ParentContainer(input)
		Set oContainer = input
	End Property

	'Input Settings
	Public Property Get Render()
		Render = bRender
	End Property

	Public Property Get Visible()
		Visible = bVisible
	End Property

	'Output Settings
	Public Property Let Render(input)
		bRender = input
	End Property

	Public Property Let Visible(input)
		bVisible = input
		oDiv.Visible=input
	End Property

	'Output Properties 
	Public Property Get ContentURL()
		ContentURL = sContentURL
	End Property

	Public Property Get HeaderText()
		HeaderText = sHeaderText
	End Property

	Public Property Get Width()
		Width = sWidth
	End Property

	Public Property Get Height()
		Height = sHeight
	End Property

	Public Property Get Id()
		Id = sId
	End Property
	
	Public Property Get CssClass()
		CssClass = sCssClass
	End Property
	
	'Input Properties 
	Public Property Let ContentURL(input)
		sContentURL = input
		oDiv.Properties.SetProperty "ContentURL", input
	End Property

	Public Property Let HeaderText(input)
		sHeaderText = input
	End Property

	Public Property Let Width(input)
		fWidth = input
		oDiv.Properties.SetProperty "width", input
	End Property

	Public Property Let Height(input)
		fHeight = input
		oDiv.Properties.SetProperty "height", input
	End Property

	Public Property Let Id(input)
		sId = input
		oDiv.Id = sId
	End Property
	
	Public Property Let CssClass(input)
		sCssClass = input
		oDiv.CssClass = sCssClass
	End Property
	
	Public Property Get IsReadonly()
		IsReadonly = oDiv.IsReadonly
	End Property
 	Public Property Let IsReadonly(input)
		oDiv.IsReadonly = input
	End Property

	'Public methods
	Public Sub AddContent(ByRef oElement)
		oDiv.AddContent(oElement)
	End Sub

	Public Function GetCode()
		GetCode=oDiv.GetCode()
	End Function

	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub
End Class 
 %>