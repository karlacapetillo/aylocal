<% 
Class Image
	Private sAlternateText, sId, sCssClass, sImageURL, fHeight, fWidth, bHasBorder
	
	'Settings
	
	'Objects 
	Private oImage, oContainer, oRelatives
	
	Private Sub Class_Initialize()
		Set oRelatives = [$Relatives](Me)
		Set oImage = new Tag
		oImage.TagName="img"
		oImage.HasClosingTag=FALSE
	End Sub

	'Output objects
	Public Property Get Style()
		Set Style = oImage.Style
	End Property
	
	Public Property Get Object()
		Set Object = oImage
	End Property
	
	'Output Interfaces
	Public Property Get Relatives()
		Set Relatives = oRelatives
	End Property
	
	Public Property Get Container()
		Set Container = oRelatives.Parent
	End Property
	Public Property Let Container(input)
		oRelatives.Parent = input
	End Property

	Public Property Get Methods()
		Set Methods = oImage.Methods
	End Property
	
	Public Property Get Properties()
		Set Properties = oImage.Properties
	End Property
	
	'Output Properties 
	Public Property Get Render()
		Render = oImage.Render
	End Property
	Public Property Let Render(input)
		oImage.Render = input
	End Property

	Public Property Get Visible()
		Visible = oImage.Visible
	End Property
	Public Property Let Visible(input)
		oImage.Visible = input
	End Property


	Public Property Get HasBorder()
		HasBorder = sHasBorder
	End Property

	Public Property Get Width()
		Width = sWidth
	End Property

	Public Property Get Height()
		Height = sHeight
	End Property

	Public Property Get AlternateText()
		AlternateText = sAlternateText
	End Property
	
	Public Property Get Id()
		Id = sId
		oImage.Id = sId
	End Property
	
	Public Property Get ImageURL()
		ImageURL = sImageURL
	End Property
	
	Public Property Get CssClass()
		CssClass = sCssClass
	End Property
	
	'Input Properties 
	Public Property Let HasBorder(input)
		bHasBorder = input
		oImage.Properties.SetProperty "border", "1"
	End Property

	Public Property Let Width(input)
		fWidth = input
		oImage.Properties.SetProperty "width", fWidth
	End Property

	Public Property Let Height(input)
		fHeight = input
		oImage.Properties.SetProperty "height", fHeight
	End Property

	Public Property Let AlternateText(input)
		sAlternateText = input
		oImage.Properties.SetProperty "alt", input
	End Property
	
	Public Property Let Id(input)
		sId = input
		oImage.Id=input
	End Property
	
	Public Property Let ImageURL(input)
		sImageURL = input
		oImage.Properties.SetProperty "src", input
	End Property

	Public Property Let CssClass(input)
		sCssClass = input
		oImage.CssClass = sCssClass
	End Property
	
	'Public methods
	Public Function GetCode()
		IF NOT(TRIM(sImageURL)<>"") THEN
			Err.Raise 1, "ASP 101", "ImageURL"&sErrPropertyNotSet
		END IF
		GetCode=oImage.GetCode()
	End Function

	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub
End Class 
 %>