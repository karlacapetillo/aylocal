<% 
Class Expander
	'Properties
	Private sId
	
	'Settings
	
	'Objects
	Private oImgExpand, oTargetObject, cObjects, oContainer, oRelatives

	
	'Private Variables
	
	Public Property Get Visible()
		Visible = oImgExpand.Visible
	End Property
	Public Property Let Visible(input)
		oImgExpand.Visible = input
	End Property

	Public Property Get Id()
		Id = oImgExpand.Id
	End Property
	Public Property Let Id(input)
		oImgExpand.Id = input
	End Property

	Public Property Get Style()
		Set Style = oImgExpand.Style
	End Property
	
	Private Sub Class_Initialize()
		Set oRelatives = [$Relatives](Me)
		Set cObjects = new Collection
		Set oImgExpand=new ImageButton
		oImgExpand.CssClass=oImgExpand.CssClass&" Expander"
		oImgExpand.Object.Properties.SetProperty "tabIndex", "0"
		oImgExpand.ImageURL=sImageLibraryPath & "expand.jpg"
		oImgExpand.Width=15
		oImgExpand.Height=15
		oImgExpand.Methods.OnKeyDown=TypeName(Me)&"_onkeydown()"
		cObjects.AddContent(oImgExpand)
	End Sub
	
	'Output objects
	Public Property Get Object()
		Set Object = oImgExpand
	End Property

	Public Property Get TargetObject()
		Set TargetObject = oTargetObject
	End Property
	Public Property Let TargetObject(input)
		Set oTargetObject = input
		oImgExpand.Methods.OnClick=TypeName(Me)&"_onclick()"
		oImgExpand.Methods.OnContextMenu=TypeName(Me)&"_oncontextmenu(); return false;"
		cObjects.AddContent(oTargetObject)
	End Property

	Public Property Get Relatives()
		Set Relatives = oRelatives
	End Property
	
	Public Property Get Container()
		Set Container = oRelatives.Parent
	End Property
	Public Property Let Container(input)
		oRelatives.Parent = input
	End Property

	Public Property Get ParentCell()
		Set ParentCell = cObjects.ParentCell
	End Property
	Public Property Let ParentCell(input)
		cObjects.ParentCell = input
	End Property
	
	Public Property Get IsReadonly()
		IsReadonly = cObjects.IsReadonly
	End Property
	Public Property Let IsReadonly(input)
		cObjects.IsReadonly = input
	End Property

	'Output Properties 
	Public Function GetCode()
		GetCode=cObjects.GetCode()
	End Function

	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub
End Class 
 %>