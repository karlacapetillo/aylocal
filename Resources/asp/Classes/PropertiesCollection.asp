<% 
Class PropertiesCollection
	'Properties
	Private sPropertiesString, sPropertiesType
	
	'Settings
	
	'Objects
	Private dProperties, oExtendedProperties
	
	'Private Variables
	
	Private Sub Class_Terminate()
		Set dProperties = nothing
		Set oExtendedProperties = nothing
	End Sub
	Private Sub Class_Initialize()
		Set dProperties=server.CreateObject("Scripting.Dictionary")
		Set oExtendedProperties = nothing
		Me.PropertiesType="HTML"
	End Sub
	
	Public Property Get ExtendedProperties()
		IF oExtendedProperties IS NOTHING THEN Set oExtendedProperties = [$Dictionary](Empty)
		Set ExtendedProperties = oExtendedProperties
	End Property

	'Output Properties
	Public Property Get PropertiesType()
		PropertiesType = sPropertiesType
	End Property

	Public Property Get PropertiesString()
		PropertiesString = sPropertiesString
	End Property
	
	Public Default Property Get All()
'		response.write "si entra<br>"
'		IF NOT dProperties.Exists(sPropertyName) THEN
'			Set dProperties.Exists(sPropertyName)=""
'		END IF
		Set All=dProperties
	End Property

	'Input Properties
	Public Property Let PropertiesType(input)
		SELECT CASE UCASE(input)
		CASE UCASE("HTML"), UCASE("javascript"), UCASE("SQLParameters")
			sPropertiesType = input
		CASE ELSE 
			Err.Raise 1, "ASP 101", input & " is not supported."
			response.end
		END SELECT
	End Property

	Public Function [Property](ByVal sKey)
		IF dProperties.exists(sKey) THEN
			[Property]=dProperties(sKey)
		ELSE
			[Property]=""
		END IF
	End Function

	Public Function GetProperty(ByVal sKey)
		IF dProperties.exists(sKey) THEN
			GetProperty=dProperties(sKey)
		ELSE
			GetProperty=""
		END IF
	End Function

	Public Sub SetProperty(ByVal sKey, ByVal input)
		IF input="" THEN
			Me.RemoveProperty(sKey)
		ELSE
			dProperties(sKey)=input
		END IF
	End Sub

	Public Sub RemoveProperty(ByVal sKey)
		IF dProperties.Exists(sKey) THEN dProperties.Remove(sKey)
	End Sub

	Public Function GetProperties()
		DIM oProperties:	Set oProperties = new XString
		DIM sPropertiesReturnString, oKeys, oItems, iKeyIndex
		sPropertiesReturnString=""
		oKeys=dProperties.Keys
		oItems=dProperties.Items
		DIM oItem
		iKeyIndex=0
		FOR EACH oItem IN oItems
			IF ISNULL(oItem) THEN
				oProperties.Append " "&oKeys(iKeyIndex)
			ELSE
				SELECT CASE UCASE(sPropertiesType)
				CASE UCASE("HTML")
'			  		sPropertiesReturnString=sPropertiesReturnString&" "&oKeys(iKeyIndex)&"="""& oItem &""""
					oProperties.Append 	" "&oKeys(iKeyIndex)&"="""& oItem &""""
				CASE UCASE("javascript")
''					IF IsNaN(oItem) THEN
''				  		sPropertiesReturnString=sPropertiesReturnString&" "&oKeys(iKeyIndex)&":'"& oItem &"';"
''					ELSE
'			  		sPropertiesReturnString=sPropertiesReturnString&" "&oKeys(iKeyIndex)&":"& oItem &";"
					oProperties.Append	" "&oKeys(iKeyIndex)&":"& oItem &";"
''					END IF
				CASE UCASE("SQLParameters")
'					IF NOT TRIM(sPropertiesReturnString)="" THEN sPropertiesReturnString=sPropertiesReturnString&", "
					IF NOT TRIM(sPropertiesReturnString)="" THEN oProperties.Append(", ")
					IF IsNaN(oItem) THEN
'						oProperties.Append(" ").Append(oKeys(iKeyIndex)).Append(":").Append(oItem).Append(";")
						oProperties.Append	" @"&oKeys(iKeyIndex)&"='"& oItem &"'"
					ELSE
'				  		sPropertiesReturnString=sPropertiesReturnString&" @"&oKeys(iKeyIndex)&"="& oItem &""
						oProperties.Append	" @"&oKeys(iKeyIndex)&"="& oItem &""
					END IF
				END SELECT
			END IF
			iKeyIndex=iKeyIndex+1
		NEXT
		sPropertiesReturnString=oProperties.Text
		GetProperties = sPropertiesReturnString
	End Function
	
	Public Function GetCode()
		GetCode=Me.GetProperties()
	End Function

	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub
End Class 
 %>