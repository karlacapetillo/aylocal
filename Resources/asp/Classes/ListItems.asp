<% 
Class ListItems
	'Properties
	'Private sCssClass, sSeparator', sOtherProperties
	
	'Settings
	'Private bRender, 
	
	'Objects
	Private oObject, oStyle, oParentCell, oProperties, oMethods
	
	'Private Variables
	Private iElementIndex, aElements(300), sReturnString, sKey, i
	
	Public Property Get Object()
		Set Object = Me
	End Property

	Private Sub Class_Initialize()
		Set oStyle = new Styles
		Me.Render=TRUE
		Set oProperties=new PropertiesCollection
	End Sub
	
	'Output objects
	Public Property Get Style()
		Set Style = oStyle
	End Property

	Public Property Get Properties()
		Set Properties = oProperties
	End Property

	'Output Properties 
'	Public Property Get ClassType()
'		ClassType = sClassType
'	End Property
	
	Public Property Get IsReadonly()
		IsReadonly = sIsReadonly
	End Property
	Public Property Let IsReadonly(input)
		sIsReadonly = input
	End Property

	Public Property Get Id()
		Err.Raise 1, "ASP 101", "Property not available"
	End Property
	
	Public Property Get CssClass()
		Err.Raise 1, "ASP 101", "Property not available"
	End Property
	
	Public Property Get Render()
		Render = bRender
	End Property
	
	'Input Properties 
	Public Property Let Render(input)
		bRender = input
		Me.ApplyProperty "Render", input
	End Property
	
	Public Property Let Visible(input)
		bVisible = input
		Me.ApplyProperty "Visible", input
	End Property
	
	Public Property Let ParentCell(input)
		Set oParentCell = input
'		Me.ApplyProperty "ParentCell", input
	End Property

'	Public Property Let ClassType(input)
'		sClassType = input
'		EVAL("Set oObject = new "&input)
'	End Property
	
	Public Property Let Id(input)
		sId = input
		Me.ApplyProperty "id", input
	End Property
	
	Public Property Let CssClass(input)
		sCssClass = input
		Me.ApplyProperty "CssClass", input
	End Property
	
	Public Property Get Separator()
		Separator = sSeparator
	End Property
	Public Property Let Separator(input)
		sSeparator = input
	End Property

	'Public methods
	Public Sub SetStyle(ByRef oNewStyle)
		Set oStyle = oNewStyle
		Me.ApplyProperty "SetStyle", oNewStyle
	End Sub

	Public Function GetItems()
		DIM i
		REDIM PRESERVE aItems(CINT(aElements(0))-1)
		FOR i=1 TO aElements(0)
			Set aItems(i-1) = aElements(i)
		NEXT
		GetItems=aItems
	End Function
	
	Public Sub AddContent(ByRef oElement)
		aElements(0)=aElements(0)+1
		Set aElements(aElements(0))=oElement
		oElement.IsReadonly=Me.IsReadonly
	End Sub

	Public Sub ClearContent()
		aElements(0)=0
	End Sub

	Public Sub ApplyProperty(sPropertyName, ByRef vValue)
		DIM i
		FOR i=1 TO aElements(0)
			IF isObject(vValue) THEN
				''@'ON ERROR RESUME NEXT
				EVAL("SET aElements(i)."&sPropertyName&"="&vValue)
				IF err.Number<>0 THEN
					response.write "No se puede asignar la propiedad "&sPropertyName&" a un objeto de tipo "&TypeName(aElements(i))
					response.end
				END IF
			ELSE
				IF vValue="" THEN vValue=""""""
				EVAL("aElements(i)."&sPropertyName&"="&vValue)
			END IF
		NEXT
	End Sub
	
	Public Sub SetProperty(sPropertyName, ByRef vValue)
		DIM i
		FOR i=1 TO aElements(0)
			aElements(i).Properties.SetProperty sPropertyName, vValue
		NEXT
	End Sub
	
	Public Function GetCode()
		IF NOT Me.Render THEN 
			GetCode=""
			EXIT Function
		END IF
		sReturnString=""
		FOR iElementIndex = 1 TO aElements(0)
			sReturnString=sReturnString& aElements(iElementIndex).GetCode() & sSeparator
		NEXT
		GetCode=sReturnString
	End Function

	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub
End Class 
 %>