<% 
Class Styles
	'Properties
	Private iWidth,iHeight,sOverFlow,sDisplay, sVisibility, sBorder, sBackgroundColor, sCursor, sStyleString, sTextDecoration, sPosition, fPosTop, fPosLeft, sBehavior
	
	'Object
	Private oProperties
	
	'Private variables
	Private bRender, sReturnString

	Private Sub Class_Initialize()
		Set oProperties = new PropertiesCollection
		oProperties.PropertiesType="javascript"
	End Sub
	
	'Output objects
	Public Property Get Properties()
		Set Properties = oProperties
	End Property
	
	
	'Properties
	Public Property Get Bold()
		Bold = UCASE(oProperties.GetProperty("font-weight"))="BOLDER"
	End Property
	Public Property Let Bold(input)
		IF CBOOL(input) THEN
			oProperties.SetProperty "font-weight", "bolder"
		ELSE
			oProperties.RemoveProperty("font-weight")
		END IF
	End Property

	Public Property Get Underline()
		Underline = UCASE(oProperties.GetProperty("text-decoration"))="underline"
	End Property
	Public Property Let Underline(input)
		IF CBOOL(input) THEN
			oProperties.SetProperty "text-decoration", "underline"
		ELSE
			oProperties.RemoveProperty("text-decoration")
		END IF
	End Property

	Public Property Get Color()
		Color = oProperties.GetProperty("color")
	End Property
	Public Property Let Color(input)
		oProperties.SetProperty "color" , input
	End Property

	Public Property Get Width()
		Width = iWidth
	End Property
	Public Property Let Width(input)
		iWidth = input
		oProperties.SetProperty "width" , input
	End Property
	
	Public Property Get Height()
		Height = iHeight
	End Property
	Public Property Let Height(input)
		iHeight = input
		oProperties.SetProperty "height" , input
	End Property
	
	Public Property Get OverFlow()
		OverFlow = sOverFlow
	End Property
	Public Property Let OverFlow(input)
		sOverFlow = input
		oProperties.SetProperty "overflow" , input
	End Property
	
	'Output properties
	Public Property Get StyleString()
		StyleString = sStyleString
	End Property

	Public Property Get Behavior()
		Behavior = sBehavior
	End Property

	Public Property Get Position()
		Position = sPosition
	End Property

	Public Property Get PosTop()
		PosTop = fPosTop
	End Property

	Public Property Get PosLeft()
		PosLeft = fPosLeft
	End Property

	Public Property Get TextDecoration()
		TextDecoration = sTextDecoration
	End Property

	Public Property Get Cursor()
		Cursor = sCursor
	End Property

	Public Property Get Display()
		Display = sDisplay
	End Property

	Public Property Get Visibility()
		Visibility = sVisibility
	End Property

	Public Property Get Border()
		Border = sBorder
	End Property


	Public Property Get BackgroundColor()
		BackgroundColor = sBackgroundColor
	End Property

	'Input properties
	Public Property Let StyleString(input)
		sStyleString = input
	End Property

	Public Property Let PosTop(input)
		fPosTop = input
		oProperties.SetProperty "top", input
	End Property

	Public Property Let PosLeft(input)
		fPosLeft = input
		oProperties.SetProperty "left", input
	End Property

	Public Property Let Behavior(input)
		sBehavior = input
		oProperties.SetProperty "behavior", "url('"&sBehavior&"')"
	End Property

	Public Property Let Position(input)
		SELECT CASE UCASE(input)
		CASE UCASE("absolute"), UCASE("relative"), UCASE("static"), ""
			sPosition = input
		CASE ELSE 
			Err.Raise 1, "ASP 101", input & " is not supported."
			response.end
		END SELECT
		oProperties.SetProperty "position", input
	End Property

	Public Property Let TextDecoration(input)
		sTextDecoration = input
		oProperties.SetProperty "text-decoration", input
	End Property

	Public Property Let Cursor(input)
		sCursor = input
		oProperties.SetProperty "cursor", input
	End Property

	Public Property Let Visibility(input)
		SELECT CASE UCASE(input)
		CASE UCASE("inherit"), UCASE("visible"), UCASE("hidden")
			sVisibility = input
			bRender = TRUE
		CASE ELSE
			Err.Raise 1, "ASP 101", "Only inherit, visible and hidden are supported for this property"
			response.end
		END SELECT
		oProperties.SetProperty "visibility", input
	End Property

	Public Property Let Display(input)
		SELECT CASE UCASE(input)
		CASE UCASE("block"), UCASE("none"), UCASE("inline"), UCASE("list-item"), UCASE("table-header-group"), UCASE("table-footer-group"), ""
			sDisplay = input
			bRender = TRUE
		CASE ELSE
			Err.Raise 1, "ASP 101", "Only block, none, inline, list-item, table-header-group and table-footer-group are supported for this property"
			response.end
		END SELECT
		oProperties.SetProperty "display", input
	End Property

	Public Property Let Border(input)
		sBorder = input
		oProperties.SetProperty "border", input
	End Property

	Public Property Let BackgroundColor(input)
		sBackgroundColor = input
		oProperties.SetProperty "background-color", input
	End Property

	Public Function GetCode()
		DIM sStyle
		sStyle=TRIM(oProperties.GetCode())
		IF sStyle="" THEN EXIT Function
		GetCode=" style="""&sStyle & sStyleString&""""
	End Function
	'Public methods
	Public Sub WriteCode()
		response.write Me.GetCode()
	End Sub


End Class
 %>