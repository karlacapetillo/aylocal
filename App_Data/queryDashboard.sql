ALTER FUNCTION Productos.getDashboard() RETURNS XML AS BEGIN
declare @nov xml
SELECT
	@nov=(
		select top 3 img=F.Foto, Nombre=P.NombreProducto, Descripcion=P.DescripcionProducto
		FROM Productos.Producto P
		JOIN Productos.TipoCategoria TC on P.TipoArticulo=TC.TipoArticulo
		JOIN Productos.Fotos F on TC.Id=F.TipoArticulo
		WHERE P.Vendedor=TC.Vendedor
		order by P.Id desc
		FOR XML PATH('producto'), root('novedades'),ELEMENTS, TYPE
		)
declare @ven xml
select
	@ven=(
		select top 3 img=F.Foto, Nombre=P.NombreProducto, Descripcion=P.DescripcionProducto
		FROM Productos.Producto P
		JOIN VentAS.DetalleVenta DV on P.Vendedor=DV.Vendedor
		JOIN Productos.TipoCategoria TC on P.TipoArticulo=TC.TipoArticulo
		JOIN Productos.Fotos F on TC.Id=F.TipoArticulo
		WHERE P.Vendedor=TC.Vendedor
		FOR XML PATH('producto'), root('vendidos'),ELEMENTS, type
		)
declare @sug xml
select
	@sug=(
		select top 3 img=F.Foto, Nombre=P.NombreProducto, Descripcion=P.DescripcionProducto, Busquedas.*
		FROM Productos.Producto P
		JOIN Productos.TipoCategoria TC on P.TipoArticulo=TC.TipoArticulo
		JOIN Productos.Fotos F on TC.Id=F.TipoArticulo
		OUTER APPLY (
			SELECT Conteo=COUNT(1) FROM [Productos].[Busqueda] WHERE P.NombreProducto like '%'+RTRIM(BusquedaProducto)+'%'
		) Busquedas
		WHERE P.Vendedor=TC.Vendedor
		ORDER BY Conteo DESC
		FOR XML PATH('producto'), root('sugerencias'),ELEMENTS, type
		)

DECLARE @output XML
;WITH XMLNAMESPACES(
			'http://panax.io/xdom/binding/interval' AS interval
			, 'http://panax.io/xdom/binding/source' AS source
			, 'http://panax.io/messages' AS message
			, 'http://panax.io/shell' AS shell
			)
SELECT @output=(
	select @nov, @ven, @sug for xml path ('shell:dashboard'),root('shell:shell'), elements, type
)

RETURN @output
END


GO


EXEC [Productos].[buscar] 'Panqu�'


SET NOCOUNT ON; IF OBJECT_ID('#Object.FindObjectsInQuery') IS NOT NULL BEGIN SELECT TOP 1 [Type], [Object_Name] FROM #Object.FindObjectsInQuery('Productos.getDashboard()') ORDER by Position END ELSE BEGIN SELECT [Type]=NULL, [Object_Name]=NULL WHERE 1=0 END


SELECT [$Tools].getOutputParameters('Productos.Dashboard')


BEGIN TRY SET NOCOUNT ON; EXEC Productos.Dashboard ;  END TRY BEGIN CATCH DECLARE @Message NVARCHAR(MAX); SELECT @Message=ERROR_MESSAGE(); EXEC [$Table].[getCustomMessage] @Message=@Message, @Exec=1; END CATCH