---Trigger que crea el login y el usuario en caso de que no se haya registrado
ALTER TRIGGER [Seguridad].[Usuario:crearLogin] ON [Seguridad].[Usuario]
FOR INSERT, UPDATE, DELETE
AS BEGIN
select * from inserted
select * from deleted
	DECLARE @username NVARCHAR(MAX)	
	SELECT	@username=RTRIM(inserted.Username)
	FROM	inserted 
	WHERE	1=1
	AND		NOT EXISTS(
		SELECT [$pk]=s.principal_id
		, [$username]=S.name
		, [$password]=sl.password_hash
		, [$enabled]=CONVERT(bit,CONVERT(tinyint,sl.is_disabled)-1)
		FROM sys.database_principals S
		JOIN master.sys.sql_logins SL ON S.name=SL.name COLLATE Latin1_General_CI_AI
		WHERE s.type='S' 
		AND s.authentication_type=1 
		AND s.name NOT IN ('dbo')
		AND s.name=inserted.Username
	)
	select @username
	IF @username IS NOT NULL BEGIN
		EXEC [Seguridad].[crearUsuario] @username=@username
	END
	select @username=null
	SELECT	@username=RTRIM(deleted.Username)
	FROM	deleted
	WHERE NOT EXISTS(select 1 from inserted where deleted.Id=inserted.Id)
		IF @username IS NOT NULL BEGIN
			EXEC [Seguridad].[eliminarUsuario] @username=@username	
		END
END
GO



ALTER PROCEDURE [Seguridad].[eliminarUsuario](@userName NVARCHAR(MAX)) AS BEGIN
	IF  @userName is null begin 
		return
	end

	DECLARE @command NVARCHAR(MAX)
	--los usuarios son a nivel bd
	IF EXISTS (
		select 1 
		from sys.database_principals s
		where s.name=@userName) begin
	SELECT @command=N'DROP USER IF EXISTS '+QUOTENAME(@userName)+''
	EXEC sp_executesql @command,N'@userName NVARCHAR(MAX)', @userName
	end
	--login a nivel instancia
	IF EXISTS (
		select 1 
		from master.sys.sql_logins sl
		where sl.name=@userName) begin
	SELECT @command=N'DROP LOGIN '+QUOTENAME(@userName)+''
	EXEC sp_executesql @command,N'@userName NVARCHAR(MAX)', @userName
	END
END
GO


ALTER PROCEDURE [Seguridad].[crearUsuario](@userName NVARCHAR(MAX), @encrypted_password NVARCHAR(MAX)='40A965D05136639974C40FAF6CFDF21D') AS BEGIN

	DECLARE @command NVARCHAR(MAX)
	--login a nivel instancia
	SELECT @command=N'CREATE LOGIN '+QUOTENAME(@userName)+' WITH PASSWORD='+QUOTENAME(RTRIM(@encrypted_password),'''')+', DEFAULT_DATABASE='+QUOTENAME(DB_NAME())+', DEFAULT_LANGUAGE=[spanish], CHECK_EXPIRATION=OFF, CHECK_POLICY=ON'
	EXEC sp_executesql @command,N'@userName NVARCHAR(MAX), @encrypted_password NVARCHAR(MAX)', @userName, @encrypted_password
	--los usuarios son a nivel bd
	SELECT @command=N'CREATE USER '+QUOTENAME(@userName)+' FOR LOGIN '+QUOTENAME(@userName)+' WITH DEFAULT_SCHEMA=dbo'
	EXEC sp_executesql @command,N'@userName NVARCHAR(MAX)', @userName

	--EXEC master..sp_addsrvrolemember @loginame = @userName, @rolename = N'bulkadmin'
	EXEC sp_addrolemember N'Panax_User', @userName
	EXEC sp_addrolemember N'Comprador', @userName

END
GO

delete from Seguridad.Usuario where Username='laura.1203@gmail.com'

SELECT * FROM Seguridad.Usuario

INSERT INTO Seguridad.Usuario (Username, Perfil) SELECT 'laura.1203@gmail.com', 1
GO

UPDATE Seguridad.Usuario SET Activo=0 WHERE Username='laura.1203@gmail.com'
go
