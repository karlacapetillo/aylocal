USE [master]
GO --EXEC #Database.createBackup 'AyLocal_parcial_v1.1.322.bak'
IF EXISTS(SELECT 1 FROM master.sys.databases sd WHERE name = N'ProyectoAyLocal')
	ALTER DATABASE [ProyectoAyLocal] SET SINGLE_USER WITH ROLLBACK IMMEDIATE 
GO
IF EXISTS(SELECT 1 FROM master.sys.databases sd WHERE name = N'ProyectoAyLocal')
	EXEC master.dbo.sp_detach_db @dbname = N'ProyectoAyLocal', @keepfulltextindexfile = N'true'
GO
RESTORE DATABASE [ProyectoAyLocal] -- FILELISTONLY --
FROM DISK = 'D:\Dropbox (Personal)\Proyectos\AyLocal\App_Data\ProyectoAyLocal_backup_202007061857.bak'
WITH REPLACE, MOVE 'Panax' TO 'D:\Dropbox (Personal)\Proyectos\AyLocal\App_Data\ProyectoAyLocal.mdf',
MOVE 'Panax_log' TO 'D:\Dropbox (Personal)\Proyectos\AyLocal\App_Data\ProyectoAyLocal_log.ldf'
GO

USE [ProyectoAyLocal]
GO
EXEC #Panax.setup
--SELECT * FROM #Cache.Configurations
--SELECT * FROM [$Cache].[Configurations]