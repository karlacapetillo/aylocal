EXEC #entity.Config 'SocioDeNegocios.Asociado','PersonaFisica','@layout:moveAfter','NombreNegocio'
EXEC #entity.Config 'SocioDeNegocios.Asociado','PersonaMoral','@layout:moveAfter','NombreNegocio'
EXEC #entity.Config 'SocioDeNegocios.Asociado','FechaInicio','@mode','none'
EXEC #entity.Config 'SocioDeNegocios.Asociado','Vendedor','@mode','none'


EXEC #Configuration.Rebuild
SELECT * FROM #Cache.Configurations WHERE EntityName='Asociado'

SELECT * FROM [#Entity].getfields('SocioDeNegocios.Asociado')
SELECT * FROM [$Ver:Beta_12].InformationSchema

GO
DECLARE @EntityName NVARCHAR(MAX)
SELECT @EntityName='Producto'
;WITH XMLNAMESPACES('http://panax.io/entity' as px, 'http://www.w3.org/2001/XMLSchema-instance' as control, 'http://panax.io/session' as session)
SELECT n.x.query('.') --select *
FROM #panax.Structure Px
CROSS APPLY px.xmlStructure.nodes('(/px:root/px:Entities/px:Entity[@Name=sql:variable("@EntityName")])[1]')n(x)
GO

EXEC #entity.Config 'Productos.Producto','DetalleVenta','@mode','none'

GO
EXEC #Metadata.rebuild

DECLARE @full_entity_name nvarchar(MAX)
SELECT @full_entity_name='Proyectos.Proyecto'
SELECT @full_entity_name='[Productos].[Articulo]'
DECLARE @SchemaName NVARCHAR(MAX), @EntityName NVARCHAR(MAX)
SELECT @SchemaName=PARSENAME(@full_entity_name, 2)
SELECT @EntityName=PARSENAME(@full_entity_name, 1)
SELECT * 
--	UPDATE C SET Version=NULL
	FROM #Cache.Configurations C
--	WHERE Version=''
WHERE EntitySchema=@SchemaName AND EntityName=@EntityName
ORDER BY EntitySchema, EntityName, FieldName

EXEC [$Ver:Beta_12].clearCache @full_entity_name
;WITH XMLNAMESPACES('http://panax.io/entity' as px, 'http://www.w3.org/2001/XMLSchema-instance' as control, 'http://panax.io/session' as session)
DELETE Px --SELECT *
FROM #panax.Structure Px
CROSS APPLY px.xmlStructure.nodes('(/px:root/px:Entities/px:Entity[@Name=sql:variable("@EntityName")])[1]')n(x)
GO

EXEC [$Ver:Beta_12].getXmlData @@userId='-1', @FullPath='', @TableName='Productos.Producto', @Mode='add', @PageIndex=DEFAULT, @PageSize=DEFAULT, @MaxRecords=DEFAULT, @ControlType=DEFAULT, @Filters=DEFAULT, @Sorters=DEFAULT, @Parameters=DEFAULT, @lang=es, @getData=1, @getStructure=1, @rebuild=DEFAULT, @columnList=DEFAULT, @output=HTML


SELECT * FROM Productos.Producto

EXEC #entity.clearconfig 'Productos.Producto','Fotos','scaffold','true'


SELECT * FROM [Productos].[Articulo]


SELECT *
FROM #entity.getFields('[Productos].[Articulo]')

SELECT * 
FROM #Cache.Configurations 
WHERE EntityName='Orden'

EXEC #Entity.config '[Productos].[Articulo]','DetalleVenta','@mode','none'

EXEC #Entity.config '[Productos].[Articulo]','Fotos','@scaffold','true'
EXEC #Entity.config '[Productos].[Articulo]','Fotos','@position','after:Servicio'

EXEC [$Ver:Beta_12].getXmlData @@userId='-1', @FullPath='', @TableName='[Productos].[Articulo]', @Mode='edit', @PageIndex=DEFAULT, @PageSize=DEFAULT, @MaxRecords=DEFAULT, @ControlType=DEFAULT, @Filters='[Id]=8', @Sorters=DEFAULT, @Parameters=DEFAULT, @lang=es, @getData=1, @getStructure=1, @rebuild=DEFAULT, @columnList=DEFAULT, @output=HTML




select * from [$Ver:Beta_12].InformationSchema

EXEC #panax.createVersion 'Beta_12', 1


EXEC [$Ver:Beta_12].getXmlData @@userId='-1', @FullPath='', @TableName='[Productos].[Articulo]', @Mode='view', @PageIndex=DEFAULT, @PageSize=DEFAULT, @MaxRecords=DEFAULT, @ControlType=DEFAULT, @Filters=DEFAULT, @Sorters=DEFAULT, @Parameters=DEFAULT, @lang=es, @getData=1, @getStructure=1, @rebuild=DEFAULT, @columnList=DEFAULT, @output=HTML--, @debug=1


EXEC [$Ver:Beta_12].getXmlData @@userId='-1', @FullPath='', @TableName='[Productos].[Articulo]', @Mode='add', @PageIndex=DEFAULT, @PageSize=DEFAULT, @MaxRecords=DEFAULT, @ControlType=DEFAULT, @Filters=DEFAULT, @Sorters=DEFAULT, @Parameters=DEFAULT, @lang=es, @getData=1, @getStructure=1, @rebuild=DEFAULT, @columnList=DEFAULT, @output=HTML


EXEC [$Ver:Beta_12].getXmlData @@userId='-1', @FullPath='', @TableName='[Productos].[Producto]', @Mode='edit', @PageIndex=DEFAULT, @PageSize=DEFAULT, @MaxRecords=DEFAULT, @ControlType=DEFAULT, @Filters='[Id]=2', @Sorters=DEFAULT, @Parameters=DEFAULT, @lang=es, @getData=1, @getStructure=1, @rebuild=DEFAULT, @columnList=DEFAULT, @output=HTML



EXEC [$Ver:Beta_12].getXmlData @@userId='-1', @FullPath='', @TableName='[Productos].[Articulo]', @Mode='edit', @PageIndex=DEFAULT, @PageSize=DEFAULT, @MaxRecords=DEFAULT, @ControlType=DEFAULT, @Filters='[Id]=11', @Sorters=DEFAULT, @Parameters=DEFAULT, @lang=es, @getData=1, @getStructure=1, @columnList=DEFAULT, @output=HTML, @debug=1, @rebuild=1



EXEC [$Ver:Beta_12].getXmlData @@userId='-1', @FullPath='', @TableName='[SocioDeNegocios].[Asociado]', @Mode='edit', @PageIndex=DEFAULT, @PageSize=DEFAULT, @MaxRecords=DEFAULT, @ControlType=DEFAULT, @Filters='[Id]=8', @Sorters=DEFAULT, @Parameters=DEFAULT, @lang=es, @getData=1, @getStructure=1, @columnList=DEFAULT, @output=HTML, @rebuild=1




EXEC [$Ver:Beta_12].getXmlData @@userId='-1', @FullPath='', @TableName='SocioDeNegocios.Asociado', @Mode='add', @PageIndex=DEFAULT, @PageSize=DEFAULT, @MaxRecords=DEFAULT, @ControlType=DEFAULT, @Filters='[Id]=11', @Sorters=DEFAULT, @Parameters=DEFAULT, @lang=es, @getData=1, @getStructure=1, @columnList=DEFAULT, @output=HTML, @debug=1, @rebuild=1


EXEC #Entity.config '[Productos].[Producto]','CertificacionesProducto','@mode','none'


EXEC [#Entity].Config 'Productos.Producto','CertificacionesProducto','@position','after:Patentes'


EXEC [#Entity].Config 'sociodenegocios.asociado',''

EXEC [#Entity].clearConfig 'SocioDeNegocios.Asociado','TipoPersona','@container:groupTabPanel','Información General'



EXEC [$Ver:Beta_12].getXmlData @@userId='-1', @FullPath='', @TableName='[SocioDeNegocios].[Asociado]', @Mode='edit', @PageIndex=DEFAULT, @PageSize=DEFAULT, @MaxRecords=DEFAULT, @ControlType=DEFAULT, @Filters='[Id]=8', @Sorters=DEFAULT, @Parameters=DEFAULT, @lang=es, @getData=1, @getStructure=1, @rebuild=DEFAULT, @columnList=DEFAULT, @output=HTML, @debug=1



EXEC [#Entity].Config '[SocioDeNegocios].[Asociado]','Venta','@mode','none'


EXEC [#Entity].Config 'Seguridad.Usuario','Password','@controlType','password'

EXEC #Configuration.Rebuild




EXEC [$Ver:Beta_12].getXmlData @@userId='-1', @FullPath='', @TableName='[Seguridad].[Usuario]', @Mode='edit', @PageIndex=DEFAULT, @PageSize=DEFAULT, @MaxRecords=DEFAULT, @ControlType=DEFAULT, @Filters='[Id]=2', @Sorters=DEFAULT, @Parameters=DEFAULT, @lang=es, @getData=1, @getStructure=1, @rebuild=1, @columnList=DEFAULT, @output=HTML


EXEC [$Ver:Beta_12].getXmlData @@userId='-1', @FullPath='', @TableName='[Seguridad].Usuario', @Mode='view', @PageIndex=1, @PageSize=30, @MaxRecords=DEFAULT, @ControlType=DEFAULT, @Filters=DEFAULT, @Sorters=DEFAULT, @Parameters=DEFAULT, @lang=es, @getData=1, @getStructure=1, @rebuild=1, @columnList=DEFAULT, @output=HTML, @debug=1


